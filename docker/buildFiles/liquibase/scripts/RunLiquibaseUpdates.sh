#!/bin/bash -x
#  run CheckDBVersionPlugin (this gets db connection), then LiquibaseUpdatePlugin
# UpdateWithLiquibasePlugin won't run if CheckDBVersionPlugin failes

# Input Folders - the output path is used to write result of version check, and any liquibase output files
# It is assumed the configFile lives in the dataDir
# if postgres is used, the db is accessed via the url from the config file
# if sqlite is used, the db file should be mounted to the output Directory.
# Mount localMachine:/pathToOutputDir/ to docker://tempFileDir/outputDir/
# Mount localMachine:/pathToDataDir to docker://tempFileDir/data/

OUTPUT_PATH=/tempFileDir/outputDir
DATA_PATH=/tempFileDir/data

dbConfigFile=$1
# command should be either "status" or "update"
lbCommand=$2

memoryString="`grep '^Xmx=' ${DATA_PATH}/${dbConfigFile} | cut -d'=' -f2`"
if [ ${memoryString} != "" ]
then
    fullXmx='-Xmx'${memoryString}
else
	fullXmx='-Xmx10G'
fi

# first run CheckDBVersionPlug, followed on same line by LiquibaseUpdatePlugin

/tassel-5-standalone/run_pipeline.pl ${fullXmx} -debug -configParameters $DATA_PATH/$dbConfigFile -CheckDBVersionPlugin -outputDir $OUTPUT_PATH -endPlugin -LiquibaseUpdatePlugin -command $lbCommand -outputDir $OUTPUT_PATH -endPlugin
