--liquibase formatted sql

--## update read mapping and paths tables for postgres and sqlite

--changeset lcj34:drop_read_mapping_read_mapping_group
DROP TABLE IF EXISTS read_mapping_read_mapping_group;

--changeset lcj34:drop_read_mapping_group
DROP TABLE IF EXISTS read_mapping_group;

--changeset lcj34:create_read_mapping_paths_sqlite dbms:sqlite

DROP TABLE IF EXISTS read_mapping_paths;
CREATE TABLE read_mapping_paths ( read_mapping_id INTEGER, path_id INTEGER,UNIQUE(read_mapping_id,path_id));

--changeset lcj34:create_read_mapping_paths_postgres dbms:postgresql

DROP TABLE IF EXISTS read_mapping_paths;
CREATE TABLE read_mapping_paths ( read_mapping_id INTEGER,path_id INTEGER, UNIQUE(read_mapping_id,path_id));

--changeset lcj34:update_paths_table_with_genoid_sqlite dbms:sqlite

DROP TABLE IF EXISTS paths;
CREATE TABLE paths ( path_id INTEGER PRIMARY KEY, genoid INTEGER, method_id INTEGER, paths_data BLOB, UNIQUE(genoid,method_id));

--changeset lcj34:update_paths_table_with_genoid_postgres dbms:postgresql

DROP TABLE IF EXISTS paths;
CREATE TABLE paths ( path_id SERIAL PRIMARY KEY, genoid INTEGER, method_id INTEGER, paths_data bytea, UNIQUE(genoid,method_id));

