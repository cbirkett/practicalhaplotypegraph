--liquibase formatted sql

--## add field asm_strand to haplotypes table.

--changeset lcj34:add_asmstrand_to_haplotypes_sqlite_0.0.35 dbms:sqlite
ALTER TABLE haplotypes ADD COLUMN asm_strand TEXT DEFAULT '.';

--changeset lcj34:add_asmstrand_to_haplotypes_postgres_0.0.35 dbms:postgresql
ALTER TABLE haplotypes ADD COLUMN asm_strand varchar(1) DEFAULT '.';