--liquibase formatted sql

--## create read_mapping and read_mapping_group tables for postgres and sqlite
--## REMEMBER: all changset ids MUST end with the PHG release these will go into !!!
--## that is how our updating process works to tell user minimum laod they need for running this.

--changeset lcj34:createGenomeFileData_sqlite_0.0.24 dbms:sqlite
DROP TABLE IF EXISTS genome_file_data;
CREATE TABLE genome_file_data(id INTEGER PRIMARY KEY, genome_path TEXT, genome_file TEXT, file_checksum TEXT, genoid INTEGER);


--changeset lcj34:createGenomeFileDataTable_postgres_0.0.24 dbms:postgresql
DROP TABLE IF EXISTS genome_file_data;
CREATE TABLE genome_file_data(id SERIAL PRIMARY KEY, genome_path INTEGER, genome_file TEXT, file_checksum TEXT, genoid INTEGER);

--## index commands are the same for both databases
--changeset lcj34:create_genome_file_data_unique_idx_0.0.24
CREATE UNIQUE INDEX genome_file_data_genoid_genome_file_key on genome_file_data (genoid, genome_file);
