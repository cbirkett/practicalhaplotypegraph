--liquibase formatted sql

--## create read_mapping and read_mapping_group tables for postgres and sqlite

--changeset lcj34:createTaxaGroupsTable_sqlite_0.0.27 dbms:sqlite
DROP TABLE IF EXISTS taxa_groups;
CREATE TABLE taxa_groups(taxa_grp_id INTEGER PRIMARY KEY, taxa_grp_name TEXT UNIQUE);

DROP TABLE IF EXISTS taxa_groups_genoid;
CREATE TABLE taxa_groups_genoid(taxa_grp_id INTEGER, genoid INTEGER);


--changeset lcj34:createTaxaGroupsTable_postgres_0.0.27 dbms:postgresql
DROP TABLE IF EXISTS taxa_groups;
CREATE TABLE taxa_groups(taxa_grp_id SERIAL PRIMARY KEY, taxa_grp_name TEXT UNIQUE);

DROP TABLE IF EXISTS taxa_groups_genoid;
CREATE TABLE taxa_groups_genoid(taxa_grp_id INTEGER, genoid INTEGER);

--## index commands are the same for both databases
--changeset lcj34:create_taxa_groups_genoid_unique_idx_0.0.27
CREATE UNIQUE INDEX taxa_groups_genoid_unique_idx on taxa_groups_genoid (taxa_grp_id, genoid);


