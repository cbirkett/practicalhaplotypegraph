kmer_count_tabf=$1
kcount_topprop_toremove=$2


n_kmers=$(wc -l <$kmer_count_tabf)
n_toremove=$(echo -e "${kcount_topprop_toremove}\t${n_kmers}" | $awk_bin '{print int($1 * $2)}')

sort -k2,2nr -t $'\t' $kmer_count_tabf | \
head -n $n_toremove | \
tail -n 1 | \
cut -f2 -d$'\t'
