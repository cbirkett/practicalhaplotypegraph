genome_gff=$1
merge_max_d=$2

#bt_bin=$(which bedtools)

grep -v '^#' $genome_gff | \
$awk_bin '
BEGIN{
  FS="\t";
  OFS="\t";
}
{
  if($3 == "gene"){
    split($9, a, ";");
    for(i in a){
      split(a[i], ap, "=");
      annot[ap[1]] = ap[2]
    }
    print $1, $4-1, $5, annot["ID"], $7
  }
}' | \
sort -k1,1V -k2,2n -t$'\t' | \
$bt_bin merge -d $merge_max_d -c 4 -o distinct -i stdin

