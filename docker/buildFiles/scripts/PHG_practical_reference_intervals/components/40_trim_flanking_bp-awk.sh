
initial_anchors_f=$1
genes_f=$2
genome_fasta_f=$3
jf_tsv_f=$4
mavg_max=$5
mavg_ln=${6:-100}
mavg_step=${7:-10}
jf_kln=${8:-11}

#bt_bin=$(which bedtools)



mavg_extend=$(( $mavg_ln / 2 ))

$bt_bin intersect -nonamecheck -wa -wb -a $initial_anchors_f -b $genes_f | \
$awk_bin '
BEGIN{
  FS="\t";
  OFS="\t"
}
{
  if($6 <= $2){
    $6 = $2 + 1
  }; 
  if($3 <= $7){
    $3 = $7 + 1
  };
  print $1, $2, $6, "l:"$4"\n"$1, $7, $3, "r:"$4
}' | \
$bt_bin getfasta -name+ -tab -fi $genome_fasta_f -bed stdin | \
$awk_bin -v kln=${jf_kln} -v mavgln=${mavg_ln} -v mavgmax=${mavg_max} -v mavgstep=${mavg_step} -v jfile=${jf_tsv_f} '

function eval_avg_calc(evalseq, kln, kcount)
{
  eval_sum = ""
  eval_count = 0
  for(j = 1; j < length(evalseq) - kln; j++){
    kseq = substr(evalseq, j, kln)

# print "subeval: "kseq"\t"j"\t"kcount[kseq]

    if(kseq in kcount){
      eval_sum = eval_sum + kcount[kseq] + 1
    }
    else{
      eval_sum = eval_sum + 1
    }
    eval_count++
  }
  if(eval_count == 0){
    return(0)
  }
  else{
    return(eval_sum / eval_count)
  }
}
BEGIN{
  FS="\t";
  OFS="\t";
  while(( getline line < jfile ) > 0 ) {
    patsplit(line, kline, "[^[:space:]]+")
    kcount[kline[1]]=kline[2]
  }
}
{

  n = patsplit($1, defline, "[^:-]+")
  name = ""
  for(l = 2; l < n-2; l++){
    name = name":"defline[l]
    sub("^:", "", name)
  }
  if(defline[1] == "l"){
    trim_start = defline[n-1]
    trim_end = defline[n]+1    
    extd =  int(mavgln/2)
    for(i = length($2); i > mavgstep; i = i - mavgstep){
      s = i - extd;
      if(s <= 0){ s = 1 }
      evalstr = substr($2, s, mavgln)
      eval_avg = eval_avg_calc(evalstr, kln, kcount)

      if(eval_avg > mavgmax){
        tstart = trim_start + i + int(mavgstep/2)
        if(tstart >= trim_end){
          tstart = trim_end-1
        }
        trim_start = tstart
        break
      }
    }
  }
  else{
    trim_start = defline[n-1]-1
    trim_end = defline[n]
    extd =  int(mavgln/2)
    for(i = mavgstep; i < length($2) - mavgstep; i = i + mavgstep){
      s = i - extd;
      if(s <= 0){ s = 1 }
      evalstr = substr($2, s, mavgln)
      eval_avg = eval_avg_calc(evalstr, kln, kcount)

      if(eval_avg > mavgmax){
        tend = trim_start + 1 + i - int(mavgstep/2)
        if(tend -int(mavgstep/2) <= trim_start + 1){
          tend = trim_start + 1
        }
        trim_end = tend
        break
      }
    }
  }
  print defline[n-2], trim_start, trim_end, name
}
'   | \
sort -k4,4b | \
$bt_bin groupby -g 1,4 -c 2,3 -o min,max | \
$awk_bin '{print $1"\t"$3"\t"$4"\t"$2}' | \
sort -k1,1V -k2,2n 
