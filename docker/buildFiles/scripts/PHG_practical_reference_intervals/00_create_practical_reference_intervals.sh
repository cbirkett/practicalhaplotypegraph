#!/bin/bash

#================================
# FUNCTION DEFINITIONS
#--------------------------------

#print instructions for script use when called
usage () {
cat <<EOF
SCRIPT PURPOSE:
  Generate reference intervals for the Practical Haplotype Graph (PHG)

EXTERNAL DEPENDENCIES:
  * bedtools 2.27.1   [https://github.com/arq5x/bedtools2/releases/tag/v2.27.1]
  * jellyfish 2.2.10  [https://github.com/gmarcais/Jellyfish/releases/tag/v2.2.10]
  * samtools
  * gawk

REQUIRED PARAMETERS:
  -i <directory path>
     Directory containing the reference sequence (fasta) and annotation (gff)
  -f <file name>
     name of fasta file containing the reference sequence
  -a <file name>
     name of genome annotation file in .gff format containing gene model annotation

OPTIONAL PARAMETERS:
  -o <directory path>
     Full path to output directory in which a sub-folder for the run will be created
     Default: input directory
  -k <integer>
     Length of kmer used for determining repetitive regions 
     Default: 11
  -e <integer>
     Number of bases by which to expand gene models for initial reference interval selection
     Default: 1000
  -m <integer>
     Distance (in bp) between genes below which gene models are merged
     Default: 100
  -p <double>
     Proportion of kmers to be considered repetitive. 
     This determines the high kmer count tail which is considered repetitive (e.g. the top 0.05 most frequent)
     Default: 0.1
  -n <integer> 
     Number of kmer copies (genome-wide) above which a kmer is considered repetitive. Overrides -p
     Default: none, -p is used by default
  -l <integer>
     The number of bases to consider when evaluating if a location in the genome is repetitive
     Default: 100
  -s <integer>
     The step size (in bp) by which to proceed outward from a gene model when evaluating flanking regions
     Default: 10

  -h This help message
EOF
}


#================================
# AUTOMATIC PARAMETERS
#--------------------------------

# I/O parameters
#---------------------
script_base_d="$(dirname "$(readlink -enq "$0")")"
components_d="${script_base_d}/components"


# Software parameters
#---------------------
export PATH="${script_base_d}/ext_bin/bin:${script_base_d}/ext_bin:$PATH"
export LD_LIBRARY_PATH="${script_base_d}/ext_bin/lib"
export st_bin=$(which samtools)
export bt_bin=$(which bedtools)
export jf_bin=$(which jellyfish)
export awk_bin=$(which gawk)



# Hardware parameters
#---------------------
# NOTE: Sets maximum RAM to use to the total accessible RAM (free + cached), minimum 5GB
#       Sets number of threads to use to one less than the total number of processors, minimum 1

min_RAM_use='5'                    # Need at least this much RAM for indexing (GB)
min_threads='1'                    # Need at least one thread/processor to run


# Set the values below if you want to overwrite the automatic defaults
gb_RAM='100'
nr_t=''

# Set max RAM usage for kmer counting if not specified
free_bin_f="$(readlink -enq "$(which 'free')")"
if [ -n "${free_bin_f}" ]
then
    avail_RAM="$(${free_bin_f} -g | awk '/^\s*^Mem:/{print $4+$7}')"
    # Exit with error if we can't allocate enough RAM
    if [ "$avail_RAM" -lt "$min_RAM_use" ]
    then
        echo "ERROR: Could not allocate enough RAM for kmer counting. Need at least ${min_RAM_use} Gb of RAM" >&2
        exit 1
    fi
fi

# Set the RAM amount; if specified less than min_RAM_use, set to $min_RAM_use
[ "${gb_RAM:=${avail_RAM}}" -lt "${min_RAM_use}" ] && gb_RAM=$min_RAM_use


# Find number of processors/threads available
getconf_bin_f="$(readlink -enq "$(which 'getconf')")"
[ -n "${getconf_bin_f}" ] && nr_t=$(( $(${getconf_bin_f} _NPROCESSORS_ONLN) - 1 ))
[ "${nr_t:=${min_threads}}" -lt "${min_threads}" ] && nr_t="${min_threads}"




#================================
# ARGUMENT PARSING
#--------------------------------

#print usage if no options were given
if [ -z "$1" ]
then
    usage
    exit 0
fi

OPTIND='1'
options=':i:f:a:o:k:e:m:n:p:l:s:h'
while getopts $options option
do
    case $option in
	i  ) input_d=$OPTARG;;
	f  ) fasta_inf=$OPTARG;;
	a  ) annot_inf=$OPTARG;;
	o  ) out_d=$OPTARG;;
	k  ) mer_ln=$OPTARG;;
	e  ) gene_expand_bp=$OPTARG;;
	m  ) min_gene_dist=$OPTARG;;
	n  ) max_kmer_count=$OPTARG;;
	p  ) trim_topkmer_prop=$OPTARG;;
	l  ) mavg_ln=$OPTARG;;
	s  ) mavg_step=$OPTARG;;
	h  ) usage; exit 0;;
        \? ) echo "Unknown option: -$OPTARG" >&2; exit 1;;
        :  ) echo "Missing option argument for -$OPTARG" >&2; exit 1;;
        *  ) echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
    esac
done


#====================================
# REQUIRED PARAM ERRORS AND DEFAULTS
#------------------------------------

# Required binaries
[ -z "${jf_bin}" ] && unset jf_bin
[ -z "${bt_bin}" ] && unset bt_bin
[ -z "${st_bin}" ] && unset st_bin
[ -z "${awk_bin}" ] && unset awk_bin
: ${jf_bin?"ERROR: Could not find the jellyfish executable on this system"}
: ${bt_bin?"ERROR: Could not find the bedtools executable on this system"}
: ${st_bin?"ERROR: Could not find the samtools executable on this system"}
: ${awk_bin?"ERROR: Could not find gawk executable on this system"}

# Required I/O
input_d="$(readlink -enq "${input_d}")"
[ -z "${input_d}" ] && unset input_d
: ${input_d?"ERROR: Could not find input directory"}


fasta_inf=$(readlink -enq "${input_d%/}/${fasta_inf}")
annot_inf=$(readlink -enq "${input_d%/}/${annot_inf}")
[ -z "${fasta_inf}" ] && unset fasta_inf
[ -z "${annot_inf}" ] && unset annot_inf
: ${fasta_inf?"ERROR: Could not find reference genome fasta file"}
: ${annot_inf?"ERROR: Could not find genome annotation gff file"}

# Optional parameters
: ${out_d:="${input_d}"}
: ${mer_ln:=11}
: ${gene_expand_bp:=1000}
: ${min_gene_dist:=100}
: ${mavg_ln:=100}
: ${mavg_step:=10}

# NOTE: $max_kmer_count and $trim_topkmer_prop are mutually exclusive
#       $max_kmer_count takes precedence
if [ -z "${max_kmer_count}" ]
then
: ${trim_topkmer_prop:=0.1}
fi


# cat <<EOF
# -i ${input_d}
# -f |${fasta_inf}|
# -a |${annot_inf}|
# -o ${out_d}
# -k ${mer_ln}
# -e ${gene_expand_bp}
# -m ${min_gene_dist}
# -p ${trim_topkmer_prop}
# -n ${max_kmer_count:-'NA'}
# -l ${mavg_ln}
# -s ${mavg_step}
#
# EOF
# exit 0

# create output subdirectory for this run
out_d="${out_d%/}/genomic_intervals_$(date -u +%F-%s)"
mkdir -p ${out_d}
out_d="$(readlink -enq ${out_d})"
[ -z "${out_d}" ] && unset out_d
: ${out_d?"ERROR: Could not create output directory."}

# create an output file with run parameters
outlog_f="${out_d}/reference_intervals_run.log"
echo -e "Run at $(date -u)" > $outlog_f
echo -e "\nRuntime parameters:" >> $outlog_f
cat >>$outlog_f <<EOF
-i ${input_d}
-f ${fasta_inf}
-a ${annot_inf}
-o ${out_d}
-k ${mer_ln}
-e ${gene_expand_bp}
-m ${min_gene_dist}
-n ${max_kmer_count}
-p ${trim_topkmer_prop}
-l ${mavg_ln}
-s ${mavg_step}

EOF


#=======================================
# RUN THE COMPONENTS
#---------------------------------------

echo -e "Run started: $(date -u).\nSaving results to folder\n\t${out_d}\nLogging run to\n\t${outlog_f}"

# Extract gene models and gene ids from gff file
# Merge overlapping gene models and gene models less than $min_gene_dist apart
# Concatenate gene ids for merged genes in a comma separated list
gene_bed_outf="${out_d%/}/$(basename ${fasta_inf%.*}).gene.bed"
bash ${components_d}/10_create_gene_bed.sh ${annot_inf} ${min_gene_dist} > ${gene_bed_outf}
echo "Created genes bed file $gene_bed_outf" >> $outlog_f

# Index the fasta file 
# Create a genome file for use by bedtools
genome_outf="${out_d%/}/$(basename ${fasta_inf%.*}).genome"
bash ${components_d}/15_create_genome_file.sh ${fasta_inf} > ${genome_outf}
echo "Created genome file $genome_outf" >> $outlog_f


# Create jellyfish database and tabular output of kmer counts
# Set kmer data RAM to 1/4 available, bloom filter to 1/2
# Set nr threads to 1/2 of those available
k_RAM=$(( ${gb_RAM} / 4 ))
b_RAM=$(( ${gb_RAM} / 2 ))
threads=$(( ${nr_t} / 2 ))
kcount_outf_base="${out_d%/}/$(basename ${fasta_inf%.*}).${mer_ln}mer_count"
bash ${components_d}/20_count_kmers.sh ${fasta_inf} ${mer_ln} ${kcount_outf_base} ${threads} ${k_RAM} ${b_RAM}
echo "Created kmer count files ${kcount_outf_base}.jf and ${kcount_outf_base}.tsv"  >> $outlog_f


# Expand the gene intervals by the specified $gene_expand_bp amount
# If gene intervals overlap after the expansion, separate them at midpoint
gene_expand_outf="${out_d%/}/$(basename ${gene_bed_outf%.*}).expand.bed"
bash ${components_d}/30_create_anchor_start_bed.sh ${gene_bed_outf} ${genome_outf} ${gene_expand_bp} > ${gene_expand_outf}
echo "Created expanded genes file $gene_expand_outf" >> $outlog_f

# Set the kmer count threshold
# If a number was provided explicitly, use that
# Otherwise, calculate from trimming proportion
: ${max_kmer_count:=$(bash ${components_d}/99_kmer_count_cutoff_val.sh ${kcount_outf_base}.tsv $trim_topkmer_prop)}
echo "Setting max kmer count to ${max_kmer_count}"  >> $outlog_f

# Trim the expanded gene intervals based on the repetiveness of flanking regions
# Procedure:  
#   - Walk out from edge of gene every $mavg_step, calculating average $mer_ln kmer counts over $mavg_ln bp
#   - Stop when the calculated average is > $max_kmer_count
gene_expand_trimmed_outf="${out_d%/}/$(basename ${gene_expand_outf%.*}).trimmed.bed"
bash ${components_d}/40_trim_flanking_bp-awk.sh ${gene_expand_outf} ${gene_bed_outf} ${fasta_inf} ${kcount_outf_base}.tsv ${max_kmer_count} ${mavg_ln} ${mavg_step} ${mer_ln} > ${gene_expand_trimmed_outf}
echo "Created trimmed expanded genes file $gene_expand_trimmed_outf"  >> $outlog_f


# Generate a report on the trimming results
report_outf="${out_d%/}/$(basename ${gene_expand_trimmed_outf%.*}).summary_report.tsv"
bash ${components_d}/50_create_report.sh ${gene_expand_trimmed_outf} ${gene_expand_outf} ${gene_bed_outf} > ${report_outf}
echo "Created report file $report_outf" >> $outlog_f

echo -e "Run finished: $(date -u)."
