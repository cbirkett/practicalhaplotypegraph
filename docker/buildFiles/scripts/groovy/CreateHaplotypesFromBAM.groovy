#!/usr/bin/env groovy



//Steps
//1. Filter BAMs
//2. Run GATK Haplotype Caller/Sentieon HaplotypeCaller
//3. Run GVCF filtering
//4. Pass on To CreateHaplotypesFromGVCF

//Set up the local variables which represent config file parameters used by most of the methods in this script.
//The defaults here are set up for the docker.  If the user would like to setup something else, they need to change the config.
gvcfFileDir = "/phg/inputDir/loadDB/gvcf/"
tempFileDir = "/phg/inputDir/loadDB/bam/temp/"
filteredOutputBAMDir= "/phg/inputDir/loadDB/bam/mapqFiltered/"
dedupedBAMDir= "/phg/inputDir/loadDB/bam/dedup/"


fullXmx = "-Xmx10G"
numThreads="10"
extendedWindowSize = "1000"
refRangeMethodList = "FocusRegion,FocusComplement"
tasselRunPipeline = "/tassel-5-standalone/run_pipeline.pl"

mapQ = "48"
sentieonLicense = ""
sentieonPath = "/sentieon/bin/sentieon"
gatkPath = "/gatk/gatk"
referenceFile = ""

keyFile = ""
keyFileSampleNameIndex = -1
keyFileFileIndex = -1
keyFileTypeIndex = -1
keyFileChrPhasedIndex = -1
keyFileGenePhasedIndex = -1
keyFilePhasingConfIndex = -1
keyFileSampleDescriptionIndex=-1



def parseArguments(args) {
    def cli = new CliBuilder(usage: 'CreateHaplotypesFromBAM.groovy -[hd] -config [dbConfigFile] -t [taxon]')

    // Create the list of options.
    cli.with {
        h longOpt: 'help', 'Show usage information'
        d longOpt: 'description', 'Show Information About this Pipeline'
        config args:1,'DB Config file. (required)',required:true
    }

    def options = cli.parse(args)
    if (!options) {
        return
    }
    // Show usage text when -h or --help option is used.
    if (options.h) {
        cli.usage()
        return
    }
    if(options.d) {
        println "This Groovy script will attempt to Create Haplotypes starting with BAM files."
        println "The steps of this pipeline are as follows:"
        println "\t1. Filter the BAMs based on MAPQ"
        println "\t2. Run HaplotypeCaller using either GATK or Sentieon to create GVCF files"
        println "\t3. Run GVCF filtering"
        println "\t4. Extract out the Calls for the Focused Reference Ranges and load the VariantContexts to the DB"

        println ""
        println "If you already have Trusted GVCF files, we recommend to use CreateHaplotypeFromGVCF.groovy"
        println ""
        cli.usage()
        return
    }

    //Parse the required parameters into the needed objects
    def dbConfigProperties = new Properties()
    try {
        File propertiesFile = new File(options.config)
        propertiesFile.withInputStream {
            dbConfigProperties.load(it)
        }
        parseConfig(dbConfigProperties)

    }
    catch(Exception e) {
        println "Error parsing config file:"+e.getMessage()
        e.printStackTrace()
        return
    }

    def keyFileEntries = parseKeyFile(keyFile)

    //TODO figure out how to get named parameters to work
    runCreateHaplotypesFromBAM(dbConfigProperties,options.config,keyFileEntries)
}

def parseConfig(Properties dbConfig) {
    gvcfFileDir = dbConfig.getProperty("gvcfFileDir","/phg/inputDir/loadDB/gvcf/")
    tempFileDir = dbConfig.getProperty("tempFileDir","/phg/inputDir/loadDB/bam/temp/")
    filteredOutputBAMDir= dbConfig.getProperty("filteredBamDir","/phg/inputDir/loadDB/bam/mapqFiltered/" )
    dedupedBAMDir= dbConfig.getProperty("dedupedBamDir","/phg/inputDir/loadDB/bam/dedup/" )


    fullXmx = "-Xmx"+dbConfig.getProperty("Xmx","10G")
    extendedWindowSize = dbConfig.getProperty("extendedWindowSize","1000")
    tasselRunPipeline = dbConfig.getProperty("tasselLocation","/tassel-5-standalone/run_pipeline.pl")

    mapQ = dbConfig.getProperty("mapQ","48")

    sentieonLicense = dbConfig.getProperty("sentieon_license")
    sentieonPath = dbConfig.getProperty("sentieonPath", "/sentieon/bin/sentieon")
    gatkPath = dbConfig.getProperty("gatkPath","/gatk/gatk")
    numThreads=dbConfig.getProperty("numThreads","10")
    referenceFile = dbConfig.getProperty("referenceFasta")


    //This looks odd, but will allow CreateIntervalBedFilesPlugin.refRangeMethods overwrite the entries if there is just a refRangeMethods
    refRangeMethodList = dbConfig.getProperty("refRangeMethods","FocusRegion,FocusComplement")
    refRangeMethodList = dbConfig.getProperty("CreateIntervalBedFilesPlugin.refRangeMethods",refRangeMethodList)


    keyFile = dbConfig.getProperty("wgsKeyFile","")


}

def parseKeyFile(String keyfile) {
    if(keyfile == "") {
        throw new IllegalStateException("Unable to find keyfile.  Be sure to set LoadHaplotypesFromGVCFPlugin.keyFile in the config")
    }
    File file = new File(keyfile)
    def line = ""
    def keyFileLines = new ArrayList<String[]>()
    file.withReader { reader ->

        line = reader.readLine()
        if(line == null || line == "") {
            throw new IllegalStateException("CreateHaplotypesFromGVCF Keyfile is empty.")
        }

        def headers = line.split("\t")

        for(int i = 0; i < headers.length; i++) {
            if(headers[i] == 'sample_name' ) {
                keyFileSampleNameIndex = i
            }
            else if(headers[i] == 'files' ) {
                keyFileFileIndex = i
            }
            else if(headers[i] == 'type') {
                keyFileTypeIndex = i
            }
            else if(headers[i] == 'chrPhased') {
                keyFileChrPhasedIndex = i
            }
            else if(headers[i] == 'genePhased') {
                keyFileGenePhasedIndex = i
            }
            else if(headers[i] == 'phasingConf') {
                keyFilePhasingConfIndex = i
            }
            else if(headers[i] == 'sample_description') {
                keyFileSampleDescriptionIndex = i
            }
        }

        //TODO verify that the headers are actually set

        while ((line = reader.readLine()) != null) {
            if(line == "") {
                continue
            }
            def lineSplit = line.split("\t")
            if(lineSplit[keyFileTypeIndex] == 'BAM')
            keyFileLines.add(lineSplit)
        }
    }
    return keyFileLines
}


/**
 * Method to process BAMs through GATK and upload GVCFs
 * @param dbConfig
 * @param dbConfigFile
 * @param keyFileEntries
 * @return
 */
def runCreateHaplotypesFromBAM(Properties dbConfig, String dbConfigFile, ArrayList<String[]> keyFileEntries) {
    parseConfig(dbConfig)
    //Create Directories if need be:
    def gvcfFileDirFile = new File(gvcfFileDir)
    def keyFileForOutput = new File(keyFile)
    if(!gvcfFileDirFile.exists()) {
        if(!gvcfFileDirFile.mkdirs()) {
            throw new IllegalStateException("Error creating GVCF output file.  Make sure you have permission to write to the following dir:"+gvcfFileDir)
        }
    }
    def tempFileDirFile = new File(tempFileDir)
    if(!tempFileDirFile.exists()) {
        if(!tempFileDirFile.mkdirs()) {
            throw new IllegalStateException("Error creating Temp Output Directory.  Make sure you have permission to write to the following dir:"+tempFileDir)
        }
    }

    def filteredOutputDirFile = new File(filteredOutputBAMDir)
    if(!filteredOutputDirFile.exists()) {
        if(!filteredOutputDirFile.mkdirs()) {
            throw new IllegalStateException("Error creating Filtered BAM Output Directory.  Make sure you have permission to write to the following dir:"+filteredOutputDirFile)
        }
    }


    //Pull Down the Bed file
    println "Extract The BED file from the DB for use with HaplotypeCaller and BAM filtering"

    def bedFile = extractBedFileFromDB(dbConfigFile)

    println "Extracted The BED file"

    for(String[] keyFileEntry : keyFileEntries) {
        def fileNames = keyFileEntry[keyFileFileIndex].split(",")
        def taxon = keyFileEntry[keyFileSampleNameIndex]
        def filteredBams = []
        for(String currentFileName : fileNames) {
            filteredBams << filterBAMFile(currentFileName)
        }

        //Run GATK Haplotype Caller or sentieon
        def gvcfFileName = runHaplotypeCaller(filteredBams, taxon)

        //Reheader the VCF to make sure it matches the name of the taxon
        reheaderGVCFFile(gvcfFileName,taxon)

        //Run GVCF Filtering
        def filteredGVCFFileName = runFilterGVCF(dbConfigFile,gvcfFileName,taxon)

        def outputArray = new String[keyFileEntry.length]
        for(int i = 0; i < keyFileEntry.length; i++) {
            if(i == keyFileTypeIndex) {
                outputArray[i] = "GVCF"
            }
            else if(i == keyFileFileIndex) {
                outputArray[i] = filteredGVCFFileName
            }
            else {
                outputArray[i] = keyFileEntry[i]
            }
        }

        //TODO output the keyFile record
        keyFileForOutput.append("\n"+outputArray.join("\t"))

    }

    //Pass the information onto CreateHaplotypesFromGVCF.groovy
    def createHapFromGVCF = new CreateHaplotypesFromGVCF()
    createHapFromGVCF.runCreateHaplotypesGVCF(dbConfig,dbConfigFile, bedFile)
}

/**
 * Method to use to make sure the key file has been parsed.
 * This one is called by CreateHaplotypesFromFastq and requires the key file to be parsed again to pick up the changes.
 * @param dbConfig
 * @param dbConfigFile
 * @return
 */
def runCreateHaplotypesFromBAM(Properties dbConfig, String dbConfigFile) {
    parseConfig(dbConfig)
    def keyFileEntries = parseKeyFile(keyFile)
    runCreateHaplotypesFromBAM(dbConfig,dbConfigFile,keyFileEntries)
}


/**
 * Method to extract out the BED file from the DB. This method will also make a BED file with extended anchors.
 * The non-extended BED file will be used for GATK, the extended one for BAM filtering.
 * @param dbConfigFile
 * @return
 */
def extractBedFileFromDB(String dbConfigFile) {
    def bedFile=tempFileDir+"intervals.bed"
    def extendedBedFile=tempFileDir+"intervals_plusminus1000.bed"
    def extractBedFilesString = "perl "+tasselRunPipeline+" "+fullXmx+" -debug -CreateIntervalBedFilesPlugin -dbConfigFile "+dbConfigFile+" -bedFile "+bedFile+" -extendedBedFile "+extendedBedFile+" -windowSize "+extendedWindowSize+" -refRangeMethods "+refRangeMethodList+" -endPlugin"

    extractBedFilesString.execute().waitForProcessOutput(System.out,System.err)

    return bedFile
}

/**
 * Filter the BAM file based on MapQ scores and then index the filtered file.  Otherwise GATK will not load the BAMs.
 * @param currentBamFile
 * @return
 */

def filterBAMFile(String currentBamFile) {
    def extendedBedFile=tempFileDir+"intervals_plusminus1000.bed"

    def filteredBAMFile = filteredOutputBAMDir + currentBamFile.substring(0,currentBamFile.lastIndexOf("."))+"_mapQ_"+mapQ+".bam"
    def samtoolsFilterCommand = "samtools view -b -o"+filteredBAMFile+" -L "+extendedBedFile+" -q "+mapQ+" "+dedupedBAMDir+currentBamFile

    samtoolsFilterCommand.execute().waitForProcessOutput(System.out,System.err)

    def samtoolsIndexCommand = "samtools index " + filteredBAMFile
    samtoolsIndexCommand.execute().waitForProcessOutput(System.out,System.err)

    return filteredBAMFile
}

/**
 * Method to run Haplotype Caller.  If Sentieon license is set in the config file, we can use sentieon to run HC.
 * If the License is not provided the code will run standard GATK
 * @param filteredBams
 * @param taxonName
 * @return
 */
def runHaplotypeCaller(List<String> filteredBams, String taxonName) {
    def bedFile=tempFileDir+"intervals.bed"
    def outputGVCFFile=gvcfFileDir+taxonName+"_haplotype_caller_output.g.vcf"

    if(sentieonLicense) {
        println "Using Sentieon to create GVCFs"

        envVars=["SENTIEON_LICENSE="+sentieonLicense]

        def inputList = ""
        for(bamFile in filteredBams) {
            inputList+=" -i "+bamFile
        }

        def runSentieonString = sentieonPath+" driver -t "+numThreads+" -r "+referenceFile+""+inputList+" --interval "+bedFile+" --algo Haplotyper --ploidy 1 --emit_mode gvcf "+outputGVCFFile
        runSentieonString.execute(envVars,null).waitForProcessOutput(System.out,System.err)


    }
    else {
        println "Using GATK to create GVCFs"

        def inputList = ""
        for(bamFile in filteredBams) {
            println bamFile
            inputList+=" -I "+bamFile
        }

        println inputList

        def runGATKString = gatkPath+" --java-options "+fullXmx+" HaplotypeCaller -R "+referenceFile+" "+inputList+" --ERC GVCF -L "+bedFile+" --ploidy 1 -O "+outputGVCFFile
        println runGATKString
        runGATKString.execute().waitForProcessOutput(System.out, System.err)
    }

    if(!new File(outputGVCFFile).exists()) {
        throw new IllegalStateException("Error running CreateHaplotypesFromBAM.groovy:  Running Haplotype Caller failed.")
    }

    //TODO error check to make sure the file was created and verify
    return outputGVCFFile
}

/**
 * Filter the GVCF which is output from the run of HaplotypeCaller.
 * This method makes use of FilterGVCFSingleFilePlugin which will apply the filtering to a single gvcf file instead of a directory.
 * @param dbConfigFile
 * @param gvcfName
 * @param taxonName
 * @return
 */
def runFilterGVCF(String dbConfigFile, String gvcfName, String taxonName) {
    def outputFilteredGVCFName = gvcfFileDir+taxonName+"_haplotype_caller_output_filtered.g.vcf"

    def filterGVCFCommand = "perl "+tasselRunPipeline+" "+fullXmx+" -debug -FilterGVCFSingleFilePlugin -configFile "+dbConfigFile+" -inputGVCFFile "+ gvcfName +" -outputGVCFFile "+ outputFilteredGVCFName +" -endPlugin"

    filterGVCFCommand.execute().waitForProcessOutput(System.out,System.err)

    if(!new File(outputFilteredGVCFName+".gz").exists()) {
        throw new IllegalStateException("Error running CreateHaplotypesFromBAM.groovy:  FilterGVCFSingleFilePlugin failed.")
    }

    return taxonName+"_haplotype_caller_output_filtered.g.vcf.gz"
}

/**
 * We need to reheader the vcf file in case the user mislabeled the ReadGroups when aligning.
 * This forces the GVCF taxon name to match what we will be storing in the db
 * @param dbConfig
 * @param gvcfName
 * @param taxonName
 * @return
 */
def reheaderGVCFFile(String gvcfName, String taxonName) {
    //Write the taxon name to a temp file then use it to reheader the vcf
    File reheaderFile = new File(tempFileDir+"reheaderFile.txt")
    reheaderFile<<taxonName

    def bcftoolsReheaderCommand = "bcftools reheader -s "+tempFileDir+"reheaderFile.txt -o "+gvcfName+"_reheader "+gvcfName
    bcftoolsReheaderCommand.execute().waitForProcessOutput(System.out,System.err)

    def moveReheaderFile = "mv "+gvcfName+"_reheader "+ gvcfName
    moveReheaderFile.execute().waitForProcessOutput(System.out,System.err)

    def file = new File(tempFileDir+"reheaderFile.txt")
    file.delete()

    if(!new File(gvcfName).exists()) {
        throw new IllegalStateException("Error running CreateHaplotypesFromBAM.groovy:  Reheader failed.")
    }
}

parseArguments(args)
