#!/usr/bin/env groovy


/**
 * Groovy Script to Run The CreateHaplotypes Script.
 * Originally this logic was in CreateHaplotypes.sh, Groovy will make the processing more consistent
 */

//Set up global variables
referenceFile = ""
numThreads = 10
picardPath = "/picard.jar"
fastqDir = "/phg/inputDir/loadDB/fastq/"
tempFileDir = "/phg/inputDir/loadDB/temp/"
outputFileDir = "/phg/inputDir/loadDB/bam/dedup/"


keyFile = ""
keyFileSampleNameIndex = -1
keyFileFileIndex = -1
keyFileTypeIndex = -1
keyFileChrPhasedIndex = -1
keyFileGenePhasedIndex = -1
keyFilePhasingConfIndex = -1
keyFileSampleDescriptionIndex=-1
keyFileLibraryIDIndex=-1


//http://mrhaki.blogspot.com/2009/09/groovy-goodness-parsing-commandline.html
/**
 * Parse the arguments coming in from the command line
 * @param args
 * @return
 */
def parseArguments(args) {
    def cli = new CliBuilder(usage: 'CreateHaplotypesFromFastq.groovy -[hd] -config [dbConfigFile] -t [taxon] -op [operationMode] -fastqs [fastqString]')

    // Create the list of options.
    cli.with {
        h longOpt: 'help', 'Show usage information'
        d longOpt: 'description', 'Show Information About this Pipeline'
        config args:1,'DB Config file. (required)',required:true
    }

    def options = cli.parse(args)
    if (!options) {
        return
    }
    // Show usage text when -h or --help option is used.
    if (options.h) {
        cli.usage()
        return
    }
    if(options.d) {
        println "This Groovy script will attempt to Create Haplotypes starting with Fastq files."
        println "The steps of this pipeline are as follows:"
        println "\t1. Check to see if the reference is BWA and fai indexed"
        println "\t\ta. If not, index using bwa index"
        println "\t2. Align the WGS fastqs to the reference using bwa mem"
        println "\t\ta. If operationMode is paired, bwa mem is run using pairs of fastqs:"
        println "\t\tb. If operationMode is single, bwa mem is run one fastq at a time:"
        println "\t3. Run sort and Picard MarkDuplicates on the resulting BAM alignments"
        println "\t4. Filter the BAMs based on MAPQ"
        println "\t4. Run HaplotypeCaller using either GATK or Sentieon to create GVCF files"
        println "\t5. Run GVCF filtering"
        println "\t6. Extract out the Calls for the Focused Reference Ranges and load the VariantContexts to the DB"

        println ""
        println "If you already have done alignment we recommend to use CreateHaplotypesFromBAM.groovy"
        println "If you already have Trusted GVCF files, we recommend to use CreateHaplotypeFromGVCF.groovy"
        println ""
        cli.usage()
        return
    }

    //Parse the required parameters into the needed objects
    def dbConfigProperties = new Properties()
    try {
        File propertiesFile = new File(options.config)
        propertiesFile.withInputStream {
            dbConfigProperties.load(it)
        }
    }
    catch(Exception e) {
        println "Error parsing config file:"+e.getMessage()
        e.printStackTrace()
        return
    }

    runCreateHaplotypesFromFastq(dbConfigProperties, options.config)
}

/**
 * Parse the config file for the needed configuration parameters
 * @param dbConfig
 * @return
 */
def parseConfig(Properties dbConfig) {
    referenceFile = dbConfig.getProperty("referenceFasta")
    numThreads = dbConfig.getProperty("numThreads","10")
    picardPath = dbConfig.getProperty("picardPath","/picard.jar")

    fastqDir = dbConfig.getProperty("fastqFileDir","/phg/inputDir/loadDB/fastq/")
    tempFileDir = dbConfig.getProperty("tempFileDir","/phg/inputDir/loadDB/temp/")
    outputFileDir= dbConfig.getProperty("dedupedBamDir","/phg/inputDir/loadDB/bam/dedup/")



    keyFile = dbConfig.getProperty("wgsKeyFile","")
}



/**
 * Method to start the pipeline to run.  This will do the following in order:
 * Index the ref fasta
 * Align fastqs using bwa mem
 * Call the next script in the pipeline.
 *
 *
 * @param dbConfig
 * @param dbConfigFile
 * @param taxonName
 * @param isPaired
 * @param fastqs
 * @return
 */
def runCreateHaplotypesFromFastq(Properties dbConfig,String dbConfigFile) {
    parseConfig(dbConfig)
    try {
        referenceFileObj = new File(referenceFile)
    }
    catch( Exception e) {
        //TODO fix logging
        println "Error config file does not contain a referenceFasta parameter. Exiting now."
        return
    }

    try {
        assert referenceFileObj.exists()
    }
    catch(AssertionError assertionError) {
        println "Error finding reference file.  File Path in config file does not exist.  If running on Docker check your mount points."
        return
    }

    def isIndexed = isRefIndexed(referenceFile)

    if(!isIndexed) {
        //Index the reference
        println "Reference is not indexed.  Indexing now."
        runReferenceIndexing(referenceFile, numThreads, picardPath)
    }


    def keyFileEntries = parseKeyFile(keyFile)
    println "Reference is indexed moving onto alignment"

    alignUsingBWA(dbConfig,dbConfigFile, keyFileEntries)
}

def parseKeyFile(String keyfile) {
    if(keyfile == "") {
        throw new IllegalStateException("Unable to find keyfile.  Be sure to set LoadHaplotypesFromGVCFPlugin.keyFile in the config")
    }
    File file = new File(keyfile)
    def line = ""
    def keyFileLines = new ArrayList<String[]>()
    file.withReader { reader ->

        line = reader.readLine()
        if(line == null || line == "") {
            throw new IllegalStateException("CreateHaplotypesFromGVCF Keyfile is empty.")
        }

        def headers = line.split("\t")

        for(int i = 0; i < headers.length; i++) {
            if(headers[i] == 'sample_name' ) {
                keyFileSampleNameIndex = i
            }
            else if(headers[i] == 'files' ) {
                keyFileFileIndex = i
            }
            else if(headers[i] == 'type') {
                keyFileTypeIndex = i
            }
            else if(headers[i] == 'chrPhased') {
                keyFileChrPhasedIndex = i
            }
            else if(headers[i] == 'genePhased') {
                keyFileGenePhasedIndex = i
            }
            else if(headers[i] == 'phasingConf') {
                keyFilePhasingConfIndex = i
            }
            else if(headers[i] == 'sample_description') {
                keyFileSampleDescriptionIndex = i
            }
            else if(headers[i] == 'library_ID') {
                keyFileLibraryIDIndex = i
            }
        }

        //TODO verify that the headers are actually set

        while ((line = reader.readLine()) != null) {
            if(line == "") {
                continue
            }
            def lineSplit = line.split("\t")
            if(lineSplit[keyFileTypeIndex] == 'FASTQ')
                keyFileLines.add(lineSplit)
        }
    }
    return keyFileLines
}

/**
 * Here we verify that we have all the bwa index files in the same directory
 * @param referenceFastaFileName
 */
def isRefIndexed(String referenceFastaFileName) {
    try {
        assert new File(referenceFastaFileName + ".amb").exists()
        assert new File(referenceFastaFileName + ".ann").exists()
        assert new File(referenceFastaFileName + ".bwt").exists()
        assert new File(referenceFastaFileName + ".fai").exists()
        assert new File(referenceFastaFileName + ".pac").exists()
        assert new File(referenceFastaFileName + ".sa").exists()
        return true
    }
    catch(AssertionError assertionError) {
        return false
    }
}

/**
 * Method to run the reference index files.
 * @param referenceFastaFileName
 * @param numThreads
 * @param picardPath
 * @return
 */
def runReferenceIndexing(String referenceFastaFileName, String numThreads, String picardPath) {

    def bwaString = "bwa index "+referenceFastaFileName
    println "Running command: "+bwaString
    bwaString.execute().waitForProcessOutput(System.out, System.err)
    def faidxString = "samtools faidx " + referenceFastaFileName
    println "Running command: "+faidxString
    faidxString.execute().waitForProcessOutput(System.out, System.err)
    def picardString = "java -XX:ParallelGCThreads="+numThreads+" -jar "+ picardPath+" CreateSequenceDictionary R= "+referenceFastaFileName
    println "Running command: "+picardString
    picardString.execute().waitForProcessOutput(System.out, System.err)
}

/**
 * Method to align the fastqs to the reference using BWA
 * @param dbConfig
 * @param dbConfigFile
 * @param keyFileEntries
 * @return
 */

def alignUsingBWA(Properties dbConfig, String dbConfigFile, ArrayList<String[]> keyFileEntries) {

    def taxonToBAMMap = new HashMap<String,List<String>>()
    def keyFileForOutput = new File(keyFile)

    def numberOfColsInKeyFile = keyFileEntries.get(0).length
    for(String[] keyFileEntry : keyFileEntries) {
        def fileNames = keyFileEntry[keyFileFileIndex].split(",").toList()
        def taxonName = keyFileEntry[keyFileSampleNameIndex]

        def libraryId = keyFileEntry[keyFileLibraryIDIndex]

        println("fastqDir:"+fastqDir)
        println("fileName:"+fileNames.join(","))


        def appendClosure = {fileName -> ""+fastqDir+fileName}
        def fastqInputStringList = fileNames.stream().map(appendClosure).collect().join(" ")

        println fastqInputStringList

        def bamFileOutputName = runBWAOnOneFastqBatch(fastqInputStringList as String, taxonName, libraryId)

        if(!taxonToBAMMap.containsKey(taxonName)) {
            taxonToBAMMap.put(taxonName,new ArrayList<String>())
        }

        taxonToBAMMap.get(taxonName).add(bamFileOutputName)


    }

    for(String taxon : taxonToBAMMap.keySet()) {
        def outputKeyFileEntry = new String[numberOfColsInKeyFile]
        outputKeyFileEntry[keyFileSampleNameIndex] = taxon
        //Check to see as this is technically optional.
        if(keyFileSampleDescriptionIndex != -1) {
            outputKeyFileEntry[keyFileSampleDescriptionIndex] = taxon + " Aligned using BWA"
        }
        outputKeyFileEntry[keyFileFileIndex] = taxonToBAMMap.get(taxon).join(",")
        outputKeyFileEntry[keyFileTypeIndex] = "BAM"
        outputKeyFileEntry[keyFileChrPhasedIndex] = "true"
        outputKeyFileEntry[keyFileGenePhasedIndex] = "true"
        outputKeyFileEntry[keyFilePhasingConfIndex] = ".99"

        keyFileForOutput.append("\n"+outputKeyFileEntry.join("\t"))
    }

    //TODO do some error checking to see if we have all the BAMs we think we just made

    def createHapFromBam = new CreateHaplotypesFromBAM()
    createHapFromBam.runCreateHaplotypesFromBAM(dbConfig, dbConfigFile)

}

/**
 * Method to run one batch of bwa on a set of fastqs.
 * @param fastqString
 * @param taxonName
 * @param idCounter
 */
def runBWAOnOneFastqBatch(String fastqString, String taxonName, String currentId) {

    new File(tempFileDir).mkdirs()
    new File(outputFileDir).mkdirs()


    println "Running BWA mem, then sorting and converting to BAM file using samtools."

    String bwaMemString = "bwa mem -M -t "+numThreads+" -R @RG\\tID:"+currentId+"\\tSM:"+taxonName+"\\tPL:ILLUMINA "+referenceFile+" "+fastqString//+" > "+tempFileDir+""+taxonName+"_"+currentId+".sam"

    println bwaMemString

    String samToSortedBamString = "samtools sort -@"+numThreads+" -o "+tempFileDir+""+taxonName+"_"+currentId+"_srt.bam -"


    def bwaToBamProcess = bwaMemString.execute() | samToSortedBamString.execute()

    bwaToBamProcess.waitForProcessOutput(System.out,System.err)

    try {
        assert new File(tempFileDir+""+taxonName+"_"+currentId+"_srt.bam").exists()
    }
    catch(AssertionError assertionError) {
        throw new IllegalStateException("Error running bwa. Unable to make "+tempFileDir+""+taxonName+"_"+currentId+"_srt.bam.  File does not exist.")
    }


    println "Using picard to mark duplicates"
    String picardMarkDupsString = "java -XX:ParallelGCThreads="+numThreads+" -jar "+picardPath+
            " MarkDuplicates INPUT="+tempFileDir+""+taxonName+"_"+currentId+"_srt.bam"+
            " OUTPUT="+outputFileDir+""+taxonName+"_"+currentId+"_srt_dedup.bam"+" METRICS_FILE="+tempFileDir+"metrix."+taxonName+"_"+currentId+"_dedup READ_NAME_REGEX=null TMP_DIR=. COMPRESSION_LEVEL=1"

    picardMarkDupsString.execute().waitForProcessOutput(System.out,System.err)
    try {
        assert new File(outputFileDir+""+taxonName+"_"+currentId+"_srt_dedup.bam").exists()
    }
    catch(AssertionError assertionError) {
        throw new IllegalStateException("Error running picard MarkDuplicates. Unable to make "+outputFileDir+""+taxonName+"_"+currentId+"_srt_dedup.bam.  File does not exist.")
    }


    println "Indexing Deduped BAM file"

    String samtoolsIndexString = "samtools index "+outputFileDir+""+taxonName+"_"+currentId+"_srt_dedup.bam"

    samtoolsIndexString.execute().waitForProcessOutput(System.out, System.err)

    try {
        assert new File(outputFileDir+""+taxonName+"_"+currentId+"_srt_dedup.bam.bai").exists()
    }
    catch(AssertionError assertionError) {
        throw new IllegalStateException("Error running samtools index. Unable to make index for "+outputFileDir+""+taxonName+"_"+currentId+"_srt_dedup.bam.")
    }

    return""+taxonName+"_"+currentId+"_srt_dedup.bam"
}

parseArguments(args)