#!/usr/bin/env groovy


//Steps
//1. Upload GVCFs to DB
/**
 * Set up the Local variables.
 */
tempFileDir = "/phg/inputDir/loadDB/bam/temp/"
tasselRunPipeline = "/tassel-5-standalone/run_pipeline.pl"
fullXmx = "-Xmx10G"
extendedWindowSize = "1000"
refRangeMethodList = "FocusRegion,FocusComplement"

/**
 * Parse the command line arguments.
 * @param args
 * @return
 */
def parseArguments(args) {
    def cli = new CliBuilder(usage: 'CreateHaplotypesFromGVCF.groovy -[hd] -config [dbConfigFile] -t [taxon] -i [inputGVCFFile]')

    // Create the list of options.
    cli.with {
        h longOpt: 'help', 'Show usage information'
        d longOpt: 'description', 'Show Information About this Pipeline'
        config args:1,'DB Config file. (required)',required:true
    }

    def options = cli.parse(args)
    if (!options) {
        return
    }
    // Show usage text when -h or --help option is used.
    if (options.h) {
        cli.usage()
        return
    }
    if(options.d) {
        println "This Groovy script will attempt to Create Haplotypes starting with GVCF files."
        println "The steps of this pipeline are as follows:"
        println "\t1. Run LoadHaplotypesFromGVCFPlugin"

        println ""
        cli.usage()
        return
    }

    //Parse the required parameters into the needed objects
    def dbConfigProperties = new Properties()
    try {
        File propertiesFile = new File(options.config)
        propertiesFile.withInputStream {
            dbConfigProperties.load(it)
        }
    }
    catch(Exception e) {
        println "Error parsing config file:"+e.getMessage()
        e.printStackTrace()
        return
    }

    println dbConfigProperties.toString()
    parseConfig(dbConfigProperties)
    //TODO figure out how to get named parameters to work
    runCreateHaplotypesGVCF(dbConfigProperties, options.config)
}

/**
 * Parse out the needed arguments from the dbConfig File
 * @param dbConfig
 * @return
 */
def parseConfig(Properties dbConfig) {
    tempFileDir = dbConfig.getProperty("tempFileDir","/phg/inputDir/loadDB/bam/temp/")


    tasselRunPipeline = dbConfig.getProperty("tasselLocation","/tassel-5-standalone/run_pipeline.pl")
    fullXmx = "-Xmx"+dbConfig.getProperty("Xmx","10G")

    extendedWindowSize = dbConfig.getProperty("extendedWindowSize","1000")

    //This looks odd, but will allow CreateIntervalBedFilesPlugin.refRangeMethods overwrite the entries if there is just a refRangeMethods
    refRangeMethodList = dbConfig.getProperty("refRangeMethods","FocusRegion,FocusComplement")
    refRangeMethodList = dbConfig.getProperty("CreateIntervalBedFilesPlugin.refRangeMethods",refRangeMethodList)

}

/**
 * Load in the GVCF file, but first pull down the bed file if this script is the one which is called.
 * @param dbConfig
 * @param dbConfigFile
 * @param taxonName
 * @param gvcfFile
 * @return
 */
def runCreateHaplotypesGVCF(Properties dbConfig, String dbConfigFile) {
    parseConfig(dbConfig)
    def tempFileDirFile = new File(tempFileDir)
    if(!tempFileDirFile.exists()) {
        if(!tempFileDirFile.mkdirs()) {
            throw new IllegalStateException("Error creating Temp Output Directory.  Make sure you have permission to write to the following dir:"+tempFileDir)
        }
    }
    def bedFileName = extractBedFileFromDB(dbConfigFile)
    runCreateHaplotypesGVCF(dbConfig, dbConfigFile, bedFileName)
}

/**
 * Load the GVCF file to the database using LoadHapSequencesFromGVCFPlugin
 * @param dbConfig
 * @param dbConfigFile
 * @param taxonName
 * @param gvcfFile
 * @param bedFile
 * @return
 */
def runCreateHaplotypesGVCF(Properties dbConfig, String dbConfigFile, String bedFile) {
    //Create the temp file directories
    parseConfig(dbConfig)
    def uploadGVCFFilesString = "perl "+tasselRunPipeline+" "+fullXmx+" -debug -configParameters "+dbConfigFile+" -LoadHaplotypesFromGVCFPlugin -bedFile "+ bedFile + " -endPlugin"

    println uploadGVCFFilesString
    uploadGVCFFilesString.execute().waitForProcessOutput(System.out,System.err)

}

/**
 * Pull out the bed file.  If running from CreateHaplotypesFromBAM.groovy this step is skipped.
 * @param dbConfigFile
 * @return
 */
def extractBedFileFromDB(String dbConfigFile) {
    def bedFile=tempFileDir+"intervals.bed"
    def extendedBedFile=tempFileDir+"intervals_plusminus1000.bed"
    def extractBedFilesString = "perl "+tasselRunPipeline+" "+fullXmx+" -debug -CreateIntervalBedFilesPlugin -dbConfigFile "+dbConfigFile+" -bedFile "+bedFile+" -extendedBedFile "+extendedBedFile+" -windowSize "+extendedWindowSize+" -refRangeMethods "+refRangeMethodList+" -endPlugin"

    extractBedFilesString.execute().waitForProcessOutput(System.out,System.err)
    return bedFile
}

parseArguments(args)