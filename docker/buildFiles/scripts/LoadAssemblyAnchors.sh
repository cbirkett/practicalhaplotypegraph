#!/bin/bash -x
# Get db connection, run AssemblyHaplotypesPlugin which creates and loads assembly sequence to the DB

#Input Folders
# Mount localMachine:/pathToAnswerDir/ to docker://tempFileDir/data/answer/
# Mount localMachine:/pathToDataDir/ to docker://tempFileDir/data/
# Mount localMachine:/pathToAlignDir to docker://tempFileDir/outputDir/align/

MOUNT_DIR=/phg
INPUT_DIR=${MOUNT_DIR}/inputDir
ASSEMBLY_DIR=${INPUT_DIR}/assemblies
OUTPUT_DIR=${MOUNT_DIR}/outputDir
REFERENCE_DIR=${INPUT_DIR}/reference


# ALIGN_PATH needs the trailing "/"
ALIGN_PATH=${OUTPUT_DIR}/align/

GVCF_PATH=${ALIGN_PATH}/gvcfs/
mkdir -p $GVCF_PATH


dbConfigFile=$1
refFasta=$2
assemblyFasta=$3
name=$4
chrom=$5
clusterSize=$6

memoryString="`grep -m 1 '^Xmx=' ${MOUNT_DIR}/${dbConfigFile} | cut -d'=' -f2`"
if [ ${memoryString} != "" ]
then
    fullXmx='-Xmx'${memoryString}
else
	fullXmx='-Xmx10G'
fi

entryPoint="`grep -m 1 '^assemblyEntryPoint' ${MOUNT_DIR}/${dbConfigFile} | cut -d'=' -f2`"
if [ ${entryPoint} != "" ]
then
    entry=${entryPoint}
else
	entry='all'
fi

if [ ${clusterSize} != "" ]
then
   nucmerCluster=${clusterSize}
else
   nucmerCluster='250'	
fi

minInversion="`grep -m 1 '^minInversionLen' ${MOUNT_DIR}/${dbConfigFile} | cut -d'=' -f2`"
if [ ${minInversion} != "" ]
then
    inversionLen=${minInversion}
else
	inversionLen='7500'
fi

/tassel-5-standalone/run_pipeline.pl ${fullXmx} -debug -configParameters $MOUNT_DIR/$dbConfigFile -AssemblyHaplotypesPlugin -dbConfigFile $MOUNT_DIR/$dbConfigFile -ref $REFERENCE_DIR/$refFasta -assembly $ASSEMBLY_DIR/$assemblyFasta -assemblyName $name -chrom $chrom  -clusterSize ${nucmerCluster}  -minInversionLen ${inversionLen} -outputDir $ALIGN_PATH -gvcfOutputDir ${GVCF_PATH} -entryPoint ${entry} -endPlugin
