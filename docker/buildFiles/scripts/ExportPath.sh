#!/bin/bash -x

#
# This script creates a HaplotypeGraph from the given database
# and consensus method, that contains only anchors
# and the variant contexts (needed to export to VCF).
#
# Then previously calculated paths through the graph
# are imported and associated with the HaplotypeGraph
#
# Those paths (one per taxa) are lastly exported to
# the specified VCF file.
#

MOUNT_DIR=/phg
OUTPUT_DIR=${MOUNT_DIR}/outputDir
DATA_DIR=${MOUNT_DIR}/data

# Properties filename of database configuration
DATABASE_CONFIG=${MOUNT_DIR}/$1
# Consensus Method used to create haplotypes in graph
CONSENSUS_METHOD=$2
# Output File
OUTPUT_FILE=${OUTPUT_DIR}/$3

PATH_METHOD_NAME=$4

fullXmx=""
memoryString="`grep -m 1 '^Xmx=' ${DATABASE_CONFIG} | cut -d'=' -f2`"
if [ ${memoryString} != "" ]
then
    fullXmx='-Xmx'${memoryString}
fi

#
# Creates HaplotypeGraph and uses as input to
# ImportHaplotypePathFilePlugin and then writes VCF output
#
# Create HaplotypeGraph (from database, only anchors, for
# consensus method) -> Import Path Files (one per taxon) ->
# Export to VCF format
#

time /tassel-5-standalone/run_pipeline.pl $fullXmx -debug -configParameters ${DATABASE_CONFIG} -HaplotypeGraphBuilderPlugin -configFile $DATABASE_CONFIG -methods $CONSENSUS_METHOD -includeVariantContexts true -endPlugin -ImportHaplotypePathFilePlugin -pathMethodName ${PATH_METHOD_NAME} -endPlugin -PathsToVCFPlugin -outputFile $OUTPUT_FILE -endPlugin

