#!/bin/bash -x
# Get db connection, run LoadGenomeIntervalsToPHGdbPlugin

#Input Folders - the output path is refereced in the config file, db will created created there
# Mount localMachine:/pathToBaseDockerDir to docker://phg/

MOUNT_DIR=/phg
INPUT_DIR=${MOUNT_DIR}/inputDir
REFERENCE_DIR=${INPUT_DIR}/reference

dbConfigFile=$1
dbRef=$2
dbAnchors=$3
genomeDataFile=$4
create_new=$5
# parameter refServerPath is just text that gets stored telling where the ref fasta file lives
# in the real world, e.g irods:/ibl/assemblies or some other server and directory location
refServerPath=$6

memoryString="`grep -m 1 '^Xmx=' ${MOUNT_DIR}/${dbConfigFile} | cut -d'=' -f2`"
if [ ${memoryString} != "" ]
then
    fullXmx='-Xmx'${memoryString}
else
	fullXmx='-Xmx10G'
fi

/tassel-5-standalone/run_pipeline.pl ${fullXmx} -debug -GetDBConnectionPlugin -config ${MOUNT_DIR}/$dbConfigFile -create $create_new -endPlugin \
                                                        -LoadAllIntervalsToPHGdbPlugin -ref $REFERENCE_DIR/$dbRef \
                                                        -anchors $MOUNT_DIR/$dbAnchors -genomeData $REFERENCE_DIR/$genomeDataFile  \
                                                        -refServerPath $refServerPath \
                                                        -outputDir $MOUNT_DIR -endPlugin

# LoadGenomeIntervalsToPHGPlugin will create a new database.
# Run liquibase changeLogSync to create the liquibase tables amd mark all changes as "ran".
cd /liquibase
lbCommand='changeLogSync'

/tassel-5-standalone/run_pipeline.pl ${fullXmx} -debug -configParameters ${MOUNT_DIR}/$dbConfigFile -LiquibaseUpdatePlugin  -outputDir $MOUNT_DIR -command ${lbCommand} -endPlugin
