#!/bin/bash -x

#Run Fastq/BAM TO Filtered GVCF pipeline
# CreateHaplotypePipeline

#Input Folders
# Mount localMachine:/pathToInputs/refFolder to docker://tempFileDir/data/reference/
# Mount localMachine:/pathToInputs/fastq to docker://tempFileDir/data/fastq/
# Mount localMachine:/pathToInputs/bam to docker://tempFileDir/data/bam/externalBams/ optional to skip alignment
# Mount localMachine:/pathToInputs/gvcf/gvcfFile.g.vcf to docker://tempFileDir/data/gvcfs/gvcfFile.g.vcf optional to skip haplotype caller.

#Steps
# If the reference fasta is not indexed, the script will create both bwa and fai indices
# If fastqs are defined, run bwa( with deduping) then filter based on mapq(48 is default minimum)
# If Bam files are included, just run mapq filtering
# Then run HaplotypeCaller
#    If a Sentieon license is included in the config file, the script will use sentieon
# Then the script will Filter the gvcfs based on the filtering criteria specified in the config file using FilterGVCFPlugin
# We then upload the final GVCF to the db using LoadHapSequencesToDBPlugin

REFERENCE_PATH=/tempFileDir/data/reference
FASTQ_PATH=/tempFileDir/data/fastq
BAM_PATH=/tempFileDir/data/bam
PICARD_PATH=/
INTERVAL_FILE=/tempFileDir/data/intervals.bed
#TODO clean this up, no need for 2 interval files
FILTER_BAM_INTERVALFILE=/tempFileDir/data/intervals_plusminus1000.bed
GVCF_PATH=/tempFileDir/data/gvcfs/
dbConfigFile=$1
currentTaxon=$2
operationMode=$3
methodName=$4
#Optional fastq string.  If this is not provided, you must use BAM files.
#This string needs each file split by commas set up dependent on the operationMode
fastqString=$5

memoryString="`grep -m 1 '^Xmx=' ${dbConfigFile} | cut -d'=' -f2`"
if [ ${memoryString} != "" ]
then
    fullXmx='-Xmx'${memoryString}
else
	fullXmx='-Xmx2G'
fi

mapQ="`grep -m 1 '^mapQ=' ${dbConfigFile} | cut -d'=' -f2`"

if [ -z "$mapQ" ];
then
    mapQ=48
fi


extendedWindowSize="`grep -m 1 '^extendedWindowSize=' ${dbConfigFile} | cut -d'=' -f2`"
if [ -z "$extendedWindowSize" ];
then
    extendedWindowSize=1000
fi

numThreads="`grep -m 1 '^numThreads=' ${dbConfigFile} | cut -d'=' -f2`"
if [ -z "$numThreads" ];
then
    numThreads=10
fi

refRangeMethodList="`grep -m 1 '^refRangeMethods=' ${dbConfigFile} | cut -d'=' -f2`"
if [ -z "$refRangeMethodList" ];
then
    refRangeMethodList='refRegionGroup'
fi

IFS=',' read -r -a fastqArray <<< "$fastqString"

#Steps
#0 Index the reference fasta
#0.5 Create the intervalFiles from the database
#1. Align Fastq to bwa mem
#2. Filter BAMs
#3. GATK Haplotype Caller/Sentieon HaplotypeCaller
#4. GVCF Filter
#5. Upload GVCF to DB

#make the directories that the rest of the script uses
mkdir -p $BAM_PATH
mkdir -p $GVCF_PATH

#Auxilary Functions
isIndexed() {

	count="`ls -1 $1.amb 2>/dev/null | wc -l`"
	if [ $count == 0 ]
	then
		return false
	fi
	count="`ls -1 $1.ann 2>/dev/null | wc -l`"
	if [ $count == 0 ]
	then
		return false
	fi
	count="`ls -1 $1.bwt 2>/dev/null | wc -l`"
	if [ $count == 0 ]
	then
		return false
	fi
	count="`ls -1 $1.pac 2>/dev/null | wc -l`"
	if [ $count == 0 ]
	then
		return false
	fi
	count="`ls -1 $1.sa 2>/dev/null | wc -l`"
	if [ $count == 0 ]
	then
		return false
	fi
	count="`ls -1 $1.fai 2>/dev/null | wc -l`"
	if [ $count == 0 ]
	then
		return false
	fi
	true
}

#Step 0. Index the reference fasta
cd /tempFileDir/data/reference/
#First check to see if we have a valid reference file
referenceNames="`ls | egrep '\.fa.gz$|\.fasta.gz$|\.fna.gz$|\.fa$|\.fasta$|\.fna$'`"

referenceFile=$REFERENCE_PATH/${referenceNames[0]}

for referenceName in $referenceNames
do
#Then check to see if we have the necessary bwa index files need a .amb, .ann, .bwt, .pac, .sa file
echo "ReferenceName:"$referenceName
if isIndexed $referenceName = true
then
	echo $referenceName" is already indexed, Moving on to alignment."
else
	echo $referenceName" is not indexed, running bwa index and samtools faidx"
	bwa index $referenceName
	samtools faidx $referenceName
	java -XX:ParallelGCThreads=${numThreads} -jar $PICARD_PATH/picard.jar CreateSequenceDictionary R= $referenceName
fi

done
echo "Indexing check done, moving on to alignment"

#Pull down the Bed Files using CreateIntervalBedFilesPlugin
perl /tassel-5-standalone/run_pipeline.pl ${fullXmx} -debug -CreateIntervalBedFilesPlugin -dbConfigFile $dbConfigFile -bedFile ${INTERVAL_FILE} -extendedBedFile ${FILTER_BAM_INTERVALFILE}  -windowSize ${extendedWindowSize} -refRangeMethods ${refRangeMethodList} -endPlugin


#Step 1.  Align any fastq files in the inputDir/fastq/ to the reference sequence in inputDir/reference/
#TODO work on using a ReadGroupFile, no real way to infer this as the reads do not have standardized file names
#We really just need the ID assigned correctly as well as the SM tag so we can run Haplotype Caller
cd $FASTQ_PATH
len=${#fastqArray[*]}         # get total elements in an array

#Setup the temp directories
mkdir -p $BAM_PATH/$currentTaxon/WithDups
mkdir -p $BAM_PATH/$currentTaxon/DedupBAMs
mkdir -p $BAM_PATH/$currentTaxon/DedupFiles

#check if we need to run in paired end mode or not.
# If we use paired end, we need to increment the loop by 2 as we need 2 fastqs
if $operationMode == "paired"
then
	echo "Operating in Paired End Mode, take pairs of samples in order and align."
	# print it
	for (( i=0; i<${len}; i+=2 ));
	do
		echo "${fastqArray}"
		pairDenom=2
		pairIndex=$((i/pairDenom))
		currentId="phg_id_"$pairIndex
		bwa mem -M -t ${numThreads} -R '@RG\tID:'${currentId}'\tSM:'${currentTaxon}'\tPL:ILLUMINA' ${referenceFile} ${fastqArray[i]} ${fastqArray[i+1]} > $BAM_PATH/${currentTaxon}/WithDups/${currentTaxon}_${currentId}.sam
		samtools view -bS $BAM_PATH/${currentTaxon}/WithDups/${currentTaxon}_${currentId}.sam > $BAM_PATH/${currentTaxon}/WithDups/${currentTaxon}_${currentId}.bam
		samtools sort -l 1 -m 4G -@ 4 -o $BAM_PATH/${currentTaxon}/WithDups/${currentTaxon}_${currentId}_srt.bam $BAM_PATH/${currentTaxon}/WithDups/${currentTaxon}_${currentId}.bam
		java -XX:ParallelGCThreads=${numThreads} -jar $PICARD_PATH/picard.jar MarkDuplicates INPUT=$BAM_PATH/${currentTaxon}/WithDups/${currentTaxon}_${currentId}_srt.bam OUTPUT=$BAM_PATH/${currentTaxon}/DedupBAMs/${currentTaxon}_${currentId}_srt_dedup.bam  METRICS_FILE=$BAM_PATH/${currentTaxon}/DedupFiles/metrix.${currentTaxon}_${currentId}_dedup  READ_NAME_REGEX=null TMP_DIR=. COMPRESSION_LEVEL=1
		samtools index $BAM_PATH/${currentTaxon}/DedupBAMs/${currentTaxon}_${currentId}_srt_dedup.bam
	done
else
	echo "Operating in Single End Mode, align each sample one at a time."
	for (( i=0; i<${len}; i+=1 ));
	do
		echo "${fastqArray}"
		currentId="phg_id_"${i}
		bwa mem -M -t ${numThreads} -R '@RG\tID:'${currentId}'\tSM:'${currentTaxon}'\tPL:ILLUMINA' ${referenceFile} ${fastqArray[i]} > $BAM_PATH/${currentTaxon}/WithDups/${currentTaxon}_${currentId}.sam
		samtools view -bS $BAM_PATH/${currentTaxon}/WithDups/${currentTaxon}_${currentId}.sam > $BAM_PATH/${currentTaxon}/WithDups/${currentTaxon}_${currentId}.bam
		samtools sort -l 1 -m 4G -@ 4 -o $BAM_PATH/${currentTaxon}/WithDups/${currentTaxon}_${currentId}_srt.bam $BAM_PATH/${currentTaxon}/WithDups/${currentTaxon}_${currentId}.bam
		java -XX:ParallelGCThreads=${numThreads} -jar $PICARD_PATH/picard.jar MarkDuplicates INPUT=$BAM_PATH/${currentTaxon}/WithDups/${currentTaxon}_${currentId}_srt.bam OUTPUT=$BAM_PATH/${currentTaxon}/DedupBAMs/${currentTaxon}_${currentId}_srt_dedup.bam  METRICS_FILE=$BAM_PATH/${currentTaxon}/DedupFiles/metrix.${currentTaxon}_${currentId}_dedup  READ_NAME_REGEX=null TMP_DIR=. COMPRESSION_LEVEL=1
		samtools index $BAM_PATH/${currentTaxon}/DedupBAMs/${currentTaxon}_${currentId}_srt_dedup.bam
	done
fi


#copy over the external bam files to DedupFiles
if [ -d /tempFileDir/data/bam/externalBams/ ]; then
  # Control will enter here if $DIRECTORY exists.
    if [ -z "$(ls -A /tempFileDir/data/bam/externalBams/)" ]; then
        echo "Empty"
    else
        echo "Copying over external bam files"
        cp -r /tempFileDir/data/bam/externalBams/ $BAM_PATH/${currentTaxon}/DedupBAMs/
    fi
fi


echo "Aligning new fastqs to Reference done, Moving onto haplotypeCaller"
echo "NOTE: IDs for BAM file ReadGroups is autogenerated, Please use Picard to update the RG fields correctly if you use these BAMs in other settings"


echo "Filtering Bam files, make sure each BAM has the sample name at the beginning separated by an underscore"

#Step 2 Filter BAM files
cd $BAM_PATH/${currentTaxon}/DedupBAMs

bamfiles="`ls *.bam`"
mkdir $BAM_PATH/${currentTaxon}/filteredBAMs
cd $BAM_PATH/${currentTaxon}/filteredBAMs

for bam in $bamfiles
do

echo $bam
bamSubString="`echo ${bam} | cut -d. -f1`"
# filter command:
samtools view -b -o $BAM_PATH/${currentTaxon}/filteredBAMs/${bamSubString}_trimmed_intervals_mapQ_${mapQ}.bam -L $FILTER_BAM_INTERVALFILE -q ${mapQ} $BAM_PATH/${currentTaxon}/DedupBAMs/${bam}

# make a new index for your filtered BAM file:
samtools index $BAM_PATH/${currentTaxon}/filteredBAMs/${bamSubString}_trimmed_intervals_mapQ_${mapQ}.bam

done

echo "Done filtering BAM files, Proceeding to GATK"

#Step 3. RUN GATK
cd $BAM_PATH/${currentTaxon}/filteredBAMs

#Get the list of taxaNames which should be separated from the rest of the file by an underscore
taxaNames="`ls *.bam | cut -d_ -f1 | sort -u`"
cd ../..

for taxon in $taxaNames
do
# Make sure its our current taxa
if [ $taxon == $currentTaxon ]
then
	echo "Taxon: " $taxon

    sentieonString="`grep -m 1 '^sentieon_license=' ${dbConfigFile} | cut -d'=' -f2`"
    export SENTIEON_LICENSE=$sentieonString

    if [ ${SENTIEON_LICENSE} != "" ]
    then
        echo "Using Sentieon to create GVCFs"
        input_list='';
        for f in $BAM_PATH/${currentTaxon}/filteredBAMs/${taxon}*.bam
        do
         input_list="${input_list} -i ${f}"
        done

        time /sentieon/bin/sentieon driver \
                    -t ${numThreads} \
                    -r $referenceFile \
                    ${input_list} \
                    --interval $INTERVAL_FILE \
                    --algo Haplotyper \
                    --ploidy 1 \
                    --emit_mode gvcf \
                    $GVCF_PATH/${taxon}_haplotype_caller_output.g.vcf

    else
        echo "Using GATK to create GVCFs"
        input_list='';
        for f in $BAM_PATH/${currentTaxon}/filteredBAMs/${taxon}*.bam
        do
         input_list="${input_list} -I ${f}"
        done

        /gatk/gatk --java-options "${fullXmx}" HaplotypeCaller \
            -R  $referenceFile \
            $input_list \
            --ERC GVCF \
            -L $INTERVAL_FILE \
            --ploidy 1 \
            -O $GVCF_PATH/${taxon}_haplotype_caller_output.g.vcf
    fi

fi

done
echo "Finished running HaplotypeCaller, Proceeding to GVCF filtering"


#rename the sample after we have run GATK
mkdir -p /tempFileDir/data/renames/
#print out the taxon name that we want to replace the sample name in the g.vcf file
printf "${taxon}" >> /tempFileDir/data/renames/${taxon}_renameFile.txt
#bcftools reheader will rewrite the sample name to a temp file
bcftools reheader -s /tempFileDir/data/renames/${taxon}_renameFile.txt -o ${GVCF_PATH}/${taxon}_haplotype_caller_output.g.vcf_temp ${GVCF_PATH}/${taxon}_haplotype_caller_output.g.vcf
#overwrite the original file and delete it
mv ${GVCF_PATH}/${taxon}_haplotype_caller_output.g.vcf_temp ${GVCF_PATH}/${taxon}_haplotype_caller_output.g.vcf


# #Step 4 Filtering GVCF File
GVCF_FILTERED_PATH=/tempFileDir/data/filteredGvcfsAndFasta/
mkdir -p ${GVCF_FILTERED_PATH}Fastas
mkdir -p ${GVCF_FILTERED_PATH}Filtered

perl /tassel-5-standalone/run_pipeline.pl ${fullXmx} -debug -FilterGVCFPlugin -refFile $referenceFile -bedFile $INTERVAL_FILE -gvcfDir $GVCF_PATH -outputDir ${GVCF_FILTERED_PATH} -configFile $dbConfigFile

echo "Done filtering GVCFs and extracting fastas: Proceeding to upload to DB"

#Write out a text file holding the information for loading into the db
rm /tempFileDir/data/${currentTaxon}_loadFile.txt

#echo -e "Genotype\tHapnumber\tDataline\tploidy\treference\tgenesPhased\tchromsPhased\tconfidence\tMethod\tMethodDetails\n${currentTaxon}_Haplotype_Caller\t0\t${currentTaxon} from Haplotype Caller\t1\tfalse\tfalse\tfalse\t1\t${methodName}\t${methodName} created for Haplotype Caller" >> /tempFileDir/data/${currentTaxon}_loadFile.txt
echo -e "Genotype\tHapnumber\tDataline\tploidy\treference\tgenesPhased\tchromsPhased\tconfidence\tMethod\tMethodDetails\n${currentTaxon}\t0\t${currentTaxon} from Haplotype Caller\t1\tfalse\tfalse\tfalse\t1\t${methodName}\t${methodName} created for Haplotype Caller" >> /tempFileDir/data/${currentTaxon}_loadFile.txt




cd ${GVCF_FILTERED_PATH}Fastas

perl /tassel-5-standalone/run_pipeline.pl ${fullXmx} -debug -GetDBConnectionPlugin -config $dbConfigFile -create false -endPlugin -LoadHapSequencesToDBPlugin -fasta ${GVCF_FILTERED_PATH}Fastas/${currentTaxon}_haplotype_caller_output_filtered.fa -genomeData /tempFileDir/data/${currentTaxon}_loadFile.txt -gvcf ${GVCF_FILTERED_PATH}Filtered/ -endPlugin


#Cleaning up
#getting rid of the rm commands for now.
mkdir -p /tempFileDir/data/outputs/gvcfs
mkdir -p /tempFileDir/data/outputs/bam
cp -r $GVCF_PATH /tempFileDir/data/outputs/gvcfs
#rm -r $GVCF_PATH
#cp -r $BAM_PATH/${currentTaxon}/DedupBAMs/ /tempFileDir/data/outputs/bam
#rm -r $BAM_PATH

echo "Finished Creating Haplotypes"


