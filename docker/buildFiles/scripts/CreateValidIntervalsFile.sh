#!/bin/bash -x
#  run CreateValidIntervalsFilePlugin.kt

# Mount localMachine:/pathToBaseDockerDir to docker:/phg/
# Input Folders -
#    it is expected the intervalsFile lives at the top mount point /phg/
#    the dbRef (reference fasta) should be in a directory mounted to /phg/inputDir/reference/
#    the generated intervals file will be created in the top level dir mounted to /phg/


MOUNT_DIR=/phg
INPUT_DIR=${MOUNT_DIR}/inputDir
REFERENCE_DIR=${INPUT_DIR}/reference

intervalsFile=$1
dbRef=$2
outputFile=$3
mergeOverlaps=$4
otherGroupName=$5

memoryString="`grep -m 1 '^Xmx=' ${MOUNT_DIR}/${dbConfigFile} | cut -d'=' -f2`"
if [ ${memoryString} != "" ]
then
    fullXmx='-Xmx'${memoryString}
else
	fullXmx='-Xmx10G'
fi

/tassel-5-standalone/run_pipeline.pl ${fullXmx} -debug -CreateValidIntervalsFilePlugin -intervalsFile ${MOUNT_DIR}/$intervalsFile  \
                                                        -referenceFasta $REFERENCE_DIR/$dbRef \
                                                        -generatedFile $MOUNT_DIR/$outputFile -mergeOverlaps $mergeOverlaps  \
                                                        -otherRegionsGroupName $otherGroupName -endPlugin
