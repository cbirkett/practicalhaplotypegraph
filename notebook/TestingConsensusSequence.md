THis book details things done to verify queries for grabbing consensus anchor sequences

Details:

**JUNIT to create dummy db**
* Run the src.test.java.net.amizegenetics.analysis.data.CreateTestDB.java class
    * this class shows how to populate a dummy db with just a few pieces of data
    * Read the comments in it to see limitations.
    * this is useful if you don't want large fastas
* Testing queries for CreateGraph.createHaplotypeSequenceFromConsensus()
    * problem: the query identifies correctly the consensus and non-consensus anchor_sequence_ids, but always pulls sequence for original id, not the consensus
    * Swapping the position of the LEFT JOIN and INNER JOIN in the existing query in this method might help performance, did not change the result.
    
           String query = "SELECT anchor_haplotypes.anchorid AS anchorid, " +
                " anchorseq_consensus_anchorseq.consensus_anchor_sequence_id AS consensus_anchor_sequence_id," +
                " anchor_sequences.anchor_sequence_id as anchor_sequence_id, " +
                " anchor_sequences.sequence as sequence, anchor_sequences.seq_hash AS seq_hash " +
                "FROM anchor_sequences " +
                "LEFT JOIN anchorseq_consensus_anchorseq " +
                " ON anchor_sequences.anchor_sequence_id = anchorseq_consensus_anchorSeq.anchor_sequence_id " +
                " AND anchorseq_consensus_anchorseq.method_id=" + methodId + " " +
                "INNER JOIN anchor_haplotypes ON anchor_sequences.anchor_sequence_id = anchor_haplotypes.anchor_sequence_id";
* Currently it looks like a list of consensus sequences must be pulled (use original sequence id if it does not map to a consensus sequence)
and then grab sequence for each of those in a loop.  Must be a better method.  WIll post when I have something working, even if it needs cleaning.

 