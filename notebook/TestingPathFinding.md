##Testing Path Finding algorithm
###Notebook denoting steps taken to create and tune the integration test testing Peter's HMM code


Main Test Class is package net.maizegenetics.pangenome.hapCalling.HapCountBestPathPluginTest.java

Test adopting Ed's code from pipelineTests.EvaluateGVCFbyKnowndSNPTest2
Compares the SNPs in the axiom array to the SNPs called in various stages of the pipeline

Known SNP set is the axiom array TUM8Lines_Maize600k_elitelines_AGPv4_Tasselsorted.vcf.gz

#TODO Fix file paths/move to Box

4 Main GVCF files to test
* W22 non-consensus GVCF used to create PHG(Used as a baseline test, we shouldnt be able to do much better than this)
* W22 containing consensus GVCF used to create PHG consensus
* GBS W22 ApeKI aligned to Consensus and GVCF exported from HMM
* GBS W22 pstI from ChrisBottoms aligned to Consensus and GVCF Exported From HMM


Integration tests are located in HapCountBestPathPluginTest.java

Through Ramu's help we learned that if we filter out some taxa, we improve the error rate immensely.

For the W22 GBS tests if we keep the taxa B97,Ky21,M162W,Mo17,Ms71,OH43,OH7B,W22, the error rate lowers to about 4%.

The code has 2 methods for when you are searching for a path in the HMM.
* The first one will create a PHG and HMM based on the provided inclusion count file and export a single gvcf file to the temp directory
    * This step is fairly time consuming(about 10 minutes)
    * When it is done it will call the second method.
* The second will evaluate the gvcf and print out the error rate and counts


Running Notes:

GBS Tests:
ApeKI

First Run using just inclusion set:

        HetIndelMissing	223005
        refCallsDoNotMatch	157
        gvcfAgreesWithKnown	113094	96.49658703071673
        mismatchGVCFisRef	6963	8.692883895131086
        mismatchGVCFisAlt1	5521	8.001449275362319
        mismatchGVCFisAlt2	2028	8.521008403361344
        totalScored	127606.0
        totalErrorRate	0.11372505995015908
        twoAlleleErrors	0.09783239032647367

* error rate relatively high, number of hetindelmissing is also very high

Using All Counts:

        HetIndelMissing	224525
        refCallsDoNotMatch	154
        gvcfAgreesWithKnown	113428	96.69906223358909
        mismatchGVCFisRef	7411	9.33375314861461
        mismatchGVCFisAlt1	3290	5.927927927927928
        mismatchGVCFisAlt2	1960	8.412017167381974
        totalScored	126089.0
        totalErrorRate	0.10041320019985883
        twoAlleleErrors	0.08486862454298154
        
* Error rate goes down a bit, still a lot of missingness

Using subsetted taxa(B97,Ky21,M162W,Mo17,Ms71,OH43,OH7B,W22).
* Filtered out the axiom SNPs not in anchor ranges power shot up to 75%
* Also counted how many Axiom SNPs are likely incorrect due to being a compliment(2459 out of the 3254 mismatchGVCFisRef)

        HetIndelMissing	44870
        refCallsDoNotMatch	148
        gvcfAgreesWithKnown	115808	98.89666951323655
        mismatchGVCFisRef	3254	4.96793893129771
        mismatchGVCFisAlt1	2206	4.837719298245614
        mismatchGVCFisAlt2	2128	8.184615384615384
        totalScored	123396.0
        totalErrorRate	0.06149307919219424
        twoAlleleErrors	0.04424778761061947

* When removing the compliments from the error, we are left with ~4%.