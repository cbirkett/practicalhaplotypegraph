THis book details procedures tested when trying to locate Dan's rampSeq short Seqs in the AssemblyGenome files
Prior tests:

  See notebook AnchorRampSeqToDBIntervals.md in PHG-36 branch  describing the process of mapping rAmpSeq contigs to PHG DB intervals.  That notebook describes what happens after Dan has created fastas using the results of this pipeline.
  
Details:  

**jar files to execute these tests were created from java classes in PHG master branch, pangenome.pipelineTests**

In all the files, each of Dan's 9776 contigs from the rampSeq input fasta are searched for in every chrom/scaffold of every assembly provided.  Both presented contig and reverse-complemented, so 2*9776 queries.  ALL occurences in each string are sought.
All methods take 3 inputs: Seq contig fasta, a directory path where Assembly fastas live, an output directory where files should be written.

* First test: Initial class FindShortSeqInAssemblyGenomes.java
    * This class parallelizes over each assembly, initially 11 threads (11 assemblies)
    * The class uses java string indexOf(string,startPos), with the "startPos" beginning at founIndex+1 when an occurrence of rampSeq string is found
    * This is setup on cbsudc01 - a large memory machine.  Still running after 36 hours.
    
* Second Test: Slightly revised FIndShortSeqInAssemblyGenomes.java
    * Same code, but indexOf(string,startPos) is incremented by index-of-contig + contig length 
    * RUnning on cbsumm15 (median memory). 
     
* Third Test:  Change the parallelization to run over each chromosome in an assembly at once (some have several hundred contigs).
    * Still using indexOf, but either wasn't coded well or just not effective.  Only 200% (2 threads) appeared in use via "top"
    * was running on cbsulm14 but I killed it
    
* Fourth Test:  Class is FindShortSeq2.java:  RUnning on cbsulm14 (large memory machine)
    * This test uses a Knuth-Morris-Pratt algorithm to do the string search.  
    * Code pulled from the web site :http://algs4.cs.princeton.edu/53substring/KMP.java.html
    * File is KMPStringSearch in PHG Master, pangenome/pipelineTests (not delivered)
    * does not appear to be any faster.  Could be overhead problems with KMP algorigthm
    
* Fifth Test:  Class is FindRampSeqInAssemblies_Second.java
    * This one is back to using indexOf(seq,startPos), with startPos incremented by "foundIndex + 2" for subsequent searches.
    * Parallizes over all the rampSeq contigs when processing each chrom of an assembly.
    * Has more threads in use - generally see > 9600% CPU
    
As of Wednesday, Oct 4, 3pm none were finished.

**UPDATE Thursday, Oct 5 2017**

THe 5th test finished in 14 hours, so that is the code to use going forward.  One slight glitch where the "begin" position column had both begin position and rampSeq contig name.  I ran a quick script to fix that and stored corrected file to cbsublfs1 in /data1/PHG/rAmpSeq_files/shortSeq_mapToAssemblyFastas.  THe code has been fixed for the next run.

Other thought on this run:  The tab-delimited file only has headers requested by Dan: rampseq contig,  assembly idLine,   startPos,   endPos.  I did not include info on whether the match was on the forward or reverse strand.  The code can be altered to print that if needed.

Also, 2 of the 11 line had no matches:  B97_scaffolds_1K.fasta and GCA_001990705.1_Zm-F7-REFERENCE-TUM-1.0_genomic.fasta.  Dan confirmed this is consistent with what he has seen.

**UPDATE Friday, Oct 6 2017**

Yesterday I updated the code to print out strand into (added + or - in new column).
Re-ran on cbsulm16 in /workdir/lcj34/phg_pipeline/rampSeq_information/rampSeq_shortSeqMatch_output2

Also deleted original from repository that parallelized over assemblies, committed the
new version that parallelizes over rampSeq Contigs when processing each assembly chrom/scaffold.

New code now called FindRampSeqContigsInAssemblies.java, living in pangenome.processAssemblyGenomes

THis has been delivered to PHG master under PHG-41.  This is currently run as a jar, not as a plugin.
Should switch to a plugin so anyone can run if they have PHG.