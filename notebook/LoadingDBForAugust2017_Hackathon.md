#This document details tasks completed for loading PHG db with latest GVCF files


##Data Specifics:  August 7, 2018
* Ref anchors still created based on V4 GFF files with 1000 bps flanking.  When there is less than 2000 bps between genes the difference is split between them.  if a gene is totally embedded within another, they are merged into 1 anchor.
* Assembly genoomes have been split into contigs in fastq files.
    * The data identification line has "@chrom:assemblyStartpos:asemblyEndPos
    * The contig stops when an N is encountered, the next contig begins at the next non-N character
    * I ignored sequence belonging to lines that indicate "scaffold", but still got some non 1-10 chroms, e.g chrPT and chrMT and chrUNKNOWN (for ph207).  We are leaving them in for the BWA-MEM stage (which Zack is running)
    
##in Progress: week of August 7, 2017
* New version of LoadHapSequenesToPHGdb that will be specific for assemblies.  This is due to different ID line that needs special processing.
* Writing method to store consensus anchor sequences
    * the fasta file idline will contain: refchrom:refstart:refend;taxaA_hapNum:taxaB_hapNum ...
* method written, have Zack's files for testing.  He has 1 file per anchor (37124 files, 680 missing consensus)
* Issues:
    * it is very slow.  Starts out adding sequences very quickly - less than 1 second. quickly degrades
    * profiling shows time is spent in execute batch
    * Attempt to fix:
        * Read files separately and process.  Lets me know how many are processed, but is slow
        * Create db only once, pass this instance as we process files.  BiMap only created once. Negligible improvment
        * Concatenate files to a single one (all chr1 together).  hasn't helped
        * Investigate disabling triggers and dropping indexes.
            * no db triggers supported in this instance (it doesn't really do FOREIGN KEYS)
            * can only drop user defined indexes, not system (UNIQUE or PRIMARY KEY)
        * Because processing gets progressively slower as the size of the tables increase, I'm testing with a DB that has removed the UNIQUE and PRIMARY KEY constraints on the anchor_sequences and anchorseq_consensus_anchorseq tables. This is just a test to confirm this is the problem.
        * No other brilliant ideas - will sleep on it.
       
## Week of August 14, 201
* Solution to loading issue:
        * the main problem was associating hapids with taxaName_hapnumber.  I was making individual db calls for this - very inefficient.  Created a method that grabbed all line_name data at once and stored to a map with hapids.  THen consulted the map when need be.
        * I also changed certain PRAGMA settings - cache_size and Page_size increased.  Not sure if this makes much difference, but googling says it should help.  It definitely doesn't hurt.
        

    