#This document details the search for a research data management platform to be installed on the new large memory BL machine.


##Desired Functionality
* open source, with quick and simple deployment
* metadata handling capabilities
* interoperability : metadata record formats following standards, e.g. OAI-PMH (Open Archives Initiative Protocol for Metadata Harvesting), dropbox integration
* content dissemination: API, search capabilities
* file size limitations:  API limitations in upload? Underlying methods for uploading large files
* search features:  API functionality
* support:  Is software currently maintained, community adoption levels
* pre-reserving DOI:  Is this a desirable? GitHub DOI integrations?
* Customization abilities

##Repository Software Considered

* DSpace: open source repository supported by DuraSpace organization available from GitHub
* DataVerse Project: open source research data repository, available from GitHub.  Used by CIMMYT and other CG centers (per Kate Dreyer)
* CKAN: Comprehensive Knowledge Archive Network, open source written in Python, available from GitHub.  Used extensively for government data.
* iRODS: Integrated Rule-Oriented Data System, open source data management available on GitHub


## Initial Decisions
* Discussed with Ed and Cinta in programmer's office.  We decided iRods was the way to go based on ease of moving files to Cyverse and to cbsu machines.
* Robert still has concerns about performance and iRods tools.
* Cinta talked with Qi/Robert and they now have better handle on the data we want to store.
* currently waiting for input from Qi/Robert