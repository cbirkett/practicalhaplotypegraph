**Assembly Interanchor Identification Methods tested**

THis notebook details the methods used to find assembly interanchor regions.

**Initial method**
The assembly anchors were identified via BLASTing the ref anchors against the assembly, filtering for quality, added 1000bps flanking on either end (splitting the difference if less than 2000 bp's separated the anchor regions).  With the older db schema, the anchor_haplotypes table contained fields identifying the assembly start/end coordinates for each anchor. 

Armed with the assembly anchor coordinates, they were sorted by chrom/position and actual inter-anchor regions were pulled .  No interanchor regions were stored when anchors mapped out of order.

Analysis: this method grabbed the actual interanchor sequence and allowed for exact re-creation of the assembly from the db.  It is no longer viable as we no longer know the assembly coordinates for the anchors.

Issues:  This method of creating assembly anchors was changed as the assembly haplotypes were different enough in length to cause problems when creating consensus sequences.  It was decided a better method of creating assembly anchors was needed, hence we moved to method 2.

**Method 2: ref inter-anchor start/end read overlaps**

DB Schema was updated, no longer keep assembly interval coordinates.
Assembly anchor regions now created via:
* create a fasta of assembly contigs, splitting each chromosome/scaffold where an N appears via processAssemblyGenomes/CreateContigFastaFromAssemblyGenomePlugin.java
* Run the assembly contig fasta and a reference genome fasta through processAssemblyGenomes/Minimap2PluginPipeline, using minimap2 to align assembly contigs to the ref; subsequently create the fastas and gvcf files needed for loading to db.  
* This method is consistent with the manner in which other lines are processed - the gvcf file is created by calling the same method used by Zack for other lines.

Get Assembly Inter-anchors:
* Take the BAM file created as part of Minimap2PipelinePlugin when aligning the assembly contigs to ref genome.
* Run this BAM file through processAssemblyGenomes/FindAssemblyInterAnchorRanges.java 
    * find reads in the BAM file that overlap the ref interanchor start and end positions
    * These reads are split at the ref position overlap; sequence from the start-to-end position become fasta lines for db loading

Analysis: The mappings did not look bad, but we missed many.  Out of 37644 ref inter-anchors, b104 only    
24582 sequences mapped.  Looked for a method that would map more.



 
**Method 3: October 24, 2017, ref anchor1-interanchor-anchor2 alignment**

Using a new method to created assembly inter-anchors.  Code is in branch PHG-35, pangenome/processAssemblyGenomes/CreateAssemblyInteranchors.java

This new method creates a fasta of ref sequence from the db.  It concats anchorX-interanchor-anchorX+1 sequence with an IDLine of >refChrom:anchor1Start:anchor1End:anchor2Start:anchor2End;inter-anchorID

The fasta above is sent through the Minimap2PipelinePlugin (with onlyBam parameter=true) and aligned against the assembly genome. 
    * opposite of method 2:  this aligns the ref anchor-interanchor-anchor against the assembly genome. Method 2 aligned assembly contigs to ref genome
    
SAMRecord is used to get the alignments (dropping non-aligned, non-primary and secondary alignments).  CIGAR string is used to find the inter-anchor regions.  Entries with excessive soft-clipping that dropped either anchor are filtered.

Analysis:  This method did well when it aligned, but after tweaking out clipped entries, it only mapped 17221 out of 37624 entries.  This is much fewer than method 2.  Most of the problem was soft clipping the first anchor (13486).  The inter-anchor regions are larger than anchor regions (as expected).  The aligner goes for the most bang for the buck, and soft clips until it gets a good reading (I think).

**Method 4: November, 2017:  Lastz with Ramu's scripts**
This method took too long.  Break b73 chrom 1 into single fastas of 5 million bps each.  Break cml247 chrom1 up the same.  Each fasta gets lastz'd against each other fasta.  Just to finish chrom 1 it took multiple days on large memory machine running 110 cores at a time.

Ramu's scripts: (attached to email from 10/30/17, 12:55pm)
step0_convertFaToNibFiles.sh
step1_pairwiseWithLastz.pl
step3_axtChain.pl
step4_chainMergeToAxtToMaf.pl




RObert says it probably took so long because there was no masking.  Ramu says we need the reads unmasked.

For time reasons, we are dropping this for now.
**Questions**

How well can we expect ref inter-anchor regions to map to assemblies?  If these are non-conserved areas of the genome, and if they are quite large, is it surprising that few areas map?

How will the interanchor regions be used?  What can they tell us when hunting for SNPs or creating a graph?
