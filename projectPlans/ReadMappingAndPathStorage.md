
#Read Mapping and Path Storage 
| Plan Specifics  |  Details |
|:---|---|
|**Epic**  |  PHG-339 |
|**Document status**   | DRAFT  |
|**Project Lead**   | Cinta Romay |
|**Partners** | Peter Bradbury, Terry Casstevens, Zack Miller, Lynn Johnson, Cinta Romay |
|**Software Lead** |  Zack Miller, Lynn Johnson |

## Objective: Problem Statement

Previous PHG code aggregated all skim sequence reads that hit a haplotype, creating "inclusion" and "exclusion" counts.
These were stored to the haplotype_counts table. The haplotype counts data was processed through a hidden markov model to identify the most
likely path through the pangenome graph.  The path was linked to the haplotype_counts entry and stored in the PHG paths table.

The algorithm has changed.  Processing now counts all haplotype ids hit by each read.  This new read mapping must be 
stored in the data base in a new table.  The identified paths must also be stored but will now link to the read mapping results.  This 
will require database schema changes. Because haplotypes ids are stored for every read, the size of the stored data is 
potentially very large. An algorithm for compressing the information will need to be developed. 

As part of this processing, a new method will be created to consistently map taxa names to fastq files via a key file.  
This key file will follow NCBI conventions, using the "cultivar" field.


## Success metrics

Explain the metrics used to determine whether your objectives were successful.

|Goal| Metric  |
|---|---|
| Data base schema updated with new/altered tables | The path of a taxon through a particular PHG subgraph can be identified and pulled from the database. |
| Taxa names identified from fastq files using new Key file | This could be a new taxon name, so the sucess metric is that after loading in a count/path, the names are consistent with the keyfile.   |
| The blob that stores the read mappings for a single fastq will be sufficiently small  | The blob will be under 500 MB.  It would be nice to be in the 10-20 MB range |
|   |   |

## Assumptions

###Biology assumptions:

* Keyfile should follow NCBI upload standards
* Storing Paths in DB should have identical Genotyping Results as from a file.

###Software Assumptions:

* Read mappings will be a collection of hapids. Once read-mappings are stored, existing hapids will not change though
 new ones could be added as long as the new hapids are greater than any existing hapids.
* Find Paths should work the same regardless if ReadMapping was stored in the DB or from a file.

## Tests (Biology and Software)
Specifc unit tests should be covered in jira tasks.  This table is intended to capture tests used to prove biological relevance.  They should be biologist approved

|   Test Description|Data Input | Expected Result   |
|---|---|---|
|Genotype 10 WGS Taxa.  Run through FindPaths and get a VCF file.  VCF file Compared to Axiom should be identical to a VCF file made from the files.  | 10 of the 282 WGS Fastqs | With MinReads=0, should have >97% Power and below 10% Error rate depending on Taxa.  |
|Genotype 10 GBS Taxa.  Run through FindPaths and get a VCF file.  VCF file Compared to Axiom should be identical to a VCF file made from the files.  | 10 of the Ames GBS Fastqs | With MinReads=0, should have >97% Power and below 10% Error rate for taxon in the DB, higher error for those not in DB.  |

## Milestones
1. Database Schema is updated
2. Decide on way to store ReadMappings
3. Keyfile import is used when doing alignment
4. Both ReadMappings and Paths are uploaded to the DB 


## Requirements

| #  | Requirement  | Importance  | Jira Issue | Status |
|---:|---|---|---|---|
| 1 | Update database schema  |  *high* | PHG-341 |  backlog |
| 2 | Create key file template | *high*  |   | backlog  |
| 3 | Methods created to populate read_mapping and read_mapping_group tables | *high* |  | backlog |

## Requirement 1 details

There will be 2 new tables:  read_mapping and read_mapping_group.  

The read mapping table will have these fields: (why "file group name" vs "group_id")

| Field | Type | Description  | 
|---:|---|---|
| read_mapping_id | integer | DB supplied id for this table entry |
| genoid| integer | Genoid of taxa, maps to genotypes table |
| file_group_name|  String | File group name from the fastq file, data represented in the key file (flowcell, lane, etc)|
| method_id | integer | method used in mapping - links to methods table |
| mapping_data | BLOB | each read will align to a set of one or more hapids. Each hapid subset hit by a read is stored along with a count of the number of times the subset was hit by a read |

The read_mapping_group table will be used to link individual read mapping entries with groups.  It will contain the 
following fields.

| Field | Type | Description  | 
|---:|---|---|
| rm_group_id| integer | DB supplied id for this table entry |
| read_mapping_id |  integer | maps to read_mapping table read_mapping_id field|
| name | String | name of group |
| description | String | user supplied description of this grouping |

The paths table will contain the same data, but the "haplotype_counts_id" will now be a foreign key to the rm_group_id of 
the read_mapping_group table. 


## Requirement 2 details

A key file must accompany each set of skim sequence files used for genoyping and path detection.  The file contents will be based 
on NCBI.  The format and required columns of this file must be specified. An example key file must be created and stored 
on the PHG WIKI.

## Requirement 3 details

The read-to-haplotype mappings resulting from running the genotyping and path detection pipeline phases must be stored in 
the database in the read_mapping, read_mapping_group and path tables.  As part of this, we should continue our disscussion for the best way of storing things in the DB.  
Multiple ideas should be coded up and tested. 
Methods must be written to create the BLOB data, proper linkings made ensuring no duplicate groups.

| Idea | Description | Pros/Cons  | Try or Not |
|---:|---|---|---|
| Just Ignore Read name | Just ignore the read name and store the mapping as it is currently being written | Pros: simple, keeps reads separate.  Cons: Huge BLOB size 5.6GB for a 10x if each read hits on average 10 hapids. | DO NOT TRY |
| Bit encode Longs | For each Read bit encode a Long to hold which hapIds get hit.  | Pros: also simple.  Decent compression(1GB per 10x fastq)  Cons: Still kinda large.  What happens if we have more than 64 haplotypes per refRange.  Also need to sort Nodes in api | TRY if other approaches fail|
| Aggregate "Hit Sets" store count | For each read turn it into a Set containing hapIds(Integer).  Then just count how many reads have the same set. Will need to do some checks to see how well this will compress in practice |Pros: Should not be too bad to implement.  Likely will have very good compression.  Scalable to any number of hapIds per refRange. Cons: could have 2^n subsets per refRange.  If this was true we would have really bad compression| TRY | 
| Store files on server BLOB is link| Have FastqToMappingPlugin only export a file.  Then move that file to a shared location and store the link in the BLOB | Pros: very easy, do not have to worry about DB sizing.  Cons: flat files. Accessing things over network.  Unable to guarantee location is up-to-date. | TRY only if everything else does not work |

A note here:  Aggregate Hit Sets seems to be the way to go.  I tested 3 of the higher depth read mappings to see how well it would compress.  It seems to drop the space required down a ton.  The number of unique sets tends to be ~5% of the number of reads.  On average there are 3.3 hapIds per unique subset which if we cound a full int per each hapId and an in for each subset for the count, the total size of the BLOB should be 10-20 MBs per fastq(or pair of fastqs) that was aligned.  This could likely be compressed as well.

## User Interactions

Users will need to create the key file to be used when running skim sequences through Phase 2 of the pipeline to infer
genotypes and determine a path through the graph.  The format must be formalized and documented on the PHG WIKI.
## Open Questions

This table contains a list of questions that need answering as the project develops.  It could be blank.

|  Question | Answer  | Source/Date Answered  |
|---|---|---|
| Is the Hit Set option for storing the ReadMappings viable?  |   |   |
|   |   |   |


## Risks

* The structure of the blob used to store read mappings has not been finalized. It is possible that we will not be able to 
identify a structure that is both compact and can be processed fast enough. This risk is moderate as we have already discussed
strategies that seem likely to work.

## Out of Scope

List the features discussed which are out of scope or might be revisited in a later release.