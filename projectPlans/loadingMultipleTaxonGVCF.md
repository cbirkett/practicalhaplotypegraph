
#Consensus for Assemblies 
| Plan Specifics  |  Details |
|:---|---|
|**Epic**  |  [PHG-353](https://tassel.atlassian.net/browse/PHG-353) |
|**Document status**   | DRAFT  |
|**Project Lead**   | Zack Miller (zrm22@cornell.edu)  |
|**Partners** | Lynn Johnson |
|**Software Lead** |  Zack Miller |

## Objective: Problem Statement

Retool the CreateHaplotypesFrom* steps to allow for faster uploads.  Need to have the user input a key file and a directory.
From this, we should be able to pair off fastqs for alignment and know what the taxon names for the genotype table needs to be.

## Success metrics
|Goal| Metric  |
|---|---|
|  Speed up loading GVCFs | Loading up GVCFs should be <<24 hrs for 400 taxon. |
|  Medium Memory Target | If we run this on TACC/Cyverse, needs to run on mm machine(<100GB RAM). |

## Assumptions

Biology assumptions:

* NONE

Software Assumptions:

* Multithreaded- we can run taxon on its own thread.
    * See if coroutines are stable enough to use.
    * If not use parallel stream or ThreadPool
* Need to be able to keep variants consistent.

## Tests (Biology and Software)

|   Test Description|Data Input | Expected Result   |
|---|---|---|
| Time Spent Running 400 taxon  | 400 Sorghum GVCFs | Should take less than 24 hrs.  Current single thread process takes ~86 hrs.  |
| RAM Test  | 400 Sorghum GVCFs | Running 10 - 20 taxon at a time.  RAM usage needs to be below 100GB.  Targeting below 64GB so it can run on even smaller machines. | 

## Milestones
1. Implement keyfile+directory input version of LoadHapSequencesFromGVCFPlugin(single thread)
2. Implement Multithreaded version of LoadHapSequencesFromGVCFPlugin
3. Rework GVCF Loader to not use HTSJDK's VCFFileReader.query
4. Run on 50 GVCFs
5. Verify that it has the same haplotypes(ids will be different,could pull out sequence and make sure things match)
6. Rework Groovy Scripts to take the different inputs.
7. Update Small Seq Test
8. Run 400 GVCFs
9. Update wiki


## Requirements
*Note the JIRA tasks will be created and updated once the Pull Request goes through.*

| #  | Requirement  | Importance  | Jira Issue | Status |
|---:|---|---|---|---|
| 1 | Implement keyfile+directory inputs  |  *critical* | PHG-359  |  **DONE** |
| 2 | Implement multithreaded version of LoadHapSequencesFromGVCF | *critical* | PHG-364 | **DONE**|
| 3 | Rework GVCF Loader to not use HTSJDK's VCFFileReader.query | *critical* | PHG-365 | **DONE**|
| 4 | Test on 50 GVCFs | *high* | PHG-XXX | **DONE** |
| 5 | Verify same results for step 4. | *high* | PHG-XXX | **DONE** | 
| 6 | Rework Groovy scripts | *critical* | PHG-370 | **IN PROGRESS** |
| 7 | Update Small Seq Test | *critical* | PHG-370 | **IN PROGRESS** |
| 8 | Run 400 GVCFs | *critical* | PHG-XXX | **BACKLOG** |
| 9 | Update Create Haplotypes on wiki | *critical* | PHG-XXX | **BACKLOG** | 
|   |   |   |   |   |

## Requirements Details
1. Implement the keyfile and directory inputs for LoadHapSequencesFromGVCFPlugin.  This allows the PHG to better pair off the gvcf file with the correct taxon name.  Will also need some initial validation to make sure keyfile is valid(no duplicates of taxon)
2. Implement multithreading for LoadHapSequencesFromGVCFPlugin.  This is critical for the speed increase.  Be aware of keeping Variants and Alleles consistent across threads.  This might be tricky to do.
3. Rework GVCF Loader to not use HTSJDK's VCFFileReader.query  This needs to happen to support long chromosomes(Wheat). The issue now is that HTSJDK does not support csi indices for GVCFs.  Proposed solution: Load GVCF file in as Map<VariantInfo>.  Then pass the VariantInfos which overlap the refRange to the DB writer.  Needs some changes on the DB IO side to work correctly.
4. Do an initial speed test using 50 or so Sorghum GVCFs. Should take a few hours.  It would be ideal if below 1 hr for the 50.   
5. Verify the 50 taxon's haplotypes are identical to the single threaded version.  If this is not, we have a bug in the multithreading.
6. Rework the groovy scripts to take into account these changes.  FastQ and Bam portions will most likely be single threaded still, but should operate on a key file as well.
7. Update small seq tests to take into account the changes.  This might already be done when testing the other parts.
8. Do a large scale 400 Sorghum GVCF upload. This is expected to finish below 24 hours.  12 hours or less would be ideal.
9. Go through and update Documentation and Docker scripts.


## User Interactions
* Groovy scripts should be even easier to deal use as there will be less inputs.

## Open Questions

This table contains a list of questions that need answering as the project develops.  It could be blank.

|  Question | Answer  | Source/Date Answered  |
|---|---|---|
|   |   |   |


## Risks
* Keeping the Variants and Alleles consistent across threads.  Potentially should be able to hold all variants and Allele values into ram and then assign ids by using a HashMap.  If we just use a single write thread, this might be pretty straightforward.
## Out of Scope
* None for now.