We are in alpha development of the PHG.  It is currently being tested in maize, sorghum, and cassava.

We expect make the first official release in late December 2017.

To learn more about the PHG - please check out our wiki:

## Practical Haplotype Graph (PHG) Wiki

* [Wiki](https://bitbucket.org/bucklerlab/practicalhaplotypegraph/wiki/Home)
