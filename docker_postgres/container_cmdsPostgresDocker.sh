#!/usr/bin/env bash
set -u
set -e
set -x

# read  parameters from a file
source $1

docker run -i --detach --name $DOCKER_CONTAINER_NAME  -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -v $POSTGRES_DATA_MOUNT:/var/lib/postgresql/data -p $DOCKER_DB_PORT:5432 $DOCKER_IMAGE_NAME
docker start $DOCKER_CONTAINER_NAME

# Scripts can be run against the container created above to access the database, using a config file with host ip address and DOCKER_DB_PORT