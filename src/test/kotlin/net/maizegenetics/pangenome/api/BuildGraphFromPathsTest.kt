package net.maizegenetics.pangenome.api

import net.maizegenetics.pangenome.hapCalling.BestHaplotypePathPlugin
import net.maizegenetics.pangenome.hapCalling.ImportDiploidPathPlugin
import net.maizegenetics.pangenome.hapCalling.PathsToVCFPlugin
import net.maizegenetics.pangenome.smallseq.RunSmallSeqTestsDocker
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.taxa.TaxaListBuilder
import org.apache.log4j.BasicConfigurator

/*
* Running these tests requires a small seq database created by running RunSmallSeqTestsDocker
*
* */
public class BuildGraphFromPathsTest {

    fun buildGraphFromPaths() {
        BasicConfigurator.configure()
        ParameterCache.load(SmallSeqPaths.dbConfigFile)

        //find haploid paths
        val graphDataSetForPath = BuildGraphFromPathsPlugin()
                .pathMethod("PATH_METHOD")
                .performFunction(null)

        val theGraph = graphDataSetForPath.getData(0).data as HaplotypeGraph
        println("Graph has ${theGraph.totalNumberTaxa()} taxa")

        //this method has to get the path key file name from key file name, which is used by read mapping
        val pathFinder = BestHaplotypePathPlugin(null, false)
        pathFinder.setConfigParameters()
        pathFinder.keyFile(SmallSeqPaths.genotypingPathKeyFile)
                .readMethodName("READS_FASTQ")
                .pathMethodName("Paths_AllPathsGraph")
                .performFunction(graphDataSetForPath)

        val vcfOutFile = "${SmallSeqPaths.outputDir}fromAllPaths.vcf"
        writePathsToVCF("Paths_AllPathsGraph", "CONSENSUS", vcfOutFile)

        try {
            RunSmallSeqTestsDocker.compareResultAndAnswer(false, SmallSeqPaths.answerDir + "SmallSeq.vcf", vcfOutFile)
        } catch (e: Exception) {
            e.printStackTrace()
            System.err.println("Consider deleting phgrepository_base from Docker and rebuilding- using docker rmi imageID")
        }

    }

    fun buildGraphFromPathsAndTaxa() {
        BasicConfigurator.configure()
        ParameterCache.load(SmallSeqPaths.dbConfigFile)

        //find haploid paths
        val myTaxaList = TaxaListBuilder().add("LineA_wgs")
                .add("LineA1_wgs")
                .add("LineB_wgs")
                .add("LineB1_wgs")
                .add("Ref_wgs")
                .add("RefA1_wgs")
                .build()
        val graphDataSetForPath = BuildGraphFromPathsPlugin()
                .pathMethod("PATH_METHOD")
                .pathTaxaList(myTaxaList)
                .performFunction(null)

        val theGraph = graphDataSetForPath.getData(0).data as HaplotypeGraph
        println("Graph has ${theGraph.totalNumberTaxa()} taxa")

        //this method has to get the path key file name from key file name, which is used by read mapping
        val pathFinder = BestHaplotypePathPlugin(null, false)
        pathFinder.setConfigParameters()
        pathFinder.keyFile(SmallSeqPaths.genotypingPathKeyFile)
                .readMethodName("READS_FASTQ")
                .pathMethodName("Paths_SelectPathsGraph")
                .performFunction(graphDataSetForPath)

        val vcfOutFile = "${SmallSeqPaths.outputDir}fromAllPaths.vcf"
        writePathsToVCF("Paths_SelectPathsGraph", "CONSENSUS", vcfOutFile)

        try {
            RunSmallSeqTestsDocker.compareResultAndAnswer(false, SmallSeqPaths.answerDir + "SmallSeq.vcf", vcfOutFile)
        } catch (e: Exception) {
            e.printStackTrace()
            System.err.println("Consider deleting phgrepository_base from Docker and rebuilding- using docker rmi imageID")
        }

    }

    fun evaluatePaths() {
        BasicConfigurator.configure()
        ParameterCache.load(SmallSeqPaths.dbConfigFile)
        val vcfOutFile = "${SmallSeqPaths.outputDir}fromAllPaths.vcf"
        writePathsToVCF("Paths_AllPathsGraph", "CONSENSUS", vcfOutFile)

        try {
            RunSmallSeqTestsDocker.compareResultAndAnswer(false, SmallSeqPaths.answerDir + "SmallSeq.vcf", vcfOutFile)
        } catch (e: Exception) {
            e.printStackTrace()
            System.err.println("Consider deleting phgrepository_base from Docker and rebuilding- using docker rmi imageID")
        }
    }

    fun writePathsToVCF(pathMethod: String, haplotypeMethod: String, outputVcfFile: String) {
        //the pathMethod is the method name for the paths. The pathHaplotypeMethod is the haplotypeMethod used for path finding.
        val graphBuilder = HaplotypeGraphBuilderPlugin(null, false)
        graphBuilder.setConfigParameters()
        val graphDataSetForPath = graphBuilder
                .methods(haplotypeMethod)
                .includeSequences(false)
                .includeVariantContexts(true)
                .performFunction(null)
        val paths = ImportDiploidPathPlugin()
            .pathMethodName(pathMethod)
            .performFunction(graphDataSetForPath)
        val vcfPlugin = PathsToVCFPlugin(null, false)
        vcfPlugin.setConfigParameters()
        vcfPlugin.outputFile(outputVcfFile)
                .performFunction(paths)

    }

    fun testSplitNodes() {
        BasicConfigurator.configure()
        ParameterCache.load(SmallSeqPaths.dbConfigFile)

        println("Testing split nodes, expecting 560 nodes after splitting")
        val graphForPath = BuildGraphFromPathsPlugin()
            .pathMethod("PATH_METHOD")
            .build()

        //graph created by the next stip should have 280 nodes
        val splitGraph = CreateGraphUtils.nodesSplitByIndividualTaxa(graphForPath, 0.99)

        println("Same for six taxa, expecting 120 nodes")

        val myTaxaList = TaxaListBuilder().add("LineA_wgs")
            .add("LineA1_wgs")
            .add("LineB_wgs")
            .add("LineB1_wgs")
            .add("Ref_wgs")
            .add("RefA1_wgs")
            .build()
        val graph6taxa = BuildGraphFromPathsPlugin()
            .pathMethod("PATH_METHOD")
            .pathTaxaList(myTaxaList)
            .build()
        CreateGraphUtils.nodesSplitByIndividualTaxa(graph6taxa, 0.99)
    }

    fun testMultipleMethods() {
        BasicConfigurator.configure()
        ParameterCache.load(SmallSeqPaths.dbConfigFile)

        println("Two path methods")
        var graphForPath = BuildGraphFromPathsPlugin()
            .pathMethod("PATH_METHOD:Paths_AllPathsGraph")
            .build()

        var countTaxaInNodes = graphForPath.nodeStream().mapToInt { node -> node.numTaxa() }.sum()
        println("count of taxa by nodes = $countTaxaInNodes")

        var taxaInGraph = graphForPath.taxaInGraph()
        println("List of taxa in graph:")
        taxaInGraph.forEach { println(it.name) }
        println("running split graph should generate 560 * 2 = 1120 nodes")
        CreateGraphUtils.nodesSplitByIndividualTaxa( CreateGraphUtils.addMissingSequenceNodes(graphForPath), 0.99)

        println("Two path methods: PATH_METHOD,FocusRegion:Paths_AllPathsGraph,FocusRegion")
        graphForPath = BuildGraphFromPathsPlugin()
            .pathMethod("PATH_METHOD,FocusRegion:Paths_AllPathsGraph,FocusRegion")
            .build()

        countTaxaInNodes = graphForPath.nodeStream().mapToInt { node -> node.numTaxa() }.sum()
        println("Count of taxa by nodes = $countTaxaInNodes")

        taxaInGraph = graphForPath.taxaInGraph()
        println("List of taxa in graph:")
        taxaInGraph.forEach { println(it.name) }

        println("running split graph should generate 1120 nodes")
        CreateGraphUtils.nodesSplitByIndividualTaxa( CreateGraphUtils.addMissingSequenceNodes(graphForPath), 0.99)

        println("Two path methods: PATH_METHOD,FocusRegion:Paths_AllPathsGraph")
        graphForPath = BuildGraphFromPathsPlugin()
            .pathMethod("PATH_METHOD,FocusRegion:Paths_AllPathsGraph")
            .build()

        countTaxaInNodes = graphForPath.nodeStream().mapToInt { node -> node.numTaxa() }.sum()
        println("Count of taxa by nodes = $countTaxaInNodes")

        println("Four path methods: PATH_METHOD,FocusRegion:PATH_METHOD,FocusComplement:Paths_AllPathsGraph,FocusRegion:Paths_AllPathsGraph,FocusComplement")
        graphForPath = BuildGraphFromPathsPlugin()
            .pathMethod("PATH_METHOD,FocusRegion:PATH_METHOD,FocusComplement:Paths_AllPathsGraph,FocusRegion:Paths_AllPathsGraph,FocusComplement")
            .build()

        countTaxaInNodes = graphForPath.nodeStream().mapToInt { node -> node.numTaxa() }.sum()
        println("Count of taxa by nodes = $countTaxaInNodes")

    }
}

fun main() {
//uncomment one of the following lines to run that test
    BuildGraphFromPathsTest().buildGraphFromPaths()
//    BuildGraphFromPathsTest().buildGraphFromPathsAndTaxa()
//    BuildGraphFromPathsTest().testSplitNodes()
//    BuildGraphFromPathsTest().testMultipleMethods()
}