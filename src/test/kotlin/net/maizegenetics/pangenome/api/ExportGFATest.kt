package net.maizegenetics.pangenome.api

import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.util.LoggingUtils

//change paths as necessary to run this test
val config = "/Users/peterbradbury/temp/phgSmallSeq/data/configSQLite.txt"
val outPath = "/Users/peterbradbury/temp/smallSeqGraph"

fun main() {
    LoggingUtils.setupDebugLogging()
    testExport()
}

fun testExport() {
    WriteGraphAsGFAPlugin(null,false)
            .outPath(outPath)
            .performFunction(DataSet.getDataSet(smallSeqHaplotypeGraph()))
}

fun smallSeqHaplotypeGraph() : HaplotypeGraph {
    return HaplotypeGraphBuilderPlugin(null, false).configFile(config)
            .chromosomes(listOf("1")).methods("CONSENSUS").build()
}
