package net.maizegenetics.pangenome.api

import net.maizegenetics.pangenome.hapCalling.BestHaplotypePathPlugin
import net.maizegenetics.pangenome.hapCalling.ImportDiploidPathPlugin
import net.maizegenetics.pangenome.hapCalling.ImportHaplotypePathFilePlugin
import net.maizegenetics.pangenome.hapCalling.PathsToVCFPlugin
import net.maizegenetics.pangenome.smallseq.RunSmallSeqTestsDocker
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.util.LoggingUtils

class AddPathsToGraphPluginTest {
    val myGraph: HaplotypeGraph

    init {
        myGraph = buildHaplotypeGraph()
    }

    fun buildHaplotypeGraph() : HaplotypeGraph{
        ParameterCache.load(SmallSeqPaths.dbConfigFile)
        val graph = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(SmallSeqPaths.dbConfigFile)
                .includeSequences(false)
                .includeVariantContexts(false)
                .methods("CONSENSUS")
                .build()

        return graph
    }

    fun printStartTaxaCounts() : AddPathsToGraphPluginTest  {
        println("Before adding paths\n--------------------------")
        printHaplotypeTaxaNumbers(myGraph)
        return this
    }

    fun testAddPathsToGraph() : AddPathsToGraphPluginTest  {
        //every range should have 34 taxa (original 6 plus 28 paths)
        val graphWithPaths = AddPathsToGraphPlugin().pathMethod("PATH_METHOD")
                .addPathsToGraph(myGraph)

        println("\nAfter adding paths\n--------------------------")
        printHaplotypeTaxaNumbers(graphWithPaths)

        return this
    }

    fun testBuildGraphFromPaths() : AddPathsToGraphPluginTest  {
        //every range should have 28 taxa
        val graphWithPaths = BuildGraphFromPathsPlugin().pathMethod("PATH_METHOD").build()

        println("\nAfter adding paths\n--------------------------")
        printHaplotypeTaxaNumbers(graphWithPaths)

        return this
    }

    fun testTaxaList() : AddPathsToGraphPluginTest {
        //every node should have three taxa
        println("\nwith taxa list with three taxa\n-------------------")
        val graphWithPaths = AddPathsToGraphPlugin().pathMethod("PATH_METHOD")
                .taxaList(TaxaListBuilder().addAll(mutableListOf("LineA_wgs","LineB_wgs","Ref_wgs")).build())
                .addPathsToGraph(myGraph)
        printHaplotypeTaxaNumbers(graphWithPaths)
        return this
    }

    fun testDiploidPath() : AddPathsToGraphPluginTest {
        //requires running FindBestDiploidPathTest.runSmallSeqTests() first to create a diploid record
        ParameterCache.load(SmallSeqPaths.dbConfigFile)
        val graph = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(SmallSeqPaths.dbConfigFile)
                .includeSequences(false)
                .includeVariantContexts(false)
                .methods("GATK_PIPELINE")
                .build()

        val graphWithPaths = AddPathsToGraphPlugin().pathMethod("PATH_DIPLOID_TEST1")
                .addPathsToGraph(graph)
        println("\nDiploidPathTest\n----------------------")
        printHaplotypeTaxaNumbers(graphWithPaths)
        return this
    }

    private fun printHaplotypeTaxaNumbers(graph : HaplotypeGraph) {
        graph.referenceRangeList().forEach { range ->
            println("$range")
            graph.nodes(range).forEach { node ->
                println("${node.id()}: ${node.numTaxa()} taxa")
            }
        }

    }

    fun testPipelineWithPaths() {
        LoggingUtils.setupDebugLogging()
        ParameterCache.load(SmallSeqPaths.dbConfigFile)
        val graph = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(SmallSeqPaths.dbConfigFile)
                .includeSequences(false)
                .includeVariantContexts(false)
                .methods("CONSENSUS")
                .build()

        val graphWithPaths = AddPathsToGraphPlugin().pathMethod("PATH_METHOD")
                .addPathsToGraph(graph)

        val pathFinder = BestHaplotypePathPlugin(null, false)
        pathFinder.setConfigParameters()
        pathFinder.keyFile(SmallSeqPaths.genotypingPathKeyFile)
                .readMethodName("READS_FASTQ")
                .pathMethodName("PATH_METHOD_ADD_PATHS")
                .minTaxaPerRange(2)
                .minReads(0)
                .performFunction(DataSet.getDataSet(graphWithPaths))

        val outfile = "/Users/peterbradbury/temp/pathTestOut.vcf"
        val graphBuilder = HaplotypeGraphBuilderPlugin(null, false)
        graphBuilder.setConfigParameters()
        val graphDataSetForPath = graphBuilder
                .methods("CONSENSUS")
                .includeSequences(false)
                .includeVariantContexts(true)
                .performFunction(null)
        val paths = ImportDiploidPathPlugin()
                .pathMethodName("PATH_METHOD_ADD_PATHS")
                .performFunction(graphDataSetForPath)
        val vcfPlugin = PathsToVCFPlugin(null, false)
        vcfPlugin.setConfigParameters()
        vcfPlugin.outputFile(outfile)
                .performFunction(paths)


        //added code to test add paths
        try {
            RunSmallSeqTestsDocker.compareResultAndAnswer(false, SmallSeqPaths.answerDir + "SmallSeq.vcf", outfile)
        } catch (e: Exception) {
            e.printStackTrace()
            System.err.println("Consider deleting phgrepository_base from Docker and rebuilding- using docker rmi imageID")
        }


    }

    fun testBuildFromPathsSmallSeq() {
        LoggingUtils.setupDebugLogging()
        ParameterCache.load(SmallSeqPaths.dbConfigFile)

        val graphWithPaths = BuildGraphFromPathsPlugin().pathMethod("PATH_METHOD").build()

        val pathFinder = BestHaplotypePathPlugin(null, false)
        pathFinder.setConfigParameters()
        pathFinder.keyFile(SmallSeqPaths.genotypingPathKeyFile)
                .readMethodName("READS_FASTQ")
                .pathMethodName("PATH_METHOD_FROM_PATHS")
                .minTaxaPerRange(2)
                .minReads(0)
                .performFunction(DataSet.getDataSet(graphWithPaths))

        val outfile = "/Users/peterbradbury/temp/pathTestOut.vcf"
        val graphBuilder = HaplotypeGraphBuilderPlugin(null, false)
        graphBuilder.setConfigParameters()
        val graphDataSetForPath = graphBuilder
                .methods("CONSENSUS")
                .includeSequences(false)
                .includeVariantContexts(true)
                .performFunction(null)
        val paths = ImportDiploidPathPlugin()
                .pathMethodName("PATH_METHOD_FROM_PATHS")
                .performFunction(graphDataSetForPath)
        val vcfPlugin = PathsToVCFPlugin(null, false)
        vcfPlugin.setConfigParameters()
        vcfPlugin.outputFile(outfile)
                .performFunction(paths)


        //added code to test add paths
        try {
            RunSmallSeqTestsDocker.compareResultAndAnswer(false, SmallSeqPaths.answerDir + "SmallSeq.vcf", outfile)
        } catch (e: Exception) {
            e.printStackTrace()
            System.err.println("Consider deleting phgrepository_base from Docker and rebuilding- using docker rmi imageID")
        }


    }

}


fun main() {
    AddPathsToGraphPluginTest()
            .printStartTaxaCounts()
            .testAddPathsToGraph()
            .testTaxaList()
            .testDiploidPath()
            .testBuildGraphFromPaths()
//            .testPipelineWithPaths()
//            .testBuildFromPathsSmallSeq()
}

