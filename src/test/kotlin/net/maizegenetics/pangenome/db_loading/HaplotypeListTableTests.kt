package net.maizegenetics.pangenome.db_loading

import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils.encodeHapidListToByteArray
import net.maizegenetics.util.LoggingUtils
import org.junit.Before
import org.junit.Test
import java.sql.SQLException
import java.sql.Statement
import java.util.*
import java.util.stream.Collectors

/**
 * THis class tests loading data to the haplotype_list table
 * It works on a database created from the smallSeq program but would
 * work on any PHG db that includes the haplotype_list table
 *
 * Before each run of this test, the haplotype_list table  entries
 * should be deleted.
 */
class HaplotypeListTableTests {
    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun testHaplotypeListTableFromPHGdbAccess() {
        // Be sure to delete all haplotype_list entries from the db before running
        // this test !!  ALso note this smallseq db was created before read_mapping
        // was changed to populate the haplotype_list_id value with something other
        // than -1 and before the haplotype_list table was programmatically populated.
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/haplotypeList_table/config_smallSeq.txt"

        val connection  = DBLoadingUtils.connection(configFile, false);
        val phg:PHGDataWriter = PHGdbAccess(connection)
        // Table has: id, list_hash, and a blob
        val hapids1 = listOf(12, 1, 71, 65)
        val hapids2 = listOf(34, 1, 71, 85)
        // THis is the same list as hapids1, but in a different order
        // It should result in the same hash value getting created
        val hapids3 = listOf(1,65,12,71)

        // verify the same hash is created for different
        val hashString1 = hapids1.sorted().map{it.toString()}.joinToString(",")
        val hashMD5_1 = AnchorDataPHG.getChecksumForString(hashString1,"MD5")

        val hashString3 = hapids3.sorted().map{it.toString()}.joinToString(",")
        val hashMD5_3 = AnchorDataPHG.getChecksumForString(hashString3,"MD5")

        assertEquals(hashMD5_1, hashMD5_3)

        // verify this entry does not yet exist
        var testId = phg.getHaplotypeListIDfromHash(hashMD5_1)
        println("values of testId = $testId")
        assertEquals(0,testId)

        // Should return 1
        var haplotypeId = phg.putHalotypeListData(hapids1)
        println("after first insert, haplotypeID = ${haplotypeId}")
        assertEquals(1,haplotypeId)

        haplotypeId = phg.putHalotypeListData(hapids2)
        println("after second insert, haplotypeID = ${haplotypeId}")
        assertEquals(2,haplotypeId)

        // repeat haplotypeId insert for 1 - should return 0 until I fix this
        haplotypeId = phg.putHalotypeListData(hapids1)
        println("after second insert for hapids1, haplotypeID = ${haplotypeId}")
        assertEquals(1,haplotypeId)


    }

    @Test
    fun testHaplotypeListTablePostgresFromPHGdbAccess() {
        // Be sure to delete all haplotype_list entries from the db before running
        // this test !!  You have to delete, recreate the table on subsequent
        // runs.  WIth sqlite, it is enough to delete all table entries.
        // But postgres will NOT reuse the id numbers.  If you add 2 entries,
        // then delete those entries, then add another entry, the id will be 3,
        // not 1.  So either drop/re-create the table after each run, OR  change
        // the haplotypeId that is expected below.

        // I do this manually:  connect to the db:
        // DROP TABLE IF EXISTS haplotype_list;
        // CREATE TABLE haplotype_list(haplotype_list_id SERIAL PRIMARY KEY, list_hash TEXT UNIQUE, hapid_list bytea);

        // This is a duplicate of the test above, but this one goes to a postgres
        // database, the one above tests sqlite

        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/haplotypeList_table/config_smallSeqPostgres.txt"

        val connection  = DBLoadingUtils.connection(configFile, false);
        val phg:PHGDataWriter = PHGdbAccess(connection)
        // Table has: id, list_hash, and a blob
        val hapids1 = listOf(12, 1, 71, 65)
        val hapids2 = listOf(34, 1, 71, 85)
        // THis is the same list as hapids1, but in a different order
        // It should result in the same hash value getting created
        val hapids3 = listOf(1,65,12,71)

        // verify the same hash is created for different
        val hashString1 = hapids1.sorted().map{it.toString()}.joinToString(",")
        val hashMD5_1 = AnchorDataPHG.getChecksumForString(hashString1,"MD5")

        val hashString3 = hapids3.sorted().map{it.toString()}.joinToString(",")
        val hashMD5_3 = AnchorDataPHG.getChecksumForString(hashString3,"MD5")

        assertEquals(hashMD5_1, hashMD5_3)

        // verify this entry does not yet exist
        var testId = phg.getHaplotypeListIDfromHash(hashMD5_1)
        println("values of testId = $testId")
        assertEquals(0,testId)

        // Should return 1 - unless you've already added/deleted, then
        // it returns the next number in line, even though there may only
        // be 1 entry in the db.  That is postgres.
        var haplotypeId = phg.putHalotypeListData(hapids1)
        println("after first insert, haplotypeID = ${haplotypeId}")
        assertEquals(1,haplotypeId)

        haplotypeId = phg.putHalotypeListData(hapids2)
        println("after second insert, haplotypeID = ${haplotypeId}")
        assertEquals(2,haplotypeId)

        // repeat haplotypeId insert for 1 - should return 0 until I fix this
        haplotypeId = phg.putHalotypeListData(hapids1)
        println("after second insert for hapids1, haplotypeID = ${haplotypeId}")
        assertEquals(1,haplotypeId)


    }
    @Test
    fun testgetReadMappingsAndgetHaplistForId() {
        // for testing - I'm running this directly after the above test,
        // and after I've manually updated read_mapping 10 to have haplotype_list_id=1
        // and read_mapping entry 20 to have haplotype_list_id=2
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/haplotypeList_table/config_smallSeq.txt"

        val connection  = DBLoadingUtils.connection(configFile, false);
        val phg:PHGDataWriter = PHGdbAccess(connection)

        var mappingWithHapids = phg.getReadMappingsForId(10)
        var haplotypeListId = mappingWithHapids.second
        println("haplotypeListId=${haplotypeListId}")
        var hapids = phg.getHaplotypeList(haplotypeListId)
        println("hapids are:")
        hapids.forEach {
            println(it)
        }
        assertTrue(hapids.size == 4)
        assertTrue(hapids.contains(65))

        mappingWithHapids = phg.getReadMappingsForId(1)
        haplotypeListId = mappingWithHapids.second
        println("haplotypeListId=${haplotypeListId}")
        assertEquals(-1,haplotypeListId)

    }
}