
package net.maizegenetics.pangenome.db_loading

import junit.framework.Assert.*
import net.maizegenetics.util.LoggingUtils
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GZipCompressionTest {

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun compressAndUncompress() {
        val origString = "CGCAGCCGGTCGACCAAAGGCACCTCTATGGGAGGTGGGGGTGAAAGGTGGGTGCACACCTTGCCGACCACCAGGCGAAACTCCTGGAGCTCAGCGCTACGCTCAGAGAGCACAACCGCCGCCTCGTCCGTCGCAGTAACGGCACCTTCGCGGGCAAGAGAAAGCTCTCGCATCCTGGCATCCGCCGCTGCCTTCGCCTCCTCCACCGATCTCTTGGCCTCCTCGGCTTGAGAAAACATCTTCTGGAGCTCTAAAAAGAGAGGAGGGCTAAAACGACCACCGAGAGACAGGTGATAAATGGCATAAGGATAAGGAGAGCGATCCACCTCCGGTGGCACCACATTCCCTCGAGAACTCTGCGTCCGCACTTTCCAGACGAGCGTTAACCTCACCAAGGGACGACTCGGCGTCCTAAGCTTGAAGACTCGCCTCGCGGAGGGGCCCCTGGAGCTGGGTCTCCTTCTCCCGGAGCCGGTCGGCTTCAACCCGAGCACCCTCGCTCTCGTAGTGTAGGTGGTCCGCCTCGCTATCAAGCCGCTCGATCTTGGAACGGAGTTCGGCAAGCTCACGTTGCTTCGCCTCAGAATGAGCGGCTTCCACACGAAGCTGCTCCGCCTCCCACTGAGCGGACAGCAGTGCCTCTGCCCTGGCCATCGACCGCGCAAAAAGACGCTACGAACAGAAAATGAGGCGATCAGCTCGAATGATGGTAAGAAAACAAAGAGAAAGCACGGCTACAAAGTGACATTCACCTGGCCGAAAGGGATGAAATCCTGCGTCATCGATGCCCGGGCAATTGAAATCGCCTCCCCTAAGCTACCGAGCGCTTCCTCGATAGTGCCCAGGCTCAGATCTCCTCGGGACGAGGCCGATTCAACCCGCCTCCAATCCTCCGCGTCCTCCTTCGGTGCCTTTGGAGGAGGGGAGGAGTGACCCCCCACGGCGCTCCCCCGACAGCAAAGGGACCACGACCCCTGCTGGGGGAGAGGAAGCGGCGACCGGCGATCGCTGGGTCAGGAGCTCGGCGGCCCCCAGGGGCGGCGCCGCCTTGAAATGATCGCCTACCCCAGCAGCGCCCAGGGCTGGCGGTGGCAGCAGTAGGGCCAGAGCCGCCGCCTCCACCGATGAAGCTCCGCCAAGGGGAGCCTCTCCGACCGCTGGAACCTCCCTTAGGTCGGCCGTGGGTGATGTCACCATGATAAGGCACGCGTCAGGGGTCGATGCCGTCGGGGCCTCTGGGGCGGTCAGGGAGGCCAGCACAGGTGGCAGGGTCGTC"

        // compress it
        val compressedString = GZipCompression.compress(origString)

        // Verify we have fewer bytes when compressed
        println("Size of original string: " + origString.length + ", number bytes in compressed string: " + compressedString!!.size)
        assertTrue(compressedString.size < origString.length)

        // uncompress it
        val uncompressed = GZipCompression.decompress(compressedString)

        // Verify the 2 are the same
        assertTrue(origString == uncompressed)
    }

    @Test
    fun testDecompressUnCompressedString() {
        val origString = "ACGT"

        // try to decompress a string that was no compressed
        val uncompressedString = GZipCompression.decompress(origString.toByteArray())
        println("origString=" + origString + ", uncompressed=" + uncompressedString);
		
        // The strings should match
        assertTrue(origString == uncompressedString)
    }

}