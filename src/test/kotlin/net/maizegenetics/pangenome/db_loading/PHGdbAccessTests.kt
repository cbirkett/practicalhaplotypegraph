package net.maizegenetics.pangenome.db_loading

import net.maizegenetics.util.LoggingUtils
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class PHGdbAccessTests {

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun testAddTaxaGroup() {
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/taxaGroupTables/testing/config_smallSeq.txt"

        val connection  = DBLoadingUtils.connection(configFile, false);
        val phg = PHGdbAccess(connection)

        val existingTaxaList = phg.getDbTaxaNames()

        assertTrue(existingTaxaList.contains("LineA_Assembly"))
        assertTrue(existingTaxaList.contains("LineA"))
        assertFalse(existingTaxaList.contains("Lynnjo"))

        val groupName = "lynn_test"
        val grpID = phg.putTaxaGroupName(groupName)
        println("grpID for Lynn_test is ${grpID}")

        val taxaList = "LineA_Assembly,LineB_Assembly".split(",").toList()

        phg.putTaxaTaxaGroups(groupName,taxaList)

        val returnedList = phg.getTaxaForTaxaGroup(groupName)
        assertTrue(returnedList.contains("LineA_Assembly"))
        assertEquals(2,returnedList.size)

        // Test getALlTaxaGroupNames() - this db is used in other tests,
        // so could be more than 1 group name present, but the one added
        // above should definitely be included.
        val groupNames = phg.getAllTaxaGroupNames();
        println("groupNames size = ${groupNames.size}")
        assertTrue(groupNames.contains("lynn_test"))

        phg.close()
    }

    @Test
    fun testGetReadMappingIdsForTaxaMethod() {
        // Before running, be sure to copy <..>/testing/phgSmallSeq.db to <..>/testing/phgSmallSeq_forDeleteTesting.db
        // to ensure all are there.
        // see full path from the config file
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/config_smallSeq.txt"
        val connection = DBLoadingUtils.connection(configFile, false);
        val phg = PHGdbAccess(connection)

        val taxaList = mutableListOf<String>()
        // Tests when method is present, but no taxa list
        var rmIds = phg.getReadMappingIdsForTaxaMethod(taxaList,"READS_FASTQ")
        println("number readMapping ids returned for method READS_FASTQ: " + rmIds.size)
        assertEquals(28,rmIds.size)

        rmIds = phg.getReadMappingIdsForTaxaMethod(taxaList,"GATK_PIPELINE")
        println("number readMapping ids returned for method GATK_PIPELINE: " + rmIds.size)
        assertEquals(0,rmIds.size)

        taxaList.add("RecLineB1RefA1gco4_wgs")
        taxaList.add("RecLineB1RefA1gco4_gbs")
        taxaList.add("LineB1_wgs")
        taxaList.add("LineA_wgs")
        rmIds = phg.getReadMappingIdsForTaxaMethod(taxaList,"READS_FASTQ")
        println("number readMapping ids returned for 4 taxa all with method READS_FASTQ: " + rmIds.size)
        assertEquals(4,rmIds.size)

        rmIds = phg.getReadMappingIdsForTaxaMethod(taxaList,"GATK_PIPELINE")
        println("number readMapping ids returned for 4 taxa, none with method GATK_PIPELIN: " + rmIds.size)
        assertEquals(0,rmIds.size)

        taxaList.add("LineB1")
        taxaList.add("LineA")

        println("\nSize of taxaList: ${taxaList.size}")
        rmIds = phg.getReadMappingIdsForTaxaMethod(taxaList,"READS_FASTQ")
        println("number readMapping ids returned for 6 taxa, 4 with method READS_FASTQ: " + rmIds.size)
        assertEquals(4,rmIds.size)
    }

    @Test
    fun testDeletePathsByMethodID() {
        // Test the PHGdbAccess.deletePathsByMethod() method

        // on each running, you want to re-copy the original db so the paths and
        // read_mappings appear again.
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/config_smallSeq.txt"
        val connection = DBLoadingUtils.connection(configFile, false);
        val phg = PHGdbAccess(connection)

        // This db has paths for wgs, assembly and those created from read mappings.
        // I'm going to delete based on the path method for "path_method", which should also delete
        // from the read_mapping table.

        phg.deletePaths("PATH_METHOD", null)

        // Manually verify paths table no longer has entries with path_id matching method "PATH_METHOD"
        // Manually verify read_mapping_paths table has no remaining entries (because all previously had path method)
        println("Finished test with with no errors! - manually check your db now")

    }

    @Test
    fun testDeleteMethodByName() {
        // test adding/deleting a method
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/config_smallSeq.txt"
        val connection = DBLoadingUtils.connection(configFile, false);
        val phg = PHGdbAccess(connection)

        val methodDesc = HashMap<String,String>()
        methodDesc.put("yes","yes")
        phg.putMethod("lynn_test2", DBLoadingUtils.MethodType.PATHS, methodDesc);

        var methodId = phg.getMethodIdFromName("lynn_test2")
        println("after adding, methodId = ${methodId}")
        assertTrue(methodId > 0)

        phg.deleteMethodByName("lynn_test2")
        methodId = phg.getMethodIdFromName("lynn_test2")
        println("after deleting, methodId = ${methodId}")

        assertTrue(methodId < 1)


    }
}