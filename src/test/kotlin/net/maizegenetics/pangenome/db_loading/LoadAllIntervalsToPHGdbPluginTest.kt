package net.maizegenetics.pangenome.db_loading

import net.maizegenetics.util.LoggingUtils
import org.junit.Test

class LoadAllIntervalsToPHGdbPluginTest {

    @Test
    fun testLoadAllIntervalsToPHGdbPluginTest() {
        LoggingUtils.setupDebugLogging()
        var refGenome = "/Users/lcj34/notes_files/phg_2018/genomes/Zm-B73-REFERENCE-NAM-5.0.fa"
        var rangeFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/testing/intervalsFromCreateValidIntervalsFile.txt"
        //var rangeFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/testing/intervals_noInteranchorsSORTED.txt"
        //var rangeFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/testing/createdIntervalsFile_closedFix_rework_3methods.txt"

        var genomeDataFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/testing/B73Ref_load_data.txt"
        //var configFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/testing/configSQLite.txt"
        //var outputDir = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/testing/"
        var configFile = "/Users/lcj34/notes_files/phg_2018/debug/ref_variants_fix/configSQLite.txt"
        var outputDir = "/Users/lcj34/notes_files/phg_2018/debug/ref_variants_fix/"

        val refServerPath = "irods:/BL/refServer"
        val dbConnectDS = GetDBConnectionPlugin(null, false)
                .configFile(configFile)
                .createNew(true)
                .performFunction(null)

        LoadAllIntervalsToPHGdbPlugin(null,false)
                .anchors(rangeFile)
                .refGenome(refGenome)
                .genomeData(genomeDataFile)
                .outputDir(outputDir)
                .refServerPath(refServerPath)
                .performFunction(dbConnectDS);
    }
}
