package net.maizegenetics.pangenome.db_loading

import net.maizegenetics.plugindef.ParameterCache
import org.junit.Test

class AddTaxaToTaxaGroupPluginTest {
    @Test
    fun testAddTaxaToTaxaGroupPlugin() {

        // This test is run, then manually checked that the values were added to the 2 db tables
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/taxaGroupTables/testing/config_smallSeq.txt"
        val taxaList = "LineA_Assembly,LineB_Assembly"
        val groupName = "lynn_Assembly"

        ParameterCache.load(configFile)
        println("Running first AddTaxaToTaxaGroupPlugin")
        AddTaxaToTaxaGroupPlugin(null,false)
                .groupName(groupName)
                .taxa(taxaList)
                .performFunction(null)

        // run a second time, this verifies duplicate entries are handled correctly
        println("Running second AddTaxaToTaxaGroupPlugin")
        AddTaxaToTaxaGroupPlugin(null,false)
                .groupName(groupName)
                .taxa(taxaList)
                .performFunction(null)

        println("Finished with test!!")
    }
}