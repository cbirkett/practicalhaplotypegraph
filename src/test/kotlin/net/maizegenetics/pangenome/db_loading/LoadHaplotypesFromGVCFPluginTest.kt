package net.maizegenetics.pangenome.db_loading

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.fastaExtraction.GVCFSequence
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.File
import java.lang.IllegalArgumentException
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDateTime
import kotlin.test.assertFailsWith

/**
 * Unit test class to test uploading GVCF files created by SmallSeqTests using LoadHaplotypesFromGVCFPlugin
 *
 * It currently will test
 * -full upload of the SmallSeq GVCFs
 * -Checking column names for misspellings.
 */
class LoadHaplotypesFromGVCFPluginTest {

    //get the current time for the method name
    val currentLocalDateTime = LocalDateTime.now()

    //Set up the global variables
    val workDir = SmallSeqPaths.baseDir
    val configFile = SmallSeqPaths.dbConfigFile
    val gvcfDir = SmallSeqPaths.gvcfDir
    val bedFile = SmallSeqPaths.anchorBedFile
    val keyFile = SmallSeqPaths.keyFile
    val refFile = SmallSeqPaths.refGenomePath
    var hapMethodName = ""
    val expectedNumberOfNodes = 60  //TODO this needs to be changed if

    //This list allows us to make and alter the column names in an automated fashion
    val keyFileHeaders = listOf("type","sample_name","sample_description","files","chrPhased","genePhased","phasingConf")

    //Global Mapping Taxon->Sequence for testing purposes:
    val gvcfSequenceMap = mutableMapOf<String, GenomeSequence>()

    /**
     * Setup the logging
     *
     * Create Test Genome Files
     *
     * Create Test KeyFile
     */
    @Before
    fun setup() {
        hapMethodName = "GATK_KEYFILE_METHOD_${currentLocalDateTime.toString().replace(":","_").replace(",","_")}"
        LoggingUtils.setupDebugLogging()
        createTestKeyfile(keyFile,keyFileHeaders,gvcfDir)
    }

    /**
     * Create a KeyFile based on the Small Seq tests
     */
    fun createTestKeyfile(fileName : String, headerColumnNames : List<String>, gvcfDirectory : String) {

        Utils.getBufferedWriter(fileName).use{output ->
            //Get the list of gvcf file names
            val gvcfFiles = File(gvcfDirectory).listFiles()
                    .filter { it.toString().endsWith("_filtered.g.vcf.gz") }
                    .map { it.name }

            //Keyfile format:
            //sample_name\tsample_description\tfiles\tchrPhased\tgenePhased\tphasingConf\n
            output.write(headerColumnNames.joinToString("\t")+"\n")
            gvcfFiles.forEach{
                val taxonName = it.split("_")[0]
                gvcfSequenceMap[taxonName] = GVCFSequence.instance(refFile,gvcfDirectory+it)
                output.write("GVCF\t${taxonName}\t${taxonName}_FromGVCF\t${it}\ttrue\ttrue\t1.0\n")
            }
        }
    }

    @Test
    fun testQueueSizeZeroOrBelow() {
        ParameterCache.load(configFile)

        try {
            LoadHaplotypesFromGVCFPlugin(null,false)
                    .wGSKeyFile(keyFile)
                    .bedFile(bedFile)
                    .gVCFDirectory(gvcfDir)
                    .reference(refFile)
                    .method(hapMethodName)
                    .methodDescription("KeyFile Based Upload")
                    .queueSize(-1)
                    .processData(null)
            Assert.fail("LoadHaplotypesFromGVCFPlugin should throw an exception. Due to negative queueSize")
        }
        catch (exc : Exception) {
            if(exc.cause !is IllegalArgumentException) {
                Assert.fail("LoadHaplotypesFromGVCFPlugin threw an exception, but should throw an IllegalArgumentException. Due to negative queueSize.\n" +
                        "Actual thrown exception: ${exc.message}")
            }
        }

        try {
            LoadHaplotypesFromGVCFPlugin(null,false)
                    .wGSKeyFile(keyFile)
                    .bedFile(bedFile)
                    .gVCFDirectory(gvcfDir)
                    .reference(refFile)
                    .method(hapMethodName)
                    .methodDescription("KeyFile Based Upload")
                    .queueSize(0)
                    .processData(null)
            Assert.fail("LoadHaplotypesFromGVCFPlugin should throw an exception. Due to 0 queueSize")
        }
        catch (exc : Exception) {
            if(exc.cause !is IllegalArgumentException) {
                Assert.fail("LoadHaplotypesFromGVCFPlugin threw an exception, but should throw an IllegalArgumentException. Due to 0 queueSize.\n" +
                        "Actual thrown exception: ${exc.message}")
            }
        }


    }

    //Method to do a full upload using the keyfile
    @Test
    fun testGVCFFullUpload() {
        //Load the config file
        ParameterCache.load(configFile)

        //Run The plugin
        LoadHaplotypesFromGVCFPlugin(null,false)
                .wGSKeyFile(keyFile)
                .bedFile(bedFile)
                .gVCFDirectory(gvcfDir)
                .reference(refFile)
                .method(hapMethodName)
                .methodDescription("KeyFile Based Upload")
                .performFunction(null)

        val graph = HaplotypeGraphBuilderPlugin(null,false)
                .includeSequences(true)
                .methods(hapMethodName)
                .configFile(configFile)
                .build()

        //Check to see if we have the right number of nodes
        Assert.assertTrue(graph.numberOfNodes()==expectedNumberOfNodes)
        val taxonToNodesMap = ArrayListMultimap.create<String,HaplotypeNode>()

        graph.referenceRanges()
                .flatMap { graph.nodes(it) }
                .flatMap { node ->
                    node.taxaList().map { taxon -> taxon.name }
                            .map { taxon -> Pair(taxon,node) }
                }
                .forEach { taxonPair -> taxonToNodesMap.put(taxonPair.first,taxonPair.second) }

        //Check the sequences in the graph.
        verifySequenceInGraph(taxonToNodesMap)
    }

    /**
     * Function to test the key file column names are correct.
     *
     * Basically add a _1 to each column one at a time and make sure it throws an IllegalStateException
     */
    @Test
    fun verifyKeyFileCorrectness() {
        ParameterCache.load(configFile)

        keyFileHeaders.forEach { currentHeaderToReplace ->
            //Get rid of the sample_description as that one can be empty
            if(currentHeaderToReplace != "sample_description") {
                val newHeaders = keyFileHeaders.map { columnName ->
                    if (currentHeaderToReplace == columnName) {
                        columnName + "_1"
                    } else {
                        columnName
                    }
                }

                //Make a temp Key file
                val tempKeyFile = "${workDir}keyfile_${currentHeaderToReplace}.txt"
                createTestKeyfile(tempKeyFile,newHeaders,gvcfDir)

                //Assert it fails
                assertFailsWith<IllegalStateException> {
                    println("Should Fail.")
                    LoadHaplotypesFromGVCFPlugin(null, false)
                            .wGSKeyFile(tempKeyFile)
                            .bedFile(bedFile)
                            .gVCFDirectory(gvcfDir)
                            .reference(refFile)
                            .method(hapMethodName)
                            .methodDescription("KeyFile Based Upload")
                            .processData(null) //Use processData as we want it to throw an exception.
                }
                //Delete the file
                Files.delete(Paths.get(tempKeyFile))
            }
        }
    }

    //TODO Speed test between single and multithreaded version


    /**
     * Test to make sure the existing sequences in the DB actually match the gvcfs.
     * This is different than testGVCFFullUpload() where we try to upload then make sure our uploads are correct.
     */
    @Test
    fun verifyExistingSequence() {
        ParameterCache.load(configFile)

        val graph = HaplotypeGraphBuilderPlugin(null,false)
                .includeSequences(true)
                .methods("GATK_PIPELINE")
                .configFile(configFile)
                .build()


        val taxonToNodesMap = ArrayListMultimap.create<String,HaplotypeNode>()

        graph.referenceRanges()
                .flatMap { graph.nodes(it) }
                .flatMap { node ->
                    node.taxaList().map { taxon -> taxon.name }
                            .map { taxon -> Pair(taxon,node) }
                }
                .forEach { taxonPair -> taxonToNodesMap.put(taxonPair.first,taxonPair.second) }

        verifySequenceInGraph(taxonToNodesMap)
    }


    /**
     * Function to verify that the sequences are all identical between GVCF and graph created sequences.
     * If this failed, it means something creating the sequences for the graph nodes has changed.
     */
    fun verifySequenceInGraph(taxonToNodesMap : Multimap<String, HaplotypeNode>) {
        taxonToNodesMap.keySet().forEach { taxonName ->
            taxonToNodesMap.get(taxonName)
                    .forEach { node ->
                        val refRange = node.referenceRange()
                        val chr = refRange.chromosome()
                        val start = refRange.start()
                        val end = refRange.end()

                        val graphSequence = node.haplotypeSequence().sequence()

                        val gvcfSequence = gvcfSequenceMap[taxonName]?.genotypeAsString(chr,start,end)?:""

                        Assert.assertEquals("GVCF and Graph Sequence do not match: ${taxonName} ${chr.name}:${start}-${end}\nGVCFSequence:\n${gvcfSequence}\nGraph:\n${graphSequence}",gvcfSequence,graphSequence)
                    }


        }
    }
}