package net.maizegenetics.pangenome.db_loading

import net.maizegenetics.util.LoggingUtils
import org.junit.Test

class CreateValidIntervalsFilePluginTest {

    @Test
    fun testCreateValidIntervalsFileTest() {
        LoggingUtils.setupDebugLogging()


        //var intervalsFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/b73v5_intervals.bed"
        // THis file contains the column for "type", though it isn't named.  It has user-defined regions with a name fro the group
        //var intervalsFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/b73v5_intervals_rework.txt"
        var intervalsFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/b73v5_intervals_3cols.txt"
        //var intervalsFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/b73v5_intervals_blank4thGene5th.txt"
       // var intervalsFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/b73v5_intervals_withInterAnchors.txt"
        //var outputFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/testing/createdIntervalsFile_closedFix.txt"
        var outputFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/testing/createdIntervalsFile_userDefinedRegionParam.txt"

        var refGenome = "/Users/lcj34/notes_files/phg_2018/genomes/Zm-B73-REFERENCE-NAM-5.0.fa"

        // LCJ - see what happens when all regions are defined - shdould be identical output to input
        CreateValidIntervalsFilePlugin(null, false)
                .generatedFile(outputFile)
                .referenceFasta(refGenome)
                .intervalsFile(intervalsFile)
                .mergeOverlaps(true)
                .userRegionsGroupName("HopScotchGroup")
                .otherRegionsGroupName("LynnInterRegion")
                .performFunction(null)

        outputFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/testing/intervals_noInterAnchors_closedFix.txt"
//        CreateValidIntervalsFilePlugin(null, false)
//                .generatedFile(outputFile)
//                .referenceFasta(refGenome)
//                .intervalsFile(intervalsFile)
//                .mergeOverlaps(true)
//                .performFunction(null)
        println("Finished")
    }
}