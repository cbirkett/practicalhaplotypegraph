package net.maizegenetics.pangenome.db_loading

import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import org.junit.Test
import java.sql.Connection
import java.sql.SQLException
import kotlin.test.assertTrue

class ChangeDBMethodTypePluginTest {
    @Test
    fun testChangeDBMethodPlugin() {
        LoggingUtils.setupDebugLogging()

        // This test is run, then manually checked that the values were added to the 2 db tables
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/taxaGroupTables/testing/config_smallSeq.txt"
        val methodName = "mummer4_PATH"
        val newType = DBLoadingUtils.MethodType.TEST_PATHS.value
        val oldType = DBLoadingUtils.MethodType.PATHS.value

        ParameterCache.load(configFile)

        val dbConnectDS = GetDBConnectionPlugin(null, false)
            .configFile(configFile)
            .createNew(false) // make sure this is false!! don't want to overwrite the db
            .performFunction(null)

        // verify the current value of the method type
        try {
            val query = "SELECT method_type from methods where name='${methodName}';"
            val connection = dbConnectDS.getData(0).data as Connection
            var origType = 0
            connection.createStatement().executeQuery(query).use { rs ->

                if (rs.next()) {
                    origType =rs.getInt(1)
                    println("origType=$origType")
                }
                assertTrue(origType==oldType)
            }

        } catch (sqle: SQLException) {
            throw IllegalStateException("error querying db: ${sqle.message}")
        }

        ChangeDBMethodTypePlugin(null,false)
            .methodName(methodName)
            .methodType(newType)
            .performFunction(dbConnectDS)

        // verify the changed value of the method type
        try {
            val query = "SELECT method_type from methods where name='${methodName}';"
            val connection = dbConnectDS.getData(0).data as Connection
            var changedType = 0
            connection.createStatement().executeQuery(query).use { rs ->

                if (rs.next()) {
                    changedType =rs.getInt(1)
                    println("changeType=$changedType")
                }
                assertTrue(changedType==newType)
            }

        } catch (sqle: SQLException) {
            throw IllegalStateException("error querying db: ${sqle.message}")
        }

        // Change it back so can be tested again

        ChangeDBMethodTypePlugin(null,false)
            .methodName(methodName)
            .methodType(oldType)
            .performFunction(dbConnectDS)

    }
}