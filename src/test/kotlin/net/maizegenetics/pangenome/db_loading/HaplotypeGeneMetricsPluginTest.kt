package net.maizegenetics.pangenome.db_loading

import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.GenomeSequenceBuilder
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.Test
import java.util.*

class HaplotypeGeneMetricsPluginTest {

    @Test
    fun testHaplotypeGeneMetricsPlugin() {
        LoggingUtils.setupDebugLogging()

        println("Running HaplotypeGeneMetricsPlugin")
        var inputDir = "/Users/lcj34/notes_files/phg_2018/debug/NAM_genes/genes_by_haplotype"
        var refName = "B73Ref"
        var refRangeFile = "/Users/lcj34/notes_files/phg_2018/debug/NAM_genes/genes_by_haplotype/reference_ranges.csv"
        var outputFile = "/Users/lcj34/notes_files/phg_2018/debug/NAM_genes/genes_by_haplotype/geneWithData2_counts.txt"

        HaplotypeGeneMetricsPlugin(null, false)
                .inputDir(inputDir)
                .refName(refName)
                .refRangeFile(refRangeFile)
                .outputFile(outputFile)
                .performFunction(null)
        println("Finished")
    }

    @Test
    fun createFastaForBlast() {
        // this method created to take sequence from low hit ref ranges
        // identified via the HaplotypeGeneMetricsPlugin, and create a
        // fasta from it to blast against specific assemblies which didn't
        // show alignment in PHG via mummer4

        var refFasta = "/Users/lcj34/notes_files/phg_2018/genomes/Zm-B73-REFERENCE-NAM-5.0.fa"
        val refRangeList: List<Int> = Arrays.asList(1332,1437,1544,1602,1960, 5532,5533,5648,6049,6144)
        var refRangeFile = "/Users/lcj34/notes_files/phg_2018/debug/NAM_genes/genes_by_haplotype/reference_ranges.csv"
        var outputFasta = "/Users/lcj34/notes_files/phg_2018/debug/NAM_genes/genes_by_haplotype/refRangeBadHitFasta_chr1.fa"

        // read refRangeFile into a map
        // read the reference range file:
        var refRangeMap = mutableMapOf<Int,String>()
        println("Befing read refRangeFile")
        Utils.getBufferedReader(refRangeFile).readLines()
                .forEach { line ->

                    var cIndex1 = line.indexOf(",");
                    val rangeId = line.substring(0,cIndex1).toInt()
                    val data = line.substring(cIndex1+1)

                    refRangeMap.put(rangeId,data)
                }
        // create GenomeSequenceBuilder for pulling ref sequence
        val refSeq = GenomeSequenceBuilder.instance(refFasta)
        var bw = Utils.getBufferedWriter(outputFasta)
        for (rangeID in refRangeList) {
            var refData = refRangeMap.get(rangeID)
            if (!refData.isNullOrBlank() ) {
                var cIndex1 = refData.indexOf(",");
                var cIndex2 = refData.indexOf(",", cIndex1 + 1);
                var chrom = refData.substring(0, cIndex1)
                var start = refData.substring(cIndex1 + 1, cIndex2).toInt()
                var end = refData.substring(cIndex2 + 1).toInt()
                var idSB = StringBuilder()
                idSB.append(">").append(chrom).append("_").append(start).append("_").append(end).append("\n")
                var seq = refSeq.genotypeAsString(Chromosome.instance(chrom),start,end)
                bw.write(idSB.toString())
                bw.write(seq)
                bw.write("\n")
            }
        }
        bw.close()
        println("Finished!!")
    }

}