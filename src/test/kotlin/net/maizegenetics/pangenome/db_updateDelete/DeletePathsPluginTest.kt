package net.maizegenetics.pangenome.db_updateDelete

import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.util.LoggingUtils
import org.junit.Before
import org.junit.Test

class DeletePathsPluginTest {
    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun deletePathsPluginTestNoMethod() {
        // Be sure to re-copy the smallSeq db before running the test
        // as entries may have been deleted from previous test runs

        // THis test will fail as method is required
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/config_smallSeq.txt"
        val method = "GATK_PIPELINE_PATH"

        // NOTE:  Using a comma-separated list here doesn't work, but does work from the command line, e.g.
        //  ~lcj34/git/tassel-5-standalone/run_pipeline.pl -debug -configParameters config_smallSeq.txt -DeletePathsPlugin -taxa LineB1,LineB -methodName GATK_PIPELINE_PATH -endPlugin
        val taxa = "LineB1,LineB"

        // taxaFile also doesn't work here, but works from the command line
        val taxaFile="/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/taxaFile.txt"

        var taxaBuilder =  TaxaListBuilder();
        //these do not have entries in the read_mapping_table, so only paths deleted
        taxaBuilder.add("LineB1");
        taxaBuilder.add("LineB")
        val taxaList = taxaBuilder.build()

        val time = System.nanoTime()
        ParameterCache.load(configFile)
        DeletePathsPlugin(null,false)
                //.methodName(method)
                .taxa(taxaList)
                .performFunction(null);
        val endingTime = (System.nanoTime() - time)/1e9
        println("\nfinished deletePathsPluginTestNoMethod in ${endingTime} seconds")

    }

    @Test
    fun deletePathsPluginTestNoTaxa() {
        // Be sure to re-copy the smallSeq db before running the test
        // as entries may have been deleted from previous test runs
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/config_smallSeq.txt"
        val method = "PATH_METHOD"

        // LCJ - REMOVE or COMMENT OUT THESE LINES _ THEY HIT
        // the ACUTAL postgres NAM DB !!!
//        val configFile = "/Users/lcj34/notes_files/phg_2018/debug/dc01_NAM_4redos/config_dc01.txt"
//        val method = "B73xMo18W_path_method"
        // END LCJ REMOVE

        val time = System.nanoTime()
        ParameterCache.load(configFile)
        DeletePathsPlugin(null,false)
                .methodName(method)
                .performFunction(null);
        val endingTime = (System.nanoTime() - time)/1e9
        println("\nfinished deletePathsPluginTestNoTaxa in ${endingTime} seconds")
    }

    @Test
    fun deletePathsPluginTestTaxaAndMethod() {
        // Be sure to re-copy the smallSeq db before running the test
        // as entries may have been deleted from previous test runs
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/config_smallSeq.txt"
        val method = "GATK_PIPELINE_PATH"

        var taxaBuilder =  TaxaListBuilder();
        //these do not have entries in the read_mapping_table, so only paths deleted
        taxaBuilder.add("LineB1");
        taxaBuilder.add("LineB")
        val taxaList = taxaBuilder.build()

        val time = System.nanoTime()
        ParameterCache.load(configFile)
        DeletePathsPlugin(null,false)
                .methodName(method)
                .taxa(taxaList)
                .performFunction(null);
        val endingTime = (System.nanoTime() - time)/1e9
        println("\nfinished deletePathsPluginTestTaxaAndMethod in ${endingTime} seconds")
    }

    @Test
    fun deletePathsPluginTestTaxaWrongMethod() {
        // Be sure to re-copy the smallSeq db before running the test
        // as entries may have been deleted from previous test runs
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/config_smallSeq.txt"
        val method = "mummer4"

        var taxaBuilder =  TaxaListBuilder();
        //these do not have entries in the read_mapping_table, so only paths deleted
        taxaBuilder.add("LineB1");
        taxaBuilder.add("LineB")
        val taxaList = taxaBuilder.build()

        val time = System.nanoTime()
        ParameterCache.load(configFile)
        DeletePathsPlugin(null,false)
                .methodName(method)
                .taxa(taxaList)
                .performFunction(null);
        val endingTime = (System.nanoTime() - time)/1e9
        println("\nfinished deletePathsPluginTestTaxaWrongMethod in ${endingTime} seconds")
    }

    @Test
    fun deletePathsPluginTestTaxaAndKeyFile() {
        // Be sure to re-copy the smallSeq db before running the test
        // as entries may have been deleted from previous test runs
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/config_smallSeq.txt"
        val method = "mummer4"

        val keyFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/genotypingKeyFile.txt"
        var taxaBuilder =  TaxaListBuilder();

        taxaBuilder.add("LineB1");
        taxaBuilder.add("LineB")
        val taxaList = taxaBuilder.build()

        // Should throw an exception  - can't specify both taxa and keyFile
        val time = System.nanoTime()
        ParameterCache.load(configFile)
        DeletePathsPlugin(null,false)
                .methodName(method)
                .taxa(taxaList)
                .keyFile(keyFile)
                .performFunction(null);
        val endingTime = (System.nanoTime() - time)/1e9
        println("\nfinished deletePathsPluginTestTaxaWrongMethod in ${endingTime} seconds")
    }

    @Test
    fun deletePathsPluginTestKeyFile() {
        // Be sure to re-copy the smallSeq db before running the test
        // as entries may have been deleted from previous test runs
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/config_smallSeq.txt"
        val method = "PATH_METHOD"

        val keyFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/genotypingKeyFile.txt"

        // Should throw an exception  - can't specify both taxa and keyFile
        val time = System.nanoTime()
        ParameterCache.load(configFile)
        DeletePathsPlugin(null,false)
                .methodName(method)
                .keyFile(keyFile)
                .performFunction(null);
        val endingTime = (System.nanoTime() - time)/1e9
        println("\nfinished deletePathsPluginTestTaxaWrongMethod in ${endingTime} seconds")
    }
}