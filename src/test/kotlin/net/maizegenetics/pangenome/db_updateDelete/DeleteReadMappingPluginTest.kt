package net.maizegenetics.pangenome.db_updateDelete

import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import org.junit.Before
import org.junit.Test

class DeleteReadMappingPluginTest {
    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun deleteReadMappingWithMethodAndKeyfile() {
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/config_smallSeq.txt"
        //val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/configDC01.txt"
        val keyFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/genotypingKeyFile.txt"
       // val keyFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/genotypingKeyFile_ALL.txt"
        val method = "READS_FASTQ"

        // genoids are 14,15,16,17,18,19,20,21 for the keyFile taxa used in testing
        // LineA1, LineA, LineB1, LineB for wgs and gbs only

        // WHen delete method is true, it should fail because the keyfile does
        // note contain all the read_mappings for this method

        val time = System.nanoTime()
        ParameterCache.load(configFile)
        DeleteReadMappingPlugin(null,false)
                .methodName(method)
                .keyFile(keyFile)
                .deleteMethod(false) // false because we didn't delete all read_mappings with this method
                .performFunction(null);
        val endingTime = (System.nanoTime() - time)/1e9
        println("\nfinished deletePathsPluginTestNoMethod in ${endingTime} seconds")

    }

    @Test
    fun deleteReadMappingNoMethod() {
        // THis should fail as method is required.
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/config_smallSeq.txt"
        val keyFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/genotypingKeyFile.txt"
        val method = "READS_FASTQ"

        // genoids are 14,15,16,17,18,19,20,21 for the keyFile taxa used in testing
        // LineA1, LineA, LineB1, LineB for wgs and gbs only

        val time = System.nanoTime()
        ParameterCache.load(configFile)
        DeleteReadMappingPlugin(null,false)
                //.methodName(method)
                .keyFile(keyFile)
                .performFunction(null);
        val endingTime = (System.nanoTime() - time)/1e9
        println("\nfinished deletePathsPluginTestNoMethod in ${endingTime} seconds")

    }

    @Test
    fun deleteReadMappingNoKeyfile() {
        // This should pass, and delete all entries from read_mapping, and corresponding entries
        // from read_mapping_paths.  It also deletes entries from paths table, leaving those
        // not associated with the specified method.
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/config_smallSeq.txt"
        val keyFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg492_delete_readMappingPaths/testing/genotypingKeyFile.txt"
        val method = "READS_FASTQ"

        // genoids are 14,15,16,17,18,19,20,21 for the keyFile taxa used in testing
        // LineA1, LineA, LineB1, LineB for wgs and gbs only

        val time = System.nanoTime()
        ParameterCache.load(configFile)
        DeleteReadMappingPlugin(null,false)
                .methodName(method)
                //.keyFile(keyFile)
                .performFunction(null);
        val endingTime = (System.nanoTime() - time)/1e9
        println("\nfinished deletePathsPluginTestNoMethod in ${endingTime} seconds")

    }
}