package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.*
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.taxa.Taxon
import net.maizegenetics.util.LoggingUtils
import org.junit.Test
import org.junit.Assert.*
import java.io.*
import java.lang.IllegalArgumentException
import java.util.*

val minimap2location = "/Users/peterbradbury/git/minimap2/minimap2"

class FindBestDiploidPathTest {

    @Test
    fun testPairToLong() {
        val pairLong: Long = intArrayToLong(intArrayOf(5,10))
        val backToInts = longToIntArray(pairLong)
        assertEquals(5, backToInts[0])
        assertEquals(5, backToInts[0])
        assertEquals(10, backToInts[1])
    }

    @Test
    fun testCountsFromHapidList() {
        val idList = listOf(HapIdSetCount(setOf(1,2,3,4), 3), HapIdSetCount(setOf(1,3,4), 4), HapIdSetCount(setOf(2,4), 2))

        val countMap : Map<Long,Int> = countsFromHapidList(idList)
        assertEquals("count map size wrong",11, countMap.size)
        assertEquals(3, countMap.getOrDefault(intArrayToLong(intArrayOf(1,2)), 0))
        assertEquals(7, countMap.getOrDefault(intArrayToLong(intArrayOf(1,3)), 0))
        assertEquals(7, countMap.getOrDefault(intArrayToLong(intArrayOf(1,4)), 0))
        assertEquals(7, countMap.getOrDefault(intArrayToLong(intArrayOf(1,1)), 0))
        assertEquals(5, countMap.getOrDefault(intArrayToLong(intArrayOf(2,4)), 0))
        assertEquals(5, countMap.getOrDefault(intArrayToLong(intArrayOf(2,2)), 0))

        //test total count
        assertEquals(9, countMap.get(-1))
    }

    @Test
    fun testNodePairs() {
        val nodeList: MutableList<HaplotypeNode> = ArrayList()
        val taxonList = TaxaListBuilder().add(Taxon("t1")).build()
        for (i in 1..3) {
            nodeList.add(HaplotypeNode(null, taxonList, i))
        }
        val pairs = nodePairs(nodeList)
        for (i in 0..8) {
            val a: Int = i / 3 + 1
            val b: Int = i % 3 + 1
            assertEquals("testing first value of pair $i", a, pairs[i].first.id())
            assertEquals("testing second value of pair $i", b, pairs[i].second.id())
        }
    }

    @Test
    fun testStartProbabilities() {
        val nodeList: MutableList<HaplotypeNode> = ArrayList()
        val taxonList = TaxaListBuilder().add(Taxon("t1")).build()
        for (i in 1..3) {
            nodeList.add(HaplotypeNode(null, taxonList, i))
        }

        val probs = startProbabilities(nodeList)

        probs.forEach { print(java.lang.String.format("%1.3e ", it)) }
        println()

        //# of successes
        val expectedProb = 1.0 / 9.0;
        for (i in 0..8) {
            assertEquals("Testing probability $i", expectedProb, probs[i], 0.00001)
        }
    }

    @Test
    fun testEmissionProbability() {
        val chr = Chromosome.instance(1)
        val refRange = ReferenceRange("refrange1",chr,1,100, 1)
        val refRange2 = ReferenceRange("refrange2",chr,101,200, 2)

        val nodeList: MutableList<HaplotypeNode> = ArrayList()
        val taxonList = TaxaListBuilder().add(Taxon("t1")).build()
        for (i in 1..3) {
            nodeList.add(HaplotypeNode(null, taxonList, i))
        }

        val rangeToNodeMap: TreeMap<ReferenceRange, List<HaplotypeNode>> = TreeMap()
        rangeToNodeMap.put(refRange, nodeList)

        val nodeList2: MutableList<HaplotypeNode> = ArrayList()
        for (i in 4..6) {
            nodeList2.add(HaplotypeNode(null, taxonList, i))
        }
        rangeToNodeMap.put(refRange2, nodeList2)

        val reads : Multimap<ReferenceRange, HapIdSetCount> = HashMultimap.create()
        reads.put(refRange, HapIdSetCount(setOf(1,2), 3))
        reads.put(refRange, HapIdSetCount(setOf(1), 3))
        reads.put(refRange, HapIdSetCount(setOf(1,2,3), 1))

        val dipEmissionProb = DiploidEmissionProbability(rangeToNodeMap,reads,0.9)

        (0..8).map { dipEmissionProb.getProbObsGivenState(it,0) }.forEach { println(it) }

        //expected values were calculated using R function dmultinom()
        //state 0 (1,1): dbinom(7,7,0.9)
        assertEquals("testing prob11", 0.4782969, dipEmissionProb.getProbObsGivenState(0,0), 1e-6)
        //state 1 (1,2): sum over n = 0 to 4, dmultinom(x = c(3 + n, 4 - n, 0),prob = c(0.45,0.45,0.1))
        assertEquals("testing prob12", 0.3699328, dipEmissionProb.getProbObsGivenState(1,0), 1e-6)
        //state 2 (1,3): sum over n = 0 to 1, dmultinom(x = c(6 + n, 1 - n, 0),prob = c(0.45,0.45,0.1))
        val prob = dipEmissionProb.getProbObsGivenState(2,0)
        assertEquals("testing prob13", 0.02989356, prob, 1e-6)

        //state 4 (2,2): dbinom(4,7,0.9)
        assertEquals("testing prob22", 0.0229635, dipEmissionProb.getProbObsGivenState(4,0), 1e-6)

        //state 3 (2,1): same as (1,2)
        assertEquals("testing prob21", 0.3699328, dipEmissionProb.getProbObsGivenState(3,0), 1e-6)

        //test range with no counts
        assertEquals("testing empty range",1.0/9.0, dipEmissionProb.getProbObsGivenState(0,1), 0.00001)
    }

    @Test
    fun compareEmissionProbabilities() {
        val chr = Chromosome.instance(1)
        val refRange = ReferenceRange("refrange1",chr,1,100, 1)
        val refRange2 = ReferenceRange("refrange2",chr,101,200, 2)

        val nodeList: MutableList<HaplotypeNode> = ArrayList()
        val taxonList = TaxaListBuilder().add(Taxon("t1")).build()
        for (i in 1..3) {
            nodeList.add(HaplotypeNode(null, taxonList, i))
        }

        val rangeToNodeMap: TreeMap<ReferenceRange, List<HaplotypeNode>> = TreeMap()
        rangeToNodeMap.put(refRange, nodeList)

        val nodeList2: MutableList<HaplotypeNode> = ArrayList()
        for (i in 4..6) {
            nodeList2.add(HaplotypeNode(null, taxonList, i))
        }
        rangeToNodeMap.put(refRange2, nodeList2)

        val reads : Multimap<ReferenceRange, HapIdSetCount> = HashMultimap.create()
        reads.put(refRange, HapIdSetCount(setOf(1,2), 14))
        reads.put(refRange, HapIdSetCount(setOf(1), 62))
        reads.put(refRange, HapIdSetCount(setOf(2), 11))
//        reads.put(refRange, HapIdSetCount(setOf(1,2,3), 5))
        reads.put(refRange, HapIdSetCount(setOf(3), 3))

        val dipEmissionProb = DiploidEmissionProbability(rangeToNodeMap,reads,0.95)
        (0..8).map { dipEmissionProb.getProbObsGivenState(it,0) }.forEach { println(it) }

    }

    @Test
    fun testTransitionProbability() {
        //build a graph with two refRanges and 3 nodes each
        //create some edges
        val chr = Chromosome.instance(1)
        val refRange = ReferenceRange("refrange1",chr,1,100, 1)
        val refRange2 = ReferenceRange("refrange2",chr,101,200, 2)

        val nodeList: MutableList<HaplotypeNode> = ArrayList()
        for (i in 1..3) {
            val myTaxa = TaxaListBuilder().add("t${i * 3 - 2}").add("t${i * 3 - 1}").add("t${i * 3}").build()
            nodeList.add(HaplotypeNode(HaplotypeSequence.getInstance("", refRange, 0.0, ""), myTaxa, i))
        }

        val rangeToNodeMap: TreeMap<ReferenceRange, List<HaplotypeNode>> = TreeMap()
        rangeToNodeMap.put(refRange, nodeList)

        val hapSequence = HaplotypeSequence.getInstance("", refRange2, 0.0, "")
        val nodeList2: MutableList<HaplotypeNode> = ArrayList()
        nodeList2.add(HaplotypeNode(hapSequence,
                TaxaListBuilder().add("t1").add("t2").build(),
                4))
        nodeList2.add(HaplotypeNode(hapSequence,
                TaxaListBuilder().add("t3").add("t4").build(),
                4))
        nodeList2.add(HaplotypeNode(hapSequence,
                TaxaListBuilder().add("t5").add("t6").build(),
                4))
        nodeList2.add(HaplotypeNode(hapSequence,
                TaxaListBuilder().add("t7").add("t8").add("t9").build(),
                4))

        rangeToNodeMap.put(refRange2, nodeList2)

        val edges = CreateGraphUtils.createEdges(rangeToNodeMap)
        val myGraph = HaplotypeGraph(edges)

        val transProb = DiploidTransitionProbability(myGraph, ArrayList(rangeToNodeMap.values), 0.01)

        transProb.setNode(1)
        assertEquals(16, transProb.numberOfStates)
        assertEquals("", 4.0/9.0, transProb.getTransitionProbability(0,0), 1e-6)
        //state 0 = (0,0); state 1 = (0,1); 0->0 p=2/3;0->1 p=1/3
        assertEquals("", 2.0/9.0, transProb.getTransitionProbability(0,1), 1e-6)
        //state 0 = (0,0); state 2 = (0,2); 0->0 p=2/3;0->2 p=0.01
        assertEquals("", 0.01 * 2.0 / 3.0, transProb.getTransitionProbability(0,2), 1e-6)
        //state 1 = (0,1); state 1 = (0,1);0->0 p=2/3; 0->1 p=1/3
        assertEquals("", 2.0/9.0, transProb.getTransitionProbability(1,1), 1e-6)
        //state 1 = (0,1); state 2 = (0,2); 0->0 p=2/3; 1->2 p=2/3
        assertEquals("", 4.0/9.0, transProb.getTransitionProbability(1,2), 1e-6)

    }

    @Test
    fun runSmallSeqTests() {
        println("running small seq tests")
        LoggingUtils.setupDebugLogging()

        //the following cannot be counted on to run on any computer, so best to leave commented out most of the time.
        smallSeqTests()
    }

    @Test
    fun testFactorialApproximation() {
        var logFactorial: Double = 0.0;
        for (n in 1..15) {
            val N = n.toDouble()
            val stirling1 = logFactorial(n)
            val stirling2 = n * Math.log(N) - N
            logFactorial += Math.log(N)

            println("$n log factorial: $logFactorial, $stirling1, $stirling2")
        }
    }

    @Test
    fun testCommand() {
        val cmd = "/Users/peterbradbury/'Box Sync'/PHG_external_apps/minimap2"
        val builder = ProcessBuilder("bash", "-c", cmd)
        builder.redirectOutput(ProcessBuilder.Redirect.INHERIT)
        builder.redirectError(ProcessBuilder.Redirect.INHERIT)
        val proc = builder.start()
        proc.waitFor()
    }

    @Test
    fun myWorkingDirectory() {
        val cmd ="pwd"
        val builder = ProcessBuilder("bash", "-c", cmd)
        builder.redirectOutput(ProcessBuilder.Redirect.INHERIT)
        builder.redirectError(ProcessBuilder.Redirect.INHERIT)
        val proc = builder.start()
        proc.waitFor()
    }
}

//    Non-junit tests
//    The following tests use the SmallSeq database and will not work correctly unless that exists
//    and the paths to it are correct

fun smallSeqTests() {
    ParameterCache.load(SmallSeqPaths.dbConfigFile)
    val readMappingMethod = "READ_MAP_TEST1"
    val pathMethod = "PATH_DIPLOID_TEST1"

    val myGraphDataSet = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(SmallSeqPaths.dbConfigFile)
            .methods("GATK_PIPELINE")
            .performFunction(null)

    val myGraph = myGraphDataSet.getData(0).getData() as HaplotypeGraph
    println("built graph")

    //test for existence of "LineA_R1.fastq", "LineB_R1.fastq", throw exception if not
    val nameLineA = "${SmallSeqPaths.dataDir}LineA_R1.fastq"
    val nameLineB = "${SmallSeqPaths.dataDir}LineB_R1.fastq"
    if (!File(nameLineA).exists()) throw IllegalArgumentException("$nameLineA does not exist.")
    if (!File(nameLineB).exists()) throw IllegalArgumentException("$nameLineB does not exist.")

    //test for existence of LineA_lineB.fastq
    //if it does not exist, create it by concatenating the two individual line fastq files
    val filenameLineC = "LineA_lineB.fastq"
    val fastqDirName = "${SmallSeqPaths.dataDir}"
    val nameC = "A_B_het"
    if (!File(filenameLineC).exists()) {
        val cmd1 = "cat $nameLineA > ${SmallSeqPaths.dataDir}$filenameLineC"
        val cmd2 = "cat $nameLineB >> ${SmallSeqPaths.dataDir}$filenameLineC"
        val proc1 = ProcessBuilder("/bin/bash", "-c", cmd1)
        proc1.redirectOutput(ProcessBuilder.Redirect.INHERIT)
        proc1.redirectError(ProcessBuilder.Redirect.INHERIT)
        proc1.start().waitFor()

        val proc2 = ProcessBuilder("/bin/bash", "-c", cmd2)
        proc2.redirectOutput(ProcessBuilder.Redirect.INHERIT)
        proc2.redirectError(ProcessBuilder.Redirect.INHERIT)
        proc2.start().waitFor()
    }
    val keyfileReads = makeKeyFileForReadMapping(filenameLineC, nameC)

    //does the haplotype fasta exist?
    //if not, create it and index it
    val hapfastaName = "${SmallSeqPaths.pangenomeDir}phgSmallSeqSequenceGatkPipeline.fa"
    val hapindexName = "${SmallSeqPaths.pangenomeDir}phgSmallSeqSequenceGatkPipeline.mmi"

    if (!File(hapfastaName).exists()) {
        WriteFastaFromGraphPlugin(null,false).outputFile(hapfastaName).performFunction(myGraphDataSet);
    }

    //if the index does not exist, create it
    if (!File(hapindexName).exists()) {
        // num bases = 4g, minimizer size = 15, window size = 10
        //minimap2/minimap2 -d ${HAPLOTYPE_INDEX} -k ${MINIMIZER_SIZE} -I ${NUM_BASES_LOADED} -w ${WINDOW_SIZE} ${HAPLOTYPE_FASTA}
        val numBases = "4G"
        val minimizerSize = "15"
        val windowSize = "10"
        val logfile = "/Users/peterbradbury/temp/index_log.txt"
        val indexCommand = "/Users/peterbradbury/'Box Sync'/PHG_external_apps/minimap2 -d $hapindexName -k $minimizerSize -I $numBases -w $windowSize $hapfastaName > $logfile"

        val proc = ProcessBuilder("bash", "-c", indexCommand)
        proc.redirectOutput(ProcessBuilder.Redirect.INHERIT)
        proc.redirectError(ProcessBuilder.Redirect.INHERIT)
        proc.start().waitFor()

        println("indexed $hapfastaName")
    }

    //run FastqToMappingPlugin to map LineA_LineB
    val mapper = FastqToMappingPlugin(null, false)
        .keyFile(keyfileReads)
        .indexFile(hapindexName)
        .fastqDir(fastqDirName)
        .minimapLocation(minimap2location)
        .methodName(readMappingMethod)
        .methodDescription("$readMappingMethod description")
        .performFunction(myGraphDataSet)

    println("read mappings written for LineA_LineB")

    DiploidPathPlugin(null, false)
        .keyFile("${SmallSeqPaths.dataDir}keyfile_reads_pathKeyFile.txt")
        .readMethodName(readMappingMethod)
        .pathMethodName(pathMethod)
        .minTaxaPerRange(3)
        .splitNodes(false)
        .splitTransitionProb(0.9)
        .minTransitionProbability(0.5)
        .inbreedingCoef(0.1)
        .performFunction(myGraphDataSet)

    //is the path correct? Examine the result.
    printPathNames(nameC, pathMethod)
}

fun makeKeyFileForReadMapping(fastqfileName : String, cultivar : String) : String {
    val keyfilename = "${SmallSeqPaths.dataDir}/keyfile_reads.txt"
    val fastqfile = fastqfileName

    PrintWriter(keyfilename).use {
        it.println("cultivar\tflowcell_lane\tfilename\tPlateID")
        it.println("${cultivar}\tmy_lane\t${fastqfile}\tmy_plate")
    }
    return keyfilename
}


private fun printPathNames(taxon : String, method : String) {
    val conn = DBLoadingUtils.connection(false)
    val sqlstr = "SELECT path_id, line_name, paths_data FROM paths, genotypes, methods WHERE paths.genoid=genotypes.genoid " +
            "AND line_name='$taxon' AND paths.method_id=methods.method_id AND methods.name='$method'"
    val sqlResult = conn.createStatement().executeQuery(sqlstr)
    sqlResult.next()
    val pathList = DBLoadingUtils.decodePathsForMultipleLists(sqlResult.getBytes("paths_data"))
    println("path list size = ${pathList.size}")
    sqlResult.close()

    for (path in pathList) {
        val hapidStr = path.joinToString(",", "(", ")")
        val hapidQuery = "SELECT haplotypes_id, line_name, chrom, range_start FROM haplotypes h, reference_ranges r, " +
                "genotypes g, gamete_haplotypes gh, gametes gam WHERE h.ref_range_id=r.ref_range_id AND g.genoid=gam.genoid " +
                "AND gam.gameteid=gh.gameteid AND gh.gamete_grp_id=h.gamete_grp_id AND haplotypes_id in $hapidStr"
        println(hapidQuery)
        println("-------------------------")
        val hapidResult = conn.createStatement().executeQuery(hapidQuery)

        println("haplotypes_id\tline_name\tchrom\trange_start")
        while (hapidResult.next()) {
            println("${hapidResult.getInt("haplotypes_id")}\t${hapidResult.getString("line_name")}\t${hapidResult.getString("chrom")}\t${hapidResult.getInt("range_start")}")
        }
        println()
        hapidResult.close()
    }

    conn.close()

}
