package net.maizegenetics.pangenome.hapCalling

import it.unimi.dsi.fastutil.longs.Long2ObjectMap
import net.maizegenetics.dna.BaseEncoder
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.*
import net.maizegenetics.pangenome.db_loading.AnchorDataPHG
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.Datum
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.taxa.Taxon
import net.maizegenetics.util.LoggingUtils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.*

@Deprecated("No longer used.  Use the Minimap2 pipeline instead.")
class IndexKmerByHammingPluginTest {
    val configFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/config.txt"
    val outputKmerMapFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/kmerMapFixingRefRange.txt"
    val outputKmerMapFileSmallKmer = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/smallKmer.txt"

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }


    @Test
    fun testChr8KmerIndexing() {
        try {
            println("Attempting to get Graph.")
            val graphDS = HaplotypeGraphBuilderPlugin(null, false)
                    .configFile(configFile)
//                    .methods("mummer4,B73Ref_regionGroup")
                    .methods("mummer4,*")
                    .performFunction(null)

            println("Created Graph.  Now indexing.")
            IndexKmerByHammingPlugin(null, false)
                .kmerMapFile(outputKmerMapFile)
//                .indexKmersPrefix('C')
                .indexKmersPrefix("C")
                .performFunction(graphDS)

        } catch(e:Exception) {
            e.printStackTrace()
            println(e.message)
        }
    }

    /**
     * Sanity check.  Do all haplotype nodes for a given kmer fall within the same reference range.
     */
    @Test
    fun testHapsForKmerInSameRefRange() {
        println("Retrieving graph")
        val graph = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("mummer4,*")
                .build()

        println("Creating idToNode Map")
        //create a hapId->haplotypeNode mapping
        val nodeIdToNodeMap = mutableMapOf<Int, HaplotypeNode>()
        val refRangeIdToRefRangeMap = mutableMapOf<Int, ReferenceRange>()
        for(refRange in graph.referenceRanges()) {
            refRangeIdToRefRangeMap[refRange.id()] = refRange
            for(node in graph.nodes(refRange)) {
                nodeIdToNodeMap[node.id()] = node
            }
        }

        println("Importing Kmer index")
        //Load up the index
//        val kmerIndex = importKmerToHapIdMapFromTextFile("/Volumes/ZackBackup2018/Temp/PHG/KmerTests/kmerMap.txt")
        val kmerIndex = importKmerToHapIdMapFromTextFile("/Volumes/ZackBackup2018/Temp/PHG/KmerTests/kmerMapOnlyInfo.txt")

        println("Checking ids")
        var sharedFullKmerCounter = 0
        for(key in kmerIndex.keys) {
            val currentRefRange = nodeIdToNodeMap[kmerIndex[key][0]]?.referenceRange()
            for(hapId in kmerIndex[key]) {
                if(nodeIdToNodeMap[hapId]?.referenceRange()?.id() != currentRefRange?.id()) {
                    println("Mistake: $key ")
                }
            }
            if(kmerIndex[key].size == graph.nodes(currentRefRange).size) {
//                println("Warning kmer: ${BaseEncoder.getSequenceFromLong(key)} is shared by all nodes in reference range")
                sharedFullKmerCounter++
            }
        }

        println("DONE")
        println("Shared Kmer across all nodes in refRange: $sharedFullKmerCounter")


    }

    @Test
    fun testLoading() {
        memoryReport("Before Load")
        val mapping =importKmerToHapIdMapFromTextFile(outputKmerMapFile)
        memoryReport("After Load")
    }

    @Test
    fun computeEdgeCaseHamming() {
        val dist = hammingDistance(BaseEncoder.getLongFromSeq("CCAACCCACAGACCGCGCGAGCCCGAGGGAAG"), BaseEncoder.getLongFromSeq("CCAACCCACAGACCGCGCGAGCCCGAGGTAAG"))
        println("$dist")
    }


    @Test
    fun testKmerExtraction() {
        //Steps
        //create a graph
        //3 reference ranges
        //3 nodes
        //100 bps each
        val nucleotides = arrayOf("A", "C", "G")
        val rand = Random(12345)
        val multiAnchorRepeat = IntArray(32) {it}
                .map{nucleotides[rand.nextInt(3)]}
                .joinToString("")

        val anchor1Sequence = IntArray(40){it}
                        .map{nucleotides[rand.nextInt(3)]}.joinToString("") +
                multiAnchorRepeat +
                IntArray(40){it}
                        .map{nucleotides[rand.nextInt(3)]}.joinToString("")


        val anchor2Sequence = IntArray(20){it}
                    .map{nucleotides[rand.nextInt(3)]}.joinToString("") +
                multiAnchorRepeat +
                IntArray(20){it}
                        .map{nucleotides[rand.nextInt(3)]}.joinToString("") +
                multiAnchorRepeat +
                        IntArray(20){it}
                        .map{nucleotides[rand.nextInt(3)]}.joinToString("")

        println(multiAnchorRepeat)
        println(anchor1Sequence)
        println(anchor2Sequence)
        //mutate the sequences
        val mutateNode1 = mutate(anchor1Sequence,listOf(5, 10, 15, 20, 25, 30, 35, 75, 80, 85, 90))
        val mutateNode2 = mutate(anchor1Sequence,listOf(10, 20, 30,  80, 90))
        val mutateNode3 = mutate(anchor1Sequence,listOf(5, 15, 25, 35, 55 ,75, 85))

        val mutateNode4 = mutate(anchor2Sequence, listOf(4, 8, 12, 16, 60,64, 68, 70, 100, 104, 108, 116))
        val mutateNode5 = mutate(anchor2Sequence, listOf(4, 12,  60, 68, 70, 100, 116))

        println("$mutateNode1\n$mutateNode2\n$mutateNode3\n$mutateNode4\n$mutateNode5")

        val refRange1 = ReferenceRange("range1", Chromosome.instance("1"),100,250,1)
        val refRange2 = ReferenceRange("range2", Chromosome.instance("1"),400,600,2)

        val taxonList1Builder = TaxaListBuilder()
        val taxaList1 = taxonList1Builder.add(Taxon("tx1")).build()
        val taxonList2Builder = TaxaListBuilder()
        val taxaList2 = taxonList2Builder.add(Taxon("tx2")).build()
        val taxonList3Builder = TaxaListBuilder()
        val taxaList3 = taxonList3Builder.add(Taxon("tx3")).build()

        val node1 = HaplotypeNode(HaplotypeSequence.getInstance(mutateNode1,refRange1, 0.0,AnchorDataPHG.getChecksumForString(mutateNode1,"MD5")),taxaList1,1)
        val node2 = HaplotypeNode(HaplotypeSequence.getInstance(mutateNode2,refRange1, 0.0,AnchorDataPHG.getChecksumForString(mutateNode2,"MD5")),taxaList2,2)
        val node3 = HaplotypeNode(HaplotypeSequence.getInstance(mutateNode3,refRange1, 0.0,AnchorDataPHG.getChecksumForString(mutateNode3,"MD5")),taxaList3,3)

        val node4 = HaplotypeNode(HaplotypeSequence.getInstance(mutateNode4,refRange2, 0.0,AnchorDataPHG.getChecksumForString(mutateNode4,"MD5")),taxaList1,4)
        val node5 = HaplotypeNode(HaplotypeSequence.getInstance(mutateNode5,refRange2, 0.0,AnchorDataPHG.getChecksumForString(mutateNode5,"MD5")),taxaList2,5)

        val treeMap = TreeMap<ReferenceRange, List<HaplotypeNode>>()
        treeMap.put(refRange1, listOf(node1, node2, node3))
        treeMap.put(refRange2, listOf(node4, node5))

        val edges = CreateGraphUtils.createEdges(treeMap)
        val graph = HaplotypeGraph(edges)


        val graphDS = DataSet(listOf(Datum("graph",graph, "graph")),null)

        //Pass the graph into IndexKmerByHammingPlugin
        IndexKmerByHammingPlugin(null, false)
                .kmerMapFile(outputKmerMapFileSmallKmer)
//                .indexKmersPrefix('C')
                .indexKmersPrefix("C")
                .performFunction(graphDS)

        val kmerMap = importKmerToHapIdMapFromTextFile(outputKmerMapFileSmallKmer)
        for(key in kmerMap.keys) {
            println("${BaseEncoder.getSequenceFromLong(key)}: ${kmerMap.get(key).joinToString(",")} ")
        }


        //create a kmer-> count map using kotlin sequence
        val node1Kmers = createKmerMap(mutateNode1)
        val node2Kmers = createKmerMap(mutateNode2)
        val node3Kmers = createKmerMap(mutateNode3)
        val node4Kmers = createKmerMap(mutateNode4)
        val node5Kmers = createKmerMap(mutateNode5)

        //compare each of refRange1 kmers against refRange 2 kmers.  If hamming distance is below 2 add to a remove set.
        var repetitiveKmerSet = HashSet<String>()

        println("Comparing 1 to 4")
        repetitiveKmerSet.addAll(compareKmers(node1Kmers,node4Kmers))
        println("Comparing 1 to 5")
        repetitiveKmerSet.addAll(compareKmers(node1Kmers,node5Kmers))
        println("Comparing 2 to 4")
        repetitiveKmerSet.addAll(compareKmers(node2Kmers,node4Kmers))
        println("Comparing 2 to 5")
        repetitiveKmerSet.addAll(compareKmers(node2Kmers,node5Kmers))
        println("Comparing 3 to 4")
        repetitiveKmerSet.addAll(compareKmers(node3Kmers,node4Kmers))
        println("Comparing 3 to 5")
        repetitiveKmerSet.addAll(compareKmers(node3Kmers,node5Kmers))

        println("Verifying node 1")
        verifyKmers(node1Kmers, repetitiveKmerSet, kmerMap)
        println("Verifying node 2")
        verifyKmers(node2Kmers, repetitiveKmerSet, kmerMap)
        println("Verifying node 3")
        verifyKmers(node3Kmers, repetitiveKmerSet, kmerMap)
        println("Verifying node 4")
        verifyKmers(node4Kmers, repetitiveKmerSet, kmerMap)
        println("Verifying node 5")
        verifyKmers(node5Kmers, repetitiveKmerSet, kmerMap)
    }

    private fun mutate(sequence:String, indexList : List<Int>) : String {
        val sb = StringBuffer(sequence)

        for(index in indexList) {
            sb.setCharAt(index,'T')
        }

        return sb.toString()
    }

    private fun createKmerMap(sequence: String) : Map<String,Int> {
        return sequence.windowedSequence(32,1,false).filter{it.get(0)=='C'}.groupingBy { it }.eachCount()
    }

    private fun compareKmers(kmerMap1:Map<String,Int>, kmerMap2:Map<String,Int>) : Set<String> {
        var repeatSet = HashSet<String>()

        for(kmer1 in kmerMap1.keys) {
            for(kmer2 in kmerMap2.keys) {
                if(hammingDist(kmer1,kmer2) < 2) {
                    if(kmer1 == "CCAACCCACAGACCGCGCGAGCCCGAGGTAAG" || kmer2 == "CCAACCCACAGACCGCGCGAGCCCGAGGTAAG"){
                        println("KmerToCheck:\n$kmer1\n$kmer2")
                    }
                    repeatSet.add(kmer1)
                    repeatSet.add(kmer2)
                }
            }
        }
        return repeatSet
    }

    private fun verifyKmers(kmersInNode: Map<String,Int> , repetitiveKmerSet: HashSet<String>, pluginKmerMap : Long2ObjectMap<IntArray>) {
        for(nodeKmer in kmersInNode.keys) {
            if(repetitiveKmerSet.contains(nodeKmer) && pluginKmerMap.containsKey(BaseEncoder.getLongFromSeq(nodeKmer))) {
                //error as it means it was repetative but still in the index
                Assert.fail("Error with kmer: $nodeKmer, was marked as repetitive, but was indexed.")
            }
            else if(!repetitiveKmerSet.contains(nodeKmer) && pluginKmerMap.containsKey(BaseEncoder.getLongFromSeq(nodeKmer))) {

            }
            else if(repetitiveKmerSet.contains(nodeKmer) && !pluginKmerMap.containsKey(BaseEncoder.getLongFromSeq(nodeKmer))) {

            }
            else {
                //Means that this kmer was in neither the repeat set or in the index.  These need to be investigated.
                Assert.fail("Error with kmer: $nodeKmer, was not found in either repeat set or in index")
            }

        }
    }

    private fun hammingDist(kmer1 : String, kmer2: String) : Int {
        var distance = 0

        for(i in 0 until 32) {
            if(kmer1[i] != kmer2[i]) {
                distance++
            }
        }

        return distance
    }

    private fun memoryReport(whenString: String) {
        for (i in 0..2) System.gc()
        val myRuntime = Runtime.getRuntime()
        val totalmem = myRuntime.totalMemory()
        val freemem = myRuntime.freeMemory()
        val msg = String.format("$whenString Memory used = %,d out of %,d total", totalmem - freemem, totalmem)
//        myLogger.info(msg)
        println(msg)
    }
}