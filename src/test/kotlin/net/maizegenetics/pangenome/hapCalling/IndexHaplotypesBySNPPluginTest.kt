package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.graph
import net.maizegenetics.util.LoggingUtils
import org.junit.Before
import org.junit.Test

class IndexHaplotypesBySNPPluginTest {
    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun testVCFUploadFull() {
        val userHome = System.getProperty("user.home")
        val vcfFile = "$userHome/temp/phgSmallSeq/dockerBaseDir/outputDir/output.vcf"
        val graphConfig = "$userHome/temp/phgSmallSeq/dockerBaseDir/configSQLite.txt"
        val debugDir = "$userHome/temp/phgSmallSeq/debugOutput/"

        val graphDS = HaplotypeGraphBuilderPlugin(null,false)
                .methods("CONSENSUS")
                .configFile(graphConfig)
                .includeSequences(false)
                .includeVariantContexts(true)
                .performFunction(null)

        IndexHaplotypesBySNPPlugin()
                .vcfFile(vcfFile)
                .outputIndexFile(debugDir+"testVCFIndex_Streaming.txt")
                .methods("CONSENSUS")
                .configFile(graphConfig)
                .performFunction(graphDS)

    }
}