package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pangenome.api.CreateGraphUtils
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.util.LoggingUtils
import org.junit.Test
import java.nio.file.Files
import kotlin.test.assertEquals

class BestHaplotypePathTest {

    @Test
    fun testPathFinding() {
        LoggingUtils.setupDebugLogging()

        //build HaplotypeGraph
        val graph = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(SmallSeqPaths.dbConfigFile)
                .methods("CONSENSUS")
                .build()
        println()
        val tempdir = Files.createTempDirectory("bestpath")
        BestHaplotypePathPlugin(null, false)
                .readMethodName("MyReadMethodName")
                .pathMethodName("MyPathMethodName")
                .readMapFilename(SmallSeqPaths.outputDir + "fastq_hap_count/LineA_R1_multimap.txt.gz")
                .pathOutDirectory(tempdir.toString())
                .splitConsensusNodes(true)
                .minTaxaPerRange(2)
                .removeRangesWithEqualCounts(false)
                .performFunction(DataSet.getDataSet(graph))

        //test overwrite, should print warning to log
        BestHaplotypePathPlugin(null, false)
                .readMethodName("MyReadMethodName")
                .pathMethodName("MyPathMethodName")
                .readMapFilename(SmallSeqPaths.outputDir + "fastq_hap_count/LineA_R1_multimap.txt.gz")
                .pathOutDirectory(tempdir.toString())
                .splitConsensusNodes(true)
                .minTaxaPerRange(2)
                .removeRangesWithEqualCounts(false)
                .overwrite(false)
                .performFunction(DataSet.getDataSet(graph))


        // create a hapid -> node map in order to get taxaList
        val hapidToNodeMap = HashMap<Int, HaplotypeNode>()
        graph.nodeStream().forEach { hapidToNodeMap.put(it.id(), it) }

        println("temp dir is $tempdir")
        println("path contents :")
        Files.list(tempdir).forEach {
            Files.newBufferedReader(it.toAbsolutePath()).use {
                it.lines().forEach { println(it) }
            }

            println()
            Files.newBufferedReader(it.toAbsolutePath()).use {
                //println version is useful for diagnostics if assert fails, can comment out otherwise
//                it.lines().filter { !(it.startsWith("#")) }
//                        .map{it.toInt()}
//                        .map { hapidToNodeMap.get(it)?.taxaList()}
//                        .forEach { if (it != null) println(it.joinToString(",")) }

                it.lines().filter { !(it.startsWith("#")) }
                        .map{it.toInt()}
                        .map { hapidToNodeMap.get(it)?.taxaList()}
                        .forEach { if (it != null) assertEquals("LineA,LineA1", it.joinToString(",")) }

            }

        }


    }
}