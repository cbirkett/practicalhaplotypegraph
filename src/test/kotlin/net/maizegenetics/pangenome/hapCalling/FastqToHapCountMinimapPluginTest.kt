package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultiset
import com.google.common.collect.Range
import com.google.common.collect.TreeBasedTable
import htsjdk.samtools.SamReaderFactory
import htsjdk.samtools.ValidationStringency
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.taxa.Taxon
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.apache.commons.math3.distribution.BinomialDistribution
import org.junit.Before
import org.junit.Test
import java.io.File
import java.util.concurrent.atomic.LongAdder

class FastqToHapCountMinimapPluginTest {
    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun testCounterSimple() {
        val samFileName = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/minimap2_paramTuning_secondary_f_1000000_5000000_k21_w11_m142_B73ToPangenome_100Reads_150bp_paired_Simulated_shiftQual-7.sam"
        val outputFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/testCountFile_TestingExclusions.txt"
        val readDebugFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/readOutputFile_TestingExclusion.txt"
        val configFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/config.txt"

        val graphDS = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("mummer4")
                .includeVariantContexts(false)
                .includeSequences(false)
                .performFunction(null)

        FastqToHapCountMinimapPlugin(null, false)
                .samFileName(samFileName)
                .hapCountFile(outputFile)
                .readOutputFile(readDebugFile)
                .performFunction(graphDS)
    }


    @Test
    fun testCounter() {
//        val samFileName = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/minimap2_paramTuning_secondary_f_10000_50000_k21_w11_B73ToPangenome_100Reads_150bp_paired_Simulated_shiftQual-7.sam"
//        val samFileName = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/minimap2_paramTuning_secondary_f_1000000_5000000_k21_w11_m142_B73ToPangenome_100Reads_150bp_paired_Simulated_shiftQual-7.sam"
        val samFileName = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/minimap2_N_20_secondary_k21_w11_I_90G_f_10k_50k_m142_B73ToPangenome_2x_150bp_paired_Simulated_shiftQual-7.sam"
        val outputFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/testCountFile_1Mill_2xFile.txt"
//        val outputFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/testCountFile_TestingFlag.txt"
//        val readDebugFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/readOutputFile_TestingFlag.txt"
        val readDebugFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/readOutputFile_1Mill_2xFile.txt"
        FastqToHapCountMinimapPlugin(null, false)
                .samFileName(samFileName)
                .hapCountFile(outputFile)
                .readOutputFile(readDebugFile)
                .performFunction(null)
    }

    @Test
    fun runFindPathAndVerify() {
        val countFileDir = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/2xCount"
        val configFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/config.txt"
        val outputDirectory = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/2xPath"

        val graphDS = HaplotypeGraphBuilderPlugin(null,false)
                .configFile(configFile)
                .methods("mummer4")
                .includeVariantContexts(false)
                .includeSequences(false)
                .performFunction(null)


        HapCountBestPathToTextPlugin(null,false)
                .configFile(configFile)
                .inclusionFilenameDir(countFileDir)
                .outputDir(outputDirectory)
                .hapCountMethod("MinimapCountMethod1")
                .pathMethod("MinimapPathMethod1")
                .performFunction(graphDS)





    }
    @Test
    fun verifyPath() {
        val pathFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/2xPath/testCountFile_1Mill_2xFile_path.txt"
        val configFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/config.txt"

//        VerifyPathsPlugin(null,false)
//                .configFile(configFile)
//                .methods("mummer4")
//                .pathFile(pathFile)
//                .targetTaxon("B73_Assembly")
//                .performFunction(null)

    }

    @Test
    fun countB73Hits() {
//        val readMappingFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/readMappingOutput.txt"
        val readMappingFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/readMappingOutputSingleRefRange.txt"
        var numberOfHits = LongAdder()
        val readHitCounter = HashMultiset.create<Int>()
        var numberB73Hits = LongAdder()




        val lines = Utils.getBufferedReader(readMappingFile).readLines()

        for( line in lines) {
            if(line.startsWith("readName")) continue
            numberOfHits.increment()
//            println(line)
            if(numberOfHits.sum() %1000000 == 0L) {
                println(numberOfHits.sum())
            }
            val lineSplit = line.split("\t")
            val listOfHapHitsString = lineSplit[4]
            val listOfHapHits = listOfHapHitsString.substring(1,listOfHapHitsString.length-1).split(",")
            if(listOfHapHits.size>19) println(line)
            readHitCounter.add(listOfHapHits.size)
            for(hapIdStr in listOfHapHits) {
                if(hapIdStr.trim().toInt() < 150897)
                    numberB73Hits.increment()
            }

        }

        println("${numberOfHits.sum()}: ${numberB73Hits.sum()}")
        println(readHitCounter.toString())




//        var numberOfB73Hits = Utils.getBufferedReader(readMappingFile).readLines()
//                .map {
//                    numberOfHits.increment()
//                    it.split("\t")[4]
//                }
//                .map { it.substring(1,it.length-1) }
//                .flatMap { it.split(",") }
//                .map { it.trim().toInt() }
//                .filter { it< 150897}
//                .count()
//        println("${numberOfHits.sum()}: $numberOfB73Hits")



    }


    @Test
    fun simpleTest() {
        var numberOfB73Hits = listOf<String>("1-4093768\tfalse\t1\t0\t[955235, 841140, 1167172, 225104, 261097, 877857, 1233818, 122400, 901703, 971705]",
        "1-4093768\ttrue\t1\t0\t[901703, 396529, 877857, 1167172, 122400, 971705, 700277, 841140, 1233818, 225104, 261097, 342478]",
        "1-4093766\tfalse\t3\t0\t[832657, 117514, 156565]",
        "1-4093766\ttrue\t0\t0\t[832657, 156565, 117514]",
        "1-4093764\tfalse\t1\t0\t[113553, 528501, 831177, 157157, 766620, 895744]"
        )
                .map {
                    it.split("\t")[4]
                }
                .map { it.substring(1,it.length-1) }

                .flatMap { it.split(",") }
                .map { it.trim().toInt() }
                .filter { it< 150897}
                .count()

        println(numberOfB73Hits)
    }


    @Test
    fun getDepthByRefRange() {
        val countFileName = ""
        val outputFileName = ""
        val configFile = ""
        //Function to load in the count file
        val hapIdToCountMap = Utils.getBufferedReader(countFileName).readLines()
                .filter { !it.startsWith("#") }
                .map { it.split("\t") }
                .map { Pair(it[0].toInt(),it[1]) } //we only need to look at inclusions
                .toMap()

        val graph = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("mummer4")
                .includeVariantContexts(false)
                .includeSequences(false)
                .build()


        val hapIdToRefRangeMap = graph.referenceRanges()
                .flatMap { graph.nodes(it) }
                .map { Pair(it.id(),it.referenceRange()) }
                .toMap()

        //Make a table of <refRange,taxon,count>
        val tableToExport = TreeBasedTable.create< ReferenceRange, String,Int>()

        graph.referenceRanges().flatMap { graph.nodes(it) }
                .forEach {
                    val countsForId = hapIdToCountMap[it.id()]
                    val taxaList = it.taxaList()
                    val refRange = it.referenceRange()

                    taxaList.forEach {taxon -> tableToExport.put(refRange,taxon.name,countsForId?.toInt()?:0) }
                }

        Utils.getBufferedWriter(outputFileName).use { out ->
            val colKeys = tableToExport.columnKeySet()
            out.write("refRangeId\tchr\tstart\tend\t${colKeys.joinToString("\t")}\n")

            tableToExport.rowKeySet().forEach { row ->
                out.write("${row.id()}\t${row.chromosome().name}\t${row.start()}\t${row.end()}\t")
                out.write(colKeys.map { col -> tableToExport.get(row,col) }
                        .joinToString("\t"))
                out.write("\n")

            }

        }

    }

    @Test
    fun checkSmallRanges() {
        val samFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/minimap2_N_20_secondary_k21_w11_I_90G_f_10k_50k_m142_B73ToPangenome_2x_150bp_paired_Simulated_shiftQual-7.sam"
        val shortHaplotypes = setOf(116428,118866,114645,115686,124293,124888,128401,131408,134548,135007,135571,133838, 134081,135872,137773,138489,138768,138800,141656,142214,145700,146073,148130,150455)

        var samReader = SamReaderFactory.makeDefault()
                .validationStringency(ValidationStringency.SILENT)
                .open(File(samFile))



        val samIterator = samReader.iterator()
        while(samIterator.hasNext()) {
            val samRecord = samIterator.next()
            if(samRecord.readUnmappedFlag) continue
            if(shortHaplotypes.contains(samRecord.contig.toInt())) {
                println(samRecord)
            }


        }
    }

    @Test
    fun checkMissingRefRangesInPath() {

//        val configFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/config.txt"
//        val pathFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/2xB73ReadsToPangenomeSingleRefRange_noDups_Paired_NoMaxFilter_PickingBestRefRange_counts_path.txt"
//        val outputFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/2xB73ReadsToPangenomeSingleRefRange_noDups_Paired_NoMaxFilter_PickingBestRefRange_counts_path_missedRefRanges.txt"

//        val configFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/config.txt"
//        val pathFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/75PercentTest/2xB73ReadsToPangenomeSingleRefRange_noDups_Paired_NoMaxFilter_PickingBestRefRange_MxErr25_counts_path.txt"
//        val outputFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/75PercentTest/2xB73ReadsToPangenomeSingleRefRange_noDups_Paired_NoMaxFilter_PickingBestRefRange_MxErr25_counts_path_missedRefRanges.txt"


        val configFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/config.txt"
        val pathFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/75PercentTest/RealData/282Paths/4882_6665_17312_HBEN2ADXX_B73_GTTTCG_k_21_f_10k50k_m_142_MxErr25_counts_path.txt"
        val outputFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/75PercentTest/RealData/282Paths/4882_6665_17312_HBEN2ADXX_B73_GTTTCG_k_21_f_10k50k_m_142_MxErr25_counts_path_MissedRefRanges_sorted.txt"
        val outputMistakeFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/75PercentTest/RealData/282Paths/4882_6665_17312_HBEN2ADXX_B73_GTTTCG_k_21_f_10k50k_m_142_MxErr25_counts_path_MistakeRefRanges_sorted.txt"

        val refRangesDroppedFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/75PercentTest/zack_droppedRefRangeIds_fromOrigDB_SORTED.txt"

        val refRangesDropped = Utils.getBufferedReader(refRangesDroppedFile).readLines().map { it.toInt() }
                .toSet()

        //Load in the graph
        val graph = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("mummer4")
                .includeVariantContexts(false)
                .includeSequences(false)
                .build()



        //Get Map<refRangeId,RefRange>
        val refRangeIdToRefRangeMap = graph.referenceRanges()
                .map { Pair(it.id(),it) }
                .toMap()

        //Get Map<hapId,RefRange>
        val hapIdToRefRangeIdMap = graph.referenceRanges()
                .flatMap { graph.nodes(it) }
                .map { Pair(it.id(),it.referenceRange().id()) }
                .toMap()

        val hapIdToHaplotypeMap = graph.referenceRanges()
                .flatMap { graph.nodes(it) }
                .map{Pair(it.id(),it)}
                .toMap()

        //Load in the path file


        //Walk through path, get hit list
        val refRangesHitIdSet = Utils.getBufferedReader(pathFile).readLines()
                .filter { !it.startsWith("#") }
                .map { it.trim().toInt() }
                .map { hapIdToRefRangeIdMap[it] }
                .toSet()

        val b73HapIdsForEachRefRange=graph.referenceRanges().flatMap { graph.nodes(it) }
                .filter { it.taxaList().contains(Taxon("B73_Assembly")) }
                .map { Pair(it.referenceRange(),it.id()) }
                .toMap()

        //Walk through list of ref ranges and if they are not hit print out object(chr,start,end,methodName)
        Utils.getBufferedWriter(outputFile).use { out ->
            out.write("RefRangeId\tchrName\tstart\tend\tlength\tmethods\n")
            refRangeIdToRefRangeMap.keys
                    .filter { !refRangesHitIdSet.contains(it) }
                    .map { refRangeIdToRefRangeMap[it] }
                    .sortedBy { it }
                    .forEach {
                        val start = it?.start()?:0
                        val end = it?.end()?:0
                        val length = end - start +1

                        out.write("${it?.id()}\t${it?.chromosome()?.name}\t${it?.start()}\t${it?.end()}\t${length}\t${it?.groupMethods()?.joinToString(",")}\n") }

        }

        Utils.getBufferedWriter(outputMistakeFile).use { out ->
            out.write("RefRangeId\tchrName\tstart\tend\tlength\tmethods\n")

            Utils.getBufferedReader(pathFile).readLines()
                    .filter { !it.startsWith("#") }
                    .map { it.trim().toInt() }
                    .map { hapIdToHaplotypeMap[it] }
                    .sortedBy { it }
                    .filter { !(it?.taxaList()?.contains(Taxon("B73_Assembly"))?:false) }
                    .map { it?.referenceRange() }
                    .forEach { val start = it?.start()?:0
                        val end = it?.end()?:0
                        val length = end - start +1

                        out.write("${it?.id()}\t${it?.chromosome()?.name}\t${it?.start()}\t${it?.end()}\t${length}\t${it?.groupMethods()?.joinToString(",")}\n") }
        }



//            refRangeIdToRefRangeMap.keys
//                    .filter { !refRangesHitIdSet.contains(it) }
//                    .map { refRangeIdToRefRangeMap[it] }
//                    .forEach {
//                        val start = it?.start()?:0
//                        val end = it?.end()?:0
//                        val length = end - start +1
//
//                        out.write("${it?.id()}\t${it?.chromosome()?.name}\t${it?.start()}\t${it?.end()}\t${length}\t${it?.groupMethods()?.joinToString(",")}\n") }

//        }
//        Utils.getBufferedWriter(outputMistakeFile).use { out ->
//            out.write("RefRangeId\tchrName\tstart\tend\tlength\tmethods\n")
//            refRangeIdToRefRangeMap.keys
//                    .filter { !refRangesHitIdSet.contains(it) }
//                    .map { refRangeIdToRefRangeMap[it] }
//                    .filter { b73HapIdsForEachRefRange.containsKey(it) }
//
//                    .forEach {
//                        val start = it?.start()?:0
//                        val end = it?.end()?:0
//                        val length = end - start +1
//
//                        out.write("${it?.id()}\t${it?.chromosome()?.name}\t${it?.start()}\t${it?.end()}\t${length}\t${it?.groupMethods()?.joinToString(",")}\n") }
//
//        }

        println(refRangeIdToRefRangeMap.keys
                .filter { !refRangesHitIdSet.contains(it) }
                .map { refRangeIdToRefRangeMap[it] }
                .filter {
                    val start = it?.start()?:0
                    val end = it?.end()?:0
                    val length = end - start +1
                    length<1100
                }
                .count())

        println(refRangeIdToRefRangeMap.keys
                .filter { !refRangesHitIdSet.contains(it) }
                .map { refRangeIdToRefRangeMap[it] }
                .filter {
                    val start = it?.start()?:0
                    val end = it?.end()?:0
                    val length = end - start +1
                    length<800
                }
                .count())

        println("NumberOf DroppedList:${refRangesDropped.size}")
        println("NumberOfRangesMissed in DroppedList:"+refRangeIdToRefRangeMap.keys
                .filter { !refRangesHitIdSet.contains(it) }
                .map { refRangeIdToRefRangeMap[it] }
                .map { it?.id() }
                .filter { refRangesDropped.contains(it) }
                .count()

        )

        println("NumberOfRangesMissed in DroppedList Sub 800bp:"+refRangeIdToRefRangeMap.keys
                .filter { !refRangesHitIdSet.contains(it) }
                .map { refRangeIdToRefRangeMap[it] }
                .filter { refRangesDropped.contains(it?.id()) }
                .filter {
                    val start = it?.start()?:0
                    val end = it?.end()?:0
                    val length = end - start +1
                    length<800
                }
                .count()

        )

    }


    @Test
    fun debugPathFinding() {
        val configFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/config.txt"
        val inputDir = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/testInput/"

        val outputDir = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/testOutput/"


        val graphDS = HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("mummer4")
                .includeSequences(false)
                .includeVariantContexts(false)
                .performFunction(null)

        HapCountBestPathToTextPlugin(null, false)
                .inclusionFilenameDir(inputDir)
                .configFile(configFile)
                .outputDir(outputDir)
                .hapCountMethod("MinimapCountMethod1")
                .pathMethod("MinimapPathMethod1")
                .performFunction(graphDS)

    }

    @Test
    fun debugSingleRefRanges() {
        val bedFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/B73UnitTests/Minimap2Tests/DebugHapCounts/TruthBedFile/2xReads_B73Simulated_150bp_seed12345_HS20_shiftqual-7__mapScore.bed"
        val chr1 = Utils.getBufferedReader(bedFile).readLines().filter { it.startsWith("1\t") }
        val rangeOfInterest = Range.closed(12044022,12073077)
        chr1.map { it.split("\t") }
                .filter { rangeOfInterest.contains(it[1].toInt()) || rangeOfInterest.contains(it[2].toInt()) }
                .forEach { println(it.joinToString("\t")) }
    }

}



