package net.maizegenetics.pangenome.hapCalling

import it.unimi.dsi.fastutil.longs.Long2ObjectMap
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap
import junit.framework.Assert.assertEquals
import net.maizegenetics.dna.BaseEncoder
import org.apache.log4j.Logger;
import org.junit.Assert
import org.junit.Test
import java.util.*

class KmerUtilsTest {
    val myLogger = Logger.getLogger(KmerUtilsTest::class.java)
    @Test
    fun testHammingDist() {
        val rand = Random(12345)
        val nucleotides = listOf("A", "C", "G", "T")

        var kmerList = mutableListOf<String>()
        for(i in 0 until 10000) {
            kmerList.add(createKmer(nucleotides,rand))
        }

//        val kmerList:List<String> = (0 .. 13).map { _ -> return createKmer(nucleotides = nucleotides, rand = rand)}
//        kmerList.forEach{println(it)}

        for(i in 0 until kmerList.size) {
            for(j in i+1 until kmerList.size) {
                val manHamming = computeHammingManually(kmerList[i], kmerList[j])
                val kmer1Encoded = BaseEncoder.getLongFromSeq(kmerList[i])
                val kmer2Encoded = BaseEncoder.getLongFromSeq(kmerList[j])
//                println("Kmers:${kmerList[i]} ${kmerList[j]}: $manHamming ${hammingDistance(kmer1Encoded,kmer2Encoded)}")
                Assert.assertEquals("Hamming Distance is not consistent for kmers:${kmerList[i]} \n${kmerList[j]}", manHamming, hammingDistance(kmer1Encoded,kmer2Encoded))
            }
        }



    }

    @Test
    fun checkNs() {
        val kmerWithNs = "ACGTACGTACGTACGTNNNNACGTACGTACGT"
        println(kmerWithNs)
        val kmerEncoded = BaseEncoder.getLongFromSeq(kmerWithNs)
        println(kmerEncoded)
        val kmerDecoded = BaseEncoder.getSequenceFromLong(kmerEncoded)
        println(kmerDecoded)
    }



    @Test
    fun dynamicKmerMask() {
        val fourMer = "ATCG"
        val fourMerEncoded = BaseEncoder.getLongFromSeq(fourMer)
        var lowMask = (1L shl (64-4)) - 1
        var highMask = lowMask.inv()

        assertEquals("4Mer Test fails. Low Mask:","AACG",BaseEncoder.getSequenceFromLong((fourMerEncoded and lowMask), 4.toByte()))
        assertEquals("4Mer Test fails. Low Mask:","ATAA",BaseEncoder.getSequenceFromLong((fourMerEncoded and highMask), 4.toByte()))


        val fiveMer = "ATCGA"
        val fiveMerEncoded = BaseEncoder.getLongFromSeq(fiveMer)
        var shift = if(fiveMer.length % 2 != 0) fiveMer.length-1 else fiveMer.length
        lowMask = (1L shl (64-shift)) -1
        highMask = lowMask.inv()

        assertEquals("5Mer Test fails. Low Mask:","AACGA",BaseEncoder.getSequenceFromLong((fiveMerEncoded and lowMask), 5.toByte()))
        assertEquals("5Mer Test fails. Low Mask:","ATAAA",BaseEncoder.getSequenceFromLong((fiveMerEncoded and highMask), 5.toByte()))


        val oneMer = "T"
        val oneMerEncoded = BaseEncoder.getLongFromSeq(oneMer)
        shift = if(oneMer.length % 2 != 0) oneMer.length-1 else oneMer.length
        lowMask = (1L shl (64-shift)) -1
        highMask = lowMask.inv()

        assertEquals("1Mer Test fails. Low Mask:","A",BaseEncoder.getSequenceFromLong((oneMerEncoded and lowMask), 1.toByte()))
        assertEquals("1Mer Test fails. Low Mask:","T",BaseEncoder.getSequenceFromLong((oneMerEncoded and highMask), 1.toByte()))


    }


    /**
     * Simple unit test to check to make sure kmers are being marked correctly.  
     */
    @Test
    fun testMarkingKmers() {
        var kmerMap = MutableKmerToIdMap(6, 'G')

        val truthMap = HashMap<Long,Boolean>()
        truthMap[BaseEncoder.getLongFromSeq("GGGGGG")] = true
        truthMap[BaseEncoder.getLongFromSeq("GGAGGG")] = false
        truthMap[BaseEncoder.getLongFromSeq("GCCGGG")] = false
        truthMap[BaseEncoder.getLongFromSeq("GGAAGG")] = false
        truthMap[BaseEncoder.getLongFromSeq("GCCCGG")] = false
        truthMap[BaseEncoder.getLongFromSeq("GCACGG")] = false
        truthMap[BaseEncoder.getLongFromSeq("GGCCCG")] = true


        val refRange1Kmer = KmerMap("GGGGGGGGAGGGGGGGGGGCCGGG", kmerSize = 6, kmerPrefix = 'G', stepSize = 6)
        kmerMap.putAll(refRange1Kmer.kmersAsLongSet(),1)

        val refRange2Kmer = KmerMap("GGAAGGGCCCGGGCACGG", kmerSize = 6, stepSize = 6, kmerPrefix = 'G')
        kmerMap.putAll(refRange2Kmer.kmersAsLongSet(),2)

        val refRange3Kmer = KmerMap("GGCCCGGCACGGGCACGGAAAAGG", kmerSize = 6, stepSize = 6, kmerPrefix = 'G')
        kmerMap.putAll(refRange3Kmer.kmersAsLongSet(),3)


        val kmersMarked = markKmersForPurgeUsingHammingDistance(kmerMap,6,2,false)

        for(i in 0 until kmersMarked.kmerArray.size) {
            if(kmersMarked.refRangeIdArray[i] == -1 || kmersMarked.purgeArray[i].toInt() == 1 ) {
                Assert.assertEquals("Purged array is not supposed to be purged: ",false, truthMap[kmersMarked.kmerArray[i]])
            }
            else {
                Assert.assertEquals("Kmer is not marked for purge, but should be: ",true, truthMap[kmersMarked.kmerArray[i]])
            }
        }

    }


    fun createKmer(nucleotides: List<String>, rand: Random) : String {
        val sb = StringBuilder()
        for(i in 0 until 32) {
            sb.append(nucleotides[rand.nextInt(4)])
        }
//        return (1..32).map{ nucleotides[rand.nextInt(4)].toList().joinToString("")}
        return sb.toString()
    }

    fun computeHammingManually(kmer1 : String, kmer2: String) : Int {
        var dist = 0
        for(i in 0 until kmer1.length) {
            if(kmer1[i] != kmer2[i]) {
                dist++
            }
        }
        return dist
    }


    @Test
    fun importExportBinaryFileTest() {
        val kmerFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/unitTestIndex.bin"

        val rand = Random(12345)
        val nucleotides = listOf("A", "C", "G", "T")
        val kmerMap = Long2ObjectOpenHashMap<IntArray>(70000000)
        var kmerSet = mutableSetOf<Long>()
        myLogger.info("Creating Kmers")
        for(i in 0 until 10000000) {
            kmerSet.add(BaseEncoder.getLongFromSeq(createKmer(nucleotides,rand)))
        }
        val kmerList = kmerSet.toList()
        myLogger.info("Number of Kmers:${kmerList.size}")
        //Going to use 100 reference ranges
        myLogger.info("Randomly associating hapIds to kmers")
        var startId = 1
        var totalNumberHapIds = 0
        for(i in 0 until kmerList.size) {
            if(i % 100 == 0) {
                startId += 20
            }

            val numOfHaps = rand.nextInt(19) + 1
            var hapSet = mutableSetOf<Int>()
            for(j in 0 until numOfHaps) {
                hapSet.add(rand.nextInt(19) + startId)
                totalNumberHapIds++
            }
            val hapArray = IntArray(hapSet.size)
            val hapList = hapSet.toList()
            for(hapId in 0 until hapList.size) {
                hapArray[hapId] = hapList[hapId]
            }
            kmerMap[kmerList[i]] = hapArray
        }

        myLogger.info("Number HapIds $totalNumberHapIds")

        //Now that we have the kmerMap lets export it and then import it.
        myLogger.info("Exporting KmerMap")
        val exportStartTime = System.nanoTime()
        exportKmerToHapIdMapToBinaryFile(kmerFile,kmerMap)
        val exportEndTime = System.nanoTime()
        myLogger.info("Exported done: ${(exportEndTime - exportStartTime)/1E9} seconds.")

        val importStartTime = System.nanoTime()
        val kmerMapImported = importKmerToHapIdMapFromBinaryFile(kmerFile)
        val importEndTime = System.nanoTime()
        myLogger.info("Importing done: ${(importEndTime - importStartTime)/1E9} seconds")

        Assert.assertEquals("Number of Kmers do not match:",kmerMap.keys.size, kmerMapImported.keys.size)
        for(kmerImported in kmerMapImported.keys) {
            val importedHapIds = kmerMapImported[kmerImported]
            val originalHapIds = kmerMap[kmerImported]

            Assert.assertEquals("HapIds Sizes do not match:",originalHapIds.size,importedHapIds.size)

            for(hapIdIndex in 0 until importedHapIds.size) {
                Assert.assertEquals("HapIds Do Not match:",originalHapIds[hapIdIndex],importedHapIds[hapIdIndex])
            }
        }
    }

    @Test
    fun importBinaryFileTest() {
        val kmerFile = "/Volumes/ZackBackup2018/Temp/PHG/KmerTests/unitTestIndex.bin"

        val importStartTime = System.nanoTime()
        val kmerMapImported = importKmerToHapIdMapFromBinaryFile(kmerFile)
        val importEndTime = System.nanoTime()
        myLogger.info("Importing done: ${(importEndTime - importStartTime)/1E9} seconds")

    }
}