package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pipeline.TasselPipeline
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import org.apache.log4j.Logger
import java.io.File
import java.io.PrintWriter
import java.nio.file.Files

class SimulatedReadsPluginTest {
    val myLogger = Logger.getLogger(SimulatedReadsPluginTest::class.java)

    fun smallSeqTestFromPlugin() {
        LoggingUtils.setupDebugLogging()
        ParameterCache.load(writeConfig())
        TasselPipeline.main(arrayOf("-SimulatedReadsPlugin"))
    }

    fun writeConfig() : String {
//        val filename = Files.createTempFile("config", "tmp").toFile()
        val filename = File("/Users/peterbradbury/temp/config.txt")
        PrintWriter(filename).use {
            it.println("user=sqlite")
            it.println("password=sqlite")
            it.println("host=localhost")
            it.println("DB=/Users/peterbradbury/temp/phgSmallSeq/phgSmallSeq.db")
            it.println("DBtype=sqlite")
            it.println("configFile=${filename}")
            it.println("SimulatedReadsPlugin.minimapPath=/Users/peterbradbury/git/minimap2/minimap2")
//            it.println("SimulatedReadsPlugin.lineName=LineB")
            it.println("SimulatedReadsPlugin.coverage=1.0")
            it.println("SimulatedReadsPlugin.pangenomeIndex=/Users/peterbradbury/temp/phgtest/smallseq_gatk.mmi")
            it.println("SimulatedReadsPlugin.haplotypeMethod=GATK_PIPELINE")
            it.println("SimulatedReadsPlugin.readMethod=TEST_SIMULATION4")
            it.println("SimulatedReadsPlugin.mapReads=false")
            it.println("SimulatedReadsPlugin.fastqDir=/Users/peterbradbury/temp/phgtest/fastq")
            it.println("SimulatedReadsPlugin.pairedEnd=false")
            it.println("SimulatedReadsPlugin.readLength=150")
            it.println("SimulatedReadsPlugin.insertLength=20")
            it.println("SimulatedReadsPlugin.maxRefRangeErr=.2")
            it.println("SimulatedReadsPlugin.maxSecondary=10")
        }
        return filename.toString()
    }

    fun evaluateResults() {

    }
}

fun main() {
    val test = SimulatedReadsPluginTest()
    test.smallSeqTestFromPlugin()
//    test.evaluateResults()
}