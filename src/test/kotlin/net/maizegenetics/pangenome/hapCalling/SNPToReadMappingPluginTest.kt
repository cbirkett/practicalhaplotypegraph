package net.maizegenetics.pangenome.hapCalling

import htsjdk.variant.variantcontext.Genotype
import htsjdk.variant.variantcontext.GenotypeBuilder
import htsjdk.variant.variantcontext.GenotypesContext
import htsjdk.variant.variantcontext.VariantContextBuilder
import htsjdk.variant.variantcontext.writer.Options
import htsjdk.variant.variantcontext.writer.VariantContextWriterBuilder
import htsjdk.variant.vcf.VCFFileReader
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.smallseq.RunSmallSeqTestsDocker.compareResultAndAnswer
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths.*
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.Before
import org.junit.Test
import java.io.File
import java.io.PrintWriter
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDateTime

class SNPToReadMappingPluginTest {

    //get the current time for the method name
    val currentLocalDateTime = LocalDateTime.now()

    //NOTE: Must run IndexHaplotypesBySNPPluginTest first

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun testVCFwithoutAD() {
        //AD is allele depth
        testSNPReadMappings("output.vcf")
    }

    @Test
    fun testVCFwithAD() {
        //if outputVCF_withAD.vcf does not exist, creates it from output.vcf
        val inputVCF = "${outputDir}/output.vcf"
        val outputVCF = "${outputDir}/output_withAD.vcf"
        if (!File(outputVCF).exists()) addDepthToVcf(inputVCF, outputVCF)

        testSNPReadMappings("output_withAD.vcf")
    }

    @Test
    fun testWriteVCFMappingsToDB() {
        testSNPReadMappings("output.vcf", true)
    }

    fun testSNPReadMappings(vcfInputFilename: String, writeToDB: Boolean = false) {

        val userDir = System.getProperty("user.home")
        val keyFile = smallSeqBaseDir + "debugOutput/SNPToReadMappingKeyFile.txt"
        val indexFile = "${userDir}/temp/phgSmallSeq/debugOutput/testVCFIndex_Streaming.txt"
        val vcfDirectory = outputDir
        val debugDir = "${smallSeqBaseDir}debugOutput/"
        val readMappingDir = "${debugDir}readmapping/"
        val methodName = "VCFUpload_${currentLocalDateTime.toString().replace(":","_").replace(",","_").replace(".","_")}"
        val pathMethodName = "path_VCFUpload_${currentLocalDateTime.toString().replace(":","_").replace(",","_").replace(".","_")}"
        val methodDesc = "VCFReadMapping"
        val configFile = dbConfigFile
        val outputPathKeyFile = "${smallSeqBaseDir}debugOutput/pathKeyFile.txt"
        val outputPathDir = "${smallSeqBaseDir}debugOutput/paths/"
        val finalVCFFile = "${smallSeqBaseDir}debugOutput/phg.vcf"

        //create the read mapping directory if it does not already exist
        Files.createDirectories(Paths.get(readMappingDir))
        //erase any old read mapppings to make sure they do not just get re-used
        for (mappingFile in File(readMappingDir).listFiles()) mappingFile.delete()

        ParameterCache.load(configFile)

        Utils.getBufferedWriter(keyFile).use { output ->
            output.write("filename\tflowcell_lane\n")
            output.write("${vcfInputFilename}\tvcfUpload${currentLocalDateTime.toString().replace(":","_").replace(",","_").replace(".","_")}")
        }

        if (writeToDB) {
            SNPToReadMappingPlugin()
                .keyFile(keyFile)
                .vcfDirectory(vcfDirectory)
                .indexFile(indexFile)
                .methodName(methodName)
                .methodDescription(methodDesc)
                .countAD(true)
                .outputPathKeyFile(outputPathKeyFile)
                .performFunction(null)

            println("Done creating ReadMappings")

            println("Done loading mappings to DB.")
        } else {
            SNPToReadMappingPlugin()
                .keyFile(keyFile)
                .vcfDirectory(vcfDirectory)
                .indexFile(indexFile)
                .readMappingOutputDir(readMappingDir)
                .methodName(methodName)
                .methodDescription(methodDesc)
                .countAD(true)
                .outputPathKeyFile(outputPathKeyFile)
                .performFunction(null)

            println("Done creating ReadMappings")

//        //Load in the ReadMappings to the DB
            ImportReadMappingToDBPlugin(null,false)
                .configFileForFinalDB(configFile)
                .outputPathFindingKeyFile(outputPathKeyFile)
                .inputDir(readMappingDir)
                .readMappingMethod(methodName)
                .loadFromDB(false)
                .performFunction(null)

            println("Done loading mappings to DB.")

        }

        val graph = HaplotypeGraphBuilderPlugin(null,false)
                .includeVariantContexts(false)
                .includeSequences(false)
                .configFile(configFile)
                .methods("CONSENSUS")
                .performFunction(null)
        //Then run BestHaplotypePathPlugin
        BestHaplotypePathPlugin(null,false)
                .readMethodName(methodName)
                .pathMethodName(pathMethodName)
                .keyFile(outputPathKeyFile)
                .maxNodesPerRange(30)
                .minTaxaPerRange(1)
                .minReads(0)
                .maxNodesPerRange(1000)
                .minTransitionProb(0.0001)
                .probReadMappedCorrectly(0.99)
                .splitConsensusNodes(true)
                .performFunction(graph)


        //Compare the paths to the paths we know about.
        val graphWithVariants = HaplotypeGraphBuilderPlugin(null,false)
                .includeVariantContexts(true)
                .includeSequences(false)
                .configFile(configFile)
                .methods("CONSENSUS")
                .performFunction(null)

        val paths = ImportDiploidPathPlugin()
            .pathMethodName(pathMethodName)
            .performFunction(graphWithVariants)

        val vcfPlugin = PathsToVCFPlugin(null, false)
        vcfPlugin.setConfigParameters()
        vcfPlugin.outputFile(finalVCFFile)
            .performFunction(paths)

        compareResultAndAnswer(false, SmallSeqPaths.answerDir + "SmallSeq.vcf", finalVCFFile)

    }

    fun addDepthToVcf(inVcf: String, outVcf: String) {
        //use this to add depth to output.vcf for testing
        val vcfFileReader = VCFFileReader(File(inVcf), false)
        val vcfFileWriter = VariantContextWriterBuilder()
            .setOutputFile(outVcf)
            .setOutputFileType(VariantContextWriterBuilder.OutputType.VCF)
            .unsetOption(Options.INDEX_ON_THE_FLY)
            .build()
        vcfFileWriter.writeHeader(vcfFileReader.fileHeader)

        val vcfIterator = vcfFileReader.iterator()
        while (vcfIterator.hasNext()) {
            val myVariantContext = vcfIterator.next()
            val myGenotypeContext = myVariantContext.genotypes
            val myVCBuilder = VariantContextBuilder(myVariantContext)

            val genotypesWithDepth = ArrayList<Genotype>()
            for (geno in myGenotypeContext) {
                val genoBuilder = GenotypeBuilder(geno)
                val myAlleles = geno.alleles
                val ADvalue = intArrayOf(0,0)
                for (alleleIndex in (0..1)) {
                    if (myAlleles[alleleIndex].isReference) ADvalue[0] += 2 else ADvalue[1] += 2
                }
                genoBuilder.AD(ADvalue)
                genotypesWithDepth.add(genoBuilder.make())
            }

            myVCBuilder.genotypes(GenotypesContext.create(genotypesWithDepth))
            vcfFileWriter.add(myVCBuilder.make())
        }

        vcfIterator.close()
        vcfFileReader.close()
        vcfFileWriter.close()
    }
    
}