package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.Multimap
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.graph
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths.*
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.File
import java.time.LocalDateTime
import java.util.stream.Collectors

/**
 * NOTE: This requires minimap2 installed on your system and on the PATH
 */
class SAMToMappingPluginTest {

    //Get the date time to tag the method in the DB so Small seq tests does not have to run each time.
    val currentLocalDateTime = LocalDateTime.now()

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    /**
     * Test loading up the sam file directly.
     */
    @Test
    fun verifySAMLoading() {
        //Create a SAM keyFile
        val fastqKeyFile = genotypingKeyFile
        val outputKeyFile = dataDir + "samKeyFile.txt"
        //Loop through each entry an run minimap2
        val keyFileLines = Utils.getBufferedReader(fastqKeyFile).readLines()

        //Create some directories.
        File(samDir).mkdirs()
        File(samDir+"/logs/").mkdirs()
        File(samDir+"/readMappings/").mkdirs()

        //Create the SAM genotyping keyfile
        Utils.getBufferedWriter(outputKeyFile).use { output ->
            output.write("cultivar\tflowcell_lane\tfilename\tPlateID\n")
            keyFileLines.filter { !it.startsWith("cultivar") }
                    .map { it.split("\t") }
                    .map {
                        //Execute Minimap2 and write new keyfile
                        val fastqFile = it[2]
                        val samFile = fastqFile.split("_")[0]+"_${it[3]}" + ".sam"

                        //Run Minimap2
                        runMinimap2(fastqFile,samFile)

                        "${it[0]}\t${it[1]}\t${samFile}\t${it[3]}"
                    }
                    .forEach { output.write(it + "\n") }
        }

        println("Processing SAMs")

        ParameterCache.load(dbConfigFile)

        //Build a graph to use.
        val graphDS = HaplotypeGraphBuilderPlugin(null, false)
                .methods("CONSENSUS")
                .includeSequences(false)
                .includeVariantContexts(false)
                .configFile(dbConfigFile)
                .performFunction(null)


        //Load in SAMToMappingPlugin
        SAMToMappingPlugin(null,false)
                .samDir(samDir)
                .keyFile(outputKeyFile)
                .lowMemMode(true)
                .methodName("ReadMappingFromSAM_$currentLocalDateTime")
                .methodDescription("Loading the ReadMappings from a SAM File")
                .outputDebugDir("$samDir/readMappings/")
                .performFunction(graphDS)
        println("Done Processing SAMs")

        val phg = PHGdbAccess(DBLoadingUtils.connection(false))
        val readMappingDecoder = ReadMappingDecoder(phg)

        //Load in the path keyfiles and use the following to get the mappings:
        // val readMappings = decodeHapIdMapping(phg.getReadMappingsForId(mappingId))
        val originalMappingKeyFile = Utils.getBufferedReader(dataDir+"genotypingKeyFile_pathKeyFile.txt").readLines()
                .filter { !it.startsWith("SampleName") }.map { it.split("\t") }
                .map { Pair(it[0],it[1]) }
                .toMap()

        val samMappingKeyFile = Utils.getBufferedReader(dataDir + "samKeyFile_pathKeyFile.txt").readLines()
                .filter { !it.startsWith("SampleName") }.map { it.split("\t") }
                .map { Pair(it[0],it[1]) }
                .toMap()

        Assert.assertEquals("Number of Taxon in keyfiles do not match", originalMappingKeyFile.keys.size,samMappingKeyFile.keys.size)
        Assert.assertEquals("All Keys in original are not in sam: ",originalMappingKeyFile.keys.size,originalMappingKeyFile.keys.filter { samMappingKeyFile.keys.contains(it) }.count())
        Assert.assertEquals("All Keys in sam are not in original: ",samMappingKeyFile.keys.size,samMappingKeyFile.keys.filter { originalMappingKeyFile.keys.contains(it) }.count())

        //Test each taxon to see if the ReadMappings are consistent.
        originalMappingKeyFile.keys.forEach { taxon ->
            println("Testing Taxon: $taxon ${originalMappingKeyFile[taxon]} ${samMappingKeyFile[taxon]}")

            val originalReadMappings = readMappingDecoder.getDecodedReadMappingForMappingId(originalMappingKeyFile[taxon]?.toInt() ?: -1)
            val samReadMappings = readMappingDecoder.getDecodedReadMappingForMappingId(samMappingKeyFile[taxon]?.toInt()?:-1)

            //Map<List<Int>,Int>
            //Check to see if the hapIdSets are equivalent
            Assert.assertEquals("Original Read Mapping HapId Sets are missing from SAM: $taxon",originalReadMappings.keys.size,originalReadMappings.keys.filter{samReadMappings.keys.contains(it)}.count())
            Assert.assertEquals("SAM Read Mapping HapId Sets are missing from Original: $taxon",samReadMappings.keys.size,samReadMappings.keys.filter{originalReadMappings.keys.contains(it)}.count())

            originalReadMappings.keys.forEach { hapIdList ->
                val origCount = originalReadMappings[hapIdList]
                val samCount = samReadMappings[hapIdList]

                Assert.assertEquals("Counts are different for hapIds:$hapIdList",origCount,samCount)
            }

        }

        //Get the first and last ids so we can force it to make errors.
        val firstId = samMappingKeyFile[samMappingKeyFile.keys.first()]?.toInt()?:-1
        val lastId = samMappingKeyFile[samMappingKeyFile.keys.last()]?.toInt()?:-1

        //Make sure everything fails:
        originalMappingKeyFile.keys.forEach { taxon ->
            println("Testing Taxon For errors: $taxon ${originalMappingKeyFile[taxon]} ${samMappingKeyFile[taxon]}")

            val samId = samMappingKeyFile[taxon]?.toInt()?:-1

            val originalReadMappings = readMappingDecoder.getDecodedReadMappingForMappingId(originalMappingKeyFile[taxon]?.toInt()?:-1)
            //Force it to have an incorrect mapping
            val samReadMappings = if(samId != firstId) {
                readMappingDecoder.getDecodedReadMappingForMappingId(firstId)
            }
            else {
                readMappingDecoder.getDecodedReadMappingForMappingId(lastId)
            }

            var allSame = true
            originalReadMappings.keys.forEach { hapIdList ->
                val origCount = originalReadMappings[hapIdList]
                val samCount = samReadMappings[hapIdList]

                if(origCount != samCount) {
                    allSame = false
                }
            }
            Assert.assertFalse("Counts are the same for all hapIds:$taxon",allSame)

        }
        phg.close()

    }

    fun runMinimap2(fastqFile : String, samFile : String) {
        val indexBuilder = ProcessBuilder("minimap2","-ax", "sr", "-t", "10", "--secondary=yes","-N20", "--eqx", pangenomeDir+"phgSmallSeqSequence.mmi", dataDir+ fastqFile)

        val indexRedirectOutput = samDir + samFile
        val indexRedirectError = samDir+"/logs/" + samFile +"_error.log"
        indexBuilder.redirectOutput(File(indexRedirectOutput))
        indexBuilder.redirectError(File(indexRedirectError))

        println("runMinimap2 command: " + indexBuilder.command().stream().collect(Collectors.joining(" ")))
        val indexProcess = indexBuilder.start()
        indexProcess.waitFor()
    }
}