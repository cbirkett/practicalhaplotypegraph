package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.pangenome.pipeline.ImputePipelinePlugin
import net.maizegenetics.pangenome.smallseq.RunSmallSeqTestsDocker
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.File
import java.time.LocalDateTime

class MultsampleBAMToMappingPluginTest {

    //get the current time for the method name
    val currentLocalDateTime = LocalDateTime.now()
    val outputGroupingFile = "/Users/zrm22/temp/phgSmallSeq/multisampleBamTest/groupingFile.txt"
    val outputMergedBAMDir = "/Users/zrm22/temp/phgSmallSeq/multisampleBamTest/bam/"
    val imputationKeyFile = "/Users/zrm22/temp/phgSmallSeq/multisampleBamTest/genotypingKeyFile.txt"
    val imputationKeyFileOriginal = "/Users/zrm22/temp/phgSmallSeq/dockerBaseDir/genotypingKeyFile.txt"
    val pathImputationKeyFile = "/Users/zrm22/temp/phgSmallSeq/multisampleBamTest/genotypingKeyFile_pathKeyFile.txt"
    val outputVCFFile = "/Users/zrm22/temp/phgSmallSeq/multisampleBamTest/multisampleBAMSNPs.vcf"
    val outputDebugDir = "/Users/zrm22/temp/phgSmallSeq/multisampleBamTest/debug/"


    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()

        //copy the key file
        File(imputationKeyFileOriginal).copyTo(File(imputationKeyFile),true)

        //Merge together the fastqs from the SmallSeqTests
        //We can use the old keyfile
        val outputMergedDir = "/Users/zrm22/temp/phgSmallSeq/multisampleBamTest/fastq/"

        val scriptTemplate = "/Users/zrm22/temp/phgSmallSeq/multisampleBamTest/sampleScript.sh"
        val referenceFile = "/Users/zrm22/temp/phgSmallSeq/dockerBaseDir/pangenome/pangenome_CONSENSUS_k21w11I90G.mmi"
        File(outputMergedDir).deleteRecursively()
        File(outputMergedBAMDir).deleteRecursively()
        File(outputDebugDir).deleteRecursively()
        File(outputMergedDir).mkdirs()
        File(outputMergedBAMDir).mkdirs()
        File(outputDebugDir).mkdirs()
        MergeFastqPlugin()
//                .fastqDir("/Users/zrm22/temp/phgSmallSeq/dockerBaseDir/inputDir/loadDB/fastq/")
                .fastqDir("/Users/zrm22/temp/phgSmallSeq/dockerBaseDir/data/")
                .outputMergedFastqDir(outputMergedDir)
                .outputGroupingFile(outputGroupingFile)
                .numberOfFastqsPerMerge(3)
                .useOriginalReadNames(false)
                .scriptTemplate(scriptTemplate)
                .performFunction(null)

        //align the merged fastqs to the phg made by small seq tests
        File(outputMergedDir).walk()
                .filter { it.isFile }
                .filter { !it.isHidden }
                .forEach {
                    println(it)
                    val command = arrayOf("minimap2", "-ax", "sr", "-t", "4", "--secondary=yes","-N20", "--eqx", referenceFile, it.toString())

                    val minimap2Process = ProcessBuilder(*command)
                            .redirectOutput(File("$outputMergedBAMDir/${it.nameWithoutExtension}.sam"))
                            .redirectError(ProcessBuilder.Redirect.INHERIT)
                            .start()
                    minimap2Process.waitFor()
                }

    }

    @Test
    fun testLoadingSmallSeq() {
        val graph = HaplotypeGraphBuilderPlugin(null, false)
                .includeVariantContexts(false)
                .includeSequences(false)
                .methods("CONSENSUS")
                .configFile("/Users/zrm22/temp/phgSmallSeq/dockerBaseDir/configSQLite.txt")
                .performFunction(null)

        ParameterCache.load("/Users/zrm22/temp/phgSmallSeq/dockerBaseDir/configSQLite.txt")


        MultisampleBAMToMappingPlugin().keyFile(imputationKeyFile)
                .fastqGroupingFile(outputGroupingFile)
                .samDir(outputMergedBAMDir)
                .methodName("multisampleBAMImpute_${currentLocalDateTime}")
                .methodDescription("Using Multisample BAM read mapping")
                .outputDebugDir(outputDebugDir)
                .outputSecondaryMappingStats(false)
                .performFunction(graph)

        verifyReadMappings()



        BestHaplotypePathPlugin(null, false)
                .keyFile(pathImputationKeyFile)
                .readMethodName("multisampleBAMImpute_${currentLocalDateTime}")
                .pathMethodName("multisampleBAMImputePath_${currentLocalDateTime}")
                .maxNodesPerRange(30)
                .minReads(0)
                .minTransitionProb(0.001)
                .probReadMappedCorrectly(0.99)
                .minTaxaPerRange(1)
                .splitConsensusNodes(true)
                .performFunction(graph)


        val graphDataSetForPath = HaplotypeGraphBuilderPlugin(null, false)
                .methods("CONSENSUS")
                .includeSequences(false)
                .includeVariantContexts(true)
                .configFile("/Users/zrm22/temp/phgSmallSeq/dockerBaseDir/configSQLite.txt")
                .performFunction(null)
        val paths = ImportDiploidPathPlugin()
                .pathMethodName("multisampleBAMImputePath_${currentLocalDateTime}")
                .performFunction(graphDataSetForPath)
        PathsToVCFPlugin(null, false)
                .outputFile(outputVCFFile)
                .performFunction(paths)

        RunSmallSeqTestsDocker.compareResultAndAnswer(false, SmallSeqPaths.answerDir + "SmallSeq.vcf", outputVCFFile)

    }

    private fun verifyReadMappings() {
        //Get out the old readMappings

        var dbConnect = DBLoadingUtils.connection("/Users/zrm22/temp/phgSmallSeq/dockerBaseDir/configSQLite.txt", false)
        val phgdb = PHGdbAccess(dbConnect)
        val readMappingDecoder = ReadMappingDecoder(phgdb)
        val mappingIdFile = "/Users/zrm22/temp/phgSmallSeq/dockerBaseDir/genotypingKeyFile_withMappingIds.txt"
        val mappingNewFileNameToReadMap = Utils.getBufferedReader(mappingIdFile).readLines().filter { !it.startsWith("cultivar") }.map { it.split("\t") }
            .map { Pair("${it[0]}_${it[1]}_ReadMapping.txt", readMappingDecoder.getDecodedReadMappingForMappingId(it[4].toInt()) ) }
            .toMap()
        //Compare the two sets
        File(outputDebugDir).walkTopDown()
                .filter { it.isFile }
                .filter { !it.isHidden }
                .forEach { newFile ->
                    val name = newFile.name
                    Assert.assertTrue("Unable to find new file name ${name}.", mappingNewFileNameToReadMap.containsKey(name))
                    val oldMappingSet = mappingNewFileNameToReadMap[name]

                    val newMappingSet = Utils.getBufferedReader(newFile.toString()).readLines()
                            .filter { !it.startsWith("#") }
                            .filter { !it.startsWith("HapIds") }
                            .map { it.split("\t") }
                            .map { Pair(it[0].split(",").map { number -> number.toInt() }.toList(), it[1].toInt()) }
                            .toMap()
                    for (mapping in newMappingSet) {
                        //Check to make sure we have all the values
                        Assert.assertTrue("Old Mapping missing  new set: ${mapping.key}", oldMappingSet!!.containsKey(mapping.key))

                        //Check the values as well.
                        Assert.assertEquals("Counts do not match.  New method: ${mapping.value}, Old Method: ${oldMappingSet[mapping.key]}", mapping.value, oldMappingSet[mapping.key])
                    }

                    for (oldMapping in oldMappingSet!!) {
                        //Check to make sure we have all the values
                        Assert.assertTrue("New Mapping missing old set: ${oldMapping.key}", newMappingSet.containsKey(oldMapping.key))

                        //Check the values as well.
                        Assert.assertEquals("Old Counts do not match.  New method: ${newMappingSet[oldMapping.key]}, Old Method: ${oldMapping.value}", newMappingSet[oldMapping.key], oldMapping.value)
                    }

                }

        phgdb.close()
    }
}