package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultimap
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.ReferenceRange
import org.junit.Test
import kotlin.test.assertEquals

class MostLikelyParentTest {
    @Test
    fun testLikelyParents() {
        //input data: parents list  (List<String)
        //a hapid -> parent map
        //a multimap of refrange to hapIdSetCounts (Multimap<ReferenceRange, HapIdSetCount>)
        val parentList = listOf("A","B","C","D","E")
        val myChrom = Chromosome.instance(1)
        val refrangeList = (1..3).map { ReferenceRange("ref", myChrom, 1 + it * 10, 10 + it * 10, it) }
        var nextHapid = 1
        val refrangeToHapidMap = refrangeList.associateWith { parentList.associateWith{
            nextHapid++
        } }

        //create a read mapping
        val refRangeToSetCounts = HashMultimap.create<ReferenceRange, HapIdSetCount>()

        var rr = refrangeList[0]
        var hapidMap = refrangeToHapidMap[rr]!!
        refRangeToSetCounts.put(rr, HapIdSetCount(setOf((hapidMap["A"]!!),hapidMap["B"]!!), 4))
        refRangeToSetCounts.put(rr, HapIdSetCount(setOf((hapidMap["C"]!!),hapidMap["D"]!!), 4))
        refRangeToSetCounts.put(rr, HapIdSetCount(setOf((hapidMap["A"]!!),hapidMap["E"]!!), 2))

        rr = refrangeList[1]
        hapidMap = refrangeToHapidMap[rr]!!
        refRangeToSetCounts.put(rr, HapIdSetCount(setOf((hapidMap["D"]!!),hapidMap["B"]!!), 3))
        refRangeToSetCounts.put(rr, HapIdSetCount(setOf((hapidMap["C"]!!),hapidMap["A"]!!), 4))
        refRangeToSetCounts.put(rr, HapIdSetCount(setOf((hapidMap["B"]!!),hapidMap["C"]!!), 2))

        rr = refrangeList[1]
        hapidMap = refrangeToHapidMap[rr]!!
        refRangeToSetCounts.put(rr, HapIdSetCount(setOf((hapidMap["A"]!!),hapidMap["B"]!!), 3))
        refRangeToSetCounts.put(rr, HapIdSetCount(setOf((hapidMap["C"]!!),hapidMap["E"]!!), 4))
        refRangeToSetCounts.put(rr, HapIdSetCount(setOf((hapidMap["A"]!!),hapidMap["D"]!!), 2))

        //First: A-15, B-12, C-14, D-9, E-6 -> A-15
        //After A: B-9, C-10, D-7, E-4 -> C-10
        //After A,C: B-3, D-0, E-0-> B-3

        val parentCounter = MostLikelyParents(parentList, refrangeToHapidMap)
        val counts = parentCounter.findMostLikelyParents(refRangeToSetCounts, 10, 1.0)
        println("-------------")
        for (count in counts) println("${count.first} : ${count.second}")
        println("-------------")

        assertEquals(Pair("A",15), counts[0], "Expected A = 15")
        assertEquals(Pair("C",10), counts[1], "Expected C = 10")
        assertEquals(Pair("B",3), counts[2], "Expected B = 3")

        assertEquals(3, counts.size, "Expected counts size = 3")

        val counts2 = parentCounter.findMostLikelyParents(refRangeToSetCounts, 2, 1.0)
        assertEquals(2, counts2.size, "Expected counts2 size = 2")

        val counts3  = parentCounter.findMostLikelyParents(refRangeToSetCounts, 10, 0.85)
        assertEquals(2, counts2.size, "Expected counts3 size = 2")
    }

}