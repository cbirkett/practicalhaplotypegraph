package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.File

class ImportReadMappingToDBPluginTest {
    //Set up the global variables
    val workDir = SmallSeqPaths.outputDir
    val minimap2Location = System.getProperty("user.home")+"/minimap2/minimap2"

    val testingDir = workDir+"/ImportReadMappingTests/"
    val dbDir = workDir+"/ImportReadMappingTests/DBs/"
    val configDir = workDir+"/ImportReadMappingTests/ConfigFiles/"
    val keyFileDir = workDir+"/ImportReadMappingTests/KeyFiles/"
    val readMappingFileDir = "$workDir/ImportReadMappingTests/ReadMappingFiles/"
    val originalDB = File("$workDir/phgSmallSeq.db")

    val numberOfDBs = 2

    val readMappingMap = mutableMapOf<Pair<String,String>,ByteArray>()

    /**
     * Setup the logging
     *
     * Create Test Genome Files
     *
     * Create Test KeyFile
     */
    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()

        //Copy the DB and write a new config file and a new key file using existing fastqs, but rename them
        File(testingDir).deleteRecursively()
        File(dbDir).mkdirs()
        File(configDir).mkdirs()
        File(keyFileDir).mkdirs()
        File(readMappingFileDir).mkdirs()

        originalDB.copyTo(File(testingDir+"phgSmallSeq.db"))
        originalDB.copyTo(File(testingDir+"phgSmallSeq_FileLoadDB.db"))
        Utils.getBufferedWriter(testingDir+"config.txt").use { output ->
            output.write("host=localHost\n")
            output.write("user=sqlite\n")
            output.write("password=sqlite\n")
            output.write("DB=${testingDir}phgSmallSeq.db\n")
            output.write("DBtype=sqlite\n")
        }
        Utils.getBufferedWriter(testingDir+"config_FileLoadDB.txt").use { output ->
            output.write("host=localHost\n")
            output.write("user=sqlite\n")
            output.write("password=sqlite\n")
            output.write("DB=${testingDir}phgSmallSeq_FileLoadDB.db\n")
            output.write("DBtype=sqlite\n")
        }

        Utils.getBufferedWriter(testingDir+"keyFile.txt").use { output ->
            output.write("cultivar\tflowcell_lane\tfilename\tPlateID\n")
            output.write("LineB1_wgs\twgsFlowcell\tLineB1_R1.fastq\twgs\n")
            output.write("LineA_wgs\twgsFlowcell\tLineA_R1.fastq\twgs\n")
        }

        //Load the config file
        ParameterCache.load(testingDir+"config.txt")

        val graph = HaplotypeGraphBuilderPlugin(null,false)
                .configFile(testingDir+"config.txt")
                .methods("CONSENSUS")
                .includeSequences(false)
                .includeVariantContexts(false)
                .performFunction(null)
        //Run the fastqs through FastqToMappingPlugin
        FastqToMappingPlugin(null,false)
                .indexFile("${workDir}pangenome/phgSmallSeqSequence.mmi")
                .fastqDir(workDir+"data/")
                .keyFile(testingDir+"keyFile.txt")
                .methodName("TestMergeMethod")
                .methodDescription("Method to test merging the DBs together")
                .minimapLocation(minimap2Location)
                .outputDebugDir(readMappingFileDir) //Using the output Debugs so I can test file uploads
                .performFunction(graph)


        //Make the configs for the dbs to be merged into the original one.
        (0 until numberOfDBs).forEach {

            originalDB.copyTo(File(dbDir+"phgSmallSeq_${it}.db"))
            Utils.getBufferedWriter(configDir + "config_${it}.txt").use { output ->
                output.write("host=localHost\n")
                output.write("user=sqlite\n")
                output.write("password=sqlite\n")
                output.write("DB=${dbDir}phgSmallSeq_${it}.db\n")
                output.write("DBtype=sqlite\n")
            }

            //Write out a key file for each config
            Utils.getBufferedWriter(keyFileDir+"keyFile_${it}.txt").use { output ->
                output.write("cultivar\tflowcell_lane\tfilename\tPlateID\n")
                output.write("LineB1_wgs\twgsFlowcell_${it}\tLineB1_R1.fastq\twgs\n")
                output.write("LineA_wgs\twgsFlowcell_${it}\tLineA_R1.fastq\twgs\n")

            }

            //Load the config file
            ParameterCache.load(configDir + "config_${it}.txt")

            val graph = HaplotypeGraphBuilderPlugin(null,false)
                    .configFile(configDir + "config_${it}.txt")
                    .methods("CONSENSUS")
                    .includeSequences(false)
                    .includeVariantContexts(false)
                    .performFunction(null)
            //Run the fastqs through FastqToMappingPlugin
            FastqToMappingPlugin(null,false)
                    .indexFile("${workDir}pangenome/phgSmallSeqSequence.mmi")
                    .fastqDir(workDir+"data/")
                    .keyFile(keyFileDir+"keyFile_${it}.txt")
                    .methodName("TestMergeMethod")
                    .methodDescription("Method to test merging the DBs together")
                    .minimapLocation(minimap2Location)
                    .outputDebugDir(readMappingFileDir) //Using the output Debugs so I can test file uploads
                    .performFunction(graph)
        }
        verifyNumberFastqsAdded()
    }

    /**
     * Function to check to see if we added the correct number of fastq mappings in the initialization code.
     */
    fun verifyNumberFastqsAdded() {
        try {
            var dbConnect = DBLoadingUtils.connection(testingDir + "config.txt", false)
            var phg = PHGdbAccess(dbConnect)

            //get the readMapping ids for this method name
            var mappings = phg.getReadMappingsForMethod("TestMergeMethod")
            mappings.forEach {
                readMappingMap[Pair(it.genoName,it.fileGroupName)] = it.readMappings
            }
            println("Original DB Size: ${mappings.size}")
            Assert.assertEquals("Original DB does not have the correct number of added readMappings",2,mappings.size)
            phg.close()

            (0 until numberOfDBs).forEach {
                dbConnect = DBLoadingUtils.connection(configDir + "config_${it}.txt", false)
                phg = PHGdbAccess(dbConnect)

                //get the readMapping ids for this method name
                mappings = phg.getReadMappingsForMethod("TestMergeMethod")
                mappings.forEach {readMappingRecord ->
                    readMappingMap[Pair(readMappingRecord.genoName,readMappingRecord.fileGroupName)] = readMappingRecord.readMappings
                }
                println("DB_${it} Size: ${mappings.size}")
                Assert.assertEquals("DB_$it does not have the correct number of added readMappings",2,mappings.size)
                phg.close()
            }

        }
        catch (exc: Exception) {
            println(exc)
        }
    }

    /**
     * Function to test if ImportReadMappingToDBPlugin will work.
     *
     * After merging it will check to see the number of readMappings for the method and then will also check to see if the mappings themselves are the same.
     *
     * TODO check the keyfile to make sure it is what is expected.
     */
    @Test
    fun testMergingDBs() {
        ImportReadMappingToDBPlugin(null,false)
                .configFileForFinalDB(testingDir + "config.txt")
                .inputDir(configDir)
                .loadFromDB(true)
                .readMappingMethod("TestMergeMethod")
                .outputPathFindingKeyFile(testingDir+"keyFile.txt")
                .performFunction(null)

        var dbConnect = DBLoadingUtils.connection(testingDir + "config.txt", false)
        var phg = PHGdbAccess(dbConnect)

        //get the readMapping ids for this method name
        var mappings = phg.getReadMappingsForMethod("TestMergeMethod")
        println("Original DB Size: ${mappings.size}")
        Assert.assertEquals("MergedDB does not have the correct number of read mappings:",2 + 2*numberOfDBs,mappings.size)


        mappings.forEach {
            val currentName = Pair(it.genoName,it.fileGroupName)
            Assert.assertArrayEquals("MergedDB does not have the same array",readMappingMap[currentName],it.readMappings)
        }


        phg.close()
    }

    @Test
    fun testUploadingFiles() {
        //Use ImportReadMappingToDBPlugin
        ImportReadMappingToDBPlugin(null,false)
                .configFileForFinalDB(testingDir+"config_FileLoadDB.txt")
                .inputDir(readMappingFileDir)
                .loadFromDB(false)
                .readMappingMethod("TestMergeMethod")
                .outputPathFindingKeyFile(testingDir+"keyFile.txt")
                .performFunction(null)

        var dbConnect = DBLoadingUtils.connection(testingDir + "config_FileLoadDB.txt", false)
        var phg = PHGdbAccess(dbConnect)

        //Check to see if we have the correct number of read mappings and that they match what we imported
        var mappings = phg.getReadMappingsForMethod("TestMergeMethod")
        println("Original DB Size: ${mappings.size}")
        Assert.assertEquals("MergedDB does not have the correct number of read mappings:",2 + 2*numberOfDBs,mappings.size)


        mappings.forEach {
            val currentName = Pair(it.genoName,it.fileGroupName)
            Assert.assertArrayEquals("MergedDB does not have the same array",readMappingMap[currentName],it.readMappings)
        }


        phg.close()
    }
}