package net.maizegenetics.pangenome.annotations

import com.google.common.collect.Range
import com.google.common.collect.RangeSet
import com.google.common.collect.TreeRangeSet
import htsjdk.tribble.gff.Gff3Feature
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.Position
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.taxa.TaxaList
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.Before
import org.junit.Test
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.test.assertEquals
import kotlin.test.assertFails

class GFFtests {

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun readGFFhtsjdk() {
        val gffFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/smallGeneGff.txt"

        // call gffReader - see if it works
        println("Test - before readGFF call\n")
        var gffFeatureSet = readGFFtoGff3Feature(gffFile)
        println("\nDOne - after readGFF call")

        // THis does skip over the headers.
        var gffFileWithHeaders = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/smallFullGff_withHeaders_withTE_B73.txt"
        println("\nTest - before gffWithHeaders call\n")
        gffFeatureSet = readGFFtoGff3Feature(gffFileWithHeaders)

        //test what the attributes look like
        println("\nHere are the attributes - how are they stored, StringToList is comma separated values, size of gffFeatures: ${gffFeatureSet.size}")
        for (feature in gffFeatureSet) {
            val attributes = feature.attributes
            println("attributes size = ${attributes.entries.size}")

            println(attributes.toString())
        }
        println("\nafter 2nd gffWithHeaders call")
        gffFileWithHeaders = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/namGffs/Zm-CML103-REFERENCE-NAM-1.0_Zm00021ab.1.gff3"

        gffFeatureSet = readGFFtoGff3Feature(gffFileWithHeaders)
        println("Entries in gffFeatues from CML103gff: ${gffFeatureSet.size}")

    }

    @Test
    fun testGff3FeatureOverlaps() {
        val time = System.nanoTime()
        val keyFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/keyFileJunit.txt"
        println("\nTest - before gffWithHeaders call\n")
        val taxonToGFFfileMap = getTaxonToGffFile(keyFile)

        val centerGffs:MutableMap<String, TreeMap<Position, ArrayList<Gff3Feature>>> = mutableMapOf()

        for ((taxon, gffFile) in taxonToGFFfileMap) {
            val time = System.nanoTime()
            val features = readGFFtoGff3Feature(gffFile)
            val featureTreeMapCenter = createTreeMapFromFeaturesCenter(features)
            centerGffs.put(taxon, featureTreeMapCenter)
            val endTime = (System.nanoTime() - time)/1e9
            println("loadGffsToGff3Feature: time to load ${taxon} gff file: ${endTime}")
        }

        val name = "B73"
        // THe center of this range will not be a key in the map.
        // This verifies the code picks the next highest key and
        // that appropriate entries are identified.
        val testRange = 43067..43100
        val asmCenterGffEntries = centerGffs.get(name)
        println("entries in asmStartGffEntries: ${asmCenterGffEntries?.keys?.size}")
        for (key in asmCenterGffEntries!!.keys) {
            println("  arrayKey: ${key.toString()}")
            if (key.position == 43118) {
                val values = asmCenterGffEntries.get(key)
                println("Size of values for 43118: ${values?.size}")
            }
        }

        if (asmCenterGffEntries == null || asmCenterGffEntries.size == 0) {
            assertFails{"No values in the asmCenterGffEntries map !!!"}
        }

        val chrom = Chromosome.instance("chr1")

        val pseudoGffEntries = getOverlappingEntriesFromGff("chr1", testRange, asmCenterGffEntries!!)
        println("Number of pseudoGffEntries found for B73-chr1 with testRange ${testRange}: ${pseudoGffEntries.size} ")
        for (entry in pseudoGffEntries) {
            println("${entry.contig} ${entry.start} ${entry.end} ${entry.type}")
        }

        // is 4 if keyfile is using B73 taxon with gff file smallFullGff_withHeaders_chr1chr2_B73.txt
        assertEquals(4, pseudoGffEntries.size)
        val elapsedTime = (System.nanoTime() - time)/1e9
        println("\n FInished test in ${elapsedTime} seconds")

    }

    @Test
    fun testGetPseudoGenomeGFFCoordinates() {

        // haplotype asm starts before the gff entry, asm ends before gff entry, 0 offset
        var gffCoords = 134374229..134374722
        var hapAsmCoords = 134370278..134374620
        var offset = 0

        var expectedResult = 3951..4342
        var pseudoGenomeCoords = getPseudoGenomeGFFCoordinates(gffCoords, hapAsmCoords, offset)
        assertEquals(expectedResult, pseudoGenomeCoords)

        // same as above, but offset from start of chrom is 1000
        offset = 1000
        expectedResult = 4951..5342
        pseudoGenomeCoords = getPseudoGenomeGFFCoordinates(gffCoords, hapAsmCoords, offset)
        assertEquals(expectedResult, pseudoGenomeCoords)

        // haplotype asm start is after the gff entry, ends after the gff entry (beginning
        // is chopped off, but includes all the end).  no offset
        offset = 0
        gffCoords = 155822773..155823423
        hapAsmCoords = 155823019..155827364
        expectedResult = 1..405
        pseudoGenomeCoords = getPseudoGenomeGFFCoordinates(gffCoords, hapAsmCoords, offset)
        assertEquals(expectedResult, pseudoGenomeCoords)


        // full gff entry is embedded in the haplotype node sequence (haplotype start is
        // less than gff start, and haplotype end is greated than gff end)
        gffCoords = 142564613..142564838
        hapAsmCoords = 142564609..142570362
        expectedResult = 4..229
        pseudoGenomeCoords = getPseudoGenomeGFFCoordinates(gffCoords, hapAsmCoords, offset)
        assertEquals(expectedResult, pseudoGenomeCoords)

        // asm gff and haplotype asm coordinates are the same
        gffCoords = 183334395..183338622
        hapAsmCoords = 183334395..183338622
        expectedResult = 1..4228
        pseudoGenomeCoords = getPseudoGenomeGFFCoordinates(gffCoords, hapAsmCoords, offset)
        assertEquals(expectedResult, pseudoGenomeCoords)

        // as above, offset is 250
        offset = 250
        expectedResult = 251..4478
        pseudoGenomeCoords = getPseudoGenomeGFFCoordinates(gffCoords, hapAsmCoords, offset)
        assertEquals(expectedResult, pseudoGenomeCoords)

    }
    @Test
    fun testGetPathsForTaxonMethod() {
        //val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/config.txt"
        //val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/config_dc01_backupMaizeDB.txt"
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/config_dc01_general.txt"
        val dbConn = DBLoadingUtils.connection(configFile, false)
        val phg = PHGdbAccess(dbConn)
        val taxon= "B97_Assembly"
        val method = "mummer4_PATH"
        val taxonList: MutableList<String> = ArrayList<String>()
        taxonList.add(taxon)
        // calls: Map<String,List<Integer>> getPathsForTaxonMethod(List<String> taxon, String method_name);
        val paths = phg.getPathsForTaxonMethod(taxonList, method);

        println("number of paths returned: ${paths.keys.size}")
        for ((taxon, hapList) in paths) {
            println("number of paths for ${taxon} is ${hapList.size}")
            println("first 10 hapids for ${taxon}:")
            for (idx in 0..10) {
                println("hapid-${idx} = ${hapList[0][idx]}")
            }
        }

        println("\n Done with test!!")
    }

    @Test
    fun testPathToGFFPlugin() {
        //val keyFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/keyFileMichelle.txt"
        val keyFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/keyFile.txt"
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/config.txt"
        val method = "mummer4_PATH"
        val taxa = "B97_Assembly"
       // val method = "michelle_b73_b97_path"
        //val taxa = "NAM_Z001E0001,NAM_Z001E0002,NAM_Z001E0003,NAM_Z001E0004,NAM_Z001E0005"
        //val gffOutputDir = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/michelle_gffOutput"

        val taxaList = createTaxaListFromFileOrString(taxa)

        val gffOutputDir = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/"
        val time = System.nanoTime()
        ParameterCache.load(configFile)
        PathToGFFPlugin(null, false)
                .keyFile(keyFile)
                .configFile(configFile)
                .method(method)
                .taxa(taxaList)
                .gffOutputDir(gffOutputDir)
                .performFunction(null);
        val endingTime = (System.nanoTime() - time)/1e9
        println("\nfinished testPathToGFFPlugin in ${endingTime} seconds")

    }

    @Test
    fun testHtsjdkReadWriteRoundtrip() {
        // THis is here to test why I'm getting an ID=1 on chr2, and ID=2 on chr3
        // Problem appears to be my code, not htsjdk - this all looks fine.
        println("Reading file ...")
        val gff3Entries =  readGFFtoGff3Feature("/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/namGffs/Zm-B97-REFERENCE-NAM-1.0_Zm00018ab.1.gff3")
        val entrySet = gff3Entries.toSet()
        println("Writing file ...")
        val outputFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/testB97_IDsFromWrite.gff3"
        writeGffFile(outputFile, entrySet, null, null)
        println("File written to: ${outputFile}")
    }

    @Test
    fun testCountGffEntriesPerChrom() {
        // Michelle would like to create the gff's in memory, then count
        // how many gffs entries occur for each chromosome, and do some other
        // metrics.  THis test case shows examples of running these metrics
        val keyFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/keyFileMichelle.txt"
        //val keyFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/keyFile.txt"
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/config.txt"
        //val method = "mummer4_PATH"
        //val taxa = "B97_Assembly"
        val method = "michelle_b73_b97_path"
        //val taxa = "NAM_Z001E0001,NAM_Z001E0002,NAM_Z001E0003,NAM_Z001E0004,NAM_Z001E0005"
        val taxa = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/taxaList.txt"
        //val gffOutputDir = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/michelle_gffOutput"

        val gffOutputDir = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing_oct2021/"
        val taxaList = createTaxaListFromFileOrString(taxa)
        //val gffOutputDir = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/"
        var time = System.nanoTime()
        ParameterCache.load(configFile)
        val dataSet = PathToGFFPlugin(null, false)
                .keyFile(keyFile)
                .configFile(configFile)
                .method(method)
                .taxa(taxaList)
                .gffOutputDir(gffOutputDir) // could write to file if want extra debug
                .performFunction(null);
        var endingTime = (System.nanoTime() - time)/1e9
        println("\nfinished PathToGffPlugin in ${endingTime} seconds")

        if (dataSet.dataSet.size == 0) {
            // error here
            assertFails{"no data returned from PathToGFFPlugin"}
        }

        // This should return a MutableMap<String, List<Set<Gff3Feature>>>
        // or perhaps just a Map<String, List<Set<Gff3Feature>>>
        // This is a List<> due to diploid handling
        val taxonGffMap = dataSet.getDataWithName("TaxonGFFmap").get(0).getData() as Map<String, List<Set<Gff3Feature>>>

        if (taxonGffMap == null) {
            assertFails {"No TaxonPHGmap returned from PathToGffPlugin"}
        }
        println("Start analysis of GFFs")

        for ((taxon, gffList) in taxonGffMap) {
            println("\nProcessing taxon: ${taxon}")

            // For our purposes, there should only be 1 set of gffs, so just get the first one
            val gffSet = gffList.get(0)

            // Get counts for a specific ID value.
            val idToMatch = "RST00010"
            val idCount = gffSingleIDcount(idToMatch, gffSet)
            println("Number of ${idToMatch}: ${idCount}")

            // Get counts of gff entries per chrom
            val chromCount = getGFFEntriesPerChrom(gffSet)
            println("counts for each chrom:")
            for ((chrom, count) in chromCount) {
                println(" ${chrom}: ${count}")
            }
        }

    }

    @Test
    fun testMapOfChromToDistinctId() {

        // This is an example for Michelle - how she can use this code.
        val keyFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/keyFileMichelle.txt"
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/config.txt"
        val method = "michelle_b73_b97_path"
        val taxa = "NAM_Z001E0001"

        // You can also pass "taxa" instead of "taxaList" to the plugin and it will work on the string
        val taxaList = createTaxaListFromFileOrString(taxa)

        //val gffOutputDir = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/"
        val gffOutputDir = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing_oct2021/"
        var time = System.nanoTime()
        ParameterCache.load(configFile)
        val dataSet = PathToGFFPlugin(null, false)
                .keyFile(keyFile)
                .configFile(configFile)
                .method(method)
                .taxa(taxa)
                .gffOutputDir(gffOutputDir) // could write to file if want extra debug
                .performFunction(null);
        var endingTime = (System.nanoTime() - time) / 1e9
        println("\nfinished PathToGffPlugin in ${endingTime} seconds")

        val taxonGffMap = dataSet.getDataWithName("TaxonGFFmap").get(0).getData() as Map<String, List<Set<Gff3Feature>>>

        if (taxonGffMap == null) {
            assertFails {"No TaxonPHGmap returned from PathToGffPlugin"}
        }
        println("Start analysis of GFFs")

        for ((taxon, gffList) in taxonGffMap) {
            // For our purposes, there should only be 1 set of gffs, so just get the first one
            val gffSet = gffList.get(0)

            // Test the functions
            val chromCounts = gffSet.groupingBy{it.contig}.eachCount()
            println("\nchromCounts key set: ${chromCounts.keys}")
            println("chroms with their counts:")
            for ((chrom, count) in chromCounts) {
                println("${chrom}: ${count}")
            }

            val distinctIDList = getDistinctGffIds(gffSet)
            println("\nnumber of DistinctID from list: ${distinctIDList.size}")
           // distinctIDList.forEach {println(it)}

            val IDbyChrom = getDistinctGffIdsByChrom(gffSet)
            println("\nNumber of distinct ids per chrom")
            IDbyChrom.keys.forEach{
                val count = IDbyChrom.get(it)
                println("${it}: ${count?.size}")
            }

            // Get number of GFF bp's included for chrom - returned is a map of <chromosome,numberBpsRepresented>
            var bpsInGff = sumPerChromGFFBasePairs(gffSet)
            println("Number of BPs IN gff, per chrom:")
            for ((chrom, count) in bpsInGff) {
                println(" chrom ${chrom}: ${count}")
            }
            // returns: a map of chromosome to number of bps NOT represented in the GFF file
            var bpsNotInGff = sumPerChromNonGFFBasePairs(gffSet)
            println("NUmber of BPs NOT in gff, per chrom:")
            for ((chrom, count) in bpsNotInGff) {
                println(" chrom ${chrom}: ${count}")
            }
        }
    }

    @Test
    fun testCountingRanges() {

        val chromRangeSet: RangeSet<Position> = TreeRangeSet.create()
        val chrom1 = Chromosome.instance("chr1")
        var pos1 = Position.of(chrom1, 6)
        var pos2 = Position.of(chrom1, 22)

        chromRangeSet.add(Range.closed(pos1, pos2))
        pos1 = Position.of(chrom1, 18)
        pos2 = Position.of(chrom1, 28)
        chromRangeSet.add(Range.closed(pos1, pos2))

        val chrom2 = Chromosome.instance("chr2")
        pos1 = Position.of(chrom2, 20)
        pos2 = Position.of(chrom2, 28)
        chromRangeSet.add(Range.closed(pos1, pos2))
        chromRangeSet.add(Range.closed(Position.of(chrom2, 10), Position.of(chrom2, 14)))

        var testRange1 = Range.closed(Position.of(chrom1, 1), Position.of(chrom1, 100))
        var testRange2 = Range.closed(Position.of(chrom2, 1), Position.of(chrom2, 100))
        val subRange1 = chromRangeSet.subRangeSet(testRange1)
        val subRange2 = chromRangeSet.subRangeSet(testRange2)
        println("subRange1: ${subRange1.toString()}")
        println("\nsubrange2: ${subRange2}")

        val includedBP = chromRangeSet.subRangeSet(testRange2).asRanges()
                .map { range ->
                    range.upperEndpoint().position - range.lowerEndpoint().position +1
                }.sum()

        println("includedBP for testRange2: ${includedBP}")
        assertEquals(14, includedBP)
    }

    @Test
    fun testBPpercentages() {
        val keyFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/keyFileMichelle.txt"
        val configFile = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/config.txt"
        //val method = "mummer4_PATH"
        //val taxa = "B97_Assembly"
        val method = "michelle_b73_b97_path"
        val taxa = "NAM_Z001E0001"
        //val taxa = "NAM_Z001E0001,NAM_Z001E0002,NAM_Z001E0003,NAM_Z001E0004,NAM_Z001E0005"
        //val taxa = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/taxaList.txt"
        //val gffOutputDir = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing/michelle_gffOutput"

        val gffOutputDir = "/Users/lcj34/notes_files/phg_2018/new_features/phg493_GFF_plugin_fromAsmCoords/testing_oct2021/"
        val taxaList = createTaxaListFromFileOrString(taxa)

        var time = System.nanoTime()
        ParameterCache.load(configFile)
        val dataSet = PathToGFFPlugin(null, false)
            .keyFile(keyFile)
            .configFile(configFile)
            .method(method)
            .taxa(taxaList)
            .gffOutputDir(gffOutputDir) // could write to file if want extra debug
            .performFunction(null);
        var endingTime = (System.nanoTime() - time)/1e9
        println("\nfinished PathToGffPlugin in ${endingTime} seconds")

        if (dataSet.dataSet.size == 0) {
            // error here
            assertFails{"no data returned from PathToGFFPlugin"}
        }

        // This should return a Map<String, List<Set<Gff3Feature>>>
        // This is a List<> due to diploid handling
        val taxonGffMap = dataSet.getDataWithName("TaxonGFFmap").get(0).getData() as Map<String, List<Set<Gff3Feature>>>
        val taxon = taxonGffMap.keys.toTypedArray().get(0)
        val gffFeatures = taxonGffMap[taxon]!!.get(0)
        val chromCount = getGFFEntriesPerChrom(gffFeatures)
        println("testing mapData command")
        var chromosomes = chromCount.keys.toList()
        println("chromosomes keys as list:\n${chromosomes}")
        val counts = chromCount.values.toList()
        println("counts as list:\n${counts}")
        val mapData = mapOf("chromosome" to chromosomes,
            "count" to counts)


        val pctBPIncludedInGFF = percentPerChromGFFBasePairs(gffFeatures)
        chromosomes = pctBPIncludedInGFF.keys.toList()
        println("\nchromosomes keys as list:\n${chromosomes}")
        val percentIn = pctBPIncludedInGFF.values.toList()
        println("percent IN as list:\n${percentIn}")
        val pctBPNOTIncludedInGFF = percentPerChromNonGFFBasePairs(gffFeatures)
        chromosomes = pctBPNOTIncludedInGFF.keys.toList()
        println("\nchromosomes keys as list:\n${chromosomes}")
        val percentOut = pctBPNOTIncludedInGFF.values.toList()
        println("percent OUT as list:\n${percentOut}")

    }
}