@file:JvmName("CreateLiquibaseTestData")
package net.maizegenetics.pangenome.liquibase

import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.badDbNameDocker
import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.badDbNameNonDocker
import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.configNoSchemaDocker
import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.configNoSchemaNonDocker
import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.configWithSchemaDocker
import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.configWithSchemaNonDocker
import org.apache.log4j.Logger
import java.nio.file.Files
import java.nio.file.Paths

import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.outputDir
import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.dataDir
import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.goodDbNameDocker
import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.goodDbNameNonDocker
import net.maizegenetics.util.Utils
import org.apache.commons.io.FileUtils
import java.io.File
import java.sql.DriverManager

    private val myLogger = Logger.getLogger("net.maizegenetics.pangenome.liquibase.CraeteLiquibaseTestData")

    fun createData() {
        try {
            if (Files.exists(Paths.get(outputDir))) FileUtils.deleteDirectory( File(outputDir));
            Files.createDirectories(Paths.get(dataDir));

            // Directories created, now create the config and db files
            // need 4 files for each:  docker/non-docker and with/without necessary schema
            // THe schema does not have to match the current PHG schema. It merely needs to
            // contain or not contain the necessary schema to determine if the db is updatable.
            // For the first branch of liquibase, this means a "variants" table.  Tables do not
            // need to be populated.

            // non-docker config files
            var bw = Utils.getBufferedWriter(configNoSchemaNonDocker)
            bw.write("host=localHost\nuser=sqlite\npassword=sqlite\nDB=${badDbNameNonDocker}\nDBtype=sqlite\n")
            bw.close()

            bw = Utils.getBufferedWriter(configWithSchemaNonDocker)
            bw.write("host=localHost\nuser=sqlite\npassword=sqlite\nDB=${goodDbNameNonDocker}\nDBtype=sqlite\n")
            bw.close()

            // docker config files

            var splitNameString = goodDbNameDocker.split("/")
            var phgNameOnly = splitNameString[splitNameString.size - 1]

            bw = Utils.getBufferedWriter(configWithSchemaDocker)
            bw.write("host=localHost\nuser=sqlite\npassword=sqlite\nDB=/tempFileDir/outputDir/${phgNameOnly}\nDBtype=sqlite\n")
            bw.close()

            splitNameString = badDbNameDocker.split("/")
            phgNameOnly = splitNameString[splitNameString.size - 1]

            bw = Utils.getBufferedWriter(configNoSchemaDocker)
            bw.write("host=localHost\nuser=sqlite\npassword=sqlite\nDB=/tempFileDir/outputDir/${phgNameOnly}\nDBtype=sqlite\n")
            bw.close()

            // create the databases for testing
            createDBs(goodDbNameNonDocker,true)
            createDBs(badDbNameNonDocker, false)
            createDBs(goodDbNameDocker,true)
            createDBs(badDbNameDocker,false)

        } catch (exc: Exception) {
            myLogger.debug(exc.message, exc)
            throw IllegalStateException("CreateLiquibaseTestData::createData: Problem creating files : " + exc.message)
        }
    }

    fun createDBs(dbname: String, goodSchema: Boolean) {
        var url = "jdbc:sqlite:" + dbname
        try {
            // getting connection creates the db
            var conn = DriverManager.getConnection(url)

            // add minimum tables - does not need to reflect real PHG
            // table does not need to be populated
            var query = "CREATE TABLE genotypes (id integer PRIMARY KEY, name text);"
            conn.createStatement().execute(query)

            // add paths table - it should be dropped and replaced with new schema
            // (hapltoype_counts_id -> rm_group_id)
            query = "CREATE TABLE paths (path_id INTEGER, haplotype_counts_id INTEGER, method_id INTEGER, haplotype_paths BLOB);"
            conn.createStatement().execute(query)

            if (goodSchema) {
                // add tables needed to verify this schema is new enough to be updated.
                // what schema gets added should change with each major branch.
                // For this first branch, a schema is "new enough" if it contains a "variants" table
                query = "CREATE TABLE variants (id integer PRIMARY KEY, name text);"
                conn.createStatement().execute(query)
            }

            conn.close()
            // There is no need to populate these tables.  The CheckDBVersionPlugin currently only looks for the
            // presence of the table.

        }catch (exc: Exception) {
            myLogger.debug(exc.message, exc)
            throw IllegalStateException("CreateLiquibaseTestData::createDBs: Problem connecting to database : " + exc.message)
        }
    }
