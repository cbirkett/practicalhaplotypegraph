package net.maizegenetics.pangenome.liquibase

class LiquibasePaths {

    companion object {
        private val userHome = System.getProperty("user.home")
        val outputDir = "$userHome/temp/phgLiquibaseTest/"
        val dataDir = outputDir + "data/"

        val configWithSchemaNonDocker = dataDir + "configSQLite_newSchema.txt" // db contains schema needed to perform liquibase updates
        val configNoSchemaNonDocker = dataDir + "configSQLite_oldSchema.txt" // db does NOT contain schema needed by liquibase
        val configWithSchemaDocker = dataDir + "configSQLite_newSchemaDocker.txt" // db contains schema needed to perform liquibase updates
        val configNoSchemaDocker = dataDir + "configSQLite_oldSchemaDocker.txt" // db does NOT contain schema needed by liquibase

        val goodDbNameNonDocker = outputDir + "goodLiquibase.db";
        val badDbNameNonDocker = outputDir + "badLiquibase.db";
        val goodDbNameDocker = outputDir + "goodLiquibaseDocker.db";
        val badDbNameDocker = outputDir + "badLiquibaseDocker.db";


    }

}