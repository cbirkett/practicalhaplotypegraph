package net.maizegenetics.pangenome.liquibase

import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.dataDir
import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.outputDir
import net.maizegenetics.plugindef.Plugin.myLogger
import net.maizegenetics.util.LoggingUtils
import org.junit.Before
import org.junit.Test
import java.io.File
import java.util.stream.Collectors
import kotlin.test.assertTrue
import java.io.BufferedReader
import java.io.InputStreamReader

/**
 * This class contains tests for the PHG liquibase docker.
 * The tests perform checks of db versions, and checks of db migration success.
 *
 * This requires the user to have created a directory named <user_home>/temp/liquibase_docker.
 * This directory must contain a file name "Dockerfile" that is "FROM initial_liquibase".
 *
 * Before running the tests, a liquibase docker is created.  This is created similar
 * to PHG SmallSeq docker:  First an initial docker without a phg.jar is created.
 * The final "phg_liquibase" docker is then created "FROM initial_liquibase". This Dockerfil
 * must add a phg.jar file (created by user and stored to the <user_home>/temp/liquibase_base
 * folder) to the /tassel-5-standalone/lib dir inside the docker.
 *
 * NOTE: If you have made db changes, then you MUST recreate the initial liquibase docker.
 * It is in this docker that the lisuiqbase change logs are copied to the docker.  New change logs will
 * not be run if the initial liquibase docker is not created.  The second docker ("phg_liquibase")
 * merely copies the new phg.jar file to the new image.
 *
 * @author lcj34
 */

class LiquibaseDockerUpdateTest {
    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun testLiquibaseDocker_noVariantsRunNo() {
        //
        // This test gives a configFile that points to a db that doesn't contain
        // the specified schema (initially a "variants" table).  The first step of
        // the plugin will fail with an error that the db isn't recent enough to
        // allow for liquibase updates.  The outputDir will contain a file called
        // "run_no.txt"

        println("Starting testLiquibaseDocker_noVariantsRunNo ...")

        // create the docker locally
        createLiquibaseDocker();

        // The createData() method creates the config and db files needed for the
        // test.
        createData()

        // this test uses docker files.
        var splitNameString = LiquibasePaths.configNoSchemaDocker.split("/")
        var configNameOnly = splitNameString[splitNameString.size - 1]

        println("\nDocker images created, start test of noVariantsRunNo\n")

        val dockerImageName = "phg_liquibase:latest"

        try {
            val builder =  ProcessBuilder(
                    "docker", "run", "--name", "test_liquibase_update", "--rm",
                    "-v", outputDir + ":/tempFileDir/outputDir/",
                    "-v", dataDir + ":/tempFileDir/data/",
                    "-t", dockerImageName,
                    "./RunLiquibaseUpdates.sh",configNameOnly, "update")

            var redirectOutput= outputDir + "/liquibase_output.log"
            var redirectError = outputDir + "/liquibase_error.log"
            builder.redirectOutput( File(redirectOutput));
            builder.redirectError( File(redirectError));

            myLogger.info("\nPlease wait, begin Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            var process = builder.start();
            var error = process.waitFor();
            if (error != 0) {
                myLogger.error("liquibase run via ProcessBuilder returned error code $error")
            }
        } catch (exc: Exception) {
            myLogger.debug(exc.message, exc)
            throw IllegalArgumentException("testLiquibaseDocker: problem running ProcessBuilder")
        }

        // Verify the run_no.txt file exsits in the outputDir
        var runNoFile = outputDir + "run_no.txt"
        println("value of runNoFile is: $runNoFile")
        var runNo = File(runNoFile).exists()
        println("testLiquibaseDocker_noVariantsRunNo: value of runNo is $runNo , expecting TRUE")
        assertTrue(runNo)

        //Manually check the outputdir's liquibase_output.log file has an exception indicating
        // the db is not recent enough to perform updates

    }


    @Test
    fun testLiquibaseDocker_VariantsRunYes_sqlite() {
        //
        // This test gives a configFile that points to a db that  contains
        // the specified schema (initially a "variants" table).  The db should
        // get updated - no errors in the log file, and the outputDir should contain
        // a file called "run_yes.txt"
        // The config file should point to an sqlite db for testing

        println("Starting testLiquibaseDocker_VariantsRunYes_sqlite ...")

        // create local docker
        createLiquibaseDocker()

        // The createData() method creates the config and db files needed for the
        // test.
        createData()

        // this test uses docker files.
        var splitNameString = LiquibasePaths.configWithSchemaDocker.split("/")
        var configNameOnly = splitNameString[splitNameString.size - 1]

        val dockerImageName = "phg_liquibase:latest"

        try {
            // the sqlite db lives in the output dir, and is mounted when the output dir is mounted
            // if it did not live in the output directory, it would need to be mounted to it
            val builder =  ProcessBuilder(
                    "docker", "run", "--name", "test_liquibase_update", "--rm",
                    "-v", outputDir + ":/tempFileDir/outputDir/",
                    "-v", dataDir + ":/tempFileDir/data/",
                    "-t", dockerImageName,
                    "./RunLiquibaseUpdates.sh",configNameOnly, "update")

            var redirectOutput= outputDir + "/liquibase_output.log"
            var redirectError = outputDir + "/liquibase_error.log"
            builder.redirectOutput( File(redirectOutput));
            builder.redirectError( File(redirectError));

            myLogger.info("Please wait, begin Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            var process = builder.start();
            var error = process.waitFor();
            if (error != 0) {
                myLogger.error("liquibase run via ProcessBuilder returned error code $error")
            }
        } catch (exc: Exception) {
            myLogger.debug(exc.message, exc)
            throw IllegalArgumentException("testLiquibaseDocker: problem running ProcessBuilder")
        }

        // Verify the run_yes.txt file exsits in the outputDir
        var runYesFile = outputDir + "run_yes.txt"
        var runYes = File(runYesFile).exists()
        println("testLiquibaseDocker_VariantsRunYes_sqlite: value of runYes is $runYes , expecting TRUE")
        assertTrue(runYes)


        //Manually check the outputdir's liquibase_output.log file that no exception has occured.
        // Then check the db to verify the new schema exists.

    }


    @Test
    fun testLiquibaseDocker_VariantsRunYes_postgres() {
        // This file is for postgres testing.
        // go to dc01 - the postgres docker image should be there.
        // create a container with it, then load some data to it
        // See /workdir/lcj34/DockerPostgresTests_working/README_dc01.txt
        // NOTE: I added a dummy variants table to the hackdb in this postgres instance.
        // This is just to make this test pass the CheckDBVersion step.

        // This test uses a configFile that points to a db that DOES contain
        // the specified schema (initially a "variants" table).  The db should
        // get updated - no errors in the log file, and the outputDir should contain
        // a file called "run_yes.txt"

        println("Starting testLiquibaseDocker ...")

        // create the docker locally
        createLiquibaseDocker();

        var dataDir = "/Users/lcj34/notes_files/hackathons/june2019/"
        var configFile = "configDockerDC01.txt"

        val outputDir = "/Users/lcj34/notes_files/phg_2018/liquibase_epic/test/"
        val dockerImageName = "phg_liquibase"

        try {
            val builder =  ProcessBuilder(
                    "docker", "run", "--name", "test_liquibase_update", "--rm",
                    "-v", outputDir + ":/tempFileDir/outputDir/",
                    "-v", dataDir + ":/tempFileDir/data/",
                    "-t", dockerImageName,
                    "./RunLiquibaseUpdates.sh",configFile, "update")

            var redirectOutput= outputDir + "/liquibase_output.log"
            var redirectError = outputDir + "/liquibase_error.log"
            builder.redirectOutput( File(redirectOutput));
            builder.redirectError( File(redirectError));
            myLogger.info("Running Command:" + builder.command().stream().collect(Collectors.joining(" ")));

            myLogger.info("Please wait, begin Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            var process = builder.start();
            var error = process.waitFor();
            if (error != 0) {
                myLogger.error("liquibase run via ProcessBuilder returned error code $error")
            }
        } catch (exc: Exception) {
            myLogger.debug(exc.message, exc)
            throw IllegalArgumentException("testLiquibaseDocker: problem running ProcessBuilder")
        }

        // Verify the run_yes.txt file exsits in the outputDir
        var runYesFile = outputDir + "run_yes.txt"
        var runYes = File(runYesFile).exists()
        assertTrue(runYes)

        //Manually check the outputdir's liquibase_output.log file to verify no exceptions occurred.
        // Manually check the db to verify new schema is present.

    }

    private fun createLiquibaseDocker() {

        // Check if liquibase_initial and liquibase_phg images exist.
        var baseExists = false
        var testExists = false
        try {
            val cmd = "docker images"
            val start = Runtime.getRuntime().exec(cmd)
            val br = BufferedReader(
                    InputStreamReader(start.inputStream))

            br.use{ bufferedReader ->
                while(true) {
                    var line = bufferedReader.readLine()
                    if (line.isNullOrBlank()) break // quit when buffer is empty
                    if (line.contains("CREATED")) continue // skip header line
                    val imageName = line.substring(0, line.indexOf(" ")) // up to first white space
                    if (imageName == "initial_liquibase") {
                        baseExists = true
                    }
                    if (imageName == "phg_liquibase") {
                        testExists = true
                    }
                }
            }
        } catch (exc: Exception) {
            throw IllegalStateException("RunSmallSeqTests:main - error reading docker images: " + exc.message)
        }

        println("BaseExists: $baseExists testExists $testExists")

        // Base repository is created if it doesn't exist.  Test repository image
        // is deleted if it does exist.
        try {

            if (testExists) {
                // remove old docker liquibase_phg image
                val builder = ProcessBuilder(
                        "docker", "rmi", "phg_liquibase")

                builder.inheritIO()
                println("Running Command:" + builder.command().stream().collect(Collectors.joining(" ")));
                //println("Command:" + builder.command().stream().collect<String, *>(Collectors.joining(" ")))
                val process = builder.start()
                val error = process.waitFor()
                if (error != 0) {
                    // assume repository doesn't exist - this isn't real error
                    println("Error removing phg_liquibase, assuming it doesn't exist, Error: $error")
                }
            }

            if (!baseExists) {
                // no liquibase_initial - create it
                // Setup variables for liquibase_initial, create base image
                val workingDir = System.getProperty("user.dir")
                println("WorkingDir: $workingDir")
                val dockerDir = "$workingDir/docker/buildFiles/liquibase"

                println("Creating repository docker ")
                val builder = ProcessBuilder(
                        "docker", "build", "-t", "initial_liquibase", dockerDir)
                builder.inheritIO()
                println("Running Command:" + builder.command().stream().collect(Collectors.joining(" ")));

                val process = builder.start()
                process.waitFor()

            }

        } catch (exc: Exception) {
            throw IllegalStateException("Error deleting/creating docker images " + exc.message)
        }

        // Create test docker image based on liquibase_initial image
        val testDockerDir = System.getProperty("user.home") + "/temp/liquibase_docker"
        println("Creating second docker on top of initial_liquibase image")

        try {
            val builder = ProcessBuilder(
                    "docker", "build", "-t", "phg_liquibase", testDockerDir)
            builder.inheritIO()
            println("Running Command:" + builder.command().stream().collect(Collectors.joining(" ")))
            val process = builder.start()
            process.waitFor()
        } catch (e: Exception) {
            println("Creating second docker failed")
            throw IllegalStateException("Creating phg_liquibase docker image from phg repository failed ")
        }

    }


}