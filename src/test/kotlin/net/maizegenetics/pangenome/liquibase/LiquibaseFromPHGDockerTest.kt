package net.maizegenetics.pangenome.liquibase

import net.maizegenetics.plugindef.Plugin
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.Before
import org.junit.Test
import java.io.File
import java.util.stream.Collectors
import kotlin.system.exitProcess
import kotlin.test.assertEquals
import kotlin.test.assertTrue

/**
 * This test case runs liquibase status and update commands from a PHG docker.
 *
 * @author lcj34
 */
class LiquibaseFromPHGDockerTest {

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun testStatusAndUpdate() {
        // this test uses a PHG docker to verify if liquibase needs to be run
        // first, create the data
        createData()

        // Now create a PHG docker.  This uses the smallseq_docker directory as
        // is done in the smallSeq tests. Same rules for running apply - update phg.jar,
        // include and updated scripts.
        createPHGDocker()

        // get the name from the paths we use for liquibase tests
        var splitNameString = LiquibasePaths.configWithSchemaDocker.split("/")
        var configNameOnly = splitNameString[splitNameString.size - 1]

        val dockerImageName = "phgrepository_test:latest" // using PHG docker, not liquibase docker
        try {
            // Test 1:
            // the sqlite db lives in the output dir, and is mounted when the output dir is mounted
            println("\nRunning FIRST test - processBuilder with status")
            var builder =  ProcessBuilder(
                    "docker", "run", "--name", "test_liquibase_update", "--rm",
                    "-v", LiquibasePaths.outputDir + ":/tempFileDir/outputDir/",
                    "-v", LiquibasePaths.dataDir + ":/tempFileDir/data/",
                    "-t", dockerImageName,
                    "./RunLiquibaseUpdates.sh",  configNameOnly, "status")

            var redirectOutput= LiquibasePaths.outputDir + "/liquibase_statusScript.log"
            var redirectError = LiquibasePaths.outputDir + "/liquibase_statusScript.log"
            builder.redirectOutput( File(redirectOutput));
            builder.redirectError( File(redirectError));

            Plugin.myLogger.info("Please wait, running Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            var process = builder.start();
            var error = process.waitFor();
            if (error != 0) {
                Plugin.myLogger.error("liquibase run via ProcessBuilder returned error code $error")
                exitProcess(error)
            }

            // Verify the run_yes.txt file exsits in the outputDir.  This was created via
            // a call to CheckDBVersionPlugin run from the RunLiquibaseUpdates.sh script above
            var runYesFile = LiquibasePaths.outputDir + "run_yes.txt"
            var runYes = File(runYesFile).exists()
            println("testLiquibaseDocker_VariantsRunYes_sqlite: value of runYes is $runYes , expecting TRUE")
            assertTrue(runYes)

            // The file "liquibase_<command>_output.log" was written to outputDir.  It should have 3 lines, the
            // last line should be "Liquibase command 'status' was executed successfully."
            // The middle line should either include "is up to date" or " change sets have not been applied to "

            var liquibaseOutputFile = LiquibasePaths.outputDir + "liquibase_status_output.log"
            var linesInOutput = Utils.getBufferedReader(liquibaseOutputFile).readLines()

            var success = linesInOutput
                    .filter {it.contains("executed successfully")}
                    .toList()
            if (success == null) {
                println("failure!! - liquibase status command was not successful")
                exitProcess(-1)
            }
            val needsUpdating = linesInOutput
                    .filter { it.contains("have not been applied") }
                    .toList()

            println("\nStatus Result:")
            if (needsUpdating != null && needsUpdating.size > 0) println(needsUpdating)
            else println("nothing to update")

            assertEquals(1, needsUpdating.size)

            // Test 2:
            // Now update the db
            println("\nRUnning second test - the update command - call ProcessBuilder")
            builder =  ProcessBuilder(
                    "docker", "run", "--name", "test_liquibase_update", "--rm",
                    "-v", LiquibasePaths.outputDir + ":/tempFileDir/outputDir/",
                    "-v", LiquibasePaths.dataDir + ":/tempFileDir/data/",
                    "-t", dockerImageName,
                    "./RunLiquibaseUpdates.sh",  configNameOnly, "update")

            redirectOutput= LiquibasePaths.outputDir + "/liquibase_updateScript.log"
            redirectError = LiquibasePaths.outputDir + "/liquibase_updateScript.log"
            builder.redirectOutput( File(redirectOutput));
            builder.redirectError( File(redirectError));

            Plugin.myLogger.info("Please wait, running Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            process = builder.start();
            error = process.waitFor();
            if (error != 0) {
                Plugin.myLogger.error("liquibase run via ProcessBuilder returned error code $error")
                exitProcess(error)
            }

            // Check results
            liquibaseOutputFile = LiquibasePaths.outputDir + "liquibase_update_output.log"
            linesInOutput = Utils.getBufferedReader(liquibaseOutputFile).readLines()

            success = linesInOutput
                    .filter {it.contains("executed successfully")}
                    .toList()
            if (success == null) {
                println("failure!! - liquibase udpate command was not successful")
                exitProcess(-1)
            }
            var updateResults = linesInOutput
                    .filter { it.contains("Update has been successful") }
                    .toList()

            println("\nUpdate Result:")
            if (updateResults != null && updateResults.size > 0) println(updateResults)
            else println("nothing was updated")

            assertEquals(1,updateResults.size)

            // Test 3:  run status again.  This time, it should tell us there is nothing
            // that needs to be updated.

            println("\nRunning THIRD test - processBuilder with status again")
            builder =  ProcessBuilder(
                    "docker", "run", "--name", "test_liquibase_update", "--rm",
                    "-v", LiquibasePaths.outputDir + ":/tempFileDir/outputDir/",
                    "-v", LiquibasePaths.dataDir + ":/tempFileDir/data/",
                    "-t", dockerImageName,
                    "./RunLiquibaseUpdates.sh",  configNameOnly, "status")

            redirectOutput= LiquibasePaths.outputDir + "/liquibase_status2Script.log"
            redirectError = LiquibasePaths.outputDir + "/liquibase_status2Script.log"
            builder.redirectOutput( File(redirectOutput));
            builder.redirectError( File(redirectError));

            Plugin.myLogger.info("Please wait, running Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            process = builder.start();
            error = process.waitFor();
            if (error != 0) {
                Plugin.myLogger.error("liquibase run via ProcessBuilder returned error code $error")
                exitProcess(error)
            }

            // Check results
            liquibaseOutputFile = LiquibasePaths.outputDir + "liquibase_status_output.log"
            linesInOutput = Utils.getBufferedReader(liquibaseOutputFile).readLines()

            success = linesInOutput
                    .filter {it.contains("executed successfully")}
                    .toList()
            if (success == null) {
                println("failure!! - liquibase udpate command was not successful")
                exitProcess(-1)
            }
            val needsUpdating2 = linesInOutput
                    .filter { it.contains("have not been applied") }
                    .toList()

            println("\nStatus 2 Result:")
            if (needsUpdating2 != null && needsUpdating2.size > 0) println(needsUpdating2)
            else println("nothing to update")

            assertEquals(0, needsUpdating2.size)

            // Test 5:
            // Now update the db - see what it shows when nothing needs to be run
            println("\nRUnning second test - the update command - call ProcessBuilder")
            builder =  ProcessBuilder(
                    "docker", "run", "--name", "test_liquibase_update", "--rm",
                    "-v", LiquibasePaths.outputDir + ":/tempFileDir/outputDir/",
                    "-v", LiquibasePaths.dataDir + ":/tempFileDir/data/",
                    "-t", dockerImageName,
                    "./RunLiquibaseUpdates.sh",  configNameOnly, "update")

            redirectOutput= LiquibasePaths.outputDir + "/liquibase_updateScript2.log"
            redirectError = LiquibasePaths.outputDir + "/liquibase_updateScript2.log"
            builder.redirectOutput( File(redirectOutput));
            builder.redirectError( File(redirectError));

            Plugin.myLogger.info("Please wait, running Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            process = builder.start();
            error = process.waitFor();
            if (error != 0) {
                Plugin.myLogger.error("liquibase run via ProcessBuilder returned error code $error")
                exitProcess(error)
            }

            // Check results
            liquibaseOutputFile = LiquibasePaths.outputDir + "liquibase_update_output.log"
            linesInOutput = Utils.getBufferedReader(liquibaseOutputFile).readLines()

            success = linesInOutput
                    .filter {it.contains("executed successfully")}
                    .toList()
            if (success == null) {
                println("failure!! - liquibase udpate command was not successful")
                exitProcess(-1)
            }
            updateResults = linesInOutput
                    .filter { it.contains("Update has been successful") }
                    .toList()

            println("\nUpdate Result:")
            if (updateResults != null && updateResults.size > 0) println(updateResults)
            else println("nothing was updated")

            assertEquals(1,updateResults.size)

            println("\nTest complete !!")

        } catch (exc: Exception) {
            Plugin.myLogger.debug(exc.message, exc)
            throw IllegalArgumentException("testLiquibaseDocker: problem running ProcessBuilder")
        }
    }
}