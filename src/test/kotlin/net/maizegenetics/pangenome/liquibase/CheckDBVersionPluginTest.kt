package net.maizegenetics.pangenome.liquibase

import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.configNoSchemaNonDocker
import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.configWithSchemaNonDocker
import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.outputDir
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import org.junit.Before
import org.junit.Test
import java.io.File
import java.nio.file.Files
import kotlin.test.assertTrue


class CheckDBVersionPluginTest {

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun testCheckDBVersionPlugin_commonData() {
        createData()
        // this test uses non-docker files.
        var configFile = configWithSchemaNonDocker
        ParameterCache.load(configFile)
        var outputDir = outputDir

        val yesFile = File(outputDir + "run_yes.txt")
        var result = Files.deleteIfExists(yesFile.toPath())

        val noFile = File(outputDir + "run_no.txt")
        result = Files.deleteIfExists(noFile.toPath())

        println("\nrunning test testCheckDBVersionPlugin, YES VARIANT TABLE with config file $configFile")
        CheckDBVersionPlugin(null, false)
                .outputDir(outputDir)
                .performFunction(null)

        assertTrue(yesFile.exists())
        assertTrue(!noFile.exists())

        // TEST 2:  Test older db version
        result = Files.deleteIfExists(yesFile.toPath())
        result = Files.deleteIfExists(noFile.toPath())

        configFile = configNoSchemaNonDocker
        ParameterCache.load(configFile)
        println("running test testCheckDBVersionPlugin, NO VARIANT TABLE with config file $configFile")
        CheckDBVersionPlugin(null, false)
                .outputDir(outputDir)
                .performFunction(null)

        assertTrue(noFile.exists())

        println("DONE - output written to $outputDir")
    }

    @Test
    fun testCheckDBVersionPlugin() {
        //CheckDBVersionPlugin - will write a file called "run_yes.txt" if the db is of a correct
        // version to run liquibase updates.  It will write a "run_no.txt" file if the db is NOT
        // appropriate to run liquibase on this db.  For the initial tests, it is checking for the
        // presence of a table named "variants".

        //  test SQLITE databases
        var configFile = "/Users/lcj34/notes_files/phg_2018/liquibase_epic/test/configSQLite.txt"
        ParameterCache.load(configFile)

        val outputDir = "/Users/lcj34/notes_files/phg_2018/liquibase_epic/test"

        // Delete files if they exist prior to running this test
        val yesFile = File("/Users/lcj34/notes_files/phg_2018/liquibase_epic/test/run_yes.txt")
        var result = Files.deleteIfExists(yesFile.toPath())

        val noFile = File("/Users/lcj34/notes_files/phg_2018/liquibase_epic/test/run_no.txt")
        result = Files.deleteIfExists(noFile.toPath())

        println("\nrunning test testCheckDBVersionPlugin, YES VARIANT TABLE with config file $configFile")
        CheckDBVersionPlugin(null, false)
                .outputDir(outputDir)
                .performFunction(null)

        assertTrue(yesFile.exists())
        assertTrue(!noFile.exists())

        // TEST 2:  Test older db version
        result = Files.deleteIfExists(yesFile.toPath())
        result = Files.deleteIfExists(noFile.toPath())

        configFile = "/Users/lcj34/notes_files/phg_2018/liquibase_epic/test/configSQLite_noVariants.txt"
        ParameterCache.load(configFile)
        println("running test testCheckDBVersionPlugin, NO VARIANT TABLE with config file $configFile")
        CheckDBVersionPlugin(null, false)
                .outputDir(outputDir)
                .performFunction(null)

        assertTrue(noFile.exists())

        println("DONE - output written to $outputDir")
    }
}