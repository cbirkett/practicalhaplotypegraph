package net.maizegenetics.pangenome.hapcollapse

import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.DataSet
import org.apache.log4j.BasicConfigurator
import org.apache.log4j.Logger
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

class ProfileMxDivTest {

    lateinit var graphDS: DataSet

    @Test
    fun setupTests() {
        println("Setting up the configurator")
        BasicConfigurator.configure()
    }

    @Test
    fun testMxDivProfilerWithKmerMethod() {
        graphDS = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(SmallSeqPaths.dbConfigFile)
            .methods("GATK_PIPELINE")
            .includeSequences(true)
            .includeVariantContexts(false)
            .performFunction(null)

        val out = SmallSeqPaths.outputDir + "/mxdivProfileKmer.txt"
        val startTime = System.nanoTime()

        ProfileMxdivPlugin()
            .outputFile(out)
            .distanceType(ProfileMxdivPlugin.DistanceType.KMER)
            .numThreads(5)
            .performFunction(graphDS)


        println("Kmer profile ran in ${(System.nanoTime() - startTime)/1e6} ms")
    }

    @Test
    fun testMxDivProfilerWithSnpMethod() {
        graphDS = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(SmallSeqPaths.dbConfigFile)
            .methods("GATK_PIPELINE")
            .includeSequences(false)
            .includeVariantContexts(true)
            .performFunction(null)

        val out = SmallSeqPaths.outputDir + "/mxdivProfileSnp.txt"
        val startTime = System.nanoTime()
        ProfileMxdivPlugin()
            .outputFile(out)
            .distanceType(ProfileMxdivPlugin.DistanceType.SNP)
            .numThreads(5)
            .performFunction(graphDS)

        println("Snp profile ran in ${(System.nanoTime() - startTime)/1e6} ms")
    }
}