package net.maizegenetics.pangenome.pipeline

import net.maizegenetics.util.LoggingUtils
import org.junit.Before
import org.junit.Test

class MakeDefaultDirectoryPluginTest {

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun testSetupDir() {
        val rootDir = "/Users/zrm22/Desktop/DirCreateTest/"
        val phgJar = "phg.jar"
        MakeDefaultDirectoryPlugin().workingDir(rootDir).jarFiles(phgJar).performFunction(null)
    }
}