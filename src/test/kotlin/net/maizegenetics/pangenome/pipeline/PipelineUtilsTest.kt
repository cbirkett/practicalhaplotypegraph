package net.maizegenetics.pangenome.pipeline

import junit.framework.Assert.assertEquals
import junit.framework.Assert.fail
import net.maizegenetics.pangenome.liquibase.LiquibasePaths
import net.maizegenetics.pangenome.liquibase.LiquibasePaths.Companion.configWithSchemaDocker
import net.maizegenetics.pangenome.liquibase.createData
import net.maizegenetics.pangenome.liquibase.createPHGDocker
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import org.junit.Before
import org.junit.Test
import java.io.File
import java.util.stream.Collectors

/**
 * Class to test the pipeline/PipelineUtils.kt:runLiquibaseCommand() function
 *
 * This class creates a PHG db that does not have the change sets stored for liquibase.
 * It then creates a new PHG docker, and runs 3 iterations of  LiquibaseCommandPlugin.
 *  - status, update, status
 *
 * LiquibaseCommandPlugin was created specifically to test the pipeline/PipelineUtils:runLiquibaseCommand()
 * method.  We have all this wrapper code as runLiquibaseCommand() needs to be called by a plugin that has
 * configuration values stored in a parameter cache.
 *
 * In addtion, in order to perform liquibase commands, currently a docker is involved for storing the
 * change logs and jdbc drivers needed by liquibase.
 *
 */

class PipelineUtilsTest {
    private val myLogger = Logger.getLogger(PipelineUtilsTest::class.java)
    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun testRunLiquibaseCommand() {

        // This needs to be run from within a Docker as liquibase expects the change log sets,
        // as well as the jdbc files for sqlite and postgres.
        // Specifically, this code tests the runLiquibaseCommand() method in PipelineUtils.kt

        println("\nCalling createData()")
        createData() // creates several sqlite dbs, one that initially needs updating

        println("\n calling createPHGDocker")
        createPHGDocker()

        // get the name from the paths we use for liquibase tests
        // These tests will be run outside of a docker, "withSchema" means it has schema needed to allow an update
        var splitNameString = LiquibasePaths.configWithSchemaDocker.split("/")
        var configNameOnly = splitNameString[splitNameString.size - 1]

        //ParameterCache.load(configWithSchemaDocker) // loads configFIle name as well as parameters listed in the config file

        // I need the docker or it won't have liquibase stuff

        // This calls a plugin that is really a wrapper for calling PipelineUtils:runLiquibaseCommand(command,outputDir)
        // The test plugin will print the outcome.
        myLogger.info("Calling PipeineUtils.runLiqubaseCommand")
        println("Calling PipelineUtils.runLiquibaseCommand")
        val dockerImageName = "phgrepository_test:latest" // using PHG docker, not liquibase docker
        try {

            var builder =  ProcessBuilder(
                    "docker", "run", "--name", "test_liquibase_update", "--rm",
                    "-v", LiquibasePaths.outputDir + ":/tempFileDir/outputDir/",
                    "-v", LiquibasePaths.dataDir + ":/tempFileDir/data/",
                    "-t", dockerImageName,
                    "/tassel-5-standalone/run_pipeline.pl", "-Xmx10G", "-debug","-configParameters", "/tempFileDir/data/$configNameOnly", "-LiquibaseCommandPlugin","-command","status","-outputDir","/tempFileDir/outputDir")

            // Writing output and error to same file
            var redirectOutput= LiquibasePaths.outputDir + "/liquibase_statusScript.log"
            var redirectError = LiquibasePaths.outputDir + "/liquibase_statusScript.log"
            builder.redirectOutput( File(redirectOutput));
            builder.redirectError( File(redirectError));
            println("Command:" + builder.command().stream().collect(Collectors.joining(" ")))
            var process = builder.start()
            process.waitFor()

            // There are 2 output files:  1 created by liqibase, and one that is the script
            // output.  In LiquibaseFromPHGDockerTest all the liquibase output was verified.
            // Here I want to verify what is returned from the method that calls the
            // liquibase commands.  That is stored in the liquibase_statusScript.log  -
            // but it is also stored in a file created by the test wrapper LiquibaseCommandPlugin.kt,
            // named "runLiquibaseCommand_returnValue.txt."
            //
            // For this test, I expect to see the result: NEEDS_UPDATING:0.0.0
            var outputFile = LiquibasePaths.outputDir + "/runLiquibaseCommand_returnValue.txt"
            var linesInOutput = Utils.getBufferedReader(outputFile).readLines()

            var success = linesInOutput
                    .filter {it.contains("NEEDS_UPDATING")}
                    .toList()
            if (success == null) {
                println("failure!! - liquibase status command was not successful")
                fail("liquibase status from PipelineUtils:runLiquibaseCommand failed")
            } else {
                println("first status is success, return is : $success")
                assertEquals("NEEDS_UPDATING:0.0.10",success.get(0))
            }

            // Now run update - verify it is successful.  This is on the same db
            println("begin update of db ...")
            builder =  ProcessBuilder(
                    "docker", "run", "--name", "test_liquibase_update", "--rm",
                    "-v", LiquibasePaths.outputDir + ":/tempFileDir/outputDir/",
                    "-v", LiquibasePaths.dataDir + ":/tempFileDir/data/",
                    "-t", dockerImageName,
                    "/tassel-5-standalone/run_pipeline.pl", "-Xmx10G", "-debug","-configParameters", "/tempFileDir/data/$configNameOnly", "-LiquibaseCommandPlugin","-command","update","-outputDir","/tempFileDir/outputDir")


            redirectOutput= LiquibasePaths.outputDir + "/liquibase_updateScript.log"
            redirectError = LiquibasePaths.outputDir + "/liquibase_updateScript.log"
            builder.redirectOutput( File(redirectOutput));
            builder.redirectError( File(redirectError));
            println("Command:" + builder.command().stream().collect(Collectors.joining(" ")))
            process = builder.start()
            process.waitFor()

           //  I expect to see the result:  "UP_TO_DATE"
            linesInOutput = Utils.getBufferedReader(outputFile).readLines()

            success = linesInOutput
                    .filter {it.contains("UP_TO_DATE")}
                    .toList()
            if (success == null) {
                fail("Failure! - liqubase update command was no successful")
            } else {
                println("UPDATE command is success, return is : $success")
            }

            // One more run - now that db is updated, verify  PipelinUtils:runLiquidbaseCommand returns UP_TO_DATE
            // for the status command
            println("begin update of db ...")
            builder =  ProcessBuilder(
                    "docker", "run", "--name", "test_liquibase_update", "--rm",
                    "-v", LiquibasePaths.outputDir + ":/tempFileDir/outputDir/",
                    "-v", LiquibasePaths.dataDir + ":/tempFileDir/data/",
                    "-t", dockerImageName,
                    "/tassel-5-standalone/run_pipeline.pl", "-Xmx10G", "-debug","-configParameters", "/tempFileDir/data/$configNameOnly", "-LiquibaseCommandPlugin","-command","status","-outputDir","/tempFileDir/outputDir")


            redirectOutput= LiquibasePaths.outputDir + "/liquibase_statusScript.log"
            redirectError = LiquibasePaths.outputDir + "/liquibase_statusScript.log"
            builder.redirectOutput( File(redirectOutput));
            builder.redirectError( File(redirectError));
            println("Command:" + builder.command().stream().collect(Collectors.joining(" ")))
            process = builder.start()
            process.waitFor()

            success = linesInOutput
                    .filter {it.contains("UP_TO_DATE")}
                    .toList()
            if (success == null) {
                fail("Failure! - 2nd liquibase status command was no successful")
            } else {
                println("2nd liquibase status command is success, return is : $success")
            }

        } catch ( exc: Exception) {

            throw IllegalStateException("ERROR - PIpelineUtilsTest failed")
        }

    }

    @Test
    fun testParseChangesetVersion() {
        var testDir = "/Users/lcj34/notes_files/phg_2018/peter_pipeline/test/"
        var liquibaseOutputFile = testDir + "/liquibase_status_output.log"
        var linesInOutput = Utils.getBufferedReader(liquibaseOutputFile).readLines()

        val changeSets = linesInOutput
                .filter { it.contains("changesets")}
                .map { it -> parseChangesetVersion(it)}
                .toList()

        println("Changsets recovered:")
        changeSets.stream().forEach{println(it)}

        // Now find the lowest number - these are already sorted, but let's be sure
        val phgVersion = findUserPHGVersion(changeSets)
        println("phgVersion: $phgVersion")

    }

    @Test
    fun testFindUserPHGVersion() {
        var list = mutableListOf<Triple<Int,Int,Int>>()
        list.add(Triple<Int,Int,Int>(0,2,2))
        list.add(Triple<Int,Int,Int>(0,2,18))
        list.add(Triple<Int,Int,Int>(0,0,2))
        list.add(Triple<Int,Int,Int>(0,0,13))
        list.add(Triple<Int,Int,Int>(1,2,1))

        val phgVersion = findUserPHGVersion(list)
        val expected = "0.0.2"
        assertEquals(expected, phgVersion)
        println("phgVersion: $phgVersion")
    }
}