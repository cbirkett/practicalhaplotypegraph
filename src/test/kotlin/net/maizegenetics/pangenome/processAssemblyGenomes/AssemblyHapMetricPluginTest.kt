package net.maizegenetics.pangenome.processAssemblyGenomes

import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.db_loading.HaplotypeGeneMetricsPlugin
import net.maizegenetics.util.LoggingUtils
import org.junit.Test

class AssemblyHapMetricPluginTest {

    @Test
    fun testAssemblyHapMetricPlugin() {
        LoggingUtils.setupDebugLogging()

        println("Running AssemblyHapMetricPlugin")
        //var configFile = "/Users/lcj34/notes_files/hackathons/feb2020/configSQLite.txt"
        var configFile = "/Users/lcj34/notes_files/phg_2018/debug/arcadio_AssemblyHapMetricsPlugin_results/config_dc01Postgres.txt"
        //var outputFile = "/Users/lcj34/notes_files/hackathons/feb2020/cintaMap.txt"
        //var outputFile = "/Users/lcj34/notes_files/hackathons/feb2020/cintaMapWithNewCode2.txt"
        var outputFile = "/Users/lcj34/notes_files/phg_2018/debug/arcadio_AssemblyHapMetricsPlugin_results/lynn_testArcadio_withRefLen.txt"

        var refRangeGroups = "FocusRegion,FocusComplement"
        //var methods = "v5_gffGenes,mummer4" // just ref and assemblies
        var methods = "v5_gffGenes,mummer4" // just ref and assemblies
        AssemblyHapMetricPlugin(null, false)
                .configFile(configFile)
                .methods(methods)
                .refRangeGroups(refRangeGroups)
                .outputFile(outputFile)
                .performFunction(null)
        println("Finished")
    }

    @Test
    fun testMutableList() {
        var gameteIdToLen= ArrayList<Int>(28);

        val arr = IntArray(28) { i -> 0 }

        for (idx in 0..27) {
            println(arr[idx])
        }

        val myArray = (1..10).toMutableList()
        println(myArray)

    }
}