package net.maizegenetics.pangenome.processAssemblyGenomes

import net.maizegenetics.pangenome.processAssemblyGenomes.AlterNucmerFirstLinePlugin
import org.junit.Test

class AlterNucmerFirstLinePluginTest {

    @Test
    fun testAlterNucmerFirstLinePlugin() {
        val inputDir = "/Users/lcj34/notes_files/phg_2018/b73v5_NAMarcadio2020_gff/test_deltas/"
        val outputDir = "/Users/lcj34/notes_files/phg_2018/b73v5_NAMarcadio2020_gff/changedDeltas/"
        val refPath = "/phg2/newRefPath/"
        val asmPath = "/phg2/newAsmPath/"

        AlterNucmerFirstLinePlugin(null,false)
                .asmPath(asmPath)
                .refPath(refPath)
                .inputDir(inputDir)
                .outputDir(outputDir)
                .performFunction(null)

        println("testAlterNucmerFirstLinePlugin finished")
    }
}