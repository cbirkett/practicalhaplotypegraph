package net.maizegenetics.pangenome.processAssemblyGenomes

import net.maizegenetics.util.LoggingUtils
import org.junit.Before
import org.junit.Test

class SplitASMContigsByLengthTest {

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()
    }

    @Test
    fun testSplit() {
        val inputFasta = "/Users/zrm22/Desktop/TempDocs/UpliftNam/JoeAsm/Buckler-CML16_mecat.contigs.fasta"
        val outputFasta = "/Users/zrm22/Desktop/TempDocs/UpliftNam/JoeAsm/Buckler-CML16_mecat_split20kbp_plugin.fasta"


        SplitASMContigsByLengthPlugin(null,false)
                .inputASMFile(inputFasta)
                .outputSplitFile(outputFasta)
                .splitSize(20000)
                .performFunction(null)

    }
}