package net.maizegenetics.pangenome.processAssemblyGenomes

import net.maizegenetics.util.LoggingUtils
import org.junit.Test

class AssemblyConsensusMetricPluginTest {
    @Test
    fun testAssemblyHapMetricPlugin() {
        LoggingUtils.setupDebugLogging()

        println("Running HaplotypeGeneMetricsPlugin")
        var configFile = "/Users/lcj34/notes_files/hackathons/feb2020/configSQLite.txt"
        var outputFile = "/Users/lcj34/notes_files/hackathons/feb2020/ConsensusHapData_0001b.txt"


        // CONSENSUS_mxdiv_0.001,CONSENSUS_mxdiv_0.005, CONSENSUS_mxdiv_0.01, CONSENSUS_mxdiv_0.0005, CONSENSUS_mxdiv_0.0001
        AssemblyConsensusMetricPlugin(null, false)
                .configFile(configFile)
                .methods("CONSENSUS_mxdiv_0.0001")
                .outputFile(outputFile)
                .performFunction(null)
        println("Finished")
    }
}