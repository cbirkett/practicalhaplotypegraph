package net.maizegenetics.pangenome.processAssemblyGenomes

import htsjdk.variant.variantcontext.VariantContext
import htsjdk.variant.vcf.VCFFileReader
import net.maizegenetics.dna.map.Position
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.File

class MAFToGVCFPluginTest {


    val sampleName = "B97"
    val workDir = SmallSeqPaths.baseDir
    val testingDir = "${workDir}/MAFToGVCFTests/"
    val refFile = "${testingDir}/B73Test.fa"
    val mafFile = "${testingDir}/B97.maf"
    val truthGVCFFile = "${testingDir}/B97_truth.gvcf"
    val outputFile = "${testingDir}/B97.gvcf"

    @Before
    fun setup() {
        LoggingUtils.setupDebugLogging()

        File(testingDir).deleteRecursively()

        //Make the dir first
        File(testingDir).mkdirs()

        //Create the ref File:
        createSimpleRef(refFile)
        //Create the MAF file:
        createMAFFile(mafFile)
        //Create the known GVCF file:
        createTruthGVCFFile(truthGVCFFile)
    }

    fun testB97GVCF() {
        val sampleName = "B97"
//        val mafFile = "/Volumes/ZackBackup2020/PHG/AnchorwaveTests/FullFile/B97_singleALignment.maf"
        val mafFile = "/Volumes/ZackBackup2020/PHG/AnchorwaveTests/FullFile/B97.maf"
        val refFile = "/Volumes/ZackBackup2020/PHG/AnchorwaveTests/FullFile/Zm-B73-REFERENCE-NAM-5.0.fa"
        val outputFile = "/Volumes/ZackBackup2020/PHG/AnchorwaveTests/FullFile/B97GVCFOutput.gvcf"


        MAFToGVCFPlugin().reference(refFile).mAFFile(mafFile).sampleName(sampleName).gVCFOutput(outputFile).performFunction(null)
    }

    @Test
    fun testSimpleMAFs() {
        MAFToGVCFPlugin().reference(refFile).mAFFile(mafFile).sampleName(sampleName).gVCFOutput(outputFile).fillGaps(false).performFunction(null)


        //Load in the output GVCF  and the truth GVCF and verify that the output is correct
        val truthVariantIterator = VCFFileReader(File(truthGVCFFile),false).iterator()
        val truthVariants = mutableListOf<VariantContext>()
        while(truthVariantIterator.hasNext()) {
            truthVariants.add(truthVariantIterator.next())
        }
        val truthMap = truthVariants.associateBy { Position.of(it.contig, it.start) }


        val outputVariantIterator = VCFFileReader(File(outputFile), false).iterator()
        val outputVariants = mutableListOf<VariantContext>()
        while(outputVariantIterator.hasNext()) {
            outputVariants.add(outputVariantIterator.next())
        }

        Assert.assertEquals("Number of Variants does not match:",truthVariants.size, outputVariants.size)

        for(variant in outputVariants) {
            if(!truthMap.containsKey(Position.of(variant.contig, variant.start))) {
                Assert.fail("No matching variant found: ${variant.contig}:${variant.start}")
            }
            val matchingTruth = truthMap[Position.of(variant.contig, variant.start)]!!

            //Check END
            Assert.assertEquals("End position does not match: outputFile: ${variant.contig}:${variant.start}-${variant.end}, " +
                                        "Truth: ${matchingTruth.contig}:${matchingTruth.start}-${matchingTruth.end}",
                matchingTruth.end, variant.end)

            //Check alleles
            Assert.assertArrayEquals("Alleles do not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.alleles.toTypedArray(), variant.alleles.toTypedArray())
            //Check GT
            Assert.assertEquals("GT Fields do not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getGenotype(0).genotypeString,variant.getGenotype(0).genotypeString)
            //Check AD
            Assert.assertArrayEquals("AD fields do not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getGenotype(0).ad, variant.getGenotype(0).ad)
            //Check ASM Contig
            Assert.assertEquals("ASM_Contig does not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getAttribute("ASM_Chr"), variant.getAttribute("ASM_Chr"))
            //Check ASM Start
            Assert.assertEquals("ASM_Start does not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getAttribute("ASM_Start"), variant.getAttribute("ASM_Start"))
            //Check ASM END
            Assert.assertEquals("ASM_End does not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getAttribute("ASM_End"), variant.getAttribute("ASM_End"))
            //Check ASM Strand
            Assert.assertEquals("ASM_Strand does not match: ${variant.contig}:${variant.start}-${variant.end}", matchingTruth.getAttribute("ASM_Strand"), variant.getAttribute("ASM_Strand"))

        }
    }

    /**
     * Function to create a reference file which will be used with the MAF file to create the GVCF
     */
    private fun createSimpleRef(outputFile : String) {
        Utils.getBufferedWriter(outputFile).use { output ->
            output.write(">chr7\n")
            output.write("${(0 until 12).map { "A" }.joinToString("")}")
            output.write("AAAGGGAATGTTAACCAAATGAATTGTCTCTTACGGTG")
            output.write("${(0 until 400).map { "A" }.joinToString("")}")
            output.write("TAAAGATGGGT\n")


            output.write(">chr1\n")
            output.write("GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")

        }
    }

    /**
     * Simple function to create a simple MAF file used for testing.  This covers most of the edge cases we have run into.
     */
    private fun createMAFFile(outputFile: String) {
        Utils.getBufferedWriter(outputFile).use {output ->
            output.write("##maf version=1 scoring=Tba.v8\n\n")

            output.write("a\tscore=23262.0\n")
            output.write("s\tB73.chr7\t12\t38\t+\t158545518\tAAA-GGGAATGTTAACCAAATGA---ATTGTCTCTTACGGTG\n")
            output.write("s\tB97.chr4\t81344243\t40\t+\t187371129\t-AA-GGGGATGCTAAGCCAATGAGTTGTTGTCTCTCAATGTG\n\n")

            output.write("a\tscore=5062.0\n")
            output.write("s\tB73.chr7\t450\t6\t+\t158545518\tTAAAGAT---GGGT\n")
            output.write("s\tB97.chr4\t81444246\t6\t+\t 187371129\tTAAGGATCCC---T\n\n")

            output.write("a\tscore=6636.0\n")
            output.write("s\tB73.chr1\t0\t40\t+\t 158545518\t-----GCAGCTGAAAACAGTCAATCTTACACACTTGGGGCCTACT\n")
            output.write("s\tB97.chr6\t53310097\t40\t + 151104725\tAAAAAGACAGCTGAAAATATCAATCTTACACACTTGGGGCCTACT\n")

        }
    }

    /**
     * Function to create the truth GVCF file
     */
    private fun createTruthGVCFFile(outputFile: String) {
        Utils.getBufferedWriter(outputFile).use { output ->
            output.write("##fileformat=VCFv4.2\n" +
                    "##FORMAT=<ID=AD,Number=3,Type=Integer,Description=\"Allelic depths for the ref and alt alleles in the order listed\">\n" +
                    "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Read Depth (only filtered reads used for calling)\">\n" +
                    "##FORMAT=<ID=GQ,Number=1,Type=Integer,Description=\"Genotype Quality\">\n" +
                    "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n" +
                    "##FORMAT=<ID=PL,Number=3,Type=Integer,Description=\"Normalized, Phred-scaled likelihoods for genotypes as defined in the VCF specification\">\n" +
                    "##INFO=<ID=AF,Number=3,Type=Integer,Description=\"Allele Frequency\">\n" +
                    "##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Total Depth\">\n" +
                    "##INFO=<ID=END,Number=1,Type=Integer,Description=\"Stop position of the interval\">\n" +
                    "##INFO=<ID=NS,Number=1,Type=Integer,Description=\"Number of Samples With Data\">\n" +
                    "##contig=<ID=7,length=461>\n" +
                    "##contig=<ID=1,length=100>\n" +
                    "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tB97\n" +
                    "1\t1\t.\tG\tAAAAAG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310103;ASM_Start=53310098;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "1\t2\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310104;ASM_Start=53310104;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "1\t3\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310105;ASM_Start=53310105;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "1\t4\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310106;ASM_Start=53310106;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "1\t5\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310107;ASM_Start=53310107;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "1\t6\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310108;ASM_Start=53310108;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "1\t7\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310109;ASM_Start=53310109;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "1\t8\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310110;ASM_Start=53310110;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "1\t9\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310113;ASM_Start=53310111;ASM_Strand=+;END=11\tGT:AD:DP\t0:1,0:1\n" +
                    "1\t12\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310114;ASM_Start=53310114;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "1\t13\t.\tA\tT,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310115;ASM_Start=53310115;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "1\t14\t.\tG\tA,<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310116;ASM_Start=53310116;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "1\t15\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=6;ASM_End=53310142;ASM_Start=53310117;ASM_Strand=+;END=40\tGT:AD:DP\t0:1,0:1\n" +

                    "7\t12\t.\tAA\tA,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344243;ASM_Start=81344243;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t14\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344248;ASM_Start=81344244;ASM_Strand=+;END=18\tGT:AD:DP\t0:1,0:1\n" +
                    "7\t19\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344249;ASM_Start=81344249;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t20\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344252;ASM_Start=81344250;ASM_Strand=+;END=22\tGT:AD:DP\t0:1,0:1\n" +
                    "7\t23\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344253;ASM_Start=81344253;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t24\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344256;ASM_Start=81344254;ASM_Strand=+;END=26\tGT:AD:DP\t0:1,0:1\n" +
                    "7\t27\t.\tC\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344257;ASM_Start=81344257;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t28\t.\tC\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344258;ASM_Start=81344258;ASM_Strand=+;END=28\tGT:AD:DP\t0:1,0:1\n" +
                    "7\t29\t.\tA\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344259;ASM_Start=81344259;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t30\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344263;ASM_Start=81344260;ASM_Strand=+;END=33\tGT:AD:DP\t0:1,0:1\n" +
                    "7\t34\t.\tA\tAGTT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344267;ASM_Start=81344264;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t35\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344268;ASM_Start=81344268;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t36\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344276;ASM_Start=81344269;ASM_Strand=+;END=43\tGT:AD:DP\t0:1,0:1\n" +
                    "7\t44\t.\tT\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344277;ASM_Start=81344277;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t45\t.\tA\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344278;ASM_Start=81344278;ASM_Strand=+;END=45\tGT:AD:DP\t0:1,0:1\n" +
                    "7\t46\t.\tC\tA,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344279;ASM_Start=81344279;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t47\t.\tG\tT,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344280;ASM_Start=81344280;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t48\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81344283;ASM_Start=81344281;ASM_Strand=+;END=50\tGT:AD:DP\t0:1,0:1\n" +
                    "7\t451\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444249;ASM_Start=81444247;ASM_Strand=+;END=453\tGT:AD:DP\t0:1,0:1\n" +
                    "7\t454\t.\tA\tG,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444250;ASM_Start=81444250;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t455\t.\tG\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444252;ASM_Start=81444251;ASM_Strand=+;END=456\tGT:AD:DP\t0:1,0:1\n" +
                    "7\t457\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444253;ASM_Start=81444253;ASM_Strand=+;END=457\tGT:AD:DP\t0:1,0:1\n" +
                    "7\t458\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444254;ASM_Start=81444254;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t459\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444255;ASM_Start=81444255;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t460\t.\tG\tC,<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444256;ASM_Start=81444256;ASM_Strand=+\tGT:AD:DP\t1:0,1,0:1\n" +
                    "7\t461\t.\tT\t<NON_REF>\t.\t.\tASM_Chr=4;ASM_End=81444257;ASM_Start=81444257;ASM_Strand=+;END=461\tGT:AD:DP\t0:1,0:1\n")
        }
    }
}