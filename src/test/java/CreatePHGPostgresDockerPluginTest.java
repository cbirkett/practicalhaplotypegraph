import org.junit.Test;

import net.maizegenetics.pangenome.db_loading.CreatePHGPostgresDockerPlugin;

/**
 * Test creating phg_docker image
 * 
 * @author lcj34
 *
 */
public class CreatePHGPostgresDockerPluginTest {
    
    @Test
    public  void testCreatePHGPostgresDocker() {
        
        String dockerDir = "/Users/lcj34/notes_files/repgen/wgs_pipeline/dockers/postgres_GOBII_based/";
        String dockerCmd = "/usr/local/bin/docker";
        
        new CreatePHGPostgresDockerPlugin()
        .dockerCmd(dockerCmd)
        .dockerDir(dockerDir)
        .performFunction(null) ;
        
        System.out.println("Finished createPHGpostgresDocker");
    } 

}
