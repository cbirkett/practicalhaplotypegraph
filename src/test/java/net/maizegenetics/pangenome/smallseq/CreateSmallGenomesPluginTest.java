/**
 *
 */
package net.maizegenetics.pangenome.smallseq;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import static net.maizegenetics.pangenome.smallseq.SmallSeqPaths.*;
import static org.junit.Assert.*;

import net.maizegenetics.util.DirectoryCrawler;

/**
 * This test class verifies the CreateSmallGenomesPlugin will create samll sequence
 * files when invoked.  It can also be used to create various test sets with
 * non-default parameters.
 *
 * @author lcj34
 */
public class CreateSmallGenomesPluginTest {
    @Test
    public void testCreateSmallGenomesPlugin() throws Exception {
        int lengthOfGenes = 2048;
        int lengthOfInterGenes = 2048;
        double proportionOfRefInterGeneDuplicated = 0.0;
        double proportionOfInterGeneDeleted = 0.0;
        int numberOfGenes = 10;
        int lengthOfReads = 128;
        int haplotypeDivergence = 32;  //every Xbp are mutated relative to ancestor
        int intraHaplotypeDivergence = 256;  //every Xbp are mutated relative to ancestor

        System.out.println("Call CreateTestGenomesPlugin with no parameters, all defaults ...");
        new CreateSmallGenomesPlugin()
                .performFunction(null);

        String inputFileGlob = "glob:*{.fa,.fq,fq.gz,fastq,fastq.txt,fastq.gz,fastq.txt.gz,.vcf,.txt,.bed}";
        List<Path> directoryFiles = DirectoryCrawler.listPaths(
                inputFileGlob, Paths.get(dataDir).toAbsolutePath());

        System.out.println("Directory path files: expecting 17");
        for (Path filePath : directoryFiles) {
            System.out.println(filePath.toString());
        }
        assertEquals(17, directoryFiles.size());

        List<String> fileNames = new ArrayList<String>();

        for (Path directoryFile : directoryFiles) {
            fileNames.add(directoryFile.getFileName().toString());
        }

        assertTrue(fileNames.contains("LineA1_R1.fastq"));
        assertTrue(fileNames.contains("LineB1_R1.gbs.fastq"));
        assertTrue(fileNames.contains("configSQLiteDocker.txt"));

        // Check some files in the answer directory

        directoryFiles = DirectoryCrawler.listPaths(
                inputFileGlob, Paths.get(answerDir).toAbsolutePath());
        System.out.println("Directory path files: expecting 12"); // this changes if CreateTestGenomes changes
        for (Path filePath : directoryFiles) {
            System.out.println(filePath.toString());
        }
        assertEquals(12, directoryFiles.size());

        fileNames.clear(); // get new list
        for (Path directoryFile : directoryFiles) {
            fileNames.add(directoryFile.getFileName().toString());
        }

        // Verify a few files that should exist
        assertTrue(fileNames.contains("LineA1.fa"));
        assertTrue(fileNames.contains("SmallSeq.vcf"));
        assertTrue(fileNames.contains("anchors.bed"));

        // Run again - this time change a few parameters
        System.out.println("\nCall CreateTestGenomesPlugin with several parameters...");
        new CreateSmallGenomesPlugin()
                .geneLength(lengthOfGenes)
                .interGeneLength(lengthOfInterGenes)
                .numberOfGenes(numberOfGenes)
                .wgsDepth(10.0)
                .gbsDepth(0.5)
                .performFunction(null);

        // Verify dataDir again
        directoryFiles = DirectoryCrawler.listPaths(
                inputFileGlob, Paths.get(dataDir).toAbsolutePath());

        System.out.println("Directory path files: expecting 31");
        for (Path filePath : directoryFiles) {
            System.out.println(filePath.toString());
        }
        assertEquals(31, directoryFiles.size());

        fileNames.clear(); // start fresh
        for (Path directoryFile : directoryFiles) {
            fileNames.add(directoryFile.getFileName().toString());
        }

        // Check some files in the data directory
        assertTrue(fileNames.contains("LineA1_R1.fastq"));
        assertTrue(fileNames.contains("LineB1_R1.gbs.fastq"));
        assertTrue(fileNames.contains("configSQLiteDocker.txt"));

        // Check some files in the answer directory
        directoryFiles = DirectoryCrawler.listPaths(
                inputFileGlob, Paths.get(answerDir).toAbsolutePath());
        System.out.println("Directory path files: expecting 19"); // true unless CreateTestGenome changes

        for (Path filePath : directoryFiles) {
            System.out.println(filePath.toString());
        }
        assertEquals(19, directoryFiles.size());

        fileNames.clear(); // get new list
        for (Path directoryFile : directoryFiles) {
            fileNames.add(directoryFile.getFileName().toString());
        }

        // Verify a few files that should exist
        assertTrue(fileNames.contains("LineA1.fa"));
        assertTrue(fileNames.contains("SmallSeq.vcf"));
        assertTrue(fileNames.contains("anchors.bed"));

        System.out.println("FInished!!");

    }
    

}
