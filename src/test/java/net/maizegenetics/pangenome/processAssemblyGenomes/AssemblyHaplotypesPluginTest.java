/**
 * 
 */
package net.maizegenetics.pangenome.processAssemblyGenomes;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.Multimap;

import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.pangenome.api.CreateGraphUtils;
import net.maizegenetics.pangenome.api.ReferenceRange;
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import net.maizegenetics.util.Tuple;
import net.maizegenetics.util.Utils;

/**
 * Tests to verify AssemblyhapltoypesPlugin processing. Related tests may be found in
 * MummerScriptProcessingTest.java
 * 
 * @author lcj34
 *
 */
public class AssemblyHaplotypesPluginTest {
    @Test
    public void testFilterCoordsOverlaps() {
        String outputDeltaFilePrefix = "/Users/lcj34/notes_files/phg_2018/training/debug/maggie_files/align/ref_EP1_Assembly_9_c250mumdeltaGPlus1000";
        String outputCoordsNoEmbedded = outputDeltaFilePrefix + ".coords_filteredPlus_noEmbedded";
        String outputCoordsFinal = outputDeltaFilePrefix + ".coords_final";
        String snpsForOverlaps = outputDeltaFilePrefix + ".snps_prefiltered"; // created in runShowSNPsWithCat
        
        MummerScriptProcessing.filterCoordsOverlaps(outputCoordsNoEmbedded, snpsForOverlaps, outputCoordsFinal);
        
        
    }
    
    @Test
    public void testCheckForOverlap() {
        Tuple<Integer,Integer> prevStartEnd = new Tuple<Integer,Integer>(46135748,46139565);
        Tuple<Integer,Integer> curStartEnd = new Tuple<Integer,Integer>(46139153,46139565);
        
        Tuple<Integer,Boolean> result = MummerScriptProcessing.checkForOverlap(prevStartEnd,curStartEnd);
        System.out.println("num overlaps " + result.x + " embedded " + result.y);
        assertEquals((int)result.x, 413);
        assertEquals(result.y,true);
        
        prevStartEnd = new Tuple<Integer,Integer>(46135748,46139565);
        curStartEnd = new Tuple<Integer,Integer>(46139153,46139560);
        
        result = MummerScriptProcessing.checkForOverlap(prevStartEnd,curStartEnd);
        System.out.println("num overlaps " + result.x + ", embedded: " + result.y);
        assertEquals((int)result.x, 408);
        assertEquals(result.y, true);
        
        prevStartEnd = new Tuple<Integer,Integer>(46135748,46139565);
        curStartEnd = new Tuple<Integer,Integer>(46135740,46139560);
        
        result = MummerScriptProcessing.checkForOverlap(prevStartEnd,curStartEnd);
        System.out.println("num overlaps " + result.x + " embedded " + result.y);
        assertEquals((int)result.x, 3813);
        assertEquals(result.y, false);
        
        // test overlap when is descending and first one starts after 2ncd, but first ends before second (
        // when lloking at descending order)
        
        System.out.println("\nTesting weird 75585382-75580853 vs 75587301-75581001");
        System.out.println("Real overlap is 75587301-75585382 plus 75581001-75580853, is 1919 + 148 = 2067");
        prevStartEnd = new Tuple<Integer,Integer>(75585382,75580853);
        curStartEnd = new Tuple<Integer,Integer>(75587301,75581001);
        
        result = MummerScriptProcessing.checkForOverlap(prevStartEnd,curStartEnd);
        System.out.println("num overlaps " + result.x + ", embedded: " + result.y);
        
        // test when first one is embedded in the second.
        System.out.println("\nCheck for first embedded in second");
        prevStartEnd = new Tuple<Integer,Integer>(75585382,75582853);
        curStartEnd = new Tuple<Integer,Integer>(75587301,75581001);
        
        result = MummerScriptProcessing.checkForOverlap(prevStartEnd,curStartEnd);
        System.out.println("num overlaps " + result.x + ", embedded: " + result.y);
        
        if (result.y == false) {
            // check other direction
            Tuple<Integer, Integer> tempStartEnd = prevStartEnd;
            prevStartEnd = curStartEnd;
            curStartEnd = tempStartEnd;
            
            result = MummerScriptProcessing.checkForOverlap(prevStartEnd,curStartEnd);
            System.out.println("FOr reverse: num overlaps " + result.x + ", embedded: " + result.y);
        }
               
    }
  
    // This test used to run debug over AssemblyHapltoypesPlugin
    // When creating/processing VariantContext issues, use this, and comment out
    // whatever code need to in AssemblyHapltoypesPlugin
    @Test
    public void testAssemblyHaplotypesPluginDebug() {
        
//        String assembly = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/performance/w22chr1.fa";
//        String ref = "/Users/lcj34/notes_files/phg_2018/b73/b73chr1.fa";
//        String outputDir = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/performance/";
//        String configFile = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/performance/configSQLite.txt";
//        String assemblyName = "W22_ASSEMBLY";
//        String chrom = "1";
//        String assembly = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/cml247chr5.fa";
//        String ref = "/Users/lcj34/notes_files/phg_2018/b73/b73chr5.fa";
//        String outputDir = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/";
//        String configFile = "/Users/lcj34/notes_files/phg_2018/dbSchema_updates/phg205_variants/configSQLite.txt";
//        String assemblyName = "CML247_Assembly";
//        String chrom = "5";
        
        String assembly = "/Users/lcj34/notes_files/phg_2018/b73/PHJ40chr10.fa";
        String ref = "/Users/lcj34/notes_files/phg_2018/b73/b73chr10.fa";
        String outputDir = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/peter_duplicate_snps/";
        String configFile = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/peter_duplicate_snps/configFile.txt";
        String assemblyName = "PHJ40_Assembly";
        String chrom = "10";
        
        new AssemblyHaplotypesPlugin()
        .assembly(assembly)
        .ref(ref)
        .outputDir(outputDir)
        .dbConfigFile(configFile)
        .assemblyName(assemblyName)
        .chrom(chrom)
        .mummer4Path("/Users/lcj34/")
        .entryPoint(AssemblyHaplotypesPlugin.ASSEMBLY_ENTRY_POINT.haplotypes)
        .performFunction(null);
    }
    
    @Test
    public void testFinalSnpFiltering() {

        String deltaPrefix = "ref_PHJ40_Assembly_10_c250mumdeltaGPlus1000";
        String outputDir = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/peter_duplicate_snps/";
        String snpsPreFiltered = outputDir + deltaPrefix + ".snps_prefiltered";

        String chrom = "10";
        
        System.out.println("testFinalSnpFIltering: calling finalSnpFiltering ... ");
        String outputDeltaPrefix = outputDir + deltaPrefix;
        MummerScriptProcessing.finalSnpFiltering(snpsPreFiltered, outputDeltaPrefix, chrom);
        
        // To verify - check the created file at outputDir ++ deltaPrefix + ".snps_final
        // compare to snpsPreFiltered for sections known to be wrong before (e.g. deletion
        // starting at ref position 1095777)
        System.out.println("testFinalSnpFiltering: done!!");
    }
    
    @Test
    public void testRefRangeForChrom() {
        String configFile = "/Users/lcj34/notes_files/phg_2018/dbSchema_updates/phg205_variants/configSQLite.txt";
        String chrom = "1";
        
        // the db referenced in the config file above has an added user group which includes 5 reference ranges from chrom 1.
        // These 5 reference ranges show up twice in the ref_range_ref_range_methods table, once with method_id 2 (matching
        // name "refRegionGroup" and once with method_id 4 (matching name "userGrp1")
        // This test is to verify those 5 ranges are not added/counted twice .
        
        Connection dbConn = DBLoadingUtils.connection(configFile, false);
        
        String query = "SELECT count (*) as chr1ranges from reference_ranges where chrom='1'";
        int actualChrom1Ranges = 0;
        try (ResultSet rs = dbConn.createStatement().executeQuery(query)) {          
            if (rs.next()) {
                actualChrom1Ranges  = rs.getInt("chr1ranges");
            }
            System.out.println("Number of chromosome 1 ranges " + actualChrom1Ranges);

        } catch (Exception exc) {            
            throw new IllegalStateException("AssemblyHaplotypesPlugin: referenceRanges: Problem querying the database: " + exc.getMessage());
        }
        
        Map<Integer, ReferenceRange> chrom1Map = AssemblyProcessingUtils.referenceRangeForChromMap(dbConn,  chrom);
                
        System.out.println("chrom1Map size from referenceRangeForChromMap " + chrom1Map.keySet().size() + ", expected: " + actualChrom1Ranges);       
        assertEquals(actualChrom1Ranges,chrom1Map.keySet().size());
    }
    
    @Test
    public  void testRefilterCoordsFileContainsPerformance( ) {
       
        // Changing from "contains" to the map method cuts the time for the tested
        // data here from 1.220075 seconds to 0.045642 seconds
        // Verified both versions contain the same entries.  Slight difference
        // in order when the start position is duplicated - the map version orders based
        // on end, then.  THis is fine. The overlaps code will take care of it.
        String coordsOrig = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/ref_PHJ40_Assembly_8_c250mumdeltaGPlus1000.coords_orig";
        String coordsFiltered = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/ref_PHJ40_Assembly_8_c250mumdeltaGPlus1000.coords_filtered";
        String coordsReturned = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/PHJ40_chr8.coords_returnedLines";
        List<String> deltaLines = new ArrayList<String>();
        List<String> gLines = new ArrayList<String>();
        List<String> sortedRefCoordsList = new ArrayList<String>();
        List<String> returnedLines = new ArrayList<String> ();
        
        String orderedList1 = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/origlistOrdered.txt";
        String orderedList2 = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/newlistOrdered.txt";
        try (BufferedReader deltaBR = Utils.getBufferedReader(coordsOrig);
             BufferedReader deltaGbr = Utils.getBufferedReader(coordsFiltered);
             BufferedReader returnedBR = Utils.getBufferedReader(coordsReturned);
             BufferedWriter bwOrig = Utils.getBufferedWriter(orderedList1);
             BufferedWriter bwNew = Utils.getBufferedWriter(orderedList2)){
                         
            String line = null;
            while ((line = deltaBR.readLine()) != null) {
                deltaLines.add(line);
            }            

            while ((line = deltaGbr.readLine()) != null) {                
                gLines.add(line); // add to list that gets merged
            }                      
          
            while ((line = returnedBR.readLine()) != null) {                
                returnedLines.add(line); 
            }      
            
            // Test method 1
            long time = System.nanoTime();
            for (String entry : deltaLines) {
                if (gLines.contains(entry) || returnedLines.contains(entry)) {
                    sortedRefCoordsList.add(entry);
                }
            }
            double firstTime = (System.nanoTime() - time)/1e9;
            System.out.println("Time to process with contains: " + firstTime + " seconds");
            
            // write list so can be compared to second test output
            for (String entry: sortedRefCoordsList) {
                bwOrig.write(entry);
                bwOrig.write("\n");
            }
            sortedRefCoordsList.clear(); // reset the list to nothing
            
            // Test method 2
            time = System.nanoTime();
            Map<Tuple<Integer,Integer>,String> refStartToEntryMap = new HashMap<>();
            for (String entry : gLines) {
                int firstTab = entry.indexOf("\t");
                int refStart = Integer.parseInt(entry.substring(0, firstTab));
                String entryMinusRefStart = entry.substring(firstTab+1);
                int refEnd = Integer.parseInt(entryMinusRefStart.substring(0,entryMinusRefStart.indexOf("\t")));
                refStartToEntryMap.put(new Tuple<Integer,Integer>(refStart,refEnd), entry);
            }
            for (String entry : returnedLines) {
                int firstTab = entry.indexOf("\t");
                int refStart = Integer.parseInt(entry.substring(0, firstTab));
                String entryMinusRefStart = entry.substring(firstTab+1);
                int refEnd = Integer.parseInt(entryMinusRefStart.substring(0,entryMinusRefStart.indexOf("\t")));
                refStartToEntryMap.put(new Tuple<Integer,Integer>(refStart,refEnd), entry);
            }
            List<Tuple<Integer,Integer>> refStartList = new ArrayList<Tuple<Integer,Integer>>(refStartToEntryMap.keySet());
            Collections.sort(refStartList);
            
            refStartList.stream().forEach(item -> {
                sortedRefCoordsList.add(refStartToEntryMap.get(item));
            });

            double secondTime = (System.nanoTime() - time)/1e9;
            System.out.println("Time to process with new method: " + secondTime + " seconds");
            
            assertTrue(secondTime < firstTime);
            
            // write list for manual comparison to first
            for (String entry: sortedRefCoordsList) {
                bwNew.write(entry);
                bwNew.write("\n");
            }
            System.out.println("FInished !!");
            
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}
