package net.maizegenetics.pangenome.processAssemblyGenomes;

import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.RangeSet;
import com.google.common.collect.TreeRangeSet;
import htsjdk.variant.variantcontext.VariantContext;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.pangenome.api.ReferenceRange;
import net.maizegenetics.pangenome.fastaExtraction.GVCFSequence;
import net.maizegenetics.util.Tuple;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class AssemblyProcessingUtilsTest {
    String bzTestSNPFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/OutputsToMakeVCFs/b73ph207_bzRegions_c250.snps";
    String bzTestCoordFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/OutputsToMakeVCFs/b73ph207_bzRegions_c250_TABDELIMITED.coords";

    @Test
    public void testSNPParsing() {
        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(bzTestSNPFile,"9");

        String printout = snpMap.asMapOfRanges().keySet()
                .stream()
                .map(positionRange -> ""+positionRange.lowerEndpoint().getPosition()+":"+positionRange.upperEndpoint().getPosition()+"\t"+snpMap.get(positionRange.lowerEndpoint()).getX()+"\t"+snpMap.get(positionRange.upperEndpoint()).getY())
                .collect(Collectors.joining("\n"));

        System.out.println(printout);
    }

    @Test
    public void testSNPParsingWithOverlap() {
        String wholeChr9 = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/1Tests/b73ph207_chr9_c250mum.snps1";
        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(wholeChr9,"9");

        String printout = snpMap.asMapOfRanges().keySet()
                .stream()
                .filter(positionRange -> positionRange.contains(Position.of(Chromosome.instance("9"),12151)))
                .map(positionRange -> ""+positionRange.lowerEndpoint().getPosition()+":"+positionRange.upperEndpoint().getPosition()+"\t"+snpMap.get(positionRange.lowerEndpoint()).getX()+"\t"+snpMap.get(positionRange.upperEndpoint()).getY())
                .collect(Collectors.joining("\n"));

        System.out.println(printout);
    }


    @Test
    public void testCoordParsing() {
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(bzTestCoordFile,"9");
        Map<Range<Position>,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(bzTestCoordFile,"9");

        RangeSet<Position> refMappingPositions = refMappings
//                .asMapOfRanges()
                .keySet()
                .stream()
                .collect(Collector.of(TreeRangeSet::create,
                        (set, range) -> set.add(range),
                        (set1,set2) -> {
                            set1.addAll(set2);
                            return set1;
                        }));

        String printout = refMappingPositions.asRanges().stream().map(positionRange -> ""+positionRange.lowerEndpoint().getPosition() + ":"+ positionRange.upperEndpoint().getPosition())
                .collect(Collectors.joining("\n"));

        System.out.println(printout);
    }

    @Test
    public void testIndelExtraction() {
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(bzTestCoordFile,"bzRegion");
// Map<Range<Position>,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(bzTestCoordFile,"bzRegion");

        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Assemblies/b73_bzRegionChrRename.fa";
        GenomeSequence refSeq = GenomeSequenceBuilder.instance(b73File);

        String ph207File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Assemblies/ph207_bzRegionChrRename.fa";
        GenomeSequence asmSeq = GenomeSequenceBuilder.instance(ph207File);


//        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings,refSeq,asmSeq);
//
//        String printout = snpMap.asMapOfRanges().keySet()
//                .stream()
//                .map(positionRange -> ""+positionRange.lowerEndpoint().getPosition()+":"+positionRange.upperEndpoint().getPosition()+"\t"+snpMap.get(positionRange.lowerEndpoint()).getX().length()+"\t"+snpMap.get(positionRange.lowerEndpoint()).getX()+"\n\t"+snpMap.get(positionRange.upperEndpoint()).getY().length()+"\t"+snpMap.get(positionRange.upperEndpoint()).getY())
//                .collect(Collectors.joining("\n"));
//
//        System.out.println(printout);

//        RangeSet<Position> indels = AssemblyProcessingUtils.getIndelRanges(refMappings);
//
//
//        System.out.println(indels.asRanges().stream().map(positionRange -> positionRange.lowerEndpoint().getPosition()+":"+positionRange.upperEndpoint().getPosition()).collect(Collectors.joining("\n")));
    }



//    11145326-11149631 = 0-2379
//
//            11153210-11157403 = 5958-10151
//
//            11173560-11175048 = 26308-27706
//
//            11181054-11182577 = 33802-35325
//
//            11183936-11184547 = 36684-37295
//
//            11188015-11192433 = 40763-45181
//
//            11218927-11220493 = 71676-73241

    @Test
    public void testVCCreation() {
        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(bzTestSNPFile,"b73_bzRegion");
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(bzTestCoordFile,"b73_bzRegion");
        Map<Range<Position>,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(bzTestCoordFile,"b73_bzRegion");
        RangeSet<Position> anchors = TreeRangeSet.create();

        anchors.add(Range.closed(Position.of("b73_bzRegion",26308),Position.of("b73_bzRegion",27706)));
        anchors.add(Range.closed(Position.of("b73_bzRegion",33802),Position.of("b73_bzRegion",35325)));
        anchors.add(Range.closed(Position.of("b73_bzRegion",36684),Position.of("b73_bzRegion",37295)));

//        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Assemblies/Zea_mays.AGPv4.dna.toplevel.fa";
        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Assemblies/b73_bzRegion.fa";
        GenomeSequence refSeq = GenomeSequenceBuilder.instance(b73File);

//        RangeSet<Position> refMappingPositions = refMappings
//                .asMapOfRanges()
//                .keySet()
//                .stream()
//                .collect(Collector.of(TreeRangeSet::create,
//                        (set, range) -> set.add((Range<Position>)range),
//                        (set1,set2) -> {
//                            set1.addAll(set2);
//                            return set1;
//                        }));


        List<VariantContext> vcs = AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSeq, "PH207", anchors,refMappings,snpMap);

        System.out.println(vcs.stream().map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

    }

    @Test
    public void testVCCreationWithNonMappedRegions() {
        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(bzTestSNPFile,"bzRegion");
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(bzTestCoordFile,"bzRegion");
        Map<Range<Position>,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(bzTestCoordFile,"bzRegion");
        RangeSet<Position> anchors = TreeRangeSet.create();

        anchors.add(Range.closed(Position.of("bzRegion",26308),Position.of("bzRegion",27706)));
        anchors.add(Range.closed(Position.of("bzRegion",33802),Position.of("bzRegion",35325)));
        anchors.add(Range.closed(Position.of("bzRegion",36684),Position.of("bzRegion",37295)));

        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Assemblies/b73_bzRegionChrRename.fa";
        GenomeSequence refSeq = GenomeSequenceBuilder.instance(b73File);

        String ph207File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Assemblies/ph207_bzRegionChrRename.fa";
        GenomeSequence asmSeq = GenomeSequenceBuilder.instance(ph207File);

        RangeMap<Position,Tuple<String,String>> nonMappedSnpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings,refSeq,asmSeq);

        snpMap.putAll(nonMappedSnpMap);

//        RangeSet<Position> refMappingPositions = refMappings
//                .asMapOfRanges()
//                .keySet()
//                .stream()
//                .collect(Collector.of(TreeRangeSet::create,
//                        (set, range) -> set.add((Range<Position>)range),
//                        (set1,set2) -> {
//                            set1.addAll(set2);
//                            return set1;
//                        }));


        List<VariantContext> vcs = AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSeq, "PH207", anchors,refMappings,snpMap);

        System.out.println(vcs.stream().map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

    }


    /**
     * This unit test should just return one vc record which is a GVCF block matching reference.
     */
    @Test
    public void testVCCreationNonVariant() {
        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(bzTestSNPFile,"bzRegion");
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(bzTestCoordFile,"bzRegion");
        Map<Range<Position>,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(bzTestCoordFile,"bzRegion");
        RangeSet<Position> anchors = TreeRangeSet.create();

        anchors.add(Range.closed(Position.of("bzRegion",26310),Position.of("bzRegion",26442)));

        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Assemblies/b73_bzRegionChrRename.fa";
        GenomeSequence refSeq = GenomeSequenceBuilder.instance(b73File);

        String ph207File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Assemblies/ph207_bzRegionChrRename.fa";
        GenomeSequence asmSeq = GenomeSequenceBuilder.instance(ph207File);

        RangeMap<Position,Tuple<String,String>> nonMappedSnpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings,refSeq,asmSeq);

        snpMap.putAll(nonMappedSnpMap);

//        RangeSet<Position> refMappingPositions = refMappings
//                .asMapOfRanges()
//                .keySet()
//                .stream()
//                .collect(Collector.of(TreeRangeSet::create,
//                        (set, range) -> set.add((Range<Position>)range),
//                        (set1,set2) -> {
//                            set1.addAll(set2);
//                            return set1;
//                        }));


        List<VariantContext> vcs = AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSeq, "PH207", anchors,refMappings,snpMap);

        System.out.println(vcs.stream().map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

    }

//
//    @Test
//    public void testVCCreationForFilteringDeltas() {
//        String snpFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/ref_PH207_c250mum.snps_deltaG";
//        String coordFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/ref_PH207_c250mum.coords_deltaG";
//
//        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(snpFile,"9");
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");
//        RangeSet<Position> anchors = TreeRangeSet.create();
//
////        58300	67504
//        anchors.add(Range.closed(Position.of("9",58300),Position.of("9",67504)));
//
//        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/b73chr9.fa";
//        GenomeSequence refSeq = GenomeSequenceBuilder.instance(b73File);
//
//        String ph207File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/ph207chr9.fasta";
//        GenomeSequence asmSeq = GenomeSequenceBuilder.instance(ph207File);
//
//        RangeMap<Position,Tuple<String,String>> nonMappedSnpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings,refSeq,asmSeq);
//
//        snpMap.putAll(nonMappedSnpMap);
//
//        RangeSet<Position> refMappingPositions = refMappings
//                .asMapOfRanges()
//                .keySet()
//                .stream()
//                .collect(Collector.of(TreeRangeSet::create,
//                        (set, range) -> set.add((Range<Position>)range),
//                        (set1,set2) -> {
//                            set1.addAll(set2);
//                            return set1;
//                        }));
//
//
//        List<VariantContext> vcs = AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSeq, "PH207", anchors,refMappings,snpMap);
//
//        System.out.println(vcs.stream().map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));
//    }
//
//    @Test
//    public void debugApr4Error() {
//        String snpFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/ErrorApr4/ref_PH207_c250mum.snps";
//        String coordFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/ErrorApr4/ref_PH207_c250mum.coords";
//
//
//        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(snpFile,"9");
//        System.out.println("Done with snpmap");
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");
//
//        System.out.println("Done processing refmappings");
//        RangeSet<Position> anchors = TreeRangeSet.create();
//
//        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/b73chr9.fa";
//        GenomeSequence refSeq = GenomeSequenceBuilder.instance(b73File);
//
//        String ph207File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/ph207chr9.fasta";
//        GenomeSequence asmSeq = GenomeSequenceBuilder.instance(ph207File);
//
//
//        //63280
////        anchors.add(Range.closed(Position.of("9",58300),Position.of("9",67504)));
//        anchors.add(Range.closed(Position.of("9",63200),Position.of("9",63320)));
//
//
////        RangeMap<Position,Tuple<String,String>> nonMappedSnpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings,refSeq,asmSeq);
//        System.out.println("Done Processing nonMapped");
////        snpMap.putAll(nonMappedSnpMap);
//
//        RangeSet<Position> refMappingPositions = refMappings
//                .asMapOfRanges()
//                .keySet()
//                .stream()
//                .collect(Collector.of(TreeRangeSet::create,
//                        (set, range) -> set.add((Range<Position>)range),
//                        (set1,set2) -> {
//                            set1.addAll(set2);
//                            return set1;
//                        }));
//
//
//        System.out.println("SNP map");
//        Map<Range<Position>,Tuple<String,String>> snpMapAsRanges = snpMap.asMapOfRanges();
//        for(Range<Position> positions : snpMapAsRanges.keySet()) {
//            if(positions.lowerEndpoint().getPosition()>63200 && positions.upperEndpoint().getPosition()<63320) {
//                System.out.println(positions.lowerEndpoint().getPosition() + "," + positions.upperEndpoint().getPosition() + "\t" + snpMapAsRanges.get(positions));
//            }
//        }
//
//
//        List<VariantContext> vcs = AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSeq, "PH207", anchors,refMappings,snpMap);
////
//        System.out.println("Variants");
//        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>63200 && variantContext.getEnd() < 63320).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));
//
//    }
//
//    @Test
//    public void debugApr4Error2() {
//        String snpFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/ErrorApr4/ref_PH207_c250mum.snps";
//        String coordFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/ErrorApr4/ref_PH207_c250mum.coords";
//
//
//        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(snpFile,"9");
//        System.out.println("Done with snpmap");
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");
//
//        System.out.println("Done processing refmappings");
//        RangeSet<Position> anchors = TreeRangeSet.create();
//
//        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/b73chr9.fa";
//        GenomeSequence refSeq = GenomeSequenceBuilder.instance(b73File);
//
//        String ph207File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/ph207chr9.fasta";
//        GenomeSequence asmSeq = GenomeSequenceBuilder.instance(ph207File);
//
//
//        //63280
////        anchors.add(Range.closed(Position.of("9",58300),Position.of("9",67504)));
//        anchors.add(Range.closed(Position.of("9",73160),Position.of("9",73200)));
//
//        System.out.println("Starting processing nonMappedSNP.");
//
//        RangeMap<Position,Tuple<String,String>> nonMappedSnpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings,refSeq,asmSeq);
//        System.out.println("Done Processing nonMapped");
////        snpMap.putAll(nonMappedSnpMap);
//
//        RangeSet<Position> refMappingPositions = refMappings
//                .asMapOfRanges()
//                .keySet()
//                .stream()
//                .collect(Collector.of(TreeRangeSet::create,
//                        (set, range) -> set.add((Range<Position>)range),
//                        (set1,set2) -> {
//                            set1.addAll(set2);
//                            return set1;
//                        }));
//
//
//        System.out.println("SNP map");
//        Map<Range<Position>,Tuple<String,String>> snpMapAsRanges = snpMap.asMapOfRanges();
//        for(Range<Position> positions : snpMapAsRanges.keySet()) {
//            if(positions.lowerEndpoint().getPosition()>73160 && positions.upperEndpoint().getPosition()<73200) {
//                System.out.println(positions.lowerEndpoint().getPosition() + "," + positions.upperEndpoint().getPosition() + "\t" + snpMapAsRanges.get(positions));
//            }
//        }
//
//
//        List<VariantContext> vcs = AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSeq, "PH207", anchors,refMappings,snpMap);
////
//        System.out.println("Variants");
//        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>73160 && variantContext.getEnd() < 73200).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));
//
//    }
//
//    @Test
//    public void debugApr4Error3() {
//        String coordFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/GNoOverlaps/ref_PH207_c250mum.coords_deltaG";
//        String snpFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/GNoOverlaps/ref_PH207_c250mum.snps_optionsG_C";
//
//
//        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(snpFile,"9");
//        System.out.println("Done with snpmap");
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");
//
//        System.out.println("Done processing refmappings");
//        RangeSet<Position> anchors = TreeRangeSet.create();
//
//        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/b73chr9.fa";
//        GenomeSequence refSeq = GenomeSequenceBuilder.instance(b73File);
//
//        String ph207File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/ph207chr9.fasta";
//        GenomeSequence asmSeq = GenomeSequenceBuilder.instance(ph207File);
//
//
//        //63280
////        anchors.add(Range.closed(Position.of("9",58300),Position.of("9",67504)));
//        anchors.add(Range.closed(Position.of("9",73160),Position.of("9",73200)));
//
//        System.out.println("Starting processing nonMappedSNP.");
//
//        RangeMap<Position,Tuple<String,String>> nonMappedSnpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings,refSeq,asmSeq);
//        System.out.println("Done Processing nonMapped");
////        snpMap.putAll(nonMappedSnpMap);
//
//        RangeSet<Position> refMappingPositions = refMappings
//                .asMapOfRanges()
//                .keySet()
//                .stream()
//                .collect(Collector.of(TreeRangeSet::create,
//                        (set, range) -> set.add((Range<Position>)range),
//                        (set1,set2) -> {
//                            set1.addAll(set2);
//                            return set1;
//                        }));
//
//
//        System.out.println("SNP map");
//        Map<Range<Position>,Tuple<String,String>> snpMapAsRanges = snpMap.asMapOfRanges();
//        for(Range<Position> positions : snpMapAsRanges.keySet()) {
//            if(positions.lowerEndpoint().getPosition()>73160 && positions.upperEndpoint().getPosition()<73200) {
//                System.out.println(positions.lowerEndpoint().getPosition() + "," + positions.upperEndpoint().getPosition() + "\t" + snpMapAsRanges.get(positions));
//            }
//        }
//
//
//        List<VariantContext> vcs = AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSeq, "PH207", anchors,refMappings,snpMap);
////
//        System.out.println("Variants");
//        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>73160 && variantContext.getEnd() < 73200).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));
//        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>63200 && variantContext.getEnd() < 63320).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));
//
//    }
//

    @Test
    public void debugApr6Error3() {
        String coordFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/LynnBugApr6/ref_PH207_c250mumdeltaGsnpC.coords";
        String snpFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/LynnBugApr6/ref_PH207_c250mumdeltaGsnpC.snps";


        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(snpFile,"9");
        System.out.println("Done with snpmap");
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");
        Map<Range<Position>,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");

        System.out.println("Done processing refmappings");
        RangeSet<Position> anchors = TreeRangeSet.create();

        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/b73chr9.fa";
        GenomeSequence refSeq = GenomeSequenceBuilder.instance(b73File);

        String ph207File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/ph207chr9.fasta";
        GenomeSequence asmSeq = GenomeSequenceBuilder.instance(ph207File);


        //63280
//        anchors.add(Range.closed(Position.of("9",58300),Position.of("9",67504)));
        anchors.add(Range.closed(Position.of("9",73160),Position.of("9",73200)));

        System.out.println("Starting processing nonMappedSNP.");

        RangeMap<Position,Tuple<String,String>> nonMappedSnpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings,refSeq,asmSeq);
        System.out.println("Done Processing nonMapped");
        snpMap.putAll(nonMappedSnpMap);

//        RangeSet<Position> refMappingPositions = refMappings
//                .asMapOfRanges()
//                .keySet()
//                .stream()
//                .collect(Collector.of(TreeRangeSet::create,
//                        (set, range) -> set.add((Range<Position>)range),
//                        (set1,set2) -> {
//                            set1.addAll(set2);
//                            return set1;
//                        }));


        System.out.println("SNP map");
        Map<Range<Position>,Tuple<String,String>> snpMapAsRanges = snpMap.asMapOfRanges();
        for(Range<Position> positions : snpMapAsRanges.keySet()) {
            if(positions.lowerEndpoint().getPosition()>16320 && positions.upperEndpoint().getPosition()<16860) {
                System.out.println(positions.lowerEndpoint().getPosition() + "," + positions.upperEndpoint().getPosition() + "\t" + snpMapAsRanges.get(positions));
            }
        }


        List<VariantContext> vcs = AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSeq, "PH207", anchors,refMappings,snpMap);
//
        System.out.println("Variants");
//        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>73160 && variantContext.getEnd() < 73200).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));
        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>16000 && variantContext.getEnd() < 17510).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));


        System.out.println("Error region:");
        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>17289197 && variantContext.getEnd() < 17293644).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

        //Attempt to make a gvcfSequence
        GenomeSequence gvcfSequence = GVCFSequence.instance(refSeq,vcs,false,"PH207");

    }

    @Test
    public void debugApr8ASMDebug() {
        String coordFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/LynnBugApr6/ref_PH207_c250mumdeltaGsnpC.coords";
        String snpFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/LynnBugApr6/ref_PH207_c250mumdeltaGsnpC.snps";


        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(snpFile,"9");
        System.out.println("Done with snpmap");
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");
        Map<Range<Position>,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");

        System.out.println("Done processing refmappings");
        RangeSet<Position> anchors = TreeRangeSet.create();

        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/b73chr9.fa";
        GenomeSequence refSeq = GenomeSequenceBuilder.instance(b73File);

        String ph207File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/ph207chr9.fasta";
        GenomeSequence asmSeq = GenomeSequenceBuilder.instance(ph207File);


        //63280
//        anchors.add(Range.closed(Position.of("9",58300),Position.of("9",67504)));
        anchors.add(Range.closed(Position.of("9",73160),Position.of("9",73200)));

        System.out.println("Starting processing nonMappedSNP.");

        RangeMap<Position,Tuple<String,String>> nonMappedSnpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings,refSeq,asmSeq);
        System.out.println("Done Processing nonMapped");
        snpMap.putAll(nonMappedSnpMap);

        System.out.println("SNP map");
        Map<Range<Position>,Tuple<String,String>> snpMapAsRanges = snpMap.asMapOfRanges();
        for(Range<Position> positions : snpMapAsRanges.keySet()) {
            if(positions.lowerEndpoint().getPosition()>0 && positions.upperEndpoint().getPosition()<9913) {
                System.out.println(positions.lowerEndpoint().getPosition() + "," + positions.upperEndpoint().getPosition() + "\t" + snpMapAsRanges.get(positions));
            }
        }


        List<VariantContext> vcs = AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSeq, "PH207", anchors,refMappings,snpMap);
//
        System.out.println("Variants");
        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>0 && variantContext.getEnd() < 9913).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));
        System.out.println("Second region");
        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>16132 && variantContext.getEnd() <= 17903).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

        System.out.println("Third region:");
        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>21134 && variantContext.getEnd() <= 63711).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

        //Attempt to make a gvcfSequence
        GenomeSequence gvcfSequence = GVCFSequence.instance(refSeq,vcs,false,"PH207");

    }


    @Test
    public void createMergedCoords() {
        String coordFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/LynnBugApr6/ref_PH207_c250mumdeltaGsnpC.coords";
//        String coordFileOutput = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/LynnBugApr6/ref_PH207_c250mumdeltaGsnpC_MergedRefRanges.coords";
        String coordFileOutput = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/LynnBugApr6/ref_PH207_c250mumdeltaGsnpC_ResizedRefRanges.coords";

//        Map<Range<Position>,List<Position>> refMappings = AssemblyProcessingUtils.mergeCoords(coordFile,"9");
        Map<Range<Position>,List<Position>> refMappings = AssemblyProcessingUtils.resizeCoords(coordFile,"9");

        AssemblyProcessingUtils.exportMergedRegions(refMappings,coordFileOutput);

    }


    /**
     * Test to see if we can resize the gvcfs based on the anchor start and end positions.
     */
    @Test
    public void testResizeAnchors() {
        String coordFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/LynnBugApr6/ref_PH207_c250mumdeltaGsnpC.coords";
        String snpFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/LynnBugApr6/ref_PH207_c250mumdeltaGsnpC.snps";


        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(snpFile,"9");
        System.out.println("Done with snpmap");
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");
        Map<Range<Position>,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");

        System.out.println("Done processing refmappings");
        RangeSet<Position> anchors = TreeRangeSet.create();

        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/b73chr9.fa";
        GenomeSequence refSeq = GenomeSequenceBuilder.instance(b73File);

        String ph207File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/ph207chr9.fasta";
        GenomeSequence asmSeq = GenomeSequenceBuilder.instance(ph207File);



        System.out.println("Starting processing nonMappedSNP.");

        RangeMap<Position,Tuple<String,String>> nonMappedSnpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings,refSeq,asmSeq);
        System.out.println("Done Processing nonMapped");
        snpMap.putAll(nonMappedSnpMap);

        System.out.println("SNP map");
        Map<Range<Position>,Tuple<String,String>> snpMapAsRanges = snpMap.asMapOfRanges();
        for(Range<Position> positions : snpMapAsRanges.keySet()) {
            if(positions.lowerEndpoint().getPosition()>0 && positions.upperEndpoint().getPosition()<9913) {
                System.out.println(positions.lowerEndpoint().getPosition() + "," + positions.upperEndpoint().getPosition() + "\t" + snpMapAsRanges.get(positions));
            }
        }


        List<VariantContext> vcs = AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSeq, "PH207", anchors,refMappings,snpMap);
        System.out.println("Third region:");
//        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>21134 && variantContext.getEnd() <= 63711).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));
        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>=45189 && variantContext.getEnd() <= 58007).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));
        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>=83679 && variantContext.getEnd() <= 90981).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));


        System.out.println("Third region resized:");

        Map<Integer,ReferenceRange> anchorMapping = new HashMap<>();
        anchorMapping.put(0,new ReferenceRange("B73", Chromosome.instance("9"), 56800, 57702 , 0));
        anchorMapping.put(1,new ReferenceRange("B73", Chromosome.instance("9"), 86164, 86722 , 1));
        anchorMapping.put(2,new ReferenceRange("B73", Chromosome.instance("9"), 86725, 86978 , 2));
        List<VariantContext> vcsResized = AssemblyProcessingUtils.splitRefRange(vcs, anchorMapping, refSeq);
        System.out.println(vcsResized.stream().filter(variantContext -> variantContext.getStart()>=45189 && variantContext.getEnd() <= 58007).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));
        System.out.println(vcsResized.stream().filter(variantContext -> variantContext.getStart()>=83679 && variantContext.getEnd() <= 90981).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

        //Attempt to make a gvcfSequence
        GenomeSequence gvcfSequence = GVCFSequence.instance(refSeq,vcs,false,"PH207");

    }


    @Test
    public void debugApr16ASMDebug() {
//        String coordFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Apr16Error/2ndTry/ref_PH207_9_c250mumdeltaGsnpC.coords_final_april16_2";
//        String snpFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Apr16Error/2ndTry/ref_PH207_9_c250mumdeltaGsnpC.snps_final_april16_2";


        String coordFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Apr16Error/3rdTry/ref_PH207_9_c250mumdeltaGsnpC.coords_final_april16_4";
        String snpFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Apr16Error/3rdTry/ref_PH207_9_c250mumdeltaGsnpC.snps_final_april16_4";


        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(snpFile,"9");
        System.out.println("Done with snpmap");
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");
        Map<Range<Position>,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");

        System.out.println("Done processing refmappings");
        RangeSet<Position> anchors = TreeRangeSet.create();

        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/b73chr9.fa";
        GenomeSequence refSeq = GenomeSequenceBuilder.instance(b73File);

        String ph207File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/ph207chr9.fasta";
        GenomeSequence asmSeq = GenomeSequenceBuilder.instance(ph207File);


        System.out.println("Starting processing nonMappedSNP.");

        RangeMap<Position,Tuple<String,String>> nonMappedSnpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings,refSeq,asmSeq);
        System.out.println("Done Processing nonMapped");
        snpMap.putAll(nonMappedSnpMap);

        System.out.println("SNP map");
        Map<Range<Position>,Tuple<String,String>> snpMapAsRanges = snpMap.asMapOfRanges();
        for(Range<Position> positions : snpMapAsRanges.keySet()) {
            if(positions.lowerEndpoint().getPosition()>=57890689 && positions.upperEndpoint().getPosition()<=57900700) {
                System.out.println(positions.lowerEndpoint().getPosition() + "," + positions.upperEndpoint().getPosition() + "\t" + snpMapAsRanges.get(positions));
            }
        }


        List<VariantContext> vcs = AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSeq, "PH207", anchors,refMappings,snpMap);

        System.out.println("Third region:");
//        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>=57898946 && variantContext.getEnd() <= 57900700).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));
//        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>=57890689 && variantContext.getEnd() <= 57900700).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));
        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>=57873244 && variantContext.getEnd() <= 58029727).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

        //Attempt to make a gvcfSequence
        GenomeSequence gvcfSequence = GVCFSequence.instance(refSeq,vcs,false,"PH207");

    }


    @Test
    public void debugApr18ASMDebug() {
        String coordFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Apr17Error/Try1/ref_PH207_9_c250mumdeltaGPlus1000.coords_final";
        String snpFile = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Apr17Error/Try1/ref_PH207_9_c250mumdeltaGPlus1000.snps_final";


        RangeMap<Position,Tuple<String,String>> snpMap = AssemblyProcessingUtils.parseMummerSNPFile(snpFile,"9");
        System.out.println("Done with snpmap");
//        RangeMap<Position,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");
        Map<Range<Position>,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordFile,"9");

        System.out.println("Done processing refmappings");
        RangeSet<Position> anchors = TreeRangeSet.create();

        String b73File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/b73chr9.fa";
        GenomeSequence refSeq = GenomeSequenceBuilder.instance(b73File);

        String ph207File = "/Volumes/ZackBackup/Temp/Pangenome/AssemblyPipeline/Align100kbRegions/Chrom9/ph207chr9.fasta";
        GenomeSequence asmSeq = GenomeSequenceBuilder.instance(ph207File);


        System.out.println("Starting processing nonMappedSNP.");

        RangeMap<Position,Tuple<String,String>> nonMappedSnpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings,refSeq,asmSeq);
        System.out.println("Done Processing nonMapped");
        snpMap.putAll(nonMappedSnpMap);

        System.out.println("SNP map");
        Map<Range<Position>,Tuple<String,String>> snpMapAsRanges = snpMap.asMapOfRanges();
        for(Range<Position> positions : snpMapAsRanges.keySet()) {
            if(positions.lowerEndpoint().getPosition()>=58101 && positions.upperEndpoint().getPosition()<=67704) {
                System.out.println(positions.lowerEndpoint().getPosition() + "," + positions.upperEndpoint().getPosition() + "\t" + snpMapAsRanges.get(positions));
            }
        }

        Map<Integer,ReferenceRange> anchorMapping = new HashMap<>();
//        58301|67504
        anchorMapping.put(0,new ReferenceRange("B73", Chromosome.instance("9"), 0, 58300 , 0));
        anchorMapping.put(1,new ReferenceRange("B73", Chromosome.instance("9"), 58301, 67504 , 1));

        List<VariantContext> vcs = AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSeq, "PH207", anchors,refMappings,snpMap);

        System.out.println("Third region:");

        System.out.println(vcs.stream().filter(variantContext -> variantContext.getStart()>=56720 && variantContext.getEnd() <= 67704).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

        List<VariantContext> vcsResized = AssemblyProcessingUtils.splitRefRange(vcs, anchorMapping, refSeq);
        System.out.println("RESIZED:********************************");
        System.out.println(vcsResized.stream().filter(variantContext -> variantContext.getStart()>=56720 && variantContext.getEnd() <= 67704).map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

        //Attempt to make a gvcfSequence
        GenomeSequence gvcfSequence = GVCFSequence.instance(refSeq,vcs,false,"PH207");
//        GenomeSequence gvcfSequenceResized = GVCFSequence.instance(refSeq,vcsResized,false,"PH207");

    }
    
    @Test
    public void testGetEntryFromTabDelimitedLine() {
        String mline = "64\tT\tC\t68\t1\tme\tyou\t0";
        int colNumber = 3;
        int totalColumns = 8;
        String colValue = AssemblyProcessingUtils.getEntryFromTabDelimitedLine( mline,  colNumber, totalColumns);
        System.out.println(mline);        
        assertEquals(colValue,"C");

        colValue = AssemblyProcessingUtils.getEntryFromTabDelimitedLine( mline,  1, totalColumns);
        assertEquals(colValue,"64");
        
        colValue = AssemblyProcessingUtils.getEntryFromTabDelimitedLine( mline,  8, totalColumns);
        assertEquals(colValue,"0");
        
        System.out.println("Finished");

    }
    
    @Test
    public void testSplitOverlappingCoordsEntry(){
        List<String> coordsList = new ArrayList<String>();
        
        // Entries 3 and 4 overlap
        coordsList.add("2192\t3077\t160850\t161742\t886\t893\t96.21\t9\tchr9");
        coordsList.add("4841\t6721\t163405\t165285\t1881\t1881\t97.13\t9\tchr9");
        coordsList.add("9813\t12176\t167454\t169817\t2364\t2364\t98.52\t9\tchr9");
        coordsList.add("12124\t12880\t170318\t171075\t757\t758\t98.55\t9\tchr9");
        coordsList.add("13661\t16332\t171576\t174252\t2672\t2677\t98.39\t9\tchr9");
        coordsList.add("16828\t17510\t174753\t175435\t683\t683\t98.54\t9\tchr9");
        coordsList.add("17611\t18610\t200000\t190000\t1000\t1001\t98.54\t9\tchr9");
        coordsList.add("18611\t19610\t190500\t180500\t1000\t1001\t98.54\t9\tchr9");
        
        // Running it to process both the overlaps and the embedded regions
        //TODO LCJ - need to create a List<String> snpList to pass to splitOverlappingCoordsEntries
        // overlap handling has changed to avoid aplitting in the middle of an indel
//        List<String> findResultsRef = MummerScriptProcessing.splitOverlappingCoordsEntries(coordsList,true);
//        System.out.println("Results from splitOverlappingCoordsEntries: ");
//        for (String result : findResultsRef) {
//            System.out.println(result);
//        }
//        
//        // Size should remain the same.
//        assertEquals(findResultsRef.size(),coordsList.size());
//
//        // This was an overlapping entry for ref - the coordinates and alignment length changed
//        assertFalse(findResultsRef.contains("12124\t12880\t170318\t171075\t757\t758\t98.55\t9\tchr9"));
//        assertTrue(findResultsRef.contains("12177\t12880\t170371\t171075\t704\t705\t98.55\t9\tchr9"));
//        
//        // This was overlapping entry for assembly, in reverse order on assembly        
//        List<String> findResultsAsm = MummerScriptProcessing.splitOverlappingCoordsEntries(coordsList,false);
//        assertFalse(findResultsAsm.contains("18611\t19610\t190500\t180500\t1000\t1001\t98.54\t9\tchr9"));
//        assertTrue(findResultsAsm.contains("19112\t19610\t189999\t180500\t499\t500\t98.54\t9\tchr9"));
        
        System.out.println("Finished");
    }
    
    @Test
    public void testCheckEmbedded(){
        List<String> coordsList = new ArrayList<String>();
        
        // Entries 3 and 4 overlap
        coordsList.add("2192\t3077\t160850\t161742\t886\t893\t96.21\t9\tchr9");
        coordsList.add("2200\t3000\t160850\t161742\t886\t893\t96.21\t9\tchr9"); // embedded ref
        coordsList.add("4841\t6721\t163405\t165285\t1881\t1881\t97.13\t9\tchr9");
        coordsList.add("9813\t12176\t167454\t169817\t2364\t2364\t98.52\t9\tchr9");
        coordsList.add("12124\t12880\t170318\t171075\t757\t758\t98.55\t9\tchr9");
        coordsList.add("13661\t16332\t171576\t174252\t2672\t2677\t98.39\t9\tchr9");
        coordsList.add("16828\t17510\t174753\t175435\t683\t683\t98.54\t9\tchr9");
        coordsList.add("16828\t17510\t174800\t174900\t683\t683\t98.54\t9\tchr9"); // embedded assembly
        coordsList.add("17611\t18610\t200000\t190000\t1000\t1001\t98.54\t9\tchr9");
        coordsList.add("18611\t19610\t190500\t180500\t1000\t1001\t98.54\t9\tchr9");
        
        // Running it to process both the overlaps and the embedded regions
        List<String> findResultsRef = MummerScriptProcessing.checkForEmbedded(coordsList,true);
        System.out.println("Results from checkForEmbedded: ");
        for (String result : findResultsRef) {
            System.out.println(result);
        }
        
        // Size should be 2 less
        assertEquals(findResultsRef.size(),coordsList.size()-2); // 2 embedded entries removed

        // Embedded ref entry - should be removed
        assertFalse(findResultsRef.contains("2200\t3000\t160850\t161742\t886\t893\t96.21\t9\tchr9"));
        
        // This was an overlapping entry for ref - it should still be present
        assertTrue(findResultsRef.contains("12124\t12880\t170318\t171075\t757\t758\t98.55\t9\tchr9"));
        
        
        // This was overlapping entry for assembly, in reverse order on assembly        
        List<String> findResultsAsm = MummerScriptProcessing.checkForEmbedded(coordsList,false);
        
        assertFalse(findResultsAsm.contains("16828\t17510\t174800\t174900\t683\t683\t98.54\t9\tchr9")); // embedded entry removed
        assertTrue(findResultsAsm.contains("18611\t19610\t190500\t180500\t1000\t1001\t98.54\t9\tchr9"));
 
        
        System.out.println("Finished");
    }
}
