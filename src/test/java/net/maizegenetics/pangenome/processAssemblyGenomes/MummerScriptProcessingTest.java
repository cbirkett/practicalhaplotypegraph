/**
 * 
 */
package net.maizegenetics.pangenome.processAssemblyGenomes;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import net.maizegenetics.util.Tuple;
import net.maizegenetics.util.Utils;

/**
 * This class contains junit tests for methods contained in the source file
 * MummerScriptProcessing
 * 
 * @author lcj34
 *
 */
public class MummerScriptProcessingTest {

    @Test
    public void testFindAlignmentsToReturn() {
        String filteredCoords = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/ref_PHJ40_Assembly_8_c250mumdeltaGPlus1000.coords_filtered";
        String origCoords = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/ref_PHJ40_Assembly_8_c250mumdeltaGPlus1000.coords_orig";
        String returnedCoords = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/PHG40_chr8.coords_returnedLines";
        String removedCoords = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/PHJ40_chr8.coords_removedFromOrig";
        
        String keptLine = "12278500\t12279630\t9426370\t9425240\t1131\t1131\t98.05\t8\t8";
        String notKeptLine = "12261787\t12263182\t155945662\t155944268\t1396\t1395\t99.79\t8\t8";
        
        List<String> deltaLines = new ArrayList<String>();
        List<String> gLines = new ArrayList<String>();
        
        try (BufferedReader deltaBR = Utils.getBufferedReader(origCoords);
             BufferedReader deltaGbr = Utils.getBufferedReader(filteredCoords);
             BufferedWriter bw1 = Utils.getBufferedWriter(returnedCoords);
             BufferedWriter bwRemoved = Utils.getBufferedWriter(removedCoords)){
  
            String line = null;
            while ((line = deltaBR.readLine()) != null) {
                deltaLines.add(line);
            }            

            while ((line = deltaGbr.readLine()) != null) {                
                gLines.add(line); // add to list that gets merged
            }          
            
            List<String> removedLines = new ArrayList<String>();
            for (String entry : deltaLines) {
                if (!gLines.contains(entry) ) {
                    removedLines.add(entry);
                    bwRemoved.write(entry);
                    bwRemoved.write("\n");
                }
            }
            bwRemoved.close();
            deltaBR.close();
            deltaGbr.close();
            
            List<String> returnedLines = new ArrayList<String> ();           
            if (removedLines.size() > 0) {
                System.out.println("Calling findAlignmentsToReturn2, removedLines size: " + removedLines.size() + " gLines.size() " + gLines.size());
                returnedLines = MummerScriptProcessing.findAlignmentsToReturnFromAfterMinusGFilter(removedLines, gLines);
            }
            
            assertTrue(returnedLines.contains(keptLine));           
            assertFalse(returnedLines.contains(notKeptLine));
            
            for (String entry : returnedLines) {
                bw1.write(entry);
                bw1.write("\n");
            }
 
            System.out.println("FInixhed !!");
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        
    }
    
    @Test
    public void testFilterCoordsOverlaps() {
        String coordsNoEmbedded = "/Users/lcj34/notes_Files/phg_2018/assembly_testing/debug/final_overlapFix_SNPerrors/ref_cml103_Assembly_7_c250.coords_filteredPlus_noEmbedded";
        String snpFile = "/Users/lcj34/notes_Files/phg_2018/assembly_testing/debug/final_overlapFix_SNPerrors/ref_cml103_Assembly_7_c250.snps_prefiltered";
        String coordsFinal = "/Users/lcj34/notes_Files/phg_2018/assembly_testing/debug/final_overlapFix_SNPerrors/ref_cml103_Assembly_7_c250.coords_final";
        
        System.out.println("Begin processing filterCoordsOverlaps");
        MummerScriptProcessing.filterCoordsOverlaps(coordsNoEmbedded, snpFile, coordsFinal);
        System.out.println("FInished junit");
    }
    
    
    @Test
    public void testCoordsFileForOverlaps() {
        
        // THe test files were pulled from cbsumm14, 
        //  /workdir/lcj34/phgAssemblyOverlapTest/DockerOutput_masterApril8/align_master
        //      and
        //  /workdir/lcj34/phgAssemblyOverlapTest/DockerOutput_masterApril8/align_overlap
        
        // The overlap STILL has overlaps, because there are cases that originally looked like this (in master)
        //26253   61338   303806  268736  35086   35071   99.57   1       1
        //79815   81437   268735  267113  1623    1623    99.75   1       1
        //81438   89770   266188  257870  8333    8319    98.96   1       1
        
        // that now looked like this (still an oerlap):
        // 26253   61338   303806  268736  35086   35071   99.57   1       1
        // 81438   89770   266188  257870  8333    8319    98.96   1       1
        
        // The issue is direction of prevEntry vs currentEntry:  ascending vs descending mixed

        String coordsFile = "/Users/lcj34/notes_Files/phg_2018/assembly_testing/debug/final_overlapFix_SNPerrors/ref_cml103_Assembly_7_c250.coords_final";
           
        try (BufferedReader br = Utils.getBufferedReader(coordsFile)) {
            System.out.println("Begin reading file");
            String prevEntry = br.readLine();
            int tabIndex1 = prevEntry.indexOf("\t");
            int tabIndex2 = prevEntry.indexOf("\t",tabIndex1+1);
            int tabIndex3 = prevEntry.indexOf("\t",tabIndex2+1);
            int tabIndex4 = prevEntry.indexOf("\t",tabIndex3+1);
            int tabIndex5 = prevEntry.indexOf("\t",tabIndex4+1);
            
            int prevRefStart = Integer.parseInt(prevEntry.substring(0,tabIndex1));
            int prevRefEnd = Integer.parseInt(prevEntry.substring(tabIndex1+1,tabIndex2));
                  
            int prevAsmStart = Integer.parseInt(prevEntry.substring(tabIndex2+1,tabIndex3));
            int prevAsmEnd = Integer.parseInt(prevEntry.substring(tabIndex3+1, tabIndex4));
 
            String currentEntry;
            int refOverlapCount = 0;
            int asmOverlapCount = 0;
            while ((currentEntry = br.readLine()) != null) {
                tabIndex1 = currentEntry.indexOf("\t");
                tabIndex2 = currentEntry.indexOf("\t",tabIndex1+1);
                tabIndex3 = currentEntry.indexOf("\t",tabIndex2+1);
                tabIndex4 = currentEntry.indexOf("\t",tabIndex3+1);
                tabIndex5 = currentEntry.indexOf("\t",tabIndex4+1);

                int refStart = Integer.parseInt(currentEntry.substring(0,tabIndex1));
                int refEnd = Integer.parseInt(currentEntry.substring(tabIndex1+1,tabIndex2));
                int asmStart = Integer.parseInt(currentEntry.substring(tabIndex2+1,tabIndex3));
                int asmEnd = Integer.parseInt(currentEntry.substring(tabIndex3+1, tabIndex4));
                
                if (currentEntry.startsWith("892990")) {
                    System.out.println("Processing 892990");
                }
                Tuple<Integer,Boolean> refResult = 
                        MummerScriptProcessing.checkForOverlap(new Tuple<Integer,Integer>(prevRefStart, prevRefEnd), 
                                new Tuple<Integer,Integer>(refStart,refEnd));
                
                Tuple<Integer,Boolean> asmResult = 
                        MummerScriptProcessing.checkForOverlap(new Tuple<Integer,Integer>(prevAsmStart, prevAsmEnd), 
                                new Tuple<Integer,Integer>(asmStart,asmEnd));
                
                if (refResult.getX() > 0) {
                    refOverlapCount++;
                    System.out.println("REF overlap a currentEntry " + currentEntry);
                }
                if (asmResult.getX() > 0) {
                    asmOverlapCount++;
                    System.out.println("ASM overlap a currentEntry " + currentEntry);
                }
                prevEntry = currentEntry;
                prevRefStart = refStart;
                prevRefEnd = refEnd;
                prevAsmStart = asmStart;
                prevAsmEnd = asmEnd;
            }
            
            System.out.println("FINished! - ref overlaps: " + refOverlapCount + ", asm overlaps: " + asmOverlapCount);
        } catch (Exception exc) {
            exc.printStackTrace();
        }

    }
    
    @Test
    public void testFinalSnpFIltering() {
        // This to test April 19, 2019 error.  After coords overlap are fixed,
        // there are a few duplicate SNP entries.  This test added to create the
        // final SNPs file to verify previous duplicates were removed. 
        
        String deltaPrefix = "ref_B104_Assembly_1_c250";
        String debugDir = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/final_overlapFix_SNPerrors/";
        String origSnpFile = debugDir + deltaPrefix + ".snps_prefiltered";
        String chrom = "1";
        
        String outputDeltaFilePrefix = debugDir + deltaPrefix;
        MummerScriptProcessing.finalSnpFiltering( origSnpFile,  outputDeltaFilePrefix,  chrom);
    }
    
}
