/**
 * 
 */
package net.maizegenetics.pangenome.processAssemblyGenomes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import net.maizegenetics.util.DirectoryCrawler;
import net.maizegenetics.util.Utils;

/**
 * Tests the plugin CreateContigFastaFromAssemblyGenomePlugin
 * from a short test fasta. 
 * 
 * Test data is stored in box sync/MaizePHG/PHG_junit_data - file testGenomeForGContigFasta.fa
 *
 * @author lcj34
 *
 */
public class CreateContigFastaFromAssemblyGenomePluginTest {
    
    public static final String userHome = System.getProperty("user.home");
    public static final String outputDir = userHome + "/temp/CreateContigFastaFromAssemblyGenomePlugin/";
    public static final String dataDir = outputDir+"data/";
    
    public static final String junitDataDir = userHome + "/Box Sync/MaizePHG/PHG_junit_data/";
    @Test
    public  void testCreateContigs() {
      
        try {
            if (Files.exists(Paths.get(outputDir))) FileUtils.deleteDirectory(new File(outputDir));
            Files.createDirectories(Paths.get(outputDir));
            Files.createDirectories(Paths.get(dataDir));
            for (Path path : DirectoryCrawler.listPaths("glob:*",Paths.get(junitDataDir))) {
                Files.copy(path,Paths.get(dataDir+path.getFileName()));
            } 
        } catch (IOException ioe) {
            
        }
 
        String genomeFile = dataDir + "testGenomeForContigFasta.fa";
        String assembly = "Test";
        
        new CreateContigFastaFromAssemblyGenomePlugin()
        .assembly(assembly)
        .genomeFile(genomeFile)
        .outputDir(outputDir)
        .performFunction(null);
        
        // A small assembly was created with sequence containing N's.
        // CreateContigFatasFromAssemblyGenomePlugin splits it where N's occur.
 
        String outputContigs = outputDir + assembly + "_asContigs.fa";
        Map<String,String> nameToSequence = new HashMap<String,String>();
        
        try (BufferedReader br = Utils.getBufferedReader(outputContigs)) {
            String line;
            StringBuilder sb = new StringBuilder();
            String curContig = "none";
            while ((line = br.readLine()) != null) {                
                if (line.startsWith(">")) {  
                    if (sb.length() > 0) {
                        nameToSequence.put(curContig, sb.toString());
                        sb.setLength(0);
                    }
                    curContig = line;
                } else {
                    sb.append(line);
                }
            }
            if (sb.length() > 0) {
                nameToSequence.put(curContig, sb.toString());
            }
        } catch (Exception exc) {
            exc.printStackTrace();
            throw new IllegalStateException("Error reading output contigs file " + exc.getMessage());
        }
        // Verify the initial header lines for each chrom, and the contig sequences that should
        // have been created.  Verify both chromosomes are processed as well as sequence past last N
        assertEquals(nameToSequence.keySet().size(),8); // splitting by N's should leave us with 8 contigs
        assertTrue(nameToSequence.containsKey(">chr13:1:86")); // 85 alleles before first N
        assertTrue(nameToSequence.containsValue("CCCAAGTCAACCGGATATTCCAAAGCAAGGGTTATGAATCAAAAGAGAAGTGCCAGCTGGATCTCTGGTTCAAAGTTCAGGGCATG"));
        assertTrue(nameToSequence.containsValue("GAAAAGGAAATCCCATTCGTCAACCTAAATGGAAGGGCAGGTAAGGGAAGGCCAGCAGGAAATTCAGTAAGT"));
        assertTrue(nameToSequence.containsValue("AAAAGGTATGTATGAACCAACAAAGGTCAAGTAGACGAACACATATCCACTCCACTAGAGAAAGCGTCGAACCAGGG"));
        assertTrue(nameToSequence.containsValue("CTTTCGAGCTTCTTATTCGATTTCTTAAATGACTTCTTCGG"));
        
        assertTrue(nameToSequence.containsKey(">chr14:1:45")); // 45 alleles before first N
        assertTrue(nameToSequence.containsValue("CGGCGACTGGATGACAGAATGGTTTTATGTAAAAAATGACCTGAA"));
        assertTrue(nameToSequence.containsValue("ATCTGTTCTTTTATTGGAACGAGAGATTTGGTACAAGAGCATATTGCCTTCAGGGTATGGCCACT"));
        assertTrue(nameToSequence.containsValue("TTGAG"));
        assertTrue(nameToSequence.containsValue("CTATCGTTATCCCATTCGAGGGCAGAAAAGAAAAAACACAAC"));
                       
        System.out.println("testCreateContigs Successfully Done !!");
    }
}
