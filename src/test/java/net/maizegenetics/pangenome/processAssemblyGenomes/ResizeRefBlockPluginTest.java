package net.maizegenetics.pangenome.processAssemblyGenomes;

import org.junit.Test;

public class ResizeRefBlockPluginTest {
    @Test
    public void testW22() {
        String vcfFile = "/Volumes/ZackBackup2018/Temp/PHG/AssemblyProcessing/ErrorCheckW22/MummerOutputs/InsFixTest/redoVCF/W22InsCorrected_norm_sort.g.vcf";
        String vcfFileCorrected = "/Volumes/ZackBackup2018/Temp/PHG/AssemblyProcessing/ErrorCheckW22/MummerOutputs/InsFixTest/redoVCF/W22InsCorrected_norm_sort_corrected.g.vcf";
        String reference = "/Volumes/ZackBackup2018/Temp/PHG/AssemblyProcessing/ErrorCheckW22/ref/Zea_mays.AGPv4.dna.toplevelMtPtv3.fa";
        new ResizeRefBlockPlugin(null,false)
                .vCFFile(vcfFile)
                .vCFFileCorrected(vcfFileCorrected)
                .ref(reference)
                .taxon("W22_Assembly")
                .processData(null);
    }
}
