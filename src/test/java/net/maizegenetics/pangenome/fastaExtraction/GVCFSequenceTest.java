package net.maizegenetics.pangenome.fastaExtraction;

import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.vcf.VCFFileReader;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import org.junit.*;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by zrm22 on 7/19/17.
 */
public class GVCFSequenceTest {

    private static GenomeSequence refSequence;
    private static final String refFileName = "data/B73_AGPv4_Chr8_first600bps.fa";

    @AfterClass
    public static void tearDownClass() {
    }

    @BeforeClass
    public static void setUp() {
        refSequence = GenomeSequenceBuilder.instance(refFileName);
    }

    @After
    public void tearDown() {
    }

    //Simple test which contains SNPs, Insertions, deletions and missing GVCF rows
    @Test
    public void loadSimpleGVCFAndExtractFasta() {
        GenomeSequence gvcfSeq = GVCFSequence.instance(refSequence,"data/simpleGVCFTestFile.g.vcf");
        String fullSequence = gvcfSeq.genotypeAsString(new Chromosome("8"),1,65);
        Assert.assertEquals("Full Sequence does not match expected","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGAAGAAACCATTGGTCANNNNTCCGTA",fullSequence);
    }

    @Test
    public void testGVCFBoundaryCases() {
        GenomeSequence gvcfSeq = GVCFSequence.instance(refSequence,"data/simpleGVCFTestFile.g.vcf");
        String sequenceStartingInMiddleRefBlock = gvcfSeq.genotypeAsString(new Chromosome("8"),10,65);
        Assert.assertEquals("Sequence starting in the middle of a refblock does not match expected","TATAGATAAAATTCTTGTATTTTTGGTGAAGAAACCATTGGTCANNNNTCCGTA",sequenceStartingInMiddleRefBlock);

        String sequenceStartingWithSNP = gvcfSeq.genotypeAsString(new Chromosome("8"),38,65);
        Assert.assertEquals("Sequence starting at SNP does not match expected","AAGAAACCATTGGTCANNNNTCCGTA",sequenceStartingWithSNP);

        String sequenceStartingWithInsert = gvcfSeq.genotypeAsString(new Chromosome("8"),41,65);
        Assert.assertEquals("Sequence starting at Insert does not match expected","AAACCATTGGTCANNNNTCCGTA",sequenceStartingWithInsert);

        String sequenceStartingWithDeletion = gvcfSeq.genotypeAsString(new Chromosome("8"),46,65);
        Assert.assertEquals("Sequence starting at deletion does not match expected","TGGTCANNNNTCCGTA",sequenceStartingWithDeletion);

        String sequenceStartingWithMissing = gvcfSeq.genotypeAsString(new Chromosome("8"),56,65);
        Assert.assertEquals("Sequence starting with Missing/Filtered does not match expected","NNNNTCCGTA",sequenceStartingWithMissing);


        String sequenceEndingInMiddleRefBlock = gvcfSeq.genotypeAsString(new Chromosome("8"),1,43);
        Assert.assertEquals("Sequence ending in the middle of a refblock does not match expected","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGAAGAAACC",sequenceEndingInMiddleRefBlock);

        String sequenceEndingWithSNP = gvcfSeq.genotypeAsString(new Chromosome("8"),1,38);
        Assert.assertEquals("Sequence ending with a SNP does not match expected","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGA",sequenceEndingWithSNP);

        String sequenceEndingWithInsert = gvcfSeq.genotypeAsString(new Chromosome("8"),1,41);
        Assert.assertEquals("Sequence ending at Insert does not match expected","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGAAGAAA",sequenceEndingWithInsert);

        String sequenceEndingWithDeletion = gvcfSeq.genotypeAsString(new Chromosome("8"),1,46);
        Assert.assertEquals("Sequence ending at deletion does not match expected","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGAAGAAACCATT",sequenceEndingWithDeletion);

        String sequenceEndingInMiddleOfDeletion = gvcfSeq.genotypeAsString(new Chromosome("8"),1,47);
        Assert.assertEquals("Sequence ending in the middle of deletion does Sequence ending at deletion.",sequenceEndingWithDeletion,sequenceEndingInMiddleOfDeletion);

        String sequenceEndingWithMissing = gvcfSeq.genotypeAsString(new Chromosome("8"),1,56);
        Assert.assertEquals("Sequence ending with missing does not match expected","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGAAGAAACCATTGGTCAN",sequenceEndingWithMissing);

        String sequenceEndingInMiddleOfMissing = gvcfSeq.genotypeAsString(new Chromosome("8"),1,58);
        Assert.assertEquals("Sequence ending in middle of missing does not match expected","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGAAGAAACCATTGGTCANNN",sequenceEndingInMiddleOfMissing);

        String deleteInsertTest = gvcfSeq.genotypeAsString(new Chromosome("8"),1,70);
        Assert.assertEquals("Sequence Deletion Insertion Test fails","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGAAGAAACCATTGGTCANNNNTCCGTAGGGGGN",deleteInsertTest);
    }

    @Test
    public void testGVCFBoundaryCasesHTSJDK() {
        VCFFileReader reader = new VCFFileReader(new File("data/simpleGVCFTestFile.g.vcf"), false);

        List<VariantContext> variants = reader.iterator().stream().collect(Collectors.toList());

        GenomeSequence gvcfSeq = GVCFSequence.instance(refSequence,variants,false,"B73");

        String sequenceStartingInMiddleRefBlock = gvcfSeq.genotypeAsString(new Chromosome("8"),10,65);
        Assert.assertEquals("Sequence starting in the middle of a refblock does not match expected","TATAGATAAAATTCTTGTATTTTTGGTGAAGAAACCATTGGTCANNNNTCCGTA",sequenceStartingInMiddleRefBlock);

        String sequenceStartingWithSNP = gvcfSeq.genotypeAsString(new Chromosome("8"),38,65);
        Assert.assertEquals("Sequence starting at SNP does not match expected","AAGAAACCATTGGTCANNNNTCCGTA",sequenceStartingWithSNP);

        String sequenceStartingWithInsert = gvcfSeq.genotypeAsString(new Chromosome("8"),41,65);
        Assert.assertEquals("Sequence starting at Insert does not match expected","AAACCATTGGTCANNNNTCCGTA",sequenceStartingWithInsert);

        String sequenceStartingWithDeletion = gvcfSeq.genotypeAsString(new Chromosome("8"),46,65);
        Assert.assertEquals("Sequence starting at deletion does not match expected","TGGTCANNNNTCCGTA",sequenceStartingWithDeletion);

        String sequenceStartingWithMissing = gvcfSeq.genotypeAsString(new Chromosome("8"),56,65);
        Assert.assertEquals("Sequence starting with Missing/Filtered does not match expected","NNNNTCCGTA",sequenceStartingWithMissing);


        String sequenceEndingInMiddleRefBlock = gvcfSeq.genotypeAsString(new Chromosome("8"),1,43);
        Assert.assertEquals("Sequence ending in the middle of a refblock does not match expected","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGAAGAAACC",sequenceEndingInMiddleRefBlock);

        String sequenceEndingWithSNP = gvcfSeq.genotypeAsString(new Chromosome("8"),1,38);
        Assert.assertEquals("Sequence ending with a SNP does not match expected","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGA",sequenceEndingWithSNP);

        String sequenceEndingWithInsert = gvcfSeq.genotypeAsString(new Chromosome("8"),1,41);
        Assert.assertEquals("Sequence ending at Insert does not match expected","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGAAGAAA",sequenceEndingWithInsert);

        String sequenceEndingWithDeletion = gvcfSeq.genotypeAsString(new Chromosome("8"),1,46);
        Assert.assertEquals("Sequence ending at deletion does not match expected","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGAAGAAACCATT",sequenceEndingWithDeletion);

        String sequenceEndingInMiddleOfDeletion = gvcfSeq.genotypeAsString(new Chromosome("8"),1,47);
        Assert.assertEquals("Sequence ending in the middle of deletion does Sequence ending at deletion.",sequenceEndingWithDeletion,sequenceEndingInMiddleOfDeletion);

        String sequenceEndingWithMissing = gvcfSeq.genotypeAsString(new Chromosome("8"),1,56);
        Assert.assertEquals("Sequence ending with missing does not match expected","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGAAGAAACCATTGGTCAN",sequenceEndingWithMissing);

        String sequenceEndingInMiddleOfMissing = gvcfSeq.genotypeAsString(new Chromosome("8"),1,58);
        Assert.assertEquals("Sequence ending in middle of missing does not match expected","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGAAGAAACCATTGGTCANNN",sequenceEndingInMiddleOfMissing);

        String deleteInsertTest = gvcfSeq.genotypeAsString(new Chromosome("8"),1,70);
        Assert.assertEquals("Sequence Deletion Insertion Test fails","TTTCCTTGCTATAGATAAAATTCTTGTATTTTTGGTGAAGAAACCATTGGTCANNNNTCCGTAGGGGGN",deleteInsertTest);

        reader.close();
    }

    @Test
    public void testFillingInMissing() {
        VCFFileReader reader = new VCFFileReader(new File("data/simpleGVCFTestFile.g.vcf"), false);

        List<VariantContext> variants = reader.iterator().stream().collect(Collectors.toList());

        GenomeSequence gvcfSeq = GVCFSequence.instance(refSequence,variants,false,"B73");

        String stillMissingTest = gvcfSeq.genotypeAsString(Chromosome.instance(8),56,59);
        String missingRef = "NNNN";

        Assert.assertEquals("Sequence Remaining Missing Test Fails:",missingRef,stillMissingTest);



        GenomeSequence gvcfSeqMissingFilledIn = GVCFSequence.instance(refSequence,variants,true,"B73");

        String missingFilledInTest = gvcfSeqMissingFilledIn.genotypeAsString(Chromosome.instance(8),56,59);
        String actualReference = refSequence.genotypeAsString(Chromosome.instance(8),56,59);

        Assert.assertEquals("Sequence Missing to Reference Test Fails:",actualReference,missingFilledInTest);

        reader.close();
    }

    @Test
    public void testMissingTaxon() {
        VCFFileReader reader = new VCFFileReader(new File("data/simpleGVCFTestFile.g.vcf"), false);

        List<VariantContext> variants = reader.iterator().stream().collect(Collectors.toList());

        //W22 is not in the sequence
        GenomeSequence gvcfSeq = GVCFSequence.instance(refSequence,variants,false,"W22");

        String stillMissingTest = gvcfSeq.genotypeAsString(Chromosome.instance(8),1,70);
        String missingRef = "N";
        int n = 70;
        String sRepeated = IntStream.range(0, n).mapToObj(i -> missingRef).collect(Collectors.joining(""));

        Assert.assertEquals("Sequence Remaining Missing Test Fails:",sRepeated,stillMissingTest);

        GenomeSequence gvcfSeqMissingFilledIn = GVCFSequence.instance(refSequence,variants,true,"W22");

        String missingFilledInTest = gvcfSeqMissingFilledIn.genotypeAsString(Chromosome.instance(8),1,70);
        String actualReference = refSequence.genotypeAsString(Chromosome.instance(8),1,70);

        Assert.assertEquals("Sequence Missing to Reference Test Fails:",actualReference,missingFilledInTest);

        reader.close();
    }

    @Test
    public void testOverlappingRecords() {
        VCFFileReader reader = new VCFFileReader(new File("data/simpleGVCFTestFile.g.vcf"), false);

        List<VariantContext> variants = reader.iterator().stream().collect(Collectors.toList());

        GenomeSequence gvcfSeq = GVCFSequence.instance(refSequence,variants,false,"B73");

        //This one tests if the refrange is on the left
        String leftRefRangeOverlap = gvcfSeq.genotypeAsString(Chromosome.instance(8),200,210);
        String expected = "TTCTTGGATTTGGG";

        Assert.assertEquals("LeftOverlap test fails: ",expected,leftRefRangeOverlap);

        //Test to see if the refrange is on the right, but is too short
        String rightRefRangeOverlapCompleteOverlap = gvcfSeq.genotypeAsString(Chromosome.instance(8),219,220);
        String rightRefExpectedCompleteOverlap = "ACCA";
        Assert.assertEquals("RightCompleteOverlap test fails: ",rightRefExpectedCompleteOverlap,rightRefRangeOverlapCompleteOverlap);

        //Test to see if the refrange is on the right
        String rightRefRangeOverlap = gvcfSeq.genotypeAsString(Chromosome.instance(8),230,235);
        String rightRefExpectedOverlap = "GTCAAAAA";
        Assert.assertEquals("RightOverlap test fails:",rightRefExpectedOverlap,rightRefRangeOverlap);


        String refRangeOverlap = gvcfSeq.genotypeAsString(Chromosome.instance(8),240,250);
        String refRangeOverlapExpected = refSequence.genotypeAsString(Chromosome.instance(8),240,250);
        Assert.assertEquals("RefRange Overlap test fails:",refRangeOverlapExpected,refRangeOverlap);

        reader.close();

    }
}
