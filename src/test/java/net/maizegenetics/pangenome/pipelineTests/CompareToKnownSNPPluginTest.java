package net.maizegenetics.pangenome.pipelineTests;

import net.maizegenetics.analysis.data.SortGenotypeFilePlugin;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.ImportUtils;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple unit test to compare the axiom with a phg produced vcf file.
 * Created by zrm22 on 12/18/17.
 */
public class CompareToKnownSNPPluginTest {

//    String axiomVCFFile = "/Volumes/ZackBackup/Temp/Pangenome/DockerTests/testOutputDockers/FindPath/TUM8Lines_Maize600k_elitelines_AGPv4_Tasselsorted.vcf";
    String axiomVCFFile = "/Volumes/ZackBackup/Temp/Pangenome/Axiom/Axiom12Taxa.vcf";
    String axiomSorted = "/Volumes/ZackBackup/Temp/Pangenome/Axiom/Axiom12Taxa_Sorted.vcf";

    String outputVCF = "/Volumes/ZackBackup/Temp/Pangenome/DockerTests/testOutputDockers/FindPath/testOutput1Renamed.vcf";

    String anchorBedFile = "/Volumes/ZackBackup/Temp/Pangenome/Axiom/hackathon_trimmed_intervals.bed";
    @Test
    public void compareToAxiom() {

        new SortGenotypeFilePlugin(null, false).inputFile(axiomVCFFile).outputFile(axiomSorted).performFunction(null);

//        GenotypeTable axiom = ImportUtils.readFromVCF(axiomVCFFile);
        GenotypeTable axiom = ImportUtils.readFromVCF(axiomSorted);

        GenotypeTable test = ImportUtils.readFromVCF(outputVCF);

        List<Datum> inputs = new ArrayList<>();
        inputs.add(new Datum("axiomGT", axiom,"axiom"));
        inputs.add(new Datum("testGT",test, "test"));

        DataSet dataSet = new DataSet(inputs,null);

        new CompareToKnownSNPPlugin(null, false).anchorRegionBed(anchorBedFile).performFunction(dataSet);
    }

}
