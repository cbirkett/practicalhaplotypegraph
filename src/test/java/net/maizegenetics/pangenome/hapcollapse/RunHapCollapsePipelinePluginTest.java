package net.maizegenetics.pangenome.hapcollapse;

import net.maizegenetics.pangenome.api.HaplotypeGraph;
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin;
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.util.Utils;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Created by zrm22 on 11/15/17.
 */
@Deprecated
public class RunHapCollapsePipelinePluginTest {
    String myMergedOutputDir = "";

    //String inputDB = "/Volumes/ZackBackup/Temp/Pangenome/DockerTests/SmallSeqTestMerge/phgSmallSeq.db";
    String configFile = "configFile.txt";

    String myReference = "/Volumes/ZackBackup/Temp/Pangenome/DockerTests/SmallSeqTestMerge/ref/Ref.fa";

    String myHaplotypeMethod = "GATK_PIPELINE";

    String dbConfigFile = "/Volumes/ZackBackup/Temp/Pangenome/DockerTests/SmallSeqTestMerge/dbConfig.txt";
    String dbConfigFileDocker = "/Volumes/ZackBackup/Temp/Pangenome/DockerTests/configSQLite.txt";

    String myConsensusVCFOutputDir = "/Volumes/ZackBackup/Temp/Pangenome/DockerTests/SmallSeqTestMerge/gvcf/";

    String myConsensusFastaOutputDir = "/Volumes/ZackBackup/Temp/Pangenome/DockerTests/SmallSeqTestMerge/fasta/";

    String myCollapseMethod = "CONSENSUS";

    String myCollapseMethodDetails = "first consensus method";


    /**
     * Test running hap collapse pipeline plugin which will be in a docker eventually
     */
    @Test
    public void testSmallSeqHapCollapse() {
        System.out.println("Loading Graph");

        HaplotypeGraph graph = new HaplotypeGraphBuilderPlugin(null, false).configFile(configFile).methods("GATK_PIPELINE," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP).includeVariantContexts(false).build();

        List<Datum> datumList = new ArrayList<>();
        datumList.add(new Datum("PHG", graph, null));
        //datumList.add(new Datum("Ref", GenomeSequenceBuilder.instance(myReference),null));
        DataSet graphToMerge = new DataSet(datumList, null);
        System.out.println("Number of Nodes In graph:" + graph.numberOfNodes());

        RunHapCollapsePipelinePlugin plugin = new RunHapCollapsePipelinePlugin(null, false)
                .reference(myReference)
                .dbConfigFile(dbConfigFile)
                .consensusVCFOutputDir(myConsensusVCFOutputDir)
                .consensusFastaOutputDir(myConsensusFastaOutputDir)
                .collapseMethod(myCollapseMethod)
                .collapseMethodDetails(myCollapseMethodDetails);


        DataSet outputDS = plugin.processData(graphToMerge);
    }

    /**
     * Test attempting to use the docker image to run the hap collapse pipeline
     */
    @Test
    public void testSmallSeqHapCollapseDocker() {
        //Unit test to run the haplotype collapse functionality from a docker.  Move this code into RunSmallSeqTestsDocker once Lynn's stuff is merged.
        try {
            ProcessBuilder builder = new ProcessBuilder(
                    "docker", "run", "--name", "small_seq_test_container", "--rm",
                    "-v", myReference + ":/tempFileDir/data/reference/Ref.fa",
                    // "-v", inputDB + ":/tempFileDir/data/phgSmallSeq.db",
                    "-v", configFile + ":/tempFileDir/data/configFile",
                    "-v", dbConfigFileDocker + ":/tempFileDir/data/configSQLite.txt",
                    "-t", "bucklerlab_phg:latest",
                    "./CreateConsensi.sh", "/tempFileDir/data/configSQLite.txt", //Config file
                    "Ref.fa", //reference name
                    ".01", //MAX_DISTANCE
                    "phgSmallSeq.db"); //dbName
            String redirectOutput = "/Volumes/ZackBackup/Temp/Pangenome/DockerTests/SmallSeqTestMerge/stdDockerCreateHaplotypes.log";
            String redirectError = "/Volumes/ZackBackup/Temp/Pangenome/DockerTests/SmallSeqTestMerge/errDockerCreateHaplotypes.log";
            builder.redirectOutput(new File(redirectOutput));
            builder.redirectError(new File(redirectError));
            System.out.println("Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            Process process = builder.start();
            process.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testSorghumHapCollapse() {
        String sorghumConfigFile = "/Volumes/ZackBackup2018/Temp/PHG/SarahSorghumTestFiles/config.txt";
        String referenceFile = "/Volumes/ZackBackup2018/Temp/PHG/SarahSorghumTestFiles/Ref/Sbicolor_313_v3.0_numberedChr.fa";
        System.out.println("Loading Graph");

        HaplotypeGraph graph = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(sorghumConfigFile)
                .methods("GATK_PIPELINE," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP)
                .includeVariantContexts(true)
                .hapids(readHapIds())
                .build();

        List<Datum> datumList = new ArrayList<>();
        datumList.add(new Datum("PHG", graph, null));
        //datumList.add(new Datum("Ref", GenomeSequenceBuilder.instance(myReference),null));
        DataSet graphToMerge = new DataSet(datumList, null);
        System.out.println("Number of Nodes In graph:" + graph.numberOfNodes());

        RunHapCollapsePipelinePlugin plugin = new RunHapCollapsePipelinePlugin(null, false)
                .reference(referenceFile)
                .dbConfigFile(sorghumConfigFile)
                .consensusVCFOutputDir("/Volumes/ZackBackup2018/Temp/PHG/SarahSorghumTestFiles/consensusVCFOutput/")
                .consensusFastaOutputDir("/Volumes/ZackBackup2018/Temp/PHG/SarahSorghumTestFiles/consensusVCFOutput/")
                .collapseMethod("CONSENSUS")
                .collapseMethodDetails("first consensus method");


        DataSet outputDS = plugin.processData(graphToMerge);
    }


    private SortedSet<Integer> readHapIds() {
        TreeSet<Integer> hapIds = new TreeSet<>();
        try(BufferedReader reader = Utils.getBufferedReader("/Volumes/ZackBackup2018/Temp/PHG/SarahSorghumTestFiles/First2000Anchors.tsv")) {

            String currentString = "";
            while((currentString = reader.readLine())!=null) {
                hapIds.add(Integer.parseInt(currentString));
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return hapIds;
    }
}
