package net.maizegenetics.pangenome.hapcollapse;

import com.google.common.collect.*;
import htsjdk.variant.variantcontext.*;
import org.junit.Assert;
import net.maizegenetics.dna.map.*;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.NucleotideAlignmentConstants;
import net.maizegenetics.pangenome.api.ReferenceRange;
import net.maizegenetics.taxa.Taxon;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class contains a few unit tests for testing the MergeGVCFUtil functions.  It will make a number of GenotypeTables and VariantContextLists
 * It will attempt to merge the VariantContexts into a GenotypeTable
 * It will also attempt to reintroduce the indels.
 */
public class MergeGVCFUtilsTest {

    Table<String,Integer,int[]> knownDepthTable = HashBasedTable.create();

    @Test
    public void convertListOfVariantContextsToGenotypeTable() {
        //Attempt to create a genotypeTable like the following:
        /*
        B73     AAATTGCGTATTACTGATG
        W22     AATTTCCG--TTACT+ATG
        Mo17    AAATTTCGTTT--CTGATG
        */

        GenomeSequence genomeSequence = GenomeSequenceBuilder.instance(Chromosome.instance("1"),"AAATTGCGTATTACTGATG");

        //Build B73 Variants
        List<VariantContext> b73Variants = processB73(genomeSequence);
        List<VariantContext> w22Variants = processW22(genomeSequence);
        List<VariantContext> mo17Variants = processMo17(genomeSequence);

        System.out.println("**********************\nB73\n**********************");
        System.out.println(b73Variants.stream().map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

        System.out.println("**********************\nW22\n**********************");
        System.out.println(w22Variants.stream().map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

        System.out.println("**********************\nMo17\n**********************");
        System.out.println(mo17Variants.stream().map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

        Multimap<Taxon,VariantContext> variantMap = ArrayListMultimap.create();
        variantMap.putAll(new Taxon.Builder("B73").build(),b73Variants);
        variantMap.putAll(new Taxon.Builder("W22").build(),w22Variants);
        variantMap.putAll(new Taxon.Builder("Mo17").build(),mo17Variants);

        ReferenceRange referenceRange = new ReferenceRange("B73Ref",Chromosome.instance("1"),1,19,100);

        GenotypeTable gt = MergeGVCFUtils.createGenotypeTableFromGVCFs(referenceRange,variantMap,genomeSequence);

        verifyPositions(gt,referenceRange);

        //Make sure we have the taxon in the genotype table that we think should be there.
        Assert.assertTrue("Merge VariantTest: Missing B73.",gt.taxa().contains(new Taxon.Builder("B73_Haplotype_Caller").build()));
        Assert.assertTrue("Merge VariantTest: Missing W22.",gt.taxa().contains(new Taxon.Builder("W22_Haplotype_Caller").build()));
        Assert.assertTrue("Merge VariantTest: Missing Mo17.",gt.taxa().contains(new Taxon.Builder("Mo17_Haplotype_Caller").build()));

        //Set up expected genotypes
        Map<Taxon,String> knownGenotypes = new HashMap<>();
        knownGenotypes.put(new Taxon.Builder("B73_Haplotype_Caller").build(), "AAATTGCGTATTACTGATG" );
        knownGenotypes.put(new Taxon.Builder("W22_Haplotype_Caller").build(), "AATTTCCG--TTACT+ATG" );
        knownGenotypes.put(new Taxon.Builder("Mo17_Haplotype_Caller").build(), "AAATTTCGTTT--CTGATG" );

        for(int taxonIndex = 0; taxonIndex < gt.numberOfTaxa(); taxonIndex++) {
            String genotypeString = "";
            for(int site = 0; site < gt.numberOfSites(); site++) {
                genotypeString+=gt.genotypeAsString(taxonIndex,site);
            }
            Assert.assertEquals("Genotypes Do not match:",knownGenotypes.get(gt.taxa().get(taxonIndex)),genotypeString);
        }

        //Check to make sure we put in the depths correctly
        for(int taxonIndex = 0; taxonIndex < gt.numberOfTaxa(); taxonIndex++) {
            for(int site = 0; site < gt.numberOfSites(); site++) {
                Assert.assertArrayEquals("Depths do not match:"+gt.taxaName(taxonIndex)+", Pos:"+gt.positions().get(site).getPosition(),knownDepthTable.get(gt.taxaName(taxonIndex),gt.positions().get(site).getPosition()),gt.depthForAlleles(taxonIndex,site));
            }
        }



        //Remove the indels and make sure it works
        GenotypeTable indelFiltered = MergeGVCFUtils.removeIndels(gt);

        //Set up the expected indel genotypes
        Map<Taxon,String> knownGenotypesIndels = new HashMap<>();
        knownGenotypesIndels.put(new Taxon.Builder("B73_Haplotype_Caller").build(), "AAATTGCGTCTATG" );
        knownGenotypesIndels.put(new Taxon.Builder("W22_Haplotype_Caller").build(), "AATTTCCGTCTATG" );
        knownGenotypesIndels.put(new Taxon.Builder("Mo17_Haplotype_Caller").build(),"AAATTTCGTCTATG" );

        for(int taxonIndex = 0; taxonIndex < indelFiltered.numberOfTaxa(); taxonIndex++) {
            String genotypeString = "";
            for(int site = 0; site < indelFiltered.numberOfSites(); site++) {
                genotypeString+=indelFiltered.genotypeAsString(taxonIndex,site);
            }
            Assert.assertEquals("Genotypes Do not match:",knownGenotypesIndels.get(indelFiltered.taxa().get(taxonIndex)),genotypeString);
        }
        //Check to make sure we put in the depths correctly
//        for(int taxonIndex = 0; taxonIndex < indelFiltered.numberOfTaxa(); taxonIndex++) {
//            for(int site = 0; site < indelFiltered.numberOfSites(); site++) {
//                System.out.println("*************");
//                System.out.println(indelFiltered.taxaName(taxonIndex));
//                System.out.println(indelFiltered.positions().get(site).getPosition());
//                System.out.println(knownDepthTable.get(indelFiltered.taxaName(taxonIndex),indelFiltered.positions().get(site).getPosition()));
//                System.out.println(indelFiltered.depthForAlleles(taxonIndex,site));
//                Assert.assertArrayEquals("Depths do not match:"+indelFiltered.taxaName(taxonIndex)+", Pos:"+indelFiltered.positions().get(site).getPosition(),knownDepthTable.get(indelFiltered.taxaName(taxonIndex),indelFiltered.positions().get(site).getPosition()),indelFiltered.depthForAlleles(taxonIndex,site));
//            }
//        }


        //Attempt to fill in the indels
        List<VariantContext> b73MinusIndels = processB73NoIndels(genomeSequence);
        List<VariantContext> w22MinusIndels = processW22NoIndels(genomeSequence);
        List<VariantContext> mo17MinusIndels = processMo17NoIndels(genomeSequence);

        List<List<VariantContext>> indelsFilledIn = FillIndelsIntoConsensus.addInIndels(referenceRange,genomeSequence,Arrays.asList(b73MinusIndels,w22MinusIndels,mo17MinusIndels),Arrays.asList(b73Variants,w22Variants,mo17Variants),0, FillIndelsIntoConsensus.INDEL_MERGE_RULE.mostCommonHaplotype);


        for(List<VariantContext> singleConsensus : indelsFilledIn) {
            System.out.println(singleConsensus.get(0).getGenotype(0).getSampleName());
            for(VariantContext vc : singleConsensus) {
                System.out.println(vc);
            }
        }


        List<VariantContext> b73PlusMo17MergedMinusIndels = processB73PlusMo17NoIndels(genomeSequence);

        List<List<VariantContext>> indelsFilledInTestNs=FillIndelsIntoConsensus.addInIndels(referenceRange,genomeSequence,Arrays.asList(b73PlusMo17MergedMinusIndels),Arrays.asList(b73Variants,w22Variants,mo17Variants),0, FillIndelsIntoConsensus.INDEL_MERGE_RULE.setToN);
        System.out.println("********************************\nFilling in merged indels with N");
        for(List<VariantContext> singleConsensus : indelsFilledInTestNs) {
            System.out.println(singleConsensus.get(0).getGenotype(0).getSampleName());
            for(VariantContext vc : singleConsensus) {
                System.out.println(vc);
            }
        }

        List<List<VariantContext>> indelsFilledInTestCommonHaps=FillIndelsIntoConsensus.addInIndels(referenceRange,genomeSequence,Arrays.asList(b73PlusMo17MergedMinusIndels),Arrays.asList(b73Variants,w22Variants,mo17Variants),0, FillIndelsIntoConsensus.INDEL_MERGE_RULE.mostCommonHaplotype);
        System.out.println("********************************\nFilling in merged indels with most common haps");
        for(List<VariantContext> singleConsensus : indelsFilledInTestCommonHaps) {
            System.out.println(singleConsensus.get(0).getGenotype(0).getSampleName());
            for(VariantContext vc : singleConsensus) {
                System.out.println(vc);
            }
        }

        List<List<VariantContext>> indelsFilledInTestCommonHapsAsHet=FillIndelsIntoConsensus.addInIndels(referenceRange,genomeSequence,Arrays.asList(b73PlusMo17MergedMinusIndels),Arrays.asList(b73Variants,w22Variants,mo17Variants),0, FillIndelsIntoConsensus.INDEL_MERGE_RULE.mostCommonHaplotypesAsHet);
        System.out.println("********************************\nFilling in merged indels with most common haps as het");
        for(List<VariantContext> singleConsensus : indelsFilledInTestCommonHapsAsHet) {
            System.out.println(singleConsensus.get(0).getGenotype(0).getSampleName());
            for(VariantContext vc : singleConsensus) {
                System.out.println(vc);
            }
        }

        List<List<VariantContext>> indelsFilledInTestAsRef=FillIndelsIntoConsensus.addInIndels(referenceRange,genomeSequence,Arrays.asList(b73PlusMo17MergedMinusIndels),Arrays.asList(b73Variants,w22Variants,mo17Variants),0, FillIndelsIntoConsensus.INDEL_MERGE_RULE.reference);
        System.out.println("********************************\nFilling in merged indels as reference");
        for(List<VariantContext> singleConsensus : indelsFilledInTestAsRef) {
            System.out.println(singleConsensus.get(0).getGenotype(0).getSampleName());
            for(VariantContext vc : singleConsensus) {
                System.out.println(vc);
            }
        }


        //TODO Convert the GenotypeTable to List<VariantContext> and make sure it matches what is expected

    }


    @Test
    public void testEmptyVariantContext() {
        GenomeSequence genomeSequence = GenomeSequenceBuilder.instance(Chromosome.instance("1"),"AAATTGCGTATTACTGATG");

        //Build B73 Variants
        List<VariantContext> b73Variants = processB73(genomeSequence);
        List<VariantContext> w22Variants = processW22(genomeSequence);
        List<VariantContext> mo17Variants = processMo17(genomeSequence);
        List<VariantContext> cml247Variants = new ArrayList<>();

        Multimap<Taxon,VariantContext> variantMap = ArrayListMultimap.create();
        variantMap.putAll(new Taxon.Builder("B73").build(),b73Variants);
        variantMap.putAll(new Taxon.Builder("W22").build(),w22Variants);
        variantMap.putAll(new Taxon.Builder("Mo17").build(),mo17Variants);
        variantMap.putAll(new Taxon.Builder("CML247").build(),cml247Variants);

        ReferenceRange referenceRange = new ReferenceRange("B73Ref",Chromosome.instance("1"),1,19,100);

        GenotypeTable gt = MergeGVCFUtils.createGenotypeTableFromGVCFs(referenceRange,variantMap,genomeSequence);

        verifyPositions(gt,referenceRange);

        //Make sure we have the taxon in the genotype table that we think should be there.
        Assert.assertTrue("Merge VariantTest: Missing B73.",gt.taxa().contains(new Taxon.Builder("B73_Haplotype_Caller").build()));
        Assert.assertTrue("Merge VariantTest: Missing W22.",gt.taxa().contains(new Taxon.Builder("W22_Haplotype_Caller").build()));
        Assert.assertTrue("Merge VariantTest: Missing Mo17.",gt.taxa().contains(new Taxon.Builder("Mo17_Haplotype_Caller").build()));
//        Assert.assertTrue("Merge VariantTest: Missing CML247.",gt.taxa().contains(new Taxon.Builder("CML247_Haplotype_Caller").build()));

        //Set up expected genotypes
        Map<Taxon,String> knownGenotypes = new HashMap<>();
        knownGenotypes.put(new Taxon.Builder("B73_Haplotype_Caller").build(), "AAATTGCGTATTACTGATG" );
        knownGenotypes.put(new Taxon.Builder("W22_Haplotype_Caller").build(), "AATTTCCG--TTACT+ATG" );
        knownGenotypes.put(new Taxon.Builder("Mo17_Haplotype_Caller").build(), "AAATTTCGTTT--CTGATG" );
        knownGenotypes.put(new Taxon.Builder("CML247_Haplotype_Caller").build(), "NNNNNNNNNNNNNNNNNNN" );

        for(int taxonIndex = 0; taxonIndex < gt.numberOfTaxa(); taxonIndex++) {
            String genotypeString = "";
            for(int site = 0; site < gt.numberOfSites(); site++) {
                genotypeString+=gt.genotypeAsString(taxonIndex,site);
            }
            Assert.assertEquals("Genotypes Do not match:"+knownGenotypes.get(gt.taxa().get(taxonIndex)),knownGenotypes.get(gt.taxa().get(taxonIndex)),genotypeString);
        }


        //Remove the indels and make sure it works
        GenotypeTable indelFiltered = MergeGVCFUtils.removeIndels(gt);

        //Set up the expected indel genotypes
        Map<Taxon,String> knownGenotypesIndels = new HashMap<>();
        knownGenotypesIndels.put(new Taxon.Builder("B73_Haplotype_Caller").build(), "AAATTGCGTCTATG" );
        knownGenotypesIndels.put(new Taxon.Builder("W22_Haplotype_Caller").build(), "AATTTCCGTCTATG" );
        knownGenotypesIndels.put(new Taxon.Builder("Mo17_Haplotype_Caller").build(),"AAATTTCGTCTATG" );
        knownGenotypesIndels.put(new Taxon.Builder("CML247_Haplotype_Caller").build(),"NNNNNNNNNNNNNN" );

        for(int taxonIndex = 0; taxonIndex < indelFiltered.numberOfTaxa(); taxonIndex++) {
            String genotypeString = "";
            for(int site = 0; site < indelFiltered.numberOfSites(); site++) {
                genotypeString+=indelFiltered.genotypeAsString(taxonIndex,site);
            }
            Assert.assertEquals("Indel Removed Genotypes Do not match:"+gt.taxa().get(taxonIndex),knownGenotypesIndels.get(indelFiltered.taxa().get(taxonIndex)),genotypeString);
        }

    }

    private void verifyPositions(GenotypeTable gt, ReferenceRange referenceRange) {
        PositionList positionList = gt.positions();
        Range<Position> ref =Range.closed(Position.of(referenceRange.chromosome(),referenceRange.start()),
                                            Position.of(referenceRange.chromosome(),referenceRange.end()));

        for(Position position: positionList) {
            Assert.assertTrue("Position is not in the refRange: "+position.toString(),ref.contains(position));
        }
    }

    private List<VariantContext> processB73(GenomeSequence refSequence) {
        // Split it up into 2 reference blocks
        List<VariantContext> variants = new ArrayList<>();
        Allele currentAllele = Allele.create("A",true);
        Genotype currentGeno = new GenotypeBuilder().name("B73").alleles(Arrays.asList(currentAllele)).DP(10).make();
        variants.add(new VariantContextBuilder()
                        .chr("1")
                        .start(1l)
                        .stop(6l)
                        .attribute("END",6)
                        .alleles(Arrays.asList(currentAllele))
                        .genotypes(currentGeno)
                        .make());

        for(int i = 1; i<=6; i++) {
            int[] depths = new int[6];
            depths[refSequence.genotype(Chromosome.instance("1"),i)] = 10;
            knownDepthTable.put("B73_Haplotype_Caller",i,depths);
        }

        currentAllele = Allele.create("C",true);
        currentGeno = new GenotypeBuilder().name("B73").alleles(Arrays.asList(currentAllele)).DP(5).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(7l)
                .stop(19l)
                .attribute("END",19)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        for(int i = 7; i<=19; i++) {
            int[] depths = new int[6];
            depths[refSequence.genotype(Chromosome.instance("1"),i)] = 5;
            knownDepthTable.put("B73_Haplotype_Caller",i,depths);
        }

        return variants;
    }

    private List<VariantContext> processB73NoIndels(GenomeSequence refSequence) {
        // Split it up into 2 reference blocks
        List<VariantContext> variants = new ArrayList<>();
        Allele currentAllele = Allele.create("A",true);
        Genotype currentGeno = new GenotypeBuilder().name("B73").alleles(Arrays.asList(currentAllele)).DP(10).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(1l)
                .stop(6l)
                .attribute("END",6)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());


        currentAllele = Allele.create("C",true);
        currentGeno = new GenotypeBuilder().name("B73").alleles(Arrays.asList(currentAllele)).DP(5).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(7l)
                .stop(7l)
                .attribute("END",7)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        currentAllele = Allele.create("G",true);
        currentGeno = new GenotypeBuilder().name("B73").alleles(Arrays.asList(currentAllele)).DP(5).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(8l)
                .stop(8l)
                .attribute("END",8)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());


        currentAllele = Allele.create("T",true);
        currentGeno = new GenotypeBuilder().name("B73").alleles(Arrays.asList(currentAllele)).DP(5).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(11l)
                .stop(11l)
                .attribute("END",11)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());


        currentAllele = Allele.create("C",true);
        currentGeno = new GenotypeBuilder().name("B73").alleles(Arrays.asList(currentAllele)).DP(5).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(14l)
                .stop(14l)
                .attribute("END",14)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        currentAllele = Allele.create("T",true);
        currentGeno = new GenotypeBuilder().name("B73").alleles(Arrays.asList(currentAllele)).DP(5).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(15l)
                .stop(15l)
                .attribute("END",15)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());



        currentAllele = Allele.create("A",true);
        currentGeno = new GenotypeBuilder().name("B73").alleles(Arrays.asList(currentAllele)).DP(5).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(17l)
                .stop(19l)
                .attribute("END",19)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        return variants;
    }

    private List<VariantContext> processW22(GenomeSequence refSequence) {
        String taxon = "W22";
        List<VariantContext> variants = new ArrayList<>();
        Allele currentAllele = Allele.create("A",true);
        Genotype currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(5).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(1l)
                .stop(2l)
                .attribute("END",2)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        for(int i = 1; i<=2; i++) {
            int[] depths = new int[6];
            depths[refSequence.genotype(Chromosome.instance("1"),i)] = 5;
            knownDepthTable.put("W22_Haplotype_Caller",i,depths);
        }

        currentAllele = Allele.create("T",false);
        Allele refAllele = Allele.create("A",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(6).AD(new int[]{0,6}).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(3l)
                .stop(3l)
                .alleles(Arrays.asList(refAllele,currentAllele))
                .genotypes(currentGeno)
                .make());

        int[] snpDepths = new int[6];
        snpDepths[NucleotideAlignmentConstants.getNucleotideAlleleByte("T")] = 6;
        knownDepthTable.put("W22_Haplotype_Caller",3,snpDepths);


        currentAllele = Allele.create("T",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(10).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(4l)
                .stop(5l)
                .attribute("END",5)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());
        for(int i = 4; i<=5; i++) {
            int[] depths = new int[6];
            depths[refSequence.genotype(Chromosome.instance("1"),i)] = 10;
            knownDepthTable.put("W22_Haplotype_Caller",i,depths);
        }

        refAllele = Allele.create("G",true);
        currentAllele = Allele.create("C",false);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(4).AD(new int[]{0,4}).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(6l)
                .stop(6l)
                .alleles(Arrays.asList(refAllele,currentAllele))
                .genotypes(currentGeno)
                .make());

        snpDepths = new int[6];
        snpDepths[NucleotideAlignmentConstants.getNucleotideAlleleByte("C")] = 4;
        knownDepthTable.put("W22_Haplotype_Caller",6,snpDepths);

        currentAllele = Allele.create("C",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(7).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(7l)
                .stop(7l)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        for(int i = 7; i<=7; i++) {
            int[] depths = new int[6];
            depths[refSequence.genotype(Chromosome.instance("1"),i)] = 7;
            knownDepthTable.put("W22_Haplotype_Caller",i,depths);
        }


        refAllele = Allele.create("GTA",true);
        currentAllele = Allele.create("G",false);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(1).AD(new int[]{0,1}).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(8l)
                .stop(10l)
                .alleles(Arrays.asList(refAllele,currentAllele))
                .genotypes(currentGeno)
                .make());

        snpDepths = new int[6];
        snpDepths[NucleotideAlignmentConstants.getNucleotideAlleleByte("G")] = 1;
        knownDepthTable.put("W22_Haplotype_Caller",8,snpDepths);

        snpDepths = new int[6];
        snpDepths[NucleotideAlignmentConstants.getNucleotideAlleleByte("-")] = 1;
        knownDepthTable.put("W22_Haplotype_Caller",9,snpDepths);

        snpDepths = new int[6];
        snpDepths[NucleotideAlignmentConstants.getNucleotideAlleleByte("-")] = 1;
        knownDepthTable.put("W22_Haplotype_Caller",10,snpDepths);

        currentAllele = Allele.create("T",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(4).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(11l)
                .stop(15l)
                .attribute("END",15)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        for(int i = 11; i<=15; i++) {
            int[] depths = new int[6];
            depths[refSequence.genotype(Chromosome.instance("1"),i)] = 4;
            knownDepthTable.put("W22_Haplotype_Caller",i,depths);
        }

        refAllele = Allele.create("G",true);
        currentAllele = Allele.create("GGGGG",false);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(5).AD(new int[]{0,5}).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(16l)
                .stop(16l)
                .alleles(Arrays.asList(refAllele,currentAllele))
                .genotypes(currentGeno)
                .make());

        snpDepths = new int[6];
        snpDepths[NucleotideAlignmentConstants.getNucleotideAlleleByte("+")] = 5;
        knownDepthTable.put("W22_Haplotype_Caller",16,snpDepths);

        currentAllele = Allele.create("A",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(6).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(17l)
                .stop(19l)
                .attribute("END",19)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        for(int i = 17; i<=19; i++) {
            int[] depths = new int[6];
            depths[refSequence.genotype(Chromosome.instance("1"),i)] = 6;
            knownDepthTable.put("W22_Haplotype_Caller",i,depths);
        }

        return variants;
    }

    private List<VariantContext> processW22NoIndels(GenomeSequence refSequence) {
        String taxon = "W22";
        List<VariantContext> variants = new ArrayList<>();
        Allele currentAllele = Allele.create("A",true);
        Genotype currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(5).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(1l)
                .stop(2l)
                .attribute("END",2)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());


        currentAllele = Allele.create("T",false);
        Allele refAllele = Allele.create("A",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(6).AD(new int[]{0,6}).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(3l)
                .stop(3l)
                .alleles(Arrays.asList(refAllele,currentAllele))
                .genotypes(currentGeno)
                .make());



        currentAllele = Allele.create("T",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(10).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(4l)
                .stop(5l)
                .attribute("END",5)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());


        refAllele = Allele.create("G",true);
        currentAllele = Allele.create("C",false);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(4).AD(new int[]{0,4}).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(6l)
                .stop(6l)
                .alleles(Arrays.asList(refAllele,currentAllele))
                .genotypes(currentGeno)
                .make());


        currentAllele = Allele.create("C",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(7).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(7l)
                .stop(7l)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());




        refAllele = Allele.create("G",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(refAllele)).DP(1).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(8l)
                .stop(8l)
                .alleles(Arrays.asList(refAllele))
                .genotypes(currentGeno)
                .make());



        currentAllele = Allele.create("T",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(4).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(11l)
                .stop(11l)
                .attribute("END",11)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        currentAllele = Allele.create("C",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(4).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(14l)
                .stop(14l)
                .attribute("END",14)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        currentAllele = Allele.create("T",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(4).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(15l)
                .stop(15l)
                .attribute("END",15)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        currentAllele = Allele.create("A",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(6).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(17l)
                .stop(19l)
                .attribute("END",19)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());



        return variants;
    }

    private List<VariantContext> processMo17(GenomeSequence refSequence) {
        String taxon = "Mo17";

        List<VariantContext> variants = new ArrayList<>();
        Allele currentAllele = Allele.create("A",true);
        Genotype currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(5).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(1l)
                .stop(5l)
                .attribute("END",5)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        for(int i = 1; i<=5; i++) {
            int[] depths = new int[6];
            depths[refSequence.genotype(Chromosome.instance("1"),i)] = 5;
            knownDepthTable.put("Mo17_Haplotype_Caller",i,depths);
        }

        Allele refAllele = Allele.create("G",true);
        currentAllele = Allele.create("T",false);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(5).AD(new int[]{0,5}).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(6l)
                .stop(6l)
                .alleles(Arrays.asList(refAllele,currentAllele))
                .genotypes(currentGeno)
                .make());

        int[] snpDepths = new int[6];
        snpDepths[NucleotideAlignmentConstants.getNucleotideAlleleByte("T")] = 5;
        knownDepthTable.put("Mo17_Haplotype_Caller",6,snpDepths);

        currentAllele = Allele.create("C",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(3).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(7l)
                .stop(9l)
                .attribute("END",9)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        for(int i = 7; i<=9; i++) {
            int[] depths = new int[6];
            depths[refSequence.genotype(Chromosome.instance("1"),i)] = 3;
            knownDepthTable.put("Mo17_Haplotype_Caller",i,depths);
        }

        refAllele = Allele.create("A",true);
        currentAllele = Allele.create("T",false);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(4).AD(new int[]{0,4}).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(10l)
                .stop(10l)
                .alleles(Arrays.asList(refAllele,currentAllele))
                .genotypes(currentGeno)
                .make());

        snpDepths = new int[6];
        snpDepths[NucleotideAlignmentConstants.getNucleotideAlleleByte("T")] = 4;
        knownDepthTable.put("Mo17_Haplotype_Caller",10,snpDepths);


        refAllele = Allele.create("TTA",true);
        currentAllele = Allele.create("T",false);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(6).AD(new int[]{0,6}).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(11l)
                .stop(13l)
                .alleles(Arrays.asList(refAllele,currentAllele))
                .genotypes(currentGeno)
                .make());

        snpDepths = new int[6];
        snpDepths[NucleotideAlignmentConstants.getNucleotideAlleleByte("T")] = 6;
        knownDepthTable.put("Mo17_Haplotype_Caller",11,snpDepths);

        snpDepths = new int[6];
        snpDepths[NucleotideAlignmentConstants.getNucleotideAlleleByte("-")] = 6;
        knownDepthTable.put("Mo17_Haplotype_Caller",12,snpDepths);

        snpDepths = new int[6];
        snpDepths[NucleotideAlignmentConstants.getNucleotideAlleleByte("-")] = 6;
        knownDepthTable.put("Mo17_Haplotype_Caller",13,snpDepths);

        currentAllele = Allele.create("C",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(7).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(14l)
                .stop(19l)
                .attribute("END",19)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        for(int i = 14; i<=19; i++) {
            int[] depths = new int[6];
            depths[refSequence.genotype(Chromosome.instance("1"),i)] = 7;
            knownDepthTable.put("Mo17_Haplotype_Caller",i,depths);
        }

        return variants;
    }

    private List<VariantContext> processMo17NoIndels(GenomeSequence refSequence) {
        String taxon = "Mo17";

        List<VariantContext> variants = new ArrayList<>();
        Allele currentAllele = Allele.create("A",true);
        Genotype currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(5).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(1l)
                .stop(5l)
                .attribute("END",5)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());


        Allele refAllele = Allele.create("G",true);
        currentAllele = Allele.create("T",false);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(5).AD(new int[]{0,5}).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(6l)
                .stop(6l)
                .alleles(Arrays.asList(refAllele,currentAllele))
                .genotypes(currentGeno)
                .make());


        currentAllele = Allele.create("C",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(3).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(7l)
                .stop(7l)
                .attribute("END",7)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        currentAllele = Allele.create("G",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(3).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(8l)
                .stop(8l)
                .attribute("END",8)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());


        refAllele = Allele.create("T",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(refAllele)).DP(6).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(11l)
                .stop(11l)
                .alleles(Arrays.asList(refAllele))
                .genotypes(currentGeno)
                .make());


        currentAllele = Allele.create("C",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(7).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(14l)
                .stop(14l)
                .attribute("END",14)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());


        currentAllele = Allele.create("T",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(7).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(15l)
                .stop(15l)
                .attribute("END",15)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        currentAllele = Allele.create("A",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(7).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(17l)
                .stop(19l)
                .attribute("END",19)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());


        return variants;
    }



    private List<VariantContext> processB73PlusMo17NoIndels(GenomeSequence genomeSequence) {
        String taxon = "B73:Mo17";

        List<VariantContext> variants = new ArrayList<>();
        Allele currentAllele = Allele.create("A",true);
        Genotype currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(5).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(1l)
                .stop(5l)
                .attribute("END",5)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());


        Allele refAllele = Allele.create("G",true);
        currentAllele = Allele.create("T",false);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(refAllele,currentAllele)).DP(5).AD(new int[]{0,5}).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(6l)
                .stop(6l)
                .alleles(Arrays.asList(refAllele,currentAllele))
                .genotypes(currentGeno)
                .make());


        currentAllele = Allele.create("C",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(3).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(7l)
                .stop(7l)
                .attribute("END",7)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        currentAllele = Allele.create("G",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(3).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(8l)
                .stop(8l)
                .attribute("END",8)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());


        refAllele = Allele.create("T",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(refAllele)).DP(6).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(11l)
                .stop(11l)
                .alleles(Arrays.asList(refAllele))
                .genotypes(currentGeno)
                .make());


        currentAllele = Allele.create("C",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(7).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(14l)
                .stop(14l)
                .attribute("END",14)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());


        currentAllele = Allele.create("T",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(7).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(15l)
                .stop(15l)
                .attribute("END",15)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());

        currentAllele = Allele.create("A",true);
        currentGeno = new GenotypeBuilder().name(taxon).alleles(Arrays.asList(currentAllele)).DP(7).make();
        variants.add(new VariantContextBuilder()
                .chr("1")
                .start(17l)
                .stop(19l)
                .attribute("END",19)
                .alleles(Arrays.asList(currentAllele))
                .genotypes(currentGeno)
                .make());


        return variants;
    }
}
