/**
 * 
 */
package net.maizegenetics.pangenome.hapcollapse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import org.junit.Test;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.dna.map.PositionList;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTableBuilder;
import net.maizegenetics.dna.snp.genotypecall.GenotypeCallTableBuilder;
import net.maizegenetics.pangenome.api.HaplotypeGraph;
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin;
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.TaxaListBuilder;

/**
 * Class to test various pieces of  RunHapConsensusPipelinePlugin and
 * methods called from RunHapConsensusPipelinePlugin
 * 
 * @author lcj34
 *
 */
public class RunHapConsensusPipelinePluginTest {

    @Test
    public void testConsensusIndelHandling() {
        // This test makes use of the smallSeq tests.
        // Create a small seq db by running RunSmallSeqTestsDocker up through the creation of haplotypes,
        // Stop before consensus is run.  
        // Save this db.  it provides  a db with variants and alleles tables populated.
        // Run the RUnHapConsensusPipelinePlugin ouside a docker here, using debug mode to
        // check the values of clustering and choosing indels.
        //
        // If you want indels, run createGenomes of small seq with refGeneInsert(0.2) (or
        // some other non-zero value).  You can also set refInterGeneDelete(0.2) to get deletions.
        //  inserts only go into genes, deletions are only coded into inter-genes.
        
        String configFile = "/Users/lcj34/notes_files/phg_2018/dbSchema_updates/phg205_variants/debug/configSQLite.txt";
        
        String REFERENCE_FILE=SmallSeqPaths.refGenomePath;
        String Haplotype_METHOD = "GATK_PIPELINE," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP;
        String CLUSTER_METHOD = "upgma";
        String COLLAPSE_METHOD = "CONSENSUS";
        String INDEL_MERGE_RULE = "setToN"; // not used
        String collapseDetails = "collapse something from variant list";
                
        String CONSENSUS_VCF_OUTPUT_DIR="/Users/lcj34/notes_files/phg_2018/dbSchema_updates/phg205_variants/debug/consensusVCFoutputDir/";
        String CONSENSUS_FASTA_OUTPUT_DIR="/Users/lcj34/notes_files/phg_2018/dbSchema_updates/phg205_variants/debug/consensusFastaOutputDir/";

        
        HaplotypeGraph graph = new HaplotypeGraphBuilderPlugin(null, false).configFile(configFile).methods(Haplotype_METHOD).includeVariantContexts(true).build();

        List<Datum> datumList = new ArrayList<>();
        datumList.add(new Datum("PHG", graph, null));
        //datumList.add(new Datum("Ref", GenomeSequenceBuilder.instance(myReference),null));
        DataSet graphToMerge = new DataSet(datumList, null);
        System.out.println("Number of Nodes In graph:" + graph.numberOfNodes());

        // This runs the pipeline and allows for debugging breakpoints.
         DataSet ds1 = new RunHapConsensusPipelinePlugin(null, false)
                .reference(REFERENCE_FILE)
                .dbConfigFile(configFile)
                .collapseMethod(COLLAPSE_METHOD)
                .collapseMethodDetails(collapseDetails)
                .performFunction(graphToMerge);
 
    }
    
    @Test
    public void testGetLongFromRefAltData() {
        // This test verifies the correct Long is created from ConsensusProcessingUtils.getLongFromRefAltData()
        
        String chromosome = "8";
        int position = 8801;
        int refLen = 7;
        int refDepth = 1;
        int altDepth = 0;
        // Create the RefAltData object, encode to a long
        RefAltData refRad =  new RefAltData(-1,chromosome,position, refLen, refDepth, altDepth);
        long refLong =  ConsensusProcessingUtils.getLongFromRefAltData(refRad);
        
        System.out.println("Testing Referenece RefAltData to long:");
        // Pull out the specific parts, verify they are what is expected
        assertEquals(1,(refLong >>> 63));
        refLong  = refLong &  ~(1L << 63); // this wipes out upper 32
        System.out.println("refLong after resetting high bit set: " + refLong + ", as hex " + String.format("0x%08X", refLong));
        
        int myPosition = (int)(refLong & 0xFFFFFFFF);
        assertEquals(position,myPosition);
        refLong = refLong >> 32;
        System.out.println("\nrefLong after shift past position " + refLong + ", as hex " + String.format("0x%08X", refLong));
        
        int myDepth = (int)(refLong & 0xFF); 
        assertEquals(refDepth,myDepth);
        refLong = refLong >>> 8; 
        System.out.println("refLong after shift past depth " + refLong + ", as hex " + String.format("0x%08X", refLong));
        
        int myLength = (int)refLong; // it's been shifted enough
        assertEquals(refLen,myLength);
        
        // now test a variant record
        System.out.println("\nTesting variant RefALtData to long:");
        int altLen = 40;
        refDepth = 6;
        altDepth = 35;
        int other = 0;
        int isIndel = 1;
        int vmID = 345;
        RefAltData altRad =  new RefAltData(vmID,chromosome,position, altLen, refDepth, altDepth);
        long altLong =  ConsensusProcessingUtils.getLongFromRefAltData(altRad);
        assertEquals(0,(refLong >>> 63));
        System.out.println("\nIS NOT a reference!!");
                
        int myOther = (int) (altLong & 0xFF);
        System.out.println("Value of myOther " + myOther);
        assertEquals(other,myOther);        
        altLong = altLong >> 8;
                
        byte indel = (byte) (altLong & 0xFF);
        System.out.println("Value of isIndel: " + indel);
        assertEquals(indel,isIndel);
        altLong = altLong >>8;
        
        int myAltDepth = (int) (altLong & 0xFF);
        System.out.println("Value of myAltDepth " + myAltDepth);
        assertEquals(altDepth,myAltDepth);
        
        altLong = altLong >> 8;
        int myRefDepth = (int) (altLong & 0xFF);
        assertEquals(refDepth,myRefDepth);
        
        altLong = altLong >> 8;
        int myVariantID = (int) altLong;
        assertEquals(vmID,myVariantID);

        System.out.println("Values expected:  quality=0, altdepth=35, refDepth=6, vmID=345");
        System.out.println("Values actual:    quality=" + myOther + ", altDepth=" + myAltDepth 
                + ", refDepth=" + myRefDepth + ", vmID=" + myVariantID);       
    }
    
    @Test
    public void testSplitConsensusTaxa() {
        
        String taxonGrouped = "LineA1_0:LineA_0:LineB_0";
        TaxaListBuilder taxaBuilder = new TaxaListBuilder();               
        
        taxaBuilder.add(taxonGrouped);
                    
        TaxaList taxaList = taxaBuilder.build();        
        
        TaxaList splitList = ConsensusProcessingUtils.splitConsensusTaxa(taxaList);
        assertEquals(3,splitList.size());
        
        assertEquals("LineA1",splitList.get(0).getName());
        
    }
    
    @Test
    public void testChooseVarIDForSNPPosition() {
        // Tests ConsensusProcessingUtils:chooseVarIDForSNPPOsitionFromGenotypeTable returns correct values 
        List<Position> varPosList = new ArrayList<Position>();
        Chromosome chrom = Chromosome.instance("1");
        for (int idx = 1; idx < 11; idx++) {
            Position pos = Position.of(chrom, idx);
            varPosList.add(pos);
        }
        PositionList variantPositionList = varPosList.stream().collect(PositionList.collectReorder());
        
        // this is position to Map<call,variantId> - needed for call to chooseVarIdForSNPPositionFromGenotypeTable() 
        Map<Integer, BiMap<Integer,Integer>> posCallVarIdMap = new HashMap<Integer,BiMap<Integer,Integer>>();
                
        // This is to set a variant ID for each position
        Map<Position,Integer> posToVaridMap = new HashMap<>();
        posToVaridMap.put(Position.of(chrom,2), -1); // map even numbers to varid -1 for ref
        posToVaridMap.put(Position.of(chrom,4),-1);
        posToVaridMap.put(Position.of(chrom,6),-1);
        posToVaridMap.put(Position.of(chrom,8), -1);
        posToVaridMap.put(Position.of(chrom,10), -1);
        posToVaridMap.put(Position.of(chrom,1), 2); // map 1,3 to varid 2
        posToVaridMap.put(Position.of(chrom,3), 2);
        posToVaridMap.put(Position.of(chrom,5), 3); // map 5,7,9 to varid 3
        posToVaridMap.put(Position.of(chrom,7), 3);
        posToVaridMap.put(Position.of(chrom,9), 3);
        
        // Create taxa list for GenotypeTable.  This is a consensus genotype table, so only 1 taxon
        TaxaListBuilder taxaBuilder = new TaxaListBuilder();
        taxaBuilder.add("ConsensusTaxaName");
         
        TaxaList taxaList = taxaBuilder.build();
        GenotypeCallTableBuilder gtb =  GenotypeCallTableBuilder.getInstance(taxaList.size(), variantPositionList.size());
        
        // Map to hold varId to callCounter map.  The callCounter is an index
        // into the ConsensusProcessingUtils:gtvalues map
        BiMap<Integer,Integer> alleleRedirect = HashBiMap.create();
        
        // Add values for genotype table entries
        for (int pidx = 0; pidx < variantPositionList.size(); pidx++) { 
            int pos = variantPositionList.get(pidx).getPosition();
            if (pos % 2 == 0 ) {
                // set even positions to ref               
                gtb.setBase(0, pidx, ConsensusProcessingUtils.gtvalues[1]); // 0 is index in taxon list (only 1 taxon). 1 is ref
            } else {
                if (pos == 1 || pos == 3 ) {
                    gtb.setBase(0, pidx, ConsensusProcessingUtils.gtvalues[2]); // map positions 1 and 3 to same value
                    if (!alleleRedirect.containsKey(posToVaridMap.get(variantPositionList.get(pidx)))){ 
                        alleleRedirect.put(posToVaridMap.get(variantPositionList.get(pidx)),2);
                    }
                } else {                    
                    gtb.setBase(0, pidx, ConsensusProcessingUtils.gtvalues[3]); // 5,7.9 map to same value
                    if (!alleleRedirect.containsKey(posToVaridMap.get(variantPositionList.get(pidx)))){ 
                        alleleRedirect.put(posToVaridMap.get(variantPositionList.get(pidx)),3);
                    }
                }
            }
            // At specified position, store the variantID/allele mapping table
            posCallVarIdMap.put(pidx,alleleRedirect);
        }
        
        GenotypeTable clusterGT =  GenotypeTableBuilder.getInstance(gtb.build(), variantPositionList, taxaList);
        // From above:  even positions mapped to the reference, ie gtvalues[1], and varId is 1
        //  positions 1/3 mapped to gtvalues[2], with varId=2
        //  positions 5/7/9 mapped to gtvalues[3], setting varId=3
        
        // Verify chooseVarIdForSNPPositionFromGenotypeTable returns the correct value
        Map<Position, Integer> resultPosToVaridMap = ConsensusProcessingUtils.chooseVarIdForSNPPositionFromGenotypeTable( clusterGT,  posCallVarIdMap );
        
        // Printing maps for visual verification
        System.out.println("Results from chooseVarIdForSNPPosition call: ");
        for (Position pos : resultPosToVaridMap.keySet()) {
            System.out.println("position.getPosition " + pos.getPosition() + ", varid: " + resultPosToVaridMap.get(pos));
        }
        System.out.println("\nOriginal map results: ");
        for (Position pos : posToVaridMap.keySet()) {
            System.out.println("position.getPosition " + pos.getPosition() + ", varid: " + posToVaridMap.get(pos));
        }
        
        // verify results. The positions returned should map to the variant ids stored in posToVaridMap above
        assertEquals(10, resultPosToVaridMap.size()); // verify the number of SNPs found
        
        // Verify return from chooseVarIdForSNPPositionFromGenotypeTable() contains all expected values
        assertTrue(resultPosToVaridMap.entrySet().containsAll(posToVaridMap.entrySet()));
        
        // assert the reverse is true - original map contains all values returned from chooseVarIdForSNPPositionFromGenotypeTable()
        assertTrue(posToVaridMap.entrySet().containsAll(resultPosToVaridMap.entrySet()));
    }
}
