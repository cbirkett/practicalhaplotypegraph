package net.maizegenetics.pangenome.hapcollapse;

import net.maizegenetics.analysis.data.SortTaxaAlphabeticallyPlugin;
import net.maizegenetics.dna.map.*;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTableBuilder;
import net.maizegenetics.dna.snp.ImportUtils;
import net.maizegenetics.dna.snp.NucleotideAlignmentConstants;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.TaxaListBuilder;
import net.maizegenetics.taxa.Taxon;
import net.maizegenetics.util.DirectoryCrawler;
import net.maizegenetics.util.Utils;

import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Deprecated
public class FindHaplotypeClustersPluginTest {
    //Ed's environment
//    private static String userHome = System.getProperty("user.home");
//    private static String vcfDir = userHome + "/Box Sync/MaizePHG/VCFsFromGVCFTyper/EmitModeConfidentTest/";
//    private static String outputDir = userHome + "/temp/";
//  Zack's environment
//  private static String userHome=System.getProperty("user.home");
//  private static String vcfDir="/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/ProteomeIntervals/GVCFTyperTest/HapCollapseTests/SampleInputFiles/";
//  private static String outputDir = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/ProteomeIntervals/GVCFTyperTest/HapCollapseTests/CollapsedOutputFiles2/";
//  private static String refFile = "/Volumes/ZackBackup/Temp/Pangenome/EdHackathonUnitTests/Zea_mays.AGPv4.dna.toplevel.fa.gz";
//  private static String outputSequenceDir = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/ProteomeIntervals/GVCFTyperTest/HapCollapseTests/CollapsedOutputFilesSequence/";

  //Zack Environment Full
  private static String userHome=System.getProperty("user.home");
  private static String vcfDir="/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/ProteomeIntervals/GVCFTyperTest/HapCollapseTests/AllGVCFTyperOutputVCFs/";
  private static String outputDir = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/ProteomeIntervals/GVCFTyperTest/HapCollapseTests/FullTest/VCFOutputs2/";
  private static String refFile = "/Volumes/ZackBackup/Temp/Pangenome/EdHackathonUnitTests/Zea_mays.AGPv4.dna.toplevel.fa.gz";
  private static String outputSequenceDir = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/ProteomeIntervals/GVCFTyperTest/HapCollapseTests/FullTest/FastaOutputs2/";


  private static List<GenotypeTable> genotypeTables = new ArrayList<>();
  private static GenomeSequence refSequence;

    @Before
    public void setUp() throws Exception {
//        List<Path> fastaPaths = DirectoryCrawler.listPaths("glob:*.vcf.gz", Paths.get(vcfDir));
        List<Path> fastaPaths = DirectoryCrawler.listPaths("glob:*.vcf.gz", Paths.get(vcfDir));
        fastaPaths.forEach(fastaPath -> {
            genotypeTables.add(ImportUtils.readFromVCF(fastaPath.toString()));
        });
        refSequence = GenomeSequenceBuilder.instance(refFile);
    }
    @Test
    public void processData() throws Exception {
        try {
            List<Path> vcfPaths = DirectoryCrawler.listPaths("glob:*.vcf", Paths.get(vcfDir));
            System.out.println(genotypeTables.size());

            refSequence = GenomeSequenceBuilder.instance(refFile);
            for (Path vcfPath: vcfPaths) {

//            DataSet input = DataSet.getDataSet(genotypeTables.get(0));
                try {
                    GenotypeTable alignment = ImportUtils.readFromVCF(vcfPath.toString());

                    DataSet input = DataSet.getDataSet(alignment);
                    new FindHaplotypeClustersPlugin(null, false)
                            .outFile(outputDir)
                            .maxDistFromFounder(0.001)
                            .sequenceOutDir(outputSequenceDir)
                            .referenceSequence(refSequence)
                            .processData(input);
                } catch(IllegalStateException e) {
                    e.printStackTrace();
                }
            }
        }
        catch(Exception e) {
            System.out.println("Error in this vcf: ");
            e.printStackTrace();
        }
    }

    @Test
    public void runPlugin() throws Exception {
    }


    public static void main(String args[]) {
        if(args.length!=5) {
            System.out.println("Incorrect number of command line parameters.");
            System.out.println("Please do the following:");
            System.out.println(">java -Xmx100g -jar FindHaplotypeClustersPluginTest.jar inputVCFDirectory outputVCFDirectory outputSequenceDirectory refFasta errorLogFileName");
        }
        List<Path> fastaPaths = DirectoryCrawler.listPaths("glob:*.vcf", Paths.get(args[0]));
        try (BufferedWriter vcfErrorLogFile = Utils.getBufferedWriter(args[4])){
            vcfErrorLogFile.write("File\tErrorMessage\n");
            GenomeSequence genomeSequence = GenomeSequenceBuilder.instance(args[3]);

            for(int i = 0; i < fastaPaths.size();i++) {
                Path fastaPath = fastaPaths.get(i);
                try {
                    DataSet input = DataSet.getDataSet(ImportUtils.readFromVCF(fastaPath.toString()));

                    FindHaplotypeClustersPlugin plugin = new FindHaplotypeClustersPlugin(null, false)
                            .outFile(args[1])
                            .maxDistFromFounder(0.001)
                            .sequenceOutDir(args[2])
                            .referenceSequence(genomeSequence);

                            plugin.processData(input);
                }catch (Exception e) {
                    System.out.println("Error in this vcf:" + fastaPath.toString());
                    try {
                        vcfErrorLogFile.write(fastaPath + "\t" + e.getMessage()+"\n");
                    }
                    catch(Exception innerExc) {
                        innerExc.printStackTrace();
                    }
                    e.printStackTrace();
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testConsistency() {
        GenotypeTable lmFile = ImportUtils.readFromVCF("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/lm15/mergedVCFOutFastMethod/chr10_stPos124266698_merged.vcf");
        GenotypeTable mmFile = ImportUtils.readFromVCF("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/mm15/mergedVCFOutFastMethod/chr10_stPos124266698_merged.vcf");
//        GenotypeTable lmFile = ImportUtils.readFromVCF("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/lm15/mergedVCFOutFastMethod/chr10_stPos132243560_merged.vcf");
//        GenotypeTable mmFile = ImportUtils.readFromVCF("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/mm15/mergedVCFOutFastMethod/chr10_stPos132243560_merged.vcf");
//        GenotypeTable lmFile = ImportUtils.readFromVCF("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/lm15/mergedVCFOutFastMethod/chr10_stPos132243560_merged.vcf");
//        GenotypeTable mmFile = ImportUtils.readFromVCF("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/mm15/mergedVCFOutFastMethod/chr10_stPos132243560_merged.vcf");
//        GenotypeTable lmFile = ImportUtils.readFromVCF("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/lm15/mergedVCFOutFastMethod/chr5_stPos120451482_merged.vcf");
//        GenotypeTable mmFile = ImportUtils.readFromVCF("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/mm15/mergedVCFOutFastMethod/chr5_stPos120451482_merged.vcf");
//        GenotypeTable lmFile = ImportUtils.readFromVCF("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/lm15/mergedVCFOutFastMethod/chr7_stPos123883325_merged.vcf");
//        GenotypeTable mmFile = ImportUtils.readFromVCF("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/mm15/mergedVCFOutFastMethod/chr7_stPos123883325_merged.vcf");


        SortTaxaAlphabeticallyPlugin sortPlugin = new SortTaxaAlphabeticallyPlugin(null, false);

        GenomeSequence refSeq = GenomeSequenceBuilder.instance("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/Zea_mays.AGPv4.dna.toplevelMtPtv3.fa");

        List<Datum> inputs = new ArrayList<>();
        inputs.add(new Datum("lmGT",lmFile,"lmFile"));
        DataSet lmDs = new DataSet(inputs,null);

        FindHaplotypeClustersPlugin findHaplotypeClustersPluginLM = new FindHaplotypeClustersPlugin(null, false).outFile("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/lm15/unitTest/").sequenceOutDir("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/lm15/unitTest/").referenceSequence(refSeq).maxDistFromFounder(.001);
//        findHaplotypeClustersPluginLM.performFunction(new SortTaxaAlphabeticallyPlugin(null, false).performFunction(lmDs));
        findHaplotypeClustersPluginLM.performFunction(lmDs);

        List<Datum> inputs2 = new ArrayList<>();
        inputs2.add(new Datum("mmGT",mmFile,"mmFile"));
        DataSet mmDs = new DataSet(inputs2,null);

        FindHaplotypeClustersPlugin findHaplotypeClustersPluginMM = new FindHaplotypeClustersPlugin(null, false).outFile("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/mm15/unitTest/").sequenceOutDir("/Volumes/ZackBackup/Temp/Pangenome/DockerTests/ConsensusTests/mm15/unitTest/").referenceSequence(refSeq).maxDistFromFounder(.001);
//        findHaplotypeClustersPluginMM.performFunction(new SortTaxaAlphabeticallyPlugin(null, false).performFunction(mmDs));
        findHaplotypeClustersPluginMM.performFunction(mmDs);

    }

    @Test
    public void testSimpleConsensus() {
        PositionList positions = new PositionListBuilder().add(Position.of(1,100)).add(Position.of(1,101)).build();
        TaxaList taxa = new TaxaListBuilder().add(new Taxon("B73")).add(new Taxon("W22")).add(new Taxon("CML247")).build();
        GenotypeTableBuilder gtb = GenotypeTableBuilder.getTaxaIncremental(positions);
        byte ADiploid = NucleotideAlignmentConstants.getNucleotideDiploidByte("AA");
        byte GDiploid = NucleotideAlignmentConstants.getNucleotideDiploidByte("GG");
        gtb.addTaxon(new Taxon("B73"),new byte[]{ADiploid,ADiploid});
        gtb.addTaxon(new Taxon("W22"),new byte[]{ADiploid,GDiploid});
        gtb.addTaxon(new Taxon("CML247"),new byte[]{ADiploid,ADiploid});

        GenotypeTable gt = gtb.build();

        for(int tx= 0; tx < taxa.size(); tx++) {
            System.out.print(taxa.taxaName(tx)+"\t");
            for(int pos = 0; pos < positions.size(); pos++) {
                System.out.print(""+gt.genotypeAsString(tx,pos));
            }
            System.out.println();
        }

        System.out.println();
        System.out.println(NucleotideAlignmentConstants.getHaplotypeNucleotide(gt.majorAllele(0)));
        System.out.println(NucleotideAlignmentConstants.getHaplotypeNucleotide(gt.minorAllele(0)));

        System.out.println(gt.minorAlleles(0).length);
        System.out.println("***************");
        System.out.println(NucleotideAlignmentConstants.getHaplotypeNucleotide(gt.majorAllele(1)));
        System.out.println(NucleotideAlignmentConstants.getHaplotypeNucleotide(gt.minorAllele(1)));

        System.out.println("Attempting to run consensus");

        DataSet ds = new DataSet(new Datum("gt",gt,"Genotype Table"),null);

        FindHaplotypeClustersPlugin clustersPlugin = new FindHaplotypeClustersPlugin(null, false)
                .maxDistFromFounder(0.01)
//                .maxDistFromFounder(0.51)
                .seqErrorRate(0.01)
                .minSiteForComp(1)
                .minTaxaInGroup(1)
                .minTaxaCoverage(1.0)
//                .maxError(.21)
                .maxError(.33)
                .replaceNsWithMajor(false)
//                .replaceNsWithMajor(true)
                .useDepthForCalls(false)
                .clusterMethod(FindHaplotypeClustersPlugin.CLUSTER_METHOD.coverage);

        DataSet output = clustersPlugin.performFunction(ds);

        List<GenotypeTable> tables = output.getDataOfType(GenotypeTable.class).stream().map(datum -> (GenotypeTable)datum.getData()).collect(Collectors.toList());

        for(GenotypeTable table : tables) {
            System.out.println("*******************************");

            TaxaList taxa2 = table.taxa();
            PositionList positions2 = table.positions();

            for(int tx= 0; tx < taxa2.size(); tx++) {
                System.out.print(taxa2.taxaName(tx)+"\t");
                for(int pos = 0; pos < positions2.size(); pos++) {
                    System.out.print(""+table.genotypeAsString(tx,pos)+" ");
                }
                System.out.println();
            }

            System.out.println("*******************************");
        }
    }

}
