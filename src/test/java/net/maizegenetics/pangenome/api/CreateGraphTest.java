/**
 * 
 */
package net.maizegenetics.pangenome.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.sqlite.SQLiteConfig;

import com.google.common.io.CharStreams;

import net.maizegenetics.pangenome.db_loading.AnchorDataPHG;
import net.maizegenetics.util.CheckSum;

/**
 * Tests individual methods in the CreateGraph class.
 * 
 * Verify with DB the fields works
 * 
 * NOTE: this works only with sqlite db.  TO work with postgres you must
 * use CreateGraphUtils.connection(configFile)
 * 
 * @author lcj34
 *
 */
public class CreateGraphTest {
    
    //private final String db = "/Users/lcj34/notes_files/repgen/wgs_pipeline/agpv4_phg_dbs/august2017_forGrant_testConsensusSeq.db";
    private static String userHome = System.getProperty("user.home");

    Connection conn = null;
    
    @Test
    public void testCreateHaplotypeSequenceFromConsensus() throws Exception {
        String host = null;
        String user = "";
        String password = "";
        String db = userHome + "/Box Sync/MaizePHG/PHG_junit_data/consensusSeq_test.db";
        Connection conn = CreateGraphUtils.connection(host, user, password, db);
        
        String method_name = "CONSENSUS_1"; // consensus method 1 as stored in test db
        Map<Integer, ReferenceRange> refRangeMap = CreateGraphUtils.referenceRangeMap( conn);
        Map<String, HaplotypeSequence> seqIDHapSeqMap = null; // CreateGraphUtils.cre( conn, refRangeMap,  method_name);
        
        // The test database has 15 anchor_haplotype entries.  The anchor_sequence_ids in anchor_haplotypes 
        // are linked to the 15 original sequences in the anchor sequence table.  There are 2 additional sequences
        // in the anchor_sequences table that are consensus sequences not reflected in the anchor_haplotypes table.
        // 
        // Of the 15 original sequences, sequence ids 1,6,11 map to consensus 16.  Of the original sequences 3,8,13 
        // map to consensus 17.  There will only be 1 entry in the map for each consensus. 
        //
        // 15-6 = 9 original sequences that are not part of a consensus.  Then add 1 for each of the consensus 
        // sequences and you get 11.  These 11 are what should be returned from the query.
        assertEquals(seqIDHapSeqMap.size(),11);
        assertTrue(seqIDHapSeqMap.containsKey(16)); // first consensus sequence id
        assertTrue(seqIDHapSeqMap.containsKey(17)); // second consensus sequence id
        assertTrue(!seqIDHapSeqMap.containsKey(1)); // should be represented by id 16
        assertTrue(!seqIDHapSeqMap.containsKey(3)); // represented by id 16
        assertTrue(!seqIDHapSeqMap.containsKey(11)); // represented by id 16
        assertTrue(!seqIDHapSeqMap.containsKey(3)); // should be represented by id 17
        assertTrue(!seqIDHapSeqMap.containsKey(8)); // represented by id 17
        assertTrue(!seqIDHapSeqMap.containsKey(13)); // represented by id 17
        
        // Verify the sequences for the consensusId are correct.
        String consensusSeq16 = "aaaCCacccccccccgggggggggggttttttttttt";
        String consensusSeq17 = "tatatatatgcGGtttactactactatcatcact";
        assertTrue(consensusSeq16.toUpperCase().equals(seqIDHapSeqMap.get(16).sequence().toUpperCase()));
        assertTrue(consensusSeq17.toUpperCase().equals(seqIDHapSeqMap.get(17).sequence().toUpperCase()));
    }
 
}
