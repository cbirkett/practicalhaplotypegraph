package net.maizegenetics.pangenome.api;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import net.maizegenetics.pangenome.api.HaplotypeNode.VariantInfo;
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths;
import net.maizegenetics.taxa.Taxon;
import net.maizegenetics.util.LoggingUtils;

public class HaplotypeNodeTest {
    
    private static HaplotypeGraph hapGraph;
    private static HaplotypeGraph refGraph;
    private static Logger myLogger = Logger.getLogger(HaplotypeNodeTest.class);
    
    @BeforeClass
    public static void loadHaplotypeGraph() {
        LoggingUtils.setupDebugLogging();
        hapGraph = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(SmallSeqPaths.dbConfigFile)
                .methods("GATK_PIPELINE," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP)
                .includeVariantContexts(true)
                .build();
        refGraph = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(SmallSeqPaths.dbConfigFile)
                .methods("B73Ref_method," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP)
                .includeVariantContexts(true)
                .build();
    }
    
    @Test
    public void testRefVariants() {
        //tests whether there are any variants in ref
        //there should not be. should only be refblocks
        refGraph.nodeStream().filter(hn -> {
            ReferenceRange range = hn.referenceRange();
            int length = range.end() - range.start() + 1;
            return length > 10;
        })
        .forEach(hn -> {
            System.out.print("ref range at " + hn.referenceRange().start() + ": ");
            Optional<List<VariantInfo>> optList = hn.variantInfos();
            if (optList.isPresent()) {
                List<VariantInfo> infoList = optList.get();
                System.out.print(infoList.size() + " variant infos");
                long numberOfVariants = infoList.stream().filter(info -> info.isVariant()).count();
                System.out.println(", " + numberOfVariants + " variants");
                assertTrue("Variants in reference", numberOfVariants == 0);
            }
            System.out.println();
        });
        
    }
    
    @Test
    public void testVariantInfo() {
        //get some graph values
        myLogger.info("Number of chromosomes = " + hapGraph.numberOfChromosomes());
        myLogger.info("Number of ranges = " + hapGraph.numberOfRanges());
        myLogger.info("Number of taxa = " + hapGraph.totalNumberTaxa());
        
        //get nodes for refrange 1
        List<HaplotypeNode> hapNodes = hapGraph.nodes(hapGraph.firstReferenceRange());
        myLogger.info("Number of nodes in the first range = " + hapNodes.size());
        HaplotypeNode myNode = hapNodes.get(0);
        myLogger.info("First node taxa = " + myNode.taxaList().stream().map(Taxon::getName)
                .collect(Collectors.joining(",")));
        String taxonName = myNode.taxaList().taxaName(0);
        List<Long> variantLongs = myNode.variantContextsLong().orElse(new ArrayList<>());
        myLogger.info(taxonName + " has " + variantLongs.size() + " variantLongs.");
        long numberOfRefBlocks = variantLongs.stream().filter(val -> val < 0).count();
        myLogger.info("Of those, " + numberOfRefBlocks + " are ref blocks.");
        
        //report some variant stats
        List<VariantInfo> infoList = myNode.variantInfos().orElse(new ArrayList<>());
        long numberOfVariants = infoList.stream().filter(VariantInfo::isVariant).count();
        long numberOfIndels = infoList.stream().filter(VariantInfo::isIndel).count();
        myLogger.info("number of Variants = " + numberOfVariants + ", number of Indels = " + numberOfIndels);
        long numberOfRefs = infoList.stream().filter(vi -> vi.genotypeString().equals(VariantInfo.Ref)).count();
        long numberOfNonRefs = infoList.stream().filter(vi -> vi.genotypeString().equals(VariantInfo.NonRef)).count();
        long numberOfMissing = infoList.stream().filter(vi -> vi.genotypeString().equals(VariantInfo.missing)).count();
        myLogger.info(String.format("Number of refs = %d, non-refs = %d, missing = %d", numberOfRefs, numberOfNonRefs, numberOfMissing));
        
//        areVariantsCalledCorrectly(1, "LineA", hapGraph.firstReferenceRange());
        
        hapGraph.referenceRangeStream().forEach(range -> areVariantsCalledCorrectly(1, "LineB", range));
        
    }
    
    private void areVariantsCalledCorrectly(int refRangeId, String taxonName, ReferenceRange range) {
        
        HaplotypeNode hapNode = hapGraph.nodes(range).stream()
                .filter(hn -> hn.taxaList().indexOf(taxonName) >= 0).findAny().get();
        
        String otherseq = hapNode.haplotypeSequence().sequence();
        String refseq = refGraph.nodes(range).get(0).haplotypeSequence().sequence();
        
        if (refseq.length() != otherseq.length()) {
            System.out.printf("ref and alt sequences have different lengths at %d%n", range.start());
            return;
        }
        List<VariantInfo> variantInfoList = hapNode.variantInfos().get();
        
        int numberOfVar = 0;
        int numberOfN = 0;
        boolean printseqs = false;
        for (VariantInfo var : variantInfoList) {
            if (var.isVariant()) {
                if (!var.altAlleleString().equals("N")) {
                    int relativePos = var.start() - range.start();
                    String altString = otherseq.substring(relativePos, relativePos + 1);
                    String refString = refseq.substring(relativePos, relativePos + 1);
                    boolean test1 = var.altAlleleString().equals(altString);
                    boolean test2 = var.refAlleleString().equals(refString);
                    if (!test1 || !test2) {
                        System.out.printf("at %d ref = %s, alt = %s, refallele = %s, altallele = %s%n", 
                                var.start(),refString, altString, var.refAlleleString(), var.altAlleleString());
                    }
                    if (var.isIndel()) printseqs = true;
                    assertTrue("other seq != altAllele at " + var.start(), test1 || var.isIndel());
                    assertTrue("ref seq != refAllele at " + var.start(), test2 || var.isIndel());
                    numberOfVar++;
                }
                else if (var.altAlleleString().equals("N")) {
                    int relativePos = var.start() - range.start();
                    char seqchar = otherseq.charAt(relativePos);
                    assertTrue("alt allele = N but sequence = " + seqchar +  " at sequence position " + var.start(),
                            otherseq.charAt(relativePos) == 'N');
                    numberOfN++;
                }
                
            }
        }
//        if (printseqs) {
//            System.out.println("ref = " + refseq);
//            System.out.println("other = " + otherseq);
//        }

        System.out.printf("At range starting at %d: %s has %d variants and %d N's%n", range.start(), taxonName, numberOfVar, numberOfN);
        
    }

}
