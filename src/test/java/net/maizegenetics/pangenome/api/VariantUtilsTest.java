package net.maizegenetics.pangenome.api;

import htsjdk.variant.variantcontext.Allele;
import htsjdk.variant.variantcontext.VariantContext;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class VariantUtilsTest {

    @Test
    public void testVariantEncoding() {
        List<Long> encodedVCs = new ArrayList<>();
        Map<Integer,DBVariant> variantMapping = new HashMap<>();
        GenomeSequence gs = GenomeSequenceBuilder.instance("/Users/zrm22/temp/phgSmallSeq/ref/Ref.fa");

        encodedVCs.add(encodeRefBlock(1,(byte)10,31));
        encodedVCs.add(encodeRefBlock(33,(byte)12,49));

        //int variantID, Chromosome chromosome, int startPosition, Allele refAllele, Allele altAllele, int ancestralID
        variantMapping.put(1,new DBVariant(1, Chromosome.instance("1"), 83, Allele.create("T",true), Allele.create("A",false),-1));
        encodedVCs.add(encodeVariant(1,(byte)0,(byte)1));


        encodedVCs.add(encodeRefBlock(84,(byte)6,63));

        variantMapping.put(2,new DBVariant(2, Chromosome.instance("1"), 148, Allele.create("C",true), Allele.create("T",false),-1));
        encodedVCs.add(encodeVariant(2,(byte)0,(byte)7));


        encodedVCs.add(encodeRefBlock(149,(byte)12,24));

        List<VariantContext> vcs = VariantUtils.extractOutVariantContext(encodedVCs,"1", gs, "LineA1",variantMapping);

        System.out.println(vcs.stream().map(variantContext -> variantContext.toString()).collect(Collectors.joining("\n")));

    }

    private long encodeRefBlock(int pos, byte dp, int length) {
        long encoded = 1;

        encoded = (encoded <<23) | length;

        encoded = (encoded << 8) | dp;

        encoded = (encoded << 32) | pos;

        return encoded;
    }

    // int variantID = (int)((variantEncoded>>32)&0xFFFF);
    //        int refDepth = AlleleDepthUtil.depthByteToInt((byte)((variantEncoded>>24) & 0xFF));
    //        int altDepth = AlleleDepthUtil.depthByteToInt((byte)((variantEncoded>>16) & 0xFF));
    private long encodeVariant(int variantID, byte refDepth, byte altDepth) {
        long encoded = variantID;

        encoded = (encoded << 8) | refDepth;
        encoded = (encoded << 8) | altDepth;
        encoded = (encoded << 16);

        System.out.println(encoded);
        System.out.println(Long.toHexString(encoded));

        return encoded;
    }
}
