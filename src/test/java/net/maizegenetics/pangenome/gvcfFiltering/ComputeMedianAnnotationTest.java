package net.maizegenetics.pangenome.gvcfFiltering;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by zrm22 on 8/8/17.
 */
public class ComputeMedianAnnotationTest {

    @Test
    public void testMedian() {
        Assert.assertEquals(10,ComputeMedianAnnotation.getMedianDepth("data/simpleGVCFTestFile.g.vcf"));
    }

    @Test
    public void testAverage() {
        Assert.assertEquals(11.6299,ComputeMedianAnnotation.getMeanDepth("data/simpleGVCFTestFile.g.vcf"),.0001);
    }

    @Test
    public void testMode() {
        Assert.assertEquals(15,ComputeMedianAnnotation.getModeDepth("data/simpleGVCFTestFile.g.vcf"));
    }
}
