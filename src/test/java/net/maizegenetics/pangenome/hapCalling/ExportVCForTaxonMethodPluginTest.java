/**
 * 
 */
package net.maizegenetics.pangenome.hapCalling;

import org.junit.Test;

/**
 * This test calls ExrpotVCForTaxonMethodPlugin to create a vcf file
 * based on variant contexts for a particular taxon in a given PHG database.
 * 
 * @author lcj34
 *
 */
@Deprecated
public class ExportVCForTaxonMethodPluginTest {

    @Test
    public void testExportVCForTaxonMethodPlugin() {
        
        new ExportVCForTaxonMethodPlugin()
        //.configFile("/Users/lcj34/notes_files/phg_2018/training/debug/configTestPaths.txt")
        .configFile("/Users/lcj34/notes_files/phg_2018/training/debug/configMaggie.txt")
        //.method("CONSENSUS")
        .methods("mummer4,*") // same in smallSeq and for Maggie's db
        //.taxon("LineA_Haplotype_Caller") // smallseq
        //.taxon("LineA1_Assembly")// smallseq
        .taxon("W22_Assembly") // maggie
        .outputFile("/Users/lcj34/notes_files/phg_2018/training/debug/exportVCForTaxonMethod_MaggieW22_includeInterAnchor.vcf")
        .processData(null);
    }
}
