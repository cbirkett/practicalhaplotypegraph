package net.maizegenetics.pangenome.hapCalling;

import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin;
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import net.maizegenetics.plugindef.DataSet;
import org.junit.Test;

/**
 * Simple test class to run ScoreRangesByInclusionCountsPlugin Created by zrm22 on 10/11/17.
 */
@Deprecated
public class ScoreRangesByInclusionCountsPluginTest {

    //    Zack Testing file path
    private static final String userHome = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles";

    //New DB
    // static final String dbPath = userHome + "/temp/hapcalling/sept_18_2017_308haps_306ConsensusWithAssemblies.db";
    static final String configFile = userHome + "/temp/hapcalling/configFile.txt";

    private static final String refRangeFile = "./data/HapCallingTestFiles/AllTaxaAveraged_Evaluation_FilteredRefRanges.txt";
    //    private static final String inclusionFiles = userHome + "/temp/hapcalling/RamuFiles/SmallInclusionFile/";
    private static final String inclusionFiles = userHome + "/temp/hapcalling/RamuFiles/CIMMYTInclusionFiles/";
    private static final String outputSummaryFile = userHome + "/temp/hapcalling/RamuFiles/ScoreInclusionsCIMMYTFilteredRefRangesInfoNodeCounts.txt";
//    private static final String outputSummaryFile = userHome + "/temp/hapcalling/RamuFiles/ScoreInclusionsSmallInclusionFile.txt";


    @Test
    public void testScoreRanges() {
        HaplotypeGraphBuilderPlugin graphBuilder = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("collapse_method_1," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP)
                .includeVariantContexts(false);

        System.out.println("Getting graph");
        DataSet graphDataSet = graphBuilder.processData(null);


        ScoreRangesByInclusionCountsPlugin scorePlugin = new ScoreRangesByInclusionCountsPlugin(null, false)
                .inclusionFilenameDir(inclusionFiles)
                .outputFileName(outputSummaryFile)
                .filterRefRanges(refRangeFile);
        scorePlugin.processData(graphDataSet);
    }


}
