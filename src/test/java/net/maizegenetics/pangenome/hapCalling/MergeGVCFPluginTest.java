package net.maizegenetics.pangenome.hapCalling;

import htsjdk.variant.variantcontext.VariantContext;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.dna.snp.ExportUtils;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.io.VCFUtil;
import net.maizegenetics.pangenome.api.HaplotypeGraph;
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin;
import net.maizegenetics.pangenome.api.ReferenceRange;
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import net.maizegenetics.pangenome.hapcollapse.FindHaplotypeClustersPlugin;
import net.maizegenetics.pangenome.hapcollapse.MergeGVCFPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.util.Utils;
import org.junit.Test;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.*;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.Collectors;

/**
 * Created by zrm22 on 11/8/17.
 * TODO fix this test to actually be a set of unit tests rather than an integration test
 * Most of the code that is used by MergeGVCFPlugin is tested in MergeGVCFUtilsTest
 */
@Deprecated
public class MergeGVCFPluginTest {

    //    String inputDB = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/TestDBs/nov2017_110HapsCIMMYTAndNAMWithGVCF.db";
    // String inputDB = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/MergeGVCFPipeline/phgSmallSeq.db";
    String inputDB = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/MergeGVCFPipeline/phgSmallSeq.db";
    String inputDBReal = "/Volumes/ZackBackup/Temp/Pangenome/DockerTests/testOutputDockers/pghTestDB_W22.db";
    String configFile = "configFile.txt";
    //    String dbConfig = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/MergeGVCFPipeline/dbConfig.txt";
    String dbConfig = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/MergeGVCFPipeline/dbConfigSmallSeq.txt";
    String dbConfigReal = "/Volumes/ZackBackup/Temp/Pangenome/DockerTests/testOutputDockers/config.txt";
    String intervalFile = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/MergeGVCFPipeline/testInterval.intervals";
    //    String outputVCFDir = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/MergeGVCFPipeline/VCFs/";
    String outputVCFDir = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/MergeGVCFPipeline/VCFsSmallSeq/";

    String referenceSequence = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/MergeGVCFPipeline/ref.fa";

    String referenceSequenceReal = "/Volumes/ZackBackup/Temp/Pangenome/DockerTests/testOutputDockers/Zea_mays.AGPv4.dna.toplevelMtPtv3.fa";

//    @Test
//    public void testMergeGVCFPlugin() {
//        System.out.println("Loading Graph");
//        SortedSet<Integer> hapIdsToKeep = new TreeSet<>();
//        hapIdsToKeep.add(3818344);
//        hapIdsToKeep.add(3780540);
//        hapIdsToKeep.add(3742736);
//        hapIdsToKeep.add(3704932);
//        hapIdsToKeep.add(3667128);
//        hapIdsToKeep.add(3629324);
//
//        HaplotypeGraph graph = new HaplotypeGraphBuilderPlugin(null,false).databaseName(inputDB).method("Haplotype_caller").includeVariantContexts(false).hapids(hapIdsToKeep).build();
//
//        System.out.println("Filtering Graph");
//        DataSet graphToMerge = new DataSet(new Datum("PHG", graph, null), null);
//
//        System.out.println("Running merge function:");
//        new MergeGVCFPlugin(null,false).dBConfig(dbConfig).outputDir(outputVCFDir).processData(graphToMerge);
//
//    }

    @Test
    public void testMergeGVCFPluginSmallSeq() {
        System.out.println("Loading Graph");
        HaplotypeGraph graph = new HaplotypeGraphBuilderPlugin(null, false).configFile(configFile).methods("GATK_PIPELINE," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP).includeVariantContexts(false).build();
        System.out.println("Number of Nodes In graph:" + graph.numberOfNodes());
        List<Datum> datumList = new ArrayList<>();
        datumList.add(new Datum("PHG", graph, null));
        datumList.add(new Datum("Ref", GenomeSequenceBuilder.instance(referenceSequence), null));
        DataSet graphToMerge = new DataSet(datumList, null);


        System.out.println("Merging VCFs");
        new MergeGVCFPlugin(null, false).dBConfig(dbConfig).outputDir(outputVCFDir).processData(graphToMerge);

    }

    @Test
    public void testMergeGVCFPluginRealData() {
        System.out.println("Loading Graph");
        HaplotypeGraph graph = new HaplotypeGraphBuilderPlugin(null,false).configFile(dbConfigReal).methods("GATK_PIPELINE," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP).includeVariantContexts(false).build();

        List<ReferenceRange> refRangeToKeep = new ArrayList<>();
        refRangeToKeep.add(graph.referenceRangeList().get(0));
        // HaplotypeGraph graph2 = new FilterGraphPlugin(null,false).refRanges(refRangeToKeep).filter(graph);
        System.out.println("Number of Nodes In graph:"+graph.numberOfNodes());
        System.out.println(refRangeToKeep.get(0).intervalString());
        List<Datum> datumList = new ArrayList<>();
        datumList.add(new Datum("PHG", graph, null));
        datumList.add(new Datum("Ref", GenomeSequenceBuilder.instance(referenceSequenceReal),null));
        datumList.add(new Datum("refRange",refRangeToKeep.get(0),null));
        DataSet graphToMerge = new DataSet(datumList, null);


        System.out.println("Merging VCFs");
        DataSet ds = new MergeGVCFPlugin(null,false).dBConfig(dbConfigReal).outputDir(outputVCFDir).processData(graphToMerge);
        List<Datum> genotypeTableDatum = ds.getDataOfType(GenotypeTable.class);
        ExportUtils.writeToVCF((GenotypeTable)genotypeTableDatum.get(0).getData(),outputVCFDir+refRangeToKeep.get(0).intervalString()+".vcf",false);
    }

    @Test
    public void testMergeGVCFPluginRealData21() {
        System.out.println("Loading Graph");
        HaplotypeGraph graph = new HaplotypeGraphBuilderPlugin(null,false).configFile(dbConfigReal).methods("GATK_PIPELINE," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP).includeVariantContexts(false).build();

        List<ReferenceRange> refRangeToKeep = new ArrayList<>();
        for(ReferenceRange referenceRange:graph.referenceRangeList()) {
            if(referenceRange.id()==21) {
                refRangeToKeep.add(referenceRange);
                break;
            }
        }
//        refRangeToKeep.add(graph.referenceRangeList().get(0));
        // HaplotypeGraph graph2 = new FilterGraphPlugin(null,false).refRanges(refRangeToKeep).filter(graph);
        System.out.println("Number of Nodes In graph:"+graph.numberOfNodes());
        System.out.println(refRangeToKeep.get(0).intervalString());
        List<Datum> datumList = new ArrayList<>();
        datumList.add(new Datum("PHG", graph, null));
        datumList.add(new Datum("Ref", GenomeSequenceBuilder.instance(referenceSequenceReal),null));
        datumList.add(new Datum("refRange",refRangeToKeep.get(0),null));
        DataSet graphToMerge = new DataSet(datumList, null);


        System.out.println("Merging VCFs");
        DataSet ds = new MergeGVCFPlugin(null,false).dBConfig(dbConfigReal).outputDir(outputVCFDir).processData(graphToMerge);
        List<Datum> genotypeTableDatum = ds.getDataOfType(GenotypeTable.class);
        ExportUtils.writeToVCF((GenotypeTable)genotypeTableDatum.get(0).getData(),outputVCFDir+refRangeToKeep.get(0).intervalString()+".vcf",false);
    }

    //Anchor id 62 will have odd indel issues.  Use this test to verify changes will make a difference.
    @Test
    public void testMergeGVCFPluginRealData62() {
        System.out.println("Loading Graph");
        HaplotypeGraph graph = new HaplotypeGraphBuilderPlugin(null,false).configFile(dbConfigReal).methods("GATK_PIPELINE," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP).includeVariantContexts(true).build();

        List<ReferenceRange> refRangeToKeep = new ArrayList<>();
        for(ReferenceRange referenceRange:graph.referenceRangeList()) {
            if(referenceRange.id()==62) {
                refRangeToKeep.add(referenceRange);
                break;
            }
        }
//        refRangeToKeep.add(graph.referenceRangeList().get(0));
        // HaplotypeGraph graph2 = new FilterGraphPlugin(null,false).refRanges(refRangeToKeep).filter(graph);
        System.out.println("Number of Nodes In graph:"+graph.numberOfNodes());
        System.out.println(refRangeToKeep.get(0).intervalString());
        List<Datum> datumList = new ArrayList<>();
        datumList.add(new Datum("PHG", graph, null));
        datumList.add(new Datum("Ref", GenomeSequenceBuilder.instance(referenceSequenceReal),null));
        datumList.add(new Datum("refRange",refRangeToKeep.get(0),null));
        DataSet graphToMerge = new DataSet(datumList, null);


        System.out.println("Merging VCFs");
        DataSet ds = new MergeGVCFPlugin(null,false).dBConfig(dbConfigReal).outputDir(outputVCFDir).processData(graphToMerge);
        List<Datum> genotypeTableDatum = ds.getDataOfType(GenotypeTable.class);
        ExportUtils.writeToVCF((GenotypeTable)genotypeTableDatum.get(0).getData(),outputVCFDir+refRangeToKeep.get(0).intervalString()+".vcf",false);
    }


    @Test
    public void testMergeGVCFPluginRealData130() {
        System.out.println("Loading Graph");
        HaplotypeGraph graph = new HaplotypeGraphBuilderPlugin(null,false).configFile(dbConfigReal).methods("GATK_PIPELINE," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP).includeVariantContexts(false).build();

        List<ReferenceRange> refRangeToKeep = new ArrayList<>();
        for(ReferenceRange referenceRange:graph.referenceRangeList()) {
            if(referenceRange.id()==130) {
                refRangeToKeep.add(referenceRange);
                break;
            }
        }
//        refRangeToKeep.add(graph.referenceRangeList().get(0));
        // HaplotypeGraph graph2 = new FilterGraphPlugin(null,false).refRanges(refRangeToKeep).filter(graph);
        System.out.println("Number of Nodes In graph:"+graph.numberOfNodes());
        System.out.println(refRangeToKeep.get(0).intervalString());
        List<Datum> datumList = new ArrayList<>();
        datumList.add(new Datum("PHG", graph, null));
        datumList.add(new Datum("Ref", GenomeSequenceBuilder.instance(referenceSequenceReal),null));
        datumList.add(new Datum("refRange",refRangeToKeep.get(0),null));
        DataSet graphToMerge = new DataSet(datumList, null);


        System.out.println("Merging VCFs");
        DataSet ds = new MergeGVCFPlugin(null,false).dBConfig(dbConfigReal).outputDir(outputVCFDir).processData(graphToMerge);
        List<Datum> genotypeTableDatum = ds.getDataOfType(GenotypeTable.class);
        ExportUtils.writeToVCF((GenotypeTable)genotypeTableDatum.get(0).getData(),outputVCFDir+refRangeToKeep.get(0).intervalString()+".vcf",false);
    }

    @Test
    public void testMergeGVCFPluginRealData2000Intervals() {

        SortedSet<Integer> hapIdsToKeep = readHapIds();
        System.out.println("Loading Graph");
        HaplotypeGraph graph = new HaplotypeGraphBuilderPlugin(null,false).configFile(dbConfigReal).methods("GATK_PIPELINE," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP).includeVariantContexts(true).hapids(hapIdsToKeep).build();


//        refRangeToKeep.add(graph.referenceRangeList().get(0));
        // HaplotypeGraph graph2 = new FilterGraphPlugin(null,false).refRanges(refRangeToKeep).filter(graph);
        System.out.println("Number of Nodes In graph:"+graph.numberOfNodes());

        GenomeSequence reference = GenomeSequenceBuilder.instance(referenceSequenceReal);

        LongAdder totalMergeTime = new LongAdder();
        LongAdder totalClusterTime = new LongAdder();
        graph.referenceRangeList().parallelStream().forEach( refRange -> {
            System.out.println(refRange.intervalString());
            List<Datum> datumList = new ArrayList<>();
            datumList.add(new Datum("PHG", graph, null));
            datumList.add(new Datum("Ref", reference, null));
            datumList.add(new Datum("refRange", refRange, null));
            DataSet graphToMerge = new DataSet(datumList, null);


            System.out.println("Merging VCFs");
            long startTimeMerge = System.nanoTime();
            DataSet ds = new MergeGVCFPlugin(null, false)
                    .dBConfig(dbConfigReal)
                    .outputDir(outputVCFDir)
                    .processData(graphToMerge);
            long endTimeMerge = System.nanoTime();

            long startTimeCluster = System.nanoTime();
            new FindHaplotypeClustersPlugin(null, false)
                    .maxDistFromFounder(.001)
                    .outFile(outputVCFDir+"ProfilerTests/gvcfs/")
                    .sequenceOutDir(outputVCFDir+"ProfilerTests/fastas/")
                    .referenceSequence(reference)
                    .processData(ds);
            long endTimeCluster = System.nanoTime();
            System.out.println("Time For Merge Plugin:"+(endTimeMerge-startTimeMerge)/1E9);
            System.out.println("Time For Cluster Plugin:"+(endTimeCluster - startTimeCluster)/1E9);

            totalMergeTime.add(endTimeMerge-startTimeMerge);
            totalClusterTime.add(endTimeCluster - startTimeCluster);

        });

        System.out.println("Avg MergeTime:"+totalMergeTime.longValue()/graph.referenceRangeList().size()/1E9);
        System.out.println("Avg ClusterTime:"+totalClusterTime.longValue()/graph.referenceRangeList().size()/1E9);

//        for(ReferenceRange refRange : graph.referenceRangeList()) {
//            System.out.println(refRange.intervalString());
//            List<Datum> datumList = new ArrayList<>();
//            datumList.add(new Datum("PHG", graph, null));
//            datumList.add(new Datum("Ref", reference, null));
//            datumList.add(new Datum("refRange", refRange, null));
//            DataSet graphToMerge = new DataSet(datumList, null);
//
//
//            System.out.println("Merging VCFs");
//            DataSet ds = new MergeGVCFPlugin(null, false).dBConfig(dbConfigReal).outputDir(outputVCFDir).haplotypeMethod("GATK_PIPELINE").processData(graphToMerge);
////            List<Datum> genotypeTableDatum = ds.getDataOfType(GenotypeTable.class);
////            ExportUtils.writeToVCF((GenotypeTable) genotypeTableDatum.get(0).getData(), outputVCFDir + refRange.intervalString() + ".vcf", false);
//
//
//            new FindHaplotypeClustersPlugin(null, false)
//                    .maxDistFromFounder(.001)
//                    .outFile(outputVCFDir+"ProfilerTests/gvcfs/")
//                    .sequenceOutDir(outputVCFDir+"ProfilerTests/fastas/")
//                    .referenceSequence(reference)
//                    .processData(ds);
//        }
    }

    /**
     * Test code to try out actual mutithreading other than using a parallel stream.
     */
    @Test
    public void testMergeGVCFPluginRealData2000IntervalsMT() {

        SortedSet<Integer> hapIdsToKeep = readHapIds();
        System.out.println("Loading Graph");
        HaplotypeGraph graph = new HaplotypeGraphBuilderPlugin(null,false).configFile(dbConfigReal).methods("GATK_PIPELINE," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP)
                .includeVariantContexts(true)
                .hapids(hapIdsToKeep)
                .build();


//        refRangeToKeep.add(graph.referenceRangeList().get(0));
        // HaplotypeGraph graph2 = new FilterGraphPlugin(null,false).refRanges(refRangeToKeep).filter(graph);
        System.out.println("Number of Nodes In graph:"+graph.numberOfNodes());

        GenomeSequence reference = GenomeSequenceBuilder.instance(referenceSequenceReal);

//        ForkJoinPool myThreadPool = ForkJoinPool.commonPool();
//        BlockingQueue<Future<RunMergeAndCluster>> myBlockingQueue = new LinkedBlockingQueue();

        int numThreads = Runtime.getRuntime().availableProcessors();
        System.out.println(numThreads);
        ExecutorService myThreadPool = Executors.newFixedThreadPool(numThreads);
//        ExecutorService myThreadPool = Executors.newFixedThreadPool(1);

//        ForkJoinPool myThreadPool = ForkJoinPool.commonPool();
//        BlockingQueue<Future<RunMergeAndCluster>> myBlockingQueue = new LinkedBlockingQueue();
        List<Future<RunMergeAndCluster>> futures = new ArrayList<>();
        for(ReferenceRange refRange : graph.referenceRangeList()) {
            List<Datum> datumList = new ArrayList<>();
            datumList.add(new Datum("PHG", graph, null));
            datumList.add(new Datum("Ref", reference,
                    null));
            datumList.add(new Datum("refRange", refRange, null));
            DataSet graphToMerge = new DataSet(datumList, null);
            System.out.println("Adding range:"+refRange.intervalString());
            try {
//                myBlockingQueue.add(myThreadPool.submit(new RunMergeAndCluster(graphToMerge, dbConfigReal, outputVCFDir, reference)));
                futures.add(myThreadPool.submit(new RunMergeAndCluster(graphToMerge, dbConfigReal, outputVCFDir, reference, referenceSequenceReal)));
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println(myThreadPool.getQueuedTaskCount());
//        for(Future currentFuture: myBlockingQueue) {
        for(Future currentFuture: futures) {
            try {
                currentFuture.get();
            }
            catch(Exception e) {
                e.printStackTrace();
            }
//            while(!currentFuture.isDone()) {
//                try {
//                    Thread.sleep(1000);
//                }
//                catch(Exception e) {
//                    e.printStackTrace();
//                }
//            }
        }
        myThreadPool.shutdown();
    }

    private class RunMergeAndCluster implements Callable {
        private final DataSet graphToMerge;
        private final String dbConfig;
        private final String outputVCFDir;
        private final GenomeSequence reference;
        private final String referenceFile;

        public RunMergeAndCluster(DataSet graphToMerge, String dbConfig, String outputVCFDir, GenomeSequence reference) {
            this.graphToMerge = graphToMerge;
            this.dbConfig = dbConfig;
            this.outputVCFDir = outputVCFDir;
            this.reference = reference;
            referenceFile = "";
        }

        public RunMergeAndCluster(DataSet graphToMerge, String dbConfig, String outputVCFDir, GenomeSequence reference, String referenceFile) {
            this.graphToMerge = graphToMerge;
            this.dbConfig = dbConfig;
            this.outputVCFDir = outputVCFDir;
            this.reference = reference;
            this.referenceFile = referenceFile;
        }


        @Override
        public Integer call() {
//            System.out.println("Running Merge");
            try {
//                System.out.println(((ReferenceRange)graphToMerge.getDataOfType(ReferenceRange.class).get(0).getData()).intervalString());
                DataSet ds = new MergeGVCFPlugin(null, false)
                        .dBConfig(dbConfig)
//                        .outputDir(outputVCFDir+"ProfilerTests/speedTest/debug/mergevcf5/")
//                        .haplotypeMethod("GATK_PIPELINE")
                        .referenceFile(referenceFile)
                        .performFunction(graphToMerge);
                //check the dataset to make sure its not empty
                if(ds.getDataSet().size()==0) {
                    System.out.println("Empty dataset "+((ReferenceRange) graphToMerge.getDataOfType(ReferenceRange.class).get(0).getData()).intervalString());
                    return 0;
                }
//                System.out.println("Running Cluster");
                DataSet mergedDS = new FindHaplotypeClustersPlugin(null, false)
                        .maxDistFromFounder(.001)
                        .outFile(outputVCFDir + "ProfilerTests/speedTest/debug/gvcfs5/")
                        .sequenceOutDir(outputVCFDir + "ProfilerTests/speedTest/debug/fastas5/")
                        .referenceSequence(reference)
                        .useDepthForCalls(false)
                        .replaceNsWithMajor(true)
                        .performFunction(ds);

                List<Datum> datumList = mergedDS.getDataOfType(GenotypeTable.class);

                ReferenceRange currentRefRange = (ReferenceRange) graphToMerge.getDataOfType(ReferenceRange.class).get(0).getData();
                List<List<VariantContext>> consensusSequences = mergedDS.getDataOfType(GenotypeTable.class)
                        .stream()
                        .map(datum -> (GenotypeTable)datum.getData())
                        .map(genotypeTable -> VCFUtil.convertGenotypeTableToVariantContextList(genotypeTable))
                        .collect(Collectors.toList());

//                List<HaplotypeNode> rawHapsWithVariants = (List<HaplotypeNode>)ds.getDataOfType(List.class).get(0).getData();
//                List<List<VariantContext>> rawVariants = rawHapsWithVariants.stream().map(haplotypeNode -> haplotypeNode.variantContexts())
//                        .filter(variantContexts -> variantContexts.isPresent())
//                        .map(variantContexts -> variantContexts.get())
//                        .filter(variantContexts -> variantContexts.size()>0)
//                        .collect(Collectors.toList());

//                List<List<VariantContext>> consensusSequenceWithIndels = FillIndelsIntoConsensus.addInIndels(currentRefRange,reference,consensusSequences,rawVariants,2, FillIndelsIntoConsensus.INDEL_MERGE_RULE.mostCommonHaplotype);



//                for(Datum currentDatum : datumList) {
//                    GenotypeTable gt = (GenotypeTable)currentDatum.getData();
//                    List<VariantContext> variants = VCFUtil.convertGenotypeTableToVariantContextList(gt);
//
//                    ReferenceRange refRange = ((ReferenceRange) graphToMerge.getDataOfType(ReferenceRange.class).get(0).getData());
//                    HapCallingUtils.writeVariantContextsToVCF(variants,outputVCFDir + "ProfilerTests/speedTest/debug/gvcfs3/outputMergedVCF_chr" + refRange.chromosome().getName() + "_stPos" + refRange.start() + "_Consensus_"+gt.taxaName(0).substring(0,5)+".vcf",null,gt.taxa().stream().map(taxon -> taxon.getName()).collect(Collectors.toList()));
//                    String fastaSequence = ExtractFastaUtils.extractFastaSequence(variants,refRange);
//                    System.out.println(refRange.intervalString()+"     "+fastaSequence);
//                }

//                GenotypeTable gt = (GenotypeTable)mergedDS.getDataOfType(GenotypeTable.class).get(0).getData();
//
//                //try to convert the gt back to list<VarContext>
//                VCFUtil.convertGenotypeTableToVariantContextList(gt);

            }
            catch(Exception e) {
                e.printStackTrace();
            }

            return 0;
        }
    }

    private SortedSet<Integer> readHapIds() {
        TreeSet<Integer> hapIds = new TreeSet<>();
        try(BufferedReader reader = Utils.getBufferedReader("/Users/zrm22/Desktop/sampleHapIds2k.tsv")) {
//        try(BufferedReader reader = Utils.getBufferedReader("/Users/zrm22/Desktop/sampleHapIds")) {

            String currentString = "";
            while((currentString = reader.readLine())!=null) {
                hapIds.add(Integer.parseInt(currentString));
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return hapIds;
    }
}
