package net.maizegenetics.pangenome.hapCalling;

import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import com.google.common.collect.TreeMultiset;
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin;
import net.maizegenetics.pangenome.api.HaplotypeNode;
import net.maizegenetics.pangenome.api.ReferenceRange;
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.util.Utils;
import org.junit.Test;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by zrm22 on 10/17/17.
 */
public class ImportHaplotypePathFilePluginTest {

    String userHome = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/temp/hapcalling/";
    // String dbFile = userHome + "/sept_18_2017_308haps_306ConsensusWithAssemblies.db";
    String configFile = userHome + "configFile.txt";

    String outputFile = userHome + "/rAmpSeq/PathDirectories/intersectionOfRefRangesNewHapIdMappingPedigree.txt";


    /**
     * Method to compute the Intersection of all ref ranges for a set of path files.  We should likely be somewhere
     * else.  Maybe a utility.
     */
    @Test
    public void computeIntersectionRefRanges() {
        ArrayList<String> directoriesForHapPaths = new ArrayList<>();
        //Add path directories
//        directoriesForHapPaths.add("/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/temp/hapcalling/rAmpSeq/PathDirectories/CK17GSBY_16577Paths/");
//        directoriesForHapPaths.add("/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/temp/hapcalling/rAmpSeq/PathDirectories/CK17GSBYMBYPaths/");
//        directoriesForHapPaths.add("/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/temp/hapcalling/rAmpSeq/PathDirectories/maize_rAmpSeq-first32platesPaths/");


        //New paths
        directoriesForHapPaths.add("/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/temp/hapcalling/rAmpSeq/VCFFiles/FilteredByRefRangesZackMappingPedigree/Paths/CK17GSBY_16557/");
        directoriesForHapPaths.add("/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/temp/hapcalling/rAmpSeq/VCFFiles/FilteredByRefRangesZackMappingPedigree/Paths/CK17GSBYMBY/");
        directoriesForHapPaths.add("/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/temp/hapcalling/rAmpSeq/VCFFiles/FilteredByRefRangesZackMappingPedigree/Paths/maize_rAmpSeq-first32plates/");

        System.out.println("Building graph");
        DataSet graphDataSet = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("collapse_method_1," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP)
                .includeVariantContexts(false)
                .performFunction(null);


        Multiset<ReferenceRange> refRangeCounts = TreeMultiset.create();


        try (BufferedWriter writer = Utils.getBufferedWriter(outputFile)) {
            for (String currentDirectory : directoriesForHapPaths) {
                System.out.println("Reading in:" + currentDirectory);
                DataSet currentResultFromImport = new ImportHaplotypePathFilePlugin(null, false)
                        .inputFileDirectory(currentDirectory)
                        .performFunction(graphDataSet);

                System.out.println("Converting nodes to refRanges:");
                //load up the haplotype paths
                Multimap<String, HaplotypeNode> haplotypePaths = (Multimap<String, HaplotypeNode>) currentResultFromImport.getDataOfType(Multimap.class).get(0).getData();

                //Use a set to get the unique reference ranges
                SortedSet<ReferenceRange> uniqueRefRanges = haplotypePaths.keySet().stream().flatMap(taxonName -> haplotypePaths.get(taxonName).stream())
                        .map(haplotypeNode -> haplotypeNode.referenceRange())
                        .collect(TreeSet::new, (set, refRange) -> set.add(refRange), (leftSet, rightSet) -> leftSet.addAll(rightSet));
                System.out.println("Adding set to refRangeCounts, size:" + uniqueRefRanges.size());
                //Add them to the count multiset
                refRangeCounts.addAll(uniqueRefRanges);
            }

            //loop through all the refRangeCounts and if it equals number of directories we want to export
            for (ReferenceRange refRange : refRangeCounts.elementSet()) {
                if (refRangeCounts.count(refRange) == directoriesForHapPaths.size()) {
                    //export the id and interval string
                    writer.write(refRange.id() + "\t" + refRange.intervalString() + "\n");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
