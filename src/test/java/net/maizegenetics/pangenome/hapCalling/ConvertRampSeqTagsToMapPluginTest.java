package net.maizegenetics.pangenome.hapCalling;

import com.google.common.collect.Multimap;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.vcf.VCFFileReader;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.pangenome.api.HaplotypeGraph;
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin;
import net.maizegenetics.pangenome.api.HaplotypeNode;
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.Taxon;
import net.maizegenetics.util.Tuple;
import net.maizegenetics.util.Utils;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by zrm22 on 10/4/17. This class holds simple methods to run the rampseq to vcf pipeline.  Does not actually
 * verify anything as a normal test.
 */
@Deprecated
public class ConvertRampSeqTagsToMapPluginTest {

    //TODO change user home to run the tests
    //All files are stored in cbsublfs1://data1/PHG/rAmpSeq_files/UnitTestFiles/
    String userHome = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/temp/hapcalling/";
    // String dbFile = userHome + "/sept_18_2017_308haps_306ConsensusWithAssemblies.db";
    String configFile = userHome + "configFile.txt";

    //Input files for the plugin
    String tagToHapIDMapFile = userHome + "rAmpSeq/DansFiles/oct4NewFiles/tags_nodes_taxa_chisq/tag_node-all_chisq-sort_by_node.tsv";
    String tagToHapIDMapFilteredFile = userHome + "rAmpSeq/DansFiles/oct4NewFiles/tags_nodes_taxa_chisq/tag_node-all_chisq-sort_by_nodeFilteredBestChiSq.tsv";
    String tagToHapIDMapFilteredFileTaxon = userHome + "rAmpSeq/DansFiles/oct4NewFiles/tags_nodes_taxa_chisq/tag_node-all_chisq-sort_by_nodeFilteredBestChiSqTaxon.tsv";
    String tagToHapIDMapFilteredFileTaxon9K = userHome + "rAmpSeq/DansFiles/oct4NewFiles/tags_nodes_taxa_chisq/tag_node-all_chisq-sort_by_nodeFilteredBestChiSqTaxon9KTaxon.tsv";


    String taxaToTagMapping = userHome + "rAmpSeq/DansFiles/oct4NewFiles/maize_pilot_subsample_deepseq-no_NAM-seq_depth_filtered-seq_filtered-tag_id.tsv";

    //VCF file used for the output
    String vcfFile = userHome + "rAmpSeq/DansFiles/pilotStudy_rAmpSeq.vcf";
    String vcfFileFilteredTaxon = userHome + "rAmpSeq/DansFiles/pilotStudy_rAmpSeqFilteredTaxon.vcf";

    //Test VCF file filtered to just have W22
    String filteredVCFFile = userHome + "rAmpSeq/DansFiles/outputVCFFiltered.vcf";
    //GBS VCF file to get a comparison of rAmpSeq to our GBS pipeline
    String gbsVCF = userHome + "HapCallingTestFiles/TempExports/apeKIBestPathAllCountsSelectTaxa.vcf";
    //Axiom VCF file to check error rate of calling known SNPs
    String axiomVCF = "/Volumes/ZackBackup/Temp/Pangenome/EdHackathonUnitTests/TUM8Lines_Maize600k_elitelines_AGPv4_Tasselsorted.vcf";


    String refRangeFileWeHaveTagsFor = userHome + "rAmpSeq/DansFiles/refRangeIdsOnlyCertainTaxa.txt";


    /**
     * Method Testing mapping the taxon to HaplotypeNode.  A similar process is done to extract paths through the graph
     */
    @Test
    public void testHapPathMapping() {
        System.out.println("Building graph");
        DataSet graphDataSet = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("collapse_method_1," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP)
                .performFunction(null);

        System.out.println("Graph Built\nConvertingTags");
        ConvertRampSeqTagsToMapPlugin convertTagsPlugin = new ConvertRampSeqTagsToMapPlugin(null, false)
                .tagToHapIdMapFile(tagToHapIDMapFile)
                .tagCountsByTaxaFile(taxaToTagMapping);
        System.out.println("Tags converted: Analyzing result");
        Multimap<String, HaplotypeNode> tagMappingResult = (Multimap<String, HaplotypeNode>) convertTagsPlugin.processData(graphDataSet).getData(0).getData();

        System.out.println(tagMappingResult.keys());
        Collection<HaplotypeNode> nodes = tagMappingResult.get("A239");

        //TODO formalize this test
        for (HaplotypeNode node : nodes) {
            System.out.println("ID: " + node.id() + "\t" + node.referenceRange().intervalString());
        }
    }


    /**
     * Simple test to create a VCF file for all taxon in the test
     */
    @Test
    public void testHapPathMappingToVCF() {
        System.out.println("Building graph");
        DataSet graphDataSet = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("collapse_method_1," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP)
                .includeVariantContexts(true)
                .performFunction(null);

        System.out.println("Graph Built\nConvertingTags");
        ConvertRampSeqTagsToMapPlugin convertTagsPlugin = new ConvertRampSeqTagsToMapPlugin(null, false)
//                .tagToHapIdMapFile(tagToHapIDMapFile) //change this file
                .tagToHapIdMapFile(tagToHapIDMapFilteredFile) //change this file
                .tagCountsByTaxaFile(taxaToTagMapping); //change this file
        System.out.println("Tags converted: Analyzing result");
        Multimap<String, HaplotypeNode> tagMappingResult = (Multimap<String, HaplotypeNode>) convertTagsPlugin.processData(graphDataSet).getData(0).getData();
        System.out.println("Processed data: printing out W22 nodes");
        System.out.println(tagMappingResult.keySet().stream().collect(Collectors.joining(",")));


        for (String taxon : tagMappingResult.keySet()) {
            System.out.println(taxon + "\t" + tagMappingResult.get(taxon).size());
        }


        // Get the W22 nodes and print them out for debugging
        Collection<HaplotypeNode> nodes = tagMappingResult.get("W22-ss");

        for (HaplotypeNode node : nodes) {
            System.out.println("ID: " + node.id() + "\t" + node.referenceRange().intervalString());
        }


        //Set up the input into the PathsToVCFPlugin and create a VCF file
        List<Datum> listOfInputData = new ArrayList<>();

        listOfInputData.add(new Datum("mulitmap", tagMappingResult, "multimap"));
        listOfInputData.add(new Datum("graph", (HaplotypeGraph) graphDataSet.getData(0).getData(), "graph"));

        DataSet inputToVCFDataset = new DataSet(listOfInputData, null);

        PathsToVCFPlugin pathsToVCFPlugin = new PathsToVCFPlugin(null, false)
                .outputFile(vcfFile);

        pathsToVCFPlugin.processData(inputToVCFDataset);


    }

    @Test
    public void testHapPathMappingToVCFTaxon() {
        System.out.println("Building graph");
        DataSet graphDataSet = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("collapse_method_1,\" + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP")
                .includeVariantContexts(true)
                .performFunction(null);

        System.out.println("Graph Built\nConvertingTags");
        ConvertRampSeqTagsToMapPlugin convertTagsPlugin = new ConvertRampSeqTagsToMapPlugin(null, false)
//                .tagToHapIdMapFile(tagToHapIDMapFile) //change this file
                .tagToHapIdMapFile(tagToHapIDMapFilteredFileTaxon) //change this file
                .tagCountsByTaxaFile(taxaToTagMapping); //change this file
        System.out.println("Tags converted: Analyzing result");
        Multimap<String, HaplotypeNode> tagMappingResult = (Multimap<String, HaplotypeNode>) convertTagsPlugin.processData(graphDataSet).getData(0).getData();
        System.out.println("Processed data: printing out W22 nodes");
        System.out.println(tagMappingResult.keySet().stream().collect(Collectors.joining(",")));


        for (String taxon : tagMappingResult.keySet()) {
            System.out.println(taxon + "\t" + tagMappingResult.get(taxon).size());
        }


        // Get the W22 nodes and print them out for debugging
        Collection<HaplotypeNode> nodes = tagMappingResult.get("W22-ss");

        for (HaplotypeNode node : nodes) {
            System.out.println("ID: " + node.id() + "\t" + node.referenceRange().intervalString());
        }


        //Set up the input into the PathsToVCFPlugin and create a VCF file
        List<Datum> listOfInputData = new ArrayList<>();

        listOfInputData.add(new Datum("mulitmap", tagMappingResult, "multimap"));
        listOfInputData.add(new Datum("graph", (HaplotypeGraph) graphDataSet.getData(0).getData(), "graph"));

        DataSet inputToVCFDataset = new DataSet(listOfInputData, null);

        PathsToVCFPlugin pathsToVCFPlugin = new PathsToVCFPlugin(null, false)
                .outputFile(vcfFileFilteredTaxon);

        pathsToVCFPlugin.processData(inputToVCFDataset);


    }

    /**
     * Method to clean up the chi squared tag->hapId mapping.  For each tag this just selects the node with the highest
     * chi squared values
     */
    @Test
    public void cleanUpChiSqTest() {
        //Method to only keep the tag->hapId mapping that has the highest chisq
        Map<String, Integer> tagToHapIdMap = new HashMap<>();
        Map<String, Double> tagToChiSqMap = new HashMap<>();

        try (BufferedReader reader = Utils.getBufferedReader(tagToHapIDMapFile);
             BufferedWriter writer = Utils.getBufferedWriter(tagToHapIDMapFilteredFile)) {
            String currentLine = reader.readLine();//Skip the header
            while ((currentLine = reader.readLine()) != null) {
                int firstTabIndex = currentLine.indexOf("\t");
                int secondTabIndex = currentLine.indexOf("\t", firstTabIndex + 1);
                int thirdTabIndex = currentLine.indexOf("\t", secondTabIndex + 1);

                String tag = currentLine.substring(0, firstTabIndex);
                int hapId = Integer.parseInt(currentLine.substring(firstTabIndex + 1, secondTabIndex));
                double chiSquared = Double.parseDouble(currentLine.substring(secondTabIndex, thirdTabIndex));

                //Check to make sure we have an entry for this tag already
                if (tagToChiSqMap.get(tag) == null) {
                    tagToChiSqMap.put(tag, 0.0);
                }
                if (chiSquared > tagToChiSqMap.get(tag)) {
                    //Take the first 2 elements which are the tag and the hapid
                    tagToHapIdMap.put(tag, hapId);
                    tagToChiSqMap.put(tag, chiSquared);
                }

            }

            writer.write("Tag\tNode\tChiSq\n");
            for (String tag : tagToHapIdMap.keySet()) {
                writer.write(tag + "\t" + tagToHapIdMap.get(tag) + "\t" + tagToChiSqMap.get(tag) + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void cleanUpChiSqTestOnlyHighCoverageRefRanges() {
        //Load in the reference range file for the references we want to keep
        System.out.println("refRangeIds");
        Set<Integer> refRangeIdsToKeep = parseRefRangeMap();
        System.out.println("taxonNames");
        Set<String> taxonSet = getTaxonNames();
        System.out.println("Graph");
        //Load the haplotype graph so we can filter out any reference ranges which are not in our set
        DataSet graphDataSet = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("collapse_method_1," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP)
                .performFunction(null);
        HaplotypeGraph graph = (HaplotypeGraph) graphDataSet.getData(0).getData();

        Map<Integer, HaplotypeNode> nodeIdToNodeMap = graph.nodeStream()
                .filter(haplotypeNode -> refRangeIdsToKeep.contains(haplotypeNode.referenceRange().id()))
                .collect(Collectors.toMap(node -> node.id(), node -> node));


        //Method to only keep the tag->hapId mapping that has the highest chisq
        Map<String, Integer> tagToHapIdMap = new HashMap<>();
        Map<String, Double> tagToChiSqMap = new HashMap<>();

        try (BufferedReader reader = Utils.getBufferedReader(tagToHapIDMapFile);
             BufferedWriter writer = Utils.getBufferedWriter(tagToHapIDMapFilteredFileTaxon)) {
            String currentLine = reader.readLine();//Skip the header
            while ((currentLine = reader.readLine()) != null) {
                int firstTabIndex = currentLine.indexOf("\t");
                int secondTabIndex = currentLine.indexOf("\t", firstTabIndex + 1);
                int thirdTabIndex = currentLine.indexOf("\t", secondTabIndex + 1);

                String tag = currentLine.substring(0, firstTabIndex);
                int hapId = Integer.parseInt(currentLine.substring(firstTabIndex + 1, secondTabIndex));
                double chiSquared = Double.parseDouble(currentLine.substring(secondTabIndex, thirdTabIndex));

                //Check to make sure we have an entry for this tag already
                if (tagToChiSqMap.get(tag) == null) {
                    tagToChiSqMap.put(tag, 0.0);
                }
                if (nodeIdToNodeMap.get(hapId) != null) {
                    if (chiSquared > tagToChiSqMap.get(tag) && checkTaxon(taxonSet, nodeIdToNodeMap.get(hapId).taxaList())) {
                        //Take the first 2 elements which are the tag and the hapid
                        tagToHapIdMap.put(tag, hapId);
                        tagToChiSqMap.put(tag, chiSquared);
                    }
                }
            }

            writer.write("Tag\tNode\tChiSq\n");
            for (String tag : tagToHapIdMap.keySet()) {
                writer.write(tag + "\t" + tagToHapIdMap.get(tag) + "\t" + tagToChiSqMap.get(tag) + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void cleanUpChiSqTestOnlyHighCoverageRefRanges9K() {
        //Load in the reference range file for the references we want to keep
        System.out.println("refRangeIds");
        Set<Integer> refRangeIdsToKeep = parseRefRangeMap();
        System.out.println("taxonNames");
        Set<String> taxonSet = getTaxonNames9K();
        System.out.println("Graph");
        //Load the haplotype graph so we can filter out any reference ranges which are not in our set
        DataSet graphDataSet = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("collapse_method_1," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP)
                .performFunction(null);
        HaplotypeGraph graph = (HaplotypeGraph) graphDataSet.getData(0).getData();

        Map<Integer, HaplotypeNode> nodeIdToNodeMap = graph.nodeStream()
                .filter(haplotypeNode -> refRangeIdsToKeep.contains(haplotypeNode.referenceRange().id()))
                .collect(Collectors.toMap(node -> node.id(), node -> node));


        //Method to only keep the tag->hapId mapping that has the highest chisq
        Map<String, Integer> tagToHapIdMap = new HashMap<>();
        Map<String, Double> tagToChiSqMap = new HashMap<>();

        try (BufferedReader reader = Utils.getBufferedReader(tagToHapIDMapFile);
             BufferedWriter writer = Utils.getBufferedWriter(tagToHapIDMapFilteredFileTaxon9K)) {
            String currentLine = reader.readLine();//Skip the header
            while ((currentLine = reader.readLine()) != null) {
                int firstTabIndex = currentLine.indexOf("\t");
                int secondTabIndex = currentLine.indexOf("\t", firstTabIndex + 1);
                int thirdTabIndex = currentLine.indexOf("\t", secondTabIndex + 1);

                String tag = currentLine.substring(0, firstTabIndex);
                int hapId = Integer.parseInt(currentLine.substring(firstTabIndex + 1, secondTabIndex));
                double chiSquared = Double.parseDouble(currentLine.substring(secondTabIndex, thirdTabIndex));

                //Check to make sure we have an entry for this tag already
                if (tagToChiSqMap.get(tag) == null) {
                    tagToChiSqMap.put(tag, 0.0);
                }
                if (nodeIdToNodeMap.get(hapId) != null) {
                    if (chiSquared > tagToChiSqMap.get(tag) && checkTaxon(taxonSet, nodeIdToNodeMap.get(hapId).taxaList())) {
                        //Take the first 2 elements which are the tag and the hapid
                        tagToHapIdMap.put(tag, hapId);
                        tagToChiSqMap.put(tag, chiSquared);
                    }
                }
            }

            writer.write("Tag\tRefRange\tNode\tChiSq\n");
            for (String tag : tagToHapIdMap.keySet()) {
                writer.write(tag + "\tnull\t" + tagToHapIdMap.get(tag) + "\t" + tagToChiSqMap.get(tag) + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void extractRAmpSeqRefRanges() {
        System.out.println("Getting graph");
        //Load the haplotype graph so we can filter out any reference ranges which are not in our set
        DataSet graphDataSet = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("collapse_method_1," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP)
                .includeVariantContexts(false)
                .performFunction(null);
        HaplotypeGraph graph = (HaplotypeGraph) graphDataSet.getData(0).getData();


        System.out.println("Graph Built\nConvertingTags");
        ConvertRampSeqTagsToMapPlugin convertTagsPlugin = new ConvertRampSeqTagsToMapPlugin(null, false)
//                .tagToHapIdMapFile(tagToHapIDMapFile) //change this file
                .tagToHapIdMapFile(tagToHapIDMapFilteredFileTaxon) //change this file
                .tagCountsByTaxaFile(taxaToTagMapping); //change this file
        System.out.println("Tags converted: Analyzing result");
        Multimap<String, HaplotypeNode> tagMappingResult = (Multimap<String, HaplotypeNode>) convertTagsPlugin.processData(graphDataSet).getData(0).getData();


        Set<Integer> refRangeIds = tagMappingResult.keySet().stream().flatMap(taxon -> tagMappingResult.get(taxon).stream()).map(haplotypeNode -> haplotypeNode.referenceRange().id()).distinct().collect(Collectors.toSet());


//        //read in the known refRanges
//        Set<Integer> refRanges = parseRefRangeMap();
//        //read in the tagToHapId map file
//        SortedSet<Integer> hapIds = parseTagToHapIdFile();
//
//        List<HaplotypeNode> nodes = GraphUtils.nodes(graph,hapIds);
//
//        Set<Integer> refRangeIds = nodes.stream().map(currentNode -> currentNode.referenceRange().id()).distinct().filter(refId -> refRanges.contains(refId)).sorted().collect(Collectors.toSet());
        try (BufferedWriter writer = Utils.getBufferedWriter(refRangeFileWeHaveTagsFor)) {
            writer.write("RefRangeId\n");
            for (Integer refId : refRangeIds) {
                writer.write(refId + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkTaxon(Set<String> taxonSet, TaxaList nodeTaxon) {
        for (Taxon taxon : nodeTaxon) {
            if (taxonSet.contains(taxon.getName())) {
                return true;
            }
        }
        return false;
    }

    private Set<Integer> parseRefRangeMap() {
        Set<Integer> refRangeIdsToKeep = new HashSet<Integer>();

        try (BufferedReader reader = Utils.getBufferedReader("./data/HapCallingTestFiles/AllTaxaAveraged_Evaluation_FilteredRefRanges.txt")) {
            String currentLine = reader.readLine();
            while ((currentLine = reader.readLine()) != null) {
                String[] currentLineSplit = currentLine.split("\t");
                refRangeIdsToKeep.add(Integer.parseInt(currentLineSplit[0]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return refRangeIdsToKeep;
    }

    private SortedSet<Integer> parseTagToHapIdFile() {
        SortedSet<Integer> parsedHapId = new TreeSet<>();

        try (BufferedReader reader = Utils.getBufferedReader(tagToHapIDMapFilteredFileTaxon)) {
            String currentLine = reader.readLine();
            while ((currentLine = reader.readLine()) != null) {
                int firstTabIndex = currentLine.indexOf("\t");
                int secondTabIndex = currentLine.indexOf("\t", firstTabIndex + 1);

                parsedHapId.add(Integer.parseInt(currentLine.substring(firstTabIndex + 1, secondTabIndex)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return parsedHapId;
    }

    private Set<String> getTaxonNames() {
        Set<String> taxonNames = new HashSet<>();
        taxonNames.add("B97");
        taxonNames.add("Ky21");
        taxonNames.add("M162W");
        taxonNames.add("Mo17");
        taxonNames.add("Ms71");
        taxonNames.add("OH43");
        taxonNames.add("OH7B");
        taxonNames.add("W22");

        return taxonNames;
    }


    private Set<String> getTaxonNames9K() {
        Set<String> taxonNames = new HashSet<>();

        taxonNames.add("CML264");
        taxonNames.add("CML495");
        taxonNames.add("CML451");
        taxonNames.add("CML551");
        taxonNames.add("CML577");
        taxonNames.add("MAIdgiRCCDIAAPEI-10");
        taxonNames.add("CML549");
        taxonNames.add("CML481");
        taxonNames.add("CML486");
        taxonNames.add("CML517");
        taxonNames.add("CML520");
        taxonNames.add("CML550");
        taxonNames.add("CML516");
        taxonNames.add("MAIdgiRARDIAAPEI-1");
        taxonNames.add("CML515");
        taxonNames.add("CML277");
        taxonNames.add("CML553");
        taxonNames.add("CML552");
        taxonNames.add("ZEAxppRALDIAAPEI-9");
        taxonNames.add("CML494");
        taxonNames.add("DTMA-182");
        taxonNames.add("CML491");

        return taxonNames;
    }


    /**
     * Test to
     */
    @Test
    public void compareRampSeqToGBS() {
        String taxonName = "W22";
        //Read in the GBS file and make maps mapping the position to its variant context
        Map<Position, VariantContext> gbsSNPs = parseVCFToMap(gbsVCF);
        Map<Position, VariantContext> rAmpSeqSNPs = parseVCFToMap(filteredVCFFile);


        int sumIntersectionPositions = 0;
        int numberOfMistakes = 0;
        for (Position rampSeqPosition : rAmpSeqSNPs.keySet()) {
            VariantContext currentGBSSnp = gbsSNPs.get(rampSeqPosition);

            if (currentGBSSnp == null) {
                //snp position not in gbs
            } else {

                //Check to see if the SNP is Missing(N/N or ./.) or is a het
                if (!currentGBSSnp.getGenotype(taxonName).getGenotypeString().equals("N/N") &&
                        !currentGBSSnp.getGenotype(taxonName).getGenotypeString().equals("./.") &&
                        !currentGBSSnp.getGenotype(taxonName).isHet() &&
                        !rAmpSeqSNPs.get(rampSeqPosition).getGenotype(taxonName).getGenotypeString().equals("N/N") &&
                        !rAmpSeqSNPs.get(rampSeqPosition).getGenotype(taxonName).getGenotypeString().equals("./.") &&
                        !rAmpSeqSNPs.get(rampSeqPosition).getGenotype(taxonName).isHet()) {

                    //Increment the number of intersecting positions(how many SNPs do the two VCFs share
                    sumIntersectionPositions++;

                    if (!currentGBSSnp.getGenotype(taxonName).getGenotypeString().equals(rAmpSeqSNPs.get(rampSeqPosition).getGenotype(taxonName).getGenotypeString())) {
                        //Increment the number of errors/ print them out to double check
                        numberOfMistakes++;
                        System.out.println(rampSeqPosition.getChromosome().toString() + "\t" + rampSeqPosition.getPosition() + "\t" + currentGBSSnp.getGenotype(taxonName).getGenotypeString() + "\t" + rAmpSeqSNPs.get(rampSeqPosition).getGenotype(taxonName).getGenotypeString());
                    }
                }
            }
        }

        System.out.println("Total Number of SNPs:" + sumIntersectionPositions);
        System.out.println("Number of Mistakes:" + numberOfMistakes);

    }

    @Test
    public void compareRampSeqToAxiom() {
        String taxonName = "W22";
        //Read in the GBS file and make maps mapping the position to its variant context
        Map<Position, VariantContext> axiomSNPs = parseVCFToMap(axiomVCF);
        Map<Position, VariantContext> rAmpSeqSNPs = parseVCFToMap(filteredVCFFile);

        List<String> chromosomes = new ArrayList<>(Arrays.asList("2", "3", "4", "7", "8", "9"));

        int sumIntersectionPositions = 0;
        int numberOfMistakes = 0;
        for (Position rampSeqPosition : rAmpSeqSNPs.keySet()) {
            VariantContext currentAxiomSnp = axiomSNPs.get(rampSeqPosition);

            if (currentAxiomSnp == null) {
                //snp position not in axiom
            } else if (!chromosomes.contains(currentAxiomSnp.getContig())) {
                //filter out any positions in the chromsomes we do not trust
            } else {

                if (!currentAxiomSnp.getGenotype(taxonName).getGenotypeString().equals("N/N") &&
                        !currentAxiomSnp.getGenotype(taxonName).getGenotypeString().equals("./.") &&
                        !currentAxiomSnp.getGenotype(taxonName).isHet() &&
                        !rAmpSeqSNPs.get(rampSeqPosition).getGenotype(taxonName).getGenotypeString().equals("N/N") &&
                        !rAmpSeqSNPs.get(rampSeqPosition).getGenotype(taxonName).getGenotypeString().equals("./.") &&
                        !rAmpSeqSNPs.get(rampSeqPosition).getGenotype(taxonName).isHet()) {

                    //Increment the number of intersecting positions
                    sumIntersectionPositions++;
                    if (!currentAxiomSnp.getGenotype(taxonName).getGenotypeString().equals(rAmpSeqSNPs.get(rampSeqPosition).getGenotype(taxonName).getGenotypeString()) &&
                            !getCompliment(currentAxiomSnp.getGenotype(taxonName).getGenotypeString()).equals(rAmpSeqSNPs.get(rampSeqPosition).getGenotype(taxonName).getGenotypeString())) {
                        //Increment the number of mistakes
                        numberOfMistakes++;
                        System.out.println(rampSeqPosition.getChromosome().toString() + "\t" + rampSeqPosition.getPosition() + "\t" + currentAxiomSnp.getGenotype(taxonName).getGenotypeString() + "\t" + rAmpSeqSNPs.get(rampSeqPosition).getGenotype(taxonName).getGenotypeString());
                    }
                }
            }
        }

        System.out.println("Total Number of SNPs:" + sumIntersectionPositions);
        System.out.println("Number of Mistakes:" + numberOfMistakes);

    }

    /**
     * Helper function to fix the compliment issue caused by the Axiom SNP
     *
     * @param genotype
     *
     * @return
     */
    private String getCompliment(String genotype) {
        if (genotype.equals("A/A")) {
            return "T/T";
        } else if (genotype.equals("T/T")) {
            return "A/A";
        } else if (genotype.equals("C/C")) {
            return "G/G";
        } else {
            return "C/C";
        }
    }

    /**
     * Simple method which will parse a VCF file into a Position-> VariantContext mapping for easy look up of contexts
     * for a given position
     *
     * @param vcfFile
     *
     * @return
     */
    private Map<Position, VariantContext> parseVCFToMap(String vcfFile) {
        VCFFileReader vcfReader = new VCFFileReader(new File(vcfFile), false);

        Map<Position, VariantContext> variants = vcfReader.iterator().stream().map(variantContext -> new Tuple<>(Position.of(variantContext.getContig(), variantContext.getStart()), variantContext)).collect(Collectors.toMap(tuple -> tuple.getX(), tuple -> tuple.getY()));

        return variants;
    }
}
