package net.maizegenetics.pangenome.hapCalling;

import static net.maizegenetics.pangenome.smallseq.SmallSeqPaths.dbConfigFile;
import static net.maizegenetics.pangenome.smallseq.SmallSeqPaths.outputDir;

import org.junit.Test;

import net.maizegenetics.pangenome.api.HaplotypeGraph;
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin;
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.util.LoggingUtils;

@Deprecated
public class FastqKmerToHapCountPluginTest {
    String kmerFile = outputDir + "kmerMap.bin";
    final String countMethodName = "FastqKmer_To_Hapcount";
    
    @Test
    public void testPlugin() {
        //this test is setup to use the database created by RunSmallSeqDockerTest
        //but does not run inside a docker
        //dbConfigFile will have to be modified for the getGraph test to work (see getGraph comment)
        //run IndexHaplotypeKmersPluginTest.testIndexingMethod before running this test to create the kmerFile
        
        LoggingUtils.setupDebugLogging();
//        fail("Not yet implemented");
        
        System.out.println("Building graph");
        DataSet graphDataSet = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(dbConfigFile)
                .methods("CONSENSUS,*")
                .includeVariantContexts(false)
                .performFunction(null);

        HaplotypeGraph graph = (HaplotypeGraph) graphDataSet.getData(0).getData();
        System.out.println("Running FastqKmerToHapCountPlugin");

        new FastqKmerToHapCountPlugin()
                .configFile(dbConfigFile)
                .kmerFile(kmerFile)
                .readDir(SmallSeqPaths.dataDir)
                .method(countMethodName)
                .loadDb(true)
                .performFunction(DataSet.getDataSet(graph));
        
        new HapCountBestPathToTextPlugin(null, false)
                .configFile(dbConfigFile)
                .outputDir(outputDir)
                .hapCountMethod(countMethodName)
                .pathMethod("FastqKmer")
                .performFunction(DataSet.getDataSet(graph));
        
        getGraph();
    }

    public void getGraph() {
        //to get this to work add # HapCountBestPathToTextPlugin parameters section
        //from dbDockerConfigFile to dbConfigFile
        //otherwise the graph does not get filtered properly
        LoggingUtils.setupDebugLogging();
        DataSet graphDataSet = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(dbConfigFile)
                .methods("CONSENSUS,*")
                .includeVariantContexts(false)
                .performFunction(null);

        String pathTaxonName = "LineB_R1";
        String targetTaxonName = "LineB";
        new PathToIgraphPlugin(null, false)
                .configFile(dbConfigFile)
                .outputBase(SmallSeqPaths.dataDir + pathTaxonName)
                .countMethod(countMethodName)
                .taxonName(pathTaxonName)
                .targetName(targetTaxonName)
                .performFunction(graphDataSet);
    }
}
