package net.maizegenetics.pangenome.hapCalling;

import com.google.common.collect.ImmutableRangeSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Multiset;
import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.RangeSet;
import com.google.common.collect.TreeMultiset;
import com.google.common.collect.TreeRangeSet;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.vcf.VCFFileReader;
import net.maizegenetics.dna.map.GeneralPosition;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.ImportUtils;
import net.maizegenetics.dna.snp.NucleotideAlignmentConstants;
import net.maizegenetics.pangenome.api.HaplotypeGraph;
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin;
import net.maizegenetics.pangenome.api.HaplotypeNode;
import net.maizegenetics.pangenome.api.ReferenceRangeEmissionProbability;
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import net.maizegenetics.pangenome.pipelineTests.SimpleGVCFReader;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.util.Tuple;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static net.maizegenetics.dna.snp.GenotypeTable.UNKNOWN_DIPLOID_ALLELE;
import static net.maizegenetics.dna.snp.NucleotideAlignmentConstants.parseNucleotideDiploidByte;

/**
 * Created by zrm22 on 9/26/17.
 */
public class HapCountBestPathPluginTest {

    //    Zack Testing file path
    private static final String userHome = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles";

    //New DB
    // static final String dbPath = userHome + "/temp/hapcalling/sept_18_2017_308haps_306ConsensusWithAssemblies.db";
    static final String configFile = userHome + "/temp/hapcalling/configFile.txt";

    static final String taxon = "W22";
    static final String apeKIInclusionFile = userHome + "/temp/hapcalling/RamuFiles/InputCounts/W22:2028_D0E3PACXX_2_CGCTGAT.txt";
    static final String pstIInclusionFile = userHome + "/temp/hapcalling/RamuFiles/InputCounts/B73W22Cross_W22-Brink-std_D0DW9ACXX_4_CCACTCA.txt";
    static final String nonConsensusRawGVCFFile = userHome + "/temp/hapcalling/HapCallingTestFiles/W22_haplotype_caller_output_filtered.g.vcf";
    static final String nonConsensusRawGVCFFilePreFilter = userHome + "/temp/hapcalling/HapCallingTestFiles/ActualW22/W22_haplotype_caller_output.g.vcf";


    //    static final String refRangeFile = userHome + "/temp/hapcalling/RamuFiles/OutputCounts/AllTaxaAveraged_Evaluation_FilteredRefRanges.txt";
//    static final String refRangeFile ="data/HapCallingTestFiles/AllTaxaAveraged_Evaluation_FilteredRefRanges.txt";
    static final String refRangeFile = "data/HapCallingTestFiles/AllTaxaAveraged_Evaluation_FilteredRefRanges_InfoScoreReversed.txt";

    static final String axiomVCF = "/Volumes/ZackBackup/Temp/Pangenome/EdHackathonUnitTests/TUM8Lines_Maize600k_elitelines_AGPv4_Tasselsorted.vcf.gz";
    static final String referenceFile = "/Volumes/ZackBackup/Temp/Pangenome/EdHackathonUnitTests/Zea_mays.AGPv4.dna.toplevel.fa.gz";

    static final String exportTempDirectory = userHome + "/temp/hapcalling/HapCallingTestFiles/TempExports/";

    static Map<Position, VariantContext> axiomContexts;

    static Map<Position, String> axiomPositionToGenotypeMapping;

    static GenotypeTable axiomGenotypeTable;

    boolean verboseOutput = true;

    static List<String> chromosomes = new ArrayList<>(Arrays.asList("2", "3", "4", "7", "8", "9"));

    static RangeSet<Position> setOfAnchorPositions;

    private static int minDepth = 3;

    private DataSet myGraphDataSet = null;

    @Before
    public void setUp() throws Exception {

        //load in the axiom test vcf file
        VCFFileReader axiomReader = new VCFFileReader(new File(axiomVCF), false);

        //Filter axiom vcf by chromosome
        axiomPositionToGenotypeMapping = axiomReader.iterator().stream().filter(variantContext -> chromosomes.contains(variantContext.getContig()))
                .filter(variantContext -> variantContext.isVariant()) //Remove invariants, does not remove when taxon matches reference call
                .filter(variantContext -> !variantContext.getGenotype("B73").getGenotypeString().equals(variantContext.getGenotype(taxon).getGenotypeString()))//remove rows where the taxon matches ref, essentially invariant between ref and currentTaxon
                .filter(variantContext -> !variantContext.getGenotype(taxon).getGenotypeString().equals("./."))
                .collect(Collectors.toMap(variantContext -> Position.of(variantContext.getContig(), variantContext.getStart()),
                        variantContext -> variantContext.getGenotype(taxon).getGenotypeString()));
//
//        axiomContexts = axiomReader.iterator().stream()
//                .collect(Collectors.toMap(variantContext -> Position.of(variantContext.getContig(),variantContext.getStart()),
//                                                                                variantContext -> variantContext));

        axiomGenotypeTable = ImportUtils.readFromVCF(axiomVCF);
        System.out.println("Read in Axiom array");


        HaplotypeGraphBuilderPlugin graphBuilder = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(configFile)
                .methods("collapse_method_1," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP)
                .includeVariantContexts(true);

        myGraphDataSet = graphBuilder.performFunction(null);
        HaplotypeGraph graph = (HaplotypeGraph) myGraphDataSet.getData(0).getData();

        setOfAnchorPositions = graph.referenceRangeStream().collect(Collector.of(TreeRangeSet::create,
                (treeRangeSet, o) -> treeRangeSet.add(Range.closed(Position.of(o.chromosome().getName(), o.start()), Position.of(o.chromosome().getName(), o.end()))),
                (leftRangeSet, rightRangeSet) -> {
                    leftRangeSet.addAll(rightRangeSet);
                    return leftRangeSet;
                }));
        System.out.println("Got anchor map");

    }


    @Test
    public void testCountingPlugin() {
        HapCountBestPathPlugin bestPathPlugin = new HapCountBestPathPlugin(null, false)
                .inclusionFilename(apeKIInclusionFile)
                .targetTaxon(taxon)
                .emissionMethod(ReferenceRangeEmissionProbability.METHOD.inclusionOnly)
                .refRangeFile(refRangeFile)
                .minReads(1);


        bestPathPlugin.performFunction(myGraphDataSet);

    }

    @Test
    public void integrationTestForApeKIPath() {
        HapCountBestPathPlugin bestPathPlugin = new HapCountBestPathPlugin(null, false)
                .inclusionFilename(apeKIInclusionFile)
                .targetTaxon(taxon)
                .emissionMethod(ReferenceRangeEmissionProbability.METHOD.inclusionOnly)
                .refRangeFile(refRangeFile)
                .minReads(1);


        DataSet processedDataSet = bestPathPlugin.performFunction(myGraphDataSet);

        List<HaplotypeNode> path = (List<HaplotypeNode>) processedDataSet.getData(0).getData();


        //from the path get the variants
        Map<Position, String> variantCalls = path.stream()
                .filter(haplotypeNode -> chromosomes.contains(haplotypeNode.referenceRange().chromosome().getName()))
                .map(currentNode -> currentNode.variantContexts()) //Get the optionals out of the nodes
                .filter(optionalContexts -> optionalContexts.isPresent())//Get rid of any that are missing
                .flatMap(optionalContexts -> optionalContexts.get().stream()) // unwrap the optional structure and flatten to a stream of VariantContexts
                .filter(variantContext -> variantContext.isVariant()) // filter out any non-variant sites
                .collect(Collectors.toMap(variantContext -> Position.of(variantContext.getContig(), variantContext.getStart()),
                        variantContext -> variantContext.getGenotype(0).getGenotypeString()));


        scorePath(axiomPositionToGenotypeMapping, variantCalls);

        long numberOfBpsInPath = path.stream().mapToInt(hapNode -> hapNode.referenceRange().end() - hapNode.referenceRange().start()).sum();

        System.out.println("Number of Bps in Path: " + numberOfBpsInPath);
    }

    @Test
    public void integrationTestForW22RawHaplotypeCaller() {
        //Baseline test for W22 using only the raw haplotypecalled variants
        //Correct test would be:
        //Get the graph containing just the W22 nodes non consensus
        //For now just grab the gvcf file used to upload the W22
        //load in the axiom test vcf file
        VCFFileReader gvcfReader = new VCFFileReader(new File(nonConsensusRawGVCFFile), false);

        //Filter axiom vcf by chromosome
        Map<Position, String> variantCalls = gvcfReader.iterator().stream().filter(variantContext -> chromosomes.contains(variantContext.getContig()))
                .filter(variantContext -> variantContext.isVariant()) //Remove invariants, does not remove when taxon matches reference call
                .filter(variantContext -> !variantContext.getGenotype(taxon).getGenotypeString().equals(variantContext.getReference().getBaseString()))


//                                                                            .filter(variantContext -> !variantContext.getGenotype("B73").getGenotypeString().equals(variantContext.getGenotype(taxon).getGenotypeString()))//remove rows where the taxon matches ref, essentially invariant between ref and currentTaxon
                .filter(variantContext -> !variantContext.getGenotype(taxon).getGenotypeString().equals("."))
                .collect(Collectors.toMap(variantContext -> Position.of(variantContext.getContig(), variantContext.getStart()),
                        variantContext -> variantContext.getGenotype(taxon).getGenotypeString() + "/" + variantContext.getGenotype(taxon).getGenotypeString()));

        scorePath(axiomPositionToGenotypeMapping, variantCalls);

    }

    @Test
    public void integrationTestForW22RawHaplotypeCallerPreFilter() {
        VCFFileReader gvcfReader = new VCFFileReader(new File(nonConsensusRawGVCFFilePreFilter), false);

        //Filter axiom vcf by chromosome
        Map<Position, String> variantCalls = gvcfReader.iterator().stream().filter(variantContext -> chromosomes.contains(variantContext.getContig()))
                .filter(variantContext -> variantContext.isVariant()) //Remove invariants, does not remove when taxon matches reference call
                .filter(variantContext -> !variantContext.getGenotype(taxon).getGenotypeString().equals(variantContext.getReference().getBaseString()))


//                                                                            .filter(variantContext -> !variantContext.getGenotype("B73").getGenotypeString().equals(variantContext.getGenotype(taxon).getGenotypeString()))//remove rows where the taxon matches ref, essentially invariant between ref and currentTaxon
                .filter(variantContext -> !variantContext.getGenotype(taxon).getGenotypeString().equals("."))
                .collect(Collectors.toMap(variantContext -> Position.of(variantContext.getContig(), variantContext.getStart()),
                        variantContext -> variantContext.getGenotype(taxon).getGenotypeString() + "/" + variantContext.getGenotype(taxon).getGenotypeString()));

        scorePath(axiomPositionToGenotypeMapping, variantCalls);
    }

    @Test
    public void integrationTestForW22Consensus() {
        //Baseline test only using Consensus nodes containing a W22 sequence
        //Get the Graph of consensus sequence containing W22 nodes
        //Loop through the axiom SNPs and see if the calls match the variants in the axiom array
    }

    @Test
    public void integrationTestForPstIPath() {
        //Integration test for a W22 based on pst1 library
    }


    @Test
    public void integrationTestForW22RawHaplotypeCallerOldScoring() {
        //Baseline test for W22 using only the raw haplotypecalled variants
        //Correct test would be:
        //Get the graph containing just the W22 nodes non consensus
        //For now just grab the gvcf file used to upload the W22
        //load in the axiom test vcf file
//        VCFFileReader gvcfReader = new VCFFileReader(new File(nonConsensusRawGVCFFile), false);
//
//        //Filter axiom vcf by chromosome
//        Map<Position,VariantContext> variantCalls =  gvcfReader.iterator().stream()
////                .filter(variantContext -> chromosomes.contains(variantContext.getContig()))
////                .filter(variantContext -> variantContext.isVariant()) //Remove invariants, does not remove when taxon matches reference call
////                .filter(variantContext -> !variantContext.getGenotype(taxon).getGenotypeString().equals(variantContext.getReference().getBaseString()))
////
////
//////                                                                            .filter(variantContext -> !variantContext.getGenotype("B73").getGenotypeString().equals(variantContext.getGenotype(taxon).getGenotypeString()))//remove rows where the taxon matches ref, essentially invariant between ref and currentTaxon
////                .filter(variantContext -> !variantContext.getGenotype(taxon).getGenotypeString().equals("."))
//                .collect(Collectors.toMap(variantContext -> Position.of(variantContext.getContig(),variantContext.getStart()),
//                        variantContext -> variantContext));

        SimpleGVCFReader variantCalls = new SimpleGVCFReader(nonConsensusRawGVCFFile, referenceFile, 100000000, 3);

        testPHGvsKnownSNPs(taxon, "B73", axiomGenotypeTable, variantCalls, true, true);

        if (taxon.equals("W22")) {
            testIBDinGVCF(axiomGenotypeTable, variantCalls);
        }
    }

    @Test
    public void integrationTestForW22RawHaplotypeCallerPreFilterOldScoring() {
        //Baseline test for W22 using only the raw haplotypecalled variants
        //Correct test would be:
        //Get the graph containing just the W22 nodes non consensus
        //For now just grab the gvcf file used to upload the W22
        //load in the axiom test vcf file
        SimpleGVCFReader variantCalls = new SimpleGVCFReader(nonConsensusRawGVCFFilePreFilter, referenceFile, 100000000, 3);

        testPHGvsKnownSNPs(taxon, "B73", axiomGenotypeTable, variantCalls, true, true);
        if (taxon.equals("W22")) {
            testIBDinGVCF(axiomGenotypeTable, variantCalls);
        }
    }

    @Test
    public void integrationTestForApeKIPathOldScoring() {
        //find the best path through the graph
        HapCountBestPathPlugin bestPathPlugin = new HapCountBestPathPlugin(null, false)
                .inclusionFilename(apeKIInclusionFile)
                .targetTaxon(taxon)
                .emissionMethod(ReferenceRangeEmissionProbability.METHOD.inclusionOnly)
                .refRangeFile(refRangeFile)
                .minReads(1);


        DataSet processedDataSet = bestPathPlugin.performFunction(myGraphDataSet);

        List<HaplotypeNode> path = (List<HaplotypeNode>) processedDataSet.getData(0).getData();

        List<VariantContext> variants = path.stream().map(haplotypeNode -> haplotypeNode.variantContexts()).filter(variantContexts -> variantContexts.isPresent()).flatMap(variantContexts -> variantContexts.get().stream()).collect(Collectors.toList());

        //Output to temp folder
        HapCallingUtils.writeVariantContextsToVCF(variants, exportTempDirectory + "apeKIBestPath.vcf", null, taxon);

        integrationTestForApeKIPathOldScoringFromFile();
    }

    @Test
    public void integrationTestForApeKIPathOldScoringFromFile() {
        SimpleGVCFReader variantCalls = new SimpleGVCFReader(exportTempDirectory + "apeKIBestPath.vcf", referenceFile, 100000000, 3);

        testPHGvsKnownSNPs(taxon, "B73", axiomGenotypeTable, variantCalls, true, true);
        if (taxon.equals("W22")) {
            testIBDinGVCF(axiomGenotypeTable, variantCalls);
        }
    }

    @Test
    public void integrationTestForApeKIPathOldScoringAllCounts() {
        //find the best path through the graph
        HapCountBestPathPlugin bestPathPlugin = new HapCountBestPathPlugin(null, false)
                .inclusionFilename(apeKIInclusionFile)
                .targetTaxon(taxon)
                .emissionMethod(ReferenceRangeEmissionProbability.METHOD.allCounts)
                .probReadMappedCorrectly(0.999)
                .refRangeFile(refRangeFile)
                .minReads(1);


        DataSet processedDataSet = bestPathPlugin.performFunction(myGraphDataSet);

        List<HaplotypeNode> path = (List<HaplotypeNode>) processedDataSet.getData(0).getData();

        List<VariantContext> variants = path.stream().map(haplotypeNode -> haplotypeNode.variantContexts()).filter(variantContexts -> variantContexts.isPresent()).flatMap(variantContexts -> variantContexts.get().stream()).collect(Collectors.toList());

        //Output to temp folder
        HapCallingUtils.writeVariantContextsToVCF(variants, exportTempDirectory + "apeKIBestPathAllCounts.vcf", null, taxon);

        integrationTestForApeKIPathOldScoringAllCountsFromFile();
    }

    @Test
    public void integrationTestForApeKIPathOldScoringAllCountsFromFile() {

        SimpleGVCFReader variantCalls = new SimpleGVCFReader(exportTempDirectory + "apeKIBestPathAllCounts.vcf", referenceFile, 100000000, 3);

        testPHGvsKnownSNPs(taxon, "B73", axiomGenotypeTable, variantCalls, true, true);

        if (taxon.equals("W22")) {
            testIBDinGVCF(axiomGenotypeTable, variantCalls);
        }
    }

    @Test
    public void integrationTestForApeKIPathOldScoringAllCountsSelectTaxa() {
        //find the best path through the graph
        HapCountBestPathPlugin bestPathPlugin = new HapCountBestPathPlugin(null, false)
                .inclusionFilename(apeKIInclusionFile)
                .targetTaxon(taxon)
                .emissionMethod(ReferenceRangeEmissionProbability.METHOD.allCounts)
                .probReadMappedCorrectly(0.999)
                .refRangeFile(refRangeFile)
                .minReads(1)
                .taxaFilterString("B97,Ky21,M162W,Mo17,Ms71,OH43,OH7B,W22");


        DataSet processedDataSet = bestPathPlugin.performFunction(myGraphDataSet);

        List<HaplotypeNode> path = (List<HaplotypeNode>) processedDataSet.getData(0).getData();

        long nonMissingW22Count = path.stream().filter(node -> node.taxaList().indexOf(taxon) > -1).filter(haplotypeNode -> !haplotypeNode.haplotypeSequence().sequence().equals("NNNNNNNNN")).count();

        List<VariantContext> variants = path.stream().map(haplotypeNode -> haplotypeNode.variantContexts()).filter(variantContexts -> variantContexts.isPresent()).flatMap(variantContexts -> variantContexts.get().stream()).collect(Collectors.toList());


        //Output to temp folder
        HapCallingUtils.writeVariantContextsToVCF(variants, exportTempDirectory + "apeKIBestPathAllCountsSelectTaxa.vcf", null, taxon);

        integrationTestForApeKIPathOldScoringAllCountsFromFileSelectTaxa();

        System.out.println("NonMissingW22Ratio:" + ((double) nonMissingW22Count / path.size()));
    }

    @Test
    public void integrationTestForApeKIPathOldScoringAllCountsFromFileSelectTaxa() {

        SimpleGVCFReader variantCalls = new SimpleGVCFReader(exportTempDirectory + "apeKIBestPathAllCountsSelectTaxa.vcf", referenceFile, 100000000, 3);

        testPHGvsKnownSNPs(taxon, "B73", axiomGenotypeTable, variantCalls, true, true);

        if (taxon.equals("W22")) {
            testIBDinGVCF(axiomGenotypeTable, variantCalls);
        }
    }


    @Test
    public void integrationTestForApeKIPathOldScoringAllCountsSelectTaxaForwardBackward() {
        //find the best path through the graph
        HapCountBestPathPlugin bestPathPlugin = new HapCountBestPathPlugin(null, false)
                .inclusionFilename(apeKIInclusionFile)
                .targetTaxon(taxon)
                .emissionMethod(ReferenceRangeEmissionProbability.METHOD.allCounts)
                .probReadMappedCorrectly(0.999)
                .refRangeFile(refRangeFile)
                .minReads(1)
                .useBackwardForward(true)
                .taxaFilterString("B97,Ky21,M162W,Mo17,Ms71,OH43,OH7B,W22");


        DataSet processedDataSet = bestPathPlugin.performFunction(myGraphDataSet);

        List<HaplotypeNode> path = (List<HaplotypeNode>) processedDataSet.getData(0).getData();

        long nonMissingW22Count = path.stream().filter(node -> node.taxaList().indexOf(taxon) > -1).filter(haplotypeNode -> !haplotypeNode.haplotypeSequence().sequence().equals("NNNNNNNNN")).count();

        List<VariantContext> variants = path.stream().map(haplotypeNode -> haplotypeNode.variantContexts()).filter(variantContexts -> variantContexts.isPresent()).flatMap(variantContexts -> variantContexts.get().stream()).collect(Collectors.toList());


        //Output to temp folder
        HapCallingUtils.writeVariantContextsToVCF(variants, exportTempDirectory + "apeKIBestPathAllCountsSelectTaxaForwardBackward.vcf", null, taxon);

        integrationTestForApeKIPathOldScoringAllCountsFromFileSelectTaxaForwardBackward();

        System.out.println("NonMissingW22Ratio:" + ((double) nonMissingW22Count / path.size()));
    }

    @Test
    public void integrationTestForApeKIPathOldScoringAllCountsFromFileSelectTaxaForwardBackward() {

        SimpleGVCFReader variantCalls = new SimpleGVCFReader(exportTempDirectory + "apeKIBestPathAllCountsSelectTaxaForwardBackward.vcf", referenceFile, 100000000, 3);

        testPHGvsKnownSNPs(taxon, "B73", axiomGenotypeTable, variantCalls, true, true);

        if (taxon.equals("W22")) {
            testIBDinGVCF(axiomGenotypeTable, variantCalls);
        }
    }


    // This VCF is located here: cbsublfs1://data1/PHG/rAmpSeq_files/UnitTestFiles
    @Test
    public void integrationTestRampSeq() {
        System.out.println("Loading in variants:");
        SimpleGVCFReader variantCalls = new SimpleGVCFReader("/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/temp/hapcalling/rAmpSeq/DansFiles/outputVCFFiltered.vcf", referenceFile, 100000000, 1);
        System.out.println("Loaded in the variant File");
        testPHGvsKnownSNPs(taxon, "B73", axiomGenotypeTable, variantCalls, true, true);

        if (taxon.equals("W22")) {
            testIBDinGVCF(axiomGenotypeTable, variantCalls);
        }
    }

    @Test
    public void integrationTestRampSeqTaxonFiltering() {
        System.out.println("Loading in variants:");
        SimpleGVCFReader variantCalls = new SimpleGVCFReader("/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/temp/hapcalling/rAmpSeq/DansFiles/pilotStudy_rAmpSeqFilteredTaxonOnlyW22.vcf", referenceFile, 100000000, 1);
        System.out.println("Loaded in the variant File");
        testPHGvsKnownSNPs(taxon, "B73", axiomGenotypeTable, variantCalls, true, true);

        if (taxon.equals("W22")) {
            testIBDinGVCF(axiomGenotypeTable, variantCalls);
        }
    }


    @Test
    public void integrationTestForPstIPathOldScoring() {
        //Integration test for a W22 based on pst1 library
        HapCountBestPathPlugin bestPathPlugin = new HapCountBestPathPlugin(null, false)
                .inclusionFilename(pstIInclusionFile)
                .targetTaxon(taxon)
                .emissionMethod(ReferenceRangeEmissionProbability.METHOD.inclusionOnly)
                .refRangeFile(refRangeFile)
                .minReads(1);


        DataSet processedDataSet = bestPathPlugin.performFunction(myGraphDataSet);

        List<HaplotypeNode> path = (List<HaplotypeNode>) processedDataSet.getData(0).getData();

        List<VariantContext> variants = path.stream().map(haplotypeNode -> haplotypeNode.variantContexts()).filter(variantContexts -> variantContexts.isPresent()).flatMap(variantContexts -> variantContexts.get().stream()).collect(Collectors.toList());

        //Output to temp folder
        HapCallingUtils.writeVariantContextsToVCF(variants, exportTempDirectory + "_pstIBestPath.vcf", null, taxon);

        SimpleGVCFReader variantCalls = new SimpleGVCFReader(exportTempDirectory + "_pstIBestPath.vcf", referenceFile, 100000000, 3);

        testPHGvsKnownSNPs(taxon, "B73", axiomGenotypeTable, variantCalls, true, true);
    }

    private boolean isCallHet(String genotype) {
        //TODO do this more efficiently

        String[] genotypeSplit = genotype.split("/");
        if (genotypeSplit.length == 1) {
            return false;
        } else if (genotypeSplit[0].equals(genotypeSplit[1])) {
            return false;
        } else {
            return true;
        }
    }

    private void scorePath(Map<Position, String> axiomPositionToGenotypeMapping, Map<Position, String> variantCalls) {
        System.out.println("NumOfVariants: " + variantCalls.size());
        System.out.println("NumOfAxiom: " + axiomPositionToGenotypeMapping.size());

        //loop through the array and compare the SNPs
        int numCorrectVariantCalls = 0;
        int numIncorrectVariantCalls = 0;
        int numIncorrectFromMissingPosition = 0;
        int numOfMissingCalledCorrect = 0;
        int numberOfHets = 0;
        int numberOfNonAnchorSNPs = 0;


        if (verboseOutput) {
            System.out.println("Position\tAxiomGenotype\tphgGenotype");
        }

        for (Position axiomSNPPos : axiomPositionToGenotypeMapping.keySet().stream().sorted().collect(Collectors.toList())) {
            String currentGenotype = axiomPositionToGenotypeMapping.get(axiomSNPPos);
            //Check to see if the variantCalls the array are contained in the phg path
            if (!variantCalls.containsKey(axiomSNPPos)) {
                if (setOfAnchorPositions.contains(axiomSNPPos)) {
                    numIncorrectFromMissingPosition++;

                    if (verboseOutput) {
                        System.out.println(axiomSNPPos.getChromosome() + ":" + axiomSNPPos.getPosition() + "\t" + currentGenotype + "\t&/&\t<<<PositionNotFound");
                    }
                } else {
                    numberOfNonAnchorSNPs++;
                }

            } else {
                if (variantCalls.get(axiomSNPPos).equals(currentGenotype)) {
                    numCorrectVariantCalls++;
                } else if (variantCalls.get(axiomSNPPos).equals("./.") || currentGenotype.equals("./.") || variantCalls.get(axiomSNPPos).equals(".") || currentGenotype.equals(".")) {
                    numCorrectVariantCalls++;
                    numOfMissingCalledCorrect++;

                } else {
                    if (isCallHet(variantCalls.get(axiomSNPPos)) || isCallHet(currentGenotype)) {
                        numberOfHets++;
                    } else {
                        numIncorrectVariantCalls++;
                        if (verboseOutput) {
                            System.out.println(axiomSNPPos.getChromosome() + ":" + axiomSNPPos.getPosition() + "\t" + currentGenotype + "\t" + variantCalls.get(axiomSNPPos));
                        }
                    }
                }


            }
        }


//        for(VariantContext phgVariantContext : variants) {
//            Position currentPosition = Position.of(phgVariantContext.getContig(),phgVariantContext.getStart());
//            String currentGenotype = phgVariantContext.getGenotype(0).getGenotypeString();
//
//            if(!axiomPositionToGenotypeMapping.containsKey(currentPosition)) {
//                numIncorrectVariantCalls++;
//                numIncorrectFromMissingPosition++;
//            }
//            else {
//                if(axiomPositionToGenotypeMapping.get(currentPosition).equals(currentGenotype)) {
//                    numCorrectVariantCalls++;
//                }
//                else {
//                    numIncorrectVariantCalls++;
//                }
//            }
//        }

        System.out.println("Number Correct:" + numCorrectVariantCalls);
        System.out.println("Number Incorrect:" + numIncorrectVariantCalls);
        System.out.println("Number Incorrect missed position:" + numIncorrectFromMissingPosition);
        System.out.println("Number of Missing call counted as correct:" + numOfMissingCalledCorrect);
        System.out.println("Number of hets not counted:" + numberOfHets);
        System.out.println("Number of non-anchor axiom SNPs:" + numberOfNonAnchorSNPs);

        System.out.println("Total: " + (numCorrectVariantCalls + numIncorrectVariantCalls + numIncorrectFromMissingPosition));
        System.out.println("TotalBps:" + setOfAnchorPositions.asRanges().stream().mapToInt(range -> range.upperEndpoint().getPosition() - range.lowerEndpoint().getPosition()).sum());

    }

    private void testPHGvsKnownSNPs(String testLine, String refLine, Map<Position, VariantContext> axiomPositionToVariantContextMapping, Map<Position, VariantContext> variantCalls, boolean errorReport, boolean regionReport) {
        System.out.println("EvaluateGVCFbyKnownSNPTest.testPHGvsknownSNPs");
//        int w22index = knownSNPgt.taxa().indexOf(testLine);
//        int b73index = knownSNPgt.taxa().indexOf(refLine);
        List<Integer> goodChromosomes = Lists.newArrayList(2, 3, 4, 7, 8, 9);  //W22 IBD with W22Rstd

        int gvcfAgreesWithKnown = 0;
        int refCallsDoNotMatch = 0;
        int mismatchGVCFisRef = 0;
        int mismatchGVCFisAlt1 = 0;
        int mismatchGVCFisAlt2 = 0;
        int hetIndelMissing = 0;

        Multiset<Position> agreeMap = TreeMultiset.create();
        Multiset<Position> mismatchGVCFisRefMap = TreeMultiset.create();
        Multiset<Position> mismatchGVCFisAlt1Map = TreeMultiset.create();
        Multiset<Position> mismatchGVCFisAlt2Map = TreeMultiset.create();

        if (errorReport)
            System.out.println("Mismatch\tErrorType\tSiteNumber\tChromosome\tPosition\tGVCF" + testLine + "\tArray" + testLine + "\t" +
                    "ReferenceAllele\tArray" + refLine + "\tInRefenceRange");

        int currentPositionCounter = 0;
        for (Position currentPosition : axiomPositionToVariantContextMapping.keySet()) {
            if (currentPosition.getChromosome().getChromosomeNumber() > 10) continue;
            if (!goodChromosomes.contains(currentPosition.getChromosome().getChromosomeNumber())) continue;
            Position binPosition = new GeneralPosition.Builder(currentPosition.getChromosome(), (currentPosition.getPosition() / 1_000_000) * 1_000_000).build();

            List<Byte> calls = Lists.newArrayList(//gVCFcallOfTest, arrayCallofTest, referenceAllele, arrayCallOfReference
                    parseNucleotideDiploidByte((variantCalls.get(currentPosition) == null) ? "N" : variantCalls.get(currentPosition).getGenotype(testLine).getGenotypeString()).orElse(UNKNOWN_DIPLOID_ALLELE),
                    parseNucleotideDiploidByte(axiomPositionToVariantContextMapping.get(currentPosition).getGenotype(testLine).getGenotypeString().replace("/", "")).orElse(UNKNOWN_DIPLOID_ALLELE),
                    parseNucleotideDiploidByte((variantCalls.get(currentPosition) == null) ? "N" : variantCalls.get(currentPosition).getReference().getBaseString()).orElse(UNKNOWN_DIPLOID_ALLELE),
                    parseNucleotideDiploidByte(axiomPositionToVariantContextMapping.get(currentPosition).getGenotype(refLine).getGenotypeString().replace("/", "")).orElse(UNKNOWN_DIPLOID_ALLELE)
            );

            //remove the indel, heterozygous, or reference genome problems
            if (calls.stream().anyMatch(call -> !NucleotideAlignmentConstants.isHomozygousACGT(call))) {
                hetIndelMissing++;
                continue;
            }
            if (calls.get(2) != calls.get(3)) {
                refCallsDoNotMatch++;
                continue;
            }
            //count the correct calls
            if (calls.get(0) == calls.get(1)) {
                gvcfAgreesWithKnown++;
                agreeMap.add(binPosition);
                continue;
            }
            //classifying the mismatches
            String errorType = "";
            if (calls.get(0) == calls.get(2)) {
                mismatchGVCFisRef++;
                errorType = "mismatchGVCFisRef";
                mismatchGVCFisRefMap.add(binPosition);
            } else if (calls.get(1) == calls.get(2)) {
                mismatchGVCFisAlt1++;
                errorType = "mismatchGVCFisAlt1";
                mismatchGVCFisAlt1Map.add(binPosition);
            }  //two states at site
            else {
                mismatchGVCFisAlt2++;
                errorType = "mismatchGVCFisAlt2";
                mismatchGVCFisAlt2Map.add(binPosition);
            } //three states at site

            String callsToString = calls.stream().map(NucleotideAlignmentConstants::getNucleotideIUPAC).collect(Collectors.joining("\t"));
            if (errorReport)
                System.out.printf("Mismatch\t%s\t%d\t%d\t%d\t%s\t%s\n", errorType, currentPositionCounter, currentPosition.getChromosome().getChromosomeNumber(),
                        currentPosition.getPosition(), callsToString, inRefRange(variantCalls.get(currentPosition)));

            currentPositionCounter++;
        }
//        for (int sites = 0; sites < knownSNPgt.numberOfSites(); sites++) {
//            Position currentPosition = knownSNPgt.positions().get(sites);
//            if (currentPosition.getChromosome().getChromosomeNumber() > 10) continue;
//            if (!goodChromosomes.contains(currentPosition.getChromosome().getChromosomeNumber())) continue;
//            Position binPosition = new GeneralPosition.Builder(currentPosition.getChromosome(), (currentPosition.getPosition() / 1_000_000) * 1_000_000).build();
//
//            List<Byte> calls = Lists.newArrayList(//gVCFcallOfTest, arrayCallofTest, referenceAllele, arrayCallOfReference
//                    parseNucleotideDiploidByte(simpleGVCFReader.genotype(currentPosition)).orElse(UNKNOWN_DIPLOID_ALLELE),
//                    parseNucleotideDiploidByte(knownSNPgt.genotypeAsString(w22index, sites)).orElse(UNKNOWN_DIPLOID_ALLELE),
//                    parseNucleotideDiploidByte(simpleGVCFReader.reference(currentPosition)).orElse(UNKNOWN_DIPLOID_ALLELE),
//                    parseNucleotideDiploidByte(knownSNPgt.genotypeAsString(b73index, sites)).orElse(UNKNOWN_DIPLOID_ALLELE)
//            );
//            //remove the indel, heterozygous, or reference genome problems
//            if (calls.stream().anyMatch(call -> !NucleotideAlignmentConstants.isHomozygousACGT(call))) {
//                hetIndelMissing++;
//                continue;
//            }
//            if (calls.get(2) != calls.get(3)) {
//                refCallsDoNotMatch++;
//                continue;
//            }
//            //count the correct calls
//            if (calls.get(0) == calls.get(1)) {
//                gvcfAgreesWithKnown++;
//                agreeMap.add(binPosition);
//                continue;
//            }
//            //classifying the mismatches
//            String errorType = "";
//            if (calls.get(0) == calls.get(2)) {
//                mismatchGVCFisRef++;
//                errorType = "mismatchGVCFisRef";
//                mismatchGVCFisRefMap.add(binPosition);
//            } else if (calls.get(1) == calls.get(2)) {
//                mismatchGVCFisAlt1++;
//                errorType = "mismatchGVCFisAlt1";
//                mismatchGVCFisAlt1Map.add(binPosition);
//            }  //two states at site
//            else {
//                mismatchGVCFisAlt2++;
//                errorType = "mismatchGVCFisAlt2";
//                mismatchGVCFisAlt2Map.add(binPosition);
//            } //three states at site
//
//            String callsToString = calls.stream().map(NucleotideAlignmentConstants::getNucleotideIUPAC).collect(Collectors.joining("\t"));
//            if(errorReport) System.out.printf("Mismatch\t%s\t%d\t%d\t%d\t%s\t%s\n", errorType, sites, currentPosition.getChromosome().getChromosomeNumber(),
//                    currentPosition.getPosition(), callsToString, simpleGVCFReader.inReferenceRange(currentPosition));
//        }


        if (regionReport) {
            System.out.println("Chromosome\tBinPosition\tAgreeCnt\tmismatchGVCFisRefCnt\tmismatchGVCFisAlt1\tmismatchGVCFisAlt2Cnt");
            for (Position position : agreeMap.elementSet()) {
                System.out.println(position.getChromosome().getChromosomeNumber() + "\t" + position.getPosition() + "\t" +
                        agreeMap.count(position) + "\t" + mismatchGVCFisRefMap.count(position)
                        + "\t" + mismatchGVCFisAlt1Map.count(position) + "\t" + mismatchGVCFisAlt2Map.count(position));
            }
        }

        System.out.println("RefLine\t" + refLine);
        System.out.println("TestLine\t" + testLine);
        System.out.println("TestChromosomes\t" + goodChromosomes.stream().map(i -> i.toString()).collect(Collectors.joining(",")));
        System.out.println("minDepth\t" + minDepth);
        System.out.println("HetIndelMissing\t" + hetIndelMissing);
        System.out.println("refCallsDoNotMatch\t" + refCallsDoNotMatch);
        System.out.println("gvcfAgreesWithKnown\t" + gvcfAgreesWithKnown + "\t" + agreeMap.entrySet().stream()
                .mapToInt(e -> e.getCount()).average().orElse(-1));
        System.out.println("mismatchGVCFisRef\t" + mismatchGVCFisRef + "\t" + mismatchGVCFisRefMap.entrySet().stream()
                .mapToInt(e -> e.getCount()).average().orElse(-1));
        System.out.println("mismatchGVCFisAlt1\t" + mismatchGVCFisAlt1 + "\t" + mismatchGVCFisAlt1Map.entrySet().stream()
                .mapToInt(e -> e.getCount()).average().orElse(-1));
        System.out.println("mismatchGVCFisAlt2\t" + mismatchGVCFisAlt2 + "\t" + mismatchGVCFisAlt2Map.entrySet().stream()
                .mapToInt(e -> e.getCount()).average().orElse(-1));

        double totalScored = gvcfAgreesWithKnown + mismatchGVCFisRef + mismatchGVCFisAlt1 + mismatchGVCFisAlt2;
        double totalErrorRate = (mismatchGVCFisRef + mismatchGVCFisAlt1 + mismatchGVCFisAlt2) / totalScored;
        double twoAlleleErrors = (mismatchGVCFisRef + mismatchGVCFisAlt1) / totalScored;

        System.out.println("totalScored\t" + totalScored);
        System.out.println("totalErrorRate\t" + totalErrorRate);
        System.out.println("twoAlleleErrors\t" + twoAlleleErrors);

        //Assert.assertTrue("Error rate at two state above 1.2%",twoAlleleErrors<0.012);
    }

    private boolean inRefRange(VariantContext vc) {
        if (vc.isIndel()) {
            return false; //need to check for indels first
        }

        if (vc.getStart() < vc.getEnd()) {
            return true;
        } else {
            return false;
        }
    }

    private void testPHGvsKnownSNPs(String testLine, String refLine, GenotypeTable knownSNPgt, SimpleGVCFReader simpleGVCFReader, boolean errorReport, boolean regionReport) {
        System.out.println("EvaluateGVCFbyKnownSNPTest.testPHGvsknownSNPs");
        int w22index = knownSNPgt.taxa().indexOf(testLine);
        int b73index = knownSNPgt.taxa().indexOf(refLine);
        List<Integer> goodChromosomes = Lists.newArrayList(2, 3, 4, 7, 8, 9);  //W22 IBD with W22Rstd

        int gvcfAgreesWithKnown = 0;
        int refCallsDoNotMatch = 0;
        int mismatchGVCFisRef = 0;
        int mismatchGVCFisAlt1 = 0;
        int mismatchGVCFisAlt2 = 0;
        int hetIndelMissing = 0;
        int numberOfComplements = 0;
        Multiset<Position> agreeMap = TreeMultiset.create();
        Multiset<Position> mismatchGVCFisRefMap = TreeMultiset.create();
        Multiset<Position> mismatchGVCFisAlt1Map = TreeMultiset.create();
        Multiset<Position> mismatchGVCFisAlt2Map = TreeMultiset.create();

        if (errorReport)
            System.out.println("Mismatch\tErrorType\tSiteNumber\tChromosome\tPosition\tGVCF" + testLine + "\tArray" + testLine + "\t" +
                    "ReferenceAllele\tArray" + refLine + "\tInRefenceRange");
        for (int sites = 0; sites < knownSNPgt.numberOfSites(); sites++) {
            Position currentPosition = knownSNPgt.positions().get(sites);
            if (!setOfAnchorPositions.contains(currentPosition)) continue; //Filter out non anchors
            if (currentPosition.getChromosome().getChromosomeNumber() > 10) continue;
            if (!goodChromosomes.contains(currentPosition.getChromosome().getChromosomeNumber())) continue;
            Position binPosition = new GeneralPosition.Builder(currentPosition.getChromosome(), (currentPosition.getPosition() / 1_000_000) * 1_000_000).build();

            List<Byte> calls = Lists.newArrayList(//gVCFcallOfTest, arrayCallofTest, referenceAllele, arrayCallOfReference
                    parseNucleotideDiploidByte(simpleGVCFReader.genotype(currentPosition)).orElse(UNKNOWN_DIPLOID_ALLELE),
                    parseNucleotideDiploidByte(knownSNPgt.genotypeAsString(w22index, sites)).orElse(UNKNOWN_DIPLOID_ALLELE),
                    parseNucleotideDiploidByte(simpleGVCFReader.reference(currentPosition)).orElse(UNKNOWN_DIPLOID_ALLELE),
                    parseNucleotideDiploidByte(knownSNPgt.genotypeAsString(b73index, sites)).orElse(UNKNOWN_DIPLOID_ALLELE)
            );
            //remove the indel, heterozygous, or reference genome problems
            if (calls.stream().anyMatch(call -> !NucleotideAlignmentConstants.isHomozygousACGT(call))) {
                hetIndelMissing++;
                continue;
            }
            if (calls.get(2) != calls.get(3)) {
                refCallsDoNotMatch++;
                continue;
            }
            //count the correct calls
            if (calls.get(0) == calls.get(1)) {
                gvcfAgreesWithKnown++;
                agreeMap.add(binPosition);
                continue;
            }
            //classifying the mismatches
            String errorType = "";
            if (calls.get(0) == calls.get(2)) {
                if (calls.get(0) == NucleotideAlignmentConstants.getNucleotideComplement(calls.get(2))) {
                    numberOfComplements++;
                }
                mismatchGVCFisRef++;
                errorType = "mismatchGVCFisRef";
                mismatchGVCFisRefMap.add(binPosition);
            } else if (calls.get(1) == calls.get(2)) {
                mismatchGVCFisAlt1++;
                errorType = "mismatchGVCFisAlt1";
                mismatchGVCFisAlt1Map.add(binPosition);
            }  //two states at site
            else {
                mismatchGVCFisAlt2++;
                errorType = "mismatchGVCFisAlt2";
                mismatchGVCFisAlt2Map.add(binPosition);
            } //three states at site

            String callsToString = calls.stream().map(NucleotideAlignmentConstants::getNucleotideIUPAC).collect(Collectors.joining("\t"));
            if (errorReport)
                System.out.printf("Mismatch\t%s\t%d\t%d\t%d\t%s\t%s\n", errorType, sites, currentPosition.getChromosome().getChromosomeNumber(),
                        currentPosition.getPosition(), callsToString, simpleGVCFReader.inReferenceRange(currentPosition));
        }


        if (regionReport) {
            System.out.println("Chromosome\tBinPosition\tAgreeCnt\tmismatchGVCFisRefCnt\tmismatchGVCFisAlt1\tmismatchGVCFisAlt2Cnt");
            for (Position position : agreeMap.elementSet()) {
                System.out.println(position.getChromosome().getChromosomeNumber() + "\t" + position.getPosition() + "\t" +
                        agreeMap.count(position) + "\t" + mismatchGVCFisRefMap.count(position)
                        + "\t" + mismatchGVCFisAlt1Map.count(position) + "\t" + mismatchGVCFisAlt2Map.count(position));
            }
        }

        System.out.println("RefLine\t" + refLine);
        System.out.println("TestLine\t" + testLine);
        System.out.println("TestChromosomes\t" + goodChromosomes.stream().map(i -> i.toString()).collect(Collectors.joining(",")));
        System.out.println("minDepth\t" + minDepth);
        System.out.println("HetIndelMissing\t" + hetIndelMissing);
        System.out.println("refCallsDoNotMatch\t" + refCallsDoNotMatch);
        System.out.println("gvcfAgreesWithKnown\t" + gvcfAgreesWithKnown + "\t" + agreeMap.entrySet().stream()
                .mapToInt(e -> e.getCount()).average().orElse(-1));
        System.out.println("mismatchGVCFisRef\t" + mismatchGVCFisRef + "\t" + mismatchGVCFisRefMap.entrySet().stream()
                .mapToInt(e -> e.getCount()).average().orElse(-1));
        System.out.println("mismatchGVCFisAlt1\t" + mismatchGVCFisAlt1 + "\t" + mismatchGVCFisAlt1Map.entrySet().stream()
                .mapToInt(e -> e.getCount()).average().orElse(-1));
        System.out.println("mismatchGVCFisAlt2\t" + mismatchGVCFisAlt2 + "\t" + mismatchGVCFisAlt2Map.entrySet().stream()
                .mapToInt(e -> e.getCount()).average().orElse(-1));

        double totalScored = gvcfAgreesWithKnown + mismatchGVCFisRef + mismatchGVCFisAlt1 + mismatchGVCFisAlt2;
        double totalErrorRate = (mismatchGVCFisRef + mismatchGVCFisAlt1 + mismatchGVCFisAlt2) / totalScored;
        double twoAlleleErrors = (mismatchGVCFisRef + mismatchGVCFisAlt1) / totalScored;

        System.out.println("totalScored\t" + totalScored);
        System.out.println("totalErrorRate\t" + totalErrorRate);
        System.out.println("twoAlleleErrors\t" + twoAlleleErrors);


        double errorAfterComplements = (totalScored - numberOfComplements - gvcfAgreesWithKnown) / (totalScored - numberOfComplements);
        System.out.println("NumberOfGVCFRefComplements\t" + numberOfComplements);
        System.out.println("Error minus the complements\t" + errorAfterComplements);

        //Assert.assertTrue("Error rate at two state above 1.2%",twoAlleleErrors<0.012);
    }

    public void testIBDinGVCF(GenotypeTable knownSNPgt, SimpleGVCFReader simpleGVCFReader) {
        RangeSet<Tuple<Integer, Integer>> ibdRegions = ImmutableRangeSet.<Tuple<Integer, Integer>>builder()
                .add(Range.closed(new Tuple<>(8, 122_000_000), new Tuple<>(8, 142_000_000)))
                .add(Range.closed(new Tuple<>(7, 29_000_000), new Tuple<>(7, 40_000_000)))
                .add(Range.closed(new Tuple<>(3, 68_000_000), new Tuple<>(3, 92_000_000)))
                .build();

        testIBDinGVCF(ibdRegions, knownSNPgt, simpleGVCFReader, false, false);
//        RangeSet<Tuple<Integer,Integer>> ibdRegions2= ImmutableRangeSet.<Tuple<Integer,Integer>>builder()
//                .add(Range.closed(new Tuple<>(8,12_000_000),new Tuple<>(8,32_000_000)))
//                .build();

        //testIBDinGVCF(ibdRegions2,false, true);

    }

    private void testIBDinGVCF(RangeSet<Tuple<Integer, Integer>> ibdRegions, GenotypeTable knownSNPgt, SimpleGVCFReader simpleGVCFReader, boolean errorReport, boolean regionReport) {
        int snpCount = 0, rangeRefCount = 0;
        for (Range<Tuple<Integer, Integer>> ibdRegion : ibdRegions.asRanges()) {
            RangeMap<Tuple<Integer, Integer>, String> ibdMap = simpleGVCFReader.getSubMap(ibdRegion);
            for (Map.Entry<Range<Tuple<Integer, Integer>>, String> rangeCall : ibdMap.asMapOfRanges().entrySet()) {
                if (rangeCall.getValue().equals("REFRANGE")) {
                    rangeRefCount += rangeCall.getKey().upperEndpoint().getY() - rangeCall.getKey().lowerEndpoint().getY();
                } else {
                    String call = rangeCall.getValue();
                    if (call.length() > 1) continue;
                    if (call.equals(simpleGVCFReader.reference(rangeCall.getKey().upperEndpoint().getX(), rangeCall.getKey().upperEndpoint().getY())))
                        continue;
                    snpCount++;
                    if (regionReport) {
                        System.out.println("rangeCall = " + rangeCall.getKey() + "->" + rangeCall.getValue());
                    }
                }
            }
        }
        System.out.println("snpCount = " + snpCount);
        System.out.println("rangeRefCount = " + rangeRefCount);
        System.out.println("snpCount/rangeRefCount = " + (double) snpCount / rangeRefCount);
        System.out.println("TODO These errors rates will drop more if the gvcf is pre-filtered for bad SNPs and duplicated regions.");


    }

}
