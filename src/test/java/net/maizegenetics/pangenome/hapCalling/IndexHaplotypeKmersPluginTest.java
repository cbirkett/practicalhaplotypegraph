package net.maizegenetics.pangenome.hapCalling;

import gnu.trove.map.hash.TLongObjectHashMap;
import net.maizegenetics.dna.BaseEncoder;
import net.maizegenetics.pangenome.api.HaplotypeGraph;
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin;
import net.maizegenetics.pangenome.api.HaplotypeNode;
import net.maizegenetics.pangenome.api.ReferenceRangeEmissionProbability;
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import net.maizegenetics.pangenome.db_loading.GetDBConnectionPlugin;
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.Taxon;
import net.maizegenetics.util.LoggingUtils;

import org.junit.Test;

import com.google.common.collect.Multiset;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static net.maizegenetics.pangenome.smallseq.SmallSeqPaths.dbConfigFile;
import static net.maizegenetics.pangenome.smallseq.SmallSeqPaths.outputDir;

/**
 * Created by zrm22 on 10/23/17.
 */
@Deprecated
public class IndexHaplotypeKmersPluginTest {

    String userHome = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/temp/hapcalling/";
//    String kmerFile = "/Users/edbuckler/temp/phgSmallSeq/kmerMap.bin";
    String kmerFile = outputDir + "kmerMap.bin";
    // String dbFile = userHome + "/sept_18_2017_308haps_306ConsensusWithAssemblies.db";

    String refRangeFile = "data/HapCallingTestFiles/AllTaxaAveraged_Evaluation_FilteredRefRanges_InfoScoreReversed.txt";
    
    @Test
    public void testIndexingMethod() {
        LoggingUtils.setupDebugLogging();

        System.out.println("Building graph");
        DataSet graphDataSet = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(dbConfigFile)
                .methods("CONSENSUS,*")
                .includeVariantContexts(false)
                .performFunction(null);

        HaplotypeGraph graph = (HaplotypeGraph) graphDataSet.getData(0).getData();
        long numNodes = graph.nodeStream().filter(node -> node.referenceRange().chromosome().getName().equals("1")).count();
        long numRefs = graph.referenceRangeStream().filter(refRange -> refRange.chromosome().getName().equals("1")).count();

        System.out.println("AvgNumNodes:" + (double) numNodes / numRefs);
        System.out.println("Indexing");

        DataSet kmerIndexDataSet = new IndexHaplotypeKmersPlugin(null, false)
                .kmerMapFile(kmerFile)
                .processData(graphDataSet);
        System.out.println("Indexing done");
    }

    @Test
    public void testkmerMap() throws IOException, ClassNotFoundException {
        System.out.println("Building graph");
        DataSet graphDataSet = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(dbConfigFile)
                .methods("CONSENSUS,*")
                .includeVariantContexts(false)
                .performFunction(null);

        HaplotypeGraph graph = (HaplotypeGraph) graphDataSet.getData(0).getData();
        Map<Integer, HaplotypeNode> indexToHapNode = graph.nodeStream().collect(Collectors.toMap(HaplotypeNode::id, n -> n));
        TLongObjectHashMap<int[]> kmerMap = null;
        try {
            kmerMap = (TLongObjectHashMap<int[]>) deserializeMapToFile(kmerFile);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Unable to deserialize kmer map.", e);
        }

        TaxaList taxaList = graph.taxaInGraph();
        System.out.println("taxaList.size() = " + taxaList.size());
        System.out.println("kmerMap = " + kmerMap.size());

        TaxaList orignalTaxaList = taxaList.stream()
                .map(t -> new Taxon(t.getName().split("_")[0]))
                .collect(TaxaList.collect());

        for (Taxon taxon : orignalTaxaList) {
            System.out.println("Target taxon:" + taxon);
            String aLineSeq = Files.readAllLines(Paths.get(SmallSeqPaths.answerDir + taxon + ".fa")).get(1);
            //String aLineSeq = Files.readAllLines(Paths.get(SmallSeqPaths.answerDir + "LineB.fa")).get(1);
            System.out.println(taxon + " Seq = " + aLineSeq);

            int countMisses = 0, countCorrectTaxonHit = 0, countHitSomething = 0;
            for (int i = 0; i < aLineSeq.length() - 32; i++) {
                String subSeq = aLineSeq.substring(i, i + 32);
                long kmerEncoded = BaseEncoder.getLongFromSeq(subSeq);
                int[] hapsHit = kmerMap.get(kmerEncoded);
                //System.out.println(i+" = " + hapsHit);
                if (hapsHit == null) {
                    countMisses++;
                    continue;
                }
                Set<Taxon> taxaHit = IntStream.of(hapsHit)
                        .mapToObj(indexToHapNode::get)
                        .flatMap(hn -> hn.taxaList().stream())
                        .map(t -> new Taxon(t.getName().split("_")[0]))
                        .collect(Collectors.toSet());
                if (taxaHit.contains(taxon)) countCorrectTaxonHit++;
                if (taxaHit.size() > 0) countHitSomething++;
                if (!taxaHit.contains(taxon) && taxaHit.size() > 0) {
                  //  System.out.println(subSeq);
                }
            }
            System.out.println("countMisses = " + countMisses);
            System.out.println("countHitSomething = " + countHitSomething);
            System.out.println("countCorrectTaxonHit = " + countCorrectTaxonHit);
            //TODO Errors caused by consensus sequence have an N and not being indexed
            System.out.println("Error rate = " + (double)(countHitSomething-countCorrectTaxonHit)/countHitSomething);
        }
        //HaplotypePath haplotypePath = graph.path(taxaList.get(0));


    }

    // Assembly processing stores the List<VariantContext>
    private Object deserializeMapToFile(String kmerFile) throws IOException, ClassNotFoundException {
        FileInputStream fileOutputStream = new FileInputStream(new File(kmerFile));
        ObjectInputStream objectStream = new ObjectInputStream(fileOutputStream);
        Object obj = objectStream.readObject();
        objectStream.close();
        fileOutputStream.close();
        return obj;
    }


    @Test
    public void testIndexingAndCountingMethod() {
        LoggingUtils.setupDebugLogging();

        System.out.println("Building graph");
        DataSet graphDataSet = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(dbConfigFile)
                .methods("CONSENSUS,*")
                .includeVariantContexts(false)
                .performFunction(null);

        HaplotypeGraph graph = (HaplotypeGraph) graphDataSet.getData(0).getData();
        System.out.println("Indexing");

        TLongObjectHashMap<int[]> kmerMap = null;
        try {
            kmerMap = (TLongObjectHashMap<int[]>) deserializeMapToFile(kmerFile);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
            
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Unable to deserialize kmer map", e);
        }

        DataSet hapCountDS = new FastqToKmerCountPlugin(null, false)
                .readFile(SmallSeqPaths.dataDir + "LineA_R1.fastq")
                .exportHaplotypeFile(SmallSeqPaths.outputDir + "LineA_R1.hapcounts.txt")
                .debugTaxon("LineA")
                .processData(DataSet.getDataSet(kmerMap));
        Multiset<Integer> hapCountMultiset = (Multiset<Integer>) hapCountDS.getData(0).getData();
        
        Connection dbConn = (Connection) new GetDBConnectionPlugin()
                .createNew(false)
                .configFile(dbConfigFile)
                .performFunction(null).getData(0).getData();
        
        try {
            
            Statement dbst = dbConn.createStatement();
            PrintWriter pw = new PrintWriter(SmallSeqPaths.outputDir + "LineA_R1.hapcountinfo.txt");
            pw.println("hapid\tgamete_group\trefRange\tchr\tstart\tincl\texcl");
            for (Integer hapid : hapCountMultiset.elementSet()) {
                
                if (hapid > 0) {
                    int incCount = hapCountMultiset.count(hapid);
                    int excCount = hapCountMultiset.count(-hapid);
                    String sql = String.format("SELECT h.haplotypes_id, h.gamete_grp_id, r.ref_range_id, chrom, range_start "
                            + "FROM haplotypes h, reference_ranges r "
                            + "WHERE h.ref_range_id=r.ref_range_id AND haplotypes_id=%d", hapid);
                    ResultSet rs = dbst.executeQuery(sql);
                    rs.next();
                    pw.printf("%d\t%d\t%d\t%s\t%d\t%d\t%d%n",rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getInt(5), incCount, excCount);
                    rs.close();
                }
                
            }
            
            pw.close();
            dbst.close();
        } catch (SQLException e1) {
            throw new IllegalArgumentException(e1);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        
        try {
            dbConn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        System.out.println("Done exporting counts");
    }

    @Test
    public void testInclusionFile() {
        System.out.println("Building graph");
        DataSet graphDataSet = new HaplotypeGraphBuilderPlugin(null, false)
                .configFile(dbConfigFile)
                .methods("collapse_method_1," + DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP)
                .includeVariantContexts(false)
                .performFunction(null);

        HaplotypeGraph graph = (HaplotypeGraph) graphDataSet.getData(0).getData();

        //Build the HMM
        DataSet bestPath = new HapCountBestPathPlugin(null, false)
                .inclusionFilename("/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/temp/hapcalling/RamuFiles/KmerBasedTests/KmerOutputsW22_3.txt")
                .targetTaxon("W22")
//                .emissionMethod(ReferenceRangeEmissionProbability.METHOD.inclusionOnly)
                .emissionMethod(ReferenceRangeEmissionProbability.METHOD.allCounts)
                .probReadMappedCorrectly(0.999)
                .refRangeFile(refRangeFile)
                .minReads(1)
                .useBackwardForward(false)
                .taxaFilterString("B97,Ky21,M162W,Mo17,Ms71,OH43,OH7B,W22")
                .processData(graphDataSet);


    }
}
