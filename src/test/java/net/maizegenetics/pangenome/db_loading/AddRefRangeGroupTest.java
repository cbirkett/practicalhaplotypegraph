/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

import static net.maizegenetics.pangenome.db_loading.DBLoadingUtils.createPathNodesForGameteGrp;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;

import org.junit.Test;

import net.maizegenetics.pangenome.api.CreateGraphUtils;
import net.maizegenetics.pangenome.api.ReferenceRange;
import net.maizegenetics.util.Utils;

/**
 * Tests for the AddRefRangeGroupPlugin
 * 
 * @author lcj34
 *
 */
public class AddRefRangeGroupTest {

    @Test
    public void testAddRefRangeGroupGoodData() {
        System.out.println("Begin testAddRefRangeGroupGoodData\n");
        String configFile = "/Users/lcj34/notes_files/phg_2018/dbSchema_updates/phg205_variants/configSQLite.txt";
        Connection dbConn = DBLoadingUtils.connection(configFile, false);
        
        String methodName = "lynnTestMethod";
        String methodDetails = "let us see if this works";
        
        String userHome = System.getProperty("user.home");
        String goodRangesFile = userHome + "/goodRangesTestFile.txt";
        
        // create the ranges file with data from the db
        SortedSet<ReferenceRange>  refRangeSet = CreateGraphUtils.referenceRanges(dbConn);
        try (BufferedWriter bw = Utils.getBufferedWriter(goodRangesFile)) {
            int count = 0;
            StringBuilder sb = new StringBuilder();
            for (ReferenceRange refRange : refRangeSet)  {          
                sb.append(refRange.chromosome().getName()).append("\t");
                int start = refRange.start()-1; // bed file is 0-based
                sb.append(start).append("\t");
                sb.append(refRange.end()).append("\n"); // bed file is exclusive on the end
                count++;
                if (count == 20) break; // just adding 20 for testing
            }
            bw.write(sb.toString());
        } catch (IOException ioe) {
            throw new IllegalStateException("testAddRefRangeGroup: ranges files write failed");
        }
        
        // run the plugin
        new AddRefRangeGroupPlugin()
        .methodName(methodName)
        .methodDetails(methodDetails)
        .configFile(configFile)
        .ranges(goodRangesFile)
        .performFunction(null);
        
        int methodId = CreateGraphUtils.methodId(dbConn,methodName);       
        assertTrue(methodId > 0);
        
        String query = "select count(*) as group_count from ref_range_ref_range_method where method_id = " + methodId;
        
        try (ResultSet rs = dbConn.createStatement().executeQuery(query)){
            if (rs.next()) {
                int group_count = rs.getInt("group_count");
                System.out.println("group_count for methodId is " + group_count);
                assertEquals(group_count,20);
            } else {
                fail("No count for methodName " + methodName);
            }
            
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        
        System.out.println("\nFirst test passed");
        // Run again, using the same method.  This should fail due to method already existing
        System.out.println("\nRunning second test:  duplicate method name, should fail");
        try {
            // run the plugin
            new AddRefRangeGroupPlugin()
            .methodName(methodName)
            .methodDetails(methodDetails)
            .configFile(configFile)
            .ranges(goodRangesFile)
            .performFunction(null);
            
           fail("Second running of test should have thrown an expcetion complaining of a duplicate method");
        } catch (Exception exc) {
            assertTrue(true); // test failed as it should have
            System.out.println("\nSecond test passed - duplicate method causes failure as expected: " + exc.getMessage());
        }

        System.out.println("JUnit finished!");                
    }
    
    @Test
    public void testAddRefRangeGroupBadRangeData() {
        String configFile = "/Users/lcj34/notes_files/phg_2018/dbSchema_updates/phg205_variants/configSQLite.txt";
        Connection dbConn = DBLoadingUtils.connection(configFile, false);
        
        String methodName = "secondGroupMethod";
        String methodDetails = "this test should fail";
        
        String userHome = System.getProperty("user.home");
        String goodRangesFile = userHome + "/badRangesTestFile.txt";
        
        System.out.println("\ntestAddREfRangeGroupBadRangeData:  RUnning test, expecting exception to be thrown\n");
        // create the ranges file with data from the db
        SortedSet<ReferenceRange>  refRangeSet = CreateGraphUtils.referenceRanges(dbConn);
        try (BufferedWriter bw = Utils.getBufferedWriter(goodRangesFile)) {
            int count = 0;
            StringBuilder sb = new StringBuilder();
            for (ReferenceRange refRange : refRangeSet)  {          
                sb.append(refRange.chromosome().getName()).append("\t");
                int start = refRange.start(); // bed file is 0-based - purposefully don't change so is bad range
                sb.append(start).append("\t");
                sb.append(refRange.end()).append("\n"); // bed file is exclusive on the end
                count++;
                if (count == 20) break; // just adding 20 for testing
            }
            bw.write(sb.toString());
        } catch (IOException ioe) {
            throw new IllegalStateException("testAddRefRangeGroup: ranges files write failed");
        }
        
        // run the plugin.  This should throw an exception: bad input file ref range data
        new AddRefRangeGroupPlugin()
        .methodName(methodName)
        .methodDetails(methodDetails)
        .configFile(configFile)
        .ranges(goodRangesFile)
        .performFunction(null);
    }

    @Test
    public void testAddPathsManuallyForGameteGrpID() {
        // LCJ: This test created as there was a request for paths to be
        // added for the reference as an assembly.  Initially,
        // AddRefRangeAsAssemblyPlugin did not create a path, this
        // was later added to Assembly and WGS code, so we want it here as well
        // I'm keeping this test here in case we need to manually add paths
        // for some other line in the future.  Doubtful, but might as well
        // keep the code.

        //String configFile = "/Users/lcj34/notes_files/phg_2018/NAM_genome_file_data/configDC01_test.txt";
        String configFile = "/Users/lcj34/notes_files/phg_2018/NAM_genome_file_data/configDC01_maize1_0.txt";

        String lineName = "B73_Assembly";
        Connection dbConnect = DBLoadingUtils.connection(configFile, false);
        if (dbConnect == null) {
            throw new IllegalStateException("AddRefRangeAsAssembly: no connection supplied!");
        }
        PHGDataWriter phg = new PHGdbAccess(dbConnect);

        // Gamete_grp_id is NOT always the genoid.  For this particular test,
        // it will be as there are no consensus in this db when this is run.
        // See AddRefRangeAsAssemblyPlugin for using phg.getGameteGroupIDFromTaxaList(gameteGroupList);
        int gamete_grp_id = phg.getGenoidFromLine(lineName);
        System.out.println("gamete_grp_id for " + lineName + " is " + gamete_grp_id);
        // Create a path
        List<Integer> hapidList = createPathNodesForGameteGrp(lineName, dbConnect, gamete_grp_id);
        byte[] pathBytes = DBLoadingUtils.encodePathsFromIntArray(hapidList);
        String pathMethod = "mummer4_PATH";
        HashMap<String, String> methodParams = new HashMap<>();
        methodParams.put("notes","path created for assembly ");

        int pathid = phg.putPathsData(pathMethod,  methodParams, lineName, null,  pathBytes,false);

        System.out.println("FInished - pathid=" + pathid + ", now closing phg connection");
        try {
            ((PHGdbAccess)phg).close();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}
