/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;

import net.maizegenetics.pangenome.db_loading.CreateIntervalsFileFromGffPlugin;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GeneralPosition;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.dna.map.Position;

/**
 * Test class to verify methods internal to the CreateIntervalsFileFromGff class
 * 
 * @author lcj34
 *
 */
public class CreateIntervalsFileFromGffPluginTest {
    
    String refFile = "/Volumes/Samsung_T1/wgs_pipeline/refGenomeFiles/Zea_mays.AGPv4.dna.toplevel.fa";
    String outputDir = "/Users/lcj34/notes_files/repgen/wgs_pipeline/anchorsFromV4gff/allGeneAnchorFiles/july2017_phg_intervals_RangeMaps/";
    //String geneFile = "/Volumes/Samsung_T1/wgs_pipeline/v4_gff_files/allGeneBedFiles";
    String geneFile = "/Volumes/Samsung_T1/wgs_pipeline/v4_gff_files/gffv4Rel34_all10chroms_gene.txt";
    int numFlanking = 1000; // number flanking bps to add on each end
    GenomeSequence myRefSequence;
    
    @Before
    public void setup() throws Exception {
        // Could create the reference sequence in the setup if multiple
        // test cases will need it.  RIght now, only 1 does, so created
        // it in that test case.
        
        //myRefSequence = GenomeSequenceBuilder.instance(refFile);
    }
    
    @Test
    public void testCreateIntervalsFileFromGff() throws Exception {
        // use this to run the full plugin
        new CreateIntervalsFileFromGffPlugin()
        .refFile(refFile)
        .outputDir(outputDir)
        .geneFile(geneFile)
        .numFlanking(numFlanking)
        .performFunction(null);
    }
    
    // Test the addRanges() class.  This class merges embedded or overlapping genes
    // into 1 entry, then adds them to a map.  The
    @Test
    public void testAddRanges() throws Exception {

        RangeMap<Position,String> geneRangeMap = TreeRangeMap.create();
        Chromosome chr1 = new Chromosome("1");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,5).build(), 
                new GeneralPosition.Builder(chr1,15).build()),"firstGene");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,20).build(), 
                new GeneralPosition.Builder(chr1,25).build()),"gene2");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,30).build(), 
                new GeneralPosition.Builder(chr1,52).build()),"gene3");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,33).build(), 
                new GeneralPosition.Builder(chr1,45).build()),"embeddedGene1");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,47).build(), 
                new GeneralPosition.Builder(chr1,60).build()),"overlappedGene1");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,75).build(), 
                new GeneralPosition.Builder(chr1,102).build()),"gene4");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,110).build(), 
                new GeneralPosition.Builder(chr1,200).build()),"gene5");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,150).build(), 
                new GeneralPosition.Builder(chr1,250).build()),"overlappedGene2");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,300).build(), 
                new GeneralPosition.Builder(chr1,302).build()),"gene6");
        
        System.out.println("Last gene added, size of range: " + geneRangeMap.asMapOfRanges().size());
        
        assertEquals(geneRangeMap.asMapOfRanges().size(),6);
        String thirtySixtyValue = "gene3-embeddedGene1-overlappedGene1";
        Position thirty = new GeneralPosition.Builder(chr1,30).build();
               
        String thirtySixtyFromMap = geneRangeMap.get(thirty);
        assertTrue(thirtySixtyValue.equals(thirtySixtyFromMap));
        
        Position twoHundred = new GeneralPosition.Builder(chr1,200).build();
        String twoHundredFromMap = geneRangeMap.get(twoHundred);
        assertTrue(twoHundredFromMap.equals("gene5-overlappedGene2"));
        
        for (Map.Entry<Range<Position>,String> entry: geneRangeMap.asMapOfRanges().entrySet()){
            Range<Position> range = entry.getKey();
            int start = range.lowerEndpoint().getPosition();
            int end = range.upperEndpoint().getPosition();
            String gene = entry.getValue();
            System.out.println(start + "," + end + " " + gene);
        }
        System.out.println("\nFinished testAddRanges!\n");       
    }
    
    // The createFlankingList class takes a map of anchor coordinates
    // and adds flanking bp's to the ends.  If there are insufficient
    // number of bps between anchors to satisfy user request, the existing
    // bp's are split between the 2 anchors.
    @Test
    public void testCreateFlankingList() throws Exception {
        RangeMap<Position,String> geneRangeMap = TreeRangeMap.create();
        Chromosome chr1 = new Chromosome("1");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,44289).build(), 
                new GeneralPosition.Builder(chr1,49837).build()),"gene1");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,50877).build(), 
                new GeneralPosition.Builder(chr1,55716).build()),"gene2");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,92299).build(), 
                new GeneralPosition.Builder(chr1,95134).build()),"gene3");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,111655).build(), 
                new GeneralPosition.Builder(chr1,118312).build()),"gene4");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,118683).build(), 
                new GeneralPosition.Builder(chr1,119739).build()),"gene5");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,122120).build(), 
                new GeneralPosition.Builder(chr1,122614).build()),"gene6");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,138378).build(), 
                new GeneralPosition.Builder(chr1,139043).build()),"gene7");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,196942).build(), 
                new GeneralPosition.Builder(chr1,199159).build()),"gene8");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,199345).build(), 
                new GeneralPosition.Builder(chr1,205715).build()),"gene9");
        privAddRangeMap(geneRangeMap, Range.closed(new GeneralPosition.Builder(chr1,209979).build(), 
                new GeneralPosition.Builder(chr1,215403).build()),"gene10");
        
        
        System.out.println("Last gene added, size of range: " + geneRangeMap.asMapOfRanges().size());
        assertEquals(geneRangeMap.asMapOfRanges().size(),10);
        
        // Create ref sequence. Needed for createFlankingLIst to find the length of each chromosome
        // when adding flanking to last gene.
        System.out.println("Create genome sequence ...");
        myRefSequence = GenomeSequenceBuilder.instance(refFile);
        
        System.out.println("Create gene-with-flanking map");
        // create flanking map - 1000bps each side: The new values should be:
        // 43289-50357   gene1
        // 50358-56716   gene2
        // 91299-96134   gene3
        // 110655-118497 gene4
        // 118499-120739 gene5
        // 121120-123614 gene6
        // 137378-140043 gene7
        // 195942-199252 gene8
        // 199253-206715 gene9
        // 208979-216403 gene10
      
        RangeMap<Position,String> flankingGeneMap = privCreateFlankingList( geneRangeMap,  1000, myRefSequence);
        
        assertEquals(flankingGeneMap.asMapOfRanges().size(),10);
        
        // gene1 - check range based on original lower bound
        Position gene = new GeneralPosition.Builder(chr1,44289).build();
        int geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        int geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(geneLowerFlank,43289);
        assertEquals(geneUpperFlank,50357);
        
        //gene2
        gene = new GeneralPosition.Builder(chr1,50877).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(geneLowerFlank,50358);
        assertEquals(geneUpperFlank,56716);

        //gene3
        gene = new GeneralPosition.Builder(chr1,92299).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(geneLowerFlank,91299);
        assertEquals(geneUpperFlank,96134);
        
        // gene4
        gene = new GeneralPosition.Builder(chr1,111655).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(geneLowerFlank,110655);
        assertEquals(geneUpperFlank,118497);
        
        // gene5
        gene = new GeneralPosition.Builder(chr1,118683).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(geneLowerFlank,118499);
        assertEquals(geneUpperFlank,120739);
        
        // gene6 - checking with original upperbound now
        gene = new GeneralPosition.Builder(chr1,122614).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(geneLowerFlank,121120);
        assertEquals(geneUpperFlank,123614);
        
        // gene7 - checking with original upperbound now
        gene = new GeneralPosition.Builder(chr1,139043).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(geneLowerFlank,137378);
        assertEquals(geneUpperFlank,140043);
   
        // gene8 - checking with original upperbound now
        gene = new GeneralPosition.Builder(chr1,199159).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(geneLowerFlank,195942);
        assertEquals(geneUpperFlank,199252);
        
        // gene9 - checking with original upperbound now
        gene = new GeneralPosition.Builder(chr1,205715).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(geneLowerFlank,199253);
        assertEquals(geneUpperFlank,206715);
        
        // gene10 - checking with a value from the middle of the original range
        gene = new GeneralPosition.Builder(chr1,210403).build();
        geneLowerFlank = flankingGeneMap.getEntry(gene).getKey().lowerEndpoint().getPosition();
        geneUpperFlank = flankingGeneMap.getEntry(gene).getKey().upperEndpoint().getPosition();
        assertEquals(geneLowerFlank,208979);
        assertEquals(geneUpperFlank,216403);
        
        System.out.println("\nFinished testCreateFlankingList!\n"); 
    }
    
    
 
    private static void privAddRangeMap(RangeMap<Position, String> geneRange, Range<Position> range, String gene) throws Exception{
        CreateIntervalsFileFromGffPlugin createIntervalsFileFromGFf = new CreateIntervalsFileFromGffPlugin();
        Class theClass = createIntervalsFileFromGFf.getClass();
        Method theMethod = theClass.getDeclaredMethod("addRange", new Class[] {RangeMap.class,Range.class,
                String.class});
        theMethod.setAccessible(true);
        theMethod.invoke(createIntervalsFileFromGFf,(Object)geneRange, (Object) range, (Object)gene);
    }
    
    private static RangeMap<Position,String> privCreateFlankingList(RangeMap<Position, String> geneRange,  int numFlanking,
            GenomeSequence myRefSequence) throws Exception{
        CreateIntervalsFileFromGffPlugin createIntervalsFileFromGFf = new CreateIntervalsFileFromGffPlugin();
        Class theClass = createIntervalsFileFromGFf.getClass();
        Method theMethod = theClass.getDeclaredMethod("createFlankingList", new Class[] {RangeMap.class, int.class,
                 GenomeSequence.class});
        theMethod.setAccessible(true);
        return (RangeMap<Position,String>)theMethod.invoke(createIntervalsFileFromGFf,(Object)geneRange, numFlanking, (Object)myRefSequence);
    }
}
