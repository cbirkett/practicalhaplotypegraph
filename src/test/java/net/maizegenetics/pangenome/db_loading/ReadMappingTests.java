package net.maizegenetics.pangenome.db_loading;

import net.maizegenetics.util.LoggingUtils;
import net.maizegenetics.util.Utils;
import org.junit.Before;
import org.junit.Test;
import java.io.BufferedWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class ReadMappingTests {
    String userHome = System.getProperty("user.home");
    String rmDBName= userHome + "/readMappingTestDB.db";
    String configFile = userHome + "/rmTestConfig.txt";

    String taxon1 = "taxon1";
    String taxon2 = "taxon2";
    String method1 = "method1";
    String method2 = "method2";
    String pathMethod1 = "pathMethod1";
    String pathMethod2 = "pathMethod2";
    int pathMethodType = DBLoadingUtils.MethodType.PATHS.value;

    String fgn1 = "fileGroupName_1";
    String fgn2 = "fileGroupName_2";
    int methodType = DBLoadingUtils.MethodType.ASSEMBLY_HAPLOTYPES.value;
    String methodDetails = "my method details";

    String stringData = "this is the test data";
    byte[] mappingData =stringData.getBytes();

    String pathsData = "1,15,62,78";
    byte[] pathBytes = pathsData.getBytes();

    @Before
    public void setup() {
        LoggingUtils.setupDebugLogging();
    }

    @Test
    public  void testReadMappingMethods() {
        System.out.println("Begin testReadMappingMethods\n");
        createTestData();

        // This will create the db with the current schema from the repository
        Connection dbConn = DBLoadingUtils.connection(configFile, true);

        // insert data
        try {
            // add taxon2
            System.out.println("Insert taxon2 to genotypes table ...");
            dbConn.setAutoCommit(true);
            StringBuilder sb = new StringBuilder();
            sb.append("INSERT INTO genotypes (ploidy, is_reference, line_name, line_data,isPhasedAcrossGenes,isPhasedAcrossChromosomes) ");
            sb.append("values (2,0,'");
            sb.append(taxon2);
            sb.append("','genotype for creating the path',0,0);");
            System.out.println("QUery: " + sb.toString());
            dbConn.createStatement().executeUpdate(sb.toString());

            // add method1
            System.out.println("Insert method2 to methods table ...");
            sb.setLength(0);
            sb.append("insert into methods (name, method_type, description) values ('");
            sb.append(method2).append("', ");
            sb.append(methodType).append(", 'Test method');");
            System.out.println("QUery: " + sb.toString());
            dbConn.createStatement().executeUpdate(sb.toString());

            // add a read mapping entry
            sb.setLength(0);
        } catch (SQLException se) {
            // This helps debug when queries have a problem
            int count = 1;
            while (se != null) {
                System.out.println("SQLException " + count);
                System.out.println("Code: " + se.getErrorCode());
                System.out.println("SqlState: " + se.getSQLState());
                System.out.println("Error Message: " + se.getMessage());
                se = se.getNextException();
                count++;
            }
            throw new IllegalStateException("error loading db ");
        } catch (Exception exc) {
            throw new IllegalStateException("Error trying to insert taxon1 to db");
        }


        // DB is now populated - get PHGdbAccess object, call new methods
        PHGDataWriter phg = new PHGdbAccess(dbConn);

        // test when taxon is not present:
        int rmID = phg.getReadMappingId(taxon1, method1, fgn1);
        assertEquals(rmID,-1);

        System.out.println("\nCalling putReadMappingData ...");
        Map<String,String> methodMap = new HashMap<>();
        methodMap.put("notes",methodDetails);
        // This should add taxon1, which is not in db
        rmID = phg.putReadMappingData( method1,  methodMap,  taxon1,  fgn1,  mappingData,false,-1);

        // This should use taxon1 values
        int rmID2 = phg.putReadMappingData(method2, methodMap,taxon1, fgn2, mappingData,false,-1);

        // This should add method1 and create the 3rd read_mapping entry
        rmID = phg.putReadMappingData( method1,  methodMap,  taxon2,  fgn1,  mappingData,false,-1);
        assertEquals(rmID,3);

        // THis should succeed and create the 4th read_mapping entry
        rmID = phg.putReadMappingData(method2, methodMap, taxon2, fgn1, mappingData,false,-1);
        assertEquals(rmID,4);

        // This should succeed and give the same result as the rmID above.
        int rmIDresult = phg.getReadMappingId(taxon2, method2, fgn1);
        assertEquals(rmID,rmIDresult);

        System.out.println("\nCall updateReadMappingHash");
        phg.updateReadMappingHash();  // update the hash (verify this call doesn't throw an error)

        // insert paths data (new paths table for read_mapping)
        List<Integer> readMappings_2 = Arrays.asList(3, 4);

        System.out.println("Putting PathsData ...");
        Map<String,String> pluginParams = new HashMap<>();
        pluginParams.put("notes",methodDetails);
        // Should add another test where isTestMethod is true
        phg.putPathsData(pathMethod1,  pluginParams, taxon2,  readMappings_2,  pathBytes, false);

        System.out.println("Getting paths data ...");
        Map<String,byte[]> taxonWithPathsMap = phg.getTaxonPathsForMethod(pathMethod1);

        System.out.println("Number returned from getTaxonPathsForMethod: " + taxonWithPathsMap.size() + ", keySEt size=" + taxonWithPathsMap.keySet().size());

        for (String taxon : taxonWithPathsMap.keySet()) {
            byte[] pathBytes = taxonWithPathsMap.get(taxon);
            String pathString = new String(pathBytes);

            System.out.println("PathBytes for " + taxon + ":" + pathString);
        }

    }

    public void createTestData() {
        // Create db config file
        try (BufferedWriter bw = Utils.getBufferedWriter(configFile)) {
            StringBuilder sb = new StringBuilder();
            sb.append("host=localHost\n");
            sb.append("user=sqlite\n");
            sb.append("password=sqlite\n");
            sb.append("DB=").append(rmDBName).append("\n");
            sb.append("DBtype=sqlite\n");

            bw.write(sb.toString());
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }
}
