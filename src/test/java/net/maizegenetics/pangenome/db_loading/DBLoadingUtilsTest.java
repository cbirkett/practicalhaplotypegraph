package net.maizegenetics.pangenome.db_loading;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonObject;

import kotlin.Pair;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.pangenome.api.HaplotypeNode;
import net.maizegenetics.pangenome.api.HaplotypeSequence;
import net.maizegenetics.pangenome.api.ReferenceRange;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.TaxaListBuilder;
import org.junit.Assert;
import org.junit.Test;

import htsjdk.variant.variantcontext.Genotype;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.vcf.VCFFileReader;
import net.maizegenetics.pangenome.hapcollapse.GVCFUtils;
import net.maizegenetics.pangenome.processAssemblyGenomes.AssemblyHaplotypesPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.util.Utils;
import org.xerial.snappy.Snappy;

/**
 * This class contains tests for the methods in DBLoadingUtils class. 
 *
 * @author zrm22, lcj34
 */
public class DBLoadingUtilsTest {
    
    @Test
    public void testConnectToPostgres() {
        // NOTE - this test needs to be re-written to use AssemblyHaplotypesMultiThreadPlugin,
        //   which has different parameters.

        // NOTE: If the postgres connection fails, try stopping/starting the postgres connection via:
        // (this is on a MAC):
        //   pg_ctl -D /usr/local/var/postgres stop
        //   pg_ctl -D /usr/local/var/postgres start   
        
        // This test verifies loading genomes and an assembly to a postgres db.
        // it exercises the "INSERT .. ON CONFLICT code
        
        // This config file is postgres on laptop outside of a docker
        //String configFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/postgres/configPostgres.txt";
        
        // THis is sqlite incase that is needed for comparison.
        //String configFile = "/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/performance/configSQLite.txt";
 
        // THis config file is a postgres docker
        String configFile = "/Users/lcj34/notes_files/phg_2018/postgres_docker/phgTestPostgresDB/DataFolders/configPostgres.txt";
        //String refGenome = "/Volumes/Samsung_T1/wgs_pipeline/refGenomeFiles/Zea_mays.AGPv4.dna.toplevel.fa";
        String refGenome = "/Volumes/Time Machine Backup/refGenomes/Zea_mays.AGPv4.dna.toplevelMtPtv3.fa"; 
        String anchorBedFile = "/Users/lcj34/notes_files/phg_2018/new_features/genomeIntervals_rework/testing/createdIntervalsFile_closedFix_rework.txt";
        String genomeDataFile2 = "/Users/lcj34/notes_files/phg_2018/dbSchema_updates/phg213_genomeIntervals/B73Ref_load_data.txt";
        
        DataSet dbConnectDS = new GetDBConnectionPlugin(null,false) 
                .configFile(configFile)
                .createNew(true)
                .performFunction(null);
                
        System.out.println("Loaindg ref genome ...");
        new LoadAllIntervalsToPHGdbPlugin()
            .anchors(anchorBedFile)
            .refGenome(refGenome)
            .genomeData(genomeDataFile2) // loading June 26, 2018 and later
            .performFunction(dbConnectDS);
        
        System.out.println("Finished loading Ref");
        System.out.println("Loading Assembly ...");
        new AssemblyHaplotypesPlugin()
        .ref("/Users/lcj34/notes_files/phg_2018/b73/b73chr1.fa")
        .assembly("/Users/lcj34/notes_files/phg_2018/b73/w22ngchr1.fa")
        .outputDir("/Users/lcj34/notes_files/phg_2018/assembly_testing/debug/performance/DockerOutput/align/")
        .dbConfigFile(configFile)
        .assemblyName("W22NG_Assembly")
        .chrom("1")
        .entryPoint(AssemblyHaplotypesPlugin.ASSEMBLY_ENTRY_POINT.haplotypes)
        .performFunction(null);
        
        // LoadGEnomeIntervalsToPHGdbPlugin closes the connection - reopen and test that data was populated  
        System.out.println("Testing db");
        Connection connection = DBLoadingUtils.connection(configFile,false);
        String query = "select count(*) as ref_range_count from reference_ranges";
        try {
            ResultSet rs = connection.createStatement().executeQuery(query);
            int refRangeCount = 0;
            if (rs.next()) {
                refRangeCount = rs.getInt("ref_range_count");
            }
            
            System.out.println("After loading intervals to postgres DB, number of reference ranges in table: " + refRangeCount);
            assertTrue(refRangeCount > 75000);
            rs.close();
            query = "select count(*) as hap_count from haplotypes where gamete_grp_id=2";
            rs = connection.createStatement().executeQuery(query);
            int hapCount = 0;
            if (rs.next()) {
                hapCount = rs.getInt("hap_count");
            }
            System.out.println("After loading w22 assembly to postgres DB, number of w22 haps in table: " + hapCount);
            connection.close();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
        
        System.out.println("DONE ");
    }

    
    @Test
    public void testConnectToSQLite() {
       // String dbName = "phgFromSQLite";
        String configProperitesFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/postgres/configSQlite.txt";
 
        boolean deleteOld = true;
        Connection connection = DBLoadingUtils.connection(configProperitesFile,deleteOld);
        //Connection connection = DBLoadingUtils.connection( host,  user,  pwd,  dbName,  "postgres");
        if (connection == null) System.out.println("\ntestConnectToSQLite: Could not get connection for config file db");
        else {
            System.out.println("Got connection for testdb_fromjava !!");

        }
        
        JsonObject json = Json.createObjectBuilder().add("geneQuality","poor").build();
        
        
        // try writing to json
 
        String query = "INSERT into test_json (some_data) values ('" + json + "')";
        //String query = "INSERT into genome_interval_versions (name,description) values ('Sally','sally description')";
        try {
            connection.createStatement().executeUpdate(query);
            connection.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println("DONE ");
    }
    
    @Test
    public void testSerializingVCFFile() {
        String vcfFileName = "data/simpleGVCFTestFile.g.vcf";
//        vcfFileName = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/consensus_chr10_stPos100051625_endPos100054362_B73.vcf.gz";
//        vcfFileName = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/TestBigFile/B97_haplotype_caller_output_filtered.g.vcf";
//        vcfFileName = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/TestBigFile/M37W_haplotype_caller_output_filtered.g.vcf";
        VCFFileReader vcfReader = new VCFFileReader(new File(vcfFileName), false);
        List<VariantContext> listOfVariants = vcfReader.iterator().stream().collect(Collectors.toList());


        byte[] vcfEncoded = DBLoadingUtils.encodeVCFFileToByteArray(vcfFileName,false,false);

        System.out.println("Number of bytes:"+vcfEncoded.length);

        List<VariantContext> vcfDecoded = DBLoadingUtils.decodeByteArrayToListOfVariantContext(vcfEncoded);

        for(int i = 0; i < vcfDecoded.size(); i++) {
            //setup the assertions to make sure the serialized version works
            VariantContext encodedVC = vcfDecoded.get(i);
            VariantContext originalVC = listOfVariants.get(i);

            testVariantContextsEqual(encodedVC,originalVC);

            if(i==0) {
                System.out.println(encodedVC);
                System.out.println(originalVC);
            }

//            System.out.println(vcfDecoded.get(i).getGenotype("B73"));
//            System.out.println(listOfVariants.get(i).getGenotype("B73"));
        }
    }


    @Test
    public void testSerializingVCFFileOnlyVariants() {
        String vcfFileName = "data/simpleGVCFTestFile.g.vcf";
        VCFFileReader vcfReader = new VCFFileReader(new File(vcfFileName), false);
        List<VariantContext> listOfVariants = vcfReader.iterator().stream().filter(variantContext -> variantContext.isVariant()).collect(Collectors.toList());


        byte[] vcfEncoded = DBLoadingUtils.encodeVCFFileToByteArray(vcfFileName,true,false);

        System.out.println("Number of bytes:"+vcfEncoded.length);

        List<VariantContext> vcfDecoded = DBLoadingUtils.decodeByteArrayToListOfVariantContext(vcfEncoded);

        for(int i = 0; i < vcfDecoded.size(); i++) {
            //setup the assertions to make sure the serialized version works
            VariantContext encodedVC = vcfDecoded.get(i);
            VariantContext originalVC = listOfVariants.get(i);

            testVariantContextsEqual(encodedVC,originalVC);

            if(i==0) {
                System.out.println(encodedVC);
                System.out.println(originalVC);
            }
        }
    }

    @Test
    public void testSerializingVCFFileMergedRefs() {
        String vcfFileName = "data/simpleGVCFTestFile.g.vcf";
        vcfFileName = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/consensus_chr10_stPos100051625_endPos100054362_B73.vcf.gz";
//        vcfFileName = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/TestBigFile/B97_haplotype_caller_output_filtered.g.vcf";
//        vcfFileName = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/TestBigFile/M37W_haplotype_caller_output_filtered.g.vcf";
        VCFFileReader vcfReader = new VCFFileReader(new File(vcfFileName), false);
        List<VariantContext> listOfVariants = vcfReader.iterator().stream().collect(Collectors.toList());

        listOfVariants = GVCFUtils.convertVCFToGVCF(listOfVariants);


        byte[] vcfEncoded = DBLoadingUtils.encodeVCFFileToByteArray(vcfFileName,false,true);

        System.out.println("Number of bytes:"+vcfEncoded.length);

        List<VariantContext> vcfDecoded = DBLoadingUtils.decodeByteArrayToListOfVariantContext(vcfEncoded);

        for(int i = 0; i < vcfDecoded.size(); i++) {
            //setup the assertions to make sure the serialized version works
            VariantContext encodedVC = vcfDecoded.get(i);
            VariantContext originalVC = listOfVariants.get(i);

            testVariantContextsEqual(encodedVC,originalVC);

            if(i==0) {
                System.out.println(encodedVC);
                System.out.println(originalVC);
            }

//            System.out.println(vcfDecoded.get(i).getGenotype("B73"));
//            System.out.println(listOfVariants.get(i).getGenotype("B73"));
        }
    }

    private void testVariantContextsEqual(VariantContext encodedVC, VariantContext originalVC) {
        //Check Chrom
        Assert.assertTrue("Serializing VCF file fail, chromosomes does not match:",encodedVC.getContig().equals(originalVC.getContig()));
        //Check start pos,endpos
        Assert.assertTrue("Serializing VCF file fail, startPosition does not match:",encodedVC.getStart()==originalVC.getStart());
        Assert.assertTrue("Serializing VCF file fail, endPosition does not match:",encodedVC.getEnd()==originalVC.getEnd());
        //Check GT, AD, DP, GQ and PL and make sure they match
        Genotype encodedGT = encodedVC.getGenotype(0);
        Genotype originalGT = originalVC.getGenotype(0);
        Assert.assertTrue("Serializing VCF file fail, GT call does not match:",encodedGT.getGenotypeString().equals(originalGT.getGenotypeString()));
        Assert.assertTrue("Serializing VCF file fail, DP call does not match:",encodedGT.getDP()==originalGT.getDP());
        Assert.assertTrue("Serializing VCF file fail, GQ call does not match:",encodedGT.getGQ()==originalGT.getGQ());
        Assert.assertArrayEquals("Serializing VCF file fail, AD call does not match:",originalGT.getAD(),encodedGT.getAD());
        Assert.assertArrayEquals("Serializing VCF file fail, PL call does not match:",originalGT.getPL(),encodedGT.getPL());
    }


    @Test
    public void testMergingRefRanges() {
        String vcfFileName = "data/simpleGVCFTestFile.g.vcf";
        vcfFileName = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/consensus_chr10_stPos100051625_endPos100054362_B73.vcf";
//        vcfFileName = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/TestBigFile/B97_haplotype_caller_output_filtered.g.vcf";
//        vcfFileName = "/Volumes/ZackBackup/Temp/Pangenome/InbredHaplotyperPipeline/HapCallingTestFiles/TestBigFile/M37W_haplotype_caller_output_filtered.g.vcf";
        VCFFileReader vcfReader = new VCFFileReader(new File(vcfFileName), false);
        List<VariantContext> listOfVariants = vcfReader.iterator().stream().collect(Collectors.toList());

        List<VariantContext> variantsMerged = GVCFUtils.convertVCFToGVCF(listOfVariants);

        int counter = 0;
        for(VariantContext vc : variantsMerged) {
            if(counter <60) {
                System.out.println(vc.toString());
            }
            counter++;
        }
    }

    // LCJ - test becomes obsolete as the haplotype_counts table has been removed
    // in favor of the read_mapping tables.
//    @Test
//    public void testEncodeHapDecodeCountsArrayFromFile( ){
//        String countsFile = System.getProperty("user.home") + "/temp/countsFileInput.txt";
//        try (BufferedWriter bw = Utils.getBufferedWriter(countsFile)) {
//            // Create the file we're going to read
//            StringBuilder countsSB = new StringBuilder();
//            for (int idx = 1; idx < 51; idx++) {
//                String line = idx + "\t" + (idx+5) + "\t" + (idx+10) + "\n";
//                countsSB.append(line);
//            }
//            bw.write(countsSB.toString());
//        } catch (Exception exc) {
//            exc.printStackTrace();
//        }
//        String inputFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/path_haploytpeCounts_code/countsDir/CML247:2005_D0E3PACXX_2_TCACC.txt";
//        //String inputFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/path_haploytpeCounts_code/countsDir/cml247Short.txt";
//
//        byte[] hapCountsFromFile = DBLoadingUtils.encodeHapCountsArrayFromFile(countsFile);
//
//        // Reverse it
//        int[][] decodedData = DBLoadingUtils.decodeHapCountsArray(hapCountsFromFile);
//        assertEquals(decodedData.length,3);
//        assertEquals(decodedData[0].length, 50); // match lines  added above
//
//        assertEquals(decodedData[0][0],1);
//        assertEquals(decodedData[1][0],6);
//        assertEquals(decodedData[2][0],11);
//
//        assertEquals(decodedData[0][11],12);
//        assertEquals(decodedData[1][11],17);
//        assertEquals(decodedData[2][11],22);
//
//        assertEquals(decodedData[0][49],50);
//        assertEquals(decodedData[1][49],55);
//        assertEquals(decodedData[2][49],60);
//
//        String configFile = "/Users/lcj34/notes_files/repgen/wgs_pipeline/path_haploytpeCounts_code/configSQLite_junit.txt";
//        Connection conn = DBLoadingUtils.connection(configFile, false);
//
//        // clear the haplotype_counts table for a clean test:
//        conn = DBLoadingUtils.connection(configFile,  false);
//        try {
//            String query = "delete from haplotype_counts;";
//            conn.createStatement().executeUpdate(query);
//        } catch (Exception exc) {
//            throw new IllegalStateException("Could not delete data from haplotype_counts table");
//        }
//
//        PHGDataWriter phg = new PHGdbAccess(conn);
//
//        phg.putHaplotypeCountsData( "counts_method", "some_detail", "CML228:2004_D0E3PACXX_2_AGTGGA", "myFastQFile", hapCountsFromFile);
//
//        try {
//            ((PHGdbAccess)phg).close();
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//
//        // Verify can get this back out
//        conn = DBLoadingUtils.connection(configFile,  false);
//        try {
//            String query = "select data from haplotype_counts where haplotype_counts_id=1";
//            System.out.println("Processing query: " + query);
//            ResultSet rs = conn.createStatement().executeQuery(query);
//            byte[] encodedBytesFromDB = rs.getBytes(1);
//            int[][] decodedDBBytes = DBLoadingUtils.decodeHapCountsArray(encodedBytesFromDB);
//
//            assertEquals(decodedDBBytes.length,3);
//            assertEquals(decodedDBBytes[0].length, 50); // match lines  added above
//
//            assertEquals(decodedDBBytes[0][0],1);
//            assertEquals(decodedDBBytes[1][0],6);
//            assertEquals(decodedDBBytes[2][0],11);
//
//            assertEquals(decodedDBBytes[0][11],12);
//            assertEquals(decodedDBBytes[1][11],17);
//            assertEquals(decodedDBBytes[2][11],22);
//
//            assertEquals(decodedDBBytes[0][49],50);
//            assertEquals(decodedDBBytes[1][49],55);
//            assertEquals(decodedDBBytes[2][49],60);
//
//        } catch (SQLException sqle) {
//            sqle.printStackTrace();
//        }
//
//    }
    
    @Test
    public void testVerifyIntervalRanges( ){
        String anchorFile = System.getProperty("user.home") + "/temp/testAnchorFile.txt";
        try (BufferedWriter bw = Utils.getBufferedWriter(anchorFile)) {
            // Create the file we're going to verify
            StringBuilder anchorSB = new StringBuilder();
            anchorSB.append("chr1\t10575\t13198\n");
            anchorSB.append("chr1\t20460\t29234\n");
            anchorSB.append("chr1\t141765\t145627\n");
            anchorSB.append("chr1\t143661\t146302\n"); // overlaps above
            anchorSB.append("chr1\t144363\t148863\n"); // overlaps above
            anchorSB.append("chr1\t175219\t177603\n"); 
            anchorSB.append("chr2\t81176\t84375\n");
            anchorSB.append("chr2\t82776\t87024\n"); // overlaps first chr2 anchor
            anchorSB.append("chr2\t108577\t113286\n");
            anchorSB.append("chr3\t116671\t122941\n");
            anchorSB.append("chr3\t140393\t145805\n");
            anchorSB.append("chr3\t159053\t164568\n");
            anchorSB.append("chr5\t158053\t164568\n"); // overlap position, but different chrom.  OK
            
            bw.write(anchorSB.toString());
        } catch (Exception exc) {
            exc.printStackTrace();
        }                
        
        Set<String> overlaps = DBLoadingUtils.verifyIntervalRanges(anchorFile);

        
        // There should be 3 entries on the overlap list
        assertEquals(overlaps.size(),3);
        System.out.println("Entries on list:");
        overlaps.stream().forEach(entry -> {
            System.out.println(entry);
        });
        boolean containsEntry = overlaps.contains("chr1\t143661\t146302");
        assertEquals(containsEntry,true);
        
        containsEntry = overlaps.contains("chr5\t158053\t164568");
        assertEquals(containsEntry,false);
        
        // Do again, but have the intervals out of order in the anchor file.
        try (BufferedWriter bw = Utils.getBufferedWriter(anchorFile)) {
            // Create the file we're going to verify
            StringBuilder anchorSB = new StringBuilder();
            anchorSB.append("chr1\t141765\t145627\n");            
            anchorSB.append("chr1\t20460\t29234\n");           
            anchorSB.append("chr1\t143661\t146302\n"); // overlaps first entry
            anchorSB.append("chr1\t10575\t13198\n");
            anchorSB.append("chr1\t144363\t148863\n"); // overlaps above 2
            anchorSB.append("chr1\t175219\t177603\n"); 
            anchorSB.append("chr2\t82776\t87024\n"); // overlaps entry below
            anchorSB.append("chr2\t81176\t84375\n");            
            anchorSB.append("chr2\t108577\t113286\n");
            anchorSB.append("chr3\t116671\t122941\n");
            anchorSB.append("chr3\t140393\t145805\n");
            anchorSB.append("chr3\t159053\t164568\n");
            anchorSB.append("chr5\t158053\t164568\n"); // overlap position, but different chrom.  OK
            
            bw.write(anchorSB.toString());
        } catch (Exception exc) {
            exc.printStackTrace();
        }                
        
        overlaps = DBLoadingUtils.verifyIntervalRanges(anchorFile);
        
        // There should be 3 entries on the overlap list - same as test above
        assertEquals(overlaps.size(),3);
        System.out.println("Entries on list:");
        overlaps.stream().forEach(entry -> {
            System.out.println(entry);
        });
        containsEntry = overlaps.contains("chr1\t143661\t146302");
        assertEquals(containsEntry,true);
        
        containsEntry = overlaps.contains("chr5\t158053\t164568");
        assertEquals(containsEntry,false);
        
        // Write anchor file with NO overlaps
        try (BufferedWriter bw = Utils.getBufferedWriter(anchorFile)) {
            // Create the file we're going to verify
            StringBuilder anchorSB = new StringBuilder();
            anchorSB.append("chr1\t10575\t13198\n");
            anchorSB.append("chr1\t20460\t29234\n");
            anchorSB.append("chr1\t141765\t145627\n");
            anchorSB.append("chr1\t146661\t147302\n"); 
            anchorSB.append("chr1\t148363\t150863\n"); 
            anchorSB.append("chr1\t175219\t177603\n"); 
            anchorSB.append("chr2\t81176\t81375\n");
            anchorSB.append("chr2\t82776\t87024\n"); 
            anchorSB.append("chr2\t108577\t113286\n");
            anchorSB.append("chr3\t116671\t122941\n");
            anchorSB.append("chr3\t140393\t145805\n");
            anchorSB.append("chr3\t159053\t164568\n");
            anchorSB.append("chr5\t158053\t164568\n"); 
            
            bw.write(anchorSB.toString());
        } catch (Exception exc) {
            exc.printStackTrace();
        }   
        
        overlaps = DBLoadingUtils.verifyIntervalRanges(anchorFile);
        
        //  should be 0 entries on the overlap list
        assertEquals(overlaps.size(),0);
        
    }
    
    @Test
    public void testCreateInitialAlleles() {
        // Test list of alleles - should create kmers of the length we need
        
        // THe number of alleles created is based on maxKmerLen, and will be
        // int numKmers = 0;
        // for (int i = 1; i <= maxKmerLen; i++ ) }
        //     numKmer += numKmer(to the i);
        // } // end
        
        int maxKmerLen = 2;
        List<String> myAlleles = DBLoadingUtils.createInitialAlleles(maxKmerLen);
        System.out.println("\nSize of allele list with 2mers: " + myAlleles.size());
        
        double numberOfAlleles = 0;
        for (int idx = 1; idx <= maxKmerLen; idx++) {
            numberOfAlleles += Math.pow(5, idx); // 5 is for A,C,G,T,N
        }
        assertEquals((int)numberOfAlleles,myAlleles.size()); 
        myAlleles.forEach(System.out::println);
        
        // test with 3
        maxKmerLen = 3;
        myAlleles = DBLoadingUtils.createInitialAlleles(maxKmerLen);
        System.out.println("\nSize of allele list with 3mers: " + myAlleles.size());
        
        numberOfAlleles = 0;
        for (int idx = 1; idx <= maxKmerLen; idx++) {
            numberOfAlleles += Math.pow(5, idx); // 5 is for A,C,G,T,N
        }
        assertEquals((int)numberOfAlleles,myAlleles.size()); 
        myAlleles.forEach(System.out::println);
        
        // test with 4
        maxKmerLen = 4;
        myAlleles = DBLoadingUtils.createInitialAlleles(maxKmerLen);
        
        
        numberOfAlleles = 0;
        for (int idx = 1; idx <= maxKmerLen; idx++) {
            numberOfAlleles += Math.pow(5, idx); // 5 is for A,C,G,T,N
        }
        assertEquals((int)numberOfAlleles,myAlleles.size()); 
        myAlleles.forEach(System.out::println);
        System.out.println("\nSize of allele list with 4mers: " + myAlleles.size());
               
    }


    @Test
    public void testEncodeandDecodePathArrayForMultipleLists() {
        // HaplotypeNode(HaplotypeSequence haplotypeSequence, TaxaList taxaList, int id, byte[] encodedVariants)
        //  HaplotypeSequence getInstance(String sequence, ReferenceRange referenceRange, double qualityScore, String seqHash)
        // ReferenceRange(String referenceName, Chromosome chromosome, int start, int end, int id)

        ReferenceRange refRange1 = new ReferenceRange("ref1", Chromosome.instance("1"),1,20, 1);
        ReferenceRange refRange2 = new ReferenceRange("ref2", Chromosome.instance("2"),1,20, 2);
        ReferenceRange refRange3 = new ReferenceRange("ref3", Chromosome.instance("3"),1,20, 3);
        ReferenceRange refRange4 = new ReferenceRange("ref4", Chromosome.instance("1"),1,20, 4);
        ReferenceRange refRange5 = new ReferenceRange("ref5", Chromosome.instance("1"),1,20, 5);

        String seq1 = "ACGTTGGCCAATAGC";
        String seq2 = "TTCCGGAATAGCAT";
        String seq3 = "GGGGAAAATTTTCCC";
        String seq4 = "AAAAGGGGTTTTCCC";
        String seq5 = "ACACACTGTGTGTAGC";

        HaplotypeSequence hapSeq1 = HaplotypeSequence.getInstance(seq1,refRange1, 34.5, "RTYU");
        HaplotypeSequence hapSeq2 = HaplotypeSequence.getInstance(seq2,refRange2, 34.5, "TYRU");
        HaplotypeSequence hapSeq3 = HaplotypeSequence.getInstance(seq3,refRange3, 34.5, "DFGH");
        HaplotypeSequence hapSeq4 = HaplotypeSequence.getInstance(seq4,refRange4, 34.5, "GHJL");
        HaplotypeSequence hapSeq5 = HaplotypeSequence.getInstance(seq5,refRange5, 34.5, "VVBM");

        String variants = "thisIsThe Variants";
        byte[] encodedVariants = variants.getBytes(); // this is just trash for testing

        TaxaList tl = new TaxaListBuilder().addAll(Arrays.asList("B73_0:CML247_0:Mo17_0")).build();
        TaxaList t2 = new TaxaListBuilder().addAll(Arrays.asList("B73_0:CML247_0")).build();
        HaplotypeNode node1 = new HaplotypeNode(hapSeq1, tl,6, "0", 0, 0, ".",-1, encodedVariants, null);
        HaplotypeNode node2 = new HaplotypeNode(hapSeq2, tl, 7, "0", 0, 0, ".",-1, encodedVariants, null);
        HaplotypeNode node3 = new HaplotypeNode(hapSeq3, tl, 8, "0", 0, 0, ",",-1, encodedVariants, null);
        HaplotypeNode node4 = new HaplotypeNode(hapSeq4, tl, 9, "0", 0, 0, ".", -1, encodedVariants, null);
        HaplotypeNode node5 = new HaplotypeNode(hapSeq5, tl, -1, "0", 0, 0, ".", -1, encodedVariants, null);

        List<List<HaplotypeNode>> diploidData = new ArrayList<List<HaplotypeNode>>();

        List<HaplotypeNode> firstList = new ArrayList<HaplotypeNode>(Arrays.asList(node1, node2, node4));
        List<HaplotypeNode> secondList = new ArrayList<HaplotypeNode>(Arrays.asList(node2, node3, node4));
        List<HaplotypeNode> thirdList = new ArrayList<HaplotypeNode>(Arrays.asList(node2, node5, node3));

        diploidData.add(firstList);
        diploidData.add(secondList);

        System.out.println("running DBLoadingUtils.encodePathArrayForMultipleLists");
        byte[] diploidDataAsBytes = DBLoadingUtils.encodePathArrayForMultipleLists(diploidData);

        // Now decode it back to a List<List<Haplotypenode>>
        System.out.println("running DBLoadingUtils.decodePathARrayFromultipleLists");

        List<List<Integer>> decodedLists = DBLoadingUtils.decodePathsForMultipleLists(diploidDataAsBytes);
        assertEquals(2,decodedLists.size());
        List<Integer> list1 = decodedLists.get(0);
        List<Integer> list2 = decodedLists.get(1);

        assertEquals(3,list1.size());
        assertEquals(3, list2.size());

        int list1Item1 = list1.get(0);
        assertEquals(6,list1Item1);
        int list1Item2 = list1.get(1);
        assertEquals(7,list1Item2);

        int list2Item2 = list2.get(1);
        assertEquals(8,list2Item2);

        System.out.println("Begin second test - uneven lists, including a -1 hapid");
        diploidData.clear();
        diploidData.add(firstList);
        diploidData.add(thirdList);

        diploidDataAsBytes = DBLoadingUtils.encodePathArrayForMultipleLists(diploidData);
        decodedLists = DBLoadingUtils.decodePathsForMultipleLists(diploidDataAsBytes);

        assertEquals(2,decodedLists.size());
        list1 = decodedLists.get(0);
        list2 = decodedLists.get(1);

        assertEquals(3,list1.size());
        assertEquals(2, list2.size()); // the -1 is dropped, so list2 has only 1 entries

        list2Item2 = list2.get(1);
        assertEquals(8,list2Item2);
        System.out.println("\nFInished!!");

    }

    @Test
    public void testDecodeOldPathsWithNewMethod() {

        ByteBuffer bBuff=ByteBuffer.allocate((Integer.SIZE/8) + Integer.SIZE/8 * 20 );
        bBuff.putInt(20); // store the size (number of hapids) as first value
        for (int idx = 1; idx < 21; idx++) {
            bBuff.putInt(idx);
        }

        try {
            byte[] dataAsByteArray= Snappy.compress(Arrays.copyOf(bBuff.array(), bBuff.position()));

            // Data is encoded using old method, decode it using MultipleLists method
            List<List<Integer>> decodedLists = DBLoadingUtils.decodePathsForMultipleLists(dataAsByteArray);
            assertEquals(1,decodedLists.size());
            List<Integer> list1 = decodedLists.get(0);

            assertEquals(20, list1.size());
            int list1Item1 = list1.get(0);
            assertEquals(1,list1Item1);
            int list1Item2 = list1.get(1);
            assertEquals(2,list1Item2);
            int list1Item15 = list1.get(14);
            assertEquals(15,list1Item15);

            System.out.println("\nFinished Successfully !");
        } catch (IOException e) {
            throw new IllegalStateException("Could not compress byte array:");
        }
    }

    @Test
    public void testformatMethodParamsToJSON() {

        Map<String,String> pluginParams = new HashMap<String,String>();
        pluginParams.put("userNotes","param values based on analysis X and reason Y for NAM");
        pluginParams.put("param1","0.01");
        pluginParams.put("param2","true");
        pluginParams.put("param3","parallel_asmKeyFile.txt");

        String jsonStringOutput = DBLoadingUtils.formatMethodParamsToJSON(pluginParams);
        System.out.println(jsonStringOutput);
        assertTrue(jsonStringOutput.contains("userNotes"));

        // JsonString has comma as separator. With 4 items, there should be 3 commas
        long count = jsonStringOutput.chars().filter(ch -> ch == ',').count();
        assertEquals(3,count);

        // JsonString separates key/value with a colon.  The list should have 4 parameters,
        // hence 4 colons.  (This test won't work if the key or values have colons in them)
        count = jsonStringOutput.chars().filter(ch -> ch == ':').count();
        assertEquals(4,count);

        Map<String,String> paramsAsMap = DBLoadingUtils.parseMethodJsonParamsToString(jsonStringOutput);
        assertEquals(4,paramsAsMap.keySet().size());

        //Check the map returned contains all the values we originally stored to the JSON string
        pluginParams.keySet().stream().forEach( key -> {
            assertTrue(paramsAsMap.containsKey(key));
        });

    }

    @Test
    public void testNonJSONParse() {
        String nonJSON = "here is a user desc that is not json";
        Map<String,String> paramsMap = null;
        try {
            paramsMap = DBLoadingUtils.parseMethodJsonParamsToString(nonJSON);
            assertTrue(paramsMap.containsKey("notes"));
            assertEquals(1, paramsMap.keySet().size());
        } catch (Exception exc){
            fail("testNonJSONParse failed - exception should have been caught in parseMethodJsonParamsToString");
            exc.printStackTrace();
        }
    }

    @Test
    public void testGetMethodFromDescription() {
        String configFile = "/Users/lcj34/notes_files/phg_2018/peter_pipeline/configSQLite.txt";

        Connection conn = DBLoadingUtils.connection(configFile, false);
        PHGDataWriter phg = new PHGdbAccess(conn);

        System.out.println("\nCheck for 9 parameters for method GATK_PIPELINE");
        Map<String,String> paramsMap = phg.getMethodDescriptionFromName("GATK_PIPELINE");
        for (String key : paramsMap.keySet()) {
            System.out.println("param=" + key + ", value=" + paramsMap.get(key));
        }
        assertEquals(9, paramsMap.keySet().size());

        // now check it can properly get the non-json string that was added
        System.out.println("\nChecking for a single parameter for method notJson:");
        paramsMap = phg.getMethodDescriptionFromName("notJson");

        for (String key : paramsMap.keySet()) {
            System.out.println("param=" + key + ", value=" + paramsMap.get(key));
        }
        assertEquals(1, paramsMap.keySet().size());

        // Finally, check it returns null if the method isn't there.
        System.out.println("\nChecking for null for LynnNonExistent:");
        paramsMap = phg.getMethodDescriptionFromName("LynnNonExistent");

        if (paramsMap == null) {
            System.out.println("paramsMap was null! - good");
        } else {
            fail("ParamsMap should have been NULL for LynnNonExistent");
        }

    }
}
