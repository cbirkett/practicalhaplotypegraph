package net.maizegenetics.pangenome.api

import com.google.common.collect.*
import net.maizegenetics.analysis.imputation.EmissionProbability
import net.maizegenetics.pangenome.hapCalling.HapIdSetCount
import net.maizegenetics.pangenome.hapCalling.SingleHapMapping
import org.apache.commons.math3.distribution.BinomialDistribution
import java.lang.IllegalArgumentException
import java.util.*
import kotlin.math.roundToInt

/**
 * @author Peter Bradbury
 *
 * The HaplotypeEmissionProbability class extends EmissionProbability. It provides methods to calculate an
 * emission probability that is used in a hidden Markov model (HMM) for imputing haplotypes. The constructor takes the
 * following arguments:
 * @param nodeTree  A map of ReferenceRange -> list of haplotype nodes in that range
 * @param readMap   A Multimap of ReferenceRange -> collection of SingleHapMapping, which represents a read and
 * the haplotypes that align to it
 * @param pCorrect  The probability that a read has been aligned correctly
 *
 */

class HaplotypeEmissionProbability(val nodeTree : NavigableMap<ReferenceRange, List<HaplotypeNode>>,
                                   val readMap : Multimap<ReferenceRange, HapIdSetCount>,
                                   val pCorrect : Double) : EmissionProbability() {
    val refRanges = ArrayList<ReferenceRange>(nodeTree.keys);
    var currentRefRangeIndex = -1
    var currentEmissionProbabilities = doubleArrayOf(0.0)
    val noReadProbabilityMap: RangeMap<Int,Pair<Double,Double>>
    var minProbability : Double = 0.0

    init {
        val builder = ImmutableRangeMap.builder<Int,Pair<Double,Double>>()
        builder.put(Range.closedOpen(0, 500), Pair(.99, 0.97))
        builder.put(Range.closedOpen(500, 1000), Pair(.99, 0.72))
        builder.put(Range.closedOpen(1000, 1500), Pair(0.97, 0.26))
        builder.put(Range.closedOpen(1500, 2000), Pair(0.94, 0.086))
        builder.put(Range.closedOpen(2000, 3000), Pair(0.93, 0.036))
        builder.put(Range.closedOpen(3000, 4000), Pair(0.896, 0.0187))
        builder.put(Range.closedOpen(4000, 500000000), Pair(0.76, 0.0071))
        noReadProbabilityMap = builder.build()
    }

    override fun getProbObsGivenState(haplotype: Int, refRange: Int): Double {
        //refrange is a 0..n index into nodeTree.keys
        //haplotype is an index into the List<HaplotypeNode>> for this ReferenceRange

        if (currentRefRangeIndex != refRange) {
            currentRefRangeIndex = refRange
            currentEmissionProbabilities = calculateFromHapSetCounts()

        }
        if (haplotype < 0) return minProbability
        return currentEmissionProbabilities[haplotype]
    }

    override fun getProbObsGivenState(state: Int, obs: Int, node: Int): Double {
        return this.getProbObsGivenState(state, node)
    }

    fun calculateLengthWeightedEmissionProbabilities(): DoubleArray {
        /*
        * If an alternate haplotype (not the correct one) contains a large enough insertion
        * not present in the correct haplotype,
        * then a low frequency of reads could hit that insertion but not the correct haplotype
        * appearing to exclude the correct haplotype.
        * If a read hits nodes other than the one with this haplotypeId and does not hit this one,
        * find the minOtherHaplotypeLength for those other nodes. Calculate a weighting factor as
        * weightFactor = max(1, thisHaplotypeLength / minOtherHaplotypeLength)
        * Let allReadSum = number of reads hitting this Haplotype + sum(weightFactors for other reads)
        * Return p = binom(trials = floor(allReadSum, successes = numberOfHits, prob = pCorrect)
        *
        * The result is that exclusions for the shortest read are weighted as one. Exclusions for other reads are upweighted.
        * */
        val myRefRange = refRanges[currentRefRangeIndex];
        val nodes = nodeTree.get(myRefRange)
        if (nodes == null || nodes.size == 0) throw IllegalArgumentException("$myRefRange contains no HaplotypeNodes.")

        val numberOfHaplotypes = nodes.size
        val myHapMappings: Collection<HapIdSetCount>? = readMap.get(myRefRange)
        if (myHapMappings == null || myHapMappings.size == 0) {
            minProbability = 1.0 / numberOfHaplotypes.toDouble()
            return DoubleArray(numberOfHaplotypes, {ndx -> minProbability})

        } else {

            //make a map of haplotypeId -> haplotype length
            val haplotypeLengthMap = nodes.map { Pair(it.id(), it.haplotypeSequence().length()) }.toMap()

            //get a count of hapids
            val hapidCounts : Multiset<Int> = HashMultiset.create()
            for (setCount in myHapMappings) for (hapid in setCount.hapIdSet) hapidCounts.add(hapid, setCount.count)

            //calculate weighted exclusion counts for each haplotype
            val probabilities = DoubleArray(numberOfHaplotypes)
            var numberOfHaplotypesWithNonZeroReads = 0;
            var sumOfProbabilities = 0.0
            val zeroReadIndices = ArrayList<Int>();
            val nonZeroReadIndices = ArrayList<Int>();

            for (ndx in 0 until numberOfHaplotypes) {
                var haplotypeInclusionCount: Int = 0
                var haplotypeExclusionCount: Double = 0.0

                //TODO rework weightings
                for (mapping in myHapMappings) {
                    //minimum sequence length for mapping

                    if (mapping.hapIdSet.contains(nodes[ndx].id())) { //this read is an inclusion
                        haplotypeInclusionCount++
                    } else {  //this read is an exclusion, calculate a weighting factor and add it to exclusion counts
                        //the minimum haplotype length of all the hapids
                        val minLength = mapping.hapIdSet.map { haplotypeLengthMap.getOrDefault(it, Int.MAX_VALUE) }.minOrNull()

                        if (minLength != null)
                            haplotypeExclusionCount += minOf(1.0, nodes[ndx].haplotypeSequence().length().toDouble() / minLength.toDouble())
                    }


                }

                //if inclusion count + exclusion count < 0.5 treat this haplotype as having 0 reads (neither included or excluded)
                val numberOfReads: Int = haplotypeInclusionCount + haplotypeExclusionCount.roundToInt()
                if (numberOfReads > 0) {
                    val binom = BinomialDistribution(numberOfReads, pCorrect)
                    probabilities[ndx] = binom.probability(haplotypeInclusionCount);
                    numberOfHaplotypesWithNonZeroReads++;
                    sumOfProbabilities += probabilities[ndx];
                    nonZeroReadIndices.add(ndx);
                } else zeroReadIndices.add(ndx)
            }

            //replace the zero read lines with min prob
            if (numberOfHaplotypesWithNonZeroReads == 0) {
                minProbability = 1.0 / numberOfHaplotypes.toDouble()
                return DoubleArray(numberOfHaplotypes, {ndx -> minProbability})
            } else  {
                minProbability = nonZeroReadIndices.map{probabilities[it]}.minOrNull() ?: -1.0
                if (minProbability == -1.0) {
                    println("WARNING: no minimum probability for $myRefRange")
                    minProbability = 1.0 / numberOfHaplotypes.toDouble()
                    return DoubleArray(numberOfHaplotypes, {ndx -> minProbability})
                } else for (ndx in zeroReadIndices) probabilities[ndx] = minProbability
            }

            return probabilities

        }
    }

    fun calculateFromHapSetCounts() : DoubleArray {
        /*
        * P_emission = P(obs|haplotype)
        * = binom(#trials, #successes, P_success)
        * where #trials = number of reads
        *   #successes = number of reads hitting the haplotype
        *   P_success = pcorrect
        *
        * assign minProbability for haplotype index = -1
        * */

        val myRefRange = refRanges[currentRefRangeIndex]
        val nodes = nodeTree[myRefRange]
        if (nodes == null || nodes.size == 0) throw IllegalArgumentException("$myRefRange contains no HaplotypeNodes.")

        val numberOfHaplotypes = nodes.size
        val myHapMappings: Collection<HapIdSetCount>? = readMap[myRefRange]

        if (myHapMappings == null || myHapMappings.size == 0) {
            //minProbability has to be set because it will be used for hap index < 0 in getProbObsGivenState()
            //since there are no reads mapped all haplotypes have the same probability
            minProbability = 1.0 / numberOfHaplotypes.toDouble()
            return DoubleArray(numberOfHaplotypes, { ndx -> minProbability })

        } else {
            val numberOfReads = myHapMappings.map { it.count }.sum()
            val binom = BinomialDistribution(numberOfReads, pCorrect)
            minProbability = binom.probability(0) //probability of 0 reads hitting a haplotype

            //this next line maps hapid -> sum of counts for sets containing that hapid
            val hapidCountMap = myHapMappings.flatMap { hapSet -> hapSet.hapIdSet.map { Pair(it, hapSet.count)} }
                .groupingBy { it.first }
                .fold(0) { sum, pr -> sum + pr.second }

            //hapids that have no counts will not be in map so set number of successes to 0 for those
            return nodes.map { hapNode ->
                val numberOfSuccesses = hapidCountMap[hapNode.id()] ?: 0
                binom.probability(numberOfSuccesses)
            }.toDoubleArray()

        }

    }

}

/*
* calculates a weight exlusion count for external reporting
* The method used produces the same result as the emission probability but using different code.
* The purpose is to duplicate the calculation so that the weighted count can be available for diagnotics.
* @param graph  a HaplotypeGraph
* @param node   a HaplotypeNode from graph
* @param readMapping    the read map used to generate a path
* @return   a Triple of inclusion count, exclusion count, length weighted exclusion count
* */
fun calculateWeightedExclusionCount(graph : HaplotypeGraph, node : HaplotypeNode, readMap : Multimap<ReferenceRange, SingleHapMapping>) : Triple<Int, Int, Double> {
    val refRange = node.referenceRange()
    val rangeNodes = graph.nodes(refRange)
    val haplotypeLengths = rangeNodes.map {it.haplotypeSequence().length()}
    val haplotypeIds = rangeNodes.map {it.id()}
    val numberOfHaplotypes = rangeNodes.size
    val myHapMappings = readMap.get(refRange)
    val numberOfReads = myHapMappings.size

    //make a map of haplotypeId -> haplotype length
    val lengthMap = HashMap<Int, Int>()
    haplotypeIds.forEachIndexed { index, i ->  lengthMap.put(i, haplotypeLengths[index])}

    var haplotypeInclusionCount: Int = 0
    var haplotypeExclusionCount: Double = 0.0

    for (mapping in myHapMappings) {
        if (mapping.hapIdSet.contains(node.id())) { //this read is an inclusion
            haplotypeInclusionCount++
        } else {  //this read is an exclusion, calculate a weighting factor and add it to exclusion counts
            //the minimum haplotype length of all the hapids
            val minLength = mapping.hapIdSet.map { lengthMap.getOrDefault(it, Int.MAX_VALUE) }.minOrNull()
            if (minLength != null)
                haplotypeExclusionCount += minOf(1.0,  node.haplotypeSequence().length()/ minLength.toDouble())
        }
    }

    return Triple<Int, Int, Double>(haplotypeInclusionCount, numberOfReads - haplotypeInclusionCount, haplotypeExclusionCount)

}