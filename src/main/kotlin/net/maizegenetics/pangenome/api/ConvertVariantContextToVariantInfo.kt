@file:JvmName("ConvertVariantContextToVariantInfo")
package net.maizegenetics.pangenome.api

import htsjdk.variant.variantcontext.VariantContext
import net.maizegenetics.dna.map.Chromosome

/**
 * Function to convert a VariantContext into a VariantInfo object.
 * Need taxon and gamete id to get the correct genotype from the VariantContext.
 * The gamete ID is used to get the correct allele for the genotype.
 */
fun convertContextToInfo(variantContext: VariantContext, taxon : String, gameteId : Int = 0) : HaplotypeNode.VariantInfo {
    val genotype = variantContext.getGenotype(taxon).getAllele(gameteId).baseString
    val refAllele = variantContext.reference.baseString
    val altAllele = if(isRefBlock(variantContext)) "" else variantContext.alternateAlleles[0].baseString ?: ""

    val depths = if(isRefBlock(variantContext)) {
        val depthArray = IntArray(1)
        depthArray[0]=variantContext.getGenotype(taxon).dp
        depthArray
    } else variantContext.getGenotype(taxon).ad

    return HaplotypeNode.VariantInfo(variantContext.contig,variantContext.start,variantContext.end,genotype,refAllele,altAllele,!isRefBlock(variantContext), depths)
}

fun convertGVCFContextToInfo(variantContext: VariantContext, gameteId : Int = 0) : HaplotypeNode.VariantInfo {
    val genotype = variantContext.getGenotype(0).getAllele(gameteId).baseString
    val refAllele = variantContext.reference.baseString
    val altAllele = if(isRefBlock(variantContext)) "" else variantContext.alternateAlleles[0].baseString ?: ""

    val depths = if(isRefBlock(variantContext)) {
        val depthArray = IntArray(1)
        depthArray[0]=variantContext.getGenotype(0).dp
        depthArray
    } else variantContext.getGenotype(0).ad

    return HaplotypeNode.VariantInfo(Chromosome.instance(variantContext.contig).name,variantContext.start,variantContext.end,genotype,refAllele,altAllele,!isRefBlock(variantContext), depths)
}

/**
 * Function to convert a list of variantContexts into a list of VariantInfos
 */
fun convertVCListToVariantInfoList(variants : List<VariantContext>, taxon: String, gameteId: Int = 0) : List<HaplotypeNode.VariantInfo> {
    return variants.map { convertContextToInfo(it,taxon,gameteId) }
            .toList()
}
fun convertGVCFListToVariantInfoList(variants : List<VariantContext>, gameteId: Int = 0) : List<HaplotypeNode.VariantInfo> {
    return variants.map { convertGVCFContextToInfo(it,gameteId) }
            .toList()
}

/**
 * Helper function to check to see if a gvcf Record represents a reference block or just a single variant.
 */
fun isRefBlock(gvcfRecord : VariantContext) : Boolean {
    return gvcfRecord.hasAttribute("END") && gvcfRecord.getReference().getBaseString().length == 1
}
