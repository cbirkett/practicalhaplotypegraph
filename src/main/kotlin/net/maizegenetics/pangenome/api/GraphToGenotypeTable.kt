@file:JvmName("GraphToGenotypeTable")

package net.maizegenetics.pangenome.api

import net.maizegenetics.dna.WHICH_ALLELE
import net.maizegenetics.dna.map.Position
import net.maizegenetics.dna.map.PositionListBuilder
import net.maizegenetics.dna.snp.GenotypeTable
import net.maizegenetics.dna.snp.GenotypeTableBuilder
import net.maizegenetics.dna.snp.NucleotideAlignmentConstants
import net.maizegenetics.dna.snp.genotypecall.GenotypeCallTableBuilder
import net.maizegenetics.taxa.TaxaList
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.taxa.Taxon
import java.util.*


/**
 * @author Terry Casstevens
 * Created December 11, 2018
 */


fun genotypeTable(graph: HaplotypeGraph, range: ReferenceRange, taxon: Taxon): GenotypeTable {
    return genotypeTable(graph, range, TaxaListBuilder().add(taxon).build())
}

fun genotypeTable(graph: HaplotypeGraph, range: ReferenceRange, taxaList: TaxaList? = null): GenotypeTable {

    val taxa = graph.taxaInRange(range)

    val nodes = nodesForRange(graph, range)

    val positions = PositionListBuilder()
            .addAll(snpPositions(nodes))
            .build()

    val genotypesBuilder = GenotypeCallTableBuilder.getInstance(taxa.numberOfTaxa(), positions.numberOfSites())

    taxa.forEachIndexed { index, taxon ->

        val node = graph.nodes(range).first { it.taxaList().contains(taxon) }
        val optional = node.variantInfos()
        if (optional.isPresent) {

            val chromosome = node.referenceRange().chromosome()
            val infos = optional.get()
            for (info in infos) {
                var alleles = info.genotypeString().split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                if (alleles.size == 1) alleles = arrayOf(alleles[0], alleles[0])

                val variantLength = info.end() - info.start() + 1

                val currentPos = Position.of(chromosome, info.start())
                val site = positions.indexOf(currentPos)

                if (site >= 0) {

                    if (alleles[0] == "REF") {
                        alleles = arrayOf(NucleotideAlignmentConstants.getHaplotypeNucleotide(positions.get(site).getAllele(WHICH_ALLELE.Reference)), NucleotideAlignmentConstants.getHaplotypeNucleotide(positions.get(site).getAllele(WHICH_ALLELE.Reference)))
                    }

                    val diploid = StringBuilder(2)

                    if (alleles[0].length > variantLength) {
                        diploid.append("+")
                    } else if (index < alleles[0].length) {
                        diploid.append(alleles[0][index])
                    } else {
                        diploid.append("-")
                    }

                    if (alleles[1].length > variantLength) {
                        diploid.append("+")
                    } else if (index < alleles[1].length) {
                        diploid.append(alleles[1][index])
                    } else {
                        diploid.append("-")
                    }

                    genotypesBuilder.setBase(index, site, NucleotideAlignmentConstants.getNucleotideDiploidByte(diploid.toString()))

                }


            }

        }
    }

    return GenotypeTableBuilder.getInstance(genotypesBuilder.build(), positions, taxa)

}

fun nodesForRange(graph: HaplotypeGraph, range: ReferenceRange): List<HaplotypeNode> {

    val result = ArrayList<HaplotypeNode>()

    val nodes = graph.nodes(range)
    if (nodes != null) {
        result += nodes
    }

    return result

}

fun snpPositions(nodes: List<HaplotypeNode>): SortedSet<Position> {

    val result = TreeSet<Position>()

    nodes
            .map { node -> node.variantInfos() }
            .filter { contexts -> contexts.isPresent }
            .flatMap { it.get() }
            .filter { context ->
                // TODO undo indel filtering
                context.isVariant && !context.isIndel
            }
            .forEach {
                val start = it.start()
                result.add(Position.builder(it.chromosome(), start)
                        .build())
            }

    return result

}



