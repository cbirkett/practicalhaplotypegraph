package net.maizegenetics.pangenome.api

import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.plugindef.*
import net.maizegenetics.taxa.TaxaList
import net.maizegenetics.taxa.TaxaListBuilder
import java.awt.Frame
import java.sql.Connection
import java.sql.SQLException
import java.util.*
import javax.swing.ImageIcon

/**
 * This class is used to add path information to an existing graph to provide more information
 * for calculating transition probabilities for finding new paths. If the paths contain haplotypes not included in the
 * original graph they will be added without variant info or seuences. The reason is that the plugin results will be used by
 * path finding, which does not use either variant info or sequence.
 */
class AddPathsToGraphPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    var myPathMethod = PluginParameter.Builder<String>("pathMethod", null, String::class.java)
            .required(true)
            .description("Gametes from this path method will be added to the HaplotypeGraph.")
            .build()

    var myTaxaList = PluginParameter.Builder<TaxaList>("pathTaxaList", null, TaxaList::class.java)
            .description("Only gametes from these taxa will be added to the graph. If null, then all gametes will be added for the path method.")
            .build()

    override fun processData(input: DataSet?): DataSet {
        if (input == null) throw IllegalArgumentException("Must supply one HaplotypeGraph as input, input was null")
        val graphList = input.getDataOfType(HaplotypeGraph::class.java)
        if (graphList.size != 1) throw IllegalArgumentException("Must supply one HaplotypeGraph as input, ${graphList.size} supplied")
        val myInputGraph = graphList[0].data as HaplotypeGraph

        val myOutputGraph = addPathsToGraph(myInputGraph)
        return DataSet(Datum("HaplotypeGraph with paths", myOutputGraph, "HaplotypeGraph with paths from ${pathMethod()}"), this)
    }

    public fun addPathsToGraph(inGraph: HaplotypeGraph) : HaplotypeGraph {
        val dbConn = DBLoadingUtils.connection(false)
        val hapidToNewGametesMultimap = createHaplotyeIdToPathGametesMap(dbConn)

        val refrangeToNewHapidsMap = hapidsByRefRangeId(dbConn, hapidToNewGametesMultimap)

        val resultNodes = TreeMap<ReferenceRange, MutableList<HaplotypeNode>>()

        var addedPathCount = 0
        inGraph.referenceRangeList().forEach { range ->
            //for each node in the graph, add path gamete names to the taxaList by creating a new node and adding to resultNodes
            //if there are no paths to add then just use the old node
            //as each node is processed remove its hapid from the map so that any remaining hapids can be used to create new nodes
            val nodeList = ArrayList<HaplotypeNode>()
            val pathHapids = refrangeToNewHapidsMap[range.id()]?.toMutableList()
            inGraph.nodes(range).forEach { node ->
                //does this node have new hapids?
                val gameteList = hapidToNewGametesMultimap.get(node.id())
                if (gameteList == null || gameteList.size == 0) {
                    //no new gametes to add just put this node in the new list
                    nodeList.add(node)
                } else {
                    //gametes to add, create a new node with the gamete names added to the taxaList
                    //getting here guarantees that there are pathHapids for this range
                    pathHapids!!.remove(node.id())
                    val taxa = TaxaListBuilder().addAll(node.taxaList()).addAll(gameteList.toTypedArray()).build()
                    val newNode = HaplotypeNode(node.haplotypeSequence(),taxa, node.id(), node.asmContig(), node.asmStart(), node.asmEnd(), node.asmStrand(),node.genomeFileID(), node.byteEncodedVariants(), node.variantMap())
                    nodeList.add(newNode)
                }
            }

            //for each new haplotype id (those left in pathHapids) add a new node
            if (pathHapids != null && pathHapids.size > 0) {
                pathHapids.forEach {
                    val gameteList = hapidToNewGametesMultimap.get(it)
                    val taxa = TaxaListBuilder().addAll(gameteList.toTypedArray()).build()
                    nodeList.add(HaplotypeNode(null, taxa, it))
                }
            }

            //add this reference range
            resultNodes.put(range, nodeList)

        }

        dbConn.close()

        val edges = CreateGraphUtils.createEdges(resultNodes)
        return HaplotypeGraph(edges)

    }

    private fun hapidsByRefRangeId(dbConn: Connection, hapidMap: Multimap<Int, String>) : MutableMap<Int, List<Int>> {

        //query hapids in batches of 1000
        val hapidSetList = hapidMap.keySet().windowed(1000, 1000, true)
        val refRangeHapidPairList = ArrayList<Pair<Int,Int>>()

        val myStatement = dbConn.createStatement()
        hapidSetList.forEach {
            val hapidQuery = "SELECT ref_range_id, haplotypes_id FROM haplotypes " +
                    "WHERE haplotypes_id in (${it.joinToString(",")})"
            val hapidRS = myStatement.executeQuery(hapidQuery)
            while (hapidRS.next()) {
                refRangeHapidPairList.add(Pair(hapidRS.getInt(1), hapidRS.getInt(2)))
            }
        }
        myStatement.close()

        return refRangeHapidPairList.groupBy({it.first}, {it.second}).toMutableMap()

    }

    /**
     * For all of the taxa in a path (or the subset returned by taxaList()),
     * this method returns a Multimap<Int,String> where the Int is a hapid and String is a gamete name
     * The gamete name is formed by appending - followed by the path number to the genoid of the taxon.
     * The path number is 1 for a haploid path and 1 or 2 for a diploid path.
     * The result is a map of haplotype_id to "names" of taxa/gametes with paths containing that haplotype
     *
     * The gamete names are constructed by appending a gamete number to the genoid to generate a unique
     * identifier for each gamete. Genoid is used instead of line_name to avoid generating names that are the same as haplotypes.
     */
    private fun createHaplotyeIdToPathGametesMap(database: Connection): Multimap<Int, String> {
        val hapidGameteMap: Multimap<Int, String> = HashMultimap.create()
        val pathQuery = "SELECT genotypes.genoid, line_name, paths_data FROM genotypes, paths, methods " +
                "WHERE genotypes.genoid=paths.genoid AND paths.method_id=methods.method_id AND name = '" + pathMethod() + "'"
        try {
            val pathRS = database.createStatement().executeQuery(pathQuery)
            val myTaxaList = taxaList()
            if (myTaxaList == null) {
                //because there is no taxa list use all records
                while (pathRS.next()) {
                    val haplotypeLists = DBLoadingUtils.decodePathsForMultipleLists(pathRS.getBytes(3))
                    var index = 1
                    for (hapList in haplotypeLists) {
                        val gameteName = pathRS.getInt(1).toString() + "-" + index++
                        for (hapid in hapList) {
                            hapidGameteMap.put(hapid, gameteName)
                        }
                    }
                }
            } else {
                //filter records on line_name. Is it in taxaList?
                // A HashSet has best performance for .contains
                val taxaSet = myTaxaList.map { it.name }.toHashSet()
                while (pathRS.next()) {
                    val lineName = pathRS.getString(2)
                    if (taxaSet.contains(lineName)) {
                        val haplotypeLists = DBLoadingUtils.decodePathsForMultipleLists(pathRS.getBytes(3))
                        var index = 1
                        for (hapList in haplotypeLists) {
                            val gameteName = pathRS.getInt(1).toString() + "-" + index++
                            for (hapid in hapList) {
                                hapidGameteMap.put(hapid, gameteName)
                            }
                        }
                    }
                }
            }
            pathRS.close()
        } catch (se: SQLException) {
            throw java.lang.IllegalArgumentException("Error executing sql: $pathQuery", se)
        }
        return hapidGameteMap
    }

    override fun getToolTipText(): String {
        return "Add paths to graph"
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "Add Paths"
    }

    /**
     * Gametes from this path method will be added to the
     * HaplotypeGraph.
     *
     * @return Path Method
     */
    fun pathMethod(): String {
        return myPathMethod.value()
    }

    /**
     * Set Path Method. Gametes from this path method will
     * be added to the HaplotypeGraph.
     *
     * @param value Path Method
     *
     * @return this plugin
     */
    fun pathMethod(value: String): AddPathsToGraphPlugin {
        myPathMethod = PluginParameter<String>(myPathMethod, value)
        return this
    }

    /**
     * Only gametes from these taxa will be added to the graph.
     * If null, then all gametes will be added for the path
     * method.
     *
     * @return Path Taxa List
     */
    fun taxaList(): TaxaList? {
        return myTaxaList.value()
    }

    /**
     * Set Path Taxa List. Only gametes from these taxa will
     * be added to the graph. If null, then all gametes will
     * be added for the path method.
     *
     * @param value Path Taxa List
     *
     * @return this plugin
     */
    fun taxaList(value: TaxaList): AddPathsToGraphPlugin {
        myTaxaList = PluginParameter<TaxaList>(myTaxaList, value)
        return this
    }
}

fun main() {
    GeneratePluginCode.generateKotlin(AddPathsToGraphPlugin::class.java)

}
