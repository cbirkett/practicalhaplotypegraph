@file:JvmName("HaplotypeGraphStream")

package net.maizegenetics.pangenome.api

import com.google.common.collect.ImmutableMap
import com.google.common.collect.ImmutableSet
import com.google.common.collect.Queues
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.taxa.TaxaList
import org.apache.log4j.Logger
import java.sql.Connection
import java.sql.ResultSet
import java.util.*
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.ForkJoinPool
import java.util.concurrent.TimeUnit
import java.util.function.Consumer
import java.util.stream.Stream
import java.util.stream.StreamSupport
import kotlin.collections.LinkedHashMap

/**
 * @author Terry Casstevens
 * Created September 17, 2018
 */

private val myLogger = Logger.getLogger("net.maizegenetics.pangenome.api.HaplotypeGraphStream")

/**
 * This returns a streams sets of haplotype nodes paired with their reference range.
 * The nodes included are defined by the haplotype method(s) / reference range group method(s)
 * provided. The nodes can either include of exclude the sequences and variants (for memory
 * efficiency).  The database connection information is specified in the config file.
 */
fun graph(configFile: String, methods: List<Pair<String, String>>, numRangesPerQuery: Int, includeSequence: Boolean = true,
          includeVariants: Boolean = false): Stream<Pair<ReferenceRange, Set<HaplotypeNode>>> {
    val database = DBLoadingUtils.connection(configFile, false)
    return StreamSupport.stream(GraphSpliterator(database, methods, numRangesPerQuery, includeSequence, includeVariants), false)
}

/**
 * This Spliterator class is the bases to create the haplotype node stream for the  {@link #graph(String, List<Pair<String, String>>, Boolean, Boolean) graph}
 */
class GraphSpliterator(database: Connection, methods: List<Pair<String, String>>, numRangesPerQuery: Int, includeSequence: Boolean,
                       includeVariants: Boolean) : Spliterator<Pair<ReferenceRange, Set<HaplotypeNode>>> {

    private val methodsTable = methods(database)
    private val referenceRanges = referenceRanges(database, methods, methodsTable)
    private val taxaLists = CreateGraphUtils.taxaListMap(database)
    private val rangesQueue = Queues.newArrayBlockingQueue<Pair<ReferenceRange, Set<HaplotypeNode>>>(1000)
    private val pool = ForkJoinPool.commonPool()
    private val future = pool.submit(LoadQueue(database, methods, includeSequence, includeVariants, methodsTable, referenceRanges, taxaLists, rangesQueue, numRangesPerQuery))

    override fun estimateSize(): Long {
        return referenceRanges.size.toLong()
    }

    override fun characteristics(): Int {
        return Spliterator.IMMUTABLE or Spliterator.ORDERED or Spliterator.SIZED
    }

    override fun tryAdvance(action: Consumer<in Pair<ReferenceRange, Set<HaplotypeNode>>>?): Boolean {

        while (!future.isDone) {
            if (rangesQueue.isNotEmpty()) {
                action?.accept(rangesQueue.remove())
                return true
            }
        }

        return if (rangesQueue.isNotEmpty()) {
            action?.accept(rangesQueue.remove())
            true
        } else {
            false
        }

    }

    override fun forEachRemaining(action: Consumer<in Pair<ReferenceRange, Set<HaplotypeNode>>>?) {

        while (!future.isDone) {
            while (rangesQueue.isNotEmpty()) {
                action?.accept(rangesQueue.remove())
            }
        }

        while (rangesQueue.isNotEmpty()) {
            action?.accept(rangesQueue.remove())
        }

    }

    override fun trySplit(): Spliterator<Pair<ReferenceRange, Set<HaplotypeNode>>>? {
        return null
    }

}

/**
 * This class loads the queue with Pair<ReferenceRange, Set<HaplotypeNode>> which is consumed by the
 * @link GraphSpliterator.
 */
private class LoadQueue(val database: Connection, val methods: List<Pair<String, String>>, val includeSequence: Boolean,
                        val includeVariants: Boolean, val methodTable: Map<String, Method>,
                        val referenceRanges: LinkedHashMap<Int, ReferenceRange>, val taxaLists: Map<Int, TaxaList>,
                        val queue: ArrayBlockingQueue<Pair<ReferenceRange, Set<HaplotypeNode>>>,
                        val numRangesPerQuery: Int) : Runnable {

    /**
     * This executed in an independent thread iterating over the reference ranges loading
     * @link MAX_RANGES_PER_QUERY.  Each query never spans chromosome boundaries.
     */
    override fun run() {

        val time = System.nanoTime()

        var currentChromosome = referenceRanges.entries.iterator().next().value.chromosome()
        var includedRanges = TreeSet<ReferenceRange>()
        for (range: ReferenceRange in referenceRanges.values) {
            if (includedRanges.size >= numRangesPerQuery || range.chromosome() != currentChromosome) {
                queueHaplotypeNodesByRange(includedRanges)
                currentChromosome = range.chromosome()
                includedRanges = TreeSet()
                includedRanges.add(range)
            } else {
                includedRanges.add(range)
            }
        }
        queueHaplotypeNodesByRange(includedRanges)

        database.close()

        myLogger.info("LoadQueue: time: " + (System.nanoTime() - time).toDouble() / 1_000_000_000.0 + " secs.")

    }

    /**
     * This function executes a single query to the database to load upto @link MAX_RANGES_PER_QUERY
     * and associated haplotype nodes.
     */
    private fun queueHaplotypeNodesByRange(includedRanges: SortedSet<ReferenceRange>) {

        /*
        SELECT DISTINCT haplotypes_id, gamete_grp_id, haplotypes.ref_range_id, seq_hash, seq_len  from haplotypes
        INNER JOIN reference_ranges ON haplotypes.ref_range_id = reference_ranges.ref_range_id
        WHERE reference_ranges.chrom = '1'
        AND reference_ranges.range_start >= 0
        AND reference_ranges.range_end <= 1000000
        AND ((haplotypes.method_id = 4))
        ORDER BY range_start, haplotypes.ref_range_id;
         */

        val firstRange = includedRanges.first()
        val lastRange = includedRanges.last()
        val startPos = firstRange.start()
        val endPos = lastRange.end()
        val chromosomeStr = firstRange.chromosome().name
        val variantMap = if (includeVariants) VariantUtils.variantIdsToVariantMap(chromosomeStr, startPos, endPos, database) else null

        val query = """SELECT DISTINCT haplotypes_id, gamete_grp_id, haplotypes.ref_range_id ${if (includeSequence) ", sequence" else ""}, seq_hash, seq_len, asm_contig, asm_start_coordinate, asm_end_coordinate, asm_strand, genome_file_id ${if (includeVariants) ", variant_list" else ""} from haplotypes
        INNER JOIN reference_ranges ON haplotypes.ref_range_id = reference_ranges.ref_range_id
        WHERE reference_ranges.chrom = '$chromosomeStr'
        AND reference_ranges.range_start >= $startPos
        AND reference_ranges.range_end <= $endPos
        ${methodClauseWithNestedSelect(methods, methodTable)}
        ORDER BY range_start, haplotypes.ref_range_id;"""

        myLogger.info("queueHaplotypeNodesByRange: query: $query")

        var rs: ResultSet? = null
        try {

            rs = database.createStatement().executeQuery(query)

            var currentRefRangeId = -1
            var currentRefRange: ReferenceRange? = null
            var nodes = ImmutableSet.Builder<HaplotypeNode>()
            while (rs.next()) {

                val hapId = rs.getInt("haplotypes_id")
                val gameteGrp = rs.getInt("gamete_grp_id")
                val refRangeId = rs.getInt("ref_range_id")
                var sequence: ByteArray? = null
                if (includeSequence) {
                    sequence = rs.getBytes("sequence")
                }
                val seqHash = rs.getString("seq_hash")
                val seqLen = rs.getInt("seq_len")
                val asmContig = rs.getString("asm_contig")
                val asmStart = rs.getInt("asm_start_coordinate")
                val asmEnd = rs.getInt("asm_end_coordinate")
                val asmStrand = rs.getString("asm_strand")
                val genomeFileID = rs.getInt("genome_file_id")
                var variants: ByteArray? = null
                if (includeVariants) {
                    variants = rs.getBytes("variant_list")
                }

                if (currentRefRangeId == -1) {

                    currentRefRangeId = refRangeId
                    currentRefRange = referenceRanges.get(refRangeId)
                            ?: throw IllegalStateException("HaplotypeGraphStream: queueHaplotypeNodesByRange: no reference range in map for ref_range_id: $refRangeId")

                } else if (currentRefRangeId != refRangeId) {

                    queue.offer(Pair(currentRefRange!!, nodes.build()), 10, TimeUnit.MINUTES)
                    currentRefRangeId = refRangeId
                    currentRefRange = referenceRanges.get(refRangeId)
                            ?: throw IllegalStateException("HaplotypeGraphStream: queueHaplotypeNodesByRange: no reference range in map for ref_range_id: $refRangeId")
                    nodes = ImmutableSet.Builder()

                }

                val hapSeq = HaplotypeSequence.getInstance(sequence, currentRefRange, 0.0, seqHash, seqLen)
                val taxa = taxaLists.get(gameteGrp)
                        ?: throw IllegalStateException("HaplotypeGraphStream: queueHaplotypeNodesByRange: no taxa list for gamete_grp_id: $gameteGrp")
                if (includeVariants) {
                    nodes.add(HaplotypeNode(hapSeq, taxa, hapId, asmContig, asmStart, asmEnd, asmStrand,genomeFileID, variants, variantMap))
                } else {
                    nodes.add(HaplotypeNode(hapSeq, taxa, hapId, asmContig, asmStart, asmEnd, asmStrand, genomeFileID))
                }

            }

            queue.offer(Pair(currentRefRange!!, nodes.build()), 10, TimeUnit.MINUTES)

        } catch (ex: Exception) {
            myLogger.debug(ex.message, ex)
            throw ex
        } finally {
            rs?.close()
        }
    }

}


/**
 * This retrieves the reference ranges matching the haplotype method / range group method pairs specified.
 */
fun referenceRanges(database: Connection, methods: List<Pair<String, String>>, methodTable: Map<String, Method>): LinkedHashMap<Int, ReferenceRange> {

    val time = System.nanoTime()

    val refLine = CreateGraphUtils.getRefLineName(database)

    val query = """SELECT DISTINCT reference_ranges.ref_range_id, chrom, range_start, range_end from haplotypes
    INNER JOIN reference_ranges ON haplotypes.ref_range_id = reference_ranges.ref_range_id
    INNER JOIN ref_range_ref_range_method ON reference_ranges.ref_range_id = ref_range_ref_range_method.ref_range_id
    ${methodClause(methods, methodTable)};"""

    myLogger.info("referenceRanges: query: $query")

    val compare: Comparator<Pair<Int, ReferenceRange>> = Comparator { o1: Pair<Int, ReferenceRange>, o2: Pair<Int, ReferenceRange> -> o1.second.compareTo(o2.second) }
    val sortedRanges = TreeSet<Pair<Int, ReferenceRange>>(compare)

    val rs = database.createStatement().executeQuery(query)
    try {

        while (rs.next()) {
            val id = rs.getInt("ref_range_id")
            val chromosome = rs.getString("chrom")
            val start = rs.getInt("range_start")
            val end = rs.getInt("range_end")

            sortedRanges.add(Pair(id, ReferenceRange(refLine, Chromosome.instance(chromosome), start, end, id)))
        }

    } finally {
        rs.close()
    }

    val result = LinkedHashMap<Int, ReferenceRange>()
    sortedRanges.forEach { (id, range) -> result.put(id, range) }

    myLogger.info("referenceRanges: number of reference ranges: " + sortedRanges.size)

    myLogger.info("referenceRanges: time: " + (System.nanoTime() - time).toDouble() / 1_000_000_000.0 + " secs.")

    return result

}

/**
 * This constructs the SQL for querying specified haplotype method / range group method pairs.
 * If range group methods are specified, it uses a nested select in order to improve
 * performance over having an inner join.
 */
private fun methodClauseWithNestedSelect(methods: List<Pair<String, String>>, methodTable: Map<String, Method>): String {

    // AND haplotypes.method_id = 4
    // AND reference_ranges.ref_range_id in (
    //        SELECT reference_ranges.ref_range_id from reference_ranges
    //                INNER JOIN ref_range_ref_range_method ON reference_ranges.ref_range_id = ref_range_ref_range_method.ref_range_id
    //                WHERE ref_range_ref_range_method.method_id = 2)

    if (methods.isEmpty()) {
        return ""
    }

    var notFirst = false;
    val methodClause = StringBuilder()
    methodClause.append(" AND (")
    for (methodPair: Pair<String, String> in methods) {

        if (notFirst) {
            methodClause.append(" OR ")
        }
        notFirst = true

        methodClause.append("(")
                .append("haplotypes.method_id = ")
                .append(methodTable.get(methodPair.first)!!.id)
        if (methodPair.second.isNotEmpty()) {
            methodClause.append(" AND ")
                    .append("reference_ranges.ref_range_id in (")
                    .append("SELECT reference_ranges.ref_range_id from reference_ranges ")
                    .append("INNER JOIN ref_range_ref_range_method ON reference_ranges.ref_range_id = ref_range_ref_range_method.ref_range_id ")
                    .append("WHERE ref_range_ref_range_method.method_id = ")
                    .append(methodTable.get(methodPair.second)!!.id)
                    .append(")")
        }
        methodClause.append(")")

    }
    methodClause.append(")")

    return methodClause.toString()

}

/**
 * This constructs the SQL for querying specified haplotype method / range group method pairs.
 */
private fun methodClause(methods: List<Pair<String, String>>, methodTable: Map<String, Method>): String {

    if (methods.isEmpty()) {
        return ""
    }

    var notFirst = false;
    val methodClause = StringBuilder()
    methodClause.append(" AND (")
    for (methodPair: Pair<String, String> in methods) {

        if (notFirst) {
            methodClause.append(" OR ")
        }
        notFirst = true

        methodClause.append("(")
                .append("haplotypes.method_id = ")
                .append(methodTable.get(methodPair.first)!!.id)
        if (methodPair.second.isNotEmpty()) {
            methodClause.append(" AND ")
                    .append("ref_range_ref_range_method.method_id = ")
                    .append(methodTable.get(methodPair.second)!!.id)
        }
        methodClause.append(")")

    }
    methodClause.append(")")

    return methodClause.toString()

}

/**
 * Data class for holding row from the PHG methods table.
 */
data class Method(val name: String, val id: Int, val type: Int)

/**
 * This loads the contents of the PHG methods table into memory.
 */
fun methods(database: Connection): Map<String, Method> {

    val query = """SELECT method_id, method_type, name from methods"""
    myLogger.info("methods: query: $query")

    val result = ImmutableMap.builder<String, Method>()
    val rs = database.createStatement().executeQuery(query)
    try {

        while (rs.next()) {
            val id = rs.getInt("method_id")
            val type = rs.getInt("method_type")
            val name = rs.getString("name")
            result.put(name, Method(name, id, type))
        }

    } finally {
        rs.close()
    }

    return result.build()

}