package net.maizegenetics.pangenome.api

import net.maizegenetics.plugindef.*
import java.awt.Frame
import java.io.PrintWriter
import java.lang.IllegalArgumentException
import javax.swing.ImageIcon
import net.maizegenetics.plugindef.PluginParameter



/**
 * Plugin to write a GFA 1.0 file and CSV haplotype node annotations.
 * Takes a graph and a file base path as input. Writes gfa and csv files to the output path.
 */
class WriteGraphAsGFAPlugin(parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    private var outPath = PluginParameter.Builder("outPath", null, String::class.java)
            .description("Base file path for exported files. Enter a path without file extensions. " +
                    "The plugin will append gfa and csv to the output files.")
            .guiName("Out Path")
            .required(true)
            .outFile()
            .build()

    override fun processData(input: DataSet?): DataSet? {
        val temp = input?.getDataOfType(HaplotypeGraph::class.java)
        if (temp?.size != 1) {
            throw IllegalArgumentException("WriteGraphAsGFAPlugin: must input one graph")
        }
        val graph = input.getData(0).data as HaplotypeGraph

        writeGraphToGFA(graph, outPath.value())
        return null
    }

    override fun pluginDescription(): String {
        return "Exports a HaplotypeGraph as a GFA 1.0 formatted file and a CSV file of haplotype node annotations. " +
                "These files can be imported by Bandage, a GFA viewer."
    }

    override fun getToolTipText(): String {
        return "Write the graph as a GFA with CSV notations."
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "GFA Export"
    }

    /**
     * Base file path for exported files. Enter a path without
     * file extensions. The plugin will append gfa and csv
     * to the output files.
     *
     * @return Out Path
     */
    fun outPath(): String {
        return outPath.value()
    }

    /**
     * Set Out Path. Base file path for exported files. Enter
     * a path without file extensions. The plugin will append
     * gfa and csv to the output files.
     *
     * @param value Out Path
     *
     * @return this plugin
     */
    fun outPath(value: String): WriteGraphAsGFAPlugin {
        outPath = PluginParameter(outPath, value)
        return this
    }

}

//fun main() {
//    GeneratePluginCode.generate(WriteGraphAsGFAPlugin::class.java)
//}

private fun writeGraphToGFA(graph : HaplotypeGraph, gfaName : String) {
    val pwGfa = PrintWriter("$gfaName.gfa")
    val pwCsv = PrintWriter("$gfaName.csv")
    pwGfa.println("H\tVN:Z:1.0")
    pwCsv.println("Name,Range,Start,Ntaxa,Taxa")
    for (refRange in graph.referenceRanges()) {
        for (node in graph.nodes(refRange)) {
            pwGfa.println("S\t${node.id()}\t${node.haplotypeSequence().sequence()}")
            val taxa = node.taxaList().map{it.getName()}.joinToString(";")
            val csvLine = "${node.id()},${refRange.id()},${refRange.start()},${node.taxaList().size},$taxa"
            pwCsv.println(csvLine)
            for (edge in graph.leftEdges(node)) {
                pwGfa.println("L\t${edge.leftHapNode().id()}\t+\t${edge.rightHapNode().id()}\t+\t0M")
            }
            for (edge in graph.rightEdges(node)) {
                pwGfa.println("L\t${edge.leftHapNode().id()}\t+\t${edge.rightHapNode().id()}\t+\t0M")
            }
        }
    }
    pwGfa.close()
    pwCsv.close()
}

