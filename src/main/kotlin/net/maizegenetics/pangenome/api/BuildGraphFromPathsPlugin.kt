package net.maizegenetics.pangenome.api

import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.Datum
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.taxa.TaxaList
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.taxa.Taxon
import org.apache.log4j.Logger
import java.awt.Frame
import java.sql.Connection
import java.util.*
import javax.swing.ImageIcon

class BuildGraphFromPathsPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    var myPathMethod = PluginParameter.Builder<String>("pathMethod", null, String::class.java)
            .required(true)
            .description("Gametes from this path method will be added to the HaplotypeGraph.")
            .build()

    var myTaxaList = PluginParameter.Builder<TaxaList>("pathTaxaList", null, TaxaList::class.java)
            .description("Only gametes from these taxa will be added to the graph. If null, then all gametes will be added for the path method.")
            .build()

    val myLogger = Logger.getLogger(BuildGraphFromPathsPlugin::class.java)

    override fun processData(input: DataSet?): DataSet {
        val myOutputGraph = build()
        return DataSet(Datum("HaplotypeGraph with paths", myOutputGraph, "HaplotypeGraph with paths from ${pathMethod()}"), this)
    }

    data class RefrangeInfo(val chr: Chromosome, val start: Int, val end: Int)

    fun build() : HaplotypeGraph {
        //some of the queries return a large number of records, so restrict the fetch size to limit memory use
        val dbConn = DBLoadingUtils.connection(false)
        dbConn.setAutoCommit(false)
        val myStatement = dbConn.createStatement()
        myStatement.setFetchSize(1000)

        //create a hapId -> refrangeId map
        //for each path add Taxon to the TaxaListBuilder for each hapid
        //   by creating a hapid -> TaxaListBuilder map
        //create refrangeId->list of hapids map for hapids with taxa
        //for each entry in refrange->list<hapid> create a ReferenceRange,list<HaplotypeNode> pair
        //make that into a NavigableMap

        //map of hapId -> refrangeId
        val hapidToRefrangeIdMap = mutableMapOf<Int,Int>()
        val hapidQuery = "SELECT haplotypes_id, ref_range_id FROM haplotypes"
        var start = System.nanoTime()
        myLogger.info("Executing $hapidQuery")
        myStatement.executeQuery(hapidQuery).use {
            while(it.next()) {
                hapidToRefrangeIdMap[it.getInt(1)] = it.getInt(2)
            }
        }
        myLogger.info("Hapid to refrange query executed in ${(System.nanoTime() - start)/1e6} ms")

        //create a hapid -> TaxaListBuilder map
        //add taxa for each path method, one at a time
        val pathMethods = pathMethod().split(",")

        //There is some overhead involved with a path method list, so if there is only one method,
        //using the single method call avoids that
        start = System.nanoTime()
        val hapidToTaxaListMap = if (pathMethods.size == 1) haplotypeIdToTaxaList(dbConn, pathMethods[0])
        else haplotypeIdToTaxaList(dbConn, pathMethods)
        myLogger.info("haplotypeIdToTaxaList ran in ${(System.nanoTime() - start)/1e6} ms")

        //get reference ranges
        start = System.nanoTime()
        val refRangeInfoList = mutableMapOf<Int, RefrangeInfo>()
        val refrangeQuery = "SELECT ref_range_id, chrom, range_start, range_end FROM reference_ranges"
        myStatement.executeQuery(refrangeQuery).use {
            while (it.next()) {
                val id = it.getInt(1)
                val chr = it.getString(2)
                val startPos = it.getInt(3)
                val endPos = it.getInt(4)
                refRangeInfoList[id] = RefrangeInfo(Chromosome.instance(chr), startPos, endPos)
            }
        }
        myLogger.info("refrangeQuery ran in ${(System.nanoTime() - start)/1e6} ms")

        //list of hapids with taxa
        val hapidList = hapidToTaxaListMap.keys
        //refrangeId -> list of hapids
        val refrangeToHapidMap = hapidList.groupBy { hapidToRefrangeIdMap[it]!! }
        //for each entry in refrange->list<hapid> create a ReferenceRange,list<HaplotypeNode> pair
        myLogger.info("Creating tree map")
        start = System.nanoTime()

        val refRangeToNodes = refrangeToHapidMap.entries.associateTo(TreeMap()) { (rrid, hapids) ->
            val info = refRangeInfoList[rrid]!!
            val refrangeObj = ReferenceRange("REF", info.chr, info.start, info.end, rrid)
            val nodeList = hapids.map { id ->
                val hapSequence = HaplotypeSequence.getInstance(null, refrangeObj, 0.0, "")
                val myTaxaList = TaxaListBuilder().noIndex().addAll(hapidToTaxaListMap[id]!!).build()
                HaplotypeNode(hapSequence, myTaxaList, id)
            }
            Pair(refrangeObj, nodeList)
        }

        myStatement.close()
        dbConn.close()
        myLogger.info("TreeMap created in ${(System.nanoTime() - start)/1e6} ms")
        return HaplotypeGraph(refRangeToNodes)
    }

    /**
     * Map haplotype ids to taxa for a single path method
     */
    private fun haplotypeIdToTaxaList(dbConn: Connection, method: String): Map<Int, List<Taxon>> {

        val hapidToLineNamesMap = mutableMapOf<Int, MutableList<Taxon>>()
        val myStatement = dbConn.createStatement()
        myStatement.setFetchSize(100)

        //is there a taxaList that limits output?
        val limitTaxa = pathTaxaList() != null
        val limitTaxaSet = if (limitTaxa) pathTaxaList()!!.map { it.name }.toHashSet() else null

        val pathQuery = "SELECT paths_data, line_name " +
                "FROM paths, methods, genotypes " +
                "WHERE paths.method_id=methods.method_id AND paths.genoid=genotypes.genoid AND name = '$method'"
        myStatement.executeQuery(pathQuery).use {
            while (it.next()) {
                val pathData = it.getBytes(1)
                val lineName = it.getString(2)

                //if there is a taxaList (limitTaxa), do not add this lineName if it is not contained in it
                if (limitTaxa && !limitTaxaSet!!.contains(lineName)) continue

                val pathLists = DBLoadingUtils.decodePathsForMultipleLists(pathData)
                updateLineNamesMap(hapidToLineNamesMap, lineName, pathLists)
            }
        }
        myStatement.close()

        return hapidToLineNamesMap
    }

    private fun updateLineNamesMap(lineNameMap: MutableMap<Int, MutableList<Taxon>>, lineName: String, pathLists: List<List<Int>>) {
        if (pathLists.size == 1) {
            val lineTaxon = Taxon(lineName)
            pathLists[0].forEach { id ->
                val nameSetFromMap = lineNameMap[id]
                val nameSet = if (nameSetFromMap != null) nameSetFromMap else {
                    val newSet = mutableListOf<Taxon>()
                    lineNameMap[id] = newSet
                    newSet
                }
                nameSet.add(lineTaxon)
            }
        } else if (pathLists.size > 1) {
            pathLists.forEachIndexed { index, path ->
                val modifiedLineName = lineName + "_${index + 1}"
                val lineTaxon = Taxon(modifiedLineName)
                path.forEach { id ->
                    val nameSetFromMap = lineNameMap[id]
                    val nameSet = if (nameSetFromMap != null) nameSetFromMap else {
                        val newSet = mutableListOf<Taxon>()
                        lineNameMap[id] = newSet
                        newSet
                    }
                    nameSet.add(lineTaxon)
                }
            }
        }
    }

    /**
     * Maps haplotypeIds to Taxa List for multiple path methods.
     */
    private fun haplotypeIdToTaxaList(dbConn: Connection, methodList: List<String>): Map<Int, List<Taxon>> {
        val hapidToLineNamesMap = mutableMapOf<Int, MutableList<Taxon>>()
        val myStatement = dbConn.createStatement()
        myStatement.setFetchSize(100)

        //is there a taxaList that limits output?
        val limitTaxa = pathTaxaList() != null
        val limitTaxaSet = if (limitTaxa) pathTaxaList()!!.map { it.name }.toHashSet() else null

        //check for duplicate names
        val duplicateLineNames = findDuplicateLineNames(methodList, dbConn)

        for (method in methodList) {
            val pathQuery = "SELECT paths_data, line_name " +
                    "FROM paths, methods, genotypes " +
                    "WHERE paths.method_id=methods.method_id AND paths.genoid=genotypes.genoid AND name = '$method'"
            myStatement.executeQuery(pathQuery).use {
                while (it.next()) {
                    val pathData = it.getBytes(1)
                    var lineName = it.getString(2)

                    //if there is a taxaList (limitTaxa), do not add this lineName if it is not contained in it
                    if (limitTaxa && !limitTaxaSet!!.contains(lineName)) continue

                    //if this is a duplicate name append the method name
                    if (duplicateLineNames.contains(lineName)) lineName += "_$method"

                    val pathLists = DBLoadingUtils.decodePathsForMultipleLists(pathData)
                    updateLineNamesMap(hapidToLineNamesMap, lineName, pathLists)

                }
            }
        }

        myStatement.close()

        return hapidToLineNamesMap

    }


    private fun findDuplicateLineNames(methodList: List<String>, dbConn: Connection): Set<String> {
        val nameLists = methodList.map { lineNamesForPathMethod(it,dbConn)}
        val numberOfMethods = methodList.size
        val duplicateSet = HashSet<String>()
        for (firstMethod in 0 until numberOfMethods) {
            for (secondMethod in (firstMethod + 1) until numberOfMethods) {
                duplicateSet.addAll(nameLists[firstMethod].intersect(nameLists[secondMethod]))
            }
        }
        return duplicateSet
    }

    private fun lineNamesForPathMethod(methodName: String, dbConn: Connection): Set<String> {
        val query = "SELECT line_name FROM paths, methods, genotypes " +
                "WHERE paths.method_id=methods.method_id AND name='$methodName' " +
                "AND paths.genoid=genotypes.genoid"
        val nameSet = HashSet<String>()
        dbConn.createStatement().executeQuery(query).use {
            while(it.next()) {
                nameSet.add(it.getString(1))
            }
        }
        return nameSet
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "Path Graph"
    }

    override fun getToolTipText(): String {
        return "Build a HaplotypeGraph from paths"
    }

    /**
     * Gametes from this path method will be added to the
     * HaplotypeGraph.
     *
     * @return Path Method
     */
    fun pathMethod(): String {
        return myPathMethod.value()
    }

    /**
     * Set Path Method. Gametes from this path method will
     * be added to the HaplotypeGraph.
     *
     * @param value Path Method
     *
     * @return this plugin
     */
    fun pathMethod(value: String): BuildGraphFromPathsPlugin {
        myPathMethod = PluginParameter(myPathMethod, value)
        return this
    }

    /**
     * Only gametes from these taxa will be added to the graph.
     * If null, then all gametes will be added for the path
     * method.
     *
     * @return Path Taxa List
     */
    fun pathTaxaList(): TaxaList? {
        return myTaxaList.value()
    }

    /**
     * Set Path Taxa List. Only gametes from these taxa will
     * be added to the graph. If null, then all gametes will
     * be added for the path method.
     *
     * @param value Path Taxa List
     *
     * @return this plugin
     */
    fun pathTaxaList(value: TaxaList): BuildGraphFromPathsPlugin {
        myTaxaList = PluginParameter(myTaxaList, value)
        return this
    }

}