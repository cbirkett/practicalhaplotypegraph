package net.maizegenetics.pangenome.pipeline

import net.maizegenetics.pangenome.db_loading.GetDBConnectionPlugin
import net.maizegenetics.pangenome.db_loading.LoadAllIntervalsToPHGdbPlugin
import net.maizegenetics.pangenome.liquibase.LiquibaseUpdatePlugin
import net.maizegenetics.plugindef.*
import org.apache.log4j.Logger
import java.awt.Frame
import java.lang.IllegalStateException
import java.nio.file.Files
import java.nio.file.Paths
import javax.swing.ImageIcon

class MakeInitialPHGDBPipelinePlugin (parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(MakeInitialPHGDBPipelinePlugin::class.java)


    /**
     * Function to run the steps previously done by LoadGenomeIntervals.sh
     */
    override fun processData(input: DataSet?): DataSet? {
        //Steps in LoadGenomeIntervals.sh
        myLogger.info("Beginning loading Genome Intervals step.")
        loadGenomeIntervals()
        myLogger.info("Done loading Genome Intervals step.")

        myLogger.info("Checking if Liquibase can be run.")
        var liquibaseDir = "/liquibase/changelogs"
        if (Files.exists(Paths.get(liquibaseDir))) {
            myLogger.info("Liquibase can be run.  Setting it up using changelogsync.")
            setupLiquibase()
            myLogger.info("Done setting up Liquibase.")
        } else {
            myLogger.warn("Warning:  liquibase not found: db has not had changelogsync run, you must run liquibase manually or from the phg_liquibase docker")
        }
        myLogger.info("MakeInitialPHGDBPipelinePlugin complete!")
        return null
    }


    /**
     * Function to loadGenomeIntervals.  Check the dbConfigFile in the parameter cache.
     */
    private fun loadGenomeIntervals() {
        val dbConnectionDataSet = GetDBConnectionPlugin(null, false)
                .configFile(ParameterCache.value("configFile").get()) //Should not need this. leaving for now until SmallSeq is fixed to make sure it works.
                .createNew(true)
                .performFunction(null)

        //Run the plugin - LoadGenomeIntervalsToPHGdbPlugin is old, use LoadAllIntervalsToPHGdbPlugin
        val loadGenomeIntervalsPlugin = LoadAllIntervalsToPHGdbPlugin(null, false)
        loadGenomeIntervalsPlugin.setConfigParameters()
        loadGenomeIntervalsPlugin
                .refGenome(ParameterCache.value("referenceFasta").get())
                .performFunction(dbConnectionDataSet)

    }

    /**
     * Function to run the initial run of Liquibase running the 'changeLogSync' command.
     */
    private fun setupLiquibase() {
        val liquibasePlugin = LiquibaseUpdatePlugin(null, false)
        liquibasePlugin.setConfigParameters()
        liquibasePlugin
                .command("changeLogSync")
                .performFunction(null)
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = MakeInitialPHGDBPipelinePlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "MakeInitialPHGDBPipelinePlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to create the DB."
    }

}

fun main() {
    GeneratePluginCode.generateKotlin(MakeInitialPHGDBPipelinePlugin::class.java)
}