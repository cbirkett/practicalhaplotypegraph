package net.maizegenetics.pangenome.pipeline

import net.maizegenetics.pangenome.liquibase.CheckDBVersionPlugin
import net.maizegenetics.pangenome.liquibase.LiquibaseUpdatePlugin
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import javax.swing.ImageIcon

/**
 *
 * This class is written specifically to test the PipelineUtils:runLiquibaseCommand() function
 * Is this the best way to do this?  This is called from a docker from within kotlin/test/pipeline/PipelineUtilsTest
 *
 * Input parameters:
 *  - command:  a specific liquibase command to run:  currently it is "status" or "update"
 *  - outputDir:  directory to which liquibase output and error files will be written.  Must exist
 *  - configFile:  must be present in the parameter cache
 * This returns a String indicating all went well, ot that the db needs updating.
 * This is a String vs boolean as we will in the future add a PHG version to run
 * if the user does not want to update the db.
 *
 */
class LiquibaseCommandPlugin (parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(LiquibaseCommandPlugin::class.java)
    private var command  = PluginParameter.Builder("command",null,String::class.java)
            .description("Liquibase command to run")
            .required(true)
            .build()

    private var outputDir = PluginParameter.Builder("outputDir",null,String::class.java)
            .description("Directory to which liquibase output and error files will be written.")
            .outDir()
            .required(true)
            .build()


    /**
     * Function to run specified liquibase commands, return value
     */
    override fun processData(input: DataSet?): DataSet? {

        var outcome = runLiquibaseCommand(command(),outputDir())

        // Grab response, write a file with result (either "NEEDS_UPDATING:<PHG VERSION>" or "UP_TO_DATE"
        myLogger.info("LiquibaseCommandPlugin: outcome from runLiquibaseCommand: $outcome")

        var outputFile = outputDir() + "/runLiquibaseCommand_returnValue.txt"
        File(outputFile).bufferedWriter().use { out ->
            out.write(outcome)
            out.write("\n")
        }

        return null
    }


    override fun getToolTipText(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getIcon(): ImageIcon {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getButtonName(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     * Liquibase command to run
     *
     * @return Command
     */
    fun command(): String {
        return command.value()
    }

    /**
     * Set Command. Liquibase command to run
     *
     * @param value Command
     *
     * @return this plugin
     */
    fun command(value: String): LiquibaseCommandPlugin {
        command = PluginParameter<String>(command, value)
        return this
    }

    /**
     * Directory to which liquibase output and error files
     * will be written.
     *
     * @return Output Dir
     */
    fun outputDir(): String {
        return outputDir.value()
    }

    /**
     * Set Output Dir. Directory to which liquibase output
     * and error files will be written.
     *
     * @param value Output Dir
     *
     * @return this plugin
     */
    fun outputDir(value: String): LiquibaseCommandPlugin {
        outputDir = PluginParameter<String>(outputDir, value)
        return this
    }

}
fun main() {
    GeneratePluginCode.generateKotlin(LiquibaseCommandPlugin::class.java)
}