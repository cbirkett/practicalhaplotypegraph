package net.maizegenetics.pangenome.pipeline

import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.hapcollapse.RunHapConsensusPipelinePlugin
import net.maizegenetics.pangenome.processAssemblyGenomes.AssemblyHaplotypesMultiThreadPlugin
import net.maizegenetics.pangenome.processAssemblyGenomes.MummerScriptProcessing
import net.maizegenetics.plugindef.*
import org.apache.log4j.Logger
import java.awt.Frame
import java.lang.IllegalArgumentException
import java.nio.file.Files
import java.nio.file.Paths
import javax.swing.ImageIcon

class PopulatePHGDBPipelinePlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private var myAssemblyMethod = PluginParameter.Builder("asmMethodName",null,String::class.java)
            .description("Method name for the Assembly Based haplotypes being uploaded.")
            .required(false)
            .build()

    private var myAssemblyKeyFile = PluginParameter.Builder("asmKeyFile",null,String::class.java)
            .description("Key File for Assembly haplotypes.")
            .inFile()
            .required(false)
            .build()

    private var myWgsMethod = PluginParameter.Builder("wgsMethodName",null,String::class.java)
            .description("Method name for the WGS Based haplotypes being uploaded.")
            .required(false)
            .build()

    private var myWgsKeyFile = PluginParameter.Builder("wgsKeyFile",null,String::class.java)
            .description("Key File for WGS haplotypes.")
            .inFile()
            .required(false)
            .build()

    private var myConsensusMethod = PluginParameter.Builder("consensusMethodName",null,String::class.java)
            .description("Method name for the Consensus being uploaded.")
            .required(false)
            .build()

    private var myInputConsensusHaplotypeMethod = PluginParameter.Builder("inputConsensusMethods", null, String::class.java)
            .description("Methods needed to populate a PHG object in order to create consensus on.  " +
                    "If left blank a method will be auto-generated based on asmMethodName and wgsMethodName.  " +
                    "If either are not set only one will be used.")
            .required(false)
            .build()

    private var myForceDBUpdate = PluginParameter.Builder("forceDBUpdate",false,Boolean::class.javaObjectType)
            .description("Parameter to force the DB to update.  If this is set to true and the DB is incompatible with the current PHG code, this will automatically update the DB if it can.")
            .required(false)
            .build()

    private var liquibaseOutputDir = PluginParameter.Builder("liquibaseOutdir", null, String::class.java)
            .description("The directory to which liquibase output files will be written.")
            .inDir()
            .required(true)
            .build()

    private var skipLiquibaseCheck = PluginParameter.Builder("skipLiquibaseCheck", false, Boolean::class.javaObjectType)
            .description("Should the liquibase check be skipped when liquibase is missing? " +
                    "Only set to true if the PHG DB version is known to match the software version.")
            .build()


    private val myLogger = Logger.getLogger(PopulatePHGDBPipelinePlugin::class.java)

    override fun processData(input: DataSet?): DataSet? {

        //Need to check to see if we need to update liquibase
        //check if liquibaseDir exists.
        myLogger.info("Checking if Liquibase can be run.")

        var liquibaseDir = "/liquibase/changelogs"
        if (!Files.exists(Paths.get(liquibaseDir))) {
            if (skipLiquibaseCheck()) {
                myLogger.warn("Liquibase changelogs not present. Liquibase check was skipped.")
                System.exit(11)
            }
            else throw IllegalArgumentException("Liquibase not found: You must run liquibase manually or from the phg_liquibase docker.")
        } else {

            // Then I run LiquibaseCommandPlugin with 'status' as the command.
            val status = runLiquibaseCommand("status",liquibaseOutputDir())
            if(status == "UP_TO_DATE") {
                // If it returns back 'UP_TO_DATE' I can proceed without any issue.
                myLogger.info("PHG DB is up to date.  Proceeding with Populating the PHG DB.")
            }
            else if(status.startsWith("NEEDS_UPDATING")) {
                // If it returns back NEEDS_UPDATING,
                // if 'force' command
                if(forceDBUpdate()) {
                    // run LiquibaseCommandPlugin with an 'update' command and log that we are updating.
                    runLiquibaseCommand("update",liquibaseOutputDir())
                }
                else  {
                    throw IllegalStateException("The Current PHG System is incompatible with the provided PHG Database.\n" +
                            "To Run with the existing DB, please use PHG/docker version: ${status.substringAfter("NEEDS_UPDATING:")}\n" +
                            "If you would like the PHG DB to be automatically updated please rerun this Plugin with -forceDBUpdate true\n")
                }
            }
            else {
                myLogger.error("Unknown status from liquibase.  Status Message: ${status}")
                throw IllegalStateException("Unknown status from liquibase.  Status Message: ${status}")
            }

        }

        //Steps for CreateHaplotypes scripts.
        if(assemblyKeyFile() != null) {
            myLogger.info("Creating ASM Haplotypes.")
            createASMHaplotypes()
        }
        if(wgsKeyFile() != null) {
            myLogger.info("Creating WGS Haplotypes.")
            createWGSHaplotypes()
        }

        myLogger.info("Creating consensus.")
        createConsensus()

        return null
    }

    /**
     * Function to create Assembly based Haplotypes.
     */
    fun createASMHaplotypes() {
        //Need to make an object to setConfigParameters()
        val assemblyPlugin = AssemblyHaplotypesMultiThreadPlugin(null, false)
        assemblyPlugin.setConfigParameters()
        assemblyPlugin.assemblyMethod(assemblyMethod()?:"mummer4")
                .keyFile(assemblyKeyFile()!!)
                .performFunction(null)
    }

    /**
     * Function to create WGS based Haplotypes.
     */
    fun createWGSHaplotypes() {
        val configFile = ParameterCache.value("configFile").get()
        var process = ProcessBuilder(listOf("/CreateHaplotypesFromFastq.groovy","-config",configFile))
                .inheritIO()
                .start()
        val error = process.waitFor()
        if (error != 0) {
            myLogger.error("Error creating WGS Haplotypes")
            throw IllegalStateException("Error creating WGS Haplotypes")
        }

    }

    /**
     * Function to run createConsensus.  If the user does not specify an inputConsensusHaplotypeMethod, we try to auto generate one.
     */
    fun createConsensus() {
        //first check whether consensus should be run
        //consensus will not be run if the consensus method is null or if it starts with **
        val consensusMethodName = consensusMethod()
        if (consensusMethodName == null) {
            myLogger.info("Consensus will not be run because no consensus method name was assigned.")
            return
        }
        if (consensusMethodName.startsWith("**")) {
            myLogger.info("Consensus will not be run because the consensus method started with **.")
            return
        }
        if (consensusMethodName == "null") {
            myLogger.info("Consensus will not be run because the consensus method name was 'null'.")
            return
        }

        //Need to see if the user supplied a graph input method.  If not try to get both ASM and WGS and use both if possible.
        val haplotypeMethod = if(inputConsensusHaplotypeMethod() == null) {
            if(assemblyMethod() == null && wgsMethod() == null) {
                throw IllegalStateException("Attempting to auto-generate a method to create a graph to run consensus on.  Both WGS and Assembly methods are missing.")
            }
            listOf(assemblyMethod(), wgsMethod())
                    .filter { it != "" } //Need the filter in case only assembly or only wgs was run.
                    .joinToString(":")
        }
        else {
            inputConsensusHaplotypeMethod()
        }

        //Create the graph
        val haplotypeGraphBuilderPlugin = HaplotypeGraphBuilderPlugin(null, false)
        haplotypeGraphBuilderPlugin.setConfigParameters() //Use this to automatically figure out config params
        val graphDS = haplotypeGraphBuilderPlugin.methods(haplotypeMethod)
                .includeVariantContexts(true)
                .includeSequences(true)
                .performFunction(null)

        //Run create Consensus
        val consensusPlugin = RunHapConsensusPipelinePlugin(null, false)
        consensusPlugin.setConfigParameters() //Use this to automatically figure out config params
        consensusPlugin.dbConfigFile(ParameterCache.value("configFile").get())
                .collapseMethod(consensusMethod())
                .performFunction(graphDS)
    }


    override fun getIcon(): ImageIcon? {
        val imageURL = PopulatePHGDBPipelinePlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "MakePHGDBPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to create and poplulate the DB and Run Consensus."
    }


    /**
     * Method name for the Assembly Based haplotypes being
     * uploaded.
     *
     * @return Asm Method Name
     */
    fun assemblyMethod(): String? {
        return myAssemblyMethod.value()
    }

    /**
     * Set Asm Method Name. Method name for the Assembly Based
     * haplotypes being uploaded.
     *
     * @param value Asm Method Name
     *
     * @return this plugin
     */
    fun assemblyMethod(value: String): PopulatePHGDBPipelinePlugin {
        myAssemblyMethod = PluginParameter<String>(myAssemblyMethod, value)
        return this
    }

    /**
     * Key File for Assembly haplotypes.
     *
     * @return Asm Key File
     */
    fun assemblyKeyFile(): String? {
        return myAssemblyKeyFile.value()
    }

    /**
     * Set Asm Key File. Key File for Assembly haplotypes.
     *
     * @param value Asm Key File
     *
     * @return this plugin
     */
    fun assemblyKeyFile(value: String): PopulatePHGDBPipelinePlugin {
        myAssemblyKeyFile = PluginParameter<String>(myAssemblyKeyFile, value)
        return this
    }

    /**
     * Method name for the WGS Based haplotypes being uploaded.
     *
     * @return Wgs Method Name
     */
    fun wgsMethod(): String? {
        return myWgsMethod.value()
    }

    /**
     * Set Wgs Method Name. Method name for the WGS Based
     * haplotypes being uploaded.
     *
     * @param value Wgs Method Name
     *
     * @return this plugin
     */
    fun wgsMethod(value: String): PopulatePHGDBPipelinePlugin {
        myWgsMethod = PluginParameter<String>(myWgsMethod, value)
        return this
    }

    /**
     * Key File for WGS haplotypes.
     *
     * @return Wgs Key File
     */
    fun wgsKeyFile(): String? {
        return myWgsKeyFile.value()
    }

    /**
     * Set Wgs Key File. Key File for WGS haplotypes.
     *
     * @param value Wgs Key File
     *
     * @return this plugin
     */
    fun wgsKeyFile(value: String): PopulatePHGDBPipelinePlugin {
        myWgsKeyFile = PluginParameter<String>(myWgsKeyFile, value)
        return this
    }

    /**
     * Method name for the Consensus being uploaded.
     *
     * @return Consensus Method Name
     */
    fun consensusMethod(): String? {
        return myConsensusMethod.value()
    }

    /**
     * Set Consensus Method Name. Method name for the Consensus
     * being uploaded.
     *
     * @param value Consensus Method Name
     *
     * @return this plugin
     */
    fun consensusMethod(value: String): PopulatePHGDBPipelinePlugin {
        myConsensusMethod = PluginParameter<String>(myConsensusMethod, value)
        return this
    }

    /**
     * Methods needed to populate a PHG object in order to
     * create consensus on.  If left blank a method will be
     * auto-generated based on asmMethodName and wgsMethodName.
     *  If either are not set only one will be used.
     *
     * @return Input Consensus Methods
     */
    fun inputConsensusHaplotypeMethod(): String? {
        return myInputConsensusHaplotypeMethod.value()
    }

    /**
     * Set Input Consensus Methods. Methods needed to populate
     * a PHG object in order to create consensus on.  If left
     * blank a method will be auto-generated based on asmMethodName
     * and wgsMethodName.  If either are not set only one
     * will be used.
     *
     * @param value Input Consensus Methods
     *
     * @return this plugin
     */
    fun inputConsensusHaplotypeMethod(value: String): PopulatePHGDBPipelinePlugin {
        myInputConsensusHaplotypeMethod = PluginParameter<String>(myInputConsensusHaplotypeMethod, value)
        return this
    }

    /**
     * Parameter to force the DB to update.  If this is set
     * to true and the DB is incompatible with the current
     * PHG code, this will automatically update the DB if
     * it can.
     *
     * @return Force D B Update
     */
    fun forceDBUpdate(): Boolean {
        return myForceDBUpdate.value()
    }

    /**
     * Set Force D B Update. Parameter to force the DB to
     * update.  If this is set to true and the DB is incompatible
     * with the current PHG code, this will automatically
     * update the DB if it can.
     *
     * @param value Force D B Update
     *
     * @return this plugin
     */
    fun forceDBUpdate(value: Boolean): PopulatePHGDBPipelinePlugin {
        myForceDBUpdate = PluginParameter<Boolean>(myForceDBUpdate, value)
        return this
    }

    /**
     * The directory to which liquibase output files will
     * be written.
     *
     * @return Liquibase Outdir
     */
    fun liquibaseOutputDir(): String {
        return liquibaseOutputDir.value()
    }

    /**
     * Set Liquibase Outdir. The directory to which liquibase
     * output files will be written.
     *
     * @param value Liquibase Outdir
     *
     * @return this plugin
     */
    fun liquibaseOutputDir(value: String): PopulatePHGDBPipelinePlugin {
        liquibaseOutputDir = PluginParameter<String>(liquibaseOutputDir, value)
        return this
    }


    /**
     * Should the liquibase check be skipped? Do not set to
     * true if running inside a Docker. Then, only set to
     * true if the PHG DB version is known to match the software
     * version.
     *
     * @return Skip Liquibase Check
     */
    fun skipLiquibaseCheck(): Boolean {
        return skipLiquibaseCheck.value()
    }

    /**
     * Set Skip Liquibase Check. Should the liquibase check
     * be skipped? Do not set to true if running inside a
     * Docker. Then, only set to true if the PHG DB version
     * is known to match the software version.
     *
     * @param value Skip Liquibase Check
     *
     * @return this plugin
     */
    fun skipLiquibaseCheck(value: Boolean): PopulatePHGDBPipelinePlugin {
        skipLiquibaseCheck = PluginParameter<Boolean>(skipLiquibaseCheck, value)
        return this
    }

}

fun main() {
    GeneratePluginCode.generateKotlin(PopulatePHGDBPipelinePlugin::class.java)
}