@file:JvmName("FastaIO")

package net.maizegenetics.pangenome.io

import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger


private val myLogger = Logger.getLogger("net.maizegenetics.pangenome.io.FastaIO")

/**
 * Writes the given paths to FASTA format.
 * Multiple files written will include the taxon name
 * and haplotype number. Include the method used to generate
 * the paths in the filenameBase if desired.
 * Each sequence is for a chromosome.
 */
fun writeFasta(paths: Map<String, List<List<HaplotypeNode>>>, filenameBase: String) {

    paths.entries.forEach { (taxon, pathsForTaxon) ->
        pathsForTaxon.forEachIndexed { index, nodes ->
            writeFasta(taxon, nodes, filenameBase, index + 1)
        }
    }

}

/**
 * Writes the given path to FASTA format.
 * filename will include the taxon name
 * and haplotype number. Include the method used to generate
 * the paths in the filenameBase if desired.
 * Each sequence is for a chromosome.
 */
fun writeFasta(taxon: String, path: List<HaplotypeNode>, filenameBase: String, haplotypeNum: Int = -1) {

    val filenameNoExtension = filenameBase.substringBefore(".fa")
    val ploidyStr = if (haplotypeNum == -1) "" else "_$haplotypeNum"
    val filename = "${filenameNoExtension}_${taxon}${ploidyStr}.fa.gz"

    myLogger.info("writeFasta: writing: $filename")

    val sortedPath = path.toMutableList()
    sortedPath.sort()

    Utils.getBufferedWriter(filename).use { writer ->

        var currentChr = sortedPath[0].referenceRange().chromosome()
        var numColumnsWritten = 0
        writer.write("${fastaSequenceDesc(currentChr, taxon, haplotypeNum)}\n")
        sortedPath.forEach { node ->
            if (currentChr != node.referenceRange().chromosome()) {
                currentChr = node.referenceRange().chromosome()
                numColumnsWritten = 0
                writer.write("\n${fastaSequenceDesc(currentChr, taxon, haplotypeNum)}\n")
            }
            node.haplotypeSequence().sequence().forEach {
                if (numColumnsWritten == 80) {
                    writer.write("\n")
                    numColumnsWritten = 0
                }
                writer.write(it.toString())
                numColumnsWritten++
            }
        }

    }

    myLogger.info("writeFasta: finished writing: $filename")

}

private fun fastaSequenceDesc(chr: Chromosome, taxon: String, haplotypeNum: Int = -1): String {

    val result = StringBuilder()
    result.append(">${chr.name}")

    if (taxon != null) result.append(" [taxon=${taxon}]")

    result.append(" [chromosome=${chr.name}]")

    if (haplotypeNum != -1) result.append(" [haplotypeNum=${haplotypeNum}]")

    return "$result"

}
