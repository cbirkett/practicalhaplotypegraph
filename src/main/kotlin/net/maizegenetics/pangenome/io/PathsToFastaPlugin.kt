package net.maizegenetics.pangenome.io

import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.PluginParameter
import java.awt.Frame
import javax.swing.ImageIcon

/**
 * This exports paths to a Fasta file(s).
 *
 * The input to this plugin is Map<String, List<List<HaplotypeNode>>>.
 * The keys are taxon names. The values are lists of paths (list of nodes).
 *
 * Each file contains one haploid path for a taxon.  The filename
 * will be the given output filename appended with taxon name and haplotype number.
 * Each entry in the files will be a single chromosome.
 *
 * @author Terry Casstevens Created February 7, 2021
 */
class PathsToFastaPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private var myOutputFile = PluginParameter.Builder("outputFile", null, String::class.java)
            .description("Output base file name")
            .guiName("Output Base VCF File Name")
            .required(true)
            .build()

    override fun processData(input: DataSet): DataSet? {

        var temp = input.getDataOfType(Map::class.java)
        if (temp.size == 0) {
            throw IllegalArgumentException("PathsToFastaPlugin: processData: must input taxa to paths map.")
        }
        val taxaToPaths = try {
            temp[0].data as Map<String, List<List<HaplotypeNode>>>
        } catch (e: ClassCastException) {
            throw IllegalArgumentException("PathsToFastaPlugin: processData: must input map of type Map<String, Pair<List<HaplotypeNode>, List<HaplotypeNode>?>>.")
        }

        writeFasta(taxaToPaths, outputFile())

        return null

    }

    /**
     * Output base file name
     *
     * @return Output Base VCF File Name
     */
    fun outputFile(): String {
        return myOutputFile.value()
    }

    /**
     * Set Output Base VCF File Name. Output base file name
     *
     * @param value Output Base VCF File Name
     *
     * @return this plugin
     */
    fun outputFile(value: String): PathsToFastaPlugin {
        myOutputFile = PluginParameter<String>(myOutputFile, value)
        return this
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "Paths to Fasta File"
    }

    override fun getToolTipText(): String {
        return "Paths to Fasta File"
    }

}
