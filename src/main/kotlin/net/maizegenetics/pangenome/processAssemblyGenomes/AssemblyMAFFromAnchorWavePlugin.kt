package net.maizegenetics.pangenome.processAssemblyGenomes

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import net.maizegenetics.pangenome.hapCalling.readInKeyFile
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.File
import java.util.stream.Collectors
import javax.swing.ImageIcon

/**
 * This class creates MAF files of assembly genomes aligned to reference genomes
 * using the anchorWave program.
 * Details on anchorwave may be found here:
 *   https://github.com/baoxingsong/AnchorWave
 * Input:
 *   - directory to which output files will be written
 *   - a keyFile that contains information on the assembly fastas, including
 *     where the source files may be found.  The required header for this file are:
 *     NOTE - the AssemblyServerDir and AssemblyGenomeFasta are not used here, but will
 *     be needed in a later step when storing the haplotypes. THey are included here to allow
 *     for a single keyfile to be used throughout the assembly processing steps
 *      - AssemblyServerDir:  server where the assembly files are hosted
 *      - AssemblyGenomeFasta: full path to the assembly fasta stored on the AssemblyServerDir
 *      - AssemblyDir: local directory where the assembly file is stored
 *      - AssemblyFasta: name of the assembly fasta file
 *      - AssemblyDBName: name to be used for this assembly in the database
 *   - gffFile: A Gff file for the reference fasta, to be used when creating a CDS fasta
 *   - refFasta:  Full path to the reference fasta
 *   - numRuns:  number of simultaneous anchorwave runs to perform
 *   - threadsPerRun: the number of threads to give each anchorwave run
 *
 * Output:
 *   - For aligned assembly, a UCSC formatted MAF file will be created and stored to the output folder
 *
 * Note on threads:  it can take up to 50G per thread when running anchorwave.  Consider both the
 * number of threads available on your machine as well as the memory that will be used for each.
 * For example:  If your machine has 512G, no more than 10 threads may be used when running anchorwave
 *   for a single run.  If you want to run 2 alignments in parallel and your machine has 512G, no more
 *   then 4 threadsPerRun
 *
 *    memoryCost ~ <number of assemblies to process> * (80+(<numThreads-1>)*50) == 920GB
 *    E.g.: 2 assemblies with 4 threads each:
 *     memoryCost ~ 2 * (80+(4-1)*50) == 460GB
 *
 *   In the calculation above, the first 80G is for the thread that was subracted in <numThreads-1>)  This
 *   includes 50Gb for the alignment matrix, another 30Gb for genomes and longest path result etc.
 *
 *   When a user has 10 threads available, there are different options, e.g. 2 runs with 5 threads each,
 *   or 5 runs with 2 threads each.  AnchorWave is using a thread for each collinear block, for some cases the last
 *   single collinear block might take sometime which using single thread.
 *   So, setting of 5 runs with 2 thread each is faster than 2 runs with 5 threads each, but cost more mems.
 *
 * Also on threads:  anchorwave does not subtract from what the user gave for number of threads.
 * Per Baoxing: "I did not subtract from the user given number. Since main function is costing a very
 * little resource while the other threads are running."
 *
 * Note on anchorwave:  Install from conda.  When running "anchorwave" it actually exectues a script which
 * checks the cpu capabilities, and then executes the correct anchorwave executable based on the
 * system's instruction set.  Installing anchorwave from conda installs all the different executables
 */
class AssemblyMAFFromAnchorWavePlugin (parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive){
    private val myLogger = Logger.getLogger(AssemblyMAFFromAnchorWavePlugin::class.java)

    // There is no results channel.  The results from the input channel are created MAF files.
    // These will be processed by another plugin (MAFToGVCFPlugin)
    data class InputChannelData( val refFasta: String, val asmFasta: String, val asmDBName: String, val outputDir: String, val gffFile: String, val refSamOutFile:String)

    private var outputDir = PluginParameter.Builder("outputDir", null, String::class.java)
        .guiName("Output Directory")
        .required(true)
        .outDir()
        .description("Output directory for writing files").build()

    private var keyFile = PluginParameter.Builder("keyFile", null, String::class.java)
        .guiName("keyFile")
        .inFile()
        .required(true)
        .description("Name of the Keyfile to process.  Must have columns AssemblyServerDir, AssemblyGenomeFasta, AssemblyDir, AssemblyFasta, and AssemblyDBName. The AssemblyFasta column should contain the name of the assembly fasta file for aligning.  The AssemblyGenomeFasta column should contain the name of the full genome fasta from which the assembly fasta came (it may be the same name as the AssemblyGenomeFasta).  ")
        .build()

    private var gffFile = PluginParameter.Builder("gffFile", null, String::class.java)
        .guiName("Ref GFF3 File")
        .inFile()
        .required(true)
        .description("Reference GFF3 file used to create the CDS fasta for minimap2 alignment").build()

    private var refFasta = PluginParameter.Builder("refFasta", null, String::class.java)
        .guiName("Reference Fasta File")
        .inFile()
        .required(true)
        .description("Full path to reference fasta file, docker specific path if running in a docker").build()

    private var threadsPerRun = PluginParameter.Builder("threadsPerRun", 2, Int::class.javaObjectType)
        .required(false)
        .description("Number of threads to use for each assembly processed.  \nThis value plus the value for numRuns should be determined based on system available threads and memory.")
        .build()

    private var numRuns = PluginParameter.Builder("numRuns", 2, Int::class.javaObjectType)
        .required(false)
        .description("Number of simultaneous assemblies to process. The anchorwave application can take up to 50G per thread for each assembly processed, plus some overhead. Consider this memory factor when providing values for threadsPerRun and numRuns")
        .build()

    private var minimap2Location = PluginParameter.Builder("minimap2Location", "minimap2", String::class.java)
        .guiName("Location of Minimap2 Executable")
        .required(false)
        .description("Location of Minimap2 on file system.  This defaults to use minimap2 if it is on the PATH environment variable.")
        .build()

    private var anchorwaveLocation = PluginParameter.Builder("anchorwaveLocation", "anchorwave", String::class.java)
        .guiName("Location of anchorwave Executable")
        .required(false)
        .description("Location of anchorwave on file system.  This defaults to use anchorwave if it is on the PATH environment variable.")
        .build()

    private var refMaxAlignCov = PluginParameter.Builder("refMaxAlignCov", 1, Int::class.javaObjectType)
        .required(false)
        .description("anchorwave proali parameter R, indicating reference genome maximum alignment coverage  .")
        .build()

    private var queryMaxAlignCov = PluginParameter.Builder("queryMaxAlignCov", 1, Int::class.javaObjectType)
        .required(false)
        .description("anchorwave proali parameter Q, indicating query genome maximum alignment coverage  .")
        .build()

    override fun processData(input: DataSet?): DataSet? {

        // In single-threaded mode, create the CDS fasta from the reference genome
        // and reference GFF file.  Then call minimap to align the reference fasta
        // and newly created CDS fasta, creating an output .SAM file.
        // These files will be used for aligning the assemblies in the multi-threaded section

        // Returns Pair<Map<String,Int>,List<List<String>>> - from minimap2Utils
        var columnMappingAndLines = readInKeyFile(keyFile())

        // Create CDS fasta from reference and gff3 file
        val cdsFasta = "${outputDir()}/refCDS.fa"
        myLogger.info("processData - call createCDSfromRefData ...")
        var success = createCDSfromRefData(refFasta(),gffFile(), cdsFasta,outputDir())

        val colNameMap = columnMappingAndLines.first

        val asmDir = colNameMap["AssemblyDir"]?:-1
        val assemblyFasta = colNameMap["AssemblyFasta"]?:-1
        val assemblyDBName = colNameMap["AssemblyDBName"]?:-1
        val asmServer = colNameMap["AssemblyServerDir"]?:-1

        //Check will automatically throw an IllegalStateException if the logic fails.
        check(asmDir != -1) { "Error processing keyfile.  Must have AssemblyDir column." }
        check(asmServer != -1) { "Error processing keyfile.  Must have AssemblyServerDir column." }
        check(assemblyFasta != -1) { "Error processing keyfile.  Must have AssemblyFasta column." }
        check(assemblyDBName != -1) { "Error processing keyfile.  Must have AssemblyDBName column." }

        // Run minimap against the reference.  Later code uses the ref.sam file
        val justNameRef = File(refFasta()).nameWithoutExtension
        val samOutFile = "${justNameRef}.sam"
        val refSamOutFile = "${outputDir()}/${samOutFile}"

        // If we're running minimap and proali at the same time (different assemblies)
        // then minimap2 should not use all the threads.  Stick with the number of threads
        // the user passed.  Can reconsider this later.
        //val threadnum = Integer.toString(Math.max(TasselPrefs.getMaxThreads() - 1, 1))
        println("Value of minimap2Location: ${minimap2Location()} ")

        val builder = ProcessBuilder(minimap2Location(), "-x", "splice", "-t", threadsPerRun().toString(), "-k", "12",
            "-a", "-p", "0.4", "-N20", refFasta(), cdsFasta)
        var redirectError = outputDir() + "/minimap2Ref_error.log"
        builder.redirectOutput( File(refSamOutFile))
        builder.redirectError( File(redirectError))

        myLogger.info(" Ref minimap Command:" + builder.command().stream().collect(Collectors.joining(" ")));
        var process = builder.start()
        var error = process.waitFor()
        if (error != 0) {
            myLogger.error("minimap2 for ${refFasta} run via ProcessBuilder returned error code $error")
            throw IllegalStateException("Error running minimap2 for reference: ${error}")
        }

        // Now multi-thread the anchorwave alignments for the keyFile assembly fastas
        // This runs minimap2 to align the assemblies against the refCDS fasta, then
        // runs anchorwave proali to create the MAF files
        runAnchorWaveMultiThread(refFasta(), columnMappingAndLines, cdsFasta, gffFile(), refSamOutFile)

        return null
    }

    // This is the multi-threaded portion of the code.  It aligns the assemblies in
    // parallel, based on the numRuns parameter.  Each run can take up to 50G per thread.
    // Each run gets "threadsPerRun()" threads for processing with anchorwave
    fun runAnchorWaveMultiThread(refFasta:String, colsAndData: Pair<Map<String, Int>, List<List<String>>>, cdsFasta:String, gffFile:String, refSamOutFile:String) {

        val colNameMap = colsAndData.first
        val keyFileLines = colsAndData.second

        // Must have columns AssemblyServerDir, AssemblyGenomeFasta, AssemblyDir, AssemblyFasta, and AssemblyDBName. The
        val asmDir = colNameMap["AssemblyDir"]?:-1
        val assemblyFasta = colNameMap["AssemblyFasta"]?:-1
        val assemblyDBName = colNameMap["AssemblyDBName"]?:-1

        //Check will automatically throw an IllegalStateException if the logic fails.
        check(asmDir != -1) { "Error processing keyfile.  Must have AssemblyDir column." }
        check(assemblyFasta != -1) { "Error processing keyfile.  Must have AssemblyFasta column." }
        check(assemblyDBName != -1) { "Error processing keyfile.  Must have AssemblyDBName column." }

        runBlocking {
            // Setup
            val inputChannel = Channel<InputChannelData>()

            // The input channel gets data needed to run minimap2 and align with anchorwave
            launch {
                myLogger.info("Adding entries to the inputChannel:")
                keyFileLines.forEach { lineList ->

                    // Column names were checked for validity above
                    val asmFile = lineList[asmDir] + "/" + lineList[assemblyFasta]
                    val dbName = lineList[assemblyDBName]

                    myLogger.info("Adding: ${asmFile} for processing")
                    //InputChannelData( val refFasta: String, val asmFasta: String, val asmDir: String, val asmDBName: String, val outputDir: String)
                    inputChannel.send(InputChannelData(refFasta,asmFile,dbName, outputDir(), gffFile, refSamOutFile ))
                }
                myLogger.info("Done Adding data to the inputChannel:")
                inputChannel.close() //Need to close this here to show the workers that it is done adding more data
            }

            // This calls mummer4 scripts to process the alignments
            val workerThreads = (1..numRuns()).map { run ->
                launch { alignAssembly(inputChannel,cdsFasta,gffFile()) }
            }

            // Do not need a coroutine that "joins" the threads sa they will all
            // terminate above when there is no more data on the input channel
        }
    }

    // Create the CDS fasta needed for the minimap2 alignments
    fun createCDSfromRefData(refFasta:String, gffFile:String, cdsFasta:String, outputDir:String):Boolean {

        //val command = "anchorwave gff2seq -r ${refFasta} -i ${gffFile} -o ${cdsFasta} "
        val builder = ProcessBuilder(anchorwaveLocation(),"gff2seq","-r",refFasta,"-i",gffFile, "-o",cdsFasta)
        var redirectOutput = outputDir + "/anchorwave_gff2seq_output.log"
        var redirectError = outputDir + "/anchorwave_gff2seq_error.log"
        builder.redirectOutput( File(redirectOutput))
        builder.redirectError( File(redirectError))

        myLogger.info("begin Command:" + builder.command().stream().collect(Collectors.joining(" ")))
        var process = builder.start()
        var error = process.waitFor()
        if (error != 0) {
            myLogger.error("createCDSfromRefData run via ProcessBuilder returned error code $error")
            return false
        }
        return true
    }


    // This function performs the actual alignment using minimap2 and anchorwave-proali
    private suspend fun alignAssembly(inputChannel: Channel<InputChannelData>, cdsFasta:String, gffFile:String) = withContext(Dispatchers.Default) {

        for (assemblyEntry in inputChannel) {
            // Column names were checked for validity above
            val justName = File(assemblyEntry.asmFasta).nameWithoutExtension
            val samShort = "${justName}.sam"
            val asmSamFile = "${assemblyEntry.outputDir}/${samShort}"

            myLogger.info("alignAssembly: asmFileFull: ${assemblyEntry.asmFasta}, outputFile: ${asmSamFile}")
            //val threadnum = Integer.toString(Math.max(TasselPrefs.getMaxThreads() - 1, 1))
            val builder = ProcessBuilder(minimap2Location(), "-x", "splice", "-t", threadsPerRun().toString(), "-k", "12",
                "-a", "-p", "0.4", "-N20", assemblyEntry.asmFasta, cdsFasta)

            var redirectError = "${assemblyEntry.outputDir}/minimap2_${justName}_error.log"
            myLogger.info("redirectError: ${redirectError}")
            builder.redirectOutput( File(asmSamFile))
            builder.redirectError( File(redirectError))
            myLogger.info(" begin minimap assembly Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            var process = builder.start()
            var error = process.waitFor()
            if (error != 0) {
                myLogger.error("minimap2 for assembly ${assemblyEntry.asmFasta} run via ProcessBuilder returned error code $error")
                throw IllegalStateException("alignAssembly: error running minimap2 for ${justName}: $error")
            }
            // We have the SAM File, call proali to align with anchorwave
            runAnchorwaveProali(gffFile, assemblyEntry.refFasta, assemblyEntry.asmFasta, cdsFasta, assemblyEntry.refSamOutFile, asmSamFile)
        }
    }


    // Using the SAM file created by minimap2 runs, the gff3 file, plus the various fastas,
    // run anchorWave with proali.
    // The MAF file should be written to outputDir()/*.maf
    // proali takes alot of memory.  User needs to be sure they have enough on the
    // machine for any mulithreading.
    //
    //  memoryCost ~ <number of assemblies to process> * (80+(<numThreads-1>)*50G)
    //  E.g.: 2 assemblies with 4 threads each:
    //     memoryCost ~ 2 * (80+(4-1)*50) == 460GB
    //
    // Here is the command :
    //   anchorwave proali -i gff3 -r ref.fa -as ref.cds.fa -a asm.sam -ar ref.sam -s asm.fasta -n asm_ref.anchorspro -R 1 -Q 1 -t 4  -o asm_ref.maf
    // The *.anchorspro and *.maf files are output files

    fun runAnchorwaveProali(gffFile:String,refFasta:String, asmFasta:String, cdsFasta:String, refSam:String, asmSam:String) {

        val justNameAsm = File(asmFasta).nameWithoutExtension
        val justNameRef = File(refFasta).nameWithoutExtension

        val anchorsproFile = "${outputDir()}/${justNameAsm}_${justNameRef}.anchorspro"
        val outputFile ="${outputDir()}/${justNameAsm}_${justNameRef}.maf"
        val builder = ProcessBuilder(anchorwaveLocation(),"proali", "-i", gffFile, "-r", refFasta, "-as", cdsFasta,"-a", asmSam, "-ar", refSam,
             "-s", asmFasta, "-n", anchorsproFile,"-R", refMaxAlignCov().toString(), "-Q", queryMaxAlignCov().toString(), "-t",threadsPerRun().toString(),"-o",outputFile)

        var redirectError = "${outputDir()}/proali_${justNameAsm}_outputAndError.log"
        myLogger.info("redirectError: ${redirectError}")
        // NOTE: anchowave proali has an output file parameter, unlike minimap2, which uses
        // command line redirection (ie >) to write the .SAM file
        //
        // Do not use the anchorwave output file parameter as the message redirect output
        // for ProcessBuilder  or you'll end up with the text "AnchorWave done!" in the middle of
        // a reference sequence, causing problems in parsing, and causing the sequence itself to be
        // on a separate line from the data describing it.  Instead, send all program output messages
        // to the error file.
        // Here, we  write all program message output (ie non MAF file output) to a single file
        // named as above.
        builder.redirectOutput( File(redirectError))
        builder.redirectError( File(redirectError))
        myLogger.info(" begin proali Command for ${justNameAsm}:" + builder.command().stream().collect(Collectors.joining(" ")));
        var process = builder.start()
        var error = process.waitFor()
        if (error != 0) {
            myLogger.error("proali for assembly ${asmFasta} run via ProcessBuilder returned error code $error")
            throw IllegalStateException("runAnchorwaveProali: error running proali for ${justNameAsm}")
        }
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = AssemblyMAFFromAnchorWavePlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return("Assembly MAF From Anchorwave")
    }

    override fun getToolTipText(): String {
        return("Use anchor wave to align assembly genome to reference, creating a MAF File for processing with MAFtoGVCFPlugin")
    }

    /**
     * Output directory for writing files
     *
     * @return Output Directory
     */
    fun outputDir(): String {
        return outputDir.value()
    }

    /**
     * Set Output Directory. Output directory for writing
     * files
     *
     * @param value Output Directory
     *
     * @return this plugin
     */
    fun outputDir(value: String): AssemblyMAFFromAnchorWavePlugin {
        outputDir = PluginParameter<String>(outputDir, value)
        return this
    }

    /**
     * Name of the Keyfile to process.  Must have columns
     * AssemblyServerDir, AssemblyGenomeFasta, RefDir, RefFasta,
     * AssemblyDir, AssemblyFasta, and AssemblyDBName. The
     * AssemblyFasta column should contain the name of the
     * assembly fasta file for aligning.  The AssemblyGenomeFasta
     * column should contain the name of the full genome fasta
     * from which the assembly fasta came (it may be the same
     * name as the AssemblyGenomeFasta).
     *
     * @return keyFile
     */
    fun keyFile(): String {
        return keyFile.value()
    }

    /**
     * Set keyFile. Name of the Keyfile to process.  Must
     * have columns AssemblyServerDir, AssemblyGenomeFasta,
     * RefDir, RefFasta, AssemblyDir, AssemblyFasta, and AssemblyDBName.
     * The AssemblyFasta column should contain the name of
     * the assembly fasta file for aligning.  The AssemblyGenomeFasta
     * column should contain the name of the full genome fasta
     * from which the assembly fasta came (it may be the same
     * name as the AssemblyGenomeFasta).
     *
     * @param value keyFile
     *
     * @return this plugin
     */
    fun keyFile(value: String): AssemblyMAFFromAnchorWavePlugin {
        keyFile = PluginParameter<String>(keyFile, value)
        return this
    }

    /**
     * Reference GFF3 file used to create the CDS fasta for
     * minimap2 alignment
     *
     * @return Ref GFF3 File
     */
    fun gffFile(): String {
        return gffFile.value()
    }

    /**
     * Set Ref GFF3 File. Reference GFF3 file used to create
     * the CDS fasta for minimap2 alignment
     *
     * @param value Ref GFF3 File
     *
     * @return this plugin
     */
    fun gffFile(value: String): AssemblyMAFFromAnchorWavePlugin {
        gffFile = PluginParameter<String>(gffFile, value)
        return this
    }

    /**
     * Full path to reference fasta file, docker specific
     * path if running in a docker
     *
     * @return Reference Fasta File
     */
    fun refFasta(): String {
        return refFasta.value()
    }

    /**
     * Set Reference Fasta File. Full path to reference fasta
     * file, docker specific path if running in a docker
     *
     * @param value Reference Fasta File
     *
     * @return this plugin
     */
    fun refFasta(value: String): AssemblyMAFFromAnchorWavePlugin {
        refFasta = PluginParameter<String>(refFasta, value)
        return this
    }

    /**
     * Number of threads to use for each assembly processed.
     * This value plus the value for numRuns should be determined
     * based on system available threads and memory.
     *
     * @return Threads Per Run
     */
    fun threadsPerRun(): Int {
        return threadsPerRun.value()
    }

    /**
     * Set Threads Per Run. Number of threads to use for each
     * assembly processed.
     * This value plus the value for numRuns should be determined
     * based on system available threads and memory.
     *
     * @param value Threads Per Run
     *
     * @return this plugin
     */
    fun threadsPerRun(value: Int): AssemblyMAFFromAnchorWavePlugin {
        threadsPerRun = PluginParameter<Int>(threadsPerRun, value)
        return this
    }

    /**
     * Number of simultaneous assemblies to process. The anchorwave
     * application can take up to 50G per thread for each
     * assembly processed, plus some overhead. Consider this
     * memory factor when providing values for threadsPerRun
     * and numRuns
     *
     * @return Num Runs
     */
    fun numRuns(): Int {
        return numRuns.value()
    }

    /**
     * Set Num Runs. Number of simultaneous assemblies to
     * process. The anchorwave application can take up to
     * 50G per thread for each assembly processed, plus some
     * overhead. Consider this memory factor when providing
     * values for threadsPerRun and numRuns
     *
     * @param value Num Runs
     *
     * @return this plugin
     */
    fun numRuns(value: Int): AssemblyMAFFromAnchorWavePlugin {
        numRuns = PluginParameter<Int>(numRuns, value)
        return this
    }
    /**
     * Location of Minimap2 on file system.  This defaults
     * to use minimap2 if it is on the PATH environment variable.
     *
     * @return Location of Minimap2 Executable
     */
    fun minimap2Location(): String {
        return minimap2Location.value()
    }

    /**
     * Set Location of Minimap2 Executable. Location of Minimap2
     * on file system.  This defaults to use minimap2 if it
     * is on the PATH environment variable.
     *
     * @param value Location of Minimap2 Executable
     *
     * @return this plugin
     */
    fun minimap2Location(value: String): AssemblyMAFFromAnchorWavePlugin {
        minimap2Location = PluginParameter<String>(minimap2Location, value)
        return this
    }

    /**
     * Location of anchorwave on file system.  This defaults
     * to use anchorwave if it is on the PATH environment
     * variable.
     *
     * @return Location of anchorwave Executable
     */
    fun anchorwaveLocation(): String {
        return anchorwaveLocation.value()
    }

    /**
     * Set Location of anchorwave Executable. Location of
     * anchorwave on file system.  This defaults to use anchorwave
     * if it is on the PATH environment variable.
     *
     * @param value Location of anchorwave Executable
     *
     * @return this plugin
     */
    fun anchorwaveLocation(value: String): AssemblyMAFFromAnchorWavePlugin {
        anchorwaveLocation = PluginParameter<String>(anchorwaveLocation, value)
        return this
    }

    /**
     * anchorwave proali parameter R, indicating reference
     * genome maximum alignment coverage  .
     *
     * @return Ref Max Align Cov
     */
    fun refMaxAlignCov(): Int {
        return refMaxAlignCov.value()
    }

    /**
     * Set Ref Max Align Cov. anchorwave proali parameter
     * R, indicating reference genome maximum alignment coverage
     *  .
     *
     * @param value Ref Max Align Cov
     *
     * @return this plugin
     */
    fun refMaxAlignCov(value: Int): AssemblyMAFFromAnchorWavePlugin {
        refMaxAlignCov = PluginParameter<Int>(refMaxAlignCov, value)
        return this
    }

    /**
     * anchorwave proali parameter Q, indicating query genome
     * maximum alignment coverage  .
     *
     * @return Query Max Align Cov
     */
    fun queryMaxAlignCov(): Int {
        return queryMaxAlignCov.value()
    }

    /**
     * Set Query Max Align Cov. anchorwave proali parameter
     * Q, indicating query genome maximum alignment coverage
     *
     * @param value Query Max Align Cov
     *
     * @return this plugin
     */
    fun queryMaxAlignCov(value: Int): AssemblyMAFFromAnchorWavePlugin {
        queryMaxAlignCov = PluginParameter<Int>(queryMaxAlignCov, value)
        return this
    }

}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(AssemblyMAFFromAnchorWavePlugin::class.java)
}
