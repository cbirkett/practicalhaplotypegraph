package net.maizegenetics.pangenome.processAssemblyGenomes

import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import javax.swing.ImageIcon

/**
 * Plugin to split each contig in an assembly fasta into chunks.  By Default 20kbp chunks are created.
 * Any contig less than 20kbp will be unchanged and written to the file denoted by outputFile.
 */
class SplitASMContigsByLengthPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(SplitASMContigsByLengthPlugin::class.java)


    private var inputASMFile = PluginParameter.Builder("inputFile",null, String::class.java)
            .guiName("Input file")
            .required(true)
            .inFile()
            .description("Input Assembly file")
            .build()


    private var splitSize = PluginParameter.Builder("splitSize", 20000, Int::class.javaObjectType)
            .required(false)
            .description("Maximum number of bps in an assembly chunk.")
            .build()

    private var outputSplitFile = PluginParameter.Builder("outputFile",null, String::class.java)
            .guiName("Output file")
            .required(true)
            .outFile()
            .description("File to output the split contig file")
            .build()


    /**
     * Method to split up each contig into splitSize'd chunks.  If contig.length < splitSize, the contig is not chunked.
     */
    override fun processData(input: DataSet?): DataSet? {
        Utils.getBufferedWriter(outputSplitFile()).use { output ->

            val reader = Utils.getBufferedReader(inputASMFile())
            var currentLine: String? = ""

            while (currentLine != null) {

                if (currentLine.startsWith(">")) {

                    val idLine = currentLine
                    //Use a StringBuilder for speed.
                    var finalSequenceLine = StringBuilder()
                    var sequenceLine = reader.readLine()
                    while(sequenceLine != null && !sequenceLine.startsWith(">"))
                    {
                        finalSequenceLine.append(sequenceLine)
                        sequenceLine = reader.readLine()
                    }
                    if(sequenceLine == null) {
                        break
                    }

                    //Split it up and write to fasta
                    val windowedSequence = finalSequenceLine.toString().windowed(splitSize(), splitSize(), true)

                    windowedSequence.forEachIndexed { index: Int, s: String ->
                        output.write("${idLine.split(" ")[0]}_${index}\n")
                        output.write("$s\n")
                    }

                    currentLine = sequenceLine
                }
                else {
                    currentLine = reader.readLine()
                }
            }

        }
        return null
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = SplitASMContigsByLengthPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "SplitASMContigsByLength"
    }

    override fun getToolTipText(): String {
        return "SplitASMContigsByLength to split the ASM contigs by length into chunks."
    }

    /**
     * Input Assembly file
     *
     * @return Input file
     */
    fun inputASMFile(): String {
        return inputASMFile.value()
    }

    /**
     * Set Input file. Input Assembly file
     *
     * @param value Input file
     *
     * @return this plugin
     */
    fun inputASMFile(value: String): SplitASMContigsByLengthPlugin {
        inputASMFile = PluginParameter<String>(inputASMFile, value)
        return this
    }

    /**
     * Maximum number of bps in an assembly chunk.
     *
     * @return Split Size
     */
    fun splitSize(): Int {
        return splitSize.value()
    }

    /**
     * Set Split Size. Maximum number of bps in an assembly
     * chunk.
     *
     * @param value Split Size
     *
     * @return this plugin
     */
    fun splitSize(value: Int): SplitASMContigsByLengthPlugin {
        splitSize = PluginParameter<Int>(splitSize, value)
        return this
    }

    /**
     * File to output the split contig file
     *
     * @return Output file
     */
    fun outputSplitFile(): String {
        return outputSplitFile.value()
    }

    /**
     * Set Output file. File to output the split contig file
     *
     * @param value Output file
     *
     * @return this plugin
     */
    fun outputSplitFile(value: String): SplitASMContigsByLengthPlugin {
        outputSplitFile = PluginParameter<String>(outputSplitFile, value)
        return this
    }

}

fun main() {
    GeneratePluginCode.generateKotlin(SplitASMContigsByLengthPlugin::class.java)
}