package net.maizegenetics.pangenome.processAssemblyGenomes

import net.maizegenetics.pangenome.db_loading.CreateValidIntervalsFilePlugin
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.DirectoryCrawler
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.BufferedWriter
import java.nio.file.Paths
import javax.swing.ImageIcon

/**
 * This method created to facilitate changing the first line of the mummer4 nucmer delta files.
 * When a new version of PHG db is being created, and it is using the same reference and
 * same assmebly fastas, the nucmer delta output can be used from the original alignment.
 *
 * The issue is the first line in this file.  That line indicates where the ref and assembly
 * fastas live.  When running this in docker vs outside docker, the paths are different.
 * And, if running with a version of docker that mounter to "/tempFileDir/..." vs a newer
 * version that mounts to "/phg/...", the first line doesn't match.
 *
 * And non-matching lines result in errors when you get to the show-snps and other stages,
 * which are often still run when the nucmer alignment step is skipped.
 *
 * @author lcj34
 */
class AlterNucmerFirstLinePlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = Logger.getLogger(CreateValidIntervalsFilePlugin::class.java)

    private var inputDir = PluginParameter.Builder("inputDir", null, String::class.java).guiName("Input Directory").required(true).inDir()
            .description("Path for directory holding the files to be changed").build()
    private var refPath = PluginParameter.Builder("refPath", null, String::class.java).guiName("Reference Path").required(true)
            .description("Path where reference fastas live ").build()
    private var asmPath = PluginParameter.Builder("asmPath", null, String::class.java).guiName("Assembly Path").required(true)
            .description("Path where assembly fastas live ").build()
    private var outputDir = PluginParameter.Builder("outputDir", null, String::class.java).guiName("Output Directory").outDir().required(true)
            .description("Full Path to which updated files will be written ").build()

    override fun processData(input: DataSet?): DataSet? {
        myLogger.info("processData begin reading files")
        val fileList = DirectoryCrawler.listPaths("glob:*{.delta,.delta_filtered}", Paths.get(inputDir()))

        for (path in fileList) {
            println("processing file: $path")
            val file = path.toFile()
            val fileName = file.getName()
            val outputFile = "${outputDir()}/${fileName}"

            Utils.getBufferedWriter(outputFile).use {bw ->
                changeFile(path.toString(), fileName, bw)
            }
        }
        return null
    }

    // read the file, alter first line to match user specified ref and assembly paths,
    // write new file to user specified directories
    fun changeFile(file: String, justFile: String, bw : BufferedWriter) {

        try {
            Utils.getBufferedReader(file).use { br ->
                var firstLine = br.readLine()

                var newLine = convertLine(firstLine)
                bw.write(newLine)
                bw.write("\n")
                var curLine = br.readLine()
                while (curLine != null) {

                    bw.write(curLine)
                    bw.write("\n")
                    curLine = br.readLine()
                }
            }
        } catch (exc: Exception) {
            throw IllegalArgumentException("AlterNucmerFirstLinePlugin:changeFile : error reading or writing file " + exc.message)
        }
    }

    // First split by space, so we have have ref and asm
    // then split each by file name
    // Grab the new ref and assembly paths, create new first line
    fun convertLine(oldLine: String): String {
        val refAndAsm = oldLine.split(" ")
        val justRefFile = refAndAsm[0].split("/").last()
        val justAsmFile = refAndAsm[1].split("/").last()

        val newLine = "${refPath()}/${justRefFile} ${asmPath()}/${justAsmFile}"
        return newLine
    }
    override fun getIcon(): ImageIcon? {
        return (null)
    }

    override fun getButtonName(): String {
        return("Alter Delta First Line")
    }

    override fun getToolTipText(): String {
        return("Helper plugin to change first line of a mummer4 nucmer or delta-filter output file so path to ref and assembly fastas are correct")
    }
    /**
     * Path for directory holding the files to be changed
     *
     * @return Input Directory
     */
    fun inputDir(): String {
        return inputDir.value()
    }

    /**
     * Set Input Directory. Path for directory holding the
     * files to be changed
     *
     * @param value Input Directory
     *
     * @return this plugin
     */
    fun inputDir(value: String): AlterNucmerFirstLinePlugin {
        inputDir = PluginParameter<String>(inputDir, value)
        return this
    }

    /**
     * Path where reference fastas live
     *
     * @return Reference Path
     */
    fun refPath(): String {
        return refPath.value()
    }

    /**
     * Set Reference Path. Path where reference fastas live
     *
     *
     * @param value Reference Path
     *
     * @return this plugin
     */
    fun refPath(value: String): AlterNucmerFirstLinePlugin {
        refPath = PluginParameter<String>(refPath, value)
        return this
    }

    /**
     * Path where assembly fastas live
     *
     * @return Assembly Path
     */
    fun asmPath(): String {
        return asmPath.value()
    }

    /**
     * Set Assembly Path. Path where assembly fastas live
     *
     *
     * @param value Assembly Path
     *
     * @return this plugin
     */
    fun asmPath(value: String): AlterNucmerFirstLinePlugin {
        asmPath = PluginParameter<String>(asmPath, value)
        return this
    }

    /**
     * Full Path to which updated files will be written
     *
     * @return Output Directory
     */
    fun outputDir(): String {
        return outputDir.value()
    }

    /**
     * Set Output Directory. Full Path to which updated files
     * will be written
     *
     * @param value Output Directory
     *
     * @return this plugin
     */
    fun outputDir(value: String): AlterNucmerFirstLinePlugin {
        outputDir = PluginParameter<String>(outputDir, value)
        return this
    }

}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(AlterNucmerFirstLinePlugin::class.java)
}