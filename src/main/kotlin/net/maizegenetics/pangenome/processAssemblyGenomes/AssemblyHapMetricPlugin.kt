package net.maizegenetics.pangenome.processAssemblyGenomes

import com.google.common.collect.HashMultimap
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.CreateGraphUtils
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Tuple
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import java.sql.Connection
import java.sql.SQLException
import java.util.*
import javax.swing.ImageIcon
import kotlin.collections.ArrayList


/**
 * This class creates a tab-delimited list of each reference range with the chr/start/end details, and
 * a column for each taxon.  In each taxon's column is the length of the haplotype asociated with
 * that reference range.  A 0 length means the haplotypes was not present for that taxon at that
 * reference range.
 *
 * The taxons included are based on gamete_grp_id from the haplotypes table.  It includes all
 * gamete_grp_ids for which there is only 1 taxon/line_name associated.
 *
 * On a large database, this will run slow unless given enough memory to process results.
 * It is suggested it be run on CBSU with the java -Xmx flag set to a value appropriate for
 * your database size.  With a db size of 63G, and 4.5 million haplotype tables entries
 *
 *
 */
class AssemblyHapMetricPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive){
    private val myLogger = Logger.getLogger(AssemblyHapMetricPlugin::class.java)
    private var configFile = PluginParameter.Builder("configFile", null, String::class.java)
            .description("File with information for database access")
            .required(true)
            .inFile()
            .build()

    private var methods = PluginParameter.Builder("methods", null, String::class.java)
            .description("Commas separated list of method names")
            .required(true)
            .build()
    private var refRangeGroups = PluginParameter.Builder("refRangeGroups", null, String::class.java)
            .description("Commas separated list of method names for the reference range groups to pull")
            .required(true)
            .build()

    private var outputFile = PluginParameter.Builder("outputFile", null, String::class.java)
            .description("File for writing matrix of haplotype coverage")
            .required(true)
            .outFile()
            .build()

    override fun processData(input: DataSet?): DataSet? {

        var methodList = convertMethodsToList(methods())

        myLogger.info("Getting connection ...")
        // get db connection
        val dbConn = DBLoadingUtils.connection(configFile(), false)
        // get reference range map

        myLogger.info("calling getMethodIdFromName with methodlist: $methodList")
        val methodIdToNameMap = getMethodIdFromNames(dbConn, methodList)

        val methodIdList = methodIdToNameMap.keys.joinToString(",")

        myLogger.info("creating refRangeMap")
        var refRangeMap = referenceRangeForAllChromMap(dbConn)

        myLogger.info("Get gameteIdToNameMap")
        // Now get all the haplotype names
        val gameteIdToNameMap = getLineNameFromGameteGroup(dbConn)

        // Get list of gamete_grp_ids associated with a single taxon
        var gameteGrpIdList = getGameteGrpIdsWithSingleTaxon(dbConn)
        gameteGrpIdList.sort()
        var idsForQuery = gameteGrpIdList.joinToString(",")

        // Make list of reference ranges for each assembly
        var querySB = StringBuilder()
        querySB.append("select ref_range_id, gamete_grp_id, seq_len, method_id from haplotypes ")
        querySB.append(" WHERE gamete_grp_id IN (")
        querySB.append(idsForQuery)
        querySB.append(") ")
        querySB.append(" and method_id IN (")
        querySB.append(methodIdList).append(") ")
        querySB.append(" order by ref_range_id, gamete_grp_id;")
        var query = querySB.toString()

        myLogger.info("refRangeToGamete query: $query")
        var refRangeToGameteTuple = HashMultimap.create<Int, Tuple<Int, Int>>();

        try {

            var rs = dbConn.createStatement().executeQuery(query)
            while (rs.next()) {
                // Don't need to save method_id, it is only in query to limit results
                var id = rs.getInt(1);
                var gamete_grp = rs.getInt(2);
                var seq_len = rs.getInt(3)

                refRangeToGameteTuple.put(id, Tuple<Int, Int>(gamete_grp, seq_len));
            }
        } catch (exc: Exception) {
            throw  IllegalStateException("AssemblyHapMetricPlugin: getLineNameFromGameteGroup: Problem querying the database: ", exc);
        }

        var size = refRangeToGameteTuple.keys().size
        println("Calling writeOutFile with refRangeToGameteTUple map keyset size: $size")

        writeOutFile(gameteIdToNameMap, gameteGrpIdList, refRangeMap, refRangeToGameteTuple, outputFile())

        try {
            dbConn.close()
        } catch (sqlExc: SQLException) {
            myLogger.info("AssemblyHapMetricPlugin: Error closing db connection " + sqlExc)
        }
        println("Finished!!")
        return null
    }

    fun writeOutFile(lineToHap: Map<Int, String>, gameteGrpIdList: List<Int>, refRangeMap: MutableMap<Int, ReferenceRange>,
                     rangesForGamete: HashMultimap<Int, Tuple<Int, Int>>, outputFile: String) {
        val headerLineSB = StringBuilder()
        headerLineSB.append("refRangeID\tgroup_type\tchr\tstart\tend")
        val keyList = ArrayList(lineToHap.keys)
        keyList.sort()

        println("processing outFile ...")
        for ( idx in keyList) {

            // only add headers for those specified in the gameteGrpIdList
            if (gameteGrpIdList.contains(idx)) {
                val name = lineToHap.get(idx)
                headerLineSB.append("\t").append(name)
            }
        }
        headerLineSB.append("\n")

        var bw = Utils.getBufferedWriter(outputFile)
        val rangeList = ArrayList(rangesForGamete.keySet())
        rangeList.sort() //
        println("writeOutFile: Size of rangeList ${rangeList.size}")
        try {
            bw.write(headerLineSB.toString())
            for ( rangeId in rangeList) {
                var rrGameteData = rangesForGamete.get(rangeId)
                var line = StringBuilder()
                var rrData = refRangeMap.get(rangeId)
                if (rrData != null) { // should never be null
                    line.append(rangeId).append("\t").append(rrData.groupMethods()).append("\t")
                    line.append(rrData.chromosome().name).append("\t")
                    line.append(rrData.start()).append("\t").append(rrData.end())
                }
                // need the rest of the data for each line
                // create list of gameteid to seqlen, 0 if not present
                //var gameteIdToLen= ArrayList<Int>(28);
                val aSize = gameteGrpIdList.size

                // These need to be organized by lowest to highest gamete_grp_id to match
                // the column headers.
                var gameteIdToLenMap = mutableMapOf<Int, Int>()
                for (data in rrGameteData) {
                    var gId = data.getX()
                    if (gId in gameteGrpIdList) {
                        var len = data.getY()
                        gameteIdToLenMap.put(gId, len)
                    }

                }
                // add remaining to list
                for (idx in gameteGrpIdList) {
                    var gameteLen = gameteIdToLenMap.get(idx)
                    if (gameteLen != null) {
                        line.append("\t").append(gameteLen)
                    } else {
                        line.append("\t").append(0)
                    }
                }
                line.append("\n")
                var finalString = line.toString()
                bw.write(finalString)
            }
            bw.close()

        } catch (exc: Exception) {
            throw IllegalStateException("Error writing output file", exc)
        }

    }

    fun getGameteGrpIdsWithSingleTaxon(db: Connection): MutableList<Int> {
        // Looking for all gamete_grp_ids where there is only 1 taxon/line_name associated with it
        // This means all gamete_grp_ids from the gamete_haplotypes table where there is only 1
        // entry for that gamete_grp_id
        var query = "select gamete_grp_id from gamete_haplotypes group by gamete_grp_id having count(*) = 1;"

        var result = mutableListOf<Int>();
        try {

            var rs = db.createStatement().executeQuery(query)
            while (rs.next()) {
                var id = rs.getInt("gamete_grp_id");
                result.add(id);
            }

        } catch (exc: Exception) {
            throw  IllegalStateException("AssemblyHapMetricPlugin:getGameteGrpIdsWithSingleTaxon: Problem querying the database: ", exc);
        }

        return result
    }

    fun getLineNameFromGameteGroup(db: Connection): MutableMap<Int, String> {
        var querySB = StringBuilder()
        querySB.append("SELECT gamete_haplotypes.gamete_grp_id, genotypes.line_name FROM gamete_haplotypes ")
        querySB.append("INNER JOIN gametes ON gamete_haplotypes.gameteid = gametes.gameteid ")
        querySB.append("INNER JOIN genotypes on gametes.genoid = genotypes.genoid ")
        querySB.append("ORDER BY gamete_haplotypes.gamete_grp_id;")

        var query = querySB.toString()
        var result = mutableMapOf<Int, String>();
        try {

            var rs = db.createStatement().executeQuery(query)
            while (rs.next()) {
                var id = rs.getInt("gamete_grp_id");
                var name = rs.getString("line_name");

                result.put(id, name);
            }

        } catch (exc: Exception) {
            throw  IllegalStateException("AssemblyHapMetricPlugin:getLineNameFromGameteGroup: Problem querying the database: ", exc);
        }

        return result
    }

    fun  referenceRangeForAllChromMap(database: Connection): MutableMap<Int, ReferenceRange> {

        if (database == null) {
            throw IllegalArgumentException("AssemblyHapMetricPlugin: referenceRangeForChromMap: Must specify database connection.");
        }

        var time = System.nanoTime();

        // Create method name for querying initial ref region and inter-region ref_range_group method ids
        var refLine = CreateGraphUtils.getRefLineName(database);

        var refRangeGroupList = convertMethodsToList(refRangeGroups())
        var querySB =  StringBuilder();

        querySB.append("select reference_ranges.ref_range_id, chrom, range_start, range_end, methods.name from reference_ranges ");
        querySB.append(" INNER JOIN ref_range_ref_range_method on ref_range_ref_range_method.ref_range_id=reference_ranges.ref_range_id ");
        querySB.append(" INNER JOIN methods on ref_range_ref_range_method.method_id = methods.method_id ");

        val refRangeGroupString =  refRangeGroupList.map{"'${it}'"}.joinToString(",")
        querySB.append(" AND methods.name in (")
        querySB.append(refRangeGroupString)
        querySB.append(")")

        var query = querySB.toString();

        myLogger.info("referenceRangesForChromMap: query statement: " + query);

        var result = mutableMapOf<Int, ReferenceRange>();
        try {

            var rs = database.createStatement().executeQuery(query)
            while (rs.next()) {
                var id = rs.getInt("ref_range_id");
                var chromosome = rs.getString("chrom");
                var start = rs.getInt("range_start");
                var end = rs.getInt("range_end");
                var methodName = rs.getString("name");
                result.put(id, ReferenceRange(refLine, Chromosome.instance(chromosome), start, end, id, methodName));
            }

        } catch (exc: Exception) {
            throw  IllegalStateException("AssemblyHapMetricPlugin: referenceRanges: Problem querying the database: ", exc);
        }

        myLogger.info("referenceRangeForAllChromMap: time: " + ((System.nanoTime() - time) / 1e9) + " secs.");

        return result;
    }

    override fun getToolTipText(): String {
        return ("Pulls data on haplotype size per reference range for each non-consensus taxon in the data base")
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = AssemblyHapMetricPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return ("AssemblyHapMetricPlugin")
    }

    /**
     * FIle with infor for database access
     *
     * @return Config File
     */
    fun configFile(): String {
        return configFile.value()
    }

    /**
     * Set Config File. FIle with infor for database access
     *
     * @param value Config File
     *
     * @return this plugin
     */
    fun configFile(value: String): AssemblyHapMetricPlugin {
        configFile = PluginParameter<String>(configFile, value)
        return this
    }

    /**
     * Commas separated list of mathod names
     *
     * @return Methods
     */
    fun methods(): String {
        return methods.value()
    }

    /**
     * Set Methods. Commas separated list of mathod names
     *
     * @param value Methods
     *
     * @return this plugin
     */
    fun methods(value: String): AssemblyHapMetricPlugin {
        methods = PluginParameter<String>(methods, value)
        return this
    }

    /**
     * Commas separated list of ref range method names
     *
     * @return RefRangeGroups
     */
    fun refRangeGroups(): String {
        return refRangeGroups.value()
    }

    /**
     * Set refRangeGroups. Commas separated list of mathod names
     *
     * @param value refRangeGroups
     *
     * @return this plugin
     */
    fun refRangeGroups(value: String): AssemblyHapMetricPlugin {
        refRangeGroups = PluginParameter<String>(refRangeGroups, value)
        return this
    }

    /**
     * File for writing matrix of gene coverage
     *
     * @return Output File
     */
    fun outputFile(): String {
        return outputFile.value()
    }

    /**
     * Set Output File. File for writing matrix of gene coverage
     *
     * @param value Output File
     *
     * @return this plugin
     */
    fun outputFile(value: String): AssemblyHapMetricPlugin {
        outputFile = PluginParameter<String>(outputFile, value)
        return this
    }

}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(AssemblyHapMetricPlugin::class.java)
}