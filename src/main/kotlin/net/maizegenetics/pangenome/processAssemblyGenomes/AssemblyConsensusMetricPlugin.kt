package net.maizegenetics.pangenome.processAssemblyGenomes

import com.google.common.collect.HashMultimap
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.pangenome.api.CreateGraphUtils
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import java.sql.Connection
import java.sql.SQLException
import javax.swing.ImageIcon

/**
 * This class takes a config file and list of  methods.  For each reference range ID,
 * it grabs from the haplotypes table the haplotypes_id, ref_range_id, gamete_grp_id, seq_len
 * and method_id for all entries with a method in the provided list.
 *
 * It writes an output file with these tab-delimited column headers:
 * refRangeID      chr     start   end     method  hapid   taxon   len
 *
 * NOTE: The file can get very large if multiple methods are used for a db with
 * millions of entries in the haplotype table.  When running for multiple consensus
 * methods, it is better to run this plugin once for each consensus method.
 */
class AssemblyConsensusMetricPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive){
    private val myLogger = Logger.getLogger(AssemblyConsensusMetricPlugin::class.java)
    private var configFile = PluginParameter.Builder("configFile", null, String::class.java)
            .description("File with information for database access")
            .required(true)
            .inFile()
            .build()

    private var outputFile = PluginParameter.Builder("outputFile", null, String::class.java)
            .description("File for writing matrix of haplotype coverage")
            .required(true)
            .outFile()
            .build()

    private var methods = PluginParameter.Builder("methods", null, String::class.java)
            .description("Commas separated list of mathod names")
            .required(true)
            .build()

    override fun processData(input: DataSet?): DataSet? {

        var methodList = convertMethodsToList(methods())
        myLogger.info("Getting connection ...")
        // get db connection
        val dbConn = DBLoadingUtils.connection(configFile(),false)
        // get reference range map

        myLogger.info("creating refRangeMap")
        var refRangeMap = referenceRangeForAllChromMap(dbConn)

        myLogger.info("Get gameteIdToNameMap")
        // Now get all the haplotype names (query now, allow for consensus multiple names per group
        val gameteIdToNamesMap = getLineNamesFromGameteGroup(dbConn)

        myLogger.info("calling getMethodIdFromName with methodlist: $methodList")
        val methodIdToNameMap = getMethodIdFromNames(dbConn,methodList)
        val methodIdList = methodIdToNameMap.keys.joinToString(",")

        // Make list of reference ranges for each assembly
        var querySB = StringBuilder()
        querySB.append("select haplotypes_id, ref_range_id, gamete_grp_id, seq_len, method_id from haplotypes where method_id in (")
        querySB.append(methodIdList).append(")")
        var refRangeToGameteData = HashMultimap.create<Int, String>();

        var query = querySB.toString()
        myLogger.info("query haplotypes table, with query: $query")

        try {

            var rs = dbConn.createStatement().executeQuery(query)
            while (rs.next()) {
                var hapId = rs.getInt(1) // haplotypes_id
                var id = rs.getInt(2) // ref_range_id
                var gamete_grp = rs.getInt(3)
                var seq_len = rs.getInt(4)
                var method_id = rs.getInt(5)

                var gameteData = StringBuilder()
                gameteData.append(method_id).append(":").append(hapId).append(":")
                gameteData.append(gamete_grp).append(":").append(seq_len)
                refRangeToGameteData.put(id, gameteData.toString());
            }

        } catch ( exc:Exception) {
            throw  IllegalStateException("AssemblyConsensusMetricPlugin: processData: Problem querying the database: ",exc);
        }

        var size = refRangeToGameteData.keys().size
        myLogger.info("Calling writeOutFile with refRangeToGameteTUple map keyset size: $size")

        writeOutFileForMethods(gameteIdToNamesMap,refRangeMap,refRangeToGameteData,methodIdToNameMap,outputFile())

        try {
            dbConn.close()
        } catch (sqlExc: SQLException) {
            myLogger.info("AssemblyConsensusMetricPlugin: Error closing db connection " + sqlExc)
        }

        return null
    }

    fun writeOutFileForMethods(gameteIdToLineNames: HashMultimap<Int,String>, refRangeMap: MutableMap<Int, ReferenceRange>,
                               rangesForGamete: HashMultimap<Int,String>, methodIdToNameMap: Map<Int,String>, outputFile: String) {
        val headerLineSB = StringBuilder()
        headerLineSB.append("refRangeID\tchr\tstart\tend\tmethod\thapid\ttaxon\tlen\n")
        val keyList = ArrayList(gameteIdToLineNames.keySet())

        var bw = Utils.getBufferedWriter(outputFile)
        val rangeList = ArrayList(rangesForGamete.keySet())
        myLogger.info("writeOutFile: Size of rangeList ${rangeList.size}")
        try {
            bw.write(headerLineSB.toString())
            for ( rangeId in rangeList) {
                var rrGameteData = rangesForGamete.get(rangeId)
                var line = StringBuilder()
                var rrData = refRangeMap.get(rangeId)
                if (rrData != null) { // should never be null
                    line.append(rangeId).append("\t").append(rrData.chromosome().name).append("\t")
                    line.append(rrData.start()).append("\t").append(rrData.end()).append("\t")
                }
                // Should have multiple values here - print one on each line

                for (data in rrGameteData) {
                    var cData = StringBuilder()
                    cData.append(line.toString())
                    var dataTokens = data.split(":") // method:haplotypes_id:gamete_grp:seq_len
                    var methodName = methodIdToNameMap.get(dataTokens[0].toInt())
                    var taxon = gameteIdToLineNames.get(dataTokens[2].toInt()).joinToString(",")

                    cData.append(methodName).append("\t").append(dataTokens[1]).append("\t")
                    cData.append(taxon).append("\t")
                    cData.append(dataTokens[3]).append("\n") // append len last
                    var finalString = cData.toString()
                    bw.write(finalString)

                }
            }
            bw.close()

        } catch (exc: Exception) {
            throw IllegalStateException("Error writing output file", exc)
        }

    }

    // Get all line names included for each gamete_grp_id
    fun getLineNamesFromGameteGroup (db: Connection): HashMultimap<Int,String> {
        var querySB = StringBuilder()
        querySB.append("SELECT gamete_haplotypes.gamete_grp_id, genotypes.line_name FROM gamete_haplotypes ")
        querySB.append("INNER JOIN gametes ON gamete_haplotypes.gameteid = gametes.gameteid ")
        querySB.append("INNER JOIN genotypes on gametes.genoid = genotypes.genoid ")
        querySB.append("ORDER BY gamete_haplotypes.gamete_grp_id;")

        var query = querySB.toString()
        var result = HashMultimap.create<Int, String>();
        try {

            var rs = db.createStatement().executeQuery(query)
            while (rs.next()) {
                var id = rs.getInt("gamete_grp_id");
                var name = rs.getString("line_name");
                result.put(id, name);
            }

        } catch ( exc:Exception) {
            throw  IllegalStateException("AssemblyConsensusMetricPlugin: getLineNameSFromGameteGroup: Problem getting line names from db: ",exc);
        }

        return result
    }
    fun  referenceRangeForAllChromMap( database: Connection): MutableMap<Int, ReferenceRange> {

        if (database == null) {
            throw IllegalArgumentException("AssemblyConsensusMetricPlugin: referenceRangeForChromMap: Must specify database connection.");
        }

        var time = System.nanoTime();

        // Create method name for querying initial ref region and inter-region ref_range_group method ids
        var refLine = CreateGraphUtils.getRefLineName( database);

        var querySB =  StringBuilder();

        querySB.append("select reference_ranges.ref_range_id, chrom, range_start, range_end, methods.name from reference_ranges ");
        querySB.append(" INNER JOIN ref_range_ref_range_method on ref_range_ref_range_method.ref_range_id=reference_ranges.ref_range_id ");
        querySB.append(" INNER JOIN methods on ref_range_ref_range_method.method_id = methods.method_id ");
        querySB.append(" AND methods.name in ('");
        querySB.append(DBLoadingUtils.REGION_REFERENCE_RANGE_GROUP);
        querySB.append("','");
        querySB.append(DBLoadingUtils.INTER_REGION_REFERENCE_RANGE_GROUP);
        querySB.append("');")

        querySB.append("select reference_ranges.ref_range_id, chrom, range_start, range_end reference_ranges ");


        var query = querySB.toString();

        myLogger.info("referenceRangesForChromMap: query statement: " + query);

        var result = mutableMapOf<Int,ReferenceRange>();
        try {

            var rs = database.createStatement().executeQuery(query)
            while (rs.next()) {
                var id = rs.getInt("ref_range_id");
                var chromosome = rs.getString("chrom");
                var start = rs.getInt("range_start");
                var end = rs.getInt("range_end");
                var methodName = rs.getString("name");
                result.put(id,  ReferenceRange(refLine, Chromosome.instance(chromosome), start, end, id, methodName));
            }

        } catch ( exc:Exception) {
            throw  IllegalStateException("AssemblyConsensusMetricPlugin: referenceRanges: Problem querying the database: ",exc);
        }

        myLogger.info("referenceRangeForAllChromMap: time: " + ((System.nanoTime() - time) / 1e9) + " secs.");

        return result;
    }

    override fun getToolTipText(): String {
        return ("Provides metrics on haplotype size per reference range for haplotypes created with specified methods.")
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = AssemblyConsensusMetricPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return ("AssemblyConsensusMetricPlugin")
    }

    /**
     * FIle with infor for database access
     *
     * @return Config File
     */
    fun configFile(): String {
        return configFile.value()
    }

    /**
     * Set Config File. FIle with infor for database access
     *
     * @param value Config File
     *
     * @return this plugin
     */
    fun configFile(value: String): AssemblyConsensusMetricPlugin {
        configFile = PluginParameter<String>(configFile, value)
        return this
    }


    /**
     * File for writing matrix of gene coverage
     *
     * @return Output File
     */
    fun outputFile(): String {
        return outputFile.value()
    }

    /**
     * Set Output File. File for writing matrix of gene coverage
     *
     * @param value Output File
     *
     * @return this plugin
     */
    fun outputFile(value: String): AssemblyConsensusMetricPlugin {
        outputFile = PluginParameter<String>(outputFile, value)
        return this
    }

    /**
     * Commas separated list of mathod names
     *
     * @return Methods
     */
    fun methods(): String {
        return methods.value()
    }

    /**
     * Set Methods. Commas separated list of mathod names
     *
     * @param value Methods
     *
     * @return this plugin
     */
    fun methods(value: String): AssemblyConsensusMetricPlugin {
        methods = PluginParameter<String>(methods, value)
        return this
    }

}

// Used also by AsemblyHapMetricPlugin.kt
fun getMethodIdFromNames(dbConn: Connection,methodList:ArrayList<String>): Map<Int,String> {
    var query = "select method_id, name from methods;"
    var methodIdToName = HashMap<Int, String>();

    try {

        var rs = dbConn.createStatement().executeQuery(query)
        while (rs.next()) {
            var id = rs.getInt("method_id");
            var name = rs.getString("name");

            // Only return methods that user requested
            if (methodList.contains(name)) {
                methodIdToName.put(id, name);
            }
        }

    } catch ( exc:Exception) {
        throw  IllegalStateException("AssemblyConsensusMetricPlugin: processData: Problem querying the database: ",exc);
    }
    return methodIdToName
}

// Used also by AssemblyHapMetricPlugin.kt
fun convertMethodsToList(methods: String) : ArrayList<String> {
    val mList = methods.split(",").toTypedArray()
    val result = ArrayList<String>()
    result.addAll(mList)

    return result
}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(AssemblyConsensusMetricPlugin::class.java)
}