package net.maizegenetics.pangenome.processAssemblyGenomes

import com.google.common.collect.Range
import com.google.common.collect.RangeMap
import com.google.common.collect.TreeRangeMap
import htsjdk.variant.variantcontext.VariantContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import net.maizegenetics.dna.map.GenomeSequenceBuilder
import net.maizegenetics.dna.map.Position
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.api.convertVCListToVariantInfoList
import net.maizegenetics.pangenome.db_loading.AnchorDataPHG
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils.createPathNodesForGameteGrp
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.pangenome.fastaExtraction.GVCFSequence
import net.maizegenetics.pangenome.hapCalling.HapCallingUtils
import net.maizegenetics.pangenome.hapCalling.readInKeyFile
import net.maizegenetics.pangenome.processAssemblyGenomes.AssemblyHaplotypesPlugin.ASSEMBLY_ENTRY_POINT
import net.maizegenetics.pangenome.processAssemblyGenomes.AssemblyProcessingUtils.*
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Tuple
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.sql.Connection
import java.sql.SQLException
import javax.swing.ImageIcon

/**
 * This class runs mummer4 alignment of chrom-chrom fastas in a parallel fashion.
 * It expects a directory for both reference and assembly fastas, split by
 * chromosome, as well as a keyFile.
 *
 * The keyfile provides information on the directory and file names for the
 * ref and assembly fastas to be aligned.  The chromosome name and assembly name for loading
 * to the database are also specified in the keyFile, which has these required headers:
 *  AssemblyServerDir RefDir  RefFasta        AssemblyDir     AssemblyGenomeFasta   AssemblyFasta   AssemblyDBName  Chromosome
 *
 * The AssemblyServerDir is the external place (NCBI, NAM genomes, other accessible server) where the
 * assembly full genome fasta may be located.  The "RefDir" and "AssemblyDir" are the directories on the local machine
 * where this software can access the reference/assembly fastas for aligning and loading to the database.
 *
 * The AssemblyGenomeFasta column should contain the full genome fasta from which the separated chromosome fasta is derived.
 * The AssemblyFasta column should contain the single chromosome fasta that will be used for alignment.  It should be
 * pulled from the full genome fasta listed in the AssemblyGenomeFasta column.
 *
 * The fastas listed in the AssembelyGenomeFasta and the AssemblyFasta columns both must live in the folder
 * indicated by the AssemblyDir column.  In addition, the link in the AssemblyServerDir column should be the more permanent
 * location for an external server where the Assembly full genome fasta is stored and may be pulled.
 *
 * The mummer4 alignment scripts will be kicked off in parallel based on
 * the number of threads the user specifies.  Default is 3, which is essentially
 * single threaded and 2 of the 3 are reserved for I/O.
 *
 * Note that running the alignments in parallel will take not just threads, but
 * memory.  This should be considered when the user decides on the number of threads.
 *
 * @author lcj34
 */
class AssemblyHaplotypesMultiThreadPlugin(parentFrame: Frame? = null, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive){
    private val myLogger = Logger.getLogger(AssemblyHaplotypesMultiThreadPlugin::class.java)
    data class InputChannelData( val refFasta: String, val asmFasta: String, val asmDBName: String, val chrom: String, val asmServer: String, val asmFullGenomeFasta: String)
    data class ResultChannelData(val IdToRefRangeMap: Map<Int, ReferenceRange>, val vcList: List<VariantContext>, val fastaInfo: List<String>)

    private var myOutputDir = PluginParameter.Builder("outputDir", null, String::class.java).guiName("Output Directory").required(true).outDir()
            .description("Output directory including trailing / for writing files").build()
    private var keyFile = PluginParameter.Builder("keyFile", null, String::class.java)
            .guiName("keyFile")
            .inFile()
            .required(true)
            .description("Name of the Keyfile to process.  Must have columns AssemblyServerDir, AssemblyGenomeFasta, RefDir, RefFasta, AssemblyDir, AssemblyFasta, AssemblyDBName, and Chromosome. The AssemblyFasta column should contain the name of the assembly chromosome fasta file for aligning.  The AssemblyGenomeFasta column should contain the name of the full genome fasta from which the chromosome only fasta came.  ")
            .build()

    // Non-required parameters - provide if not on user path
    private var mummer4Path = PluginParameter.Builder("mummer4Path", "/mummer/bin/", String::class.java).guiName("Mummer4 Executables Path")
            .inDir()
            .description("Path where mummer4 executables live: nucmer, delta-filter, show-snps, show-coords  ")
            .build()
    private var clusterSize = PluginParameter.Builder("clusterSize", 250, Int::class.javaObjectType).guiName("Mummer4 Nucmer Cluster Size ")
            .description("Cluster size to use with mummer4 nucmer script. ")
            .build()

    private var myGVCFOutputDir = PluginParameter.Builder("gvcfOutputDir", null, String::class.java).guiName("GVCF Output Dir")
            .required(false).outDir()
            .description("Directory for gvcf files to be output for later use")
            .build()

    private var entryPoint = PluginParameter.Builder("entryPoint", ASSEMBLY_ENTRY_POINT.all, ASSEMBLY_ENTRY_POINT::class.java)
            .guiName("Assembly Entry Point")
            .description("Where to begin processing. All runs everything.  Refilter means run from the re-filtering of the coords files. hapltypes runs just the haplotypes processing.")
            .build()

    private var minInversionLen = PluginParameter.Builder("minInversionLen", 7500, Int::class.javaObjectType).guiName("Minimum Inversion Length").required(false)
            .description("Minimum length of inversion for it to be kept as part of the alignment. Default is 7500").build()


    private var loadDB = PluginParameter.Builder("loadDB", true, Boolean::class.javaObjectType).guiName("Load to DataBase")
            .required(false)
            .description("Boolean: true means load haplotypes to db, false means do not populate the database.  Defaults to true")
            .build()

    private var assemblyMethod = PluginParameter.Builder("assemblyMethod", "mummer4", String::class.java).guiName("Assembly Method Name")
            .description("Name to be stored to methods table for assembly method, default is mummer4  ")
            .build()

    private var myNumThreads = PluginParameter.Builder("numThreads", 3, Int::class.javaObjectType)
            .required(false)
            .description("Number of threads used to process assembly chromosomes.  The code will subtract 2 from this number to have the number of worker threads.  It leaves 1 thread for IO to the DB and 1 thread for the Operating System.")
            .build()

    private var isTestMethod = PluginParameter.Builder("isTestMethod", false, Boolean::class.javaObjectType)
        .description("Indication if the data is to be loaded against a test method. Data loaded with test methods are not cached with the PHG ktor server")
        .required(false).build()

    override fun processData(input: DataSet?): DataSet? {

        // Returns Pair<Map<String,Int>,List<List<String>>> - from minimap2Utils
        var columnMappingAndLines = readInKeyFile(keyFile())

        alignChromsMultiThreaded(columnMappingAndLines)

        return null
    }

    fun alignChromsMultiThreaded(colsAndData: Pair<Map<String, Int>, List<List<String>>>) {

        var dbConnect = DBLoadingUtils.connection(false)

        if (dbConnect == null) {
            throw  IllegalArgumentException("alignChromsMultiThreaded: error connecting to database  ");
        }

        val colNameMap = colsAndData.first
        val keyFileLines = colsAndData.second

        //Get out the column indices as they will be consistent for the whole file
        val refDir = colNameMap["RefDir"]?:-1
        val refFasta = colNameMap["RefFasta"]?:-1
        val asmDir = colNameMap["AssemblyDir"]?:-1
        val assemblyFasta = colNameMap["AssemblyFasta"]?:-1
        val assemblyDBName = colNameMap["AssemblyDBName"]?:-1
        val chromosome = colNameMap["Chromosome"]?:-1
        val asmServer = colNameMap["AssemblyServerDir"]?:-1
        val asmFullFasta = colNameMap["AssemblyGenomeFasta"]?:-1

        //Check will automatically throw an IllegalStateException if the logic fails.
        check(refDir != -1) { "Error processing keyfile.  Must have RefDir column." }
        check(asmDir != -1) { "Error processing keyfile.  Must have AssemblyDir column." }
        check(asmServer != -1) { "Error processing keyfile.  Must have AssemblyServerDir column." }
        check(refFasta != -1) { "Error processing keyfile.  Must have RefFasta column." }
        check(assemblyFasta != -1) { "Error processing keyfile.  Must have AssemblyFasta column." }
        check(assemblyDBName != -1) { "Error processing keyfile.  Must have AssemblyDBName column." }
        check(chromosome != -1) { "Error processing keyfile.  Must have Chromosome column." }
        check(asmFullFasta != -1) { "Error processing keyfile.  Must have AssemblyGenomeFasta column." }

        // Setup the Coroutines for parallel processing
        runBlocking {
            // Setup
            val inputChannel = Channel<InputChannelData>()
            val resultChannel = Channel<ResultChannelData>()
            val numThreads = Math.max(numThreads() - 2, 1)

            // The input channel gets the ref/assembly fastas names and the chromosome
            launch {
                myLogger.info("Adding entries to the inputChannel:")
                keyFileLines.forEach { lineList ->

                    // Column names were checked for validity above
                    val refFile = lineList[refDir] + "/" + lineList[refFasta]
                    val asmFile = lineList[asmDir] + "/" + lineList[assemblyFasta]
                    val dbName = lineList[assemblyDBName]
                    val chrom = lineList[chromosome]
                    val asmServer = lineList[asmServer]
                    val asmGenome = lineList[asmDir] + "/" + lineList[asmFullFasta]
                    myLogger.info("Adding: ${refFile} and ${asmFile} for chrom ${chrom} with asmServer ${asmServer}")
                    inputChannel.send(InputChannelData(refFile, asmFile, dbName, chrom, asmServer,asmGenome))
                }
                myLogger.info("Done Adding data to the inputChannel:")
                inputChannel.close() //Need to close this here to show the workers that it is done adding more data
            }

            // This calls mummer4 scripts to process the alignments
            val workerThreads = (1..numThreads).map { threadNum ->
                launch { processAssemblyChrom(inputChannel, resultChannel, dbConnect) }
            }

            // Loading the DB is single threaded.
            if (loadDB()) {
                launch {
                    processDBUploading(resultChannel, dbConnect)
                    myLogger.info("Finished writing to the DB.")
                }
            }


            //Create a coroutine to make sure all the async coroutines are done processing, then close the result channel.
            //If this is not here, this will run forever.
            launch {
                workerThreads.forEach { it.join() }
                resultChannel.close()
            }
        }

        try {
            dbConnect.close();
        } catch (sqle: SQLException) {
            myLogger.warn("createAndLoadAssemblyData: error closing SQL connection: " + sqle.message);
        }
    }

    /**
     * This function calls mummer4 commands to align a species chromosome against
     * a reference chromosome. It calls in succession: nucmer, show-coords, and show-snps
     * It also performs additional filtering as data is prepared to be loaded to the db.
     *
     * Once finished, the alignment and variant data is added to a ResultChannel for
     * single threaded loading to the database.
     */
    private suspend fun processAssemblyChrom(inputChannel: Channel<InputChannelData>,
                                             resultChannel: Channel<ResultChannelData>,
                                             dbConn: Connection)
                                               = withContext(Dispatchers.Default) {
        for (assemblyEntry in inputChannel) {
            // This gets all the code in AssemblyHaplotypesPlugin, up to loading the DB.
            // mummer4 will write output to the directory indicated with the prefix.
            var time = System.nanoTime();

            val ref = assemblyEntry.refFasta
            val assembly = assemblyEntry.asmFasta
            val chrom = assemblyEntry.chrom
            val dbName = assemblyEntry.asmDBName
            val asmServer = assemblyEntry.asmServer
            val asmGenome = assemblyEntry.asmFullGenomeFasta

            var outputDeltaFilePrefix = outputDir() + "/" + "ref_" + dbName + "_" + chrom + "_c" + clusterSize();

            // Define output files to be created or used during processing
            var outputCoordsOrig = outputDeltaFilePrefix + ".coords_orig"
            var outputCoordsFiltered = outputDeltaFilePrefix + ".coords_filtered"

            // *.coords_filteredPlu_noEmbedded contains coordinates from *.coords_filtered, some inversion
            // from the original coords that were returned, all embedded entries removed.
            var outputCoordsNoEmbedded = outputDeltaFilePrefix + ".coords_filteredPlus_noEmbedded"
            var coordsFile = outputDeltaFilePrefix + ".coords_final" // used to create haplotypes

            var snpsForOverlaps = outputDeltaFilePrefix + ".snps_prefiltered" // created in runShowSNPsWithCat
            var snpFile = outputDeltaFilePrefix + ".snps_final"

            myLogger.info("Start processing with entryPoint = ${entryPoint()}")
            if (entryPoint() == ASSEMBLY_ENTRY_POINT.all) {
                time = System.nanoTime()
                myLogger.info("Call alignWithNucmer, delta file will be written to " + outputDeltaFilePrefix + ".delta")
                MummerScriptProcessing.alignWithNucmer(ref, assembly, outputDeltaFilePrefix, outputDir(), mummer4Path(), clusterSize())
                myLogger.info("align with Nucmer for ${assembly} took: " + (System.nanoTime() - time) / 1e9 + " seconds")

            }
            if (entryPoint() == ASSEMBLY_ENTRY_POINT.all || entryPoint() == ASSEMBLY_ENTRY_POINT.deltafilter ) {

                time = System.nanoTime()
                myLogger.info("call runDeltaFilter")
                MummerScriptProcessing.runDeltaFilter(outputDeltaFilePrefix, outputDir(), mummer4Path())
                myLogger.info("runDeltaFilter took: " + (System.nanoTime() - time) / 1e9 + " seconds")

                time = System.nanoTime();
                myLogger.info("call runShowCoords")
                MummerScriptProcessing.runShowCoords(outputDeltaFilePrefix, outputDir(), mummer4Path())
                myLogger.info("runShowCoords took: " + (System.nanoTime() - time) / 1e9 + " seconds")
            }

            if (entryPoint() == ASSEMBLY_ENTRY_POINT.all || entryPoint() == ASSEMBLY_ENTRY_POINT.deltafilter || entryPoint() == ASSEMBLY_ENTRY_POINT.refilter) {
                time = System.nanoTime()
                myLogger.info("Entry point includes REFILTER: call refilterCoords")
                MummerScriptProcessing.refilterCoordsFile(outputDeltaFilePrefix, outputCoordsOrig, outputCoordsFiltered, outputCoordsNoEmbedded,
                        chrom, minInversionLen())
                myLogger.info("refilterCoordsFile for ${chrom} took: " + (System.nanoTime() - time) / 1e9 + " seconds")

                // The coords parameter is the original, non-merged coords file entries containing
                // coordinates removed when filtering that were added back in refilterCoordsFile().
                // This file is used to filter SNPs added back when the original and filtered
                // SNP lists are merged.
                time = System.nanoTime()
                myLogger.info("call runShowSNPs")
                MummerScriptProcessing.runShowSNPsWithCat(outputDeltaFilePrefix, outputCoordsNoEmbedded, outputDir(), mummer4Path(), chrom)
                myLogger.info("runShowSNPsWithCat took: " + (System.nanoTime() - time) / 1e9 + " seconds")

                // *.coords_final starts with the coords_filteredPlus_noEmbedded and splits overlapping entries.
                var outputCoordsFinal = outputDeltaFilePrefix + ".coords_final"
                // The *.snps_prefiltered file is used when processing overlaps to prevent splitting
                // in the middle of an indel
                myLogger.info("second coordinates filtering, call MummerScriptProcessing.filterOverlaps")

                time = System.nanoTime();
                MummerScriptProcessing.filterCoordsOverlaps(outputCoordsNoEmbedded, snpsForOverlaps, outputCoordsFinal)
                myLogger.info("filterCoordsOverlaps took: " + (System.nanoTime() - time) / 1e9 + " seconds")

                // Write final snps file.  This method will create the <outputDeltaFilePrefix>.snps_final file.
                // It filters out any SNP that doesn't fall within a region represented in the *.coords_final file
                time = System.nanoTime()
                myLogger.info("call finalSnpFiltering, verifySnps")
                MummerScriptProcessing.finalSnpFiltering(snpsForOverlaps, outputDeltaFilePrefix, chrom)
                myLogger.info("finalSnpFiltering took: " + (System.nanoTime() - time) / 1e9 + " seconds")
            }

            // The code below is executed for all entry points.

            // Check if mummer4 created a SNP File.  It does not when there
            // are no SNPs found.

            val snpMap: RangeMap<Position, Tuple<String, String>> = if (Files.exists(Paths.get(snpFile)) && File(snpFile).length() > 0){
                myLogger.info("Calling parseMummerSNPFile")
                time = System.nanoTime()
                AssemblyProcessingUtils.parseMummerSNPFile(snpFile, chrom)

            } else {
                myLogger.warn(" NO mummerSNP file created !! processing without SNPs")
                TreeRangeMap.create()
            }

            myLogger.info("parseMummerSNPFile took: " + (System.nanoTime() - time) / 1e9 + " seconds")

            myLogger.info("Call parseCoordinateRegions with refChrom : " + chrom)
            time = System.nanoTime();
            var refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordsFile, chrom);
            myLogger.info("parseCoordinateRegions took: " + (System.nanoTime() - time) / 1e9 + " seconds")

            var refSequence = GenomeSequenceBuilder.instance(ref)
            var assemblySequence = GenomeSequenceBuilder.instance(assembly)

            //Get the mappings between the nonMapped regions in the corrdinates and add them as SNPs
            time = System.nanoTime();
            var nonMappedSnpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings, refSequence, assemblySequence);
            myLogger.info("setupIndelVariants took: " + (System.nanoTime() - time) / 1e9 + " seconds")
            snpMap.putAll(nonMappedSnpMap)

            // We have the alignments.  Connect to the db, pull reference Ranges, create anchors and interanchors
            myLogger.info("call referenceRangeForChromMap - getting anchors for chrom from the db")
            //var idByRangeMap: Map<Integer, ReferenceRange> = AssemblyProcessingUtils.referenceRangeForChromMap( dbConn,   chrom);
            val idByRangeMap = AssemblyProcessingUtils.referenceRangeForChromMap(dbConn, chrom)

            // Get a rangeSet of these anchors
            var  anchors = getAnchorRangeSet(idByRangeMap)

            // Get VariantContext list from the anchors and refMappingPositions created above
            myLogger.info("calling extractAnchorVariantContextsFromAssemblyAlignments")
            time = System.nanoTime()
            var fullVC = if (snpMap.asMapOfRanges().size == 0 && refMappings.size == 1) {
                // no snps from above - we likely have assembly and reference files matching.
                // Just split the chrom based on reference ranges, make all as reference blocks
                myLogger.info("Creating variantContext a single reference block per ref range")
                createVCasRefBlock(refSequence, dbName, anchors, refMappings)
            } else {
                AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSequence, dbName, anchors, refMappings, snpMap)
            }
            myLogger.info("extractAnchorVariantContextsFromAssemblyAlignments took: " + (System.nanoTime() - time) / 1e9 + " seconds")

            time = System.nanoTime();
            var vcsResized : List<VariantContext> = AssemblyProcessingUtils.splitRefRange(fullVC, idByRangeMap, refSequence)
            myLogger.info("splitRefRange took: " + (System.nanoTime() - time) / 1e9 + " seconds")

            if(gVCFOutputDir() != null) {
                var outputGVCFFileName = gVCFOutputDir() + dbName + "_Chr_"+chrom+".g.vcf"

                HapCallingUtils.writeVariantContextsToVCF(fullVC, outputGVCFFileName, null, dbName)
            }

            // Setup result channel with refRangeMap, VC info, and fastas/chrom/dbName info
            val fastaInfo = listOf(ref, assembly, chrom, dbName,asmServer,asmGenome)

            if (loadDB()) {
                // User may want to run this pipeline to create the mummer4 files,
                // but not load the DB.  This scenario could happen if the mummer4
                // alignments were run on different machines, then the mummer4 files
                // were brought back to a single machine for loading to 1 db.
                //
                // Note the data here is for a full chromosome
                resultChannel.send(ResultChannelData(idByRangeMap.toMap(), vcsResized, fastaInfo))
            }
        } // end for-loop
    }

    /**
     * Single threaded function to upload the assembly data to the DB.
     */
    private suspend fun processDBUploading(resultChannel: Channel<ResultChannelData>, dbConn: Connection) = withContext(Dispatchers.Default) {
        var assemblyNameToGameteGrpId  = HashMap<String,Int>()
        val phg = PHGdbAccess(dbConn)

        // Each result channel entry has data for a full chromosome
        for (result in resultChannel) {
            val idByRangeMap = result.IdToRefRangeMap
            val vcs = result.vcList
            // ref and asm in fastasInfo are the full path relative to this processing
            val refAndAsm = result.fastaInfo
            var time = System.nanoTime();

            var gidToAssemblyDataMap =  HashMap<Int, AnchorDataPHG>();
            var refSequence = GenomeSequenceBuilder.instance(refAndAsm.get(0))

            var chrom = refAndAsm.get(2)
            var dbName = refAndAsm.get(3)

            val hapGS = GVCFSequence.instance(refSequence, vcs, true, dbName) // make this once for full list

            var droppedCount = 0;
            var noAnchorSpecificVC = 0
            try  {

                var method = assemblyMethod()
                var time2 = System.nanoTime()

                // load initial data to tables genotypes, gamete_groups, method, etc
                var gameteGrpAndGenomeDataFileIds = loadInitialAssemblyData(dbName, method, clusterSize(), dbConn, pluginParameters(), refAndAsm,isTestMethod())
                var gamete_grp_id = gameteGrpAndGenomeDataFileIds.getX()
                var genomeFileId = gameteGrpAndGenomeDataFileIds.getY()
                myLogger.info("createAndLoadAssemblyData: time to loadInitialAssemblyData " + (System.nanoTime() - time2) / 1e9 + " seconds")
                var mapCount = 0
                var totalCount = 0

                // Create RangeMap for Variant Context
                //RangeMap<Position, VariantContext> positionRangeToVariantContextMap = TreeRangeMap.create()
                var positionRangeToVariantContextMap: RangeMap<Position, VariantContext> = TreeRangeMap.create()
                for( variantContext in vcs) {
                    positionRangeToVariantContextMap.put(Range.closed(Position.of(variantContext.getContig(), variantContext.getStart()),
                            Position.of(variantContext.getContig(), variantContext.getEnd())),
                            variantContext)
                }

                // Get list of inter-anchor reference ranges.
                // Don't close phg after use as it will kill the connection, needed later

                var vcEndProcessed =  ArrayList<Int>()
                var vcStartProcessed =  ArrayList<Int>()

                // Sort the ref_range_ids into a sorted set for processing below
                var intervalIDSet = idByRangeMap.keys.toSortedSet()

                myLogger.info("createAndLoadAssemblyData: begin creating assembly haplotypes data");
                // get variant contexts for each anchor position.
                for ( genome_interval_id in intervalIDSet) {

                    var refRange = idByRangeMap.get(genome_interval_id)

                    if (refRange == null) {
                        // This should never happen, but check required in Kotlin
                        myLogger.warn("no refRange for id ${genome_interval_id}")
                        continue
                    }

                    time2 = System.nanoTime();
                    var anchorSpecificVCList = AssemblyProcessingUtils.findVCListForAnchor(positionRangeToVariantContextMap,
                            Position.of(chrom, refRange.start()), Position.of(chrom, refRange.end()))

                    if (anchorSpecificVCList.size == 0) {
                        noAnchorSpecificVC++;
                        continue  // no assembly mappings for this anchor
                    }

                    var vcStart = anchorSpecificVCList.get(0)
                    var vcEnd = anchorSpecificVCList.get(anchorSpecificVCList.size - 1)
                    var asmStart = vcStart.getStart()
                    var asmEnd = vcEnd.getEnd()


                    // Check if last VC spans more than 1 reference range.
                    // We will only use it to add sequence to the first range it represents.
                    // ONLY variants span reference ranges.  Reference Blocks
                    // have previously been split by reference range.
                    if (asmStart < refRange.start() ) {
                        // This record spans multiple reference ranges
                        if (anchorSpecificVCList.size == 1) {
                            myLogger.info("CreateAndLoadAssembly: VC START overlaps: skipping haplotype for refRange "
                                    + genome_interval_id + ", asmStart=" + asmStart + ", refRange.start=" + refRange.start())
                            noAnchorSpecificVC++ // no haplotype will be created for this reference range
                            continue;
                        }
                        // more than 1 VC record.  Skip the first if this is an ANCHOR or its sequence has
                        // already been added to the end of a previous inter-anchor record.
                        // Start from the second entry in the VC list.
                        var vcTempEnd = vcStart.getEnd()
                        if ( vcStartProcessed.contains(asmStart) || vcEndProcessed.contains(vcTempEnd)) {
                            myLogger.info("CreateAndLoadAssembly: VC START overlaps: skip first VC records for refRange ID "
                                    + genome_interval_id + ", asmStart=" + asmStart + ", refRange.start=" + refRange.start())
                            vcStart = anchorSpecificVCList.get(1)
                            asmStart = vcStart.getStart()
                            anchorSpecificVCList.removeAt(0) // remove so isn't included with variants below
                        } else {
                            myLogger.info("CreateAndLoadAssembly: VC overlaps, giving sequence to reference_range " + genome_interval_id)
                            vcStartProcessed.add(asmStart)
                            vcEndProcessed.add(vcTempEnd)
                        }

                    }

                    // Now check for VC extending beyond the end.
                    // We will only use it to add sequence to an inter-anchor.
                    // ONLY variants span reference ranges.  Reference Blocks
                    // have previously been split by reference range.
                    if (asmEnd > refRange.end() ) {
                        // This record spans multiple reference ranges
                        if (anchorSpecificVCList.size == 1) {
                            myLogger.info("CreateAndLoadAssembly: VC END overlaps: skipping haplotype for refRange "
                                    + genome_interval_id)
                            noAnchorSpecificVC++ // no haplotype will be created for this reference range
                            continue
                        }
                        // more than 1 VC record.  Skip the last if this is an ANCHOR or its sequence has
                        // already been added. Otherwise its sequence will be added to the start
                        // of the next record that it overlaps, which should be an inter-anchor.
                        // Here, end with the previous last entry in the VC list.
                        var vcTempStart = vcEnd.getStart()
                        if ( vcStartProcessed.contains(vcTempStart) || vcEndProcessed.contains(asmEnd)) {
                            myLogger.info("CreateAndLoadAssembly: VC END overlaps: skip last VC records for refRange ID "
                                    + genome_interval_id)
                            var lastEntry = anchorSpecificVCList.size-1
                            vcEnd = anchorSpecificVCList.get(lastEntry - 1)
                            asmEnd = vcEnd.getEnd()
                            anchorSpecificVCList.removeAt(lastEntry) // remove so isn't included with variants below
                        } else {
                            myLogger.info("CreateAndLoadAssembly: VC overlaps, giving sequence to reference_range " + genome_interval_id)
                            vcStartProcessed.add(vcTempStart)
                            vcEndProcessed.add(asmEnd)
                        }

                    }
                    var asmLen = (vcEnd.getEnd() - vcStart.getStart()) + 1
                    var refLen = (refRange.end() - refRange.start()) +  1

                    var asmContig = vcStart.contig // is this ref or query? For now, should be the same for PHG
                    var asmStartCoordinate = vcStart.getAttributeAsInt("ASM_Start",-1)
                    var asmEndCoordinate = vcEnd.getAttributeAsInt("ASM_End",-1)

                    // skip anchors that are too short. 250 is our mincluster size in nucmer
                    // there are some ref anchors whose length < 250.  keep those
                    if ((asmLen < clusterSize()) && asmLen < refLen) {
                        droppedCount++
                        myLogger.info("createAndLoadAssemblyData: Skipping anchor with asmLen " + asmLen + " and refLen " + refLen)
                        continue
                    }

                    // This is still reference sequence.  The asm sequence is created based on what is stored
                    // for the SNPs and the ref sequence.
                    var hapSequence = hapGS.genotypeAsString(refRange.chromosome(), asmStart, asmEnd)

                    val debugInfo = "RefRangeId = $genome_interval_id chr/st/end: ${refRange.chromosome().getName()} " +
                            "/${refRange.start()}/${refRange.end()} " +
                            "AsmStart=${asmStart}, asmEnd= ${asmEnd}, asmLen= ${asmLen} " +
                            ", refLen= ${refLen}, seqLen= ${hapSequence.length} vcListSize: ${anchorSpecificVCList.size}"

                    //myLogger.info(debugInfo) // keep for debugging

                    var variants = convertVCListToVariantInfoList(anchorSpecificVCList, dbName, 0)

                    // Create AnchorDataPHG object for map
                    var refIntervals = Range.closed(Position.of(refRange.chromosome(), refRange.start()),
                            Position.of(refRange.chromosome(), refRange.end()));

                    var aData =  AnchorDataPHG(refIntervals, asmContig, asmStartCoordinate, asmEndCoordinate, ".","none", null, variants, hapSequence, genomeFileId)
                    gidToAssemblyDataMap.put(genome_interval_id, aData)

                    mapCount++;
                    totalCount++;
                    if (mapCount == 3000) {
                        // load after each 3000 entries
                        myLogger.info("createANdLoadAssemblyData process 3000 before load took: " + (System.nanoTime() - time) / 1e9 + " seconds")
                        myLogger.info("AssemblyHaplotypesPlugin:createAndLoadAssemblyData call loadAssemblyDataToD with next 3000, totalCount= " + totalCount)
                        time = System.nanoTime()

                        loadAssemblyDataToDB(gamete_grp_id, method, dbConn, gidToAssemblyDataMap.toMap(), chrom)
                        myLogger.info("createAndLoadAssemblyData process DB 3000 load took: " + (System.nanoTime() - time) / 1e9 + " seconds")
                        gidToAssemblyDataMap.clear()
                        mapCount = 0
                        time = System.nanoTime()
                    }
                }
                myLogger.info("createAndLoadAssemblyData: count of assembly data to load: " + mapCount)
                myLogger.info("createAndLoadAssemblyData: size of vcStartProcessed " + vcStartProcessed.size
                        + " size of vcEndProcessed " + vcEndProcessed.size)
                if (mapCount > 0) {
                    myLogger.info("createAndLoadAssemblyData: call loadAssemblyDataToD with last data, totalCount= " + totalCount);
                    loadAssemblyDataToDB(gamete_grp_id, method, dbConn, gidToAssemblyDataMap, chrom)
                }

                // This will be overwritten as multiple chromosome from the same assembly are processed.
                // This is expected.  The intent is to get the full list of assemblies represented in
                // the key file.
                assemblyNameToGameteGrpId.put(dbName,gamete_grp_id)

                // db connection is closed in calling method

            } catch (ioe: Exception) {
                ioe.printStackTrace()
                throw  IllegalArgumentException("createAndLoadAssemblyData: error loading assembly haplotypes" + ioe.message)
            }

            myLogger.info("createAndLoadAssemblyData: Finished loading assembly chrom data in " + (System.nanoTime() - time) / 1e9
                    + " seconds, num dropped due to length:  " + droppedCount + ", no anchorSpecificVCs: " + noAnchorSpecificVC);
        }

        myLogger.info("createAndLoadAssemblyData: Finished processing resultChannel input, moving to path creation")
        // Add paths to the DB
        for (entry in assemblyNameToGameteGrpId) {
            val dbName = entry.key
            val gamete_grp_id = entry.value
            myLogger.info("creating Paths for assembly $dbName")
            // Add paths to the db
            val hapidList = createPathNodesForGameteGrp(dbName, dbConn, gamete_grp_id)
            if (hapidList.size > 0) {
                val pathBytes = DBLoadingUtils.encodePathsFromIntArray(hapidList)
                val pathMethod = "mummer4_PATH"
                val methodParams: MutableMap<String,String> = mutableMapOf()
                methodParams.put("notes","path created when assembly was loaded")

                val pathid = phg.putPathsData(pathMethod,  methodParams, dbName, null,  pathBytes, isTestMethod())
                myLogger.info("Paths added to db for assembly $dbName, pathid=$pathid")
            } else {
                myLogger.warn("No path entries created for Assembly " + dbName);
            }
        }
    }

    override fun getToolTipText(): String {
        return("Align, create and load assembly haplotypes to database in parallel by chromosome")
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = AssemblyHaplotypesMultiThreadPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return("AssemblyHaplotypesMultiThreadPlugin")
    }

    /**
     * Output directory including trailing / for writing files
     *
     * @return Output Directory
     */
    fun outputDir(): String {
        return myOutputDir.value()
    }

    /**
     * Set Output Directory. Output directory including trailing
     * / for writing files
     *
     * @param value Output Directory
     *
     * @return this plugin
     */
    fun outputDir(value: String): AssemblyHaplotypesMultiThreadPlugin {
        myOutputDir = PluginParameter<String>(myOutputDir, value)
        return this
    }

    /**
     * Name of the Keyfile to process.  Must have columns
     * AssemblyServerDir, AssemblyGenomeFasta, RefDir, RefFasta,
     * AssemblyDir, AssemblyFasta, AssemblyDBName, and Chromosome.
     * The AssemblyFasta column should contain the name of
     * the assembly chromosome fasta file for aligning.  The
     * AssemblyGenomeFasta column should contain the name
     * of the full genome fasta from which the chromosome
     * only fasta came.
     *
     * @return keyFile
     */
    fun keyFile(): String {
        return keyFile.value()
    }

    /**
     * Set keyFile. Name of the Keyfile to process.  Must
     * have columns AssemblyServerDir, AssemblyGenomeFasta,
     * RefDir, RefFasta, AssemblyDir, AssemblyFasta, AssemblyDBName,
     * and Chromosome. The AssemblyFasta column should contain
     * the name of the assembly chromosome fasta file for
     * aligning.  The AssemblyGenomeFasta column should contain
     * the name of the full genome fasta from which the chromosome
     * only fasta came.
     *
     * @param value keyFile
     *
     * @return this plugin
     */
    fun keyFile(value: String): AssemblyHaplotypesMultiThreadPlugin {
        keyFile = PluginParameter<String>(keyFile, value)
        return this
    }

    /**
     * Path where mummer4 executables live: nucmer, delta-filter,
     * show-snps, show-coords
     *
     * @return Mummer4 Executables Path
     */
    fun mummer4Path(): String {
        return mummer4Path.value()
    }

    /**
     * Set Mummer4 Executables Path. Path where mummer4 executables
     * live: nucmer, delta-filter, show-snps, show-coords
     *
     *
     * @param value Mummer4 Executables Path
     *
     * @return this plugin
     */
    fun mummer4Path(value: String): AssemblyHaplotypesMultiThreadPlugin {
        mummer4Path = PluginParameter<String>(mummer4Path, value)
        return this
    }

    /**
     * Cluster size to use with mummer4 nucmer script.
     *
     * @return Mummer4 Nucmer Cluster Size
     */
    fun clusterSize(): Int {
        return clusterSize.value()
    }


    /**
     * Set Mummer4 Nucmer Cluster Size . Cluster size to use
     * with mummer4 nucmer script.
     *
     * @param value Mummer4 Nucmer Cluster Size
     *
     * @return this plugin
     */
    fun clusterSize(value: Int): AssemblyHaplotypesMultiThreadPlugin {
        clusterSize = PluginParameter<Int>(clusterSize, value)
        return this
    }

    /**
     * Directory for gvcf files to be output for later use
     *
     * @return GVCF Output Dir
     */
    fun gVCFOutputDir(): String?{
        return myGVCFOutputDir.value()
    }

    /**
     * Set GVCF Output Dir. Directory for gvcf files to be
     * output for later use
     *
     * @param value GVCF Output Dir
     *
     * @return this plugin
     */
    fun gVCFOutputDir(value: String): AssemblyHaplotypesMultiThreadPlugin {
        myGVCFOutputDir = PluginParameter<String>(myGVCFOutputDir, value)
        return this
    }

    /**
     * Where to begin processing. All runs everything.  Refilter
     * means run from the re-filtering of the coords files.
     * hapltypes runs just the haplotypes processing.
     *
     * @return Assembly Entry Point
     */
    /**
     * Where to begin processing. All runs everything.  Refilter
     * means run from the re-filtering of the coords files.
     * hapltypes runs just the haplotypes processing.
     *
     * @return Assembly Entry Point
     */
    fun entryPoint(): ASSEMBLY_ENTRY_POINT {
        return entryPoint.value()
    }

    /**
     * Set Assembly Entry Point. Where to begin processing.
     * All runs everything.  Refilter means run from the re-filtering
     * of the coords files. hapltypes runs just the haplotypes
     * processing.
     *
     * @param value Assembly Entry Point
     *
     * @return this plugin
     */
    /**
     * Set Assembly Entry Point. Where to begin processing.
     * All runs everything.  Refilter means run from the re-filtering
     * of the coords files. hapltypes runs just the haplotypes
     * processing.
     *
     * @param value Assembly Entry Point
     *
     * @return this plugin
     */
    fun entryPoint(value: ASSEMBLY_ENTRY_POINT): AssemblyHaplotypesMultiThreadPlugin {
        entryPoint = PluginParameter<ASSEMBLY_ENTRY_POINT>(entryPoint, value)
        return this
    }

    /**
     * Minimum length of inversion for it to be kept as part
     * of the alignment. Default is 7500
     *
     * @return Minimum Inversion Length
     */
    /**
     * Minimum length of inversion for it to be kept as part
     * of the alignment. Default is 7500
     *
     * @return Minimum Inversion Length
     */
    fun minInversionLen(): Int {
        return minInversionLen.value()
    }

    /**
     * Set Minimum Inversion Length. Minimum length of inversion
     * for it to be kept as part of the alignment. Default
     * is 7500
     *
     * @param value Minimum Inversion Length
     *
     * @return this plugin
     */
    /**
     * Set Minimum Inversion Length. Minimum length of inversion
     * for it to be kept as part of the alignment. Default
     * is 7500
     *
     * @param value Minimum Inversion Length
     *
     * @return this plugin
     */
    fun minInversionLen(value: Int): AssemblyHaplotypesMultiThreadPlugin {
        minInversionLen = PluginParameter<Int>(minInversionLen, value)
        return this
    }

    /**
     * Boolean: true means load haplotypes to db, false means
     * do not populate the database.  Defaults to true
     *
     * @return Load to DataBase
     */
    /**
     * Boolean: true means load haplotypes to db, false means
     * do not populate the database.  Defaults to true
     *
     * @return Load to DataBase
     */
    fun loadDB(): Boolean {
        return loadDB.value()
    }

    /**
     * Set Load to DataBase. Boolean: true means load haplotypes
     * to db, false means do not populate the database.  Defaults
     * to true
     *
     * @param value Load to DataBase
     *
     * @return this plugin
     */
    /**
     * Set Load to DataBase. Boolean: true means load haplotypes
     * to db, false means do not populate the database.  Defaults
     * to true
     *
     * @param value Load to DataBase
     *
     * @return this plugin
     */
    fun loadDB(value: Boolean): AssemblyHaplotypesMultiThreadPlugin {
        loadDB = PluginParameter<Boolean>(loadDB, value)
        return this
    }

    /**
     * Name to be stored to methods table for assembly method,
     * default is mummer4
     *
     * @return Assembly Method Name
     */
    fun assemblyMethod(): String {
        return assemblyMethod.value()
    }

    /**
     * Set Assembly Method Name. Name to be stored to methods
     * table for assembly method, default is mummer4
     *
     * @param value Assembly Method Name
     *
     * @return this plugin
     */
    fun assemblyMethod(value: String): AssemblyHaplotypesMultiThreadPlugin {
        assemblyMethod = PluginParameter<String>(assemblyMethod, value)
        return this
    }

    /**
     * Number of threads used to process assembly chromosomes.
     *  The code will subtract 2 from this number to have
     * the number of worker threads.  It leaves 1 thread for
     * IO to the DB and 1 thread for the Operating System.
     *
     * @return Num Threads
     */
    fun numThreads(): Int {
        return myNumThreads.value()
    }

    /**
     * Set Num Threads. Number of threads used to process
     * assembly chromosomes.  The code will subtract 2 from
     * this number to have the number of worker threads.
     * It leaves 1 thread for IO to the DB and 1 thread for
     * the Operating System.
     *
     * @param value Num Threads
     *
     * @return this plugin
     */
    fun numThreads(value: Int): AssemblyHaplotypesMultiThreadPlugin {
        myNumThreads = PluginParameter<Int>(myNumThreads, value)
        return this
    }

    /**
     * Indication if the data is to be loaded against a test
     * method. Data loaded with test methods are not cached
     * with the PHG ktor server
     *
     * @return Is Test Method
     */
    fun isTestMethod(): Boolean {
        return isTestMethod.value()
    }

    /**
     * Set Is Test Method. Indication if the data is to be
     * loaded against a test method. Data loaded with test
     * methods are not cached with the PHG ktor server
     *
     * @param value Is Test Method
     *
     * @return this plugin
     */
    fun isTestMethod(value: Boolean): AssemblyHaplotypesMultiThreadPlugin {
        isTestMethod = PluginParameter<Boolean>(isTestMethod, value)
        return this
    }


}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(AssemblyHaplotypesMultiThreadPlugin::class.java)
}