package net.maizegenetics.pangenome.liquibase

import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.File
import java.nio.file.Files
import java.util.*
import java.util.stream.Collectors
import javax.swing.ImageIcon
import java.lang.Compiler.command



/**
 * This script is the second of a pair.  When called from the RunLiquibaseUPdates.sh, it follows
 * a call to CheckDBVersionPlugin.  The latter verifies the db is a candidate for liquibase
 * updates (ie, it contains tables/columns indicating the version is appropriate for the
 * current liquibase changesets).
 *
 * This plugin assumes a ParameterCache is used for the db configuration variables. The parameter
 * cache is added to the command line prior to the plugin name, as below:
 *
 * /tassel-5-standalone/run_pipeline.pl ${fullXmx} -debug -configParameters $DATA_PATH/$dbConfigFile -CheckDBVersionPlugin -outputDir $OUTPUT_PATH -endPlugin -LiquibaseUpdatePlugin -outputDir $OUTPUT_PATH -endPlugin
 *
 * CheckDBVersionPlugin writes one of 2 files: either run_yes.txt or run_no.txt
 * They are written to the same output directory that is mounted here.  If run_yes.txt
 * is not present, this script will abort with an error message indicating the db cannot be
 * updated.
 *
 * LiquibaseUpdatePlugin runs liquibase with a jdbc driver, determined based
 * on the DBtype parameter from the config file.  It is assumed this script is run from
 * within a PHG liquibase docker. The correct jdbc driver (sqlite or postgres) is selected based on the
 * DBtype parameter present in the config file.
 *
 * This method is run for both the "changeLogSync" command (which marks all changes logs as "ran"),
 * and the "update" command, which executes each changeset from the changelogs files.
 *
 */
class LiquibaseUpdatePlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive){
    private val myLogger = Logger.getLogger(LiquibaseUpdatePlugin::class.java)

    private var outputDir = PluginParameter.Builder("outputDir", null, String::class.java)
            .description("Directory path to write any liquibase output files.")
            .guiName("Output Directory")
            .required(true)
            .outDir()
            .build()

    private var command = PluginParameter.Builder("command", "update", String::class.java)
            .description("Command for liquibase to execute: must be update or changeLogSync, defaults to update.")
            .guiName("Liquibase command")
            .build()

    override fun processData(input: DataSet?): DataSet? {

        // check required parameters from the parameter cache
        val dbTypeOpt = ParameterCache.value("DBtype")
        if (!dbTypeOpt.isPresent) {
            throw java.lang.IllegalStateException("CheckDBVersionPlugin: processData: DBtype must be set in configuration file (ParameterCache)")
        }
        val dbType = dbTypeOpt.get()
        if (dbType.isNullOrEmpty()) {
            throw IllegalArgumentException("LiquibaseUpdatePlugin:processData: configfile must include DBtype parameter with value of either sqlite or postgres")
        }

        val dbNameOpt = ParameterCache.value("DB")
        val dbName = dbNameOpt.get()
        if (dbName.isNullOrEmpty()) {
            throw IllegalArgumentException("LiquibaseUpdatePlugin:processData: configfile must include DB parameter with name of database.")
        }

        val user = ParameterCache.value("user").get()
        val pwd = ParameterCache.value("password").get()

        var jdbcList: MutableList<String> = mutableListOf()

        try {

            // Check output dir for a file called "run_yes.txt".  This would have been created
            // from the CheckDBVersionPlugin call, which proceeds the invocation of this plugin
            // when calling the RunLiquibaseUpdates.sh script

            val yesTxt = outputDir() + "/run_yes.txt"
            val yesFile = File(yesTxt)
            if (!yesFile.exists()) {
                myLogger.error("LiquibaseUpdatePLugin: File $yesTxt does not exist.  CheckDBVersionPlugin has determined your database is not recent enough to be updated with liquibase.  It does not contain the variants table.")
                throw IllegalArgumentException("LiquibaseUpdatePlugin: database is too old to be updated with the liquibase changesets")
            }

            // This adds just the file name to the jdbcList.  The drivers will begin with "postgresql"
            // or "sqlite" but the versions may differ.  Should only be 1 of each type stored in the docker

            File("/liquibase/lib/").listFiles().forEach {
                jdbcList.add(it.getName())
            }

            // Find the name of the jdbc driver file, these should have been copied
            // to /liquibase/lib in the Dockerfile.
            var jdbcDriver: String = "null"
            for (driver in jdbcList) {
                if (driver.contains(dbType)) {
                    jdbcDriver = driver
                }
            }

            if (jdbcDriver == "null") {
                throw IllegalArgumentException("LiquibaseUpdatePlugin:processData: Docker container /liquibase/lib has no jdbc driver matching $dbType")
            }

            // Create sqlite/postgres specific variables for the liquibase command
            var driverLine: String = ""
            var urlLine: String = ""
            if (dbType == "sqlite") {
                driverLine = "--driver=org.sqlite.JDBC"
                var urlSB = StringBuilder()
                urlSB.append("--url=jdbc:sqlite:")
                urlSB.append(dbName)
                urlLine = urlSB.toString()

            } else {
                var host = ParameterCache.value("host").get()
                driverLine = "--driver=org.postgresql.Driver"
                var urlSB = StringBuilder()
                urlSB.append("--url=jdbc:postgresql://")
                urlSB.append(host)
                urlSB.append("/")
                urlSB.append(dbName)
                urlLine = urlSB.toString()
            }

            var usrLine = "--username=$user"
            var pwdLine = "--password=$pwd"

            // The Docker file puts the user into the /liquibase directory
            // drivers (postgres and sqlite jars) were added to the /liquibase/lib
            // dir, and from there are automatically added to the classpath by liquibase

            // --verbose is not valid for update command.  Needed for details of "status" command
            // for change sets
            val builder =  if (command().toLowerCase().equals("status")) {
                ProcessBuilder(
                        "liquibase", driverLine, urlLine, usrLine, pwdLine,
                        "--changeLogFile=/liquibase/changelogs/db.changelog-master.xml",command(),"--verbose")
            } else {
                ProcessBuilder(
                        "liquibase", driverLine, urlLine, usrLine, pwdLine,
                        "--changeLogFile=/liquibase/changelogs/db.changelog-master.xml","--loglevel=FINE",command())
            };

            var redirectOutput = outputDir() + "/liquibase_" + command() + "_output.log"
            var redirectError = outputDir() + "/liquibase_" + command() + "_error.log"
            builder.redirectOutput( File(redirectOutput));
            builder.redirectError( File(redirectError));

            myLogger.info("Please wait, begin Command:" + builder.command().stream().collect(Collectors.joining(" ")));
            var process = builder.start();
            var error = process.waitFor();
            if (error != 0) {
                myLogger.error("liquibase run via ProcessBuilder returned error code $error")
            }

        }catch (exc: Exception) {
            myLogger.debug(exc.message, exc)
            throw IllegalArgumentException("LiquibaseUpdatePlugin::processData: problem running liquibase")
        }

        return null

    }

    override fun getIcon(): ImageIcon? {
        val imageURL = LiquibaseUpdatePlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "Liqiibase Update"
    }

    override fun getToolTipText(): String {
        return "Run liquibase to apply changesets for db updates"
    }

    /**
     * Directory path to write any liquibase output files.
     *
     * @return Output Directory
     */
    fun outputDir(): String {
        return outputDir.value()
    }

    /**
     * Set Output Directory. Directory path to write any liquibase
     * output files.
     *
     * @param value Output Directory
     *
     * @return this plugin
     */
    fun outputDir(value: String): LiquibaseUpdatePlugin {
        outputDir = PluginParameter(outputDir, value)
        return this
    }

    /**
     * Command for liquibase to execute: must be update or
     * changeLogSync, defaults to update.
     *
     * @return Liquibase command
     */
    fun command(): String {
        return command.value()
    }

    /**
     * Set Liquibase command. Command for liquibase to execute:
     * must be update or changeLogSync, defaults to update.
     *
     * @param value Liquibase command
     *
     * @return this plugin
     */
    fun command(value: String): LiquibaseUpdatePlugin {
        command = PluginParameter(command, value)
        return this
    }

}
fun main(args: Array<String>) {
    GeneratePluginCode.generate(LiquibaseUpdatePlugin::class.java)
}