package net.maizegenetics.pangenome.liquibase

import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.ParameterCache
import net.maizegenetics.plugindef.PluginParameter
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.File
import java.nio.file.Files
import javax.swing.ImageIcon


/**
 * This class connects to a data base, checks the db for the presense of specific
 * schema, and writes a file to the output directory indicating presence or absence of
 * the queried schema.
 *
 * This is used to determine if liquibase should run.  Liquibase is initially run on a
 * version of the PHG db that contains the "variants" table.  IF this table is not present,
 * this version of liquibase will not run, and user will be instructed to use an older
 * PHG version for connecting.
 *
 * The table or fields in a table that are checked will change as the db schema changes.
 * There will be a new version of a Liquibase Docker created when ever the PHG db
 * schema changes enough that liquibase alone cannot update the data.  IE, the PHG code
 * will branch and the list of changesets will start fresh.  For example:  The change
 * to add variants as "list<Long>" vs List<VariantContext>" with the addition of the variants
 * and alleles tables was major.  Any changes similar to this, where the internal data types
 * change, will require a new PHG and PHG liquibase branch.
 *
 * The PHG liquibase docker version number must be in-sync with the PHG version.  Whenever a
 * new PHG Docker version comes out, a new Liquibase Docker version must also be created
 * with the same version number.  This is true even if there were no db changes.  This is to
 * facilitate the user knowing which PHG liquibase docker matches their PHG docker version.
 *
 * Details of this Plugin:
 * - connect to DB
 * - query db for specified schema
 * - write file to output directory:
 *      - <outputDir>/run_yes.txt OR
 *      - <outputDir>/run_no.txt
 *      run_yes.txt has 1 line that says "yes"
 *      run_no.txt has 1 line that says "no" with details on which table/columns are missing
 * Presence of "run_yes.txt file is all that is needed to run liquibase update (called via another script)
 */

class CheckDBVersionPlugin(parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(CheckDBVersionPlugin::class.java)

    private var outputDir = PluginParameter.Builder("outputDir", null, String::class.java)
            .description("Directory path to output file indicating whether to run liquibase.")
            .guiName("Output Directory")
            .required(true)
            .outDir()
            .build()

    override fun processData(input: DataSet?): DataSet? {

        // Delete files if they exist prior to running this test
        var yesTxt = outputDir() + "/run_yes.txt"
        val yesFile = File(yesTxt)
        myLogger.info("Deleting yesFile $yesTxt if it exists")
        Files.deleteIfExists(yesFile.toPath())

        var noTxt = outputDir() + "run_no.txt"
        val noFile = File(noTxt)
        myLogger.info("Deleting noFile $noTxt if it exists")
        Files.deleteIfExists(noFile.toPath())

        val dbTypeOpt = ParameterCache.value("DBtype")
        if (!dbTypeOpt.isPresent) {
            throw java.lang.IllegalStateException("CheckDBVersionPlugin: processData: DBtype must be set in configuration file (ParameterCache)")
        }
        val dbType = dbTypeOpt.get()

        val tableName = "variants"
        try {
            val dbConn = DBLoadingUtils.connection(false)
            var querySB = StringBuilder()
            if (dbType == "sqlite") {
                querySB.append("select name FROM sqlite_master where type='table' and name='")
                querySB.append(tableName)
                querySB.append("';")
            } else if (dbType == "postgres") {
                querySB.append("select table_name from information_schema.tables where table_name='")
                querySB.append(tableName)
                querySB.append("';")
            } else {
                dbConn.close()
                throw IllegalStateException("CheckDBVersionPlugin::processdata: unsupported DB type $dbType")
            }
            var query = querySB.toString()

            myLogger.info("queueHaplotypeNodesByRange: query: $query")

            val rs = dbConn.createStatement().executeQuery(query)
            if (rs.next()) {
                // table exists - write run_yes.txt
                var outputFile = outputDir() + "/run_yes.txt"
                File(outputFile).bufferedWriter().use { out ->
                    out.write("yes\n")
                }
            } else {
                var outputFile = outputDir() + "/run_no.txt"
                File(outputFile).bufferedWriter().use { out ->
                    out.write("no - to update, db must be version that contains the variants table\n")
                }
            }
            dbConn.close()

        } catch (exc: Exception) {
            myLogger.debug(exc.message, exc)
            throw IllegalStateException("CheckDBVersionPlugin::processdata: Problem connecting to database : " + exc.message)
        }

        return null
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = CheckDBVersionPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "Check DB"
    }

    override fun getToolTipText(): String {
        return "Check DB version for liquibase updates"
    }

    /**
     * Directory path to output file indicating whether to
     * run liquibase.
     *
     * @return Output Directory
     */
    fun outputDir(): String {
        return outputDir.value()
    }

    /**
     * Set Output Directory. Directory path to output file
     * indicating whether to run liquibase.
     *
     * @param value Output Directory
     *
     * @return this plugin
     */
    fun outputDir(value: String): CheckDBVersionPlugin {
        outputDir = PluginParameter(outputDir, value)
        return this
    }

}
