package net.maizegenetics.pangenome.db_loading

import com.google.common.collect.BoundType
import com.google.common.collect.Range
import com.google.common.collect.RangeMap
import com.google.common.collect.TreeRangeMap
import htsjdk.variant.variantcontext.VariantContext
import htsjdk.variant.vcf.VCFFileReader
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.dna.map.GenomeSequenceBuilder
import net.maizegenetics.dna.map.Position
import net.maizegenetics.dna.snp.io.ReadBedfile
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.canVariantInfoBeAddedToRefBlockList
import net.maizegenetics.pangenome.api.convertGVCFContextToInfo
import net.maizegenetics.pangenome.api.mergeRefBlocks
import net.maizegenetics.pangenome.fastaExtraction.GVCFSequence
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Sizeof
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.File
import java.sql.Connection
import javax.swing.ImageIcon

/**
 * Plugin to load up a list of GVCF files given an input keyfile.
 *
 * This plugin will open up the key file and loop through each record loading up the db.
 *
 * TODO work on multithreading this process for speed increases.
 */
class LoadHaplotypesFromGVCFPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = Logger.getLogger(LoadHaplotypesFromGVCFPlugin::class.java)

    //Create a Data class to hold the information needed to write to the DB.
    //This is just a temporary object so we can pass along all the different variables easily.
    data class ChromosomeGVCFRecord(val currentChrom : String, val currentTaxon : String, val gameteId : Int, val anchorDataMapping : Map<Int,AnchorDataPHG>, val ghd : GenoHaploData)

    private var myWGSKeyFile = PluginParameter.Builder("wgsKeyFile", null, String::class.java)
            .description("Keyfile used to specify the inputs to the DB.  Must have columns sample_name, files, type, chrPhased, genePhased, and phasingConf. Optionally you can have sample_description. Ordering of the columns does not matter.\n" +
                    "sample_name is the name of the taxon you are uploading.\n" +
                    "files is a comma-separated list of file names(without path) providing the names of the GVCF files to be uploaded.  If there are multiple files, gamete_ids will be assigned in order.\n" +
                    "type is the type of the files.  To use this for this Plugin GVCF needs to be specified." +
                    "chrPhased and genePhased must be either 'true' or 'false' and represent if the chromosome or the genes are phased or not\n" +
                    "phasingConf must be a number between 0.0 and 1.0 representing the confidence in the phasing.\n" +
                    "sample_description to provide a short description of the sample name to be uploaded.  If this column is not specified an empty description will be used.")
            .inFile()
            .required(true)
            .build()

    private var myGVCFDirectory = PluginParameter.Builder("gvcfDir",null, String::class.java)
            .description("Directory holding all the gvcf files.")
            .inDir()
            .required(true)
            .build()

    private var myReference = PluginParameter.Builder("referenceFasta", null, String::class.java)
            .description("Input Reference Fasta")
            .required(true)
            .inFile()
            .build()

    private var bedFile = PluginParameter.Builder("bedFile", null, String::class.java)
            .description("File holding the Reference Range Information")
            .required(true)
            .inFile()
            .build()

    private var myMethod = PluginParameter.Builder("haplotypeMethodName",null,String::class.java)
            .description("Method name for the haplotypes being uploaded.")
            .required(true)
            .build()

    private var myMethodDescription = PluginParameter.Builder("haplotypeMethodDescription",null,String::class.java)
            .description("Method description to be written if the method is new.")
            .required(false)
            .build()

    private var myNumThreads = PluginParameter.Builder("numThreads",3, Int::class.javaObjectType)
            .description("Number of threads used to upload.  The GVCF upload will subtract 2 from this number to have the number of worker threads.  It leaves 1 thread for IO to the DB and 1 thread for the Operating System.")
            .required(false)
            .build()

    private var myMaxDBUploadQueue = PluginParameter.Builder("maxNumHapsStaged", 10000, Int::class.javaObjectType)
            .description("Number of Haplotype instances to stage per chromosome before a DB write is triggered.  Lower this number if you are running into RAM issues.  It will take longer to process, but should help balance the load.")
            .required(false)
            .build()

    private var myMergeRefBlocks = PluginParameter.Builder("mergeRefBlocks",false,Boolean::class.javaObjectType)
            .description("Merge consecutive GVCF ReferenceBlocks together.  If there is at least 1 bp between two gvcf refBlock records, the records will not be merged")
            .required(false)
            .build()

    private var myQueueSize = PluginParameter.Builder("queueSize",30, Int::class.javaObjectType)
            .description("Size of Queue used to pass information to the DB writing thread.  Increase this number to have better thread utilization at the expense of RAM.  " +
                    "If you are running into Java heap Space/RAM issues and cannot use a bigger machine, decrease this parameter.")
            .required(false)
            .build()

    private var isTestMethod = PluginParameter.Builder("isTestMethod", false, Boolean::class.javaObjectType)
        .description("Indication if the data is to be loaded against a test method. Data loaded with test methods are not cached with the PHG ktor server")
        .required(false).build()

    override fun processData(input: DataSet?): DataSet? {
        //Load the Bed File an the reference sequence.
        val focusRanges = ReadBedfile.getClosedRangesAsPositions(bedFile()).asRanges()

        check(focusRanges.isNotEmpty()) { "Error Loading in Bed file, file is empty.  Please double check: ${bedFile()}." }

        val referenceSequence = GenomeSequenceBuilder.instance(reference())

        try {
            //Setup the DB connection and the DB Writer object.
            var dbConnect = DBLoadingUtils.connection(false)
            val phg = PHGdbAccess(dbConnect)


            //Parse the keyfile and verify that the required headers are present
            val keyFileParsed = parseKeyFile(wGSKeyFile())
            val headerMap = keyFileParsed.first
            val sampleNameIndex = headerMap["sample_name"]?:-1
            val sampleDescriptionIndex = headerMap["sample_description"]?:-1
            val sampleTypeIndex = headerMap["type"]?:-1
            val filesIndex = headerMap["files"]?:-1
            val chrPhasedIndex = headerMap["chrPhased"]?:-1
            val genePhasedIndex = headerMap["genePhased"]?:-1
            val phasingConfidenceIndex = headerMap["phasingConf"]?:-1

            //Verify the needed columns are correct.  sample_description can be empty.  Throw a warning in this case.
            if(sampleNameIndex == -1) {
                throw IllegalStateException("Error loading KeyFile.  No column named 'sample_name' found.")
            }
            if(filesIndex == -1) {
                throw IllegalStateException("Error loading KeyFile.  No column named 'files' found.")
            }
            if(chrPhasedIndex == -1) {
                throw IllegalStateException("Error loading KeyFile.  No column named 'chrPhased' found. true or false values are expected here.")
            }
            if(genePhasedIndex == -1) {
                throw IllegalStateException("Error loading KeyFile.  No column named 'genePhased' found. true or false values are expected here.")
            }
            if(phasingConfidenceIndex == -1) {
                throw IllegalStateException("Error loading KeyFile.  No column named 'phasingConf' found. Expected values are between 0.0 and 1.0.")
            }
            if(sampleTypeIndex == -1) {
                throw IllegalStateException("Error loading KeyFile.  No column named 'type' found.")
            }
            if(sampleDescriptionIndex == -1) {
                myLogger.warn("No column named 'sample_description' found in keyfile. Will not be writing a description to the DB.")
            }


            val entries = keyFileParsed.second

            //Get the RangeMap<Position,RefRangeId>
            //This map will allow us to easily figure out what reference range we are in based on the position we are looking at.
            //This is actually mostly so we can associate a RefRange ID for the DB uploading based on the start position of the BED file.
            //Because we do not have a graph at the moment, we have to use direct SQL calls from the phg object.
            val allIntervalRanges = phg.getIntervalRangesWithIDForChrom("all")

            val chrToIntervalMapping = mutableMapOf<String,RangeMap<Position,Int>>()
            allIntervalRanges.asMapOfRanges()
                    .keys
                    .forEach {
                        val chr = it.lowerEndpoint().chromosome.name
                        if(!chrToIntervalMapping.containsKey(chr)) {
                            chrToIntervalMapping[chr] = TreeRangeMap.create<Position,Int>()
                        }

                        chrToIntervalMapping[chr]?.put(it,allIntervalRanges[it.lowerEndpoint()])
                    }


            phg.close()
            dbConnect = DBLoadingUtils.connection(false)

            //Coroutines
            //create Blocking scope otherwise execution of this will not wait for it to finish
            runBlocking {

                if(queueSize()<0) {
                    throw IllegalArgumentException("Queue size is negative.  Please specify at least 0 for the size of the Multithreading Queue")
                }

                //Setup the channels  The input channel will hold KeyFile rows.  The Result Channel holds ChromosomeGVCFRecords to write.
                //If queueSize is set to 0, we should just set up a default queue.  If its above 0, we can use that as the queue size.
                val inputChannel = Channel<List<String>>(queueSize())
                val resultChannel = Channel<ChromosomeGVCFRecord>(queueSize())
                val numThreads = (numThreads() - 2).coerceAtLeast(1)

                //Setup the channel to pass information to the worker coroutines
                //Basically this just needs to add each keyfile row to the channel so the worker coroutines can start working on them when ready.
                launch {
                    myLogger.info("Adding entries to the inputChannel:")
                    entries.filter { it[sampleTypeIndex] == "GVCF" }.forEach {
                        myLogger.info("Adding: ${it[0]}")
                        inputChannel.send(it)
                    }
                    myLogger.info("Done Adding KeyFile entries to the inputChannel:")
                    inputChannel.close() //Need to close this here to show the workers that it is done adding more data
                }

                //For the number of threads on the machine, set up
                val workerThreads = (1..numThreads).map { threadNum ->
                    launch { processKeyFileEntry(inputChannel,resultChannel, headerMap, referenceSequence, focusRanges, chrToIntervalMapping) }
                }

                launch {
                    processDBUploading(resultChannel,dbConnect,isTestMethod())
                    myLogger.info("Finished writing to the DB.")
                }


                //Create a coroutine to make sure all the async coroutines are done processing, then close the result channel.
                //If this is not here, this will run forever.
                launch {
                    workerThreads.forEach { it.join() }
                    resultChannel.close()
                }
                myLogger.info("Setup Coroutines.")
            }
            //Need to close these here otherwise they will throw an error if closed too early.
            dbConnect.close()
        }
        catch(exc : Exception) {
            throw IllegalStateException("Error writing to the DB: ", exc)
        }


        return null
    }

    /**
     * Parse the key file keeping track of the header positions as well.
     */
    private fun parseKeyFile(keyfile:String) :Pair<Map<String,Int>,List<List<String>>> {

        var headerLineMap = mutableMapOf<String,Int>()
        val fileLines = Utils.getBufferedReader(keyfile).readLines()
                .filter { it!="" } //Get rid of any empty lines in the file.
                .map { it.split("\t") }

        val headerLine = fileLines.first()
        for(i in 0 until headerLine.size) {
            headerLineMap[headerLine[i]] = i
        }

        return Pair(headerLineMap, fileLines.drop(1)) //list.drop(1) removes the first entry in the list.  In this case its the header line.
    }


    /**
     * Method process each keyFile entry in a multithreaded manner.  By setting this function to use Dispatcher.Default, it parallelizes automatically.
     * This will go through each file for the taxon and extract out sequence and VariantContexts for each reference range.
     * It will then collect the information into a ChromsomeGVFRecord and add it to a list.
     * Then once the file is done processing, all the chromosomes are passed to the result channel and passed to the DB thread.
     */
    private suspend fun processKeyFileEntry(inputChannel: Channel<List<String>>, resultChannel: Channel<ChromosomeGVCFRecord>, headerMap : Map<String,Int>, referenceSequence : GenomeSequence, focusRanges : Set<Range<Position>>, allIntervalRegions : Map<String,RangeMap<Position,Int>>) = withContext(Dispatchers.Default) {
        for(channelEntry in inputChannel) {
            val sampleNameIndex = headerMap["sample_name"]?:-1
            val sampleDescriptionIndex = headerMap["sample_description"]?:-1
            val filesIndex = headerMap["files"]?:-1
            val chrPhasedIndex = headerMap["chrPhased"]?:-1
            val genePhasedIndex = headerMap["genePhased"]?:-1
            val phasingConfidenceIndex = headerMap["phasingConf"]?:-1

            val line = channelEntry[sampleNameIndex]
            val lineData = if(sampleDescriptionIndex == -1) "" else channelEntry[sampleDescriptionIndex] //Check to see if user specified.
            val genePhased = channelEntry[genePhasedIndex].toBoolean()
            val chrPhased = channelEntry[chrPhasedIndex].toBoolean()
            val phasingConfidence = channelEntry[phasingConfidenceIndex].toFloat()
            val files =  channelEntry[filesIndex].split(",")
            val totalPloidy = files.size

            var totalTimeCreatingSeq = 0L
            var totalTimeCreatingObject = 0L

            //Loop through each file in the keyFile line.
            (0 until files.size).forEach { gvcfFileIndex ->
                val chrNameToChrObject = focusRanges.map { it.lowerEndpoint().chromosome }.distinct().map { Pair(it.name,it) }.toMap()
                val currentGVCFFile = gVCFDirectory() + files[gvcfFileIndex]
                myLogger.info("Attempting to create GVCF Sequence for ${currentGVCFFile}")
                var anchorSequences = HashMap<Int, AnchorDataPHG>()

                var chromAnchorStartEnd: RangeMap<Position, Int>? = null


                val ghd = GenoHaploData(totalPloidy,
                        false,//This is not the ref
                        line,
                        lineData,
                        genePhased, //genesPhased
                        chrPhased, //chrPhased
                        gvcfFileIndex,
                        phasingConfidence)//confidence


                var currentChromosome = ""
                var prevChromosome = ""

                myLogger.info("Done setting up variables for file ${currentGVCFFile}. Moving on to processing reference ranges.")

                val gvcfFileReader = VCFFileReader(File(currentGVCFFile), false)
                val gvcfFileIterator = gvcfFileReader.iterator()

                //Grab the first gvcf record
                var currentGVCFRecord:VariantContext? = gvcfFileIterator.next()

                // Need to make sure that the gvcf chromosome is not behind the first bed file chromosome
                // If we do not do this, it will not write anything to the DB as it will be out of order.
                //In kotlin < on objects will resolve a .compareTo() Call
                while(currentGVCFRecord != null && (Chromosome.instance(currentGVCFRecord.contig) < focusRanges.first().lowerEndpoint().chromosome) ) {
                    currentGVCFRecord = gvcfFileIterator.next()
                }


                var startTime = System.nanoTime()
                for (currentRange in focusRanges) {

                    check(!(currentRange.lowerBoundType()==BoundType.OPEN || currentRange.upperBoundType()==BoundType.OPEN)) { "Error reading BED file.  Currently have either an Open started or a Closed ended range." }

                    val variantTempList = mutableListOf<HaplotypeNode.VariantInfo>()
                    var consecVariantList = mutableListOf<HaplotypeNode.VariantInfo>()
                    val currentBedRegionChr = currentRange.lowerEndpoint().chromosome


                    val currentRefRangeStartPos = currentRange.lowerEndpoint().position
                    // Grab the last physical position.
                    val currentRefRangeEndPos = currentRange.upperEndpoint().position

                    if(currentGVCFRecord != null && (Chromosome.instance(currentGVCFRecord.contig) > currentRange.lowerEndpoint().chromosome)) {
                        continue
                    }

                    if (currentRange.lowerEndpoint().chromosome == null || currentRange.lowerEndpoint().chromosome.name != currentChromosome) {
                        chromAnchorStartEnd = allIntervalRegions[currentRange.lowerEndpoint().chromosome.name]
                        prevChromosome = currentChromosome
                        currentChromosome = chromAnchorStartEnd?.asMapOfRanges()
                                ?.keys
                                ?.first()
                                ?.lowerEndpoint()
                                ?.chromosome
                                ?.name ?: ""

                        if (anchorSequences.size > 0) {
                            // write to the DB here
                            myLogger.info("\nStaging ${line} chrom ${prevChromosome} for DB uploading.")
                            myLogger.info("Time spent creating Sequences for  Chr:${currentChromosome} for Line: ${line} : ${(totalTimeCreatingSeq)/1E9}sec")
                            myLogger.info("Time spent creating GVCFSequence for  Chr:${currentChromosome} for Line: ${line} : ${(totalTimeCreatingObject)/1E9}sec")
                            totalTimeCreatingSeq = 0
                            totalTimeCreatingObject = 0

                            myLogger.info("Time spent creating Chr:${currentChromosome} for Line: ${line} : ${(System.nanoTime() - startTime)/1E9}sec")
                            startTime = System.nanoTime()
                            resultChannel.send(ChromosomeGVCFRecord(prevChromosome, line, gvcfFileIndex, anchorSequences, ghd))
                            anchorSequences = HashMap<Int, AnchorDataPHG>() // these have been processed, clear the map, start fresh with next chrom
                        }
                    }

                    //Check to see if current range is fully behind GVCF range.  Chromosomes must be the same.  If chromosomes are different
                    // |---B---|
                    //            |--G----|
                    if(currentGVCFRecord != null && currentBedRegionChr == chrNameToChrObject?.get(Chromosome.instance(currentGVCFRecord.contig).name) && currentRefRangeEndPos < currentGVCFRecord.start ) {
//                        myLogger.info("Missing Node Here! Taxon:${line} bedRegion${currentBedRegionChr.name}:${currentRefRangeStartPos}-${currentRefRangeEndPos} " +
//                                "gvcfRegion:${currentGVCFRecord.contig}:${currentGVCFRecord.start}-${currentGVCFRecord.end}")
                        continue
                    }

                    //check to see if the GVCF should be added to the temp list
                    if(currentGVCFRecord != null && currentBedRegionChr == chrNameToChrObject?.get(Chromosome.instance(currentGVCFRecord.contig).name)
                            && checkRegionsOverlap(currentRefRangeStartPos,currentRefRangeEndPos,currentGVCFRecord.start, currentGVCFRecord.end)) {
                        val newGVCFInfo = convertGVCFContextToInfo(currentGVCFRecord)

                        //Check to see if newGVCFInfo cannot be added to consecVariantList
                        //If non-consecutive or variant, this will be false
                        if(!canVariantInfoBeAddedToRefBlockList(consecVariantList,newGVCFInfo)) {
                            //Make sure we have infos to merge.
                            // If we have consecutive SNPs this would be a problem without this check.
                            if(consecVariantList.size != 0) {
                                //If the user wants to merge, merge, if not add all refBlocks to variantTempList
                                if(mergeRefBlocks()) {
                                    val mergedInfo = mergeRefBlocks(consecVariantList)
                                    variantTempList.add(mergedInfo)
                                }
                                else {
                                    variantTempList.addAll(consecVariantList)
                                }
                            }

                            consecVariantList = mutableListOf()

                            //If the newGVCFInfo is a variant, we just add it directly to the variantTempList
                            if(newGVCFInfo.isVariant || newGVCFInfo.isIndel) {
                                variantTempList.add(newGVCFInfo)
                            }
                            else {
                                consecVariantList.add(newGVCFInfo)
                            }

                        }
                        else {
                            //We can safely add it here.
                            consecVariantList.add(newGVCFInfo)
                        }

                    }

                    //Loop through the GVCF file until we catch up with the current Bed File range.
                    while(currentGVCFRecord != null && (Chromosome.instance(currentGVCFRecord.contig) < currentRange.lowerEndpoint().chromosome) ) {
                        currentGVCFRecord = gvcfFileIterator.next()
                    }

                    //Check to see if the previous GVCF record fully covers the Bed range
                    //      |---B---|
                    // |---------G-------|
                    if(currentGVCFRecord != null && currentBedRegionChr == chrNameToChrObject?.get(Chromosome.instance(currentGVCFRecord.contig).name)
                            && currentRefRangeStartPos >= currentGVCFRecord.start && currentRefRangeEndPos >= currentGVCFRecord.start
                            && currentRefRangeStartPos <= currentGVCFRecord.end && currentRefRangeEndPos <= currentGVCFRecord.end) {

                        val newGVCFInfo = convertGVCFContextToInfo(currentGVCFRecord)

                        //Check to see if newGVCFInfo cannot be added to consecVariantList
                        //If non-consecutive or variant, this will be false
                        if(!canVariantInfoBeAddedToRefBlockList(consecVariantList,newGVCFInfo)) {
                            //Make sure we have infos to merge.
                            // If we have consecutive SNPs this would be a problem without this check.
                            if(consecVariantList.size != 0) {
                                val mergedInfo = mergeRefBlocks(consecVariantList)
                                variantTempList.add(mergedInfo)
                            }

                            consecVariantList = mutableListOf()

                            //regardless if newGVCFInfo is ref or variant, add to the list for export.
                            variantTempList.add(newGVCFInfo)
                        }
                        else {
                            consecVariantList.add(newGVCFInfo)
                            if(mergeRefBlocks()) {
                                //Merging here as we are about to write to the DB.
                                val mergedInfo = mergeRefBlocks(consecVariantList)
                                variantTempList.add(mergedInfo)
                            }
                            else {
                                variantTempList.addAll(consecVariantList)
                            }
                        }

                        val gsStartTime = System.nanoTime()
                        var gs =GVCFSequence.instance(referenceSequence, variantTempList, false)
                        totalTimeCreatingObject += (System.nanoTime()-gsStartTime)
                        //Extract out the fasta sequence:
                        val sequence = gs?.genotypeAsString(currentRange.lowerEndpoint().chromosome,
                                currentRefRangeStartPos,
                                currentRefRangeEndPos)

                        //nulling out to allow GC to release
                        gs = null

                        totalTimeCreatingSeq += (System.nanoTime()-gsStartTime)
                        //Add the sequences and the encoded GVCF to the datastructure
                        val mapEntry = chromAnchorStartEnd!!.getEntry(currentRange.lowerEndpoint())
                        val anchorId = mapEntry!!.value
                        // asmFileId is only for assemblies -send -1
                        val aData = AnchorDataPHG(currentRange,"0", 0,0, ".",currentGVCFFile, null, variantTempList, sequence,-1)

                        anchorSequences[anchorId] = aData

                        continue
                    }

                    //If not, we should loop through the GVCF records until we either leave the BED region
                    while(gvcfFileIterator.hasNext()) {
                        //check to see if current VCF record is past range
                        currentGVCFRecord = gvcfFileIterator.next()
                        val currentVCFChr = chrNameToChrObject[Chromosome.instance(currentGVCFRecord?.contig).name]
                        val currentVCFStart = currentGVCFRecord?.start?:0
                        val currentVCFEnd = currentGVCFRecord?.end?:0

                        //Need to break out once the VCF has moved past the bed region
                        // |---B---|
                        //             |---G----|
                        if(currentVCFChr == null || currentVCFChr != currentBedRegionChr ||currentVCFStart > currentRefRangeEndPos) {
                            break
                        }

                        //If the GVCF End is still before the Bed region, it means that we need to just move to the next GVCF record
                        //                  |-----B----|
                        // |---G---|
                        if(currentVCFEnd < currentRefRangeStartPos) {
                            continue
                        }

                        //If it has passed, either GVCF overlaps BED region ends, GVCF fully covers BED file or GVCF is fully covered by the BED
                        if(currentGVCFRecord != null) {
                            val newGVCFInfo = convertGVCFContextToInfo(currentGVCFRecord)

                            //Check to see if newGVCFInfo cannot be added to consecVariantList
                            //If non-consecutive or variant, this will be false
                            if(!canVariantInfoBeAddedToRefBlockList(consecVariantList,newGVCFInfo)) {
                                if(consecVariantList.size != 0) {
                                    if(mergeRefBlocks()) {
                                        val mergedInfo = mergeRefBlocks(consecVariantList)
                                        variantTempList.add(mergedInfo)
                                    }
                                    else {
                                        variantTempList.addAll(consecVariantList)
                                    }
                                }
                                consecVariantList = mutableListOf()

                                if(newGVCFInfo.isVariant) {
                                    variantTempList.add(newGVCFInfo)
                                }
                                else {
                                    consecVariantList.add(newGVCFInfo)
                                }

                            }
                            else {
                                consecVariantList.add(newGVCFInfo)
                            }
                        }

                        //This is to fix if there is a GVCF record which spans multiple BedRegions
                        // |---B---|    |---B---|
                        //     |------------G------------|
                        //If we do not check this, we will move on to the next gvcf file incorrectly and have a chance to skip the second Bed region
                        if(currentRefRangeEndPos < currentVCFEnd) {
                            break
                        }
                    }

                    if(consecVariantList.size != 0 ) {
                        if(mergeRefBlocks()) {
                            variantTempList.add(mergeRefBlocks(consecVariantList))
                        }
                        else {
                            variantTempList.addAll(consecVariantList)
                        }
                    }

                    val gsStartTime = System.nanoTime()
                    var gs =GVCFSequence.instance(referenceSequence, variantTempList, false)
                    totalTimeCreatingObject += (System.nanoTime()-gsStartTime)
                    //Extract out the fasta sequence:
                    val sequence = gs?.genotypeAsString(currentRange.lowerEndpoint().chromosome,
                            currentRefRangeStartPos,
                            currentRefRangeEndPos)
                    //nulling out to allow GC to release
                    gs = null
                    totalTimeCreatingSeq += (System.nanoTime()-gsStartTime)
                    //Add the sequences and the encoded GVCF to the datastructure
                    val mapEntry = chromAnchorStartEnd!!.getEntry(currentRange.lowerEndpoint())
                    val anchorId = mapEntry!!.value
                    val aData = AnchorDataPHG(currentRange, "0",0,0, ".",currentGVCFFile, null, variantTempList, sequence, -1)

                    anchorSequences[anchorId] = aData

                }

                if (anchorSequences.size > 0) {
                    // load the data on per-chrom basis
                    myLogger.info("\nStaging ${line} chrom $currentChromosome for DB Uploading")
                    myLogger.info("Time spent creating Chr:${currentChromosome} for Line: ${line} : ${(System.nanoTime() - startTime)/1E9}sec")

                    resultChannel.send(ChromosomeGVCFRecord(currentChromosome,line,gvcfFileIndex,anchorSequences,ghd))

                    anchorSequences = HashMap<Int, AnchorDataPHG>()
                }
                //Send the results to the channel so the DB writer can process.
                myLogger.info("\nDone Processing ${line} Adding to the queue for DB Uploading.")
            }
        }
    }

    /**
     * Simple function to test to see if the bed region and the gvcf region are overlapping at all.
     */
    private fun checkRegionsOverlap(bedStart : Int, bedEnd : Int, gvcfStart : Int, gvcfEnd:Int) : Boolean {
        return (bedStart <= gvcfStart && gvcfStart <= bedEnd) ||
                (bedStart <= gvcfEnd && gvcfEnd <= bedEnd)
    }

    /**
     * Function to process uploading the Chromosome GVCF Records to the DB.
     * This is only running on one thread to prevent deadlock/thread inconsistencies.
     */
    private suspend fun processDBUploading(resultChannel: Channel<ChromosomeGVCFRecord>, dbConnect: Connection, isTestMethod:Boolean) = withContext(Dispatchers.Default){

        var lineToGameteGrpId  = HashMap<String,Int>()
        val phg = PHGdbAccess(dbConnect)
        for(chrGVCFRecord in resultChannel) {
            Sizeof.printMemoryUse()

                val ghd = chrGVCFRecord.ghd
                val line = chrGVCFRecord.currentTaxon
                val gvcfFileIndex = chrGVCFRecord.gameteId
                val anchorSequences = chrGVCFRecord.anchorDataMapping
                val currentChromosome = chrGVCFRecord.currentChrom

                myLogger.info("DBProcessing Line: ${line} Chr: ${currentChromosome}")

                try {
                    phg.putGenoAndHaploTypeData(ghd)

                    val nameWithHap = line + "_" + gvcfFileIndex //Create the name for the gamete.  Using the ordering provided by the user's keyfile.
                    val gameteGroupList = ArrayList<String>()
                    myLogger.info("Figuring out gamete Group Id for ${line} chr:${currentChromosome}")
                    gameteGroupList.add(nameWithHap)
                    phg.putGameteGroupAndHaplotypes(gameteGroupList)

                    val methodType = if (isTestMethod) DBLoadingUtils.MethodType.TEST_ANCHOR_HAPLOTYPES else DBLoadingUtils.MethodType.ANCHOR_HAPLOTYPES

                    val method_id = phg.putMethod(method(), methodType, pluginParameters())

                    val gamete_grp_id = phg.getGameteGroupIDFromTaxaList(gameteGroupList)

                    myLogger.info("Writing ${line} chr:${currentChromosome} to the DB.")
                    phg.putHaplotypesData(gamete_grp_id, method_id, anchorSequences, currentChromosome,maxDBUploadQueue())

                    // This will be overwritten as multiple chromosome from the same line are processed.
                    // This is expected.  The intent is to get the full list of taxon represented in
                    // the key file.
                    lineToGameteGrpId.put(line,gamete_grp_id)

                } catch (exc: Exception) {
                    myLogger.info("Found Exception")
                    myLogger.info("${line} ${gvcfFileIndex} Chr: $currentChromosome")
                    myLogger.info(exc)
                    phg.close()
                    throw exc
                }
                myLogger.info("Done DBProcessing Line: ${line} Chr: ${currentChromosome}")
//                phg.close()
        }
        myLogger.info("loadHaplotypesFromGVCFPlugin: Finished processing resultChannel input, moving to path creation")
        // Add paths to the DB for each taxon
        for (entry in lineToGameteGrpId) {
            val line = entry.key
            val gamete_grp_id = entry.value
            myLogger.info("creating Paths for taxon $line")
            val hapidList = DBLoadingUtils.createPathNodesForGameteGrp(line, dbConnect, gamete_grp_id)
            if (hapidList.size > 0) {
                val pathBytes = DBLoadingUtils.encodePathsFromIntArray(hapidList)
                val pathMethod = method() + "_PATH"
                val methodParams: MutableMap<String,String> = mutableMapOf()
                methodParams.put("notes","path created when WGS was loaded from GVCF")

                val pathid = phg.putPathsData(pathMethod,  methodParams, line, null,  pathBytes, isTestMethod())
                myLogger.info("Paths added to db for $line, pathid=$pathid")
            } else {
                myLogger.warn("No path data found for haplotypes from GVCF for line: $line" )
            }
        }
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = LoadHaplotypesFromGVCFPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "LoadHaplotypesFromGVCF"
    }

    override fun getToolTipText(): String {
        return "Plugin upload a GVCF file to the DB."
    }

    /**
     * Keyfile used to specify the inputs to the DB.  Must
     * have columns sample_name, files, type, chrPhased, genePhased,
     * and phasingConf. Optionally you can have sample_description.
     * Ordering of the columns does not matter.
     * sample_name is the name of the taxon you are uploading.
     * files is a comma-separated list of file names(without
     * path) providing the names of the GVCF files to be uploaded.
     *  If there are multiple files, gamete_ids will be assigned
     * in order.
     * type is the type of the files.  To use this for this
     * Plugin GVCF needs to be specified.chrPhased and genePhased
     * must be either 'true' or 'false' and represent if the
     * chromosome or the genes are phased or not
     * phasingConf must be a number between 0.0 and 1.0 representing
     * the confidence in the phasing.
     * sample_description to provide a short description of
     * the sample name to be uploaded.  If this column is
     * not specified an empty description will be used.
     *
     * @return Wgs Key File
     */
    fun wGSKeyFile(): String {
        return myWGSKeyFile.value()
    }

    /**
     * Set Wgs Key File. Keyfile used to specify the inputs
     * to the DB.  Must have columns sample_name, files, type,
     * chrPhased, genePhased, and phasingConf. Optionally
     * you can have sample_description. Ordering of the columns
     * does not matter.
     * sample_name is the name of the taxon you are uploading.
     * files is a comma-separated list of file names(without
     * path) providing the names of the GVCF files to be uploaded.
     *  If there are multiple files, gamete_ids will be assigned
     * in order.
     * type is the type of the files.  To use this for this
     * Plugin GVCF needs to be specified.chrPhased and genePhased
     * must be either 'true' or 'false' and represent if the
     * chromosome or the genes are phased or not
     * phasingConf must be a number between 0.0 and 1.0 representing
     * the confidence in the phasing.
     * sample_description to provide a short description of
     * the sample name to be uploaded.  If this column is
     * not specified an empty description will be used.
     *
     * @param value Wgs Key File
     *
     * @return this plugin
     */
    fun wGSKeyFile(value: String): LoadHaplotypesFromGVCFPlugin {
        myWGSKeyFile = PluginParameter<String>(myWGSKeyFile, value)
        return this
    }

    /**
     * Directory holding all the gvcf files
     *
     * @return Gvcf Dir
     */
    fun gVCFDirectory(): String {
        return myGVCFDirectory.value()
    }

    /**
     * Set Gvcf Dir. Directory holding all the gvcf files
     *
     * @param value Gvcf Dir
     *
     * @return this plugin
     */
    fun gVCFDirectory(value: String): LoadHaplotypesFromGVCFPlugin {
        myGVCFDirectory = PluginParameter(myGVCFDirectory, value)
        return this
    }
    /**
     * Input Reference Fasta
     *
     * @return Ref
     */
    fun reference(): String {
        return myReference.value()
    }

    /**
     * Set Ref. Input Reference Fasta
     *
     * @param value Ref
     *
     * @return this plugin
     */
    fun reference(value: String): LoadHaplotypesFromGVCFPlugin {
        myReference = PluginParameter(myReference, value)
        return this
    }

    /**
     * File holding the Reference Range Information
     *
     * @return Bed File
     */
    fun bedFile(): String {
        return bedFile.value()
    }

    /**
     * Set Bed File. File holding the Reference Range Information
     *
     * @param value Bed File
     *
     * @return this plugin
     */
    fun bedFile(value: String): LoadHaplotypesFromGVCFPlugin {
        bedFile = PluginParameter(bedFile, value)
        return this
    }

    /**
     * Method name for the haplotypes being uploaded.
     *
     * @return Method
     */
    fun method(): String {
        return myMethod.value()
    }

    /**
     * Set Method. Method name for the haplotypes being uploaded.
     *
     * @param value Method
     *
     * @return this plugin
     */
    fun method(value: String): LoadHaplotypesFromGVCFPlugin {
        myMethod = PluginParameter(myMethod, value)
        return this
    }

    /**
     * Method description to be written if the method is new.
     *
     * @return Method Description
     */
    fun methodDescription(): String {
        return myMethodDescription.value()
    }

    /**
     * Set Method Description. Method description to be written
     * if the method is new.
     *
     * @param value Method Description
     *
     * @return this plugin
     */
    fun methodDescription(value: String): LoadHaplotypesFromGVCFPlugin {
        myMethodDescription = PluginParameter(myMethodDescription, value)
        return this
    }

    /**
     * Number of threads used to upload
     *
     * @return Num Threads
     */
    fun numThreads() : Int {
        return myNumThreads.value()
    }

    /**
     * Set Num Threads. Number of threads used to upload
     *
     * @param value Num Threads
     *
     * @return this plugin
     */
    fun numThreads(value : Int) : LoadHaplotypesFromGVCFPlugin {
        myNumThreads = PluginParameter(myNumThreads, value)
        return this
    }

    /**
     * Number of Haplotype instances to stage per chromosome
     * before a DB write is triggered.  Lower this number
     * if you are running into RAM issues.  It will take longer
     * to process, but should help balance the load.
     *
     * @return Max Num Haps Staged
     */
    fun maxDBUploadQueue(): Int {
        return myMaxDBUploadQueue.value()
    }

    /**
     * Set Max Num Haps Staged. Number of Haplotype instances
     * to stage per chromosome before a DB write is triggered.
     *  Lower this number if you are running into RAM issues.
     *  It will take longer to process, but should help balance
     * the load.
     *
     * @param value Max Num Haps Staged
     *
     * @return this plugin
     */
    fun maxDBUploadQueue(value: Int): LoadHaplotypesFromGVCFPlugin {
        myMaxDBUploadQueue = PluginParameter<Int>(myMaxDBUploadQueue, value)
        return this
    }

    /**
     * Merge consecutive GVCF ReferenceBlocks together.  If
     * there is at least 1 bp between two gvcf refBlock records,
     * the records will not be merged
     *
     * @return Merge Ref Blocks
     */
    fun mergeRefBlocks(): Boolean {
        return myMergeRefBlocks.value()
    }

    /**
     * Set Merge Ref Blocks. Merge consecutive GVCF ReferenceBlocks
     * together.  If there is at least 1 bp between two gvcf
     * refBlock records, the records will not be merged
     *
     * @param value Merge Ref Blocks
     *
     * @return this plugin
     */
    fun mergeRefBlocks(value: Boolean): LoadHaplotypesFromGVCFPlugin {
        myMergeRefBlocks = PluginParameter<Boolean>(myMergeRefBlocks, value)
        return this
    }

    /**
     * Size of Queue to pass information to the DB writing
     * thread.  Increase this number to have better thread
     * utilization at the expense of RAM.  If you are running
     * into Java heap Space/RAM issues and cannot use a bigger
     * machine, decrease this parameter.
     *
     * @return Queue Size
     */
    fun queueSize(): Int {
        return myQueueSize.value()
    }

    /**
     * Set Queue Size. Size of Queue to pass information to
     * the DB writing thread.  Increase this number to have
     * better thread utilization at the expense of RAM.  If
     * you are running into Java heap Space/RAM issues and
     * cannot use a bigger machine, decrease this parameter.
     *
     * @param value Queue Size
     *
     * @return this plugin
     */
    fun queueSize(value: Int): LoadHaplotypesFromGVCFPlugin {
        myQueueSize = PluginParameter<Int>(myQueueSize, value)
        return this
    }

    /**
     * Indication if the data is to be loaded against a test
     * method. Data loaded with test methods are not cached
     * with the PHG ktor server
     *
     * @return Is Test Method
     */
    fun isTestMethod(): Boolean {
        return isTestMethod.value()
    }

    /**
     * Set Is Test Method. Indication if the data is to be
     * loaded against a test method. Data loaded with test
     * methods are not cached with the PHG ktor server
     *
     * @param value Is Test Method
     *
     * @return this plugin
     */
    fun isTestMethod(value: Boolean): LoadHaplotypesFromGVCFPlugin {
        isTestMethod = PluginParameter<Boolean>(isTestMethod, value)
        return this
    }


}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(LoadHaplotypesFromGVCFPlugin::class.java)
}