package net.maizegenetics.pangenome.db_loading

import com.google.common.collect.*
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.dna.map.GenomeSequenceBuilder
import net.maizegenetics.dna.map.Position
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Tuple
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import java.util.*
import javax.swing.ImageIcon
import kotlin.collections.HashSet

/**
 * This class takes an intervals file, a genome fastas and creates a valid PHG intervals
 * file that can be loaded via the LoadAllIntervalsTOPHGdbPlugin.
 *
 * User may specify if they want overlapping ranges merged.  If they don't and overlapping
 * ranges are found, an error is returned.
 *
 * If no overlapping ranges are found, or the user sets "mergeOverlaps" to true, then the
 * software creates inter-region ranges.  These are given the name "UNNAMED".
 *
 * The input intervals file is expected to be in a tab-delimited file where the first column
 * is the chrom (String), the second column is the start position (0-based, inclusive) the 3rd
 * column is the interval end position (0-based/exclusive), and the 4th column holds the group
 * name to which the range belongs.  The 4th column is optional, but if it exists, it must
 * be blank or hold the group name for the interval.  If users want to add their own notes
 * for each interval, e.g. which gene an interval comprises, that data must be in column
 * 5.  Columns past 4 will be ignored.
 *
 *
 * Output:
 *  - an intervals file that contains merged overlaps (if requested) and inter-region intervals (if requested).
 *    It will contain columns for Chrom, StartPosition, EndPosition and Type, where type is either RefRegion or RefInterRegion
 *    The "type" field will be used in the LoadAllIntervalsoPHGdbPlugin to determine the group to which each
 *    interval belongs. (this goes into the ref_range_ref_range_method table)
 *
 *  - If the user requests no merging of overlaps, but overlaps are found, an Exception will be thrown and
 *    the user will be instructed to fix the file, or run with "mergeOverlaps" set to true.
 */
class CreateValidIntervalsFilePlugin (parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(CreateValidIntervalsFilePlugin::class.java)

    private var referenceFasta = PluginParameter.Builder("referenceFasta", null, String::class.java).guiName("Reference Genome File").required(true).inFile()
            .description("Reference Genome File in fasta format for aligning against ").build()
    private var intervalsFile = PluginParameter.Builder("intervalsFile", null, String::class.java).guiName("Intervals File").required(true).inFile()
            .description("tab-delimited file containing chrom, interval start position, interval end position, type.  ").build()
    private var generatedFile = PluginParameter.Builder("generatedFile", null, String::class.java).guiName("Output File").outFile().required(true)
            .description("Full Path to file to write created intervals file  ").build()
    private var mergeOverlaps = PluginParameter.Builder("mergeOverlaps", false, Boolean::class.javaObjectType).guiName("Merge Overlapping Ranges").required(false)
            .description("Whether to merge any overlapping ranges found in the bed file.  Default is no.  If overlaps are found and mergeOverLaps is no, processing will be stopped and error printed. ").build()
    private var userRegionsGroupName = PluginParameter.Builder("userRegionsGroupName", "FocusRegion", String::class.java).guiName("User Regions Group Name").required(false)
            .description("Name to give group of reference intervals provided in the user intervals file.  This is used if there is no name column in the provided file.  ").build()
    private var otherRegionsGroupName = PluginParameter.Builder("otherRegionsGroupName", "FocusComplement", String::class.java).guiName("Generated Regions Group Name").required(false)
            .description("Name to give group of reference intervals NOT included in the user intervals file  ").build()

    override fun processData(input: DataSet?): DataSet? {

        // Verify anchor file - this also merges overlapping intervals
        myLogger.info("verifying the interval ranges")
        val rangesAndOverlaps = verifyAndMergeIntervalRanges(intervalsFile())
        val overlaps = rangesAndOverlaps.getY()
        if (overlaps != null) {
            if (!mergeOverlaps() && overlaps.size > 0) {
                // User doesn't want overlaps merged, PHG DB needs distinct ranges.  Throw error
                overlaps.forEach { myLogger.error("LoadAllIntervals: anchorOverlap entry: ${it}") }
                throw IllegalArgumentException("CreateValidIntervalsFilePlugin: intervals file has overlapping positions. Please consolidate/remove overlaps or re-run with mergeOverlaps set to true")
            }
        }

        // rangesAndOverlaps from verifyAndMErgIntervalRanges returns Tuple<RangeMap, Set<String>>
        var userRanges = rangesAndOverlaps.getX()

        val refInterRegionRanges: RangeSet<Position> = TreeRangeSet.create()

        // create the ref Genome - we need this to split the chromosomes
        val myRefSequence = GenomeSequenceBuilder.instance(referenceFasta())
        // We have a good set of intervals now.  Create inter-region intervals if they don't exist.
        // File returned to the user will represent the entire genome.  User can edit out
        // these additional regions if they don't want them.
        myLogger.info("Creating the interRegion unnamed Ranges")
        // This must be on a per-chrom basis to get the infinity fixed
        var chromInterRegionRanges = createInterRegionRanges(userRanges, myRefSequence)
        if (!chromInterRegionRanges.isEmpty()) {
            refInterRegionRanges.addAll(chromInterRegionRanges)
        }

        // we have refRegionRanges and refInterRegionRanges.  Merge the two, should be no overlaps
        // Using  Map vs Set as Set coalesces the ranges, giving us 1 big range.  I need them distinct.
        // These should be added in order.
        val allRanges: RangeMap<Position,String> = TreeRangeMap.create()
        allRanges.putAll(userRanges)

        val groupName = otherRegionsGroupName()
        refInterRegionRanges.asRanges().forEach { range -> allRanges.put(range, groupName)}


        // Write output File
        myLogger.info("writing the created intervals file to directory ${generatedFile()}")
        writeIntervalsFile(generatedFile(), allRanges)
        myLogger.info("Finished with CreateValidIntervalsFile")
        return null
    }

    /**
     * Write the user file - it should have user defined and non user-included intervals interspersed
     * This is the order for writing to the db.
     */
    private fun writeIntervalsFile(intervalsFile: String, allRangesMap: RangeMap<Position,String>) {

        val groupName = otherRegionsGroupName()
        Utils.getBufferedWriter(intervalsFile).use{output ->

            //Intervals file format: - write header columns
            output.write("chrom\tchromStart\tchromEnd\tname\n")
            allRangesMap.asMapOfRanges().keys.forEach { key ->
                val chrom =  key.lowerEndpoint().chromosome.name

                // We used a closed range, and 0-based.
                val type = allRangesMap.asMapOfRanges().get(key)

                // verifyAndMergeIntervalRanges changed the ref Ranges from inclusive/Exclusive to inclusive/inclusive
                // to handle a coalesce problem with adjacent (but not overlapping) regions.  Add the 1 back here to
                // the endposition of the refRegions
                // Because the inter-regions were created based on the inclusive/inclusive versions, we also have to
                // adjust the start of the inter_region by 1.  (end of region increases by 1, so start of inter-region
                // also starts at 1

                val startPos = if (type.equals(groupName)) key.lowerEndpoint().position +1 else key.lowerEndpoint().position
                val endPos = if (type.equals(groupName)) key.upperEndpoint().position else key.upperEndpoint().position +1
                if (startPos < endPos) {
                    // Issue when creating inter-region intervals. When the reference has adjacent
                    // ranges. the complement code creates a 1 bp range between them, which it should not do.
                    // filter those out here.  NOTE: these are 0-based inclusive/exclusive.  So start pos must
                    // be less than end position.
                    output.write("${chrom}\t${startPos}\t${endPos}\t${type}\n")
                }
            }
        }
    }

    private fun createInterRegionRanges(refRanges: RangeMap<Position,String>, refSequence: GenomeSequence): RangeSet<Position> {
        var interRegions : RangeSet<Position> = TreeRangeSet.create()
        // parse the RangeSet based on chrom:
        val chromosomes = refSequence.chromosomes()
        for (chrom in chromosomes) {
            val size = refSequence.chromosomeSize(chrom)
            val chromRanges: RangeMap<Position,String> = refRanges.subRangeMap(Range.closed(Position.of(chrom, 0), Position.of(chrom, size-1)))

            if (chromRanges.asMapOfRanges().isEmpty()) {
                continue // user didn't specify any ranges for this chrom, so don't create inter-region ranges
            }

            // Copy the ranges into a set:
            val chromRangeSet: RangeSet<Position> = TreeRangeSet.create()
            chromRanges.asMapOfRanges().entries.forEach { range ->
                chromRangeSet.add(range.key)
            }

            val complementRanges = chromRangeSet.complement()
            // Using -1 as the ref-interRegion start gets adjusted by 1 for the bed file
            // due to using inclusive/inclusive when processing the regions due to the coalese problem.
            // That 1 throws off the inter-region start by 1.  So the intervals file, which is 0 based,
            // should get -1+1 for a 0 start for the first range.  When these are added to the PHG DB,
            // they are translated to physical positions (1-based)

            //This call changes the "infinite" on either end to a specific start/end for the inter-regions
            val fixedComplementRanges = complementRanges.subRangeSet(Range.closed(Position.of(chrom, -1), Position.of(chrom, size)))
            interRegions.addAll(fixedComplementRanges)
        }

        return interRegions
    }

    // If the range names differ for overlapping ranges, throw error.
    // And ... must allow for multiple range groups - could be 1, 2 or more.
    fun verifyAndMergeIntervalRanges(anchorFile: String): Tuple<RangeMap<Position,String>, Set<String>> {
        val overlappingPositions: MutableSet<String> = HashSet() // overlaps to be returned
        val intervalRanges: RangeSet<Position> = TreeRangeSet.create()
        val intervalRangeMap: RangeMap<Position, String> = TreeRangeMap.create()
        // Read the anchor file, store to RangeSet.  The RangeSet merges the intervals
        // Return a bed file with merged regions.

        // A closeOpen range results in RangeSet coalescing ranges that are adjacent, but not overlapping,
        // which is undesirable.
        // If the ranges are closed, not closedOpen, and they abut, they don't coalesce
        try {
            Utils.getBufferedReader(anchorFile).use { br ->
                var curLine = br.readLine()

                while (curLine != null) {
                    if (curLine.toUpperCase().contains("CHROMSTART")) {
                        curLine = br.readLine()
                        continue
                    } // skip header line
                    val tokens = curLine.split("\t")
                    if (tokens.size < 3) {
                        throw IllegalArgumentException("bad intervals file, error with line ${curLine} - Need at least 3 columns: chromosome, startPosition, EndPosition in bed format.  A 4th column,Name, is optional and provides a group name for the interval on that line.")
                    }

                    val chrom = Chromosome.instance(tokens[0])
                    val interval = Range.closed(Position.of(chrom, tokens[1].toInt()), Position.of(chrom, tokens[2].toInt()-1))
                    addRange(intervalRangeMap,interval, curLine, overlappingPositions)

                    curLine = br.readLine()
                }
            }
        } catch (exc: Exception) {
            throw IllegalArgumentException("CreateValidIntervalsFilePlugin : error reading intervals file " + exc.message)
        }

        // return the list in bedfile format
        return Tuple(intervalRangeMap, overlappingPositions)
    }

    private fun addRange(geneRange: RangeMap<Position, String>, range: Range<Position>, currentLine: String, overlappingPosition:MutableSet<String>) {
        val overlaps: List<Map.Entry<Range<Position>, String>> = ArrayList<Map.Entry<Range<Position>, String>>(
                geneRange.subRangeMap(range).asMapOfRanges().entries)
        val lineTokens = currentLine.split("\t")
        var methodName = userRegionsGroupName() // start with default
        if (lineTokens.size >= 4) {
            if (!lineTokens.get(3).isNullOrEmpty()) methodName = lineTokens.get(3) // change to user supplied name
        }

        //if overlaps has length, merge ranges together
        if (overlaps.size != 0) {
            val overlappingEntry = geneRange.getEntry(overlaps[0].key.lowerEndpoint())
            val oldMethod = overlappingEntry!!.value.toString()
            if (oldMethod.equals(methodName)) {
                // Update overlappingEntry value with new merged gene value.
                // 2nd put is to ensure new entry is merged with the new value
                geneRange.put(overlappingEntry.key, methodName)
                geneRange.putCoalescing(range, methodName)
                overlappingPosition.add(currentLine)
            } else {
                // Ranges overlap and were defined with different methodIds - this is an error
                val sb = StringBuilder()
                sb.append("CreateValidIntervalsFilePlugin:addRange: error - overlapping entries have different methods. ")
                sb.append("orgin entry: ${overlappingEntry?.key?.lowerEndpoint().toString()}  has method name: ${oldMethod}")
                sb.append("new entry: ${range.lowerEndpoint().toString()} has method: ${methodName}")
                throw IllegalArgumentException(sb.toString())
            }


        } else {
            geneRange.put(range, methodName)
        }
    }

    override fun getToolTipText(): String {
        return("Create valid interval file for loading as reference ranges to the PHG database")
    }

    override fun getIcon(): ImageIcon? {
        return(null)
    }

    override fun getButtonName(): String {
        return("Create Valid Intervals File")
    }
    /**
     * Reference Genome File for aligning against
     *
     * @return Reference Genome File
     */
    fun referenceFasta(): String {
        return referenceFasta.value()
    }

    /**
     * Set Reference Genome File. Reference Genome File for
     * aligning against
     *
     * @param value Reference Genome File
     *
     * @return this plugin
     */
    fun referenceFasta(value: String): CreateValidIntervalsFilePlugin {
        referenceFasta = PluginParameter<String>(referenceFasta, value)
        return this
    }

    /**
     * Tab-delimited file containing chrom, interval start position, interval
     * end position, type
     *
     * @return Intervals File
     */
    fun intervalsFile(): String {
        return intervalsFile.value()
    }

    /**
     * Set Intervals File. Tab-delimited file containing chrom, intervals
     * start position, intervals end position, type
     * end
     *
     * @param value Intervals File
     *
     * @return this plugin
     */
    fun intervalsFile(value: String): CreateValidIntervalsFilePlugin {
        intervalsFile = PluginParameter<String>(intervalsFile, value)
        return this
    }

    /**
     * Full path with file name to write created intervals file
     *
     * @return Output File
     */
    fun generatedFile(): String {
        return generatedFile.value()
    }

    /**
     * Set Output File. Full path plus file name to write created
     * intervals file
     *
     * @param value Output File
     *
     * @return this plugin
     */
    fun generatedFile(value: String): CreateValidIntervalsFilePlugin {
        generatedFile = PluginParameter<String>(generatedFile, value)
        return this
    }

    /**
     * Whether to merge any overlapping ranges found in the
     * bed file.  Default is no.  If overlaps are found and
     * mergeOverLaps is no, processing will be stopped and
     * error printed.
     *
     * @return Merge Overlapping Ranges
     */
    fun mergeOverlaps(): Boolean {
        return mergeOverlaps.value()
    }

    /**
     * Set Merge Overlapping Ranges. Whether to merge any
     * overlapping ranges found in the bed file.  Default
     * is no.  If overlaps are found and mergeOverLaps is
     * no, processing will be stopped and error printed.
     *
     * @param value Merge Overlapping Ranges
     *
     * @return this plugin
     */
    fun mergeOverlaps(value: Boolean): CreateValidIntervalsFilePlugin {
        mergeOverlaps = PluginParameter<Boolean>(mergeOverlaps, value)
        return this
    }

    /**
     * Name to give group of reference intervals provided
     * in the user intervals file.  This is used if there
     * is no name column in the provided file.
     *
     * @return User Regions Group Name
     */
    fun userRegionsGroupName(): String {
        return userRegionsGroupName.value()
    }

    /**
     * Set User Regions Group Name. Name to give group of
     * reference intervals provided in the user intervals
     * file.  This is used if there is no name column in the
     * provided file.
     *
     * @param value User Regions Group Name
     *
     * @return this plugin
     */
    fun userRegionsGroupName(value: String): CreateValidIntervalsFilePlugin {
        userRegionsGroupName = PluginParameter<String>(userRegionsGroupName, value)
        return this
    }


    /**
     * Name to give group of intervals not defined in the
     * user intervals file
     *
     * @return Other Regions Group Name
     */
    fun otherRegionsGroupName(): String {
        return otherRegionsGroupName.value()
    }

    /**
     * Set Other Regions Group Name. Name to give group of
     * intervals not defined in the user intervals file
     *
     * @param value Other Regions Group Name
     *
     * @return this plugin
     */
    fun otherRegionsGroupName(value: String): CreateValidIntervalsFilePlugin {
        otherRegionsGroupName = PluginParameter<String>(otherRegionsGroupName, value)
        return this
    }

}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(CreateValidIntervalsFilePlugin::class.java)
}