package net.maizegenetics.pangenome.db_loading

data class ReadMappingDBRecord(val methodName: String, val methodDetails:String , val readMappingId:Int, val readMappings: ByteArray, val genoName : String,val fileGroupName:String)

