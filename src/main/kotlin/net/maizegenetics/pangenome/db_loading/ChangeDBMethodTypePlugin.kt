package net.maizegenetics.pangenome.db_loading
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import org.apache.log4j.Logger
import java.awt.Frame
import java.sql.Connection
import java.sql.SQLException
import javax.swing.ImageIcon

/**
 * This class allows a user to connect to a database and change the method type for an
 * entry in the PHG database methods table.
 * The original purpose of this plugin is to allow some methods to be marked as "test" methods
 * (based on DBLoadingUtils.METHOD_TYPE values).  Then later, if they are considered good,
 * they can be converted to a more permanent type.
 *
 * When using the PHG KTOR brapi web service, graphs are cached based on method type.
 * We do not cache graphs whose type matches one of the test types.
 *
 * Example: original method type was TEST_PATHS and it now becomes PATHS
 *
 * This plugin requires a db connection dataSet to be passed in.  This can be obtained
 * via the GetDBConnectionPlugin plugin.
 *
 */
class ChangeDBMethodTypePlugin(parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = Logger.getLogger(ChangeDBMethodTypePlugin::class.java)

    private var methodName = PluginParameter.Builder("methodName", null, String::class.java)
        .guiName("Method Name").required(true)
        .description("Name of stored method whose type you would like to change ")
        .build()

    private var methodType = PluginParameter.Builder("methodType", null, Int::class.javaObjectType)
        .guiName("PHG Method Type ID").required(true)
        .description("Integer value of the db method type to be associated with the supplied method name ")
        .build()

    override fun processData(input: DataSet): DataSet? {
        val connection = input.getData(0).data as Connection
            ?: throw IllegalStateException("ChangeDBMethodTypePlugin: no connection supplied!")

        val query = "UPDATE methods set method_type=${methodType()} WHERE name='${methodName()}';"
        myLogger.info("ChangeDBMethodTypePlugin: update statement: $query")

        try {
            connection.createStatement().executeUpdate(query)
        } catch (sql: SQLException) {
            var sqle = sql
            var count = 1
            while (sqle != null) {
                myLogger.error("updateMethodType SQLException $count")
                myLogger.error("Code: " + sqle.errorCode)
                myLogger.error("SqlState: " + sqle.sqlState)
                myLogger.error("Error Message: " + sqle.message)
                sqle = sqle.nextException
                count++
            }
            throw IllegalStateException("ChangeDBMethodTypePlugin: error updating method type: ${sqle.message}" )
        }


        return null
    }
    override fun getIcon(): ImageIcon? {
        val imageURL = ChangeDBMethodTypePlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return("ChangeDBMethodTypePlugin")
    }

    override fun getToolTipText(): String {
        return("For a given method name stored in the db methods table, change the type to that specified by user")
    }
    /**
     * Name of stored method whose type you would like to
     * change
     *
     * @return Method Name
     */
    fun methodName(): String {
        return methodName.value()
    }

    /**
     * Set Method Name. Name of stored method whose type you
     * would like to change
     *
     * @param value Method Name
     *
     * @return this plugin
     */
    fun methodName(value: String): ChangeDBMethodTypePlugin {
        methodName = PluginParameter<String>(methodName, value)
        return this
    }

    /**
     * Integer value of the db method type to be associated
     * with the supplied method name
     *
     * @return PHG Method Type ID
     */
    fun methodType(): Int {
        return methodType.value()
    }

    /**
     * Set PHG Method Type ID. Integer value of the db method
     * type to be associated with the supplied method name
     *
     *
     * @param value PHG Method Type ID
     *
     * @return this plugin
     */
    fun methodType(value: Int): ChangeDBMethodTypePlugin {
        methodType = PluginParameter<Int>(methodType, value)
        return this
    }

}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(ChangeDBMethodTypePlugin::class.java)
}