package net.maizegenetics.pangenome.db_loading

import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import org.apache.log4j.Logger
import java.awt.Frame
import javax.swing.ImageIcon


class AddTaxaToTaxaGroupPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = Logger.getLogger(AddTaxaToTaxaGroupPlugin::class.java)

    private var groupName = PluginParameter.Builder("groupName", null, String::class.java).guiName("Taxa Group Name")
            .required(true)
            .description("Name for this taxa group. ").build()
    private var taxa = PluginParameter.Builder("taxa", null, String::class.java).guiName("Taxa List")
            .required(true)
            .description("Comma separated list of taxa.  Taxa names must match the names exsiting in the PHG database ").build()

    override fun processData(input: DataSet?): DataSet? {

        val dbConnect = DBLoadingUtils.connection( false)
        val phg = PHGdbAccess(dbConnect)

        //verify taxa.  If taxa is not in db, throw an exception with the missing taxa printed
        val existingTaxa = phg.getDbTaxaNames()
        val userTaxaList = taxa().split(",")

        val missingTaxaList = userTaxaList.filter{ !existingTaxa.contains(it) }
        require(missingTaxaList.size == 0){"User taxa list has these taxa which aren't included in the db: ${missingTaxaList.joinToString(",")}"}

        // Check if method exists, add the taxa.  You should add with INSERT or IGNORE
        myLogger.info("User taxa list is verified, adding taxa to group ${groupName()}")
        phg.putTaxaTaxaGroups(groupName(),userTaxaList)

        phg.close()

        return null
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return ("Add Taxa to Taxa Group")
    }

    override fun getToolTipText(): String {
        return ("Add taxa to new or existing group. If group doesn't exit it will be created")
    }

    /**
     * Name for this taxa group.
     *
     * @return Taxa Group Name
     */
    fun groupName(): String {
        return groupName.value()
    }

    /**
     * Set Taxa Group Name. Name for this taxa group.
     *
     * @param value Taxa Group Name
     *
     * @return this plugin
     */
    fun groupName(value: String): AddTaxaToTaxaGroupPlugin {
        groupName = PluginParameter<String>(groupName, value)
        return this
    }

    /**
     * Comma separated list of taxa.  Taxa names must match
     * the names exsiting in the PHG database
     *
     * @return Taxa List
     */
    fun taxa(): String {
        return taxa.value()
    }

    /**
     * Set Taxa List. Comma separated list of taxa.  Taxa
     * names must match the names exsiting in the PHG database
     *
     *
     * @param value Taxa List
     *
     * @return this plugin
     */
    fun taxa(value: String): AddTaxaToTaxaGroupPlugin {
        taxa = PluginParameter<String>(taxa, value)
        return this
    }

}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(AddTaxaToTaxaGroupPlugin::class.java)
}