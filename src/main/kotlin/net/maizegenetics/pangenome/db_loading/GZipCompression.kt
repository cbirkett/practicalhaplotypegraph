@file:JvmName("GZipCompression")

package net.maizegenetics.pangenome.db_loading

import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStreamReader
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

import org.apache.log4j.Logger

/**
 * Examples from https://stackoverflow.com/questions/16351668/compression-and-decompression-of-string-data-in-java
 *          and : https://myadventuresincoding.wordpress.com/2016/01/02/java-simple-gzip-utility-to-compress-and-decompress-a-string/
 * Class used to compress strings for memory efficient storage to db.
 *
 * And readLine() in a While from here:
 *   https://discuss.kotlinlang.org/t/assignment-not-allow-in-while-expression/339/34
 *   
 * @author lcj34
 */

object GZipCompression {
    private val myLogger = Logger.getLogger(GZipCompression::class.java)


    // Compress a string using Java's GZIPOutputStream method
    @JvmStatic
    fun compress(str: String?): ByteArray? {
        if (str == null || str.length == 0) {
            return null
        }
        try {
            ByteArrayOutputStream().use { obj ->
                GZIPOutputStream(obj).use { gzip ->
                    gzip.write(str.toByteArray(charset("UTF-8")))
                    gzip.flush()
                    gzip.close()
                    return obj.toByteArray()
                }
            }
        } catch (exc: Exception) {
            myLogger.debug(exc.message, exc)
            throw IllegalArgumentException("Unable to gzip compress string: $str")
        }

    }

    // Uncompress a string using Java's GzipInputStream
    @JvmStatic
    fun decompress(compressedString: ByteArray?): String {

        if (compressedString == null || compressedString.size == 0) {
            return ""
        }

        val outStr = StringBuilder()

        if (isCompressed(compressedString)) {
            try {
                GZIPInputStream(ByteArrayInputStream(compressedString)).use { gis ->
                    BufferedReader(InputStreamReader(gis, "UTF-8")).use { bufferedReader ->
                        val list = mutableListOf<String>()
                        while(true) {
                            list.add(bufferedReader.readLine() ?: break)
                        }
                        outStr.append(list.joinToString("\n"))
                    }
                }
            } catch (exc: Exception) {
                myLogger.debug(exc.message, exc)
                throw IllegalArgumentException("decompress: cannot uncompress byte string: " + exc.message)
            }

        } else {
	    val stringFromBytes : String = String(compressedString)
            outStr.append(stringFromBytes)
        }

        return outStr.toString()
    }

    fun isCompressed(compressed: ByteArray): Boolean {
        return compressed[0] == GZIPInputStream.GZIP_MAGIC.toByte() && compressed[1] == (GZIPInputStream.GZIP_MAGIC shr 8).toByte()
    }

}
