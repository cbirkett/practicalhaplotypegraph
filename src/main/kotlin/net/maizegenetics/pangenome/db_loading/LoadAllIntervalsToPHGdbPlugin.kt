package net.maizegenetics.pangenome.db_loading

import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import com.google.common.collect.Range
import htsjdk.variant.variantcontext.VariantContext
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.dna.map.GenomeSequenceBuilder
import net.maizegenetics.dna.map.Position
import net.maizegenetics.pangenome.processAssemblyGenomes.AssemblyProcessingUtils
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.File
import java.io.IOException
import java.sql.Connection
import java.util.*
import javax.swing.ImageIcon

import net.maizegenetics.pangenome.api.*


/**
 * This class takes as input a BED formatted intervals file.  Expected BED header columns are "chrom", "chromStart", "chromEnd",
 * and "name" (as per the bed file format requirements.  Any other columns in the bed file are ignored.
 *
 * The class will create and load intervals for the PHG reference_ranges table based on the user provided intervals file.
 * It will also load the haplotypes table and the ref_range_ref_range_method tables, the latter pulling group names
 * from the "name" field of the intervals file.
 */
class LoadAllIntervalsToPHGdbPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = Logger.getLogger(LoadAllIntervalsToPHGdbPlugin::class.java)

    private var refGenome = PluginParameter.Builder("ref", null, String::class.java).guiName("Reference Genome File").required(true).inFile()
            .description("Referemce Genome File for aligning against ").build()
    private var anchors = PluginParameter.Builder("anchors", null, String::class.java).guiName("Anchors File").required(true).inFile()
            .description("Tab-delimited file containing Chrom, StartPosition, EndPosition, Type").build()
    private var genomeData = PluginParameter.Builder("genomeData", null, String::class.java).guiName("Genome Data File").required(true)
            .description("Path to tab-delimited file containing genome speciic data with header line:\nGenotype Hapnumber Dataline Ploidy Reference GenePhased ChromPhased Confidence Method MethodDetails")
            .build()
    private var outputDir = PluginParameter.Builder("outputDir", null, String::class.java).guiName("Output Directory").outDir().required(true)
            .description("Directory to write liquibase changeLogSync output ").build()
    private var refServerPath = PluginParameter.Builder("refServerPath", null, String::class.java).guiName("Reference Server Path").required(true)
            .description("String that contains a server and file path where the reference genome will be stored for future access.  This ia a more permanent location, not where the genome file lives for procssing via this plugin.").build()
    private var isTestMethod = PluginParameter.Builder("isTestMethod", false, Boolean::class.javaObjectType)
        .description("Indication if the data is to be loaded against a test method. Data loaded with test methods are not cached with the PHG ktor server")
        .required(false).build()

    // This data is populated from the genomeData file
    private var line // refName to be stored as line_name in genotypes table, e.g. B73Ref
            : String = ""
    private var line_data: String = ""
    private var ploidy = 0
    private var hapNumber = 0
    private var genesPhased = false
    private var chromsPhased = false
    private var conf = 0f
    private var hapMethod: String = ""
    private var hapMethodDetails: String = ""

    var myRefSequence: GenomeSequence? = null

    override fun postProcessParameters() {
        // parse input file to find arguments
        myLogger.info("postProcessParameters: reading genomeDataFile: " + genomeData())
        val br = Utils.getBufferedReader(genomeData())
        try {
            val headers = br.readLine() // read the header line
            var lineIndex = -1
            var lineDataIndex = -1
            var ploidyIndex = -1
            var hapNumberIndex = -1
            var genesPhasedIndex = -1
            var chromPhasedIndex = -1
            var confIndex = -1
            var methodIndex = -1
            var methodDetailsIndex = -1
            var idx = 0
            myLogger.info("GenomeFile header line: $headers")
            for (header in headers.split("\t".toRegex()).toTypedArray()) {
                if (header.equals("Genotype", ignoreCase = true)) {
                    lineIndex = idx
                } else if (header.equals("Hapnumber", ignoreCase = true)) {
                    hapNumberIndex = idx
                } else if (header.equals("Dataline", ignoreCase = true)) {
                    lineDataIndex = idx
                } else if (header.equals("ploidy", ignoreCase = true)) {
                    ploidyIndex = idx
                } else if (header.equals("genesPhased", ignoreCase = true)) {
                    genesPhasedIndex = idx
                } else if (header.equals("chromsPhased", ignoreCase = true)) {
                    chromPhasedIndex = idx
                } else if (header.equals("confidence", ignoreCase = true)) {
                    confIndex = idx
                } else if (header.equals("Method", ignoreCase = true)) {
                    methodIndex = idx
                } else if (header.equals("MethodDetails", ignoreCase = true)) {
                    methodDetailsIndex = idx
                }
                idx++
            }
            if (lineIndex == -1 || lineDataIndex == -1 || ploidyIndex == -1 || hapNumberIndex == -1 || genesPhasedIndex == -1 || chromPhasedIndex == -1 || confIndex == -1 || methodIndex == -1 || methodDetailsIndex == -1) {
                myLogger.error("LoadAllIntervalsToPHGdbPlugin: ERROR - Genotype datafile does not contain the required 9 fields")
                myLogger.error("Please check your file for the tab delimited, case-insensistive headers: ")
                myLogger.error("  Genotype Hapnumber Dataline Ploidy GenesPhased ChromsPhased Confidence Method MethodDetails")
                throw IllegalArgumentException("Wrong number of header columns in genome data file")
            }
            // All headers are present - now get the data
            val dataLine = br.readLine()
            val dataTokens = dataLine.split("\\t".toRegex()).toTypedArray()
            require(dataTokens.size == 9) { "ERROR - wrong number of data items in genotype datafile, expecting 9, found " + dataTokens.size }
            line = dataTokens[lineIndex]
            line_data = dataTokens[lineDataIndex]
            ploidy = dataTokens[ploidyIndex].toInt()
            hapNumber = dataTokens[hapNumberIndex].toInt()
            genesPhased = java.lang.Boolean.parseBoolean(dataTokens[genesPhasedIndex])
            chromsPhased = java.lang.Boolean.parseBoolean(dataTokens[chromPhasedIndex])
            conf = dataTokens[confIndex].toFloat()
            hapMethod = dataTokens[methodIndex]
            hapMethodDetails = dataTokens[methodDetailsIndex]
        } catch (ioe: IOException) {
            myLogger.error("LoadAllIntervalsToPHGdbPlugin: error parsing ref genome data file")
            throw IllegalArgumentException("Error parsing ref genome data file: " + ioe.message)
        }
    }


    override fun processData(input: DataSet): DataSet? {

        // Verify anchor file
        val overlaps = DBLoadingUtils.verifyIntervalRanges(anchors())

        if ( overlaps.size > 0) {
            // Overlaps not permitted.  User can fix via manually or via CreateValidIntervalsFilePlugin.  Throw error
            overlaps.stream().forEach { entry: String -> myLogger.error("LoadAllIntervalsToPHGdbPlugin:  range Overlap entry: $entry") }
            throw IllegalArgumentException("LoadAllIntervalsToPHGdbPlugin: intervals file has overlapping positions. Please consolidate/remove overlaps or run the CreateValidIntervalsFilePlugin with mergeOverlaps set to true to get a valid intervals files.")
        }

        val totalTime = System.nanoTime()

        val dbConnect = input.getData(0).data as Connection
                ?: throw IllegalStateException("LoadAllIntervalsToPHGdbPlugin: no connection supplied!")
        myLogger.info("LoadAllIntervaltoDBPlugin: have connection, create PHGdbAccess object")

        val phg: PHGDataWriter = PHGdbAccess(dbConnect)
        myRefSequence = GenomeSequenceBuilder.instance(refGenome())
        myLogger.info("LoadAllIntervaltoDBPlugin:  finished GenomeSequenceBuilder for ref genome")


        val methodParams = pluginParameters()
        methodParams.put("notes", hapMethodDetails)
        var time = System.nanoTime()
        // anchorRangeList to be used when creating/loading inter-anchors below
        createLoadRefRanges(phg, anchors(), refGenome(), ploidy, line,
                line_data, hapMethod, methodParams, hapNumber, genesPhased,
                chromsPhased, conf)

        myLogger.info("time for createLoadRefRanges: ${((System.nanoTime() - time)) / 1E9} sec")

        time = System.nanoTime()

        // Reference intervals have been loaded.  WIth just 1 reference per DB, there should not yet be variants.
        // Pre-load some allele data:
        val maxKmerLen = 5 // defaulting to 5, which gives us 3905 initial allele strings
        val initialAlleleList = DBLoadingUtils.createInitialAlleles(maxKmerLen)
        // Use LinkedHashSet so alleles are processed in the order in which they were inserted.
        // This allows for A,C,G,T,N followed by AA,CA,GA,TA,NA,AC,CC etc
        val initialAlleleSet: Set<String> = LinkedHashSet(initialAlleleList)
        phg.putAlleleData(initialAlleleSet)
        try {
            (phg as PHGdbAccess).close()
        } catch (exc: java.lang.Exception) {
            myLogger.error("Error attempting to close PHG db")
        }

        // Write file for liquibase db version check.
        writeLiquibaseFile(outputDir())
        myLogger.info(" Finished, TotalTime for LoadAllIntervalsToPHGdbPlugin was ${(System.nanoTime() - totalTime) / 1e9} seconds")
        return null
    }

    private fun createLoadRefRanges(phg: PHGDataWriter, ranges: String, refGenome: String, ploidy: Int, refLine: String,
                                    line_data: String, hapMethod: String, methodParams: MutableMap<String, String>, hapNumber: Int, genesPhased: Boolean,
                                    chromsPhased: Boolean, conf: Float)  {

        // method returns a list of refRegions and refInterRegions which will be used to
        // load the ref_range_ref_range_methods table

        myLogger.info("begin createLoadRefRanges, hapMethod = ${hapMethod}")
        var groupAndPositionsMap : Multimap<String, Range<Position>> = HashMultimap.create()

        var anchorsToLoad = ArrayList<AnchorDataPHG>()

        var refGrpMethods : MutableSet<String> = mutableSetOf()
        // Process the user interval ranges file
        try {
            Utils.getBufferedReader(ranges).use { br ->
                var chrom: String? = "-1"
                var prevChrom = "-1"
                var line: String? = null
                var chr: Chromosome? = null

                var chromAnchors = 0

                // FIrst load the genotypes and genome_file_data tables.
                // The haplotypes table needs the genome_file_data:id value,
                // and the genome_file_data table needs the genotypes:genoid value.

                val ghd = GenoHaploData(ploidy, true, refLine, line_data, genesPhased, chromsPhased, hapNumber, conf)
                phg.putGenoAndHaploTypeData(ghd)

                // Get the genoid created in call to putGenoAndHaplotypeData()
                // Needed for adding genome_file_data entry

                // Get the genoid created in call to putGenoAndHaplotypeData()
                // Needed for adding genome_file_data entry
                val genoid = phg.getGenoidFromLine(refLine)

                val genomeDataFileID = phg.putGenomeFileData(refServerPath(), refGenome, genoid)

                line = br.readLine()

                while (line != null) {
                    if (line.toUpperCase().contains("CHROMSTART")) {
                        line = br.readLine()
                        continue // skip header line
                    }
                    val tokens = line.split("\t")
                    if (tokens.size < 4) {
                        throw IllegalArgumentException("Error processing intervals file on line : ${line} . Must have values for columns chrom, chromStart, chromEnd and name")
                    }
                    chrom = tokens[0]

                    if (chrom != prevChrom) {
                        myLogger.info("Total anchors for chrom $prevChrom: $chromAnchors")
                        myLogger.info("Starting chrom $chrom")
                        chr = Chromosome.instance(chrom)
                        prevChrom = chrom
                        chromAnchors = 0
                    }

                    val anchorStart = tokens[1].toInt() + 1 // convert to physical position
                    val anchorEnd = tokens[2].toInt() // bed file is exclusive, no need to change

                    chromAnchors++
                    // get bytes from reference, convert to string, add data to list
                    val anchorString = myRefSequence!!.genotypeAsString(chr, anchorStart, anchorEnd)
                    val intervalStart = Position.of(Chromosome.instance(chrom), anchorStart)
                    val intervalEnd= Position.of(Chromosome.instance(chrom), anchorEnd)
                    val intervalRange = Range.closed(intervalStart, intervalEnd)
                    val type = tokens[3]
                    groupAndPositionsMap.put(type, intervalRange)
                    refGrpMethods.add(type) // for putRefAnchors() below

                    // Create VCList:
                    val rangeVCList: MutableList<VariantContext> = ArrayList()
                    val vc = AssemblyProcessingUtils.createRefRangeVC(myRefSequence, refLine, intervalStart, intervalEnd, intervalStart, intervalEnd)
                    rangeVCList.add(vc)

                    // Using refGenome() as the vcf as this is reference,
                    val adata = AnchorDataPHG(intervalRange, chrom, anchorStart, anchorEnd,"+",
                            refGenome, null, convertVCListToVariantInfoList(rangeVCList, refLine, 0), anchorString, genomeDataFileID)
                    anchorsToLoad.add(adata)

                    line = br.readLine()
                }
                myLogger.info("Total intervals for chrom ${prevChrom} : ${chromAnchors}")
            }

            // References are added as haplotype
            myLogger.info("createLoadRefRanges: finished processing ranges, call putGenoAndHapolotypeData")

            // Put the method data - identifies for each haplotype how the sequences were created
            // Also identifies the initial ref_range_group method.
            val methodType = if (isTestMethod()) DBLoadingUtils.MethodType.TEST_ANCHOR_HAPLOTYPES else DBLoadingUtils.MethodType.ANCHOR_HAPLOTYPES
            val hapMethodId = phg.putMethod(hapMethod, methodType, methodParams)

            // Load the gamete_groups and gamete_haplotypes table
            val nameWithHap = refLine + "_" + hapNumber
            val gameteGroupList: MutableList<String> = ArrayList()
            gameteGroupList.add(nameWithHap)
            phg.putGameteGroupAndHaplotypes(gameteGroupList)

            // Add data to the reference_ranges table.  Only change - comment out adding to ref_range_ref_range_methods table
            myLogger.info("mainProcessData: line has been added, load anchor data ...")
            phg.putAllAnchors(anchorsToLoad, -1)

            // phg.putRefAnchoData needs the ref_range_id for user interval, which it gets from
            // the ref_range_ref_range_methods tables.  Populate that table.  IT must be populated
            // after putAllAnchors() finishes and before putRefAnchorData() is called.
            myLogger.info("createLoadRefRanges: calling writeRefRangeRefRangeMethodTable... ")
            writeRefRangeRefRangeMethodTable(groupAndPositionsMap, phg, methodParams)

            // Put ref anchor_sequences data,  it calls putHaplotypes to store sequence data.
            // hapMethodId is descriptive of method that created intervals, not indicative of refRegin vs refInterRegion
            myLogger.info("createLoadREfRanges: calling putRefAnchorData, hapMethodId= ${hapMethodId} size of anchorsToLoad ${anchorsToLoad.size}")
            phg.putRefAnchorData(refLine, hapNumber, anchorsToLoad, hapMethodId, refGrpMethods, refLine, null)

        } catch (exc: Exception) {
            throw IllegalArgumentException("LoadAllIntervalsToPHGdbPlugin : error processing intervals file " + exc.message)
        }
        return
    }

    private fun writeRefRangeRefRangeMethodTable(groupAndPositionsMap: Multimap<String, Range<Position>>, phg:
    PHGDataWriter, methodParams: MutableMap<String, String>) {
        // ids for ref_range_ref_range_method table: returns RangeMap<Position,Integer>
        val dbRangeIds = phg.getIntervalRangesWithIDForChrom("all")
        myLogger.info("writeRefRangeRefRangeMethodTable -  begin")

        groupAndPositionsMap.keySet().forEach { key ->

            var ranges = groupAndPositionsMap.get(key)
            var idList = ArrayList<Int>()
            for (range in ranges) {
                var id = dbRangeIds.asMapOfRanges().get(range)
                if (id == null || id < 0) {
                    throw IllegalArgumentException("writeREfRangeRefRangeMethodTable: error, interval not found in db for range ${range.toString()}")
                }
                idList.add(id)
            }

            // TODO - how to get user supplied description here?
            val refGrpMethodDetails = "User supplied group"
            var refRangeParams = pluginParameters()
            refRangeParams.put("grpNotes", refGrpMethodDetails)

            myLogger.info("writeRefRangeRefRangeMethodTable - adding method ${key}")
            var methodId = phg.putMethod(key, DBLoadingUtils.MethodType.REF_RANGE_GROUP, refRangeParams)
            myLogger.info("writeREfRangeRefRangeMEthodTable: methodId for ${key}: ${methodId}")
            phg.putRefRangeRefRangeMethod(methodId, idList)
        }
        myLogger.info("writeRefRangeRefRangeMethodTable finished")
    }

    private fun writeLiquibaseFile(outputDir: String) {
        val runYes = "$outputDir/run_yes.txt"
        try {
            Utils.getBufferedWriter(runYes).use { bw -> bw.write("yes/n") }
        } catch (exc: java.lang.Exception) {
            myLogger.error(exc.message, exc)
            throw IllegalStateException("LoadAllIntervalsToPHGdbPlugin:writeLiquibaseFile - error writing file $runYes")
        }
    }

    override fun getToolTipText(): String {
        return("Load intervals and haplotypes from user intervals file and reference genome fasta ")
    }

    override fun getIcon(): ImageIcon ?{
        return (null)
    }

    override fun getButtonName(): String {
        return("Load All Intervals to PHG DB")
    }

    /**
     * Reference Genome File for aligning against
     *
     * @return Reference Genome File
     */
    fun refGenome(): String {
        return refGenome.value()
    }

    /**
     * Set Reference Genome File. Reference Genome File for
     * aligning against
     *
     * @param value Reference Genome File
     *
     * @return this plugin
     */
    fun refGenome(value: String): LoadAllIntervalsToPHGdbPlugin {
        refGenome = PluginParameter<String>(refGenome, value)
        return this
    }

    /**
     * Tab-delimited file containing Chrom, StartPosition, EndPosition
     * and Type
     *
     * @return Anchors File
     */
    fun anchors(): String {
        return anchors.value()
    }

    /**
     * Set Anchors File. Tab-delimited file containing StartPosition, EndPosition
     * Type
     *
     * @param value Anchors File
     *
     * @return this plugin
     */
    fun anchors(value: String): LoadAllIntervalsToPHGdbPlugin {
        anchors = PluginParameter<String>(anchors, value)
        return this
    }

    /**
     * Path to tab-delimited file containing genome specific
     * data with header line:
     * Genotype Hapnumber Dataline Ploidy Reference GenePhased
     * ChromPhased Confidence Method MethodDetails
     *
     * @return Genome Data File
     */
    fun genomeData(): String {
        return genomeData.value()
    }

    /**
     * Set Genome Data File. Path to tab-delimited file containing
     * genome speciic data with header line:
     * Genotype Hapnumber Dataline Ploidy Reference GenePhased
     * ChromPhased Confidence Method MethodDetails
     *
     * @param value Genome Data File
     *
     * @return this plugin
     */
    fun genomeData(value: String): LoadAllIntervalsToPHGdbPlugin {
        genomeData = PluginParameter<String>(genomeData, value)
        return this
    }

    /**
     * Directory to write liquibase changeLogSync output
     *
     * @return Output Directory
     */
    fun outputDir(): String {
        return outputDir.value()
    }

    /**
     * Set Output Directory. Directory to write liquibase
     * changeLogSync output
     *
     * @param value Output Directory
     *
     * @return this plugin
     */
    fun outputDir(value: String): LoadAllIntervalsToPHGdbPlugin {
        outputDir = PluginParameter<String>(outputDir, value)
        return this
    }

    /**
     * String that contains a server and file path where the
     * reference genome will be stored for future access.
     *  This ia a more permanent location, not where the genome
     * file lives for procssing via this plugin.
     *
     * @return Reference Server Path
     */
    fun refServerPath(): String {
        return refServerPath.value()
    }

    /**
     * Set Reference Server Path. String that contains a server
     * and file path where the reference genome will be stored
     * for future access.  This ia a more permanent location,
     * not where the genome file lives for procssing via this
     * plugin.
     *
     * @param value Reference Server Path
     *
     * @return this plugin
     */
    fun refServerPath(value: String): LoadAllIntervalsToPHGdbPlugin {
        refServerPath = PluginParameter<String>(refServerPath, value)
        return this
    }

    /**
     * Indication if the data is to be loaded against a test
     * method. Data loaded with test methods are not cached
     * with the PHG ktor server
     *
     * @return Is Test Method
     */
    fun isTestMethod(): Boolean {
        return isTestMethod.value()
    }

    /**
     * Set Is Test Method. Indication if the data is to be
     * loaded against a test method. Data loaded with test
     * methods are not cached with the PHG ktor server
     *
     * @param value Is Test Method
     *
     * @return this plugin
     */
    fun isTestMethod(value: Boolean): LoadAllIntervalsToPHGdbPlugin {
        isTestMethod = PluginParameter<Boolean>(isTestMethod, value)
        return this
    }



}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(LoadAllIntervalsToPHGdbPlugin::class.java)
}