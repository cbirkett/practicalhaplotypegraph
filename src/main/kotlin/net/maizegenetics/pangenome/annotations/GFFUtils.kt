package net.maizegenetics.pangenome.annotations

import com.google.common.collect.ContiguousSet
import com.google.common.collect.Range
import com.google.common.collect.RangeSet
import com.google.common.collect.TreeRangeSet
import htsjdk.tribble.AbstractFeatureReader
import htsjdk.tribble.TribbleException
import htsjdk.tribble.annotation.Strand
import htsjdk.tribble.gff.*
import net.maizegenetics.util.Utils
import java.io.IOException
import java.nio.file.Paths
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.Position
import net.maizegenetics.pangenome.api.*
import net.maizegenetics.taxa.TaxaList
import net.maizegenetics.taxa.TaxaListBuilder
import java.util.*
import java.util.regex.Pattern
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet

/**
 * GFFUtils class:  Contains functions used to process GFF3 files.
 * These files are processed using  htsjdk GFF3 functions
 *
 * Restrictions:  The gff files read by the htsjdk reader must end
 * with .gff3, .gff3.gz, .gff or .gff.gz
 *
 * @author lcj34
 */


/**
 * Function to use htsjdk to read gff into memory.
 * Returns a Set<Gff3Feature> .
 *
 * NOTE:  gff files must end with .gff3, .gff, .gff3.gz or .gff.gz
 *        Any other extension causes htsjdk feature reader to throw an exception
 *
 * The returned feature set may be printed using htsdjk Gff3Writer
 */
fun readGFFtoGff3Feature(gffFile: String):List<Gff3Feature> {

    val gffFeatures: MutableSet<Gff3Feature> = HashSet<Gff3Feature>()
    AbstractFeatureReader.getFeatureReader(gffFile, null, Gff3Codec(), false).use{ reader ->
        for (feature in reader.iterator()) {
            // This grabs a line. It skips over commented lines
            gffFeatures.add(feature)
        }
    }

    // While gff files are generally sorted by chr, they are not always sorted by start/end.
    // The start/end postions start over when they are in a second transcript.
    // For our processing, in order to get all overlap efficiently, these need have all entries
    // sorted by chr/start/end.  This means T0002 will be interspered with T0001.

    val sortedGffEntries = gffFeatures.toList().sortedWith(Comparator { first: Gff3Feature, second: Gff3Feature ->
        var contigCompare = first.contig.compareTo(second.contig)
        if (contigCompare == 0) {
            var startCompare = first.start.compareTo(second.start)
            if (startCompare == 0) {
                first.end.compareTo(second.end)
            } else {
                first.start.compareTo(second.start)
            }
        } else {
            first.contig.compareTo(second.contig)
        }
    })
    return sortedGffEntries
}

/**
 * Function takes a key file with columns taxon, gffFile.
 * For each taxon, read the gff file using htsjdk gff feature reader.
 * Return a map of taxon to associated Gff3Feature entries
 */
fun loadGFFsToGff3Feature(keyFile: String): Map<String, TreeMap<Position,ArrayList<Gff3Feature>>> {

    val resultsTree:MutableMap<String, TreeMap<Position,ArrayList<Gff3Feature>>> = mutableMapOf()
    //val taxonToGFFfileMap:MutableMap<String, String> = HashMap<String, String>()
    val taxonToGFFfileMap:MutableMap<String, String> = mutableMapOf()
    Utils.getBufferedReader(keyFile)
            .forEachLine{
                val parsedLine = it.split("\t");
                taxonToGFFfileMap.put(parsedLine[0], parsedLine[1])
            }

    // process each gff file
    for ((taxon, gffFile) in taxonToGFFfileMap) {
        val time = System.nanoTime()
        val features = readGFFtoGff3Feature(gffFile)
        val featureTreeMap = createTreeMapFromFeaturesCenter(features)
        resultsTree.put(taxon, featureTreeMap)
        val endTime = (System.nanoTime() - time)/1e9
        println("loadGffsToGff3Feature: time to load ${taxon} gff file: ${endTime}")
    }
    return resultsTree
}

/**
 * Reads a key file with columns for "taxon" and "Path/name of gff file"
 * Returns a map of taxon->fileName
 */
fun getTaxonToGffFile(keyFile:String): Map<String,String>  {
    val taxonToGFFfileMap:MutableMap<String, String> = HashMap<String, String>()
    Utils.getBufferedReader(keyFile)
            .forEachLine{
                val parsedLine = it.split("\t")
                taxonToGFFfileMap.put(parsedLine[0], parsedLine[1])
            }
    return taxonToGFFfileMap
}
/**
 * This method creates a mapping of the feature center position (posSTart + posEnd)/2 to
 * list of Gff3Features
 */
fun createTreeMapFromFeaturesCenter(features: List<Gff3Feature>): TreeMap<Position,ArrayList<Gff3Feature>> {
    val treeMap = TreeMap<Position,ArrayList<Gff3Feature>>()

    for (feature in features) {

        // Don't include the "chromosome" line.  This will overlap with everything for that chromosome
        if (feature.type.equals("chromosome")) continue
        // for gffs, the start is always <= end per the specs
        val center = (feature.end + feature.start)/2
        val chrom = Chromosome.instance(feature.contig)
        val centerPos = Position.of(chrom,center)
        var posList = treeMap.get(centerPos)
        if (posList == null) {
                posList = ArrayList<Gff3Feature>()
        }
        posList.add(feature)
        treeMap.put(centerPos,posList)
    }

    return treeMap
}

/**
 * This method takes a path ( a list of integer haplotype ids),
 *   a phg graph that is based on the haplotypeIds in the path,
 *   and an optional output file name.
 *
 *  From the graph it  pulls the asm contig and coordinates for each
 *  item on the path, finds regions in the Gff3 entries that overlap
 *  with the graph haplotype asm coordinate entries, and creates a set
 *  of Gff3Feature entries.
 *
 *  If an outputFile is specified, the gff file is written to the specified path.
 *
 * Return:  A set of Gff3Features
 *
 */
fun makeGffFromPath(path: List<Int>, centerGffs: Map<String, TreeMap<Position,ArrayList<Gff3Feature>>>, graph: HaplotypeGraph, outputFile: String?): Set<Gff3Feature> {

    var time = System.nanoTime()
    val pseudoGenomeGff3Features :MutableSet<Gff3Feature> = mutableSetOf()

    // List of reference ranges will be used to order the haplotypenodes
    // so all chromosome entries are processed together.
    val idToRefRangeMap = mutableMapOf<Int, ReferenceRange>()
    println("makeGffFromPath: creating idToRefRangeMap")
    for(refRange in graph.referenceRanges()) {
        for(node in graph.nodes(refRange)) {
            idToRefRangeMap[node.id()] = refRange
        }
    }

    // this walks through the ordered list of reference ranges
    // there should only be 1 node at each ref ranges as this is a path
    val firstRange = graph.referenceRanges().first()
    var curChrom = firstRange.chromosome().name
    var prevChrom = curChrom
    var offset = 0;

    // There will be multiple reference ranges per chromosome.
    var count = 0
    var total = 0
    var chromGffSource = "PHG"
    for(refRange in graph.referenceRanges()) {
        val nodesAtRefRange = graph.nodes(refRange)
        curChrom = nodesAtRefRange[0].asmContig() // contig names should match reference chromosome names
        if (!curChrom.equals(prevChrom)) {
            // When score is < 0, it will print Gff3Constants.UNDEFINED_FIELD_VALUEm, which is "."
            // Create "chromosome" Entry for gff
            val gff3FeatureEntry = createGffChromosomeEntry(prevChrom, offset)

            // This unfortunately comes at the end, not the beginning. of this chromosome list. But we don't
            // know the size until we've finished with haplotypes for this chrom.
            pseudoGenomeGff3Features.add(gff3FeatureEntry)
            offset = 0 // starting a new chromosome, offset becomes 0
        };
        prevChrom = curChrom
        for(node in nodesAtRefRange) { // there should only be 1
            // get the asm coordinates, compare to gff
            val taxon = node.taxaList().get(0)
            val asmCenterGffEntries = centerGffs.get(taxon.name)
            if (asmCenterGffEntries == null || asmCenterGffEntries.size == 0) {
               // println("makeGffFromPath: WARNING: no gff entries for taxon ${taxon.name}")
                continue // go on to next one
            }

            val pseudoGffEntries = getOverlappingEntriesFromGff(node.asmContig(), node.asmStart()..node.asmEnd(), asmCenterGffEntries)
            for (entry in pseudoGffEntries) {
                // convert the coordinates to pseudoGenome Coordinates
                val newRange = getPseudoGenomeGFFCoordinates(entry.start..entry.end, node.asmStart()..node.asmEnd(), offset)

                // Update the entry to be specific to gff coordinates.
                // Add an annotations for:
                //   reference_range_id
                //   haplotypes_id
                //   haplotype taxon
                //   asm coordinates for haplotype
                // The haplotype asm coordinates - can be used to distinguish partial
                // vs full mapping based on size of asm sequence and where the coordinates overlapped

                val annotations:MutableMap<String,List<String>> = mutableMapOf()
                annotations.putAll(entry.attributes)
                val refRangeList = listOf("${refRange.id()}")
                annotations.put("referenceRangeID",refRangeList)
                val haplotypeIDList = listOf("${node.id().toString()}")
                annotations.put("haplotypeID",haplotypeIDList)
                val taxonList = listOf("${taxon}") // the taxon from which these entries came.
                annotations.put("taxon",taxonList)
                val annoList = listOf("${node.asmStart()}","${node.asmEnd()}")
                annotations.put("halotypeAsmCoordinates", annoList)

                // Create the Gff3Feature entry, add to list
                val gff3FeatureEntry = Gff3FeatureImpl(entry.contig, entry.source, entry.type, newRange.first, newRange.last, entry.score, entry.strand, entry.phase, annotations)
                pseudoGenomeGff3Features.add(gff3FeatureEntry)
            }
        }

        count++
        if (count > 10000) {
            total += count
            count = 0
            println("makeGffFromPath: entries finished: ${total}")
        }
        offset += nodesAtRefRange[0].haplotypeSequence().length()
    }

    // Create the last "chromosome" entry for this file
    val gff3FeatureEntry = createGffChromosomeEntry(curChrom, offset)
    pseudoGenomeGff3Features.add(gff3FeatureEntry)

    if (count > 0) total += count
    println("makeGffFromPath: all entries finished, total: ${total}")
    if (outputFile != null) {
        println("makeGffFromPath: writing gff file ${outputFile}")
        writeGffFile(outputFile, pseudoGenomeGff3Features,null,null)
    }
    val endingTime = (System.nanoTime() - time)/1e9
    println("makeGffFromPath: time to process path: ${endingTime}")
    return pseudoGenomeGff3Features
}

/**
 * Using htsjdk classes,  writeGffFile will separately write comments, sequenceRegions, and features.
 * The list of comments (List<String>) and regions (Set<SequenceRegion>) are optional
 * The list of features (Set<Gff3Feature>) is required.
 *
 * The data is written to a GFF3 formatted file.
 */
fun writeGffFile(outputFile: String, features: Set<Gff3Feature>, comments: List<String>?, regions: Set<SequenceRegion>?) {

    try {

        Gff3Writer(Paths.get(outputFile)).use { writer ->
            // comments are optional
            comments?.forEach {
                writer.addComment(it)
            }

            // regions are optional
            regions?.forEach {
                writer.addDirective(Gff3Codec.Gff3Directive.SEQUENCE_REGION_DIRECTIVE, it)
            }
            // There should be features!
            features.forEach {
                writer.addFeature(it)
            }

        }
    } catch (ioe: IOException) {
        throw TribbleException("Error writing to file $outputFile", ioe)
    }
}

/**
 * Search the gff entry map for overlaps with the haplotypeNode asm coordinates.
 * Requires as input a map keyed by TASSEL Position object as well as the contig
 * and coordinates from the haplotypeNode.
 */
fun getOverlappingEntriesFromGff(contig: String, haplotypeRange:IntRange, asmCenterGffs: TreeMap<Position,ArrayList<Gff3Feature>>): Set<Gff3Feature> {

    val gff3Features = HashSet<Gff3Feature>() // no ordering here!
    val hapChrom = Chromosome.instance(contig)
    var hapStartPos = Position.of(hapChrom,haplotypeRange.first)
    var hapEndPos = Position.of(hapChrom,haplotypeRange.endInclusive)

    // during assembly processing, we did not change the direction of the
    // assembly coordinates.  If the start > end, it means it was aligned
    // on the reverse strand.  Flip them here so we don't have problems
    // pulling the submap below.
    if (hapStartPos > hapEndPos) {
        var temp = hapStartPos
        hapStartPos = hapEndPos
        hapEndPos = temp
    }
    // The asmCenterGffs has a key that is a Position object whose "position" is created from the
    // middle point of the coordinate range.  We grab a submap of the gff that contains this endpoint.
    // the submap will be based on the haplotype node's asm coordinates.  "floorKey" and "ceilingKey"
    // are used as the specific haplotype node start/end positions may not be keys in the map.

    // Start with floorKey 1 less than the haplotype node start position to get more
    // gff3 entries around the center position.
    var floorKeyTemp = asmCenterGffs.floorKey(hapStartPos)
    var floorKey = floorKeyTemp
    if (floorKeyTemp != null) {
        floorKey = Position.of(floorKeyTemp.chromosome, floorKeyTemp.position-1)
    }

    // Because submap below grabs the values in an inclusive/exclusive manner,
    // the ceilingKey value must be 1 greater than what we want.
    var ceilingKeyTemp = asmCenterGffs.ceilingKey(hapEndPos)
    var ceilingKey = ceilingKeyTemp
    if (ceilingKeyTemp != null) {
        ceilingKey = Position.of(ceilingKeyTemp.chromosome,ceilingKeyTemp.position+1)
    }

    if (floorKey == null) {
        if (ceilingKey != null) {
            floorKey = Position.of(ceilingKey.chromosome,ceilingKey.position-1)
        }
    }
    if (ceilingKey == null) {
        if (floorKey != null) {
            ceilingKey = Position.of(floorKey.chromosome,floorKey.position+1)
        }
    }

    val subMap1 = if (floorKey != null ) asmCenterGffs.subMap(floorKey,ceilingKey).flatMap { it -> it.value } else null

    // There are many instances where the coordinates from floorKey or ceilingKey do not overlap the
    // haplotypeNode's asm coordinates.  So the entries returned in the submap must be checked for overlap.
    if (subMap1 != null) {
        for (gff3entry in subMap1) {
            val gffChromName = Chromosome.instance(gff3entry.contig).name
            if (gffChromName.equals(hapChrom.name)) { // do I need this conditional ??
                if ((haplotypeRange.first <= gff3entry.end) && (haplotypeRange.endInclusive >= gff3entry.start)) {
                    gff3Features.add(gff3entry)
                }
            }
        }
    }
    return gff3Features
}


/**
 * Creates new start/end coordinates based on the parts of the haplotype node that intersect
 * with the range for an assembly GFF3 entry.  When calculating the new coordinates, the range
 * overlaps and the offset from the start of the pseudo-genome are considered.
 *
 * Return: IntRange holding the new start/end pseudo-genome coordinates.
 */
fun getPseudoGenomeGFFCoordinates(asmGffRange: IntRange, hapNodeRange: IntRange, offset: Int):IntRange {

    // Because the range is inclusive/inclusive, the size is actually + 1
    // But the value we need to add is just the difference between the start and end.
    val featureSize = asmGffRange.endInclusive - asmGffRange.start
    // startDiff is negative if the haplotype start coordinate begins after
    // the assembly gff entry start value.
    var hapStart = hapNodeRange.start
    var hapEnd = hapNodeRange.endInclusive
    if (hapStart > hapEnd) {
        hapStart = hapEnd
        hapEnd = hapNodeRange.start
    }
    val startDiff = asmGffRange.start - hapStart
    val endDiff = hapEnd - asmGffRange.endInclusive
    // endDiff and endAdjust are negative if the haplotype node coordinate end value
    // is less than the assembly gff end value, meaning it doesn't cover the full GFF entry
    val endAdjust = if (endDiff > 0) 0 else endDiff

    // pgStart is > 1 if the haplotypeNode starts  prior to the gff entry
    val pgStart = if (startDiff > 0) startDiff else 1

    // If startDiff is 0, or pgStart > 1, the haplotype node includes the beginning
    // of the assembly gff entry.  The end is then the size of this feature, minus
    // any part of the end that is not included in the haplotype node.
    // Otherwise, if pgStart == 1, it means the beginning of the feature was not included.
    // The end is then adjusted by subtracting a count of the missing bps from both
    // ends from the feature size.

    val pgEnd = if (startDiff == 0 || pgStart > 1) pgStart + featureSize + endAdjust
                else pgStart + featureSize + endAdjust + startDiff
    val returnStart = pgStart+offset
    val returnEnd = pgEnd+offset

    return pgStart+offset..pgEnd+offset
}

/**
 * Create the "chromosome" type line in the gff file
 */
fun createGffChromosomeEntry(prevChrom:String, offset:Int):Gff3Feature {
    var annotations:MutableMap<String,List<String>> = mutableMapOf()
    annotations.put("ID",listOf(prevChrom))
    annotations.put("Name",listOf("chromosome:PHG-pseudo_genome"))
    var chromGffSource = "PHG"
    // When score or phase values are < 0, Gff3Feature writer will print Gff3Constants.UNDEFINED_FIELD_VALUE,
    // which is "." and what we want here
    val minus1 = -1
    val minus1Double = minus1.toDouble()
    val gff3FeatureEntry = Gff3FeatureImpl(prevChrom,chromGffSource,"chromosome",1,
            offset,minus1Double, Strand.NONE, -1, annotations)

    return gff3FeatureEntry
}

/**
 * Counts the number of times a single GFF ID appears.
 * User may specify the full ID or just part of it.  The
 * code checks for GFF entries where the ID starts with
 * the user specified string.
 *
 * returns:  number of times that ID appears in the feature set.
 */
fun gffSingleIDcount(idToMatch:String, gffSet:Set<Gff3Feature>):Int {

    val idCount = gffSet.asSequence().map{feature -> feature.id}
            .filter{it.startsWith(idToMatch)}
            .count()
    return idCount
}

/**
 * Takes a set of Gff3Features, creates a mapping of
 * GFF id (ID from the attributes column) to number of times it appears
 * The "count" parameter tells by what amount to shorten the ID.
 *
 * For example:  If the user only wants the first 8 characters of the
 * ID field considered, count would be 8.   This would allow for counting
 * something like all instances of RST00015*.
 *
 * If "count" is > than the length of the ID, the full value is used as the key.
 *
 * returns:  a mapping of ID (subsetted to first "count" letters) to number of
 * times it occurs.
 */
fun gffAllIDcount(gffSet:Set<Gff3Feature>, count:Int):Map<String,Int> {
    val idToCountMap = gffSet.asSequence().groupingBy { it.id.take(count)}.eachCount()
    return idToCountMap
}

/**
 * Counts the gff entries per chromosome
 *
 * returns:  Map of chromsome -> number of entries
 */
fun getGFFEntriesPerChrom(gffSet:Set<Gff3Feature>):Map<String,Int> {
    val chromCounts = gffSet.groupingBy{it.contig}.eachCount()
    return chromCounts
}

/**
 * Counts the number of distinct ID values either across the full
 * GFF3Feature set (if contig==null) or for a specific contig
 */
fun countDistinctID(gffSet:Set<Gff3Feature>,contig:String?): Int {
    var distinctCount = 0
    if (contig == null) {
        // count all distinct IDs
       distinctCount= gffSet.distinctBy {gff3Feature -> gff3Feature.id }.count()
    } else {
        distinctCount = gffSet.asSequence()
                .filter{it.contig.equals(contig)}
                .distinctBy {it.id}
                .count()
    }
    return distinctCount
}

/**
 * Get list of distinct GFF IDs in the full Gff3Feature Set
 *
 * returns: List<String> where String is the ID value
 */
fun getDistinctGffIds(gffSet:Set<Gff3Feature>): List<String> {
    val distinctIDs = gffSet.asSequence()
            .map{it -> it.id}
            .distinct().toList()
    return distinctIDs
}

/**
 * Creates map of contig(chrom) to set of distinct Gff3Feature ids
 * associated with that contig
 */
fun getDistinctGffIdsByChrom(gffSet:Set<Gff3Feature>): Map<String,Set<String>> {
    val chromToIds = TreeMap<String,MutableSet<String>>()

    for (feature in gffSet) {
        val contig = feature.contig
        val idSet =  chromToIds?.get(contig)?:mutableSetOf<String>()

        idSet.add(feature.id)
        chromToIds.put(contig,idSet)
    }
    return chromToIds
}

/**
 * Calculates the number of BPs for each contig/chromosome in the GFF file
 * Overlapping positions are only counted once.
 *
 * return:  A map of chromosome to number of included positions
 */
fun sumPerChromGFFBasePairs(gffSet:Set<Gff3Feature>): Map<String,Int> {

    val chromSizeMap = mutableMapOf<String,Int>() // holds total length of each chromsome

    // Using RangeSets as GFFs may have overlapping positions among different types (Exon, CDS, etc)
    val chromRangeSet: RangeSet<Position> = TreeRangeSet.create()
    gffSet.asSequence().forEach {
        val chrom = Chromosome.instance(it.contig) // because db used Chromosome object, must get gff to match
        if (it.type.equals("chromosome")) {
            chromSizeMap.put(chrom.name,it.end)
        } else {
            val range= Range.closed(Position.of(chrom,it.start),Position.of(chrom,it.end))
            chromRangeSet.add(range)
        }

    }

    // process the overlaps for each chrom
    // The number of base pairs represented in the GFF is the total number of
    // non-overlapping basepairs that occur for each chromosome.  The lower bound
    // is 1 and the upper bound is the size of the chromosome.
    var chromBPsumMap = chromSizeMap.map {(chr,size) ->
        val chrom = Chromosome.instance(chr)
        val testRange = Range.closed(Position.of(chrom,1),Position.of(chrom,size))
        val includedBP = chromRangeSet.subRangeSet(testRange).asRanges()
                .map { range ->
                    range.upperEndpoint().position - range.lowerEndpoint().position +1
                }.sum()
        Pair(chrom.name, includedBP)
    }.toMap()


    return chromBPsumMap
}

fun percentPerChromGFFBasePairs(gffSet:Set<Gff3Feature>): Map<String,Double> {
    val chromSizeMap = mutableMapOf<String,Int>() // holds total length of each chromsome

    // Using RangeSets as GFFs may have overlapping positions among different types (Exon, CDS, etc)
    val chromRangeSet: RangeSet<Position> = TreeRangeSet.create()
    gffSet.asSequence().forEach {
        val chrom = Chromosome.instance(it.contig) // because db used Chromosome object, must get gff to match
        if (it.type.equals("chromosome")) {
            chromSizeMap.put(chrom.name,it.end)
        } else {
            val range= Range.closed(Position.of(chrom,it.start),Position.of(chrom,it.end))
            chromRangeSet.add(range)
        }

    }

    // process the overlaps for each chrom
    // The number of base pairs represented in the GFF is the total number of
    // non-overlapping basepairs that occur for each chromosome.  The lower bound
    // is 1 and the upper bound is the size of the chromosome.
    var chromBPsumMap = chromSizeMap.map {(chr,size) ->
        val chrom = Chromosome.instance(chr)
        val testRange = Range.closed(Position.of(chrom,1),Position.of(chrom,size))
        val includedBP = chromRangeSet.subRangeSet(testRange).asRanges()
            .map { range ->
                range.upperEndpoint().position - range.lowerEndpoint().position +1
            }.sum()
        val percent = (includedBP.toDouble()/size.toDouble()) * 100
        Pair(chrom.name, percent)
    }.toMap()


    return chromBPsumMap
}
/**
 * Calculates the number of BPs for each contig/chromosome that were not
 * present in the GFF file.  For example:  If the chromosome is of length
 * 2000, and there are only 850 total base pairs for that chromosome
 * in the GFF, then the non-represented number is 1150
 *
 * return: a map of chromosome to number of bps NOT represented in the GFF file
 */
fun sumPerChromNonGFFBasePairs(gffSet:Set<Gff3Feature>): Map<String,Int> {

    val chromSizeMap = mutableMapOf<String,Int>() // holds total length of each chromsome

    // Using RangeSets as GFFs may have overlapping positions among different types (Exon, CDS, etc)
    val chromRangeSet: RangeSet<Position> = TreeRangeSet.create()
    gffSet.asSequence().forEach {
        val chrom = Chromosome.instance(it.contig) // because db used Chromosome object, must get gff to match
        if (it.type.equals("chromosome")) {
            chromSizeMap.put(chrom.name,it.end)
        } else {
            val range= Range.closed(Position.of(chrom,it.start),Position.of(chrom,it.end))
            chromRangeSet.add(range)
        }
    }

    // process the overlaps for each chrom
    // The number of base pairs represented in the GFF is the total number of
    // non-overlapping basepairs that occur for each chromosome.  The upper and
    // lower bound are 1 and the size of the chromosome.
    // Subtract the overlapping bps from the total bps in the chromosome to get
    // the number of chromsome base-pairs that are not represented in this GFF set.
    var chromBPsumMap = chromSizeMap.map { (chr,size) ->
        val chrom = Chromosome.instance(chr)
        val testRange = Range.closed(Position.of(chrom,1),Position.of(chrom,size))
        val includedBP = chromRangeSet.subRangeSet(testRange).asRanges()
                .map { range ->
                    range.upperEndpoint().position - range.lowerEndpoint().position +1
                }.sum()

        // This line differentiates this method from sumPerChromGFFBasePairs()
        Pair(chrom.name,size-includedBP)
    }.toMap()

    return chromBPsumMap
}

fun percentPerChromNonGFFBasePairs(gffSet:Set<Gff3Feature>): Map<String,Double> {

    val chromSizeMap = mutableMapOf<String,Int>() // holds total length of each chromsome

    // Using RangeSets as GFFs may have overlapping positions among different types (Exon, CDS, etc)
    val chromRangeSet: RangeSet<Position> = TreeRangeSet.create()
    gffSet.asSequence().forEach {
        val chrom = Chromosome.instance(it.contig) // because db used Chromosome object, must get gff to match
        if (it.type.equals("chromosome")) {
            chromSizeMap.put(chrom.name,it.end)
        } else {
            val range= Range.closed(Position.of(chrom,it.start),Position.of(chrom,it.end))
            chromRangeSet.add(range)
        }
    }

    // process the overlaps for each chrom
    // The number of base pairs represented in the GFF is the total number of
    // non-overlapping basepairs that occur for each chromosome.  The upper and
    // lower bound are 1 and the size of the chromosome.
    // Subtract the overlapping bps from the total bps in the chromosome to get
    // the number of chromsome base-pairs that are not represented in this GFF set.
    var chromBPsumMap = chromSizeMap.map { (chr,size) ->
        val chrom = Chromosome.instance(chr)
        val testRange = Range.closed(Position.of(chrom,1),Position.of(chrom,size))
        val includedBP = chromRangeSet.subRangeSet(testRange).asRanges()
            .map { range ->
                range.upperEndpoint().position - range.lowerEndpoint().position +1
            }.sum()

        val percent = ((size-includedBP).toDouble()/size.toDouble()) * 100
        // This line differentiates this method from sumPerChromGFFBasePairs()
        Pair(chrom.name,percent)
    }.toMap()

    return chromBPsumMap
}

fun createTaxaListFromFileOrString(taxa: String): TaxaList {
    if (taxa.endsWith(".txt")) {
        try {
            val builder = TaxaListBuilder()
            Utils.getBufferedReader(taxa).use { br ->
                var line = br.readLine()
                val sep = Pattern.compile("\\s+")
                while (line != null) {
                    line = line.trim()
                    val parsedline = sep.split(line)
                    for (idx in parsedline.indices) {
                        if (parsedline[idx] != null || parsedline[idx]!!.length != 0) {
                            builder.add(parsedline[idx])
                        }
                    }
                    line = br.readLine()
                }
            }
            return builder.build()
        } catch (exc: Exception) {
            throw IllegalArgumentException("Error reading taxa file ${taxa}")
        }
    } else {
        val taxa = taxa.trim().split(",")
        return TaxaListBuilder().addAll(taxa).build()
    }
}