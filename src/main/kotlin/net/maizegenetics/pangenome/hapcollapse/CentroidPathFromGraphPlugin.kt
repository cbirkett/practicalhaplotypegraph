package net.maizegenetics.pangenome.hapcollapse

import net.maizegenetics.pangenome.api.*
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.pangenome.hapCalling.BestHaplotypePathPlugin
import net.maizegenetics.pangenome.smallseq.SmallSeqPaths
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import java.awt.Frame
import java.io.PrintWriter
import javax.swing.ImageIcon

/**
 * CentroidPathFromGraphPlugin calculates centroids from an input graph based on kmers from the HaplotypeNode sequence.
 * The id of the haplotype that is closest to the centroid for each reference range is stored as a path.
 * The path sequence can also be output to a fasta with one record per chromosome. In addition, a bedfile of reference range
 * coordinates can be written with the coordinates of the centroid genome.
 *
 * Input: One HaplotypeGraph. The db config parameters must be in the parameter
 * cache, for instance, by including -configParameters in the command that calls the plugin.
 *
 * Output: The plugin always returns null but can store the path in PHG DB, write a centroid genome fasta,
 * and write a reference range bed file.
 */
class CentroidPathFromGraphPlugin (parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {
    private var pathName = PluginParameter.Builder("pathName", "centroidPath", String::class.java)
        .description("The name to give this path in the database")
        .build()

    private var writeToDB = PluginParameter.Builder("writeToDB", true, Boolean::class.javaObjectType)
        .description("Should the resulting path be written to the PHG DB? Specify the database connection parameters using -configParameters.")
        .build()

    private var fastaName = PluginParameter.Builder("fastaName", null, String::class.java)
        .description("The name and path to which the fasta file will be written. If the file name ends in .gz, the file will be gzipped.")
        .outFile()
        .build()

    private var bedfileName = PluginParameter.Builder("bedfileName", null, String::class.java)
        .description("The name and path to which a bedfile will be written. The bedfile will contain " +
                "the reference range start and end coordinates for a centroid reference sequence.")
        .outFile()
        .build()

    private var kmerSize = PluginParameter.Builder("kmerSize", 7,Int::class.javaObjectType)
        .description("Kmer size")
        .build()

    private var isTestMethod = PluginParameter.Builder("isTestMethod", false, Boolean::class.javaObjectType)
        .description("Indication if the data is to be loaded against a test method. Data loaded with test methods are not cached with the PHG ktor server")
        .required(false).build()

    private val methodName = "Centroid_Path"

    override fun processData(input: DataSet?): DataSet? {
        require(input != null) {"Input to CentroidPathFromGraph is null."}
        val dataList = input.getDataOfType(HaplotypeGraph::class.java)
        require(dataList.size == 1) {"CentroidPathFromGraph requires one HaplotypeGraph as input."}

        val writeFasta = fastaName() != null
        val writeBed = bedfileName() != null
        val myGraph = dataList[0].data as HaplotypeGraph

        //for each reference range in the graph get the hapid closest to the center as determined by kmer distance
        val nodeList = myGraph.referenceRangeList().map { centralHaplotypeId(myGraph, it) }.filterNotNull()
        val hapidList = nodeList.map { it.id()}

        //add the path to the db
        if (writeToDB()) {
            val pathBytes = DBLoadingUtils.encodePathsFromIntArray(hapidList)
            val methodDetail = this.pluginParameters()
            val genoLineName = pathName()

            val phgDb = PHGdbAccess(DBLoadingUtils.connection(false))
            phgDb.putPathsData(methodName, methodDetail, genoLineName, null, pathBytes, isTestMethod())
            phgDb.close()
        }

        if (writeFasta) writeFastaFile(nodeList)

        if (writeBed) writeBedFile(nodeList)

        return null
    }

    /**
     * This method calculates the center of a distance matrix computed using kmer distance then finds the closest haplotype
     */
    private fun centralHaplotypeId(graph: HaplotypeGraph, refrange: ReferenceRange): HaplotypeNode? {
        val nodes = graph.nodes(refrange)
        val nodeToKmerMap = nodes.associateWith { node -> computeKmerCounts(node.haplotypeSequence().sequence(), kmerSize()) }

        //compute the centroid
        //kmersScored is just a set of all kmers (Strings)
        val kmersScored = nodeToKmerMap.values.map { it.keys }.flatten().toSet()

        //the centroid is a map of kmer -> mean score for that kmer
        val centroid = kmersScored.associateWith { kmer ->  averageKmerScore(kmer, nodeToKmerMap.values)}

        //find the distance of each node to the centroid and return the HaplotypeNode closest to it
        return nodes.minByOrNull { node -> computeKmerEuclideanDistToCentroid(centroid, nodeToKmerMap[node]!!, 1.0) }
    }

    private fun averageKmerScore(kmer: String, kmerMaps: Collection<Map<String,Int>>): Double {
        return kmerMaps.map { kmap -> (kmap[kmer] ?: 0).toDouble() }.average()
    }

    private fun writeFastaFile(nodeList: List<HaplotypeNode>) {
        //output the path as a fasta
        //sort the HaplotypeNodes by Reference Range first
        //then concatenate the sequence for each chromosome and write it out
        val chromToNodeMap = nodeList.groupBy { it.referenceRange().chromosome() }
        val chromosomes = chromToNodeMap.keys.sorted()

        PrintWriter(Utils.getBufferedWriter(fastaName())).use { pw ->
            for (chr in chromosomes) {
                val sortedNodes = chromToNodeMap[chr]!!.sortedBy { it.referenceRange() }
                val sequence = sortedNodes.map { it.haplotypeSequence().sequence() }.joinToString("")
                pw.println(">${chr.name}")
                pw.println(sequence)
            }
        }

    }

    private fun writeBedFile(allNodes: List<HaplotypeNode>) {
        //build a map of ReferenceRange -> HaplotypeNode
        //This works because there will be at most one HaplotypeNode per ReferenceRange
        val refrangeToNodeMap = allNodes.associateBy { node -> node.referenceRange() }

        //for each reference range write the cumulative bp coordinates
        //bedfile coordinates are zero based, (inclusive,excluseive)
        //bedfile columns are chrom, chromStart, chromEnd, name

        PrintWriter(Utils.getBufferedWriter(bedfileName())).use { pw ->
            var currentChromName = ""
            var position = 0
            //get sorted list of ReferenceRanges
            val sortedRefranges = refrangeToNodeMap.keys.toSortedSet()

            for (refrange in sortedRefranges) {
                val chromName = refrange.chromosome().name
                if (currentChromName != chromName) {
                    //0 is the first position for each chromosome
                    position = 0
                    currentChromName = chromName
                }
                val myNode = refrangeToNodeMap[refrange]
                if (myNode != null && myNode.id() != -1  ) {  //skip missing ReferenceRanges
                    val myLength = myNode.haplotypeSequence().length()

                    pw.println("$chromName\t${position}\t${position + myLength}\tReferenceRange ${refrange.id()}")
                    position += myLength
                }

            }
        }
    }

    override fun pluginDescription(): String {
        return "CentroidPathFromGraphPlugin finds the node closest to the centroid " +
                "for each reference range. Node positions and distances are based on kmer distance. " +
                "It will store the haplotype ids as a path in the PHG db, output a fasta file of concatenated centroid " +
                "node sequences for each chromosome, and output a bed file of reference range coordinates " +
                "based on the centroid sequences as a reference."
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "Centroid"
    }

    override fun getToolTipText(): String {
        return "Find the centroid path"
    }

    // The following getters and setters were auto-generated.
    // Please use this method to re-generate.
    //
    // fun main(args: Array<String>) {
    //     GeneratePluginCode.generateKotlin(CentroidPathFromGraphPlugin::class.java)
    // }

    /**
     * The name to give this path in the database
     *
     * @return Path Name
     */
    fun pathName(): String {
        return pathName.value()
    }

    /**
     * Set Path Name. The name to give this path in the database
     *
     * @param value Path Name
     *
     * @return this plugin
     */
    fun pathName(value: String): CentroidPathFromGraphPlugin {
        pathName = PluginParameter(pathName, value)
        return this
    }

    /**
     * Should the resulting path be written to the PHG DB?
     * Specify the database connection parameters using -configParameters.
     *
     * @return Write To D B
     */
    fun writeToDB(): Boolean {
        return writeToDB.value()
    }

    /**
     * Set Write To D B. Should the resulting path be written
     * to the PHG DB? Specify the database connection parameters
     * using -configParameters.
     *
     * @param value Write To D B
     *
     * @return this plugin
     */
    fun writeToDB(value: Boolean): CentroidPathFromGraphPlugin {
        writeToDB = PluginParameter(writeToDB, value)
        return this
    }

    /**
     * The name and path to which the fasta file well be written
     *
     * @return Fasta Name
     */
    fun fastaName(): String? {
        return fastaName.value()
    }

    /**
     * Set Fasta Name. The name and path to which the fasta
     * file well be written
     *
     * @param value Fasta Name
     *
     * @return this plugin
     */
    fun fastaName(value: String): CentroidPathFromGraphPlugin {
        fastaName = PluginParameter(fastaName, value)
        return this
    }

    /**
     * The name and path to which a bedfile will be written.
     * The bedfile will contain the reference range start
     * and end coordinates for a centroid reference sequence.
     *
     * @return Bedfile Name
     */
    fun bedfileName(): String? {
        return bedfileName.value()
    }

    /**
     * Set Bedfile Name. The name and path to which a bedfile
     * will be written. The bedfile will contain the reference
     * range start and end coordinates for a centroid reference
     * sequence.
     *
     * @param value Bedfile Name
     *
     * @return this plugin
     */
    fun bedfileName(value: String): CentroidPathFromGraphPlugin {
        bedfileName = PluginParameter(bedfileName, value)
        return this
    }

    /**
     * Kmer size
     *
     * @return Kmer Size
     */
    fun kmerSize(): Int {
        return kmerSize.value()
    }

    /**
     * Set Kmer Size. Kmer size
     *
     * @param value Kmer Size
     *
     * @return this plugin
     */
    fun kmerSize(value: Int): CentroidPathFromGraphPlugin {
        kmerSize = PluginParameter(kmerSize, value)
        return this
    }

    /**
     * Indication if the data is to be loaded against a test
     * method. Data loaded with test methods are not cached
     * with the PHG ktor server
     *
     * @return Is Test Method
     */
    fun isTestMethod(): Boolean {
        return isTestMethod.value()
    }

    /**
     * Set Is Test Method. Indication if the data is to be
     * loaded against a test method. Data loaded with test
     * methods are not cached with the PHG ktor server
     *
     * @param value Is Test Method
     *
     * @return this plugin
     */
    fun isTestMethod(value: Boolean): CentroidPathFromGraphPlugin {
        isTestMethod = PluginParameter<Boolean>(isTestMethod, value)
        return this
    }


}
