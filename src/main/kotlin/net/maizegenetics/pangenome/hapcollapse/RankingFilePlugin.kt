package net.maizegenetics.pangenome.hapcollapse

import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.LoggingUtils
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import javax.swing.ImageIcon
import kotlin.streams.toList

class RankingFilePlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(RankingFilePlugin::class.java)

    private var output = PluginParameter.Builder("output", null, String::class.java)
            .description("Output Ranking File.")
            .outFile()
            .required(true)
            .build()

    override fun processData(input: DataSet): DataSet? {

        val temp = input.getDataOfType(HaplotypeGraph::class.java)
        require(temp.size == 1) { "RankingFilePlugin: processData: must input one HaplotypeGraph" }
        val graph = temp[0].data as HaplotypeGraph

        Utils.getBufferedWriter(output()).use { writer ->

            var rank = 1

            graph.nodeStream().toList()
                    .map { it.taxaList() }
                    .flatten()
                    .map { it.name }
                    .groupingBy { it }
                    .eachCount()
                    .toList()
                    .sortedBy { (_, value) -> value }
                    .forEach { writer.write("${it.first}\t${rank++}\n") }

        }

        return null

    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "Create Ranking File for Concensus"
    }

    override fun getToolTipText(): String {
        return "Create Ranking File for Concensus"
    }

    /**
     * Output Ranking File.
     *
     * @return Output
     */
    fun output(): String {
        return output.value()
    }

    /**
     * Set Output. Output Ranking File.
     *
     * @param value Output
     *
     * @return this plugin
     */
    fun output(value: String): RankingFilePlugin {
        output = PluginParameter(output, value)
        return this
    }

}

fun main() {
    LoggingUtils.setupDebugLogging()
    val config = "/Users/tmc46/phg/configs/configSmallSeqTest.txt"
    val graph = HaplotypeGraphBuilderPlugin(null, false)
            .configFile(config)
            .methods("mummer4")
            .build()
    RankingFilePlugin()
            .output("/Users/tmc46/git/practicalhaplotypegraph/rank.txt")
            .performFunction(DataSet.getDataSet(graph))
}
