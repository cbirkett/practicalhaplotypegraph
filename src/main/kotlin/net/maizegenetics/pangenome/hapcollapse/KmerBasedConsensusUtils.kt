@file:JvmName("KmerBasedConsensusUtils")

package net.maizegenetics.pangenome.hapcollapse

import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.taxa.TaxaListBuilder
import net.maizegenetics.taxa.distance.DistanceMatrix
import net.maizegenetics.taxa.distance.DistanceMatrixBuilder
import org.apache.log4j.Logger

private val myLogger = Logger.getLogger("net.maizegenetics.pangenome.hapcollapse.KmerBasedConsensusUtils")

/**
 * How to calculate the distance between kmer counts
 */
enum class DistanceCalculation {
    Manhattan, Euclidean
}

/**
 * This returns the distance matrix for the taxa in the given reference range using the method of distance calculation
 */
fun kmerDistanceMatrix(referenceRange: ReferenceRange, nodes: List<HaplotypeNode>, kmerSize: Int, calculationType: DistanceCalculation): DistanceMatrix {

    if (referenceRange.id() % 1000 == 0) {
        myLogger.debug("Processing RefRange:${referenceRange.intervalString()}")
    }

    val kmerMaps = nodes.map { node -> computeKmerCounts(node.haplotypeSequence().sequence(), kmerSize) }
    //loop through pairs of kmermaps and compute the distance and write to the distance matrix

    val taxaWithInfo = TaxaListBuilder().addAll(nodes.map { node -> node.taxaList().map { taxon -> taxon.name }.joinToString("_") }).build()

    val dmBuilder = DistanceMatrixBuilder.getInstance(taxaWithInfo)

    val computeKmerDistance = when (calculationType) {
        DistanceCalculation.Manhattan -> ::computeKmerManhattanDist
        DistanceCalculation.Euclidean -> ::computeKmerEuclideanDist
    }

    val rangeLength = (referenceRange.end() - referenceRange.start() + 1).toDouble()
    for (i in 0 until kmerMaps.size - 1) {
        for (j in i + 1 until kmerMaps.size) {
            dmBuilder.set(i, j, computeKmerDistance(kmerMaps[i], kmerMaps[j], rangeLength))
        }
    }

    return dmBuilder.build()

}

/**
 * Counts the Kmers (of kmerSize) for the given Haplotype
 */
fun computeKmerCounts(sequence: String, kmerSize: Int): Map<String, Int> {
    return sequence.windowedSequence(kmerSize, 1, false)
            .filter { !it.contains("N") }
            .groupingBy { it }
            .eachCount()
}

/**
 * Computes Kmer distance based on Euclidean formula
 */
fun computeKmerEuclideanDist(kmerMap1: Map<String, Int>, kmerMap2: Map<String, Int>, divisor : Double): Double {
    //Get the union of the kmers
    val kmersScored = kmerMap1.keys.union(kmerMap2.keys)

    return Math.sqrt(kmersScored.map {
        val kmerCount1 = kmerMap1[it] ?: 0
        val kmerCount2 = kmerMap2[it] ?: 0
        (kmerCount1 - kmerCount2) * (kmerCount1 - kmerCount2)
    }.sum().toDouble()) / divisor
}

/**
 * Computes Kmer distance based on Euclidean formula for distance to a centroid.
 * Similar to distance calculation between two kmer maps, except the centroid has doubles for scores.
 */
fun computeKmerEuclideanDistToCentroid(centroid: Map<String, Double>, kmerMap: Map<String, Int>, divisor : Double): Double {
    //Centroid will have scores for all kmers
    val kmersScored = centroid.keys

    return Math.sqrt(kmersScored.map {
        val kmerCount1 = centroid[it] ?: 0.0
        val kmerCount2 = kmerMap[it] ?: 0
        (kmerCount1 - kmerCount2) * (kmerCount1 - kmerCount2)
    }.sum()) / divisor
}

/**
 * Computes Kmer distance based on Manhattan formula
 */
fun computeKmerManhattanDist(kmerMap1: Map<String, Int>, kmerMap2: Map<String, Int>, divisor : Double): Double {
    //Get the union of the kmers
    val kmersScored = kmerMap1.keys.union(kmerMap2.keys)

    return kmersScored.map {
        val kmerCount1 = kmerMap1[it] ?: 0
        val kmerCount2 = kmerMap2[it] ?: 0
        Math.abs(kmerCount1 - kmerCount2)
    }.sum().toDouble() / divisor
}