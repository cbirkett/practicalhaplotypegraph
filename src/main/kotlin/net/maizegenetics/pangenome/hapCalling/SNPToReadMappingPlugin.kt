package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultiset
import htsjdk.variant.vcf.VCFFileReader
import net.maizegenetics.dna.map.Position
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.DirectoryCrawler
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.BufferedWriter
import java.io.File
import java.io.PrintWriter
import java.lang.IllegalStateException
import java.nio.file.Paths
import javax.swing.ImageIcon

class SNPToReadMappingPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) :
    AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(SNPToReadMappingPlugin::class.java)

    private var keyFile = PluginParameter.Builder("keyFile", null, String::class.java)
        .guiName("keyFile")
        .inFile()
        .required(true)
        .description("Name of the Keyfile to process.  Must have columns flowcell_lane and filename. In the filename column a VCF file needs to be specified.  The taxa name will be pulled from the VCF.")
        .build()

    private var indexFile = PluginParameter.Builder("indexFile", null, String::class.java)
        .guiName("Index File")
        .inFile()
        .required(true)
        .description("Name of the SNP to HapIdList index file created by IndexHaplotypesBySNPPlugin")
        .build()

    private var vcfDirectory = PluginParameter.Builder("vcfDir", null, String::class.java)
        .guiName("VCF Dir")
        .inDir()
        .required(true)
        .description("Directory of VCF files.")
        .build()

    private var readMappingOutputDir = PluginParameter.Builder("outputDir", null, String::class.java)
        .guiName("outputDir")
        .outDir()
        .required(false)
        .description("Read Mapping Output Directory for writing readmappings to files. If not supplied, read mappings will be written to the database.")
        .build()

    private var methodName = PluginParameter.Builder("methodName", null, String::class.java)
        .guiName("Method Name")
        .required(true)
        .description("Method name to be stored in the DB.")
        .build()

    private var methodDescription = PluginParameter.Builder("methodDescription", "read mapping from SNPs", String::class.java)
        .guiName("Method Description")
        .required(false)
        .description("Method description to be stored in the DB along with parameter values")
        .build()

    private var countAD = PluginParameter.Builder("countAlleleDepths", true, Boolean::class.javaObjectType)
        .guiName("Count Allele Depths")
        .required(false)
        .description("Use the Allele Depths as the counts for the Read Mappings instead of SNPs")
        .build()

    private var outputPathKeyFile = PluginParameter.Builder("outputPathKeyFile",null, String::class.java)
        .required(false)
        .outFile()
        .description("Output Key File that can be used in path finding. This is optional and will only be written if read mappings are written to the database.  If no file path is supplied or mappings are written to file, no keyfile will be written.")
        .build()

    private var isTestMethod = PluginParameter.Builder("isTestMethod", false, Boolean::class.javaObjectType)
        .description("Indication if the data is to be loaded against a test method. Data loaded with test methods are not cached with the PHG ktor server")
        .required(false).build()

    override fun processData(input: DataSet?): DataSet? {
        val columnMappingAndLines = readInKeyFile(keyFile())
        val keyFileColumnNameMap = columnMappingAndLines.first
        val keyFileLines = columnMappingAndLines.second

        //Get out the column indices as they will be consistent for the whole file
        val fileNameCol1 = keyFileColumnNameMap["filename"] ?: -1
        val flowcellCol = keyFileColumnNameMap["flowcell_lane"] ?: -1

        //Check will automatically throw an IllegalStateException if the logic fails.
        check(flowcellCol != -1) { "Error processing keyfile.  Must have flowcell_lane column." }
        check(fileNameCol1 != -1) { "Error processing keyfile.  Must have filename column." }

        val directoryFileNames = DirectoryCrawler.listPaths("glob:*{.vcf.gz,.vcf}", Paths.get(vcfDirectory()))
            .map { it.fileName.toString() }
            .toHashSet()

        val taxonFileGroupList = mutableListOf<Pair<String,String>>()
        keyFileLines.filter { isKeyEntryInDir(directoryFileNames, it, fileNameCol1, -1) }
            .forEach {
                myLogger.info("Processing record: ${it.joinToString(",")}")
                val file1 = it[fileNameCol1]
                val fileGrpName = it[flowcellCol]
                val vcfFileReader = VCFFileReader(File(vcfDirectory() + file1), false)

                val taxaForVCFFile = vcfFileReader.fileHeader.genotypeSamples
                taxaForVCFFile.forEach { taxonFileGroupList.add(Pair(it, fileGrpName)) }

                val bufferedWriters =  if (readMappingOutputDir() == null) null else taxaForVCFFile
                    .map { taxon ->
                        Pair(
                            taxon,
                            Utils.getBufferedWriter("${readMappingOutputDir()}/${taxon}_readMappings.txt")
                        )
                    }
                    .toMap()

                if (bufferedWriters != null) addHeadersToOutputFiles(bufferedWriters, fileGrpName, methodName(), methodDescription())

                verifyNoDuplicatesInKeyFile(taxaForVCFFile.map { taxon ->
                    KeyFileUniqueRecord(
                        methodName(),
                        taxon,
                        fileGrpName
                    )
                })


                //Loop through both the VCF file and the index using 2 pointers(iterators)
                //We need to do this because it only takes 98 seconds to iterate and split full wheat index and barely uses any RAM
                //If we load the index into RAM it uses 100GB+ and takes a substantial amount of time
                //Most of the time only one vcf file will be processed per worker node so we really will not be saving any time by preloading the Map

                //After verification we can start processing
                //Loop through the VCF file
                vcfFileReader.iterator().use { vcfIterator ->
                    Utils.getBufferedReader(indexFile()).use { indexIterator ->
                        indexIterator.readLine() //this is the header we can ignore
                        var currentBatchRefRangeId = ""

                        var multisets =
                            taxaForVCFFile.map { taxon -> Pair(taxon, HashMultiset.create<String>()) }.toMap()

                        if (vcfIterator.hasNext()) {
                            var currentVCFRecord = vcfIterator.next()

                            var currentIndexLine = indexIterator.readLine() //Get the first non-header line

                            var currentIndexLineParsed = parseIndexLine(currentIndexLine)
                            var currentIndexRefRangeId = currentIndexLineParsed[0]


                            if (currentIndexRefRangeId != currentBatchRefRangeId) {
                                //update currentBatchRefRangeId
                                currentBatchRefRangeId = currentIndexRefRangeId
                            }

                            var currentIndexPosition =
                                Position.of(currentIndexLineParsed[1], currentIndexLineParsed[2].toInt())
                            var currentVCFPosition = Position.of(currentVCFRecord.contig, currentVCFRecord.start)

                            //This could probably be just a while(true), but putting in the checks just to be safe
                            while (currentIndexLine != null && currentVCFRecord != null && vcfIterator.hasNext()) {
                                //parse out currentIndexLine

                                //If they are the same position
                                if (currentIndexPosition == currentVCFPosition) {
                                    //Score all the taxa in vcf
                                    val indexAlleles = arrayOf(currentIndexLineParsed[3], currentIndexLineParsed[4])
                                    taxaForVCFFile.forEachIndexed { index, taxon ->
                                        if (countAD() && currentVCFRecord.getGenotype(index).hasAD()) {
                                            val currentAD = currentVCFRecord.getGenotype(index).ad

                                            //countAD() is true, use allele depth
                                            if (currentAD[0] > 0) multisets[taxon]?.add(
                                                currentIndexLineParsed[5],
                                                currentAD[0]
                                            )
                                            if (currentAD[1] > 0) multisets[taxon]?.add(
                                                currentIndexLineParsed[6],
                                                currentAD[1]
                                            )

                                        } else {
                                            //else use the genotype with a count of three for each allele
                                            val genotype = currentVCFRecord.getGenotype(index).alleles.map { it.baseString }
                                            if (genotype[0] == genotype[1]) {
                                                if (genotype[0] == indexAlleles[0]) multisets[taxon]?.add(
                                                    currentIndexLineParsed[5],
                                                    6
                                                )
                                                else if (genotype[0] == indexAlleles[1]) multisets[taxon]?.add(
                                                    currentIndexLineParsed[6],
                                                    6
                                                )
                                            } else {
                                                if (genotype[0] == indexAlleles[0] || genotype[1] == indexAlleles[0]) multisets[taxon]?.add(
                                                    currentIndexLineParsed[5],
                                                    3
                                                )
                                                if (genotype[0] == indexAlleles[1] || genotype[1] == indexAlleles[1]) multisets[taxon]?.add(
                                                    currentIndexLineParsed[6],
                                                    3
                                                )
                                            }
                                        }
                                    }


                                    //move both currentIndexLine and vcfIterator up 1
                                    currentIndexLine = indexIterator.readLine()
                                    if (currentIndexLine == null) {
                                        break
                                    }
                                    currentIndexLineParsed = parseIndexLine(currentIndexLine)
                                    currentIndexRefRangeId = currentIndexLineParsed[0]
                                    currentIndexPosition =
                                        Position.of(currentIndexLineParsed[1], currentIndexLineParsed[2].toInt())
                                    currentVCFRecord = if (vcfIterator.hasNext()) vcfIterator.next() else break
                                    currentVCFPosition = Position.of(currentVCFRecord!!.contig, currentVCFRecord.start)
                                    if (currentIndexRefRangeId != currentBatchRefRangeId) {
                                        if (currentBatchRefRangeId != "" && bufferedWriters != null) {
                                            // Export the maps
                                            exportReadMappingsForTaxon(multisets, bufferedWriters)

                                            //Reset the maps
                                            multisets = taxaForVCFFile.map { taxon ->
                                                Pair(
                                                    taxon,
                                                    HashMultiset.create<String>()
                                                )
                                            }.toMap()

                                        }
                                        //update currentBatchRefRangeId
                                        currentBatchRefRangeId = currentIndexRefRangeId
                                    }
                                } else if (currentVCFPosition > currentIndexPosition) {
                                    //move currentIndexLine 1 position forward
                                    currentIndexLine = indexIterator.readLine()
                                    if (currentIndexLine == null) {
                                        break
                                    }
                                    currentIndexLineParsed = parseIndexLine(currentIndexLine)
                                    currentIndexRefRangeId = currentIndexLineParsed[0]
                                    currentIndexPosition =
                                        Position.of(currentIndexLineParsed[1], currentIndexLineParsed[2].toInt())

                                    if (currentIndexRefRangeId != currentBatchRefRangeId && bufferedWriters != null) {
                                        if (currentBatchRefRangeId != "") {
                                            // Export the maps
                                            exportReadMappingsForTaxon(multisets, bufferedWriters)

                                            //Reset the maps
                                            multisets = taxaForVCFFile.map { taxon ->
                                                Pair(
                                                    taxon,
                                                    HashMultiset.create<String>()
                                                )
                                            }.toMap()

                                        }
                                        //update currentBatchRefRangeId
                                        currentBatchRefRangeId = currentIndexRefRangeId
                                    }


                                } else if (currentIndexPosition > currentVCFPosition) {
                                    //else if vcfIterator chr < currentIndexLine chr || (vcfIterator chr == currentIndexLine chr && vcfIterator pos < currentIndexLine pos )
                                    //move vcfIterator 1 position forward
                                    currentVCFRecord = if (vcfIterator.hasNext()) vcfIterator.next() else break
                                    currentVCFPosition = Position.of(currentVCFRecord!!.contig, currentVCFRecord.start)
                                } else {
                                    throw IllegalStateException("Unable to compare the index and vcf positions. Check the files manually to make sure Chromosome and Position is consistent")
                                }
                            }

                        }
                        if (bufferedWriters == null) storeReadMappingsInDB(multisets, fileGrpName)
                    }
                }

                //Closing out the writers
                if (bufferedWriters != null) bufferedWriters.values.forEach { writer -> writer?.close() }

            }
        writeKeyFileIfRequested(taxonFileGroupList)
        return null
    }

    /**
     * Simple function to parse the index lines quickly by using indexOf instead of String.split()
     * When I profiled this, split was taking a large amount of time.  So to limit it, this was coded up.
     * Unsure of overall efficiency however and could likely be reverted to indexLine.split()
     */
    fun parseIndexLine(indexLine: String): List<String> {
        val firstTabIndex = indexLine!!.indexOf("\t")
        val secondTabIndex = indexLine!!.indexOf("\t", firstTabIndex + 1)
        val thirdTabIndex = indexLine!!.indexOf("\t", secondTabIndex + 1)
        val fourthTabIndex = indexLine!!.indexOf("\t", thirdTabIndex + 1)
        val fifthTabIndex = indexLine!!.indexOf("\t", fourthTabIndex + 1)
        val sixthTabIndex = indexLine!!.indexOf("\t", fifthTabIndex + 1)


        val refRangeId = indexLine!!.substring(0, firstTabIndex)
        val chr = indexLine!!.substring(firstTabIndex + 1, secondTabIndex)
        val pos = indexLine!!.substring(secondTabIndex + 1, thirdTabIndex)
        val refAllele = indexLine!!.substring(thirdTabIndex + 1, fourthTabIndex)
        val altAllele = indexLine!!.substring(fourthTabIndex + 1, fifthTabIndex)
        val refHapIds = indexLine!!.substring(fifthTabIndex + 1, sixthTabIndex)
        val altHapIds = indexLine!!.substring(sixthTabIndex + 1)

        return listOf(refRangeId, chr, pos, refAllele, altAllele, refHapIds, altHapIds)
    }

    fun addHeadersToOutputFiles(
        writers: Map<String, BufferedWriter>,
        fileGroupName: String,
        methodName: String,
        methodDescription: String?
    ) {
        writers.keys.forEach { taxon ->
            val output = writers[taxon]
            output?.write("#line_name=${taxon}\n")
            output?.write("#file_group_name=${fileGroupName}\n")
            output?.write("#method_name=${methodName}\n")
            output?.write("#method_description=${methodDescription}\n")
            output?.write("HapIds\tcount\n")
        }
    }

    fun exportReadMappingsForTaxon(
        multisets: Map<String, HashMultiset<String>>,
        bufferedWriters: Map<String, BufferedWriter>
    ) {
        multisets.keys.forEach { taxon ->
            val currentMultiset = multisets[taxon]
            val currentBufferedWriter = bufferedWriters[taxon]
            if (currentMultiset?.isNotEmpty() == true) {
                val mapToOutput =
                    currentMultiset?.elementSet()?.map { element -> Pair(element, currentMultiset?.count(element)) }
                        ?.toMap()
                mapToOutput?.keys
                    ?.filter { it != "" } //Should not have any missing alleles, but to be safe, we check here as well.
                    ?.forEach { currentBufferedWriter?.write("${it}\t${mapToOutput[it]}\n") }
            }
        }
    }

    fun storeReadMappingsInDB(
        multisets: Map<String, HashMultiset<String>>,
        fileGroupName: String

    ) {
        //a hapIdMap is a map of List<Int>(hapids) -> count of hapid set
        //the input is a multimap of taxon -> a multiset of Strings that are a comma-separated list of hapids
        //the method encodeHapIdMapping(hapIdMapping : Map<List<Int>,Int>) expects a map of hapid list (Int) -> count
        //so the task here is to convert the multiset for each taxon into a hapIdMapping expected by encodeHapIdMapping
        //then to call phg.putReadMappingData to put that in the database
        val phg = PHGdbAccess(DBLoadingUtils.connection(false))

        multisets.keys.forEach { taxon->
            val hapIdMapping = HashMap<List<Int>, Int>()
            println(message = "taxon: $taxon")
            val currentMultiset = multisets[taxon]
            if (currentMultiset?.isNotEmpty() == true) {
                for (hapidStr in currentMultiset.elementSet()) {
                    if (hapidStr?.isNotEmpty() == true) {
                        val hapidList = hapidStr.split(',').map { it.toInt() }.toList()
                        hapIdMapping.put(hapidList, currentMultiset.count(hapidStr))
                    }
                }
                val encodedHapids = encodeHapIdMapping(hapIdMapping)
                println("at putReadMappingData: taxon= $taxon, fileGroupName = $fileGroupName, bytes = ${encodedHapids?.size ?: 0}")
                phg.putReadMappingData(
                    methodName(),
                    this.pluginParameters(),
                    taxon,
                    fileGroupName,
                    encodedHapids,
                    isTestMethod(),
                    -1
                )
            }
        }

        phg.close()
    }

    fun writeKeyFileIfRequested(taxaGroupPairs: List<Pair<String,String>>) {
        val keyfile = outputPathKeyFile()

        if (keyfile != null) {
            val phg = PHGdbAccess(DBLoadingUtils.connection(false))
            val taxaGroupMap = taxaGroupPairs.groupBy({it.first},{it.second})
            val sortedTaxa = taxaGroupMap.keys.toSortedSet()

            // Update the read mapping hash: PHGdbAccess:readMapToReadMappingIDMap
            // This hash is used by getReadMappingId()  It was not updated when
            // read mappings were added/deleted.
            phg.updateReadMappingHash()
            PrintWriter(keyfile).use { pw ->
                pw.println("SampleName\tReadMappingIds\tLikelyParents")
                sortedTaxa.forEach { taxon ->
                    val idString = taxaGroupMap[taxon]!!
                        .map { group -> phg.getReadMappingId(taxon, methodName(), group).toString()}
                        .joinToString(",")
                    pw.println("$taxon\t$idString\t")
                }
            }
            phg.close()
        }

    }

    override fun getIcon(): ImageIcon? {
        val imageURL = FastqToMappingPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "SNPToHaplotypePlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to Create Read Mappings given a SNP file"
    }

    /**
     * Name of the Keyfile to process.  Must have columns
     * cultivar, flowcell_lane, filename, and PlateID. In
     * the filename column a GVCF file needs to be specified
     *
     * @return keyFile
     */
    fun keyFile(): String {
        return keyFile.value()
    }

    /**
     * Set keyFile. Name of the Keyfile to process.  Must
     * have columns cultivar, flowcell_lane, filename, and
     * PlateID. In the filename column a GVCF file needs to
     * be specified
     *
     * @param value keyFile
     *
     * @return this plugin
     */
    fun keyFile(value: String): SNPToReadMappingPlugin {
        keyFile = PluginParameter<String>(keyFile, value)
        return this
    }

    /**
     * Name of the SNP to HapIdList index file created by
     * IndexHaplotypesBySNPPlugin
     *
     * @return Index File
     */
    fun indexFile(): String {
        return indexFile.value()
    }

    /**
     * Set Index File. Name of the SNP to HapIdList index
     * file created by IndexHaplotypesBySNPPlugin
     *
     * @param value Index File
     *
     * @return this plugin
     */
    fun indexFile(value: String): SNPToReadMappingPlugin {
        indexFile = PluginParameter<String>(indexFile, value)
        return this
    }

    /**
     * Directory ofGVCF files.
     *
     * @return VCF Dir
     */
    fun vcfDirectory(): String {
        return vcfDirectory.value()
    }

    /**
     * Set VCF Dir. Directory ofGVCF files.
     *
     * @param value VCF Dir
     *
     * @return this plugin
     */
    fun vcfDirectory(value: String): SNPToReadMappingPlugin {
        vcfDirectory = PluginParameter<String>(vcfDirectory, value)
        return this
    }

    /**
     * Debug Output Directory.
     *
     * @return outputDir
     */
    fun readMappingOutputDir(): String? {
        return readMappingOutputDir.value()
    }

    /**
     * Set outputDir. Debug Output Directory.
     *
     * @param value outputDir
     *
     * @return this plugin
     */
    fun readMappingOutputDir(value: String): SNPToReadMappingPlugin {
        readMappingOutputDir = PluginParameter<String>(readMappingOutputDir, value)
        return this
    }

    /**
     * Method name to be stored in the DB.
     *
     * @return Method Name
     */
    fun methodName(): String {
        return methodName.value()
    }

    /**
     * Set Method Name. Method name to be stored in the DB.
     *
     * @param value Method Name
     *
     * @return this plugin
     */
    fun methodName(value: String): SNPToReadMappingPlugin {
        methodName = PluginParameter<String>(methodName, value)
        return this
    }

    /**
     * Method description to be stored in the DB.
     *
     * @return Method Description
     */
    fun methodDescription(): String? {
        return methodDescription.value()
    }

    /**
     * Set Method Description. Method description to be stored
     * in the DB.
     *
     * @param value Method Description
     *
     * @return this plugin
     */
    fun methodDescription(value: String): SNPToReadMappingPlugin {
        methodDescription = PluginParameter<String>(methodDescription, value)
        return this
    }

    /**
     * Use the Allele Depths as the counts for the Read Mappings
     * instead of SNPs
     *
     * @return Count Allele Depths
     */
    fun countAD(): Boolean {
        return countAD.value()
    }

    /**
     * Set Count Allele Depths. Use the Allele Depths as the
     * counts for the Read Mappings instead of SNPs
     *
     * @param value Count Allele Depths
     *
     * @return this plugin
     */
    fun countAD(value: Boolean): SNPToReadMappingPlugin {
        countAD = PluginParameter<Boolean>(countAD, value)
        return this
    }

    /**
     * Output Key File that can be used in path finding. This
     * is optional and will only be written if read mappings
     * are written to the database.  If no file path is supplied
     * or mappings are written to file, no keyfile will be
     * written.
     *
     * @return Output Key File
     */
    fun outputPathKeyFile(): String? {
        return outputPathKeyFile.value()
    }

    /**
     * Set Output Key File. Output Key File that can be used
     * in path finding. This is optional and will only be
     * written if read mappings are written to the database.
     *  If no file path is supplied or mappings are written
     * to file, no keyfile will be written.
     *
     * @param value Output Key File
     *
     * @return this plugin
     */
    fun outputPathKeyFile(value: String): SNPToReadMappingPlugin {
        outputPathKeyFile = PluginParameter<String>(outputPathKeyFile, value)
        return this
    }

    /**
     * Indication if the data is to be loaded against a test
     * method. Data loaded with test methods are not cached
     * with the PHG ktor server
     *
     * @return Is Test Method
     */
    fun isTestMethod(): Boolean {
        return isTestMethod.value()
    }

    /**
     * Set Is Test Method. Indication if the data is to be
     * loaded against a test method. Data loaded with test
     * methods are not cached with the PHG ktor server
     *
     * @param value Is Test Method
     *
     * @return this plugin
     */
    fun isTestMethod(value: Boolean): SNPToReadMappingPlugin {
        isTestMethod = PluginParameter<Boolean>(isTestMethod, value)
        return this
    }


}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(SNPToReadMappingPlugin::class.java)
}