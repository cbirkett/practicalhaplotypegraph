package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.Multiset
import com.google.common.collect.TreeMultiset
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.HaplotypeGraphStreamBuilderPlugin
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.taxa.TaxaList
import net.maizegenetics.taxa.Taxon
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import java.util.stream.Collectors
import javax.swing.ImageIcon

class VerifyPathsPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = Logger.getLogger(VerifyPathsPlugin::class.java)

    private var configFile = PluginParameter.Builder("configFile", null, String::class.java)
            .description("Database configuration file")
            .guiName("Config File")
            .required(true)
            .inFile()
            .build()

    private var myMethods = PluginParameter.Builder("methods", null, String::class.java)
            .required(true)
            .description("Pairs of methods (haplotype method name and range group method name). Method pair separated by a comma, and pairs separated by semicolon. The range group is optional \n" + "Usage: <haplotype method name1>,<range group name1>;<haplotype method name2>,<range group name2>;<haplotype method name3>")
            .build()

    private var pathFile = PluginParameter.Builder("pathFile", null, String::class.java)
            .description("File Holding the path")
            .guiName("Path File")
            .required(true)
            .inFile()
            .build()

    private var targetTaxon = PluginParameter.Builder("targetTaxon", null, String::class.java)
            .description("Taxon which we think this path is from")
            .guiName("Target Taxon")
            .required(true)
            .build()


    override fun processData(input: DataSet?): DataSet? {
//        var graph = HaplotypeGraphStreamBuilderPlugin(null, false)
//                .configFile(configFile())
//                .methods(methods())
//                .includeSequences(false)
//                .includeVariantContexts(false)
//                .build()

        var graph = HaplotypeGraphBuilderPlugin(null,false)
                .configFile(configFile())
                .methods(methods())
                .includeVariantContexts(false)
                .includeSequences(false)
                .build()


        val idToRefRangeMap = mutableMapOf<Int,ReferenceRange>()
        for(refRange in graph.referenceRanges()) {
            for(node in graph.nodes(refRange)) {
                idToRefRangeMap[node.id()] = refRange
            }
        }


        val taxonSplit = targetTaxon().split(",")

        //Read in the path file
        val pathNodeIds = Utils.getBufferedReader(pathFile()).lines()
                .filter { !it.startsWith("#") }
                .map { it.toInt() }
                .collect(Collectors.toList())
        var nodeNoW22Set = mutableSetOf<Int>()
        for(refRange in graph.referenceRanges()) {
            val nodesAtRefRange = graph.nodes(refRange)
            var w22Present = false
            for(node in nodesAtRefRange) {
                for(taxon in taxonSplit) {
                    if(node.taxaList().contains(Taxon(taxon))) {
                        w22Present=true
                        break
                    }
                }
//                if(node.taxaList().contains(Taxon(targetTaxon()))) {
//                    w22Present=true
//                    break
//                }
            }

            if(!w22Present) {
                for(node in nodesAtRefRange) {
                    nodeNoW22Set.add(node.id())
                }
            }

        }
        //stream through the graph only keeping nodes that have W22 in taxa
        val W22NodesInGraph:List<Int> = graph.nodeStream().filter {
            var hasW22 = false
            for(taxon in taxonSplit) {
                if(it.taxaList().contains(Taxon(taxon))) {
                    hasW22 = true
                    break
                }
            }
             hasW22
        }
                .map{it.id()}
                .collect(Collectors.toList())

        val nodeToRefRangeStartMap : Map<Int,Int> = graph.nodeStream()
                .filter { t: HaplotypeNode? -> t != null }
                .collect(Collectors.toList())
                .map { it.id() to it.referenceRange().start() }
                .toMap()




//        val nodeIdToTaxonList:Map<Int, TaxaList> = graph.nodeStream().collect(Collectors.toList())
//                .collect(Collectors.toMap(node -> node.id(), node -> taxaList().stream.collect(Collectors.joining(","))))

//        val W22NodesInGraph = graph.flatMap { it.second.stream() }
//                .filter { it.taxaList().contains(Taxon(targetTaxon())) }
//                .map { it.id() }
//                .collect(Collectors.toList())
        //if node in path is in W22 nodes, count, if not increment error
        var correct = 0
        var error = 0
        var noW22Error = 0
        var correctInFirst130Mb = 0
        var errorInFirst130MB = 0
        val totalCountPerChrMultiset : Multiset<Int> = TreeMultiset.create()
        val correctCountPerChrMultiset: Multiset<Int> = TreeMultiset.create()
        for (pathId in pathNodeIds) {
            val nodeStart = nodeToRefRangeStartMap.getOrDefault(pathId,-1)
            totalCountPerChrMultiset.add(idToRefRangeMap[pathId]?.chromosome()?.chromosomeNumber)
            if (W22NodesInGraph.contains(pathId)) {
                correct++
                correctCountPerChrMultiset.add(idToRefRangeMap[pathId]?.chromosome()?.chromosomeNumber)
               if(nodeStart in 0..100000000) {
                    correctInFirst130Mb++
                }

            } else {
                error++
                if(nodeStart in 0..100000000) {
                    errorInFirst130MB++
                }
                if(nodeNoW22Set.contains(pathId)) {
                    noW22Error++
                }
            }
        }

        println("Num Correct:$correct\nNumError:$error\nErrorRate:${((error.toDouble()) / (correct + error))}\nNoW22Count:$noW22Error")

        println("NumCorrectInFirst130MB $correctInFirst130Mb\nErrorInFirst130Mb $errorInFirst130MB\nErrorRate:${((errorInFirst130MB.toDouble())/(correctInFirst130Mb+errorInFirst130MB))}")

        for(chrom in totalCountPerChrMultiset.elementSet()) {
            println("Chr$chrom: Num Scored:${totalCountPerChrMultiset.count(chrom)} Num Correct:${correctCountPerChrMultiset.count(chrom)}")
            println("\tError Rate:${1-(correctCountPerChrMultiset.count(chrom).toDouble()/totalCountPerChrMultiset.count(chrom))}")
        }


        return null
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = IndexKmerByHammingPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "Index Kmers"
    }

    override fun getToolTipText(): String {
        return "Create kmer index for haplotype counts"
    }

    /**
     * Database configuration file
     *
     * @return Config File
     */
    fun configFile(): String {
        return configFile.value()
    }

    /**
     * Set Config File. Database configuration file
     *
     * @param value Config File
     *
     * @return this plugin
     */
    fun configFile(value: String): VerifyPathsPlugin {
        configFile = PluginParameter(configFile, value)
        return this
    }

    /**
     * Pairs of methods (haplotype method name and range group
     * method name). Method pair separated by a comma, and
     * pairs separated by semicolon. The range group is optional
     *
     * Usage: <haplotype method name1>,<range group name1>;<haplotype method name2>,<range group name2>;<haplotype method name3>
     *
     * @return Methods
    </haplotype></range></haplotype></range></haplotype> */
    fun methods(): String {
        return myMethods.value()
    }

    /**
     * Set Methods. Pairs of methods (haplotype method name
     * and range group method name). Method pair separated
     * by a comma, and pairs separated by semicolon. The range
     * group is optional
     * Usage: <haplotype method name1>,<range group name1>;<haplotype method name2>,<range group name2>;<haplotype method name3>
     *
     * @param value Methods
     *
     * @return this plugin
    </haplotype></range></haplotype></range></haplotype> */
    fun methods(value: String): VerifyPathsPlugin {
        myMethods = PluginParameter(myMethods, value)
        return this
    }

    /**
     * File Holding the path
     *
     * @return Path File
     */
    fun pathFile(): String {
        return pathFile.value()
    }

    /**
     * Set Path File. File Holding the path
     *
     * @param value Path File
     *
     * @return this plugin
     */
    fun pathFile(value: String): VerifyPathsPlugin {
        pathFile = PluginParameter(pathFile, value)
        return this
    }

    /**
     * Taxon which we think this path is from
     *
     * @return Target Taxon
     */
    fun targetTaxon(): String {
        return targetTaxon.value()
    }

    /**
     * Set Target Taxon. Taxon which we think this path is
     * from
     *
     * @param value Target Taxon
     *
     * @return this plugin
     */
    fun targetTaxon(value: String): VerifyPathsPlugin {
        targetTaxon = PluginParameter(targetTaxon, value)
        return this
    }

}

fun main(args: Array<String>) {
    GeneratePluginCode.generate(VerifyPathsPlugin::class.java)
}