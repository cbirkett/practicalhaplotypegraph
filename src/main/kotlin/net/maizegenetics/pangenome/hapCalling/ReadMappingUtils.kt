@file:JvmName("ReadMappingUtils")

package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.GZipCompression
import net.maizegenetics.util.Utils
import java.util.*

/**
 * Function to encode the HapId Map into a byte array and compress it.
 */
fun encodeHapIdMapping(hapIdMapping : Map<List<Int>,Int>) : ByteArray? {
    val nonStandardEncoding = hapIdMapping.keys.map{
        val counts = hapIdMapping[it]
        "${it.joinToString(",")}\t${counts}"
    }.joinToString("\n")
    return GZipCompression.compress(nonStandardEncoding)
}

/**
 * Encodes haplotypeSet counts by first translating each haplotypeSet into a BitSet.
 * [hapIdMapping] maps a list of haplotype ids to their counts.
 * [hapIdToRefRangeMap] is a map of haplotype id to ReferenceRange.
 * [refRangeToHapIdMap] maps refRangeId to a map of haplotype id to its index,
 * where the index represents the order of the haplotype ids in that ReferenceRange,
 * The haplotype ids in each reference range are indexed in ascending order.
 */
fun encodeHapIdsAsBitSet(hapIdMapping: Map<List<Int>, Int>, hapIdToRefRangeMap : Map<Int, ReferenceRange>,
                         refRangeToHapIdMap : Map<Int, Map<Int,Int>>) : ByteArray? {

    val recordSeparator = "\n"
    val rangeidSeparator = ":"
    val setcountSeparator = ";"
    val numberSeparator = ","

    //order hapid sets by refrange to avoid repeating refrange id for each set
    val refRangeToHapidSetMap = hapIdMapping.entries.groupBy { (hapset, _) -> hapIdToRefRangeMap[hapset[0]]?.id() ?: -1 }
    //codes the HapidSetCounts for each ReferenceRange id as
    // refRangeId:count, long, long, ...;count, long, long, ...;...
    val packedString = refRangeToHapidSetMap.keys.map { rangeid ->
        //setCounts is a list of Map.Entry<List<haplotype id>, count> [HapIdSetCounts]
        val setCounts = refRangeToHapidSetMap[rangeid]!!
        //hapidIndex is the hapid to index map for this ReferenceRange
        val hapidIndex = refRangeToHapIdMap[rangeid]?: mapOf()
        val setCountString = setCounts.map { (hapset, count) ->
            val bitSet = BitSet(hapidIndex.size)
            for (hapid in hapset) bitSet.set(hapidIndex[hapid]!!)
            val longArray = bitSet.toLongArray()
            "$count$numberSeparator${longArray.joinToString(numberSeparator)}"
        }.joinToString(setcountSeparator)
        "$rangeid$rangeidSeparator$setCountString"
    }.joinToString(recordSeparator)

    return GZipCompression.compress(packedString)
}

fun bitSetToIntList(bitSet: BitSet) : List<Int> {
    val indexList = mutableListOf<Int>()
    var ndx = bitSet.nextSetBit(0)
    while (ndx > -1) {
        indexList.add(ndx)
        ndx = bitSet.nextSetBit(ndx + 1)
    }
    return indexList
}

/**
 * Function to decode the HapId byte array into the Mapping to be used by Path Finding.
 */
fun decodeHapIdMapping(encodedReadMapping : ByteArray) : Map<List<Int>,Int> {
    val unzippedEncoded = GZipCompression.decompress(encodedReadMapping)
    //if (unzippedEncoded.startsWith("bitset")) return decodeHapIdBitSets(unzippedEncoded, ???)
    return unzippedEncoded.split("\n")
            .map { it.split("\t") }
            .filter { it.size == 2 }
            .map { Pair(it[0].split(",").map { hapId -> hapId.toInt() }.toList(),it[1].toInt()) }
            .toMap()
}

/**
 * Function to export the read mapping files to disk.  This is mostly for debugging purposes.
 */
fun exportReadMapping(outputFileName: String, hapIdMapping: Map<List<Int>, Int>, taxon : String, fileGroupName : String, methodName : String, methodDescription : String) {
    Utils.getBufferedWriter(outputFileName).use { output ->
        output.write("#line_name=${taxon}\n")
        output.write("#file_group_name=${fileGroupName}\n")
        output.write("#method_name=${methodName}\n")
        output.write("#method_description=${methodDescription}\n")
        output.write("HapIds\tcount\n")
        hapIdMapping.keys.forEach { output.write("${it.joinToString(",")}\t${hapIdMapping[it]}\n") }
    }
}

fun readInReadMappings(inputFileName: String, graph: HaplotypeGraph) : Multimap<ReferenceRange, HapIdSetCount> {
    val readMappings = importReadMapping(inputFileName)

    val hapIdToRangeMap = getHapToRefRangeMap(graph)

    val refRangeToReadMappingMultimap = HashMultimap.create<ReferenceRange, HapIdSetCount>()

    readMappings.keys.forEach {hapIds ->
        val count = readMappings[hapIds]?:0

        refRangeToReadMappingMultimap.put(hapIdToRangeMap[hapIds.first()], HapIdSetCount(hapIds.toSortedSet(), count))
    }

    return refRangeToReadMappingMultimap
}

/**
 * Function to read in the Readmapping file into a Map<List<Int>,Int>
 */
fun importReadMapping(inputFileName: String) : Map<List<Int>, Int> {
    return Utils.getBufferedReader(inputFileName).readLines()
            .filter { !it.startsWith("#") } //remove any of the metadata
            .filter { !it.startsWith("HapIds") }
            .map {
                val lineSplit = it.split("\t")
                val hapIds = lineSplit[0].split(",")
                        .map { hapId -> hapId.toInt() }
                        .toList()
                val count = lineSplit[1].toInt()

                Pair(hapIds, count)
            }
            .toMap()
}

/**
 * Function to export the best hit mapping.  This allows for easier debugging.
 */
fun exportBestHitMap(bestHitMap : Map<Pair<String,Boolean>, BestAlignmentGroup>, readMappingFile : String) {
    Utils.getBufferedWriter(readMappingFile).use { out ->

        out.write("readName\tstrand\teditDistance\tlengthOfMapping\thapId\n")

        bestHitMap.keys.map { bestHitMap[it] }
                .forEach {
                    out.write("${it?.readName}\t${it?.strand}\t${it?.bestEditDistance}\t${it?.lengthOfMapping}\t${it?.listOfHapIds}\n")
                }
    }
}

/**
 * Function to write out the multimap data structure to a file.  This requires the whole multimap to be constructed first
 */
fun exportMultimap(bestMapping: Multimap<ReferenceRange, SingleHapMapping>, outputFileName: String) {
    Utils.getBufferedWriter(outputFileName).use { out ->
        out.write("RefRangeId\tReadName\tsetOfHapIds\n")
        bestMapping.keySet()
                .sortedBy { it }
                .forEach {refRange ->
                    val mappings = bestMapping[refRange]
                    mappings.forEach { mapping ->
                        out.write("${refRange.id()}\t${mapping.readName}\t${mapping.hapIdSet.joinToString(",")}\n")
                    }
                }
    }
}

/**
 * Function to do some filtering on read mappings then will convert the mappings into the correct datastructure for the multimaps
 *
 * This can handle both paired end and single ends depending on the mapping functions passed in.
 *
 * The first is a filter function takes a String for the readName and the Mapping<Pair<String,Boolean>,BestAlignmentGroup> and will return a Boolean to filter
 *
 * The second function is a mapping function which creates a Pair<ReadName,Set<HapIds>>
 *
 * For instance in paired end mode, the filter function makes sure that both strand reads are represented in the map
 *  And the mapping function will take the intersection of the hapids for both strands which helps remove the bad mapping regions
 *
 * The single end is basically making sure that the read name is in the Map and the mapping function will just take the hapIds and put it in a Pair
 */
fun convertHitMapToSingleHapMapping(readNames:Set<String>, bestHitMap: Map<Pair<String, Boolean>, BestAlignmentGroup>,
                                    hapIdToRangeMap: Map<Int, ReferenceRange>, filterFunction :(String,Map<Pair<String,Boolean>,BestAlignmentGroup>) -> Boolean,
                                    mapToListFunction : (String,Map<Pair<String,Boolean>,BestAlignmentGroup>) -> Pair<String,Set<Int>>) :List<Pair<ReferenceRange?, SingleHapMapping>> {
    return readNames.filter { filterFunction(it,bestHitMap) }
            .map {
                mapToListFunction(it,bestHitMap)
            }
            .filter { !it.second.isEmpty() }
            .map {Pair(hapIdToRangeMap[it.second.first()],SingleHapMapping(it.first,it.second))}
            .filter { (it.second.readName == "").not() }
}

/**
 * Function to read in the multimapHits from a text file.
 *
 */
fun readInMultimapHitsFromFile(inputFileName: String) : Multimap<String, String> {
    //Open up the input file name and read in the files
    val multimapFiles = Utils.getBufferedReader(inputFileName).readLines()

    val fileMultimap = HashMultimap.create<String,String>()
    multimapFiles.map { it.split("\t") }
            .forEach { fileMultimap.put(it[0],it[1]) }

    return fileMultimap

}

/**
 * Function to read in the multimap Hits from a file and store it in a list.  The graph is needed to get a refRangeId to RefRange mapping
 */
fun readInListOfMultimapHits(inputFiles :List<String>, graph: HaplotypeGraph) : Multimap<ReferenceRange,SingleHapMapping> {
    return readInListOfMultimapHits(inputFiles,graph.referenceRanges().map { Pair(it.id(),it) }.toMap())
}

/**
 * Function to read in the Multimap
 */
fun readInListOfMultimapHits(inputFiles:List<String>,rangeIdToRangeMap:Map<Int,ReferenceRange>) :  Multimap<ReferenceRange,SingleHapMapping> {
    val finalMultimap = HashMultimap.create<ReferenceRange,SingleHapMapping>()

    inputFiles
            .map { readInMultimapHits(it,rangeIdToRangeMap) }
            .forEach { currentMapping ->
                finalMultimap.putAll(currentMapping)
            }

    return finalMultimap
}

/**
 * Function to read in the multimap Hits from a file and store it in a Multimap.  The graph is needed to get a refRangeId to RefRange mapping
 */
fun readInMultimapHits(inputFileName : String, graph: HaplotypeGraph): Multimap<ReferenceRange,SingleHapMapping>{
    return readInMultimapHits(inputFileName,graph.referenceRanges().map { Pair(it.id(),it) }.toMap())
}

/**
 * Function to read in the multimap hits.
 */
fun readInMultimapHits(inputFileName: String, rangeIdToRangeMap:Map<Int,ReferenceRange>) : Multimap<ReferenceRange,SingleHapMapping> {
    val linesInMultimapFile = Utils.getBufferedReader(inputFileName).readLines()
    val readMultimap = HashMultimap.create<ReferenceRange,SingleHapMapping>()
    linesInMultimapFile
            .filter { !it.startsWith("RefRangeId") }
            .map { it.split("\t") }
            .filter { rangeIdToRangeMap.containsKey(it[0].toInt()) }
            .forEach { it ->
                val refRangeId = it[0].toInt()
                val readName = it[1]
                val hapIds = it[2].split(",").map { hapId -> hapId.toInt() }.toMutableSet()

                readMultimap.put(rangeIdToRangeMap[refRangeId], SingleHapMapping(readName,hapIds))
            }

    return readMultimap
}

fun getHaplotypeMethodsForReadMappings(readMap : Multimap<ReferenceRange, HapIdSetCount>, numberOfRefRanges : Int = 1000) : List<String> {
    val refranges = readMap.keySet().take(numberOfRefRanges)
    val hapSet = refranges.flatMap { readMap[it] }.flatMap { it.hapIdSet }.toSet()
    return getHaplotypeMethodForHapids(hapSet)
}

fun getHaplotypeMethodForHapids(hapidList : Collection<Int>) : List<String> {
    val batchSize = 1000
    val dbConn = DBLoadingUtils.connection(false)
    val nameSet = TreeSet<String>()
    hapidList.windowed(batchSize, batchSize,true).forEach {
        val hapString = it.joinToString(",","(",")")
        val hapQuery = "SELECT distinct name FROM haplotypes, methods WHERE haplotypes.method_id=methods.method_id AND haplotypes_id in $hapString"
        val hapResult = dbConn.createStatement().executeQuery(hapQuery)
        while (hapResult.next()) {
            nameSet.add(hapResult.getString(1))
        }
    }

    dbConn.close()
    return nameSet.toList()
}

