package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultimap
import com.google.common.collect.HashMultiset
import com.google.common.collect.Multimap
import com.google.common.collect.Multiset
import htsjdk.samtools.SAMRecord
import htsjdk.samtools.SamReader
import htsjdk.samtools.SamReaderFactory
import htsjdk.samtools.ValidationStringency
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeGraphBuilderPlugin
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.api.referenceRanges
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.File
import javax.swing.ImageIcon
import net.maizegenetics.util.Utils



/**
 * This class will be the 3rd attempt for a genotyping pipeline.
 *
 * For now read in a SAM file  Generate from minimap2 and will output a HapCount File.
 * Inclusions are counted if both pairs occur within the same haplotype.
 * Exclusions are counted for the rest of the reads which are not hit by this pair.
 *
 * Future TODOs:
 *      - Run Minimap using ProcessBuilder and directly output a HapCount File
 *      - Figure out DB updating for HapCounts
 *
 *
 */
@Deprecated("Use FastqToMappingPlugin instead.")
class FastqToHapCountMinimapPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(FastqToHapCountMinimapPlugin::class.java)

    private var samFileName = PluginParameter.Builder("samFile", null, String::class.java)
            .guiName("SAM file to process")
            .inFile()
            .required(true)
            .description("Name of the SAM file to process")
            .build()

    private var readOutputFile = PluginParameter.Builder("readMappingOutput", null, String::class.java)
            .guiName("Output Read-> haplotype Mapping File")
            .outFile()
            .required(true)
            .description("Name of Read->haplotype Mapping file.")
            .build()

    private var hapCountFile = PluginParameter.Builder("hapCountFile", null, String::class.java)
            .guiName("Output Haplotype Count File")
            .outFile()
            .required(true)
            .description("Name of the haplotype count file.")
            .build()

    private var pairedMode = PluginParameter.Builder("paired", true, Boolean::class.java)
            .required(false)
            .description("Run in paired mode.  This will remove any haplotype counts if they are not agreed by the pair.")
            .build()

    private var maxRefRangeError = PluginParameter.Builder("maxRefRageErr", .25, Double::class.javaObjectType)
            .required(false)
            .description("Maximum allowed error when choosing best reference range to count.  Error is computed 1 - (mostHitRefCount/totalHits)")
            .build()

    override fun processData(input: DataSet?): DataSet? {
        //Load in the graph to check reference Ranges

        val temp = input?.getDataOfType(HaplotypeGraph::class.java)
                ?: throw IllegalArgumentException("IndexKmerByHammingPlugin: processData: must input one HaplotypeGraph type not null: ")
        if (temp.size != 1) {
            throw IllegalArgumentException("IndexKmerByHammingPlugin: processData: must input one HaplotypeGraph: " + temp.size)
        }
        val graph:HaplotypeGraph = temp[0].data as HaplotypeGraph
        val hapIdToRefRangeMap = getHapToRefRangeMap(graph)
        //Take SAM file input TODO generate SAM file and pipe directly into here
        val samFileReader = loadSAMFileIntoSAMReader(samFileName())
        myLogger.info("Done loading in Sam File")
        //Get equally "best hits" for each read(pair)
        val bestHitMapMultipleRefRanges = scoreSAMFile(samFileReader)
        myLogger.info("Done Scoring sam file")

//        val bestHitMap = filterReadsMultipleRefRanges(bestHitMapMultipleRefRanges, hapIdToRefRangeMap)
        val bestHitMap = keepHapIdsForSingleRefRange(bestHitMapMultipleRefRanges, hapIdToRefRangeMap, maxRefRangeError()?:0.0)

        readOutputFile()?.let {
            exportBestHitMap(bestHitMap, readOutputFile())
            myLogger.info("Done exporting hit map")
        }


        //Count hits and export file
        val inclusionAndExclusionMultiset = countHaplotypeHits(bestHitMap,graph,hapIdToRefRangeMap)
        myLogger.info("Done counting")
        //TODO write to DB counts
        exportCounts(inclusionAndExclusionMultiset)
        myLogger.info("Done exporting counts.")
        return null
    }


    /**
     * Function to filter out reads which hit multiple reference ranges
     */
    fun filterReadsMultipleRefRanges(bestHitMap: Map<Pair<String,Boolean>, BestAlignmentGroup>, hapIdToRangeMap: Map<Int, ReferenceRange>) : Map<Pair<String,Boolean>, BestAlignmentGroup> {
       return bestHitMap.entries.filter{ spansSingleRefRange(it, hapIdToRangeMap) }
                .map { Pair(it.key,it.value) }
                .toMap()
    }


    /**
     * Function to count the haplotype hits.  Only hapIds where both reads in the pair hit are returned.  Otherwise the mapping is ignored.
     */
    fun countHaplotypeHits(bestReadMappings : Map<Pair<String,Boolean>, BestAlignmentGroup>, graph: HaplotypeGraph, hapIdToRangeMap: Map<Int, ReferenceRange>) : Pair<Multiset<Int>,Multiset<Int>> {
        val inclusionMultiset = HashMultiset.create<Int>()
        val exclusionMultiset = HashMultiset.create<Int>()


        if(pairedMode()) {
            val readNames = bestReadMappings.keys
                    .map { it.first }
                    .toSet()

            readNames.filter { bestReadMappings.containsKey(Pair(it,true)) && bestReadMappings.containsKey(Pair(it,false)) }
                    .map {
                        val forwardMappings = bestReadMappings[Pair(it,false)]?.listOfHapIds?:listOf<Int>()
                        val reverseMappings = bestReadMappings[Pair(it,true)]?.listOfHapIds?: listOf<Int>()

                        forwardMappings.intersect(reverseMappings)
                    }
                    .forEach { hapIdSet ->
                        hapIdSet.forEach { inclusionMultiset.add(it) }
                        //Add exclusions
                        val excludeNodeList = findExclusions(hapIdSet,graph,hapIdToRangeMap)
                        excludeNodeList.forEach{exclusionMultiset.add(it)}
                    }
        }
        else {
            bestReadMappings.keys
                    .map { bestReadMappings[it] }
                    .map {
                        it?.listOfHapIds ?: setOf<Int>()
                    } //Not using a flat map in case we need to do some additional logic for exclusions
                    .forEach { hapIdList ->
                        //Add inclusions
                        hapIdList.forEach { inclusionMultiset.add(it) }
                        //Add exclusions
                        val excludeNodeList = findExclusions(hapIdList,graph,hapIdToRangeMap)
                        excludeNodeList.forEach{exclusionMultiset.add(it)}
                    }

        }

        return Pair(inclusionMultiset,exclusionMultiset)
    }

    /**
     * Simple Exclusion finding method.  To be replaced with something more complicated if need be.
     */
    private fun findExclusions(hapIdList: Set<Int>, graph: HaplotypeGraph, hapIdToRangeMap: Map<Int, ReferenceRange>): List<Int> {
        //Get out the full set of nodes in that reference range for the graph
        if(hapIdList.isEmpty()) return listOf()

        val currentReferenceRange = hapIdToRangeMap[hapIdList.first()]
        val hapIdsAtRefRange = graph.nodes(currentReferenceRange)
        return hapIdsAtRefRange
                .map{it.id()}
                .filter{!hapIdList.contains(it)}


    }

    /**
     * Function to export the inclusion and exclusion counts to a text file.
     */
    fun exportCounts(multisets : Pair<Multiset<Int>,Multiset<Int>>) {
        val inclusionSet = multisets.first
        val exclusionSet = multisets.second

        val allIdSet = getAllInclusionsAndExclusions(inclusionSet,exclusionSet)

        Utils.getBufferedWriter(hapCountFile()).use { out ->
            allIdSet.forEach {
                out.write("${it}\t${inclusionSet.count(it)}\t${exclusionSet.count(it)}\n")
            }
        }
    }

    /**
     * Simple function to merge the inclusionIds and exclusion Ids together
     */
    fun getAllInclusionsAndExclusions(inclusionSet:Multiset<Int>, exclusionSet:Multiset<Int>) : Set<Int> {
        val idSet = mutableSetOf<Int>()
        idSet.addAll(inclusionSet.elementSet())
        idSet.addAll(exclusionSet.elementSet())

        return idSet
    }


    override fun getIcon(): ImageIcon? {
        val imageURL = FastqToHapCountMinimapPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "FastqToHapCountMinimapPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to Align a Fastq file and export a hapCount File "
    }

    /**
     * Name of the SAM file to process
     *
     * @return SAM file to process
     */
    fun samFileName(): String {
        return samFileName.value()
    }

    /**
     * Set SAM file to process. Name of the SAM file to process
     *
     * @param value SAM file to process
     *
     * @return this plugin
     */
    fun samFileName(value: String): FastqToHapCountMinimapPlugin {
        samFileName = PluginParameter(samFileName, value)
        return this
    }

    /**
     * Name of Read->haplotype Mapping file.
     *
     * @return Output Read-> haplotype Mapping File
     */
    fun readOutputFile(): String {
        return readOutputFile.value()
    }

    /**
     * Set Output Read-> haplotype Mapping File. Name of Read->haplotype
     * Mapping file.
     *
     * @param value Output Read-> haplotype Mapping File
     *
     * @return this plugin
     */
    fun readOutputFile(value: String): FastqToHapCountMinimapPlugin {
        readOutputFile = PluginParameter(readOutputFile, value)
        return this
    }

    /**
     * Name of the haplotype count file.
     *
     * @return Output Haplotype Count File
     */
    fun hapCountFile(): String {
        return hapCountFile.value()
    }

    /**
     * Set Output Haplotype Count File. Name of the haplotype
     * count file.
     *
     * @param value Output Haplotype Count File
     *
     * @return this plugin
     */
    fun hapCountFile(value: String): FastqToHapCountMinimapPlugin {
        hapCountFile = PluginParameter(hapCountFile, value)
        return this
    }

    /**
     * Run in paired mode.  This will remove any haplotype
     * counts if they are not agreed by the pair.
     *
     * @return Paired
     */
    fun pairedMode(): Boolean {
        return pairedMode.value()
    }

    /**
     * Set Paired. Run in paired mode.  This will remove any
     * haplotype counts if they are not agreed by the pair.
     *
     * @param value Paired
     *
     * @return this plugin
     */
    fun pairedMode(value: Boolean): FastqToHapCountMinimapPlugin {
        pairedMode = PluginParameter(pairedMode, value)
        return this
    }

    /**
     * Maximum allowed error when choosing best reference
     * range to count.  Error is computed 1 - (mostHitRefCount/totalHits)
     *
     * @return Max Ref Rage Err
     */
    fun maxRefRangeError(): Double? {
        return maxRefRangeError.value()
    }

    /**
     * Set Max Ref Rage Err. Maximum allowed error when choosing
     * best reference range to count.  Error is computed 1
     * - (mostHitRefCount/totalHits)
     *
     * @param value Max Ref Rage Err
     *
     * @return this plugin
     */
    fun maxRefRangeError(value: Double?): FastqToHapCountMinimapPlugin {
        maxRefRangeError = PluginParameter(maxRefRangeError, value)
        return this
    }

}

fun main(args: Array<String>) {
    GeneratePluginCode.generate(FastqToHapCountMinimapPlugin::class.java)
}