@file:JvmName("Minimap2Utils")

package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import htsjdk.samtools.*
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.prefs.TasselPrefs
import net.maizegenetics.util.DirectoryCrawler
import net.maizegenetics.util.Utils
import java.io.BufferedInputStream
import java.io.File
import java.nio.file.Paths
import kotlin.collections.HashSet
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import org.apache.log4j.Logger
import java.lang.IllegalStateException


/**
 * Utility class holding various functions used by the minimap2 processing pipeline.
 * Each function itself should have a better description of what it does.
 */
private val myLogger = Logger.getLogger("net.maizegenetics.pangenome.hapCalling.Minimap2Utils")

data class BestAlignmentGroup(val readName: String, val strand : Boolean, val bestEditDistance:Int, val lengthOfMapping: Int, val listOfHapIds: MutableSet<Int>, val hapIdToSecondaryStats: MutableMap<Int,SecondaryStats>)
data class SecondaryStats(val AS: Int, val de: Float, val startPosition : Int)
data class SingleHapMapping(val readName: String, val hapIdSet: Set<Int>)
data class KeyFileUniqueRecord(val methodName : String, val taxonName : String, val fileGroupName : String)
data class HapIdSetCount(val hapIdSet : Set<Int>, val count : Int)
data class HapIdStats(val hapId : Int, var count : Int, var NM : Int, var AS : Int, var de : Float, var startPositions : MutableList<Int>) {
    fun addStats(newCount : Int, newNM : Int, newAS : Int, newDE : Float, newStart : Int) {
        count += newCount
        NM += newNM
        AS += newAS
        de += newDE
        startPositions.add(newStart)
    }
}

enum class ReadMappingInputFileFormat {
    FASTQ,SAM
}

/**
 * Function to create a HapId to RefRange mapping for all of the HapIds in a graph
 */
fun getHapToRefRangeMap(graph: HaplotypeGraph) : Map<Int, ReferenceRange> {
    return graph.referenceRanges()
            .flatMap { graph.nodes(it) }
            .map { Pair(it.id(),it.referenceRange()) }
            .toMap()
}
fun getHapIdToSequenceLength(graph: HaplotypeGraph) : Map<Int,Int> {
    return graph.referenceRanges()
            .flatMap { graph.nodes(it) }
            .map { Pair(it.id(),it.haplotypeSequence().length()) }
            .toMap()
}

fun getRefRangeToHapidMap(graph: HaplotypeGraph) : Map<Int,Map<Int,Int>>{
    //This creates a map of ReferenceRangeId -> (map of hapid -> index)
    return graph.referenceRanges().associateBy(
        {refrange -> refrange.id()},
        { refrange ->
            graph.nodes(refrange).map { it.id() }.sorted()
                .mapIndexed { index, hapid ->  Pair(hapid, index)}
                .toMap()
        }
    )
}

fun getHaplotypeListIdForGraph(graph: HaplotypeGraph, phgAccessor: PHGdbAccess) : Int {
    val haplotypeIdList = graph.referenceRangeList().map { rr -> graph.nodes(rr).map { node -> node.id() } }.flatten()
    return phgAccessor.putHalotypeListData(haplotypeIdList)
}

fun filterRead(currentSamRecord: SAMRecord, pairedEnd:Boolean) : Boolean {
    //Ignore unmapped reads
    if(currentSamRecord.readUnmappedFlag) {
        return true
    }
    if(pairedEnd) {
        try {
            //Ignore mappings where pair of reads are on the same strand
            if (currentSamRecord.mateNegativeStrandFlag == currentSamRecord.readNegativeStrandFlag) {
                return true
            }
        }
        catch(e: Exception) {
            myLogger.info("Error processing read ${currentSamRecord.readName}.  Unable to properly pair the reads.")
            throw e
        }

    }

    //Ignore clipped reads
    if(currentSamRecord.cigar.isClipped) {
        return true
    }

    return false
}


/**
 * Function to load the SAM file into a SAM reader
 * Could remove this, but we should leave it in as you may need to read from a file in the future.
 */
fun loadSAMFileIntoSAMReader(fileName:String) : SamReader {
    return SamReaderFactory.makeDefault()
            .validationStringency(ValidationStringency.SILENT)
            .open(File(fileName))
}

/**
 * Method to run minimap2 using information provided by the Key file.
 *
 * This assumes that there must be a column named filename and if paired end is requested a column named filename2.
 *
 * There are additional pieces of information that will also be provided by the keyfile, but will be implemented once the DB changes are implemented.
 *
 */
fun runMinimapFromKeyFile(minimapLocation: String, keyFileName:String,
                          inputFileDir : String, referenceFile: String,
                          graph: HaplotypeGraph,
                          maxRefRangeError: Double,
                          methodName : String, methodDescription: String?,
                          pluginParams: Map<String,String>,
                          outputDebugReadMappingDir : String,
                          outputSecondaryMappingStats : Boolean = false,
                          maxSecondary: Int = 20,
                          inputFileFormat: ReadMappingInputFileFormat = ReadMappingInputFileFormat.FASTQ,
                          fParameter: String = "f1000,5000",
                          isTestMethod: Boolean) {
    var dbConnect = DBLoadingUtils.connection(false)
    val phg = PHGdbAccess(dbConnect)

    val columnMappingAndLines = readInKeyFile(keyFileName)
    val keyFileColumnNameMap = columnMappingAndLines.first
    val keyFileLines = columnMappingAndLines.second

    //Get out the column indices as they will be consistent for the whole file
    val taxonCol = keyFileColumnNameMap["cultivar"]?:-1
    val fileNameCol1 = keyFileColumnNameMap["filename"]?:-1
    val fileNameCol2 = if (keyFileColumnNameMap.containsKey("filename2")) keyFileColumnNameMap["filename2"]?:-1 else -1
    val flowcellCol = keyFileColumnNameMap["flowcell_lane"]?:-1

    //Check will automatically throw an IllegalStateException if the logic fails.
    check(taxonCol != -1) { "Error processing keyfile.  Must have cultivar column." }
    check(flowcellCol != -1) { "Error processing keyfile.  Must have flowcell_lane column." }
    check(fileNameCol1 != -1) { "Error processing keyfile.  Must have filename column." }

    val globString = when(inputFileFormat) {
        ReadMappingInputFileFormat.FASTQ -> "glob:*{.fastq.gz,.fq.gz,.fastq,.fq}"
        ReadMappingInputFileFormat.SAM -> "glob:*{.sam,.bam}"
    }

    val directoryFilePaths = DirectoryCrawler.listPaths(globString, Paths.get(inputFileDir))
    val fileNameToPathMap = directoryFilePaths.associateBy { it.fileName.toString() }
    val directoryFileNames = fileNameToPathMap.keys.toHashSet()

    val keyFileRecordsToFileMap = mutableMapOf<KeyFileUniqueRecord,Pair<String,String>>()
    val keyFileRecordsToEntry = mutableMapOf<List<String>, KeyFileUniqueRecord>()
    val keyFileRecordsToMappingId = mutableMapOf<KeyFileUniqueRecord,Int>()
    val keyFileRecords = keyFileLines
            .filter { isKeyEntryInDir(directoryFileNames,it,fileNameCol1, fileNameCol2) }
            .map {
                val file1 = it[fileNameCol1]
                val file2 = if (fileNameCol2 != -1) it[fileNameCol2] else ""
                val taxon = it[taxonCol]
                //fileGrpName = file names because sometimes user does not know plate and flow cell
                val uniqueRecord = KeyFileUniqueRecord(methodName, taxon, it[flowcellCol])

                keyFileRecordsToFileMap[uniqueRecord] = Pair(file1,file2)
                keyFileRecordsToEntry[it] = uniqueRecord
                uniqueRecord
            }
            .toList()

    verifyNoDuplicatesInKeyFile(keyFileRecords)

    //create maps needed for BitSet encoding
    val hapIdToRefRangeMap = getHapToRefRangeMap(graph)
    val hapIdToLengthMap = getHapIdToSequenceLength(graph)
    val refRangeToHapIdMap = getRefRangeToHapidMap(graph)
    val haplotypeListId = getHaplotypeListIdForGraph(graph, phg)

    // Update the read mapping hash - this is used by getReadMappingId() in the loop below
    // This assumes there  will be no duplicates in this file we are processing. We are only
    // updating the hash here - not after each submission below.
    // Inside outputKeyFiles() it will be run again to pick up the new readMappingIds that
    // were created below.
    var time = System.nanoTime();
    myLogger.info("runMinimapFromKeyFile: calling updateReadMappingHash()")
    phg.updateReadMappingHash()
    var hashLoadTime = (System.nanoTime() - time)/1e9
    myLogger.info("runMinimapFromKeyFile, updateReadMappingHash took ${hashLoadTime} seconds")

    keyFileRecordsToFileMap.keys.map { Pair(it,keyFileRecordsToFileMap[it]) }
            .forEach {
                val keyFileRecord = it.first
                val taxon = keyFileRecord.taxonName
                val fileGroupName = keyFileRecord.fileGroupName
                val file1 = it.second?.first ?: ""
                val file2 = it.second?.second ?: ""

                check(file1 != "") { "file1 is missing.  Check your keyfile: cultivar ${taxon}, flowcell_lane ${fileGroupName} " }

                //Check to see if the read mapping is already stored in the db.
                val readMappingId = phg.getReadMappingId(taxon, methodName, fileGroupName)

                //If the id == -1, it means the read mapping is not in the db, we need to process.
                if(readMappingId == -1) {
                    myLogger.info("Setting up MinimapRun for: cultivar ${taxon}, flowcell_lane ${fileGroupName}.")
                    val startTimeSetUpRun = System.nanoTime()

                    val samReader = when(inputFileFormat) {
                        ReadMappingInputFileFormat.FASTQ -> setupMinimapRun(minimapLocation, referenceFile, fileNameToPathMap[file1].toString(), if(file2!="") fileNameToPathMap[file2].toString() else "", maxSecondary, fParameter)
                        ReadMappingInputFileFormat.SAM -> loadSAMFileIntoSAMReader(fileNameToPathMap[file1].toString())
                    }


                    myLogger.info("Time spent setting up run: Taxon:${taxon} : ${(System.nanoTime() - startTimeSetUpRun)/1E9}sec")

                    val keyFileObj = File(keyFileName)

                    val keyFileNameWithoutExtension = keyFileObj.parent + "/" + keyFileObj.nameWithoutExtension
                    val outputHapIdToStatsFile = if(outputSecondaryMappingStats) {"${keyFileNameWithoutExtension}_${taxon}_${fileGroupName}_additionalMappingStats.txt" } else { "" }

                    val processingSamFileStartTime = System.nanoTime()
                    val hapIdMapping = scoreSamFileCountHapSetHits(samReader, hapIdToRefRangeMap, maxRefRangeError, (file2 != ""),outputHapIdToStatsFile,hapIdToLengthMap) //if file2 =="" it is single end so paired = false
                    myLogger.info("Time spent processing SAM: Taxon:${taxon} : ${(System.nanoTime() - processingSamFileStartTime)/1E9}sec")

                    if(hapIdMapping.size == 0) {
                        myLogger.info("No reads have mapped for: cultivar ${taxon}, flowcell_lane ${fileGroupName}")
                    }
                    else {

                        myLogger.info("Done running minimap2 for: cultivar ${taxon}, flowcell_lane ${fileGroupName}")
                        loadBitSetEncodedReadMappingsToDB(hapIdMapping, taxon, fileGroupName, pluginParams,
                            methodDescription, phg, methodName, keyFileRecordsToMappingId, keyFileRecord,
                            outputDebugReadMappingDir, hapIdToRefRangeMap, refRangeToHapIdMap, haplotypeListId, isTestMethod)
                    }
                }
                else {
                    myLogger.info("Skipping Keyfile entry: cultivar ${taxon}, flowcell_lane $fileGroupName has already been processed and loaded into the DB.")
                }
            }


    outputKeyFiles(keyFileName, keyFileLines, flowcellCol, phg, taxonCol, methodName, keyFileColumnNameMap)

    phg.close()
}

/**
 * Function to output mapping id and path key files to be used by the path finding.
 */
private fun outputKeyFiles(keyFileName: String, keyFileLines: List<List<String>>, flowcellCol: Int, phg: PHGdbAccess, taxonCol: Int, methodName: String, keyFileColumnNameMap: Map<String, Int>) {
    //Create the outputFileNames
    val keyFileObj = File(keyFileName)

    //Check to see if the folder path is included.
    //If it is not included, keyFileObj.parent will return a null so we can just preappend ./ to the filename.
    val keyFileNameWithoutExtension = if (keyFileObj.parent != null) {
        keyFileObj.parent + "/" + keyFileObj.nameWithoutExtension
    } else {
        "./" + keyFileObj.nameWithoutExtension
    }

    val outputMappingKeyFile = "${keyFileNameWithoutExtension}_withMappingIds.txt"
    val outputPathKeyFile = "${keyFileNameWithoutExtension}_pathKeyFile.txt"

    //Export the mapping ids
    //keyFileColumnNameMap is a Map<String,Int> of name, column index
    //keyFileLines is a List of List<String> with the split contents of each line in the key file

    // Update the hash so all readmapping ids are known.
    // This will pickup all the read mappings created from the calling method.
    // If we don't call updateReadMappingHash() here, getReadMappingId() will
    // query the DB for each readmapping that isn't present.
    // That might be quicker if the number of readmappings just entered is low
    var time = System.nanoTime();
    myLogger.info("outputKeyFiles: calling updateReadMappingHash()")
    phg.updateReadMappingHash()
    var hashLoadTime = (System.nanoTime() - time)/1e9
    myLogger.info("outputKeyFiles, updateReadMappingHash took ${hashLoadTime} seconds")

    //export each row of the keyfile with the read mapping id appended
    val keyFileLinesWithId = keyFileLines.map { keyFileRow ->
        val fileGroup = keyFileRow[flowcellCol]
        val id = phg.getReadMappingId(keyFileRow[taxonCol], methodName, fileGroup)
        Pair(keyFileRow, id)
    }.filter { it.second > -1 }

    Utils.getBufferedWriter(outputMappingKeyFile).use { output ->
        val getColumnResorted = mutableListOf<String>()
        keyFileColumnNameMap.keys.forEach { keyColumnName ->
            val columnIndex = keyFileColumnNameMap[keyColumnName]
            check(columnIndex != null) { "Column name ${keyColumnName} does not exist in the map" }
            getColumnResorted.add(columnIndex, keyColumnName)
        }

        output.write("${getColumnResorted.joinToString("\t")}\treadMappingIds\n")
        keyFileLinesWithId.forEach { output.write("${it.first.joinToString("\t")}\t${it.second}\n") }
    }

    val taxonToHapIds = keyFileLinesWithId.groupBy({ it.first[taxonCol] }, { it.second })

    //Write out a sample path key file, one row per taxon in the original key file
    Utils.getBufferedWriter(outputPathKeyFile).use { output ->
        output.write("SampleName\tReadMappingIds\tLikelyParents\n")
        taxonToHapIds.keys.forEach { taxon ->
            val hapIds = taxonToHapIds[taxon] ?: listOf()
            output.write("${taxon}\t${hapIds.joinToString(",")}\t\t\n")
        }
    }
}

/**
 * Function to load the Read Mappings to the DB.  This will encode the ReadMappings and the load them in given the provided information.
 */
private fun loadReadMappingsToDB(hapIdMapping: Map<List<Int>, Int>,
                                 taxon: String,
                                 fileGroupName: String,
                                 pluginParams: Map<String, String>,
                                 methodDescription: String?,
                                 phg: PHGdbAccess,
                                 methodName: String,
                                 keyFileRecordsToMappingId: MutableMap<KeyFileUniqueRecord, Int>,
                                 keyFileRecord: KeyFileUniqueRecord,
                                 outputDebugReadMappingDir: String,
                                 isTestMethod:Boolean) {
    myLogger.info("Compressing ReadMappings into GZipped Json.")
    //Encode the HapIdMapping into the serialized format
    val encodedReadMapping = encodeHapIdMapping(hapIdMapping)
    check(encodedReadMapping != null) { "Error Encoding Read Mappings: cultivar ${taxon}, flowcell_lane ${fileGroupName}" }
    myLogger.info("Done Compressing ReadMappings.  Pushing it to the DB.")

    val finalParamMap = pluginParams.toMutableMap()
    if (methodDescription != null) finalParamMap.put("notes", methodDescription)

    //Create object to upload to the DB and return it.
    val readMappingId = phg.putReadMappingData(methodName, finalParamMap, taxon, fileGroupName, encodedReadMapping, isTestMethod, -1)

    keyFileRecordsToMappingId[keyFileRecord] = readMappingId

    if (outputDebugReadMappingDir != "") {
        val outputFileName = "${outputDebugReadMappingDir}/${taxon}_${fileGroupName}_ReadMapping.txt"
        myLogger.info("Debug Output Dir Specified:  Writing file:${outputFileName}")
        exportReadMapping(outputFileName, hapIdMapping, taxon, fileGroupName, methodName,
                DBLoadingUtils.formatMethodParamsToJSON(finalParamMap))
    }

    myLogger.info("Done processing cultivar ${taxon}, flowcell_lane ${fileGroupName}.  Moving on to next keyfile Entry.")
}

/**
 * Function to load the Read Mappings to the DB.  This will encode the ReadMappings and the load them in given the provided information.
 */
private fun loadBitSetEncodedReadMappingsToDB(hapIdMapping: Map<List<Int>, Int>,
                                              taxon: String,
                                              fileGroupName: String,
                                              pluginParams: Map<String, String>,
                                              methodDescription: String?,
                                              phg: PHGdbAccess,
                                              methodName: String,
                                              keyFileRecordsToMappingId: MutableMap<KeyFileUniqueRecord, Int>,
                                              keyFileRecord: KeyFileUniqueRecord,
                                              outputDebugReadMappingDir: String,
                                              hapIdToRefRangeMap: Map<Int, ReferenceRange>,
                                              refRangeToHapIdMap: Map<Int, Map<Int,Int>>,
                                              hapListId: Int,
                                              isTestMethod: Boolean) {
    myLogger.info("Compressing ReadMappings into GZipped Json.")
    //Encode the HapIdMapping into the serialized format
    val encodedReadMapping = encodeHapIdsAsBitSet(hapIdMapping, hapIdToRefRangeMap, refRangeToHapIdMap)
    check(encodedReadMapping != null) { "Error Encoding Read Mappings: cultivar ${taxon}, flowcell_lane ${fileGroupName}" }
    myLogger.info("Done Compressing ReadMappings.  Pushing it to the DB.")

    val finalParamMap = pluginParams.toMutableMap()
    if (methodDescription != null) finalParamMap.put("notes", methodDescription)

    //Create object to upload to the DB and return it.
    val readMappingId = phg.putReadMappingData(methodName, finalParamMap, taxon, fileGroupName, encodedReadMapping,isTestMethod, hapListId)

    keyFileRecordsToMappingId[keyFileRecord] = readMappingId

    if (outputDebugReadMappingDir != "") {
        val outputFileName = "${outputDebugReadMappingDir}/${taxon}_${fileGroupName}_ReadMapping.txt"
        myLogger.info("Debug Output Dir Specified:  Writing file:${outputFileName}")
        exportReadMapping(outputFileName, hapIdMapping, taxon, fileGroupName, methodName,
                DBLoadingUtils.formatMethodParamsToJSON(finalParamMap))
    }

    myLogger.info("Done processing cultivar ${taxon}, flowcell_lane ${fileGroupName}.  Moving on to next keyfile Entry.")
}

/**
 * Function to upload multisample BAM files being used by MultisampleBAMToMappingPlugin.
 *
 * This needs to be different than runMinimapFromKeyFile as runMinimapFromKeyFile only processes 1 file for one taxon.
 * Having 1 file for multiple taxon makes things much more difficult as we need to keep the iterator up as we switch files.
 */
fun createReadMappingsFromMultisampleBAMs(graph: HaplotypeGraph,keyFileName:String,
                                          fastqGroupingFileName: String,
                                          inputFileDir : String,
                                          maxRefRangeError: Double,
                                          methodName : String,
                                          methodDescription: String?,
                                          pluginParams: Map<String,String>,
                                          outputDebugReadMappingDir : String,
                                          outputSecondaryMappingStats : Boolean = false,
                                          isTestMethod: Boolean) {
    var dbConnect = DBLoadingUtils.connection(false)
    val phg = PHGdbAccess(dbConnect)

    //TODO when paired end works, remove this.
    val pairedEnd = false

    val fastqGroupLines = readInKeyFile(fastqGroupingFileName)
    val fastqColNameMap = fastqGroupLines.first
    val fastqLines = fastqGroupLines.second

//    "originalFile\tnewFile\tBatch\tnumReads\n"
    val originalFileCol = fastqColNameMap["originalFile"] ?: -1
    val newFileCol = fastqColNameMap["newFile"] ?: -1
    val batchCol = fastqColNameMap["Batch"] ?: -1
    val numReadsCol = fastqColNameMap["numReads"] ?: -1

    check(originalFileCol != -1) {"Error processing Fastq Grouping File.  Must have originalFile column."}
    check(newFileCol != -1) {"Error processing Fastq Grouping File.  Must have newFile column."}
    check(batchCol != -1) {"Error processing Fastq Grouping File.  Must have Batch column."}
    check(numReadsCol != -1) {"Error processing Fastq Grouping File.  Must have numReads column."}


    val columnMappingAndLines = readInKeyFile(keyFileName)
    val keyFileColumnNameMap = columnMappingAndLines.first
    val keyFileLines = columnMappingAndLines.second

    //Get out the column indices as they will be consistent for the whole file
    val taxonCol = keyFileColumnNameMap["cultivar"] ?: -1
    val fileNameCol1 = keyFileColumnNameMap["filename"] ?: -1
    val fileNameCol2 = if (keyFileColumnNameMap.containsKey("filename2")) keyFileColumnNameMap["filename2"]
            ?: -1 else -1
    val flowcellCol = keyFileColumnNameMap["flowcell_lane"] ?: -1

    //Check will automatically throw an IllegalStateException if the logic fails.
    check(taxonCol != -1) { "Error processing keyfile.  Must have cultivar column." }
    check(flowcellCol != -1) { "Error processing keyfile.  Must have flowcell_lane column." }
    check(fileNameCol1 != -1) { "Error processing keyfile.  Must have filename column." }

    val globString = "glob:*{.sam,.bam}"

    val directoryFilePaths = DirectoryCrawler.listPaths(globString, Paths.get(inputFileDir))
    val fileNameToPathMap = directoryFilePaths.associateBy { it.fileName.toString() }
    val directoryFileNames = fileNameToPathMap.keys.toHashSet()

    val keyFileRecordsToFileMap = mutableMapOf<KeyFileUniqueRecord, Pair<String, String>>()
    val keyFileRecordsToEntry = mutableMapOf<List<String>, KeyFileUniqueRecord>()
    val keyFileRecordsToMappingId = mutableMapOf<KeyFileUniqueRecord, Int>()
    val keyFileRecords = keyFileLines
            .map {
                val file1 = it[fileNameCol1]
                val file2 = if (fileNameCol2 != -1) it[fileNameCol2] else ""
                val taxon = it[taxonCol]
                //fileGrpName = file names because sometimes user does not know plate and flow cell
                val uniqueRecord = KeyFileUniqueRecord(methodName, taxon, it[flowcellCol])

                keyFileRecordsToFileMap[uniqueRecord] = Pair(file1, file2)
                keyFileRecordsToEntry[it] = uniqueRecord
                uniqueRecord
            }
            .toList()

    verifyNoDuplicatesInKeyFile(keyFileRecords)

    val hapIdToRefRangeMap = getHapToRefRangeMap(graph)
    val refRangeToHapIdMap = getRefRangeToHapidMap(graph)
    val hapIdToLengthMap = getHapIdToSequenceLength(graph)
    val hapListId = getHaplotypeListIdForGraph(graph, phg)

    //Set this up for debugging purposes
    val keyFileObj = File(keyFileName)
    val keyFileNameWithoutExtension = keyFileObj.parent + "/" + keyFileObj.nameWithoutExtension

    //This is setup this way in case there are 2 fastqs.  The merge Fastq plugin will need to be pair-aware to make this work correctly.
    val fileNameToKeyEntry = keyFileRecordsToFileMap.keys
            .flatMap { listOf(Pair(keyFileRecordsToFileMap[it]?.first, it),Pair(keyFileRecordsToFileMap[it]?.second, it))  }
            .toMap()

    var currentMergedBAMFile = ""
    var currentSAMReader:SAMRecordIterator? = null
    var samCounter = 0
    for (fastqFile in fastqLines) {

        var bestReadMap = mutableMapOf<Pair<String, Boolean>, BestAlignmentGroup>()

        //Need a new hapIdMultiset for each record in the key file.
        var hapIdMultiset = HapIdMultiset()
        var hapIdToStatMap = mutableMapOf<Int,HapIdStats>()

        //Try to figure out the file that matches the grouping.
        val nameWithoutExtension = "${inputFileDir}${File(fastqFile[fastqColNameMap["newFile"]?:0]).nameWithoutExtension}"
        val currentBamResolvedFastqFile = if(File("${nameWithoutExtension}.bam").exists()) {
            "${nameWithoutExtension}.bam"
        }
        else if(File("${nameWithoutExtension}.sam").exists()) {
            "${nameWithoutExtension}.sam"
        }
        else {
            throw IllegalStateException("Unable to find corresponding SAM or BAM file for: ${fastqFile[fastqColNameMap["newFile"]?:0]}.  Please check your grouping file.")
        }
        if(currentBamResolvedFastqFile != currentMergedBAMFile) {
            //Different BAM file
            //need to find and load in a new one.
            //Check to make sure we do not have any extra reads here.  If we do throw an error.
            if(currentSAMReader?.hasNext()?:false) {
                while(currentSAMReader?.hasNext()?:false) {
                    val currentRecord = currentSAMReader!!.next()
                    println("Extra: ${currentRecord.readName}")
                }
                throw IllegalStateException("ERROR: SamIterator has more entries. Please check the grouping file.")
            }

            //set and reset variables which span multiple unmerged fastqs
            currentMergedBAMFile = currentBamResolvedFastqFile
            currentSAMReader = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT).open(File(currentMergedBAMFile)).iterator()
            bestReadMap = mutableMapOf<Pair<String, Boolean>, BestAlignmentGroup>()
            //Need a new hapIdMultiset for each record in the key file.
            hapIdMultiset = HapIdMultiset()
            hapIdToStatMap = mutableMapOf<Int,HapIdStats>()

            samCounter = 0
        }

        var currentReadName = ""
        var readCounter = 0
        val readSet = mutableSetOf<String>()

        while(readCounter < fastqFile[fastqColNameMap["numReads"]?:0].toInt()) {
            samCounter++
            val currentRecord = currentSAMReader!!.next()

            //TODO Make this work with paired end.
            if (filterRead(currentRecord, pairedEnd)) {
                //Need to add in the name in case we filter out all reads.
                if(currentReadName != currentRecord.readName) {
                    readCounter++
                    readSet.add(currentRecord.readName)
                    currentReadName = currentRecord.readName
                }
                continue
            }

            if(currentReadName == "") {
                readCounter++
                readSet.add(currentRecord.readName)
                currentReadName = currentRecord.readName
            }

            if(currentReadName != currentRecord.readName) {
                //Process the batch
                addBestReadMapToHapIdMultiset(bestReadMap, hapIdToRefRangeMap, maxRefRangeError, false, hapIdMultiset, hapIdToStatMap)

                readCounter++
                readSet.add(currentRecord.readName)
                //Set the new readName
                currentReadName = currentRecord.readName
                //Reset the best hit map
                bestReadMap = mutableMapOf<Pair<String, Boolean>, BestAlignmentGroup>()
            }

            attemptToAddSAMRecordToBestReadMap(pairedEnd, currentRecord, currentReadName, bestReadMap)

        }

        //Add in the last record as well.
        addBestReadMapToHapIdMultiset(bestReadMap, hapIdToRefRangeMap, maxRefRangeError, false, hapIdMultiset, hapIdToStatMap)
        //Reset the best hit map
        bestReadMap = mutableMapOf<Pair<String, Boolean>, BestAlignmentGroup>()

        val keyFileRecord = fileNameToKeyEntry[fastqFile[fastqColNameMap["originalFile"]?:0]]
        checkNotNull(keyFileRecord) {"Key File entry for: ${fastqFile[fastqColNameMap["originalFile"]?:0]} is missing. "}
        val taxon = keyFileRecord.taxonName
        val fileGroupName = keyFileRecord.fileGroupName



        val outputDebugFile = if(outputSecondaryMappingStats) {"${keyFileNameWithoutExtension}_${taxon}_${fileGroupName}_additionalMappingStats.txt" } else { "" }
        exportHapIdStats(outputDebugFile, hapIdToStatMap, hapIdToLengthMap)

        val multisetMap = hapIdMultiset.getMap()
        val hapIdMapping = multisetMap.keys.map { Pair(it.toList(),multisetMap[it]?:0) }.toMap()

        val outputFileName = "${outputDebugReadMappingDir}${taxon}_${fileGroupName}_ReadMapping.txt"
        myLogger.info("Debug Output Dir Specified:  Writing file:${outputFileName}")

        val finalParamMap = pluginParams.toMutableMap()
        exportReadMapping(outputFileName, hapIdMapping, taxon, fileGroupName, methodName,
                DBLoadingUtils.formatMethodParamsToJSON(finalParamMap))

        loadBitSetEncodedReadMappingsToDB(hapIdMapping, taxon, fileGroupName, pluginParams, methodDescription, phg,
            methodName, keyFileRecordsToMappingId, keyFileRecord, outputDebugReadMappingDir, hapIdToRefRangeMap,
            refRangeToHapIdMap, hapListId, isTestMethod)

        //set and reset variables which span multiple unmerged fastqs
        bestReadMap = mutableMapOf<Pair<String, Boolean>, BestAlignmentGroup>()
        //Need a new hapIdMultiset for each record in the key file.
        hapIdMultiset = HapIdMultiset()
        hapIdToStatMap = mutableMapOf<Int,HapIdStats>()

        println("originalFile: ${fastqFile[fastqColNameMap["originalFile"]?:0]}")
        println("Number of Records: ${fastqFile[fastqColNameMap["numReads"]?:0]}")
        println("Actual processed for batch: ${readCounter}")
        println("Number of unique reads: ${readSet.size}")

    }

    outputKeyFiles(keyFileName, keyFileLines, flowcellCol, phg, taxonCol, methodName, keyFileColumnNameMap)

    phg.close()
}


/**
 * Function to read in the key file.  The first of the pair is the column mapping and the second is a 2-d list.
 */
fun readInKeyFile(fileName: String) : Pair<Map<String,Int>,List<List<String>>> {
    val lines = Utils.getBufferedReader(fileName).readLines()

    val cols = lines[0].split("\t")
    val columnMapping = cols.indices.map { Pair(cols[it],it) }.toMap()

    val linesSplit = (1 until lines.size).map { lines[it] }
            .filter { it != "" }
            .map { it.split("\t") }
            .toList()

   return Pair(columnMapping,linesSplit)
}

/**
 * verify that there are no duplicate entries in the key file.  This is just to let the user know if there is a duplicate.
 */
fun verifyNoDuplicatesInKeyFile(keyFileRecords : List<KeyFileUniqueRecord>) {
    val keySet = mutableSetOf<KeyFileUniqueRecord>()
    for(record in keyFileRecords) {
        if(keySet.contains(record)) {
            myLogger.warn("Duplicate KeyFile Entry Found in the Keyfile:${record.taxonName} ${record.fileGroupName} Using the last entry in the keyfile.")
        }
        keySet.add(record)
    }
}

/**
 * Method to check to see if there are missing files found in the key file but are missing in the Directory.
 * If they are its ok, we expect the keyfile to have more entries than the directory.
 */
fun isKeyEntryInDir(fileNames : HashSet<String>, currentKeyRecord : List<String>, fileCol1 : Int, fileCol2 : Int) : Boolean {
    val file1 = currentKeyRecord[fileCol1]
    return if(fileCol2 == -1) {
        val isValidFilename = fileNames.contains(file1)
        if (!isValidFilename) myLogger.error("input directory does not contain $file1. The directory name should not be part of the file name in the keyfile.")
        isValidFilename
    } else {
        val file2 = currentKeyRecord[fileCol2]
        //condition file2=="" allows filename2 to be blank.
        val areValidFilenames = fileNames.contains(file1) && (file2 == "" || fileNames.contains(file2))
        if (!areValidFilenames) myLogger.error("input directory does not contain both of $file1 and $file2. The directory name should not be part of the filename in the keyfile.")
        areValidFilenames
    }
}

/**
 * Function that sets up the paired or single end minimap commands and builds the SamReader
 *
 * The SamReader is used later to pick optimal hits from the alignments.
 *
 * If secondFastq is "", this will assume that single end is what needs to be run
 */
fun setupMinimapRun(minimapLocation : String, referenceFile : String, firstFastq:String, secondFastq:String = "", maxSecondary : Int = 20, fParameter : String = "f1000,5000") : SamReader {
    val threadnum = Integer.toString(Math.max(TasselPrefs.getMaxThreads() - 1, 1))
    val command = if (secondFastq == "")  arrayOf(minimapLocation, "-ax", "sr", "-t", threadnum, "--secondary=yes","-N$maxSecondary", "-${fParameter}", "--eqx", referenceFile, firstFastq)
                    else arrayOf(minimapLocation, "-ax", "sr", "-t", threadnum,"--secondary=yes", "-N$maxSecondary", "-${fParameter}", "--eqx", referenceFile, firstFastq,secondFastq)

    myLogger.info("Running Minimap2 Command:\n${command.joinToString(" ")}")
    val minimap2Process = ProcessBuilder(*command)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .start()
    val minimap2Out = BufferedInputStream(minimap2Process.inputStream, 5000000)

    //create a SamReader from that
    return SamReaderFactory.makeDefault()
            .validationStringency(ValidationStringency.SILENT).open(SamInputResource.of(minimap2Out))
}


/**
 * Function to score the SAM file.  This will group all the equally best hits for each read and put them into a map.
 */
@Deprecated("Only paired end full Sam Iterator")
fun scoreSAMFile(samReader: SamReader) : Map<Pair<String,Boolean>, BestAlignmentGroup> {
    //To make this work correctly with paired end, we need to keep track of which one of the pairs the read is
    //Loop through the sam file
    val samIterator = samReader.iterator()

    val bestReadMap = mutableMapOf<Pair<String,Boolean>,BestAlignmentGroup>()
    var mappingCounter = 0
    while(samIterator.hasNext()) {
        val currentSamRecord = samIterator.next()
        mappingCounter++
        if(mappingCounter%1000000 == 0) {
            myLogger.info("Processed ${mappingCounter} alignments.")
        }
        if(filterRead(currentSamRecord, true)) {
            continue
        }

        val readName = currentSamRecord.readName
        val isNegStrand = currentSamRecord.readNegativeStrandFlag
        val bestReadMapKey = Pair(readName,isNegStrand)

        //Check to see if its in the map
        if(!bestReadMap.containsKey(bestReadMapKey)) {
            bestReadMap[bestReadMapKey] = BestAlignmentGroup(readName, isNegStrand,
                    currentSamRecord.getIntegerAttribute("NM"),
                    currentSamRecord.readLength,
                    mutableSetOf(currentSamRecord.contig.toInt()),
                    mutableMapOf(Pair(currentSamRecord.contig.toInt(),SecondaryStats(currentSamRecord.getIntegerAttribute("AS"),
                                                                                        currentSamRecord.getAttribute("de") as Float,
                                                                                        currentSamRecord.alignmentStart)))) //THis assumes that we use the hapID as the haplotype name
        }

        else {

            val currentBestAlignmentGroup = bestReadMap[bestReadMapKey]
            val lengthOfBestMapping = currentBestAlignmentGroup?.lengthOfMapping?:0

            val currentEditDistance = currentSamRecord.getIntegerAttribute("NM")
            val currentBestEditDistance = currentBestAlignmentGroup?.bestEditDistance?:Int.MAX_VALUE
            if(currentEditDistance < currentBestEditDistance) {
                //Replace this group with the new mapping
                bestReadMap.replace(bestReadMapKey, BestAlignmentGroup(readName, isNegStrand,
                        currentSamRecord.getIntegerAttribute("NM"),
                        currentSamRecord.readLength,
                        mutableSetOf(currentSamRecord.contig.toInt()),
                        mutableMapOf(Pair(currentSamRecord.contig.toInt(),SecondaryStats(currentSamRecord.getIntegerAttribute("AS"),
                                                                                        currentSamRecord.getAttribute("de") as Float,
                                                                                        currentSamRecord.alignmentStart)))))
            }
            else if(currentEditDistance == currentBestEditDistance){
                //Add this id to the list
                val currentBestGroupList = currentBestAlignmentGroup?.listOfHapIds?: mutableSetOf()
                currentBestGroupList.add(currentSamRecord.contig.toInt())
            }

        }
    }
    return bestReadMap
}

/**
 * Function to score the SAM file when running on a lower memory machine.
 * Essentially instead of creating a giant multimap like before, we will immediately output the needed file for the PathFinding step
 * This will return the same results as the original pipeline with the assumption that minimap2 will always return mappings in read order
 *
 */
@Deprecated("Old version of the Low memory mode.  Replaced by scoreSamFileCountHapSetHits()")
fun scoreSAMFileLowMem(samReader: SamReader, outputFileName: String, hapIdToRefRangeMap: Map<Int, ReferenceRange>, maxRefRangeError: Double, pairedEnd :Boolean ) {
    myLogger.info("Operating in low memory mode. If minimap2 does not return mappings where reads are grouped this will not return accurate results.")
    //Need to be a bit smarter with our processing
    //Reads are coming off sorted by read name
    Utils.getBufferedWriter(outputFileName).use { out ->
        out.write("RefRangeId\tReadName\tsetOfHapIds\n")
        //set up dummy initial read name
        var currentStoredReadName = ""
        val samIterator = samReader.iterator()

        var bestReadMap = mutableMapOf<Pair<String, Boolean>, BestAlignmentGroup>()
        var mappingCounter = 0
        while (samIterator.hasNext()) {
            val currentSamRecord = samIterator.next()
            mappingCounter++
            if (mappingCounter % 1000000 == 0) {
                myLogger.info("Processed ${mappingCounter} alignments.")
            }

            if (filterRead(currentSamRecord, pairedEnd)) {
                continue
            }

            val readName = currentSamRecord.readName
            //Set the read name for the first mapping in the iterator
            if (currentStoredReadName == "") {
                currentStoredReadName = readName
            }
            if (readName != currentStoredReadName) {
                //Need to filter out hapids which span multiple reference ranges
                val bestHitMap = keepHapIdsForSingleRefRange(bestReadMap, hapIdToRefRangeMap, maxRefRangeError)

                //should only be one read name, but this code works for the full mem so I copied it and adapted
                val readNames = bestHitMap.keys
                        .map { it.first }
                        .toSet()

                if(pairedEnd) {
                    convertHitMapToSingleHapMapping(readNames,bestHitMap,hapIdToRefRangeMap,
                            {name, hitMap -> hitMap.containsKey(Pair(name,true)) && hitMap.containsKey(Pair(name,false))},
                            {name, hitMap ->  val forwardMappings = hitMap[Pair(name,false)]?.listOfHapIds?:listOf<Int>()
                                val reverseMappings = hitMap[Pair(name,true)]?.listOfHapIds?: listOf<Int>()
                                Pair(name,forwardMappings.intersect(reverseMappings))})
                            .forEach {
                                out.write("${it.first?.id()}\t${currentStoredReadName}\t${it.second.hapIdSet.joinToString(",")}\n") //Write out what is needed for the file
                            }
                }
                else {
                    convertHitMapToSingleHapMapping(readNames,bestHitMap,hapIdToRefRangeMap,
                            {name,hitMap -> hitMap.containsKey(Pair(name, false))},//Create a filtering lambda and pass it
                            {name,hitMap -> Pair(name,hitMap[Pair(name,false)]?.listOfHapIds?:setOf<Int>())}) //Create a mapping lambda and pass it
                            .forEach {
                                out.write("${it.first?.id()}\t${currentStoredReadName}\t${it.second.hapIdSet.joinToString(",")}\n") //Write out what is needed for the file
                            }
                }
                //reset the map and current name
                currentStoredReadName = readName //update the read name
                bestReadMap = mutableMapOf() //reset the bestReadMap so we can add new entries to it
            }


            val isNegStrand = if(!pairedEnd) pairedEnd else currentSamRecord.readNegativeStrandFlag
            val bestReadMapKey = Pair(readName, isNegStrand)

            //Check to see if its in the map
            if (!bestReadMap.containsKey(bestReadMapKey)) {
                bestReadMap[bestReadMapKey] = BestAlignmentGroup(readName, isNegStrand,
                        currentSamRecord.getIntegerAttribute("NM"),
                        currentSamRecord.readLength,
                        mutableSetOf(currentSamRecord.contig.toInt()),
                        mutableMapOf(Pair(currentSamRecord.contig.toInt(),SecondaryStats(currentSamRecord.getIntegerAttribute("AS"),
                        currentSamRecord.getAttribute("de") as Float,
                                currentSamRecord.alignmentStart)))) //THis assumes that we use the hapID as the haplotype name
            } else {
                //We have the key already in our map, so we need to see if the edit distance is lower
                val currentBestAlignmentGroup = bestReadMap[bestReadMapKey]

                val currentEditDistance = currentSamRecord.getIntegerAttribute("NM")
                val currentBestEditDistance = currentBestAlignmentGroup?.bestEditDistance ?: Int.MAX_VALUE
                if (currentEditDistance < currentBestEditDistance) {
                    //Replace this group with the new mapping
                    bestReadMap.replace(bestReadMapKey, BestAlignmentGroup(readName, isNegStrand,
                            currentSamRecord.getIntegerAttribute("NM"),
                            currentSamRecord.readLength,
                            mutableSetOf(currentSamRecord.contig.toInt()),mutableMapOf(Pair(currentSamRecord.contig.toInt(),SecondaryStats(currentSamRecord.getIntegerAttribute("AS"),
                            currentSamRecord.getAttribute("de") as Float,
                            currentSamRecord.alignmentStart)))))
                } else if (currentEditDistance == currentBestEditDistance) {
                    //Add this id to the list
                    val currentBestGroupList = currentBestAlignmentGroup?.listOfHapIds ?: mutableSetOf()
                    currentBestGroupList.add(currentSamRecord.contig.toInt())
                }

            }
        }
    }

}

/**
 * Function to score a sam record.  This will output a Map<List<Int>,Int> which is the hapId Hit set.
 *
 * For each subset of hapids we return a count of how many reads hit that exact subset.  This mapping is used in the HMM.
 */
fun scoreSamFileCountHapSetHits(samReader: SamReader, hapIdToRefRangeMap: Map<Int, ReferenceRange>, maxRefRangeError: Double, pairedEnd :Boolean, outputDebugFile : String, hapIdToLengthMap : Map<Int,Int> ) : Map<List<Int>,Int> {
    myLogger.info("Running in Low memory mode.  Simply counting the number of reads which hit a given set of haplotype ids")

    var currentStoredReadName = ""
    val samIterator = samReader.iterator()

    var bestReadMap = mutableMapOf<Pair<String, Boolean>, BestAlignmentGroup>()
    var mappingCounter = 0

    val hapIdMultiset = HapIdMultiset()

    val hapIdToStatMap = mutableMapOf<Int,HapIdStats>()
    var totalNextTimeList = mutableListOf<Double>()
    while (samIterator.hasNext()) {
        val nextStartTime = System.nanoTime()
        val currentSamRecord = samIterator.next()
        totalNextTimeList.add((System.nanoTime() - nextStartTime)/1E9)
        mappingCounter++
        if (mappingCounter % 1000000 == 0) {
            myLogger.info("Processed ${mappingCounter} alignments.")
        }

        if (filterRead(currentSamRecord, pairedEnd)) {
            continue
        }
        val readName = currentSamRecord.readName
        //Set the read name for the first mapping in the iterator
        if (currentStoredReadName == "") {
            currentStoredReadName = readName
        }
        if (readName != currentStoredReadName) {
            addBestReadMapToHapIdMultiset(bestReadMap, hapIdToRefRangeMap, maxRefRangeError, pairedEnd, hapIdMultiset, hapIdToStatMap)
            //reset the map and current name
            currentStoredReadName = readName //update the read name
            bestReadMap = mutableMapOf() //reset the bestReadMap so we can add new entries to it
        }

        attemptToAddSAMRecordToBestReadMap(pairedEnd, currentSamRecord, readName, bestReadMap)
    }
    //Need to do it one last time to be sure we process the last read
    addBestReadMapToHapIdMultiset(bestReadMap, hapIdToRefRangeMap, maxRefRangeError, pairedEnd, hapIdMultiset, hapIdToStatMap)



    myLogger.info("TotalTiming For samReader.next:${totalNextTimeList.sum()}")
//    println(totalNextTimeList.joinToString(","))
    //Exporting hapId->Stats
    exportHapIdStats(outputDebugFile, hapIdToStatMap, hapIdToLengthMap)

    val multisetMap = hapIdMultiset.getMap()
    return multisetMap.keys.map { Pair(it.toList(),multisetMap[it]?:0) }.toMap()
}

/**
 * Function to export the HapIdStats file.  If outputDebugFile is not specified, this will do nothing.
 */
private fun exportHapIdStats(outputDebugFile: String, hapIdToStatMap: MutableMap<Int, HapIdStats>, hapIdToLengthMap: Map<Int, Int>) {
    if (outputDebugFile != "") {
        Utils.getBufferedWriter(outputDebugFile).use { out ->
            out.write("hapId\thapLength\tcount\ttotalNM\ttotalAS\ttotalDE\tlistOfStartPos\n")
            hapIdToStatMap.keys
                    .map { Pair(hapIdToStatMap[it]!!, hapIdToLengthMap[it]) }
                    .forEach { statsPair ->
                        val stats = statsPair.first
                        val hapLength = statsPair.second
                        out.write("${stats.hapId}\t${hapLength}\t${stats.count}\t${stats.NM}\t${stats.AS}\t${stats.de}\t${stats.startPositions.joinToString(",")}\n")
                    }
        }
    }
}

/**
 * Function to try to add a SAM record to bestReadMap.
 * bestReadMap holds the currently best know set of reads which have the best(lowest) NM.
 * If the current Record is better, we replace the old entry with a new one.
 * If the current Record is the same, we add it to the list
 * If the current Record is worse, we ignore.
 */
private fun attemptToAddSAMRecordToBestReadMap(pairedEnd: Boolean, currentSamRecord: SAMRecord, readName: String, bestReadMap: MutableMap<Pair<String, Boolean>, BestAlignmentGroup>) {
    val isNegStrand = if (!pairedEnd) pairedEnd else currentSamRecord.readNegativeStrandFlag
    val bestReadMapKey = Pair(readName, isNegStrand)

    //Check to see if its in the map
    if (!bestReadMap.containsKey(bestReadMapKey)) {
        bestReadMap[bestReadMapKey] = BestAlignmentGroup(readName, isNegStrand,
                currentSamRecord.getIntegerAttribute("NM"),
                currentSamRecord.readLength,
                mutableSetOf(currentSamRecord.contig.toInt()), mutableMapOf(Pair(currentSamRecord.contig.toInt(), SecondaryStats(currentSamRecord.getIntegerAttribute("AS"),
                currentSamRecord.getAttribute("de") as Float,
                currentSamRecord.alignmentStart)))) //THis assumes that we use the hapID as the haplotype name
    } else {
        //We have the key already in our map, so we need to see if the edit distance is lower
        val currentBestAlignmentGroup = bestReadMap[bestReadMapKey]

        val currentEditDistance = currentSamRecord.getIntegerAttribute("NM")
        val currentBestEditDistance = currentBestAlignmentGroup?.bestEditDistance ?: Int.MAX_VALUE
        if (currentEditDistance < currentBestEditDistance) {
            //Replace this group with the new mapping
            bestReadMap.replace(bestReadMapKey, BestAlignmentGroup(readName, isNegStrand,
                    currentSamRecord.getIntegerAttribute("NM"),
                    currentSamRecord.readLength,
                    mutableSetOf(currentSamRecord.contig.toInt()), mutableMapOf(Pair(currentSamRecord.contig.toInt(), SecondaryStats(currentSamRecord.getIntegerAttribute("AS"),
                    currentSamRecord.getAttribute("de") as Float,
                    currentSamRecord.alignmentStart)))))
        } else if (currentEditDistance == currentBestEditDistance) {
            //Add this id to the list
            val currentBestGroupList = currentBestAlignmentGroup?.listOfHapIds ?: mutableSetOf()
            if (!currentBestGroupList.contains(currentSamRecord.contig.toInt())) {
                (currentBestAlignmentGroup?.hapIdToSecondaryStats
                        ?: mutableMapOf()).put(currentSamRecord.contig.toInt(), SecondaryStats(currentSamRecord.getIntegerAttribute("AS"),
                        currentSamRecord.getAttribute("de") as Float,
                        currentSamRecord.alignmentStart))
            }
            currentBestGroupList.add(currentSamRecord.contig.toInt())

        }

    }
}

private fun addBestReadMapToHapIdMultiset(bestReadMap: MutableMap<Pair<String, Boolean>, BestAlignmentGroup>, hapIdToRefRangeMap: Map<Int, ReferenceRange>, maxRefRangeError: Double, pairedEnd: Boolean, hapIdMultiset: HapIdMultiset, hapIdToStatMap: MutableMap<Int, HapIdStats>) {
    //Need to filter out hapids which span multiple reference ranges
    val bestHitMap = keepHapIdsForSingleRefRange(bestReadMap, hapIdToRefRangeMap, maxRefRangeError)

    //should only be one read name, but this code works for the full mem so I copied it and adapted
    val readNames = bestHitMap.keys
            .map { it.first }
            .toSet()

    if (pairedEnd) {
        convertHitMapToSingleHapMapping(readNames, bestHitMap, hapIdToRefRangeMap,
                { name, hitMap -> hitMap.containsKey(Pair(name, true)) && hitMap.containsKey(Pair(name, false)) },
                { name, hitMap ->
                    val forwardMappings = hitMap[Pair(name, false)]?.listOfHapIds ?: listOf<Int>()
                    val reverseMappings = hitMap[Pair(name, true)]?.listOfHapIds ?: listOf<Int>()
                    Pair(name, forwardMappings.intersect(reverseMappings))
                })
                .forEach {
//                            out.write("${it.first?.id()}\t${currentStoredReadName}\t${it.second.hapIdSet.joinToString(",")}\n") //Write out what is needed for the file
                    hapIdMultiset.add(it.second.hapIdSet)
                    it.second.hapIdSet.forEach { hapId ->

                        val secondaryStats = bestHitMap[Pair(readNames.first(), false)]?.hapIdToSecondaryStats?.get(hapId)
                        addSecondaryStats(hapIdToStatMap, hapId, bestHitMap, readNames, secondaryStats, false)
                        val secondaryStatsForward = bestHitMap[Pair(readNames.first(), true)]?.hapIdToSecondaryStats?.get(hapId)
                        addSecondaryStats(hapIdToStatMap, hapId, bestHitMap, readNames, secondaryStats, true)
                    }
                }
    } else {
        convertHitMapToSingleHapMapping(readNames, bestHitMap, hapIdToRefRangeMap,
                { name, hitMap -> hitMap.containsKey(Pair(name, false)) },//Create a filtering lambda and pass it
                { name, hitMap ->
                    Pair(name, hitMap[Pair(name, false)]?.listOfHapIds ?: setOf<Int>())
                }) //Create a mapping lambda and pass it
                .forEach {
//                            out.write("${it.first?.id()}\t${currentStoredReadName}\t${it.second.hapIdSet.joinToString(",")}\n") //Write out what is needed for the file
                    hapIdMultiset.add(it.second.hapIdSet)
                    it.second.hapIdSet.forEach { hapId ->

                        val secondaryStats = bestHitMap[Pair(readNames.first(), false)]?.hapIdToSecondaryStats?.get(hapId)
                        addSecondaryStats(hapIdToStatMap, hapId, bestHitMap, readNames, secondaryStats, false)
                    }
                }
    }
}

/**
 * Function to add the secondary stats to the hapIdToStatMap even when the read is not already there.
 */
private fun addSecondaryStats(hapIdToStatMap: MutableMap<Int, HapIdStats>, hapId: Int, bestHitMap: Map<Pair<String, Boolean>, BestAlignmentGroup>, readNames: Set<String>, secondaryStats: SecondaryStats?, strand: Boolean) {
    if (!hapIdToStatMap.containsKey(hapId)) {
        hapIdToStatMap[hapId] = HapIdStats(hapId, 1, bestHitMap[Pair(readNames.first(), strand)]?.bestEditDistance ?: 0,
                secondaryStats?.AS ?: 0, secondaryStats?.de ?: 1.0f, mutableListOf(secondaryStats?.startPosition ?: -1))
    } else {
        hapIdToStatMap[hapId]?.addStats(1, bestHitMap[Pair(readNames.first(), strand)]?.bestEditDistance ?: 0,
                secondaryStats?.AS ?: 0, secondaryStats?.de ?: 1.0f, secondaryStats?.startPosition ?: -1)
    }
}

/**
 * Function to keep hapids if they are hitting multiple reference ranges too frequently.  If there is a little bit of noise it can be filtered.
 */
fun keepHapIdsForSingleRefRange(bestHitMap: Map<Pair<String,Boolean>, BestAlignmentGroup>, hapIdToRangeMap: Map<Int, ReferenceRange>, maxRefRangeError: Double) : Map<Pair<String,Boolean>, BestAlignmentGroup> {
    return bestHitMap.entries
            .map{
                if(spansSingleRefRange(it,hapIdToRangeMap)) {
                    Pair(it.key,it.value)
                }
                else {
                    removeExtraRefRangeHits(it,hapIdToRangeMap, maxRefRangeError)
                }
            }
            .filter { it.second.listOfHapIds.size>0 } //Filter out any reads which do not have any mappings
            .toMap()
}

/**
 * Function to check to see if the reads only hit one reference range
 */
fun spansSingleRefRange(currentMapping: Map.Entry<Pair<String, Boolean>, BestAlignmentGroup>, hapIdToRangeMap : Map<Int, ReferenceRange>): Boolean {
    val hapIds = currentMapping.value.listOfHapIds

    val numberOfRefRangeGroups = hapIds.map { hapIdToRangeMap[it]?.id() }
            .groupBy { it }
            .count()
    return (numberOfRefRangeGroups==1)
}

/**
 * Function to remove any reads which hit more than one reference range ambiguously.
 */
fun removeExtraRefRangeHits(currentMapping: Map.Entry<Pair<String, Boolean>, BestAlignmentGroup>, hapIdToRangeMap : Map<Int, ReferenceRange>, maxRefRangeError: Double) : Pair<Pair<String,Boolean>,BestAlignmentGroup> {
    val bestMappings = currentMapping.value
    val hapIds = bestMappings.listOfHapIds

    val refRangeToHapIdMultimap = HashMultimap.create<Int,Int>()
    hapIds.forEach {
        check(hapIdToRangeMap.containsKey(it)) {"HapID ${it} is not found in the graph.  HapID ${it} is was aligned against which means that it is in the Pangenome.fa, but not in the graph passed to FastqToReadMappingPlugin or SAMToReadMappingPlugin.  Please check and make sure that the methods used when creating the graph match the methods used when creating the pangenome."}
        refRangeToHapIdMultimap.put(hapIdToRangeMap[it]?.id(),it)
    }

    val highestCountRefRangeId = findBestRefRange(refRangeToHapIdMultimap,maxRefRangeError)

    if(highestCountRefRangeId == -1) {
        return Pair(currentMapping.key,BestAlignmentGroup(bestMappings.readName,bestMappings.strand,bestMappings.bestEditDistance,bestMappings.lengthOfMapping, mutableSetOf(), mutableMapOf()))
    }

    //Now that we know what the highest count was, we can create a new Best Alignment Group
    return Pair(currentMapping.key,BestAlignmentGroup(bestMappings.readName,bestMappings.strand,bestMappings.bestEditDistance,bestMappings.lengthOfMapping,refRangeToHapIdMultimap[highestCountRefRangeId],bestMappings.hapIdToSecondaryStats))
}

/**
 * Function to check to see if the read hits a multiple reference ranges less than maxRefRangeError
 * Basically this is to filter out any reads which hit multiple reference ranges equally well
 */
fun findBestRefRange(refRangeToIdMapping : Multimap<Int, Int>, maxRefRangeError:Double) : Int {
    var bestRefRange = -1
    var totalNumberOfHits = 0
    var bestCount = 0

    refRangeToIdMapping.keySet().forEach {
        if(refRangeToIdMapping[it].size > bestCount) {
            bestCount = refRangeToIdMapping[it].size
            bestRefRange = it
        }
        totalNumberOfHits+= refRangeToIdMapping[it].size
    }

    if(maxRefRangeError <= 1 - (bestCount.toDouble()/totalNumberOfHits)) {
        return -1
    }

    return bestRefRange
}

/**
 * Function to create all the multimap hits for a single reference range.  This will remove alignments where the both reads are not agreeing on the same set of hapIds
 */
fun createMultimapHitsByRefRange(bestHitMap : Map<Pair<String,Boolean>, BestAlignmentGroup>, hapIdToRangeMap: Map<Int, ReferenceRange> ) : Multimap<ReferenceRange,SingleHapMapping> {
    val mappingsByReferenceRange = HashMultimap.create<ReferenceRange,SingleHapMapping>()

    val readNames = bestHitMap.keys
            .map { it.first }
            .toSet()

    convertHitMapToSingleHapMapping(readNames,bestHitMap,hapIdToRangeMap,
            {name, hitMap -> hitMap.containsKey(Pair(name,true)) && hitMap.containsKey(Pair(name,false))},
            {name, hitMap ->  val forwardMappings = hitMap[Pair(name,false)]?.listOfHapIds?:listOf<Int>()
                val reverseMappings = hitMap[Pair(name,true)]?.listOfHapIds?: listOf<Int>()
                Pair(name,forwardMappings.intersect(reverseMappings))})
            .forEach { mappingsByReferenceRange.put(it.first,it.second) }

    return mappingsByReferenceRange
}

//Keeping this commented, but still uploading in case we want to bring this in in the future.
//data class AlignmentRecordPair(val name:String, val firstHapId:Int, val firstPos:Int, val firstNM:Int,
//                               val firstCIGAR:String, val secondHapId:Int, val secondPos:Int, val secondNM:Int, val secondCIGAR:String)
//
//fun createReadMappingsMultipleIndices(samRecordFiles : List<String>) :Map<SortedSet<Int>, Int> {
//
//    val bestAlignmentMap = mutableMapOf<String, MutableList<AlignmentRecordPair>>()
//
//    for(sam in samRecordFiles) {
//
//        val samRecords = loadSAMFileIntoSAMReader(sam).iterator()
//        while(samRecords.hasNext()) {
//            val alignment = samRecords.next()
//            //TODO figure out how to pair off alignments(existing code)
//            val name = alignment.readName
//            if(bestAlignmentMap.containsKey(name)) {
//                val alignNM = alignment.getAttribute("NM") as Int
//                if(alignNM < bestAlignmentMap[name]?.first()?.firstNM?:0) {
//                    bestAlignmentMap[name] = mutableListOf(buildAlignmentRecord(alignment))
//                }
//                else if(alignNM == bestAlignmentMap[name]?.first()?.firstNM?:0) {
//                    bestAlignmentMap[name]?.add(buildAlignmentRecord(alignment))
//                }
//                else {
//                    //Ignore not a good alignment
//                }
//            }
//            else {
//                //add to map
//                bestAlignmentMap[name] = mutableListOf(buildAlignmentRecord(alignment))
//            }
//        }
//    }
//
//    bestAlignmentMap.map{ it.value.map{ align -> align.firstHapId }.toSortedSet() }
//            .groupingBy{ it }
//            .eachCount()
//}