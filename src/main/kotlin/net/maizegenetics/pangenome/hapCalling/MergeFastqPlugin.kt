package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.BufferedWriter
import java.io.File
import javax.swing.ImageIcon

/**
 * Plugin to combine all fastq files in the input -fastqDir into sets of combined Fastqs.
 * he number to be batched together is set by -numToMerge.  It will also generate a template script for running minimap2
 * If -useOriginalReadNames is set to true, the fastq records will not be renamed.
 * This should only be used if the read names are unique in each fastq.  Otherwise minimap2 will mix samples.
 *
 * It will also output a Grouping file which is used in MultisampleBAMToMappingPlugin so the PHG knows how to split back out the alignments for the reads.
 */
class MergeFastqPlugin(parentFrame: Frame?=null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(MergeFastqPlugin::class.java)

    private var fastqDir = PluginParameter.Builder("fastqDir", null, String::class.java)
            .guiName("Fastq dir to process")
            .inDir()
            .required(true)
            .description("Name of the Fastq dir to process.")
            .build()

    private var outputMergedFastqDir = PluginParameter.Builder("outputDir", null, String::class.java)
            .guiName("Directory to write out merged fastq files.")
            .outDir()
            .required(true)
            .description("Directory to write out the fastq files.")
            .build()

    private var outputGroupingFile = PluginParameter.Builder("outputGroupingFile", null, String::class.java)
            .guiName("File To output the groupings which have been merged.")
            .outFile()
            .required(true)
            .description("File to write out the groupings.")
            .build()

    private var numberOfFastqsPerMerge = PluginParameter.Builder("numToMerge", 50, Int::class.javaObjectType)
            .guiName("Number of Fastas To Merge")
            .required(false)
            .description("Number of fastqs to merge into a single fastq")
            .build()


    private var scriptTemplate = PluginParameter.Builder("scriptTemplate","runMinimapTemp.sh", String::class.java)
            .guiName("Template Script Name")
            .outFile()
            .required(false)
            .description("Generated Template Script for future minimap runs.  This makes it easier to run minimap on the merged fastq files.")
            .build()

    private var useOriginalReadNames = PluginParameter.Builder("useOriginalReadNames", false, Boolean::class.javaObjectType)
            .guiName("Use Original Read Names")
            .required(false)
            .description("Use the original ReadNames instead of a running count")
            .build()

    override fun processData(input: DataSet?): DataSet? {

        //walk through each file
        val filesGrouped = File(fastqDir()).walkTopDown()
                .filter { it.isFile }
                .filter { !it.isHidden }
                .windowed(numberOfFastqsPerMerge(), numberOfFastqsPerMerge(), true)
                .toList()
        Utils.getBufferedWriter(outputGroupingFile()).use { outputFile ->
            Utils.getBufferedWriter(scriptTemplate()).use { outputScript ->
                outputScript.write("#!/bin/bash -x\n")
                outputFile.write("originalFile\tnewFile\tBatch\tnumReads\n")
                val totalBatchCount = filesGrouped.size
                for ((index, fastqFileList) in filesGrouped.withIndex()) {
                    myLogger.info("Writing out batch ${index} out of ${totalBatchCount}")
                    val fastqOutputName = "mergedFastq_batch_${index}.fq"
                    val bamOutputName = "mergedFastq_batch_${index}.bam"
                    mergeFiles(fastqOutputName, outputFile, fastqFileList, index)
                    outputScript.write("time minimap2 -a -x sr -t 20 --secondary=yes -N50 -f1000,5000 --eqx -Q <refIndex> ${outputMergedFastqDir()}${fastqOutputName} | samtools view -b > <bamFolder>/${bamOutputName}\n")

                }
            }
        }

        return null
    }

    fun mergeFiles(mergedFileName : String, outputGroupingWriter : BufferedWriter, fileList : List<File>, batchId:Int) {
        Utils.getBufferedWriter("${outputMergedFastqDir()}${mergedFileName}").use { outputMergedFastaWriter ->
            var readNumber = 0
            for(fastqFile in fileList) {
                var totalReadNumForBatch = 0
                Utils.getBufferedReader(fastqFile.toString()).use { reader ->
                    var line:String? = ""//reader.readLine()
                    var lineNumber = 1
                    while (line != null) {
                        line = reader.readLine() //header
                        if(useOriginalReadNames()) {
                            outputMergedFastaWriter.write("${line}\n")
                        }
                        else {
                            outputMergedFastaWriter.write("@${readNumber}\n")
                        }
                        readNumber++
                        totalReadNumForBatch++
                        line = reader.readLine() //sequence
                        outputMergedFastaWriter.write("${line}\n")
                        line = reader.readLine() //separator
                        outputMergedFastaWriter.write("${line}\n")
                        line = reader.readLine() //qual
                        outputMergedFastaWriter.write("${line}\n")
                    }
                }
                outputGroupingWriter.write("${fastqFile.name}\t${mergedFileName}\t${batchId}\t${totalReadNumForBatch}\n")
            }

        }
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = MergeFastqPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "MergeFastqPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to Merge Fastqs"
    }
    /**
     * Name of the Fastq dir to process.
     *
     * @return Fastq dir to process
     */
    fun fastqDir(): String {
        return fastqDir.value()
    }

    /**
     * Set Fastq dir to process. Name of the Fastq dir to
     * process.
     *
     * @param value Fastq dir to process
     *
     * @return this plugin
     */
    fun fastqDir(value: String): MergeFastqPlugin {
        fastqDir = PluginParameter<String>(fastqDir, value)
        return this
    }

    /**
     * Directory to write out the fastq files.
     *
     * @return Directory to write out merged fastq files.
     */
    fun outputMergedFastqDir(): String {
        return outputMergedFastqDir.value()
    }

    /**
     * Set Directory to write out merged fastq files.. Directory
     * to write out the fastq files.
     *
     * @param value Directory to write out merged fastq files.
     *
     * @return this plugin
     */
    fun outputMergedFastqDir(value: String): MergeFastqPlugin {
        outputMergedFastqDir = PluginParameter<String>(outputMergedFastqDir, value)
        return this
    }

    /**
     * File to write out the groupings.
     *
     * @return File To output the groupings which have been merged.
     */
    fun outputGroupingFile(): String {
        return outputGroupingFile.value()
    }

    /**
     * Set File To output the groupings which have been merged..
     * File to write out the groupings.
     *
     * @param value File To output the groupings which have been merged.
     *
     * @return this plugin
     */
    fun outputGroupingFile(value: String): MergeFastqPlugin {
        outputGroupingFile = PluginParameter<String>(outputGroupingFile, value)
        return this
    }

    /**
     * Number of fastqs to merge into a single fastq
     *
     * @return Number of Fastas To Merge
     */
    fun numberOfFastqsPerMerge(): Int {
        return numberOfFastqsPerMerge.value()
    }

    /**
     * Set Number of Fastas To Merge. Number of fastqs to
     * merge into a single fastq
     *
     * @param value Number of Fastas To Merge
     *
     * @return this plugin
     */
    fun numberOfFastqsPerMerge(value: Int): MergeFastqPlugin {
        numberOfFastqsPerMerge = PluginParameter<Int>(numberOfFastqsPerMerge, value)
        return this
    }

    /**
     * Template Script for minimap runs using the generated
     * file
     *
     * @return Template Script Name
     */
    fun scriptTemplate(): String {
        return scriptTemplate.value()
    }

    /**
     * Set Template Script Name. Template Script for minimap
     * runs using the generated file
     *
     * @param value Template Script Name
     *
     * @return this plugin
     */
    fun scriptTemplate(value: String): MergeFastqPlugin {
        scriptTemplate = PluginParameter<String>(scriptTemplate, value)
        return this
    }

    /**
     * Use the original ReadNames instead of a running count
     *
     * @return Use Original Read Names
     */
    fun useOriginalReadNames(): Boolean {
        return useOriginalReadNames.value()
    }

    /**
     * Set Use Original Read Names. Use the original ReadNames
     * instead of a running count
     *
     * @param value Use Original Read Names
     *
     * @return this plugin
     */
    fun useOriginalReadNames(value: Boolean): MergeFastqPlugin {
        useOriginalReadNames = PluginParameter<Boolean>(useOriginalReadNames, value)
        return this
    }

}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(MergeFastqPlugin::class.java)
}