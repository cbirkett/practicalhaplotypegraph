package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultimap
import com.google.common.collect.HashMultiset
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import net.maizegenetics.pangenome.api.CreateGraphUtils
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.plugindef.*
import java.awt.Frame
import java.util.ArrayList
import javax.swing.ImageIcon
import org.apache.log4j.Logger
import java.io.PrintWriter
import kotlin.math.max

/**
 * DiploidPathPlugin finds the most likely pair of paths through all ordered pair of nodes in a HaplotypeGraph for each sample listed in a keyfile. The keyfile together with the read method name provide the information needed to pull
 * read mappings from a PHG database.
 * The HaplotypeGraph must be supplied to the performFunction method while the keyFile name must be supplied as a parameter.
 */
class DiploidPathPlugin(parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    constructor() : this(null, false)

    private val myLogger = Logger.getLogger(DiploidPathPlugin::class.java)

    private var keyFile = PluginParameter.Builder("keyFile", null, String::class.java)
            .description("KeyFile file name.  Must be a tab separated file using the following headers:\n" +
                    "SampleName\tReadMappingIds\tLikelyParents\n" +
                    "ReadMappingIds and LikelyParents need to be comma separated for multiple values")
            .required(true)
            .inFile()
            .build()

    private var readMethodName = PluginParameter.Builder("readMethod", null, String::class.java)
            .required(true)
            .description("The name of the read mapping method in the PHG DB")
            .guiName("Read Mapping Method")
            .build()

    private var pathMethodName = PluginParameter.Builder("pathMethod", null, String::class.java)
            .required(true)
            .description("The name of the path method used to write the results to the PHG DB")
            .guiName("Path Method")
            .build()

    private var pathMethodDescription = PluginParameter.Builder("pathMethodDescription", null, String::class.java)
            .description("An additional description that will be stored with the path method name, if desired.")
            .build()

    private var minTaxaPerRange = PluginParameter.Builder("minTaxa", 20, Int::class.javaObjectType)
            .description("minimum number of taxa per anchor reference range. Ranges with fewer taxa will not be included in the output node list.")
            .build()

    private var probabilityCorrect = PluginParameter.Builder("probCorrect", 0.99, Double::class.javaObjectType)
            .description("The probability that a read mapped to the correct haplotypes")
            .guiName("Probability Correct")
            .build()

    private var minTransitionProbability = PluginParameter.Builder("minTransition", 0.001, Double::class.javaObjectType)
            .description("The minimum transition probability between a pair of nodes in adjacent reference ranges.")
            .guiName("Min Transition Probability")
            .build()

    private var maxNodesPerRange = PluginParameter.Builder("maxHap", 11, Int::class.javaObjectType)
            .description("Any range with more than maxHap haplotypes will not be included in the path.")
            .guiName("Maximum Number of Haplotypes")
            .build()

    private var minReadsPerRange = PluginParameter.Builder("minReads", 1, Int::class.javaObjectType)
            .description("Any range with fewer than minReads will not be included in the path.")
            .guiName("Minimum Read Number")
            .build()

    private var removeEqualCounts = PluginParameter.Builder("removeEqual", false, Boolean::class.javaObjectType)
            .description("Any range for which all haplotypes have the same number of read counts will not be included in the path.")
            .guiName("Remove Equal")
            .build()

    private var maxReadsPerKB = PluginParameter.Builder("maxReadsKB", 1000, Int::class.javaObjectType)
            .description("Any range with more than maxReadsKB reads per kilobase of sequence will not be included in the path.")
            .guiName("Maximum Reads per KB")
            .build()

    private var splitNodes = PluginParameter.Builder("splitNodes", false, Boolean::class.javaObjectType)
            .description("If splitTaxa is true, then each taxon will be assigned its own node in the graph prior to path finding.")
            .guiName("Split Taxa")
            .build()

    private var splitTransitionProb = PluginParameter.Builder("splitProb", 0.99, Double::class.javaObjectType)
            .description("The transition probability for moving between nodes of the same taxon will be set to this number. ")
            .guiName("Split Probability")
            .build()

    private var myNumThreads = PluginParameter.Builder("numThreads", 3, Int::class.javaObjectType)
            .description("Number of threads used to find paths.  The path finding will subtract 2 from this number to have the number of worker threads.  It leaves 1 thread for IO to the DB and 1 thread for the Operating System.")
            .required(false)
            .build()

    private var classicAlgorithm = PluginParameter.Builder("classicAlgorithm", false, Boolean::class.javaObjectType)
        .description("Use the classic algorithm to compute the Viterbi algorithm as described in Rabiner 1989. Significantly slower than the default algorithm.")
        .build()

    private var inbreedingCoef = PluginParameter.Builder("inbreedCoef", 0.0, Double::class.javaObjectType)
        .description("The coefficient of inbreeding, f, for the taxa being imputed.")
        .build()

    private var maxParents = PluginParameter.Builder("maxParents", Int.MAX_VALUE, Int::class.javaObjectType)
        .description("To restrict path finding to the most likely parents, the number of parents used " +
                "will not be greater than maxParents. The number of parents used will be the minimum of maxParents " +
                "and the number of parents needed to reach minCoverage. If both maxParents and minCoverage are left " +
                "at the default, all parents in the input HaplotypeGraph will be used.")
        .build()

    private var minCoverage = PluginParameter.Builder("minCoverage", 1.0, Double::class.javaObjectType)
        .description("To restrict path finding to the most likely parents, the smallest number of parents " +
                "needed to provide read coverage greater than or equal to minCoverage will be used to find paths. " +
                "If maxParents is smaller, that number of parents will be used.")
        .build()

    private var likelyParentFile = PluginParameter.Builder("parentOutputFile", null, String::class.java)
        .outFile()
        .description("The name and path of the file of likely parents and their read counts. " +
                "If no file name is provided the likely parents will not be written to a file.")
        .build()

    private var isTestMethod = PluginParameter.Builder("isTestMethod", false, Boolean::class.javaObjectType)
        .description("Indication if the data is to be loaded against a test method. Data loaded with test methods are not cached with the PHG ktor server")
        .required(false).build()

    override fun processData(input: DataSet?): DataSet {
        if (input == null) throw java.lang.IllegalArgumentException("Must supply a HaplotypeGraph")

        val graphData = input.getDataOfType(HaplotypeGraph::class.java)
        if (graphData.size != 1) throw java.lang.IllegalArgumentException("Must supply exactly one HaplotypeGraph")
        val myGraph = graphData[0].data as HaplotypeGraph

        //if splitNodes then reset minTransition to (1 - splitProb)/number of taxa
        if (splitNodes()) {
            val splitMinTransition = (1 - splitTransitionProb()) / myGraph.totalNumberTaxa()
            if (splitMinTransition < minTransitionProbability()) {
                myLogger.info("minTransition was reset to $splitMinTransition because splitProb=true")
                minTransitionProbability(splitMinTransition)
            }
        }

        if (numThreads() > 3) processKeyfileMultithreaded(myGraph)
        else processKeyfile(myGraph)

        return DataSet.getDataSet(ArrayList<DataSet>())  //empty list
    }

    private fun processKeyfile(myGraph: HaplotypeGraph) {

        val hapIdToRefRangeMapping = myGraph.referenceRanges().flatMap { myGraph.nodes(it) }
            .associate { Pair(it.id(), it.referenceRange()) }

        //create a PHGdbAccess instance for loading paths
        val readMappingDecoder = ReadMappingDecoder()
        val phg = readMappingDecoder.phgAccess

        //create a PathFactory for the efficient algorithm
        //this filters on minTaxaPerRange and requiredTaxaList
        val pathFactory = PathFinderForSingleTaxonNodesFactory(
            CreateGraphUtils.addMissingSequenceNodes(myGraph),
            minTaxaPerRange = minTaxaPerRange(),
            minReadsPerRange = minReadsPerRange(),
            removeEqual = removeEqualCounts(),
            maxReadsPerKB = maxReadsPerKB(),
            sameGameteProbability = splitTransitionProb(),
            probCorrect = probabilityCorrect(),
            inbreedCoef = inbreedingCoef(),
            maxParents = maxParents(),
            minCoverage = minCoverage()
        )

        //Parse the keyfile
        val keyFileParsed = readInKeyFile(keyFile.value())
        val headers = keyFileParsed.first
        val entries = keyFileParsed.second

        val sampleNameColIndex = headers["SampleName"] ?: -1
        val readMappingColIndex = headers["ReadMappingIds"] ?: -1
        val likelyParentColIndex = headers["LikelyParents"] ?: -1

        check(sampleNameColIndex != -1) { "Error processing keyfile.  Must have SampleName column." }
        check(readMappingColIndex != -1) { "Error processing keyfile.  Must have ReadMappingIds column." }
        check(likelyParentColIndex != -1) { "Error processing keyfile.  Must have LikelyParents column." }

        val useLikelyParents = !classicAlgorithm() && (maxParents() < myGraph.totalNumberTaxa() || minCoverage() < 1.0)
        val likelyParentsFilename = likelyParentFile()
        val myWriter = if (!classicAlgorithm() && useLikelyParents && likelyParentsFilename != null) {
            val out = PrintWriter(likelyParentsFilename)
            out.println("sample_name\ttotal_reads\tparent\torder\tnumber_of_reads\tcumulative_reads")
            out
        } else null

        entries.forEach { keyFileEntry ->
            val aggregatedReadMappingCounts = HashMultiset.create<List<Int>>()
            val refRangeToHapIdSetCounts = HashMultimap.create<ReferenceRange, HapIdSetCount>()
            val taxonName = keyFileEntry[sampleNameColIndex]
            //Loop through each readMappingID
            val readMappingIds = keyFileEntry[readMappingColIndex].split(",").map { mappingId -> mappingId.toInt() }
            //retrieve the readMapping byte[] from the db
            for (mappingId in readMappingIds) {
                val readMappings = readMappingDecoder.getDecodedReadMappingForMappingId(mappingId)
                readMappings.forEach {
                    //Merge all ReadMapping counts into a single readMapping
                    aggregatedReadMappingCounts.add(it.key, it.value)
                }
            }

            //Now that we have the multiset, we need to associate the mappings with the reference range it comes from
            aggregatedReadMappingCounts.elementSet()
                    .filter { it.isNotEmpty() }
                    .forEach {
                        val referenceRange = hapIdToRefRangeMapping[it[0]]
                        refRangeToHapIdSetCounts.put(referenceRange, HapIdSetCount(it.toSet(), aggregatedReadMappingCounts.count(it)))
                    }

            if (classicAlgorithm.value()) {
                val pathFinder = DiploidCountsToPath(pathFactory.myGraph, refRangeToHapIdSetCounts,
                    maxNodesPerRange = maxNodesPerRange(),
                    maxReadsPerKB = maxReadsPerKB(),
                    minReadsPerRange = minReadsPerRange(),
                    minTransitionProbability = minTransitionProbability(),
                    probabilityCorrect = probabilityCorrect(),
                    removeRangesWithEqualCounts = removeEqualCounts(),
                    splitNodes = splitNodes(),
                    splitTransitionProb = splitTransitionProb())

                //encode the paths as a byte[] and write to the DB along with the method name and details
                val myPath = pathFinder.getDiploidPath()
                val myEncodedPath = DBLoadingUtils.encodePathArrayForMultipleLists(myPath)
                phg.putPathsData(pathMethodName(), pluginParameters(), taxonName, readMappingIds, myEncodedPath, isTestMethod())
            } else if (useLikelyParents) {
                val pathFinder = pathFactory.build(refRangeToHapIdSetCounts)
                val likelyParentList = pathFinder.likelyParents
                if (myWriter != null) {
                    //myWriter.println("$sampleName\t$totalReads\t$parent\t$order\t$numberOfReads\t$cumulativeReads")
                    val totalReads = refRangeToHapIdSetCounts.values().sumBy { it.count }
                    var cumulativeReads = 0
                    likelyParentList.forEachIndexed { index, pair ->
                        val parent = pair.first
                        val numberOfReads = pair.second
                        cumulativeReads += numberOfReads
                        val cumulativeProportion = cumulativeReads / totalReads.toDouble()
                        myWriter.println("$taxonName\t$totalReads\t$parent\t${index + 1}\t$numberOfReads\t$cumulativeProportion")
                    }
                }
                val myPath = pathFinder.findBestDiploidPath()
                val myEncodedPath = DBLoadingUtils.encodePathArrayForMultipleLists(myPath)
                phg.putPathsData(pathMethodName(), pluginParameters(), taxonName, readMappingIds, myEncodedPath, isTestMethod())
            } else {
                val start = System.nanoTime()
                val parentString = keyFileEntry[likelyParentColIndex].trim()
                val myPath = pathFactory.build(refRangeToHapIdSetCounts, parentString).findBestDiploidPath()
                myLogger.info("Found path for sample in ${(System.nanoTime() - start)/1e9} sec.")

                val myEncodedPath = DBLoadingUtils.encodePathArrayForMultipleLists(myPath)
                phg.putPathsData(pathMethodName(), pluginParameters(), taxonName, readMappingIds, myEncodedPath, isTestMethod())
            }

        }
        phg.close()
        if (myWriter != null) myWriter.close()

    }

    data class PathFindingResult(val keyFileEntry: List<String>, val path: List<List<HaplotypeNode>>, val likelyParents: List<Pair<String,Int>>, val totalReads: Int = -1)

    private fun processKeyfileMultithreaded(myGraph: HaplotypeGraph) {
        val hapIdToRefRangeMapping = myGraph.referenceRanges().flatMap { myGraph.nodes(it) }
                .associate { Pair(it.id(), it.referenceRange()) }

        //create a PHGdbAccess instance for loading paths
        val dbConnect = DBLoadingUtils.connection(false)
        val phg = PHGdbAccess(dbConnect)
        val readMappingDecoder = ReadMappingDecoder(phg)

        //Parse the keyfile
        val keyFileParsed = readInKeyFile(keyFile.value())
        val headers = keyFileParsed.first
        val entries = keyFileParsed.second

        val sampleNameColIndex = headers["SampleName"] ?: -1
        val readMappingColIndex = headers["ReadMappingIds"] ?: -1
        val likelyParentColIndex = headers["LikelyParents"] ?: -1

        check(sampleNameColIndex != -1) { "Error processing keyfile.  Must have SampleName column." }
        check(readMappingColIndex != -1) { "Error processing keyfile.  Must have ReadMappingIds column." }
        check(likelyParentColIndex != -1) { "Error processing keyfile.  Must have LikelyParents column." }

        //create a PathFactory for the efficient algorithm
        //add missing sequence nodes to graph
        val pathFactory = PathFinderForSingleTaxonNodesFactory(
            CreateGraphUtils.addMissingSequenceNodes(myGraph),
            minTaxaPerRange = minTaxaPerRange(),
            minReadsPerRange = minReadsPerRange(),
            removeEqual = removeEqualCounts(),
            maxReadsPerKB = maxReadsPerKB(),
            sameGameteProbability = splitTransitionProb(),
            probCorrect = probabilityCorrect(),
            inbreedCoef = inbreedingCoef(),
            maxParents = maxParents(),
            minCoverage = minCoverage()
        )

        runBlocking {
            val inputChannel = Channel<List<String>>()
            val resultChannel = Channel<PathFindingResult>()
            val numThreads = max(numThreads() - 2, 1)

            launch {
                myLogger.info("Adding entries to the inputChannel:")
                entries.forEach { keyFileEntry ->
                    myLogger.info("Adding: ${keyFileEntry[0]}")
                    inputChannel.send(keyFileEntry)
                }
                myLogger.info("Done Adding KeyFile entries to the inputChannel:")
                inputChannel.close() //Need to close this here to show the workers that it is done adding more data
            }

            val workerThreads = (1..numThreads).map { threadNum ->
                launch { processKeyFileEntry(pathFactory, inputChannel, resultChannel, readMappingDecoder, hapIdToRefRangeMapping, sampleNameColIndex, readMappingColIndex) }
            }

            launch {
                processDBUploading(resultChannel, phg, sampleNameColIndex, readMappingColIndex)
                myLogger.info("Finished writing to the DB.")
            }


            //Create a coroutine to make sure all the async coroutines are done processing, then close the result channel.
            //If this is not here, this will run forever.
            launch {
                workerThreads.forEach { it.join() }
                resultChannel.close()
            }
        }

        phg.close()

    }

    /**
     * Method to process a keyFile entry and find a path.
     *
     * Might need to pull out the DB reading portion.  The multithreading will just be finding the path in this case.
     */
    private suspend fun processKeyFileEntry(pathFactory: PathFinderForSingleTaxonNodesFactory, inputChannel: Channel<List<String>>, resultChannel: Channel<PathFindingResult>, mappingDecoder: ReadMappingDecoder, hapIdToRefRangeMapping: Map<Int, ReferenceRange>, sampleNameColIndex: Int, readMappingColIndex: Int) = withContext(Dispatchers.Default) {
        for (keyFileEntry in inputChannel) {
            val aggregatedReadMappingCounts = HashMultiset.create<List<Int>>()
            val refRangeToHapIdSetCounts = HashMultimap.create<ReferenceRange, HapIdSetCount>()

            //Loop through each readMappingID
            val readMappingIds = keyFileEntry[readMappingColIndex].split(",").map { mappingId -> mappingId.toInt() }
            //retrieve the readMapping byte[] from the db
            for (mappingId in readMappingIds) {
                val readMappings = mappingDecoder.getDecodedReadMappingForMappingId(mappingId)
                readMappings.forEach {
                    //Merge all ReadMapping counts into a single readMapping
                    aggregatedReadMappingCounts.add(it.key, it.value)
                }
            }

            //Now that we have the multiset, we need to associate the mappings with the reference range it comes from
            aggregatedReadMappingCounts.elementSet()
                    .filter { it.isNotEmpty() }
                    .forEach {
                        val referenceRange = hapIdToRefRangeMapping[it[0]]
                        refRangeToHapIdSetCounts.put(referenceRange, HapIdSetCount(it.toSet(), aggregatedReadMappingCounts.count(it)))
                    }
            //Run find path using this read mapping
            //Write the read mapping to the DB
            //If Export file is specified, write out the readmapping

            var likelyParents = listOf<Pair<String,Int>>()
            val diploidPath = if (classicAlgorithm.value()) {
                val pathFinder = DiploidCountsToPath(pathFactory.myGraph, refRangeToHapIdSetCounts,
                    maxNodesPerRange = maxNodesPerRange(),
                    maxReadsPerKB = maxReadsPerKB(),
                    minReadsPerRange = minReadsPerRange(),
                    minTransitionProbability = minTransitionProbability(),
                    probabilityCorrect = probabilityCorrect(),
                    removeRangesWithEqualCounts = removeEqualCounts(),
                    splitNodes = splitNodes(),
                    splitTransitionProb = splitTransitionProb())

                pathFinder.getDiploidPath()
            } else {
                val start = System.nanoTime()
                val pathFinder = pathFactory.build(refRangeToHapIdSetCounts)
                val path = pathFinder.findBestDiploidPath()
                likelyParents = pathFinder.likelyParents
                myLogger.info{"Found path for sample in ${(System.nanoTime() - start)/1e9} sec."}
                path
            }
            val totalReads = refRangeToHapIdSetCounts.values().sumOf { it.count }
            resultChannel.send(PathFindingResult(keyFileEntry, diploidPath, likelyParents, totalReads))
        }
    }

    /**
     * Single threaded function to upload the path results to the DB.  This will simply take the path and encode it into a byte[].
     *
     */
    private suspend fun processDBUploading(resultChannel: Channel<PathFindingResult>, phg: PHGdbAccess, sampleNameColIndex: Int, readMappingColIndex: Int) = withContext(Dispatchers.Default) {
        val parentFile = likelyParentFile()
        val reportWriter = if (parentFile != null) PrintWriter(parentFile) else null
        if (reportWriter != null) reportWriter.println("sample_name\ttotal_reads\tparent\torder\tnumber_of_reads\tcumulative_reads")
        for (result in resultChannel) {
            val keyFileEntry = result.keyFileEntry
            val path = result.path
            val encodedPath = DBLoadingUtils.encodePathArrayForMultipleLists(path)
            val taxonName = keyFileEntry[sampleNameColIndex]

            //Loop through each readMappingID
            val readMappingIds = keyFileEntry[readMappingColIndex].split(",").map { mappingId -> mappingId.toInt() }
            phg.putPathsData(pathMethodName(), pluginParameters(), taxonName, readMappingIds, encodedPath, isTestMethod())

            //write out likely parents
            var cumulativeReads = 0
            if (reportWriter != null) result.likelyParents.forEachIndexed { index, pair ->
                val totalReads = result.totalReads
                val parent = pair.first
                val order = index + 1
                val numberOfReads = pair.second
                cumulativeReads += numberOfReads
                reportWriter.println("$taxonName\t$totalReads\t$parent\t$order\t$numberOfReads\t${cumulativeReads.toDouble() / totalReads.toDouble()}")
            }

        }
        if (reportWriter != null) reportWriter.close()
    }

    override fun getToolTipText(): String {
        return "Create a diploid path from a fastq file"
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = DiploidPathPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "Diploid Path"
    }

    override fun pluginDescription(): String {
        return "DiploidPathPlugin finds the best path through all ordered pair of nodes in a HaplotypeGraph given a key file with sample names and read mapping ids."
    }

    // The following getters and setters were auto-generated.
    // Please use this method to re-generate.
    //
    // fun main(args: Array<String>) {
    //     GeneratePluginCode.generateKotlin(DiploidPathPlugin::class.java)
    // }

    /**
     * KeyFile file name.  Must be a tab separated file using
     * the following headers:
     * SampleName	ReadMappingIds	LikelyParents
     * ReadMappingIds and LikelyParents need to be comma separated
     * for multiple values
     *
     * @return KeyFile
     */
    fun keyFile(): String {
        return keyFile.value()
    }

    /**
     * Set KeyFile. KeyFile file name.  Must be a tab separated
     * file using the following headers:
     * SampleName	ReadMappingIds	LikelyParents
     * ReadMappingIds and LikelyParents need to be comma separated
     * for multiple values
     *
     * @param value KeyFile
     *
     * @return this plugin
     */
    fun keyFile(value: String): DiploidPathPlugin {
        keyFile = PluginParameter(keyFile, value)
        return this
    }

    /**
     * The name of the read mapping method in the PHG DB
     *
     * @return Read Mapping Method
     */
    fun readMethodName(): String {
        return readMethodName.value()
    }

    /**
     * Set Read Mapping Method. The name of the read mapping
     * method in the PHG DB
     *
     * @param value Read Mapping Method
     *
     * @return this plugin
     */
    fun readMethodName(value: String): DiploidPathPlugin {
        readMethodName = PluginParameter(readMethodName, value)
        return this
    }

    /**
     * The name of the path method used to write the results
     * to the PHG DB
     *
     * @return Path Method
     */
    fun pathMethodName(): String {
        return pathMethodName.value()
    }

    /**
     * Set Path Method. The name of the path method used to
     * write the results to the PHG DB
     *
     * @param value Path Method
     *
     * @return this plugin
     */
    fun pathMethodName(value: String): DiploidPathPlugin {
        pathMethodName = PluginParameter(pathMethodName, value)
        return this
    }

    /**
     * An additional description that will be stored with
     * the path method name, if desired.
     *
     * @return Path Method Description
     */
    fun pathMethodDescription(): String? {
        return pathMethodDescription.value()
    }

    /**
     * Set Path Method Description. An additional description
     * that will be stored with the path method name, if desired.
     *
     * @param value Path Method Description
     *
     * @return this plugin
     */
    fun pathMethodDescription(value: String): DiploidPathPlugin {
        pathMethodDescription = PluginParameter(pathMethodDescription, value)
        return this
    }

    /**
     * minimum number of taxa per anchor reference range.
     * Ranges with fewer taxa will not be included in the
     * output node list.
     *
     * @return Min Taxa
     */
    fun minTaxaPerRange(): Int {
        return minTaxaPerRange.value()
    }

    /**
     * Set Min Taxa. minimum number of taxa per anchor reference
     * range. Ranges with fewer taxa will not be included
     * in the output node list.
     *
     * @param value Min Taxa
     *
     * @return this plugin
     */
    fun minTaxaPerRange(value: Int): DiploidPathPlugin {
        minTaxaPerRange = PluginParameter(minTaxaPerRange, value)
        return this
    }

    /**
     * The probability that a read mapped to the correct haplotypes
     *
     * @return Probability Correct
     */
    fun probabilityCorrect(): Double {
        return probabilityCorrect.value()
    }

    /**
     * Set Probability Correct. The probability that a read
     * mapped to the correct haplotypes
     *
     * @param value Probability Correct
     *
     * @return this plugin
     */
    fun probabilityCorrect(value: Double): DiploidPathPlugin {
        probabilityCorrect = PluginParameter(probabilityCorrect, value)
        return this
    }

    /**
     * The minimum transition probability between a pair of
     * nodes in adjacent reference ranges.
     *
     * @return Min Transition Probability
     */
    fun minTransitionProbability(): Double {
        return minTransitionProbability.value()
    }

    /**
     * Set Min Transition Probability. The minimum transition
     * probability between a pair of nodes in adjacent reference
     * ranges.
     *
     * @param value Min Transition Probability
     *
     * @return this plugin
     */
    fun minTransitionProbability(value: Double): DiploidPathPlugin {
        minTransitionProbability = PluginParameter(minTransitionProbability, value)
        return this
    }

    /**
     * Any range with more than maxHap haplotypes will not
     * be included in the path.
     *
     * @return Maximum Number of Haplotypes
     */
    fun maxNodesPerRange(): Int {
        return maxNodesPerRange.value()
    }

    /**
     * Set Maximum Number of Haplotypes. Any range with more
     * than maxHap haplotypes will not be included in the
     * path.
     *
     * @param value Maximum Number of Haplotypes
     *
     * @return this plugin
     */
    fun maxNodesPerRange(value: Int): DiploidPathPlugin {
        maxNodesPerRange = PluginParameter(maxNodesPerRange, value)
        return this
    }

    /**
     * Any range with fewer than minReads will not be included
     * in the path.
     *
     * @return Minimum Read Number
     */
    fun minReadsPerRange(): Int {
        return minReadsPerRange.value()
    }

    /**
     * Set Minimum Read Number. Any range with fewer than
     * minReads will not be included in the path.
     *
     * @param value Minimum Read Number
     *
     * @return this plugin
     */
    fun minReadsPerRange(value: Int): DiploidPathPlugin {
        minReadsPerRange = PluginParameter(minReadsPerRange, value)
        return this
    }

    /**
     * Any range for which all haplotypes have the same number
     * of read counts will not be included in the path.
     *
     * @return Remove Equal
     */
    fun removeEqualCounts(): Boolean {
        return removeEqualCounts.value()
    }

    /**
     * Set Remove Equal. Any range for which all haplotypes
     * have the same number of read counts will not be included
     * in the path.
     *
     * @param value Remove Equal
     *
     * @return this plugin
     */
    fun removeEqualCounts(value: Boolean): DiploidPathPlugin {
        removeEqualCounts = PluginParameter(removeEqualCounts, value)
        return this
    }

    /**
     * Any range with more than maxReadsKB reads per kilobase
     * of sequence will not be included in the path.
     *
     * @return Maximum Reads per KB
     */
    fun maxReadsPerKB(): Int {
        return maxReadsPerKB.value()
    }

    /**
     * Set Maximum Reads per KB. Any range with more than
     * maxReadsKB reads per kilobase of sequence will not
     * be included in the path.
     *
     * @param value Maximum Reads per KB
     *
     * @return this plugin
     */
    fun maxReadsPerKB(value: Int): DiploidPathPlugin {
        maxReadsPerKB = PluginParameter(maxReadsPerKB, value)
        return this
    }

    /**
     * If splitTaxa is true, then each taxon will be assigned
     * its own node in the graph prior to path finding.
     *
     * @return Split Taxa
     */
    fun splitNodes(): Boolean {
        return splitNodes.value()
    }

    /**
     * Set Split Taxa. If splitTaxa is true, then each taxon
     * will be assigned its own node in the graph prior to
     * path finding.
     *
     * @param value Split Taxa
     *
     * @return this plugin
     */
    fun splitNodes(value: Boolean): DiploidPathPlugin {
        splitNodes = PluginParameter(splitNodes, value)
        return this
    }

    /**
     * When splitTaxa is true, the transition probability
     * for moving between nodes of the same taxon will be
     * set to this number.
     *
     * @return Split Probability
     */
    fun splitTransitionProb(): Double {
        return splitTransitionProb.value()
    }

    /**
     * Set Split Probability. When splitTaxa is true, the
     * transition probability for moving between nodes of
     * the same taxon will be set to this number.
     *
     * @param value Split Probability
     *
     * @return this plugin
     */
    fun splitTransitionProb(value: Double): DiploidPathPlugin {
        splitTransitionProb = PluginParameter(splitTransitionProb, value)
        return this
    }

    /**
     * Number of threads used to upload
     *
     * @return Num Threads
     */
    fun numThreads(): Int {
        return myNumThreads.value()
    }

    /**
     * Set Num Threads. Number of threads used to upload
     *
     * @param value Num Threads
     *
     * @return this plugin
     */
    fun numThreads(value: Int): DiploidPathPlugin {
        myNumThreads = PluginParameter(myNumThreads, value)
        return this
    }

    /**
     * Use the classic algorithm to compute the Viterbi algorithm
     * as described in Rabiner 1989. Significantly slower
     * than the default algorithm.
     *
     * @return Classic Algorithm
     */
    fun classicAlgorithm(): Boolean {
        return classicAlgorithm.value()
    }

    /**
     * Set Classic Algorithm. Use the classic algorithm to
     * compute the Viterbi algorithm as described in Rabiner
     * 1989. Significantly slower than the default algorithm.
     *
     * @param value Classic Algorithm
     *
     * @return this plugin
     */
    fun classicAlgorithm(value: Boolean): DiploidPathPlugin {
        classicAlgorithm = PluginParameter(classicAlgorithm, value)
        return this
    }

    /**
     * The coefficient of inbreeding, f, for the taxa being
     * imputed.
     *
     * @return Inbreed Coef
     */
    fun inbreedingCoef(): Double {
        return inbreedingCoef.value()
    }

    /**
     * Set Inbreed Coef. The coefficient of inbreeding, f,
     * for the taxa being imputed.
     *
     * @param value Inbreed Coef
     *
     * @return this plugin
     */
    fun inbreedingCoef(value: Double): DiploidPathPlugin {
        inbreedingCoef = PluginParameter(inbreedingCoef, value)
        return this
    }
    /**
     * To restrict path finding to the most likely parents,
     * the number of parents used will not be greater than
     * maxParents. The number of parents used will be the
     * minimum of maxParents and the number of parents needed
     * to reach minCoverage. If both maxParents and minCoverage
     * are left at the default, all parents in the input HaplotypeGraph
     * will be used.
     *
     * @return Max Parents
     */
    fun maxParents(): Int {
        return maxParents.value()
    }

    /**
     * Set Max Parents. To restrict path finding to the most
     * likely parents, the number of parents used will not
     * be greater than maxParents. The number of parents used
     * will be the minimum of maxParents and the number of
     * parents needed to reach minCoverage. If both maxParents
     * and minCoverage are left at the default, all parents
     * in the input HaplotypeGraph will be used.
     *
     * @param value Max Parents
     *
     * @return this plugin
     */
    fun maxParents(value: Int): DiploidPathPlugin {
        maxParents = PluginParameter(maxParents, value)
        return this
    }

    /**
     * To restrict path finding to the most likely parents,
     * the smallest number of parents needed to provide read
     * coverage greater than or equal to minCoverage will
     * be used to find paths. If maxParents is smaller, that
     * number of parents will be used.
     *
     * @return Min Coverage
     */
    fun minCoverage(): Double {
        return minCoverage.value()
    }

    /**
     * Set Min Coverage. To restrict path finding to the most
     * likely parents, the smallest number of parents needed
     * to provide read coverage greater than or equal to minCoverage
     * will be used to find paths. If maxParents is smaller,
     * that number of parents will be used.
     *
     * @param value Min Coverage
     *
     * @return this plugin
     */
    fun minCoverage(value: Double): DiploidPathPlugin {
        minCoverage = PluginParameter(minCoverage, value)
        return this
    }

    /**
     * The name and path of the file of likely parents and
     * their read counts. If no file name is provided the
     * likely parents will not be written to a file.
     *
     * @return Parent Output File
     */
    fun likelyParentFile(): String? {
        return likelyParentFile.value()
    }

    /**
     * Set Parent Output File. The name and path of the file
     * of likely parents and their read counts. If no file
     * name is provided the likely parents will not be written
     * to a file.
     *
     * @param value Parent Output File
     *
     * @return this plugin
     */
    fun likelyParentFile(value: String): DiploidPathPlugin {
        likelyParentFile = PluginParameter(likelyParentFile, value)
        return this
    }
    /**
     * Indication if the data is to be loaded against a test
     * method. Data loaded with test methods are not cached
     * with the PHG ktor server
     *
     * @return Is Test Method
     */
    fun isTestMethod(): Boolean {
        return isTestMethod.value()
    }

    /**
     * Set Is Test Method. Indication if the data is to be
     * loaded against a test method. Data loaded with test
     * methods are not cached with the PHG ktor server
     *
     * @param value Is Test Method
     *
     * @return this plugin
     */
    fun isTestMethod(value: Boolean): DiploidPathPlugin {
        isTestMethod = PluginParameter<Boolean>(isTestMethod, value)
        return this
    }


}

//fun main(args: Array<String>) {
//    GeneratePluginCode.generateKotlin(DiploidPathPlugin::class.java)
//}
