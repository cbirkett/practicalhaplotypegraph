package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pangenome.api.HaplotypeGraph
import org.apache.log4j.Logger
import java.awt.Frame
import javax.swing.ImageIcon
import net.maizegenetics.plugindef.*


/**
 * Plugin to map reads to the pangenome using Minimap2
 *
 * If running in paired mode, it will align the pairs using minimap and process the resulting SAM records.
 *
 * Only equally optimal mappings(By Edit Distance(NM) are kept)
 *
 * Additional filtering is done to remove reads which are unmapped or clipped
 *
 * If running in paired mode, the reads must be on opposite strands and both must currently hit the same haplotype in a single reference range.
 *
 * Optimal mappings across reference ranges are processed, but reads are only assigned to haplotypes in the reference
 *  range with the most hits(providing that at least 1- maxRefRangeError percentage hit that refRange)
 *  Based on testing, this seems to be working the best to balance #of reads used with accuracy.
 */
class FastqToMappingPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(FastqToMappingPlugin::class.java)

    private var indexFile = PluginParameter.Builder("minimap2IndexFile", null, String::class.java)
            .guiName("Minimap2 index file for pangenome")
            .inFile()
            .required(true)
            .description("Name of the indexFile file to process")
            .build()

    private var keyFile = PluginParameter.Builder("keyFile", null, String::class.java)
            .guiName("keyFile")
            .inFile()
            .required(true)
            .description("Name of the Keyfile to process.  Must have columns cultivar, flowcell_lane, filename, and PlateID.  Optionally for paired end reads, filename2 is needed.  " +
                    "If filename2 is not supplied, Minimap2 will run in single end mode.  Otherwise will be paired.")
            .build()

    private var fastqDir = PluginParameter.Builder("fastqDir", null, String::class.java)
            .guiName("Fastq dir to process")
            .inDir()
            .required(true)
            .description("Name of the Fastq dir to process.")
            .build()


    private var maxRefRangeError = PluginParameter.Builder("maxRefRangeErr", .25, Double::class.javaObjectType)
            .required(false)
            .description("Maximum allowed error when choosing best reference range to count.  Error is computed 1 - (mostHitRefCount/totalHits)")
            .build()

    private var lowMemMode = PluginParameter.Builder("lowMemMode", true, Boolean::class.javaObjectType)
            .required(false)
            .description("Run in low memory mode.")
            .build()

    private var maxSecondaryAlignments = PluginParameter.Builder("maxSecondary", 20, Int::class.javaObjectType)
            .required(false)
            .description("Maximum number of secondary alignments to be returned by minimap2. This will be the value of the -N parameter in the minimap2 command line. If the value is too low, some valid read mappings will not be reported.")
            .build()

    private var fParameter = PluginParameter.Builder("fParameter", "f1000,5000", String::class.java)
            .required(false)
            .description("The f parameter used by minimap2. If the sr preset (-x sr) is used then this parameter takes the form f<int1,int2>. " +
                    "From the minimap2 man page: If integer, ignore minimizers occuring more than INT1 times. " +
                    "INT2 is only effective in the --sr or -xsr mode, which sets the threshold for a second round of seeding.")
            .build()

    private var minimapLocation = PluginParameter.Builder("minimapLocation", "minimap2", String::class.java)
            .guiName("Location of Minimap2 Executable")
            .required(false)
            .description("Location of Minimap2 on file system.  This defaults to use minimap2 if it is on the PATH environment variable.")
            .build()

    private var methodName = PluginParameter.Builder("methodName", null, String::class.java)
            .guiName("Method Name")
            .required(true)
            .description("Method name to be stored in the DB.")
            .build()

    private var methodDescription = PluginParameter.Builder("methodDescription", null, String::class.java)
            .guiName("Method Description")
            .required(false)
            .description("Method description to be stored in the DB.")
            .build()

    private var outputDebugDir = PluginParameter.Builder("debugDir", "", String::class.java)
            .guiName("Debug Directory to write out read Mapping files.")
            .outDir()
            .required(false)
            .description("Directory to write out the read mapping files.  This is optional for debug purposes.")
            .build()

    private var outputSecondaryMappingStats = PluginParameter.Builder("outputSecondaryStats",false,Boolean::class.javaObjectType)
            .guiName("Output secondary mapping statistics.")
            .required(false)
            .description("Ouptput Secondary Mapping Statistics such as total AS for each haplotype ID")
            .build()

    private var isTestMethod = PluginParameter.Builder("isTestMethod", false, Boolean::class.javaObjectType)
        .description("Indication if the data is to be loaded against a test method. Data loaded with test methods are not cached with the PHG ktor server")
        .required(false).build()

    override fun processData(input: DataSet?): DataSet? {
        //Load in the graph to check reference Ranges

        val temp = input?.getDataOfType(HaplotypeGraph::class.java)
                ?: throw IllegalArgumentException("FastqToMappingPlugin: processData: must input one HaplotypeGraph type not null: ")
        if (temp.size != 1) {
            throw IllegalArgumentException("FastqToMappingPlugin: processData: must input one HaplotypeGraph: " + temp.size)
        }
        val graph: HaplotypeGraph = temp[0].data as HaplotypeGraph

        runMinimapFromKeyFile(minimapLocation = minimapLocation(),
            keyFileName = keyFile(),
            inputFileDir = fastqDir(),
            referenceFile = indexFile(),
            graph =  graph,
            maxRefRangeError = maxRefRangeError(),
            methodName = methodName(),
            methodDescription = methodDescription(),
            pluginParams = pluginParameters(),
            outputDebugReadMappingDir = outputDebugDir(),
            outputSecondaryMappingStats = outputSecondaryMappingStats(),
            maxSecondary = maxSecondaryAlignments(),
            inputFileFormat = ReadMappingInputFileFormat.FASTQ,
            fParameter = fParameter(),
            isTestMethod =isTestMethod()
        )
        return null

    }

    override fun getIcon(): ImageIcon? {
        val imageURL = FastqToMappingPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "FastqToHapCountMinimapPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to Align a Fastq file and export a hapCount File "
    }

    /**
     * Name of the indexFile file to process
     *
     * @return Minimap2 index file for pangenome
     */
    fun indexFile() : String {
        return indexFile.value()
    }

    /**
     * Set Minimap2 index file for pangenome. Name of the
     * indexFile file to process
     *
     * @param value Minimap2 index file for pangenome
     *
     * @return this plugin
     */
    fun indexFile(value : String) : FastqToMappingPlugin {
        indexFile = PluginParameter(indexFile, value)
        return this
    }

    /**
     * Name of the Keyfile to process.  Must have columns
     * cultivar, flowcell_lane, filename, and PlateID.  Optionally
     * for paired end reads, filename2 is needed.  If filename2
     * is not supplied, Minimap2 will run in single end mode.
     *  Otherwise will be paired.
     *
     * @return keyFile
     */
    fun keyFile(): String {
        return keyFile.value()
    }

    /**
     * Set keyFile. Name of the Keyfile to process.  Must
     * have columns cultivar, flowcell_lane, filename, and
     * PlateID.  Optionally for paired end reads, filename2
     * is needed.  If filename2 is not supplied, Minimap2
     * will run in single end mode.  Otherwise will be paired.
     *
     * @param value keyFile
     *
     * @return this plugin
     */
    fun keyFile(value : String) : FastqToMappingPlugin {
        keyFile = PluginParameter(keyFile, value)
        return this
    }

    /**
     * Name of the Fastq dir to process.
     *
     * @return Fastq dir to process
     */
    fun fastqDir(): String {
        return fastqDir.value()
    }

    /**
     * Set Fastq dir to process. Name of the Fastq dir to
     * process.
     *
     * @param value Fastq dir to process
     *
     * @return this plugin
     */
    fun fastqDir(value: String): FastqToMappingPlugin {
        fastqDir = PluginParameter(fastqDir, value)
        return this
    }

    /**
     * Maximum allowed error when choosing best reference
     * range to count.  Error is computed 1 - (mostHitRefCount/totalHits)
     *
     * @return Max Ref Range Err
     */
    fun maxRefRangeError() : Double {
        return maxRefRangeError.value()
    }

    /**
     * Set Max Ref Range Err. Maximum allowed error when choosing
     * best reference range to count.  Error is computed 1
     * - (mostHitRefCount/totalHits)
     *
     * @param value Max Ref Range Err
     *
     * @return this plugin
     */
    fun maxRefRangeError(value : Double) : FastqToMappingPlugin {
        maxRefRangeError = PluginParameter(maxRefRangeError, value)
        return this
    }

    /**
     * Run in low memory mode.
     *
     * @return Low Mem Mode
     */
    fun lowMemMode() : Boolean {
        return lowMemMode.value()
    }

    /**
     * Set Low Mem Mode. Run in low memory mode.
     *
     * @param value Low Mem Mode
     *
     * @return this plugin
     */
    fun lowMemMode(value : Boolean) : FastqToMappingPlugin {
        lowMemMode = PluginParameter(lowMemMode, value)
        return this
    }

    /**
     * Maximum number of secondary alignments to be returned
     * by minimap2. This will be the value of the -N parameter
     * in the minimap2 command line. If the value is too low,
     * some valid read mappings will not be reported.
     *
     * @return Max Secondary
     */
    fun maxSecondaryAlignments(): Int {
        return maxSecondaryAlignments.value()
    }

    /**
     * Set Max Secondary. Maximum number of secondary alignments
     * to be returned by minimap2. This will be the value
     * of the -N parameter in the minimap2 command line. If
     * the value is too low, some valid read mappings will
     * not be reported.
     *
     * @param value Max Secondary
     *
     * @return this plugin
     */
    fun maxSecondaryAlignments(value: Int): FastqToMappingPlugin {
        maxSecondaryAlignments = PluginParameter<Int>(maxSecondaryAlignments, value)
        return this
    }

    /**
     * The f parameter used by minimap2. If the sr preset
     * (-x sr) is used then this parameter takes the form
     * f<int1,int2>. From the minimap2 man page: If integer,
     * ignore minimizers occuring more than INT1 times. INT2
     * is only effective in the --sr or -xsr mode, which sets
     * the threshold for a second round of seeding.
     *
     * @return F Parameter
     */
    fun fParameter(): String {
        return fParameter.value()
    }

    /**
     * Set F Parameter. The f parameter used by minimap2.
     * If the sr preset (-x sr) is used then this parameter
     * takes the form f<int1,int2>. From the minimap2 man
     * page: If integer, ignore minimizers occuring more than
     * INT1 times. INT2 is only effective in the --sr or -xsr
     * mode, which sets the threshold for a second round of
     * seeding.
     *
     * @param value F Parameter
     *
     * @return this plugin
     */
    fun fParameter(value: String): FastqToMappingPlugin {
        fParameter = PluginParameter<String>(fParameter, value)
        return this
    }

    /**
     * Location of Minimap2 on file system.  This defaults
     * to use minimap2 if it is on the PATH environment variable.
     *
     * @return Location of Minimap2 Executable
     */
    fun minimapLocation() : String {
        return minimapLocation.value()
    }

    /**
     * Set Location of Minimap2 Executable. Location of Minimap2
     * on file system.  This defaults to use minimap2 if it
     * is on the PATH environment variable.
     *
     * @param value Location of Minimap2 Executable
     *
     * @return this plugin
     */
    fun minimapLocation(value : String) : FastqToMappingPlugin {
        minimapLocation = PluginParameter(minimapLocation, value)
        return this
    }

        /**
     * Method name to be stored in the DB.
     *
     * @return Method Name
     */
    fun methodName() :String {
        return methodName.value()
    }

    /**
     * Set Method Name. Method name to be stored in the DB.
     *
     * @param value Method Name
     *
     * @return this plugin
     */
    fun methodName(value : String) : FastqToMappingPlugin {
        methodName = PluginParameter(methodName, value)
        return this
    }

    /**
     * Method description to be stored in the DB.
     *
     * @return Method Description
     */
    fun methodDescription() : String? {
        return methodDescription.value()
    }

    /**
     * Set Method Description. Method description to be stored
     * in the DB.
     *
     * @param value Method Description
     *
     * @return this plugin
     */
    fun methodDescription(value : String) : FastqToMappingPlugin {
        methodDescription = PluginParameter(methodDescription, value)
        return this
    }

    /**
     * Directory to write out the read mapping files.  This
     * is optional for debug purposes.
     *
     * @return Debug Directory to write out read Mapping files.
     */
    fun outputDebugDir() : String {
        return outputDebugDir.value()
    }

    /**
     * Set Debug Directory to write out read Mapping files..
     * Directory to write out the read mapping files.  This
     * is optional for debug purposes.
     *
     * @param value Debug Directory to write out read Mapping files.
     *
     * @return this plugin
     */
    fun outputDebugDir(value : String) : FastqToMappingPlugin {
        outputDebugDir = PluginParameter(outputDebugDir, value)
        return this
    }

    /**
     * Ouptput Secondary Mapping Statistics such as total
     * AS for each haplotype ID
     *
     * @return Output secondary mapping statistics.
     */
    fun outputSecondaryMappingStats(): Boolean {
        return outputSecondaryMappingStats.value()
    }

    /**
     * Set Output secondary mapping statistics.. Ouptput Secondary
     * Mapping Statistics such as total AS for each haplotype
     * ID
     *
     * @param value Output secondary mapping statistics.
     *
     * @return this plugin
     */
    fun outputSecondaryMappingStats(value: Boolean): FastqToMappingPlugin {
        outputSecondaryMappingStats = PluginParameter<Boolean>(outputSecondaryMappingStats, value)
        return this
    }

    /**
     * Indication if the data is to be loaded against a test
     * method. Data loaded with test methods are not cached
     * with the PHG ktor server
     *
     * @return Is Test Method
     */
    fun isTestMethod(): Boolean {
        return isTestMethod.value()
    }

    /**
     * Set Is Test Method. Indication if the data is to be
     * loaded against a test method. Data loaded with test
     * methods are not cached with the PHG ktor server
     *
     * @param value Is Test Method
     *
     * @return this plugin
     */
    fun isTestMethod(value: Boolean): FastqToMappingPlugin {
        isTestMethod = PluginParameter<Boolean>(isTestMethod, value)
        return this
    }


}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(FastqToMappingPlugin::class.java)
}