package net.maizegenetics.pangenome.hapCalling

import java.util.*

/**
 * Guava like Multiset which will store a sorted set of hapids as the key and how many reads hit that exact subset of ids.
 */
class HapIdMultiset {
    var multiset :MutableMap<SortedSet<Int>,Int> = mutableMapOf()

    /**
     * Method to increment the count for a given hapId set
     */
    fun add(hapIds : Set<Int>) {
        val sortedSet = hapIds.toSortedSet()
        if(multiset.containsKey(sortedSet)) {
            multiset[sortedSet] = (multiset?.get(sortedSet)?:0)+1
        }
        else {
            multiset[sortedSet] = 1
        }
    }

    /**
     * Method to return the count for a given hapId set.  We sort the hapId set again just to be safe.
     */
    fun count(hapIds: Set<Int>) : Int {
        return multiset[hapIds.toSortedSet()]?:0
    }

    /**
     * Method to clear out the multiset and create a new empty one.
     */
    fun clear() {
        multiset = mutableMapOf()
    }

    /**
     * Method to get out the backing Map.  This can be more easily worked with once everything is added to the Multiset.
     */
    fun getMap() : MutableMap<SortedSet<Int>,Int> {
        return multiset
    }
}