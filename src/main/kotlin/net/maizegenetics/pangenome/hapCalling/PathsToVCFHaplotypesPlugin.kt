package net.maizegenetics.pangenome.hapCalling

import htsjdk.variant.variantcontext.Allele
import htsjdk.variant.variantcontext.GenotypeBuilder
import htsjdk.variant.variantcontext.VariantContext
import htsjdk.variant.variantcontext.VariantContextBuilder
import htsjdk.variant.variantcontext.writer.Options
import htsjdk.variant.variantcontext.writer.VariantContextWriterBuilder
import htsjdk.variant.vcf.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import net.maizegenetics.dna.map.Chromosome
import net.maizegenetics.dna.map.GenomeSequence
import net.maizegenetics.dna.map.GenomeSequenceBuilder
import net.maizegenetics.dna.map.PositionList
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.File
import java.util.*
import javax.swing.ImageIcon
import kotlin.collections.HashMap

/**
 * This exports diploid or haploid paths to a VCF file with haplotype allele values (Not SNPs).
 *
 * main thread get Paths (Either 1) haploid or diploid paths from DB, 2) use taxa from graph (TreeMap<ReferenceRange, MutableMap<taxon: String, hapids: MutableList<Int>>>)
 * main thread get Taxa List (List<String>)
 * multi-threaded foreach (ReferenceRange) -> variantContextChannel (Channel<Deferred<List<VariantContext>>>)
 *     get list of positions for all SNPs
 *     create a map foreach position that has taxon to VariantInfos (SortedMap<position: Int, Map<taxon: String, List<VariantInfo?>>>)
 *     foreach (position)
 *         create VariantContext (using htsjdk)
 *             haploid → homozygous unphased
 *             diploid → phased
 * main thread write VCF file
 *     Write VCF Header
 *     foreach (ReferenceRange)
 *         Write List<VariantContext> from variantContextChannel (Channel<Deferred<List<VariantContext>>>)
 *
 *
 * @author Terry Casstevens Created March 8, 2021
 */
class PathsToVCFHaplotypesPlugin(parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(PathsToVCFHaplotypesPlugin::class.java)

    private var myOutputFile = PluginParameter.Builder("outputFile", null, String::class.java)
            .description("Output file name")
            .guiName("Output VCF File Name")
            .required(true)
            .outFile()
            .build()

    private var myRefRangeForSNPFile = PluginParameter.Builder("refRangeFileVCF", null, String::class.java)
            .description("Reference Range file used to subset the paths for only specified regions of the genome.")
            .guiName("Reference Range File")
            .required(false)
            .inFile()
            .build()

    private var myRefGenome = PluginParameter.Builder("referenceFasta", null, String::class.java)
            .description("Reference Genome.")
            .guiName("Reference Genome")
            .required(false)
            .inFile()
            .build()

    private var myMakeDiploid = PluginParameter.Builder("makeDiploid", true, Boolean::class.javaObjectType)
            .description("Whether to report haploid paths as homozygousdiploid")
            .build()

    private var myPositions = PluginParameter.Builder("positions", null, PositionList::class.java)
            .description("Positions to include in VCF. Can be specified by Genotype file (i.e. VCF, Hapmap, etc.), bed file, or json file containing the requested positions.")
            .guiName("Position List")
            .required(false)
            .build()

    private lateinit var myGraph: HaplotypeGraph

    // Number of ranges included in export used for progress reporting
    private var myNumRanges = 0

    // Channel used to pass lists of VariantContext from multi-threaded code
    // creating them and single threaded code writing to VCF.
    // These are queued as a Deferred Job instead of completed
    // list of VariantContext to maintain the order positions in
    // the resulting VCF file.
    private val variantContextChannel = Channel<Deferred<List<VariantContext>>>(7)

    private var referenceSequence: GenomeSequence? = null

    override fun preProcessParameters(input: DataSet) {
        val temp = input.getDataOfType(HaplotypeGraph::class.java)
        require(temp.size == 1) { "PathsToVCFHaplotypesPlugin: preProcessParameters: must input one HaplotypeGraph" }
        myGraph = temp[0].data as HaplotypeGraph
    }

    override fun postProcessParameters() {
        outputFile(Utils.addSuffixIfNeeded(outputFile(), ".vcf"))
    }

    override fun processData(input: DataSet): DataSet? {

        if (refGenome() != null) {
            referenceSequence = GenomeSequenceBuilder.instance(refGenome())
        }

        // Get optional list of Reference Ranges to export to the VCF file
        val refRangesToKeepSetOptional = parseRefRangeFile(refRangeForSNPFile())
        val refRangesToKeep = if (refRangesToKeepSetOptional.isPresent) refRangesToKeepSetOptional.get() else null

        // This will hold the path information in a different structure
        // for easier use later.
        // TreeMap<ReferenceRange, MutableMap<taxon: String, hapids: MutableList<Int>>>
        val rangeToTaxonToHapids = TreeMap<ReferenceRange, MutableMap<String, MutableList<Int>>>()

        // Create empty maps for each range
        myGraph.referenceRangeList()
                .filter { refRangesToKeep == null || refRangesToKeep.contains(it.id()) }
                .forEach { range ->
                    rangeToTaxonToHapids[range] = HashMap()
                }

        myNumRanges = rangeToTaxonToHapids.size

        myLogger.info("PathsToVCFHaplotypesPlugin: processData: number of ranges: $myNumRanges")

        // This converts the path information into the map structure declared above.
        var temp = input.getDataOfType(Map::class.java)
        val taxa = when (temp.size) {
            0 -> { //get the haplotypes from the phg graph

                rangeToTaxonToHapids.keys
                        .map { myGraph.nodes(it) }
                        .flatten()
                        .forEach { node ->
                            node.taxaList().forEach { taxon ->
                                val taxonToHapids = rangeToTaxonToHapids[node.referenceRange()]!!
                                var hapids = taxonToHapids[taxon.name]
                                if (hapids == null) {
                                    hapids = mutableListOf()
                                    taxonToHapids[taxon.name] = hapids
                                }
                                hapids.add(node.id())
                            }
                        }

                myGraph.taxaInGraph().map { it.name }.toList()

            }
            1 -> { // Use paths provided in input

                val taxaToPaths = temp[0].data as Map<String, List<List<HaplotypeNode>>>

                taxaToPaths.forEach { (taxon, paths) ->
                    paths.forEach { nodes ->
                        nodes.forEach { node ->
                            if (refRangesToKeep == null || refRangesToKeep.contains(node.referenceRange().id())) {
                                val taxonToHapids = rangeToTaxonToHapids[node.referenceRange()]!!
                                var hapids = taxonToHapids[taxon]
                                if (hapids == null) {
                                    hapids = mutableListOf()
                                    taxonToHapids[taxon] = hapids!!
                                }
                                hapids!!.add(node.id())
                            }
                        }
                    }
                }

                taxaToPaths.keys.toList()

            }
            else -> {
                throw IllegalArgumentException("PathsToVCFHaplotypesPlugin: processData: At most one paths map can be supplied as input.")
            }

        }

        myLogger.info("PathsToVCFHaplotypesPlugin: processData: number of taxa: ${taxa.size}")

        // multi-threading creation of VariantContexts
        CoroutineScope(Dispatchers.IO).launch {
            hapidsByRange(rangeToTaxonToHapids)
        }

        // write VariantContexts to file and block main thread until done
        runBlocking { writeToVCF(taxa) }

        return null

    }

    /**
     * This starts the multi-threaded processing of paths to VariantContexts,
     * one thread per ReferenceRange.  It puts Deferred Jobs on
     * the variantContextChannel in order of ReferenceRanges.
     *
     * @param rangeToTaxonToHapids SortedMap<ReferenceRange, MutableMap<taxon: String, hapids: MutableList<Int>>>
     */
    private suspend fun hapidsByRange(rangeToTaxonToHapids: SortedMap<ReferenceRange, MutableMap<String, MutableList<Int>>>) = withContext(Dispatchers.IO) {

        rangeToTaxonToHapids.forEach { (range, taxonToHapids) ->
            variantContextChannel.send(async {
                listOf(variantContexts(range, taxonToHapids))
            })
        }

        variantContextChannel.close()

    }

    private fun createHeader(taxaNames: List<String>): VCFHeader {

        val headerLines: MutableSet<VCFHeaderLine> = HashSet()

        if (refGenome() != null) {
            headerLines.add(VCFHeaderLine("reference", File(refGenome()).name))
        }

        headerLines.add(VCFFormatHeaderLine("AD", 3, VCFHeaderLineType.Integer, "Allelic depths for the ref and alt alleles in the order listed"))
        headerLines.add(VCFFormatHeaderLine("DP", 1, VCFHeaderLineType.Integer, "Read Depth (only filtered reads used for calling)"))
        headerLines.add(VCFFormatHeaderLine("GQ", 1, VCFHeaderLineType.Integer, "Genotype Quality"))
        headerLines.add(VCFFormatHeaderLine("GT", 1, VCFHeaderLineType.String, "Genotype"))
        headerLines.add(VCFFormatHeaderLine("PL", VCFHeaderLineCount.G, VCFHeaderLineType.Integer, "Normalized, Phred-scaled likelihoods for genotypes as defined in the VCF specification"))
        headerLines.add(VCFInfoHeaderLine("DP", 1, VCFHeaderLineType.Integer, "Total Depth"))
        headerLines.add(VCFInfoHeaderLine("NS", 1, VCFHeaderLineType.Integer, "Number of Samples With Data"))
        headerLines.add(VCFInfoHeaderLine("AF", 3, VCFHeaderLineType.Integer, "Allele Frequency"))
        headerLines.add(VCFInfoHeaderLine("END", 1, VCFHeaderLineType.Integer, "Stop position of the interval"))

        var contigIndex = 0;
        myGraph.chromosomes()
                .forEach { chromosome ->
                    val attributes = mutableMapOf<String, String>()
                    attributes["ID"] = chromosome.name
                    headerLines.add(VCFContigHeaderLine(attributes, contigIndex++))
                }

        // ##ALT=<ID=124,Description=“B73:1:1232-10000”>
        myGraph.nodeStream()
                .forEach { node ->
                    headerLines.add(VCFAltHeaderLine("<ID=${nodeID(node)}, Description=\"${nodeDescription(node)}\">", VCFHeaderVersion.VCF4_2))
                }

        return VCFHeader(VCFHeaderVersion.VCF4_2, headerLines, taxaNames.toSet())

    }

    private fun nodeDescription(node: HaplotypeNode): String {
        return "${node.taxaList().joinToString { it.name }}:${node.asmContig()}:${node.asmStart()}-${node.asmEnd()}"
    }

    /**
     * This pulls deferred jobs from the variantContextChannel.
     * It waits sequentially for each job to finish and
     * writes each VariantContext to the VariantContextWriter.
     */
    private suspend fun writeToVCF(taxa: List<String>) {

        val vcfOutputWriterBuilder = VariantContextWriterBuilder().unsetOption(Options.INDEX_ON_THE_FLY)

        vcfOutputWriterBuilder.setOutputFile(outputFile())
                .setOutputFileType(VariantContextWriterBuilder.OutputType.VCF)
                .setOption(Options.ALLOW_MISSING_FIELDS_IN_HEADER)
                .build()
                .use { writer ->

                    writer.writeHeader(createHeader(taxa))

                    var numRangesWritten = 0
                    for (contexts in variantContextChannel) {
                        contexts.await().forEach {
                            writer.add(it)
                        }
                        numRangesWritten++
                        val progress = (numRangesWritten.toDouble() / myNumRanges.toDouble() * 100.0).toInt()
                        progress(progress, null)
                    }

                }

    }

    private fun nodeID(node: HaplotypeNode): String {
        return node.id().toString()
    }

    private fun symbolicAllele(node: HaplotypeNode): String {
        return "<${nodeID(node)}>"
    }

    private fun alleleRef(range: ReferenceRange): Allele {
        val allele = if (referenceSequence != null) {
            referenceSequence!!.genotypeAsString(Chromosome.instance(range.chromosome().name), range.start())
        } else {
            "A"
        }
        return Allele.create(allele, true)
    }

    private fun symbolicAlleleAlt(node: HaplotypeNode): Allele {
        return Allele.create(symbolicAllele(node), false)
    }

    private fun createVariantContext(range: ReferenceRange, taxaToIDs: Map<String, List<Int?>>, hapidsToNodes: Map<Int, HaplotypeNode>): VariantContext {

        val alleles = hapidsToNodes.values
                .asSequence()
                .map { node -> Pair(node.id(), symbolicAlleleAlt(node)) }
                .toMap()

        val genotypes = taxaToIDs
                .map { (taxon, hapids) ->
                    if (hapids.isEmpty()) {
                        GenotypeBuilder(taxon, listOf(Allele.NO_CALL)).phased(false).make()
                    } else {
                        val alleles = hapids.map { alleles[it] }
                        if (alleles.size == 1 && makeDiploid()) {
                            GenotypeBuilder(taxon, listOf(alleles[0], alleles[0])).phased(false).make()
                        } else {
                            GenotypeBuilder(taxon, alleles).phased(true).make()
                        }
                    }
                }
                .toList()

        val resultAlleles = mutableListOf<Allele>()
        resultAlleles.add(alleleRef(range))
        resultAlleles.addAll(alleles.values)

        return VariantContextBuilder()
                .source(".")
                .alleles(resultAlleles)
                .chr(range.chromosome().name)
                .start(range.start().toLong())
                .stop(range.end().toLong())
                .attribute("END", range.end().toString())
                .genotypes(genotypes)
                .make()

    }

    private fun variantContexts(range: ReferenceRange, taxaToNodes: Map<String, List<Int>>): VariantContext {

        val hapidsToNodes = myGraph.nodes(range)
                .map { Pair(it.id(), it) }
                .toMap()

        return createVariantContext(range, taxaToNodes, hapidsToNodes)

    }

    /**
     * Gets optional list of reference range ids to include in export.
     */
    private fun parseRefRangeFile(refRangeFile: String?): Optional<Set<Int>> {
        if (refRangeFile == null) {
            return Optional.empty()
        }
        val refRangeIdSet: MutableSet<Int> = HashSet()
        try {
            Utils.getBufferedReader(refRangeFile).use { reader ->
                var currentLine: String
                while (reader.readLine().also { currentLine = it } != null) {
                    val tabIndex = currentLine.indexOf("\t")
                    refRangeIdSet.add(currentLine.substring(0, tabIndex).toInt())
                }
            }
        } catch (e: Exception) {
            myLogger.error("Error loading in the refRangeFile: $refRangeFile")
            throw IllegalStateException("Error loading refRangeFile: $refRangeFile", e)
        }
        return Optional.of(refRangeIdSet)
    }

    /**
     * Output file name
     *
     * @return Output VCF File Name
     */
    fun outputFile(): String {
        return myOutputFile.value()
    }

    /**
     * Set Output VCF File Name. Output file name
     *
     * @param value Output VCF File Name
     *
     * @return this plugin
     */
    fun outputFile(value: String): PathsToVCFHaplotypesPlugin {
        myOutputFile = PluginParameter<String>(myOutputFile, value)
        return this
    }

    /**
     * Reference Range file used to further subset the paths
     * for only specified regions of the genome.
     *
     * @return Reference Range File
     */
    fun refRangeForSNPFile(): String? {
        return myRefRangeForSNPFile.value()
    }

    /**
     * Set Reference Range File. Reference Range file used
     * to further subset the paths for only specified regions
     * of the genome.
     *
     * @param value Reference Range File
     *
     * @return this plugin
     */
    fun refRangeForSNPFile(value: String): PathsToVCFHaplotypesPlugin {
        myRefRangeForSNPFile = PluginParameter<String>(myRefRangeForSNPFile, value)
        return this
    }

    /**
     * Reference Genome.
     *
     * @return Reference Genome
     */
    fun refGenome(): String? {
        return myRefGenome.value()
    }

    /**
     * Set Reference Genome. Reference Genome.
     *
     * @param value Reference Genome
     *
     * @return this plugin
     */
    fun refGenome(value: String): PathsToVCFHaplotypesPlugin {
        myRefGenome = PluginParameter<String>(myRefGenome, value)
        return this
    }

    /**
     * Whether to report haploid paths as homozygousdiploid
     *
     * @return Make Homozygous
     */
    fun makeDiploid(): Boolean {
        return myMakeDiploid.value()
    }

    /**
     * Set Make Diploid. Whether to report haploid paths as homozygousdiploid
     *
     * @param value Make Homozygous
     *
     * @return this plugin
     */
    fun makeDiploid(value: Boolean): PathsToVCFHaplotypesPlugin {
        myMakeDiploid = PluginParameter<Boolean>(myMakeDiploid, value)
        return this
    }

    /**
     * Positions to include in VCF. Can be specified by Genotype
     * file (i.e. VCF, Hapmap, etc.), bed file, or json file
     * containing the requested positions.
     *
     * @return Position List
     */
    fun positions(): PositionList? {
        return myPositions.value()
    }

    /**
     * Set Position List. Positions to include in VCF. Can
     * be specified by Genotype file (i.e. VCF, Hapmap, etc.),
     * bed file, or json file containing the requested positions.
     *
     * @param value Position List
     *
     * @return this plugin
     */
    fun positions(value: PositionList): PathsToVCFHaplotypesPlugin {
        myPositions = PluginParameter<PositionList>(myPositions, value)
        return this
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return "Paths as Haplotypes to VCF"
    }

    override fun getToolTipText(): String {
        return "Paths as Haplotypes to VCF"
    }

}