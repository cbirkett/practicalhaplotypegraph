package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultimap
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.DirectoryCrawler
import org.apache.log4j.Logger
import java.awt.Frame
import java.lang.IllegalStateException
import java.nio.file.Path
import java.nio.file.Paths
import javax.swing.ImageIcon

/**
 * Plugin to Take a Directory of Fastq files and will map them to the pangenome using FastqToMapping Plugin.
 *
 * This plugin if run in paired mode will attempt to pair up the fastq files in the directory.
 *
 * This assumes that the files have names like this:
 *  taxa1_R1.fastq
 *  taxa1_R2.fastq
 *
 *  As long as everything before the last token split by '_' is the same, it will find the pair correctly.
 *
 *  If Files are not in the correct format, FastqToMappingPlugin Should be run instead.
 *
 *  This also works with GZipped fastq files as well.
 *
 *  If Paired = false, it will just walk through each file and align them separately.
 *
 *
 */
@Deprecated("FastqDirToMappingPlugin is now fully replaced by FastqToMappingPlugin.")
class FastqDirToMappingPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(FastqDirToMappingPlugin::class.java)


    private var indexFile = PluginParameter.Builder("minimap2IndexFile", null, String::class.java)
            .guiName("Minimap2 index file for pangenome")
            .inFile()
            .required(true)
            .description("Name of the indexFile file to process")
            .build()
    private var fastqDir = PluginParameter.Builder("fastqDir", null, String::class.java)
            .guiName("Fastq dir to process")
            .inDir()
            .required(true)
            .description("Name of the Fastq dir to process.")
            .build()


    private var mappingFileDir = PluginParameter.Builder("mappingFileDir", null, String::class.java)
            .guiName("Output Read Mapping file Dir")
            .outDir()
            .required(true)
            .description("Name of the haplotype count file.")
            .build()

    private var pairedMode = PluginParameter.Builder("paired", true, Boolean::class.javaObjectType)
            .required(false)
            .description("Run in paired mode.  This will remove any haplotype counts if they are not agreed by the pair.")
            .build()

    private var maxRefRangeError = PluginParameter.Builder("maxRefRangeErr", .25, Double::class.javaObjectType)
            .required(false)
            .description("Maximum allowed error when choosing best reference range to count.  Error is computed 1 - (mostHitRefCount/totalHits)")
            .build()

    private var minimapLocation = PluginParameter.Builder("minimapLocation", "minimap2", String::class.java)
            .guiName("Location of Minimap2 Executable")
            .required(false)
            .description("Location of Minimap2 on file system.  This defaults to use minimap2 if it is on the PATH environment variable.")
            .build()

    override fun processData(input: DataSet?): DataSet? {
        val temp = input?.getDataOfType(HaplotypeGraph::class.java)
                ?: throw IllegalArgumentException("IndexKmerByHammingPlugin: processData: must input one HaplotypeGraph type not null: ")
        if (temp.size != 1) {
            throw IllegalArgumentException("IndexKmerByHammingPlugin: processData: must input one HaplotypeGraph: " + temp.size)
        }
        //Get all the fastq files and feed the file names one by one into FastqToMappingPlugin
        val inclusionFiles = DirectoryCrawler.listPaths("glob:*{.fastq.gz,.fq.gz,.fastq,.fq}", Paths.get(fastqDir()))
        //We could have done this in bash, but automatically finding the pairs will be a lot easier in java

        val pairNameMultimap = HashMultimap.create<String, Path>()


        if(!pairedMode()) {
            myLogger.info("Operating in Single End Mode.  If you wish to run paired mode, set paired = true.  Read Mappings will be removed if the read has been clipped or unaligned.")
            //walk through the files
            inclusionFiles.forEach {
                var fileName = it.fileName.toString().replace(".gz","").replace(".fq","").replace(".fastq","")

                //put them into a multimap<String, filePath>
                pairNameMultimap.put(fileName, it)
            }
            //Walk through the keys in the multimap and run FastqToMappingPlugin
            pairNameMultimap.keySet().forEach {
                val files = pairNameMultimap[it].toList().sorted()

                myLogger.info("Aligning: $it")
                FastqToMappingPlugin(null, false)
                        .indexFile(indexFile())
//                        .mappingFile("${mappingFileDir()}${it}_multimap.txt.gz")
//                        .firstFastqFile(files[0].toString())
                        .maxRefRangeError(maxRefRangeError())
//                        .pairedMode(pairedMode())
                        .minimapLocation(minimapLocation())
                        .performFunction(input)

            }
        }
        else {
            myLogger.info("Operating in Paired End Mode.  If you wish to run single end mode, set paired = true.  If this plugin cannot find both files for the pair, an error will be thrown. \nRead Mappings will be removed if the read has been clipped, unaligned, or if the pair of reads are not mapping properly to the same reference range.")

            //walk through the files
            inclusionFiles.forEach {
                val fileName = it.fileName.toString()
                //split on _
                val fileNameWithoutLastToken = fileName.split("_").dropLast(1).joinToString("_")
                //put them into a multimap<String, filePath>
                pairNameMultimap.put(fileNameWithoutLastToken, it)
            }
            //Walk through the keys in the multimap and run FastqToMappingPlugin
            pairNameMultimap.keySet().forEach {
                val files = pairNameMultimap[it].toList().sorted()

                if(files.size<2) {
                    throw IllegalStateException("FastqDirToMappingPlugin: Cannot find both files using the prefix:${it}.  \n" +
                            "To Fix, please remove single end files from the Directory and run again.  \n" +
                            "Then run the Single end Files using paired=false")
                }

                myLogger.info("Aligning: $it")
                var mappingPlugin = FastqToMappingPlugin(null, false)
                        .indexFile(indexFile())
//                        .mappingFile("${mappingFileDir()}${it}_multimap.txt.gz")
//                        .firstFastqFile(files[0].toString())

                if (pairedMode()) {
//                    mappingPlugin = mappingPlugin.secondFastqFile(files[1].toString())
                }

                mappingPlugin.maxRefRangeError(maxRefRangeError())
//                        .pairedMode(pairedMode())
                        .minimapLocation(minimapLocation())
                        .performFunction(input)


            }
        }
        return null
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = FastqDirToMappingPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "FastqDirToHapCountMinimapPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to Align a Fastq directory and export a hapCount File "
    }

    /**
     * Name of the indexFile file to process
     *
     * @return Minimap2 index file for pangenome
     */
    fun indexFile(): String {
        return indexFile.value()
    }

    /**
     * Set Minimap2 index file for pangenome. Name of the
     * indexFile file to process
     *
     * @param value Minimap2 index file for pangenome
     *
     * @return this plugin
     */
    fun indexFile(value: String): FastqDirToMappingPlugin {
        indexFile = PluginParameter(indexFile, value)
        return this
    }

    /**
     * Name of the Fastq dir to process.
     *
     * @return Fastq dir to process
     */
    fun fastqDir(): String {
        return fastqDir.value()
    }

    /**
     * Set Fastq dir to process. Name of the Fastq dir to
     * process.
     *
     * @param value Fastq dir to process
     *
     * @return this plugin
     */
    fun fastqDir(value: String): FastqDirToMappingPlugin {
        fastqDir = PluginParameter(fastqDir, value)
        return this
    }

    /**
     * Name of the haplotype count file.
     *
     * @return Output Read Mapping file Dir
     */
    fun mappingFileDir(): String {
        return mappingFileDir.value()
    }

    /**
     * Set Output Read Mapping file Dir. Name of the haplotype
     * count file.
     *
     * @param value Output Read Mapping file Dir
     *
     * @return this plugin
     */
    fun mappingFileDir(value: String): FastqDirToMappingPlugin {
        mappingFileDir = PluginParameter(mappingFileDir, value)
        return this
    }

    /**
     * Run in paired mode.  This will remove any haplotype
     * counts if they are not agreed by the pair.
     *
     * @return Paired
     */
    fun pairedMode(): Boolean {
        return pairedMode.value()
    }

    /**
     * Set Paired. Run in paired mode.  This will remove any
     * haplotype counts if they are not agreed by the pair.
     *
     * @param value Paired
     *
     * @return this plugin
     */
    fun pairedMode(value: Boolean): FastqDirToMappingPlugin {
        pairedMode = PluginParameter(pairedMode, value)
        return this
    }

    /**
     * Maximum allowed error when choosing best reference
     * range to count.  Error is computed 1 - (mostHitRefCount/totalHits)
     *
     * @return Max Ref Range Err
     */
    fun maxRefRangeError(): Double {
        return maxRefRangeError.value()
    }

    /**
     * Set Max Ref Rage Err. Maximum allowed error when choosing
     * best reference range to count.  Error is computed 1
     * - (mostHitRefCount/totalHits)
     *
     * @param value Max Ref Range Err
     *
     * @return this plugin
     */
    fun maxRefRangeError(value: Double?): FastqDirToMappingPlugin {
        maxRefRangeError = PluginParameter(maxRefRangeError, value)
        return this
    }

    /**
     * Location of Minimap2 on file system.  Can use minimap2
     * if it is on the PATH environment variable. This will
     * default to minimap2/minimap2 by default.
     *
     * @return Location of Minimap2 Executable
     */
    fun minimapLocation(): String {
        return minimapLocation.value()
    }

    /**
     * Set Location of Minimap2 Executable. Location of Minimap2
     * on file system.  Can use minimap2 if it is on the PATH
     * environment variable. This will default to minimap2/minimap2
     * by default.
     *
     * @param value Location of Minimap2 Executable
     *
     * @return this plugin
     */
    fun minimapLocation(value: String): FastqDirToMappingPlugin {
        minimapLocation = PluginParameter(minimapLocation, value)
        return this
    }

}

fun main(args: Array<String>) {
    GeneratePluginCode.generate(FastqDirToMappingPlugin::class.java)
}