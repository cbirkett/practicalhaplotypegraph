package net.maizegenetics.pangenome.hapCalling

import com.google.common.collect.HashMultimap
import com.google.common.collect.HashMultiset
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import net.maizegenetics.pangenome.api.*
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.taxa.TaxaList
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.File
import java.io.PrintWriter
import javax.swing.ImageIcon


/**
 * Plugin that takes a haplotype graph and a set of read mappings to infer the best (most likely) path through the graph
 * given the read mappings. Read mappings are a list of reads with a set of haplotypes to which that read aligned.
 *
 * The plugin can (1) take a file of read mappings and return a file with a list of haplotypes or (2) take read mappings
 * from a PHG DB and store the resulting list of haplotypes in the DB.
 *
 * If (1) the input is a file, then the plugin can take
 * either a file or a directory containing multiple files. If a directory, all read mapping files will be processed and
 * the haplotype lists output as separate files to an output directory. If the output directory is not specified, then
 * the lists will be written to the input directory. Any path files of the same name, will not be overwritten and a message
 * will be written to the log to that effect, unless the overwrite flag is set to true.
 *
 * If (2) the input comes from a PHG DB, an input read map method and the output path method must be supplied.
 * In addition, a specific taxon or list of taxa for which paths are to be imputed can be supplied. If paths for any of the
 * taxa and methods exist, the paths will not be imputed and a warning message will be written to the log.
 * If an overwrite flag is set to true, any existing paths will be overwritten and a message to that effect written to the log.
 */
class BestHaplotypePathPlugin (parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = Logger.getLogger(BestHaplotypePathPlugin::class.java)
    private val mapExtension = "_multimap.txt.gz"  //the map extension added by FastqDirToMappingPlugin
    private val pathExtension = "_path.txt"  //the path extension added by ExportHaplotypePathToFilePlugin

    enum class ALGORITHM_TYPE {classic, efficient}

    private var keyFile = PluginParameter.Builder("keyFile",null, String::class.java)
            .description("KeyFile file name.  Must be a tab separated file using the following headers:\n" +
                    "SampleName\tReadMappingIds\tLikelyParents\n" +
                    "ReadMappingIds and LikelyParents need to be comma separated for multiple values")
            .required(true)
            .inFile()
            .build()

    private var readMapFilename = PluginParameter.Builder("readFile", null, String::class.java)
            .description("Filename of read mappings. Do not supply both a filename and a directory.")
            .guiName("Read Map File")
            .inFile()
            .build()

    private var readMapDirectory =  PluginParameter.Builder("readDir", null, String::class.java)
            .description("Directory of read mapping files. If this is supplied, do not also assign a read filename.")
            .guiName("Read Map Directory")
            .inDir()
            .build()

    private var pathOutDirectory =  PluginParameter.Builder("outDir", null, String::class.java)
            .description("Directory to which path files will be written.")
            .guiName("Path Output Directory")
            .outDir()
            .build()

    private var readMethodName = PluginParameter.Builder("readMethod", null, String::class.java)
            .required(true)
            .description("The name of the read mapping method in the PHG DB")
            .guiName("Read Mapping Method")
            .build()

    private var pathMethodName = PluginParameter.Builder("pathMethod", null, String::class.java)
            .required(true)
            .description("The name of the path method used to write the results to the PHG DB")
            .guiName("Path Method")
            .build()

    private var pathMethodDescription = PluginParameter.Builder("pathMethodDescription", null, String::class.java)
            .description("An additional description that will be stored with the path method name, if desired.")
            .build()

    private var overwrite = PluginParameter.Builder("overwrite", false, Boolean::class.javaObjectType)
            .description("If an output pathfile already exists for a taxon, then it will be overwritten if overwrite = true. Otherwise, it will not and a warning will be written to the log. Likewise for paths in the PHG DB.")
            .guiName("Overwrite Paths")
            .build()

    private var minTaxaPerRange = PluginParameter.Builder("minTaxa", 20, Int::class.javaObjectType)
            .description("minimum number of taxa per anchor reference range. Ranges with fewer taxa will not be included in the output node list.")
            .build()

    private var minReads = PluginParameter.Builder("minReads", 1, Int::class.javaObjectType)
            .description("minimum number of reads per anchor reference range. Ranges with fewer reads will not be included in the output node list.")
            .build()

    private var maxReadsPerKB = PluginParameter.Builder("maxReads", 10000, Int::class.javaObjectType)
            .description("maximum number of include counts per anchor reference range Kb. Ranges with more reads will not be included in the output node list.")
            .build()

    private var maxNodesPerRange = PluginParameter.Builder("maxNodes", 1000, Int::class.javaObjectType)
            .description("maximum number of nodes per reference range. Ranges with more nodes will not be included in the output node list.")
            .build()

    private var minTransitionProb = PluginParameter.Builder("minTransitionProb", 0.001, Double::class.javaObjectType)
            .description("minimum probability of a transition between nodes at adjacent reference ranges.")
            .build()

    private var probReadMappedCorrectly = PluginParameter.Builder("probCorrect", 0.99, Double::class.javaObjectType)
            .description("minimum number of reads per anchor reference range. Ranges with fewer reads will not be included in the output node list.")
            .build()

    private var splitConsensusNodes = PluginParameter.Builder("splitNodes", true, Boolean::class.javaObjectType)
            .description("split consensus nodes into one node per taxon.")
            .build()

    private var splitTransitionProb = PluginParameter.Builder("splitProb", 0.99, Double::class.javaObjectType)
            .description("When the consensus nodes are split by taxa, this is the transition probability for moving from a node to the next node of the same taxon. It equals 1 minus the probability that the path will switch between taxa.")
            .build()

    private var useBackwardForward = PluginParameter.Builder("usebf", false, Boolean::class.javaObjectType)
            .description("Use the Backward-Forward algorithm instead of the Viterbi algorithm for the HMM.")
            .build()

    private var minProbBF = PluginParameter.Builder("minP", 0.8, Double::class.javaObjectType)
            .description("Only nodes with minP or greater probability will be kept in the path when using the Backward-Forward algorithm,")
            .build()

    private var bfInfoFilename = PluginParameter.Builder("bfInfoFile", null, String::class.java)
            .description("The base name of the file to node probabilities from the backward-forward algorithm will be written. taxonName.txt will be appended to each file.")
            .outFile()
            .dependentOnParameter(useBackwardForward)
            .build()

    private var removeRangesWithEqualCounts: PluginParameter<Boolean> = PluginParameter.Builder("removeEqual", true, Boolean::class.javaObjectType)
            .description("Ranges with equal read counts for all haplotypes should be removed from the graph. Defaults to true but will be always be false if minReads = 0.")
            .build()

    private var myNumThreads = PluginParameter.Builder("numThreads",3, Int::class.javaObjectType)
            .description("Number of threads used to find paths.  The path finding will subtract 2 from this number to have the number of worker threads.  It leaves 1 thread for IO to the DB and 1 thread for the Operating System.")
            .required(false)
            .build()

    private var requiredTaxaList = PluginParameter.Builder("requiredTaxa", null, TaxaList::class.java)
            .description("Optional list of taxa required to have haplotypes. Any reference range that does not have a haplotype" +
                    " for one of these taxa will not be used for path finding. This can be a comma separated list of taxa (no spaces unless surrounded by quotes), " +
                    "file (.txt) with list of taxa names to include, or a taxa list file (.json or .json.gz). By default, all taxa will be included.")
            .build()

    private var algorithmType = PluginParameter.Builder("algorithmType", ALGORITHM_TYPE.efficient, ALGORITHM_TYPE::class.java)
            .description("the type of algorithm. Choices are classic, which is the original implementation described by Rabiner 1989," +
                    " or efficient, which is modified for improved computational efficiency.")
            .range(ALGORITHM_TYPE.values())
            .build()

    private var maxParents = PluginParameter.Builder("maxParents", Int.MAX_VALUE, Int::class.javaObjectType)
        .description("To restrict path finding to the most likely parents, the number of parents used " +
                "will not be greater than maxParents. The number of parents used will be the minimum of maxParents " +
                "and the number of parents needed to reach minCoverage. If both maxParents and minCoverage are left " +
                "at the default, all parents in the input HaplotypeGraph will be used.")
        .build()

    private var minCoverage = PluginParameter.Builder("minCoverage", 1.0, Double::class.javaObjectType)
        .description("To restrict path finding to the most likely parents, the smallest number of parents " +
                "needed to provide read coverage greater than or equal to minCoverage will be used to find paths. " +
                "If maxParents is smaller, that number of parents will be used.")
        .build()

    private var likelyParentFile = PluginParameter.Builder("parentOutputFile", null, String::class.java)
        .outFile()
        .description("The name and path of the output file where the likely parents and their read counts will be written. " +
                "If no file name is provided the likely parents will not be written to a file.")
        .build()

    private var isTestMethod = PluginParameter.Builder("isTestMethod", false, Boolean::class.javaObjectType)
        .description("Indication if the data is to be loaded against a test method. Data loaded with test methods are not cached with the PHG ktor server")
        .required(false).build()


    override fun preProcessParameters(input: DataSet?) {
        //exactly one HaplotypeGraph must be supplied as input
        if (input == null || input.getDataOfType(HaplotypeGraph::class.java).size != 1)
            throw IllegalArgumentException("Exactly one HaplotypeGraph must be supplied as input")
    }

    override fun postProcessParameters() {
        if (readMapFilename() != null && readMapDirectory() != null)
            throw IllegalArgumentException("Cannot specify both an input file and directory.")


    }

    override fun processData(input: DataSet?): DataSet? {
        var hapgraph = input!!.getDataOfType(HaplotypeGraph::class.java)[0].data as HaplotypeGraph

        //add missing nodes
        //split if required
        //filter on maxNodes
        //check for all ranges removed

        //if split is true, reset minTransition to (1 - splitProb)/number of taxa if it is smaller
        if (splitConsensusNodes()) {
            val splitMinTransition = (1.0 - splitTransitionProb()) / hapgraph.totalNumberTaxa()
            if (splitMinTransition < minTransitionProb()) {
                myLogger.info("minTransition was reset to $splitMinTransition because splitProb=true")
                minTransitionProb(splitMinTransition)
            }
        }

        if (algorithmType() == ALGORITHM_TYPE.classic) {
            //filter on numtaxa first, then add missing nodes and, if requested, split on taxa
            hapgraph = applyClassicRangeFilters(hapgraph)
            hapgraph = CreateGraphUtils.addMissingSequenceNodes(hapgraph)
            if (splitConsensusNodes()) hapgraph = CreateGraphUtils.nodesSplitByIndividualTaxa(hapgraph, splitTransitionProb())
        } else if (requiredTaxaList.value() != null && requiredTaxaList.value().size > 0) {
            //remove ranges missing required taxa
            myLogger.info("Before filtering on required taxa, the graph has ${hapgraph.numberOfRanges()} ranges")
            hapgraph = removeRangesMissingRequiredTaxa(hapgraph)
            myLogger.info("After filtering on required taxa, the graph has ${hapgraph.numberOfRanges()} ranges")
        }

        if (readMapFilename() != null) {
            processReadFile(readMapFilename()!!, hapgraph)
        } else if (readMapDirectory() != null) {
            val readDir = File(readMapDirectory())
            if (readDir.exists() && readDir.isDirectory) processReadDirectory(readDir, hapgraph)
            else throw IllegalArgumentException("$readDir is not a directory")
        } else {
            //multithreaded find paths sets thread number = numThreads - 2
            //So unless numThreads is at least four use single threaded processing.
            if (numThreads() > 3) findPathsFromDBReadMappingsMultithread(hapgraph,keyFile())
            else findPathsFromDBReadMappings(hapgraph,keyFile())
        }
        return null
    }

    /**
     * Method to process a single read file and write the resulting path to the output directory.
     * If overwrite = false, then the method will not overwrite an existing path file.
     * @param readfilename  The full path of the the file with read mappings
     * @param graph The HaplotypeGraph that will be used to infer a path as a list of haplotype ids
     */
    fun processReadFile(readfilename : String, graph : HaplotypeGraph) {
        val taxonName = parseTaxonName(readfilename)
        val pathFile = File(pathOutDirectory(), taxonName + pathExtension)

        if (pathFile.exists() && !(overwrite())) {
            myLogger.warn("$readfilename was not processed because a path file for that taxon already exists.")
            return
        }

        val readMap = readInReadMappings(readfilename, graph)
        val converter = ConvertReadsToPathUsingHMM()
                .minReadsPerRange(minReads())
                .removeRangesWithEqualCounts(removeRangesWithEqualCounts())
                .maxReadsPerRangeKB(maxReadsPerKB())
                .minTransitionProbability(minTransitionProb())
                .transitionProbabilitySameTaxon(splitTransitionProb())
                .probabilityReadMappingCorrect(probReadMappedCorrectly())
                .readMap(readMap)

        if (minReads() == 0) converter.removeRangesWithEqualCounts(false)
        converter.filterHaplotypeGraph(graph)
        val result: List<HaplotypeNode> =
            if (useBackwardForward.value()) {
                converter.haplotypeCountsToPathProbability()
                converter.nodeListFromProbabilities(minProbBF.value(), bfInfoFilename())
            } else {
                converter.haplotypeCountsToPath()
            }

        //create a Multimap<String, HaplotypeNode) for the data
        val finalParamMap = pluginParameters().toMutableMap()
        if (pathMethodDescription() != null) finalParamMap.put("notes", pathMethodDescription())

        val description = DBLoadingUtils.formatMethodParamsToJSON(finalParamMap)
        val nodemap = HashMultimap.create<String, HaplotypeNode>()
        result.forEach { nodemap.put(taxonName, it) }
        ExportHaplotypePathToFilePlugin(null, false)
                .outputFileDirectory(pathOutDirectory())
                .refVersion("REF_VERSION")
                .hapCountMethod(readMethodName())
                .pathMethod(pathMethodName())
                .pathMethodDetails(description)
                .performFunction(DataSet.getDataSet(nodemap))

    }

    /**
     * Method that gets a list of read mapping files from the read directory and calls processReadFile
     * for each of the files.
     * @param readDir   The directory containing the read mapping files to be processed
     * @param graph The HaplotypeGraph used to infer paths
     *
     * @see processReadFile
     */
    fun processReadDirectory(readDir : File, graph : HaplotypeGraph) {
        val fileList = readDir.list { dir, name ->  name.endsWith(mapExtension)}
        fileList.forEach { processReadFile(it, graph) }
    }

    /**
     * Method that gets a list of read mappings from the database for a specific method and writes a path to the database
     * for each read mapping record
     * @param graph The HaplotypeGraph used to infer paths
     */
    fun findPathsFromDBReadMappings(graph: HaplotypeGraph, keyFile: String) {

        val dbConnect = DBLoadingUtils.connection(false)
        val phgDB = PHGdbAccess(dbConnect)
        val readMappingDecoder = ReadMappingDecoder(phgDB)

        //Parse the keyfile
        val keyFileParsed = readInKeyFile(keyFile)
        val headers = keyFileParsed.first
        val entries = filterKeyFileOnExistingPaths(keyFileParsed)

        val sampleNameColIndex = headers["SampleName"] ?: -1
        val readMappingColIndex = headers["ReadMappingIds"] ?: -1
        val likelyParentColIndex = headers["LikelyParents"] ?: -1

        check(sampleNameColIndex != -1) { "Error processing keyfile.  Must have SampleName column." }
        check(readMappingColIndex != -1) { "Error processing keyfile.  Must have ReadMappingIds column." }
        check(likelyParentColIndex != -1) { "Error processing keyfile.  Must have LikelyParents column." }

        val hapIdToRefRangeMapping = graph.referenceRanges().flatMap { graph.nodes(it) }
            .map { Pair(it.id(),it.referenceRange()) }
            .toMap()

        //make the path finder factory
        val pathFactory = if (algorithmType() == ALGORITHM_TYPE.efficient && !useBackwardForward())
            PathFinderForSingleTaxonNodesFactory(graph,
                minTaxaPerRange = minTaxaPerRange(),
                minReadsPerRange = minReads(),
                removeEqual = removeRangesWithEqualCounts(),
                maxReadsPerKB = maxReadsPerKB(),
                sameGameteProbability = splitTransitionProb(),
                probCorrect = probReadMappedCorrectly(),
                requiredTaxaList = requiredTaxaList(),
                inbreedCoef = 1.0,
                maxParents = maxParents(),
                minCoverage = minCoverage())
        else null

        val useLikelyParents = graph.totalNumberTaxa() > maxParents() || minCoverage() < 1.0
        val likelyParentsFilename = likelyParentFile()
        val myWriter = if (algorithmType() == ALGORITHM_TYPE.efficient && useLikelyParents && likelyParentsFilename != null) {
            val out = PrintWriter(likelyParentsFilename)
            out.println("sample_name\ttotal_reads\tparent\torder\tnumber_of_reads\tcumulative_reads")
            out
        } else null

        //Loop through the keyfile
        entries.forEach {keyFileEntry ->
            val aggregatedReadMappingCounts = HashMultiset.create<List<Int>>()
            val refRangeToHapIdSetCounts = HashMultimap.create<ReferenceRange,HapIdSetCount>()
            val taxonName = keyFileEntry[sampleNameColIndex]
            val parentList = keyFileEntry[likelyParentColIndex].trim()

            //Loop through each readMappingID
            val readMappingIds = keyFileEntry[readMappingColIndex].split(",").map { mappingId -> mappingId.toInt() }
            //retrieve the readMapping byte[] from the db
            for(mappingId in readMappingIds) {
                val readMappings = readMappingDecoder.getDecodedReadMappingForMappingId(mappingId)
                readMappings.forEach {
                    //Merge all ReadMapping counts into a single readMapping
                    aggregatedReadMappingCounts.add(it.key,it.value)
                }
            }

            //Now that we have the multiset, we need to associate the mappings with the reference range it comes from
            aggregatedReadMappingCounts.elementSet()
                    .filter { it.isNotEmpty() }
                    .forEach {
                        val referenceRange = hapIdToRefRangeMapping[it[0]]
                        refRangeToHapIdSetCounts.put(referenceRange,HapIdSetCount(it.toSet(),aggregatedReadMappingCounts.count(it)))
                    }

            //Run find path using this read mapping
            //Write the read mapping to the DB
            //If Export file is specified, write out the readmapping
            var likelyParentList = listOf<Pair<String,Int>>()
            val start = System.currentTimeMillis()
            val result: List<HaplotypeNode> =
                    if (useBackwardForward.value()) {
                        val converter = ConvertReadsToPathUsingHMM()
                                .minReadsPerRange(minReads())
                                .removeRangesWithEqualCounts(removeRangesWithEqualCounts())
                                .maxReadsPerRangeKB(maxReadsPerKB())
                                .minTransitionProbability(minTransitionProb())
                                .transitionProbabilitySameTaxon(splitTransitionProb())
                                .probabilityReadMappingCorrect(probReadMappedCorrectly())
                                .readMap(refRangeToHapIdSetCounts)

                        if (minReads() == 0) converter.removeRangesWithEqualCounts(false)
                        if (parentList.length > 0)
                            converter.filterHaplotypeGraph(FilterGraphPlugin(null,false).taxaList(parentList).filter(graph))
                        else converter.filterHaplotypeGraph(graph)
                        converter.haplotypeCountsToPathProbability()
                        converter.nodeListFromProbabilities(minProbBF.value(), bfInfoFilename())
                    } else if (algorithmType() == ALGORITHM_TYPE.efficient && pathFactory != null) {
                        val pathFinder = pathFactory.build(refRangeToHapIdSetCounts, parentList)
                        likelyParentList = pathFinder.likelyParents
                        pathFinder.findBestPath()
                    } else {
                        val converter = ConvertReadsToPathUsingHMM()
                                .minReadsPerRange(minReads())
                                .removeRangesWithEqualCounts(removeRangesWithEqualCounts())
                                .maxReadsPerRangeKB(maxReadsPerKB())
                                .minTransitionProbability(minTransitionProb())
                                .transitionProbabilitySameTaxon(splitTransitionProb())
                                .probabilityReadMappingCorrect(probReadMappedCorrectly())
                                .readMap(refRangeToHapIdSetCounts)

                        if (minReads() == 0) converter.removeRangesWithEqualCounts(false)
                        if (parentList.length > 0)
                            converter.filterHaplotypeGraph(FilterGraphPlugin(null,false).taxaList(parentList).filter(graph))
                        else converter.filterHaplotypeGraph(graph)
                        converter.haplotypeCountsToPath()
                    }

            myLogger.info("found path for $taxonName in ${System.currentTimeMillis() - start} ms")
            //Convert result into a byte[] and write it to the DB.
            val encodedPath = DBLoadingUtils.encodePathArrayFromSet(result.toSortedSet())
            phgDB.putPathsData(pathMethodName(), pluginParameters(), taxonName, readMappingIds, encodedPath, isTestMethod())

            if (pathFactory != null && myWriter != null) {
                //myWriter.println("$sampleName\t$totalReads\t$parent\t$order\t$numberOfReads\t$cumulativeReads")
                val totalReads = refRangeToHapIdSetCounts.values().sumBy { it.count }
                var cumulativeReads = 0
                likelyParentList.forEachIndexed { index, pair ->
                    val parent = pair.first
                    val numberOfReads = pair.second
                    cumulativeReads += numberOfReads
                    val cumulativeProportion = cumulativeReads / totalReads.toDouble()
                    myWriter.println("$taxonName\t$totalReads\t$parent\t${index + 1}\t$numberOfReads\t$cumulativeProportion")
                }
            }
        }

        phgDB.close()
        if (myWriter != null) myWriter.close()
    }

    data class PathFindingResult(val keyFileEntry: List<String>, val path: List<HaplotypeNode>, val likelyParents: List<Pair<String,Int>>, val totalReads: Int = -1)

    /**
     * Method to find paths by extracting the read mappings from the DB.
     * Then it will aggregate the counts together by summing the counts for a given hapId set.
     *
     * The code will then find a path for those read mappings.
     */
    fun findPathsFromDBReadMappingsMultithread(graph: HaplotypeGraph, keyFile: String) {
        val hapIdToRefRangeMapping = getHapIdToRefRangeMapping(graph)

        val dbConnect = DBLoadingUtils.connection(false)
        val phgdb = PHGdbAccess(dbConnect)
        val readMappingDecoder = ReadMappingDecoder(phgdb)

        //Parse the keyfile
        val keyFileParsed = readInKeyFile(keyFile)
        val headers = keyFileParsed.first
        val entries = filterKeyFileOnExistingPaths(keyFileParsed)

        val sampleNameColIndex = headers["SampleName"] ?: -1
        val readMappingColIndex = headers["ReadMappingIds"] ?: -1
        val likelyParentColIndex = headers["LikelyParents"] ?: -1

        check(sampleNameColIndex != -1) { "Error processing keyfile.  Must have SampleName column." }
        check(readMappingColIndex != -1) { "Error processing keyfile.  Must have ReadMappingIds column." }
        check(likelyParentColIndex != -1) { "Error processing keyfile.  Must have LikelyParents column." }

        //make the path factory
        val pathFactory = PathFinderForSingleTaxonNodesFactory(
            graph,
            minTaxaPerRange = minTaxaPerRange(),
            minReadsPerRange = minReads(),
            removeEqual = removeRangesWithEqualCounts(),
            maxReadsPerKB = maxReadsPerKB(),
            sameGameteProbability = splitTransitionProb(),
            probCorrect = probReadMappedCorrectly(),
            requiredTaxaList = requiredTaxaList(),
            inbreedCoef = 1.0,
            maxParents = maxParents(),
            minCoverage = minCoverage()
        )

        runBlocking {
            val inputChannel = Channel<List<String>>()
            val resultChannel = Channel<PathFindingResult>()
            val numThreads = Math.max(numThreads() - 2, 1)

            launch {
                myLogger.info("Adding entries to the inputChannel:")
                entries.forEach {keyFileEntry ->
                    myLogger.info("Adding: ${keyFileEntry[0]}")
                    inputChannel.send(keyFileEntry)
                }
                myLogger.info("Done Adding KeyFile entries to the inputChannel:")
                inputChannel.close() //Need to close this here to show the workers that it is done adding more data
            }

            val workerThreads = (1..numThreads).map { threadNum ->
                launch { processKeyFileEntry(pathFactory, inputChannel,resultChannel, readMappingDecoder, hapIdToRefRangeMapping, sampleNameColIndex, readMappingColIndex, likelyParentColIndex) }
            }

            launch {
                processDBUploading(resultChannel, phgdb, sampleNameColIndex, readMappingColIndex)
                myLogger.info("Finished writing to the DB.")
            }


            //Create a coroutine to make sure all the async coroutines are done processing, then close the result channel.
            //If this is not here, this will run forever.
            launch {
                workerThreads.forEach { it.join() }
                resultChannel.close()
            }
        }

        phgdb.close()

    }

    private fun filterKeyFileOnExistingPaths(keyFileParsed : Pair<Map<String,Int>,List<List<String>>>) : List<List<String>> {
        val phg = PHGdbAccess(DBLoadingUtils.connection(false))
        val sampleNameColIndex = keyFileParsed.first["SampleName"] ?: throw java.lang.IllegalArgumentException("key file has no SampleName column")
        val pathTaxa = phg.getTaxaForPathMethod(pathMethodName())
        val filteredRows = keyFileParsed.second.filter { row ->
            !pathTaxa.contains(row[sampleNameColIndex])
        }
        phg.close()

        myLogger.info("The pathKeyFile has ${keyFileParsed.second.size} rows. Paths will be found for ${filteredRows.size}. " +
                "The others already have paths in the db.")

        return filteredRows
    }

    /**
     * Method to process a keyFile entry and find a path.
     *
     * Might need to pull out the DB reading portion.  The multithreading will just be finding the path in this case.
     */
    private suspend fun processKeyFileEntry(pathFactory: PathFinderForSingleTaxonNodesFactory, inputChannel: Channel<List<String>>,
                                            resultChannel: Channel<PathFindingResult>, mappingDecoder: ReadMappingDecoder,
                                            hapIdToRefRangeMapping : Map<Int, ReferenceRange>, sampleNameColIndex : Int,
                                            readMappingColIndex : Int, parentColumnIndex: Int) = withContext(Dispatchers.Default) {
        for (keyFileEntry in inputChannel) {
            val aggregatedReadMappingCounts = HashMultiset.create<List<Int>>()
            val refRangeToHapIdSetCounts = HashMultimap.create<ReferenceRange,HapIdSetCount>()
            val taxonName = keyFileEntry[sampleNameColIndex]
            val parentList = keyFileEntry[parentColumnIndex]

            //Loop through each readMappingID
            val readMappingIds = keyFileEntry[readMappingColIndex].split(",").map { mappingId -> mappingId.toInt() }
            //retrieve the readMapping byte[] from the db
            for(mappingId in readMappingIds) {
                val readMappings = mappingDecoder.getDecodedReadMappingForMappingId(mappingId)
                readMappings.forEach {
                    //Merge all ReadMapping counts into a single readMapping
                    aggregatedReadMappingCounts.add(it.key,it.value)
                }
            }

            //Now that we have the multiset, we need to associate the mappings with the reference range it comes from
            aggregatedReadMappingCounts.elementSet()
                    .filter { it.isNotEmpty() }
                    .forEach {
                        val referenceRange = hapIdToRefRangeMapping[it[0]]
                        refRangeToHapIdSetCounts.put(referenceRange,HapIdSetCount(it.toSet(),aggregatedReadMappingCounts.count(it)))
                    }
            //Run find path using this read mapping
            //Write the read mapping to the DB
            //If Export file is specified, write out the readmapping
            var likelyParentList = listOf<Pair<String,Int>>()
            val result: List<HaplotypeNode> =
                    if (useBackwardForward.value()) {
                        val converter = ConvertReadsToPathUsingHMM()
                                .minReadsPerRange(minReads())
                                .removeRangesWithEqualCounts(removeRangesWithEqualCounts())
                                .maxReadsPerRangeKB(maxReadsPerKB())
                                .minTransitionProbability(minTransitionProb())
                                .transitionProbabilitySameTaxon(splitTransitionProb())
                                .probabilityReadMappingCorrect(probReadMappedCorrectly())
                                .readMap(refRangeToHapIdSetCounts)

                        if (minReads() == 0) converter.removeRangesWithEqualCounts(false)
                        converter.filterHaplotypeGraph(pathFactory.myGraph)
                        if (parentList.length > 0)
                            converter.filterHaplotypeGraph(FilterGraphPlugin(null,false).taxaList(parentList).filter(pathFactory.myGraph))
                        else converter.filterHaplotypeGraph(pathFactory.myGraph)

                        converter.haplotypeCountsToPathProbability()
                        converter.nodeListFromProbabilities(minProbBF.value(), bfInfoFilename())
                    } else if (algorithmType() == ALGORITHM_TYPE.efficient){
                        val pathFinder = pathFactory.build(refRangeToHapIdSetCounts, parentList)
                        likelyParentList = pathFinder.likelyParents
                        pathFactory.build(refRangeToHapIdSetCounts, parentList).findBestPath()
                    } else {
                        val converter = ConvertReadsToPathUsingHMM()
                                .minReadsPerRange(minReads())
                                .removeRangesWithEqualCounts(removeRangesWithEqualCounts())
                                .maxReadsPerRangeKB(maxReadsPerKB())
                                .minTransitionProbability(minTransitionProb())
                                .transitionProbabilitySameTaxon(splitTransitionProb())
                                .probabilityReadMappingCorrect(probReadMappedCorrectly())
                                .readMap(refRangeToHapIdSetCounts)

                        if (minReads() == 0) converter.removeRangesWithEqualCounts(false)
                        if (parentList.length > 0)
                            converter.filterHaplotypeGraph(FilterGraphPlugin(null,false).taxaList(parentList).filter(pathFactory.myGraph))
                        else converter.filterHaplotypeGraph(pathFactory.myGraph)

                        converter.haplotypeCountsToPath()
                    }

            val totalReads = refRangeToHapIdSetCounts.values().sumBy { it.count }
            resultChannel.send(PathFindingResult(keyFileEntry, result, likelyParentList, totalReads))
        }
    }

    /**
     * Single threaded function to upload the path results to the DB and, optionally, write lines to the likely parents report.
     * This takes the path and encodes it into a byte[].
     */
    private suspend fun processDBUploading(resultChannel: Channel<PathFindingResult>, phg: PHGdbAccess, sampleNameColIndex : Int, readMappingColIndex : Int) = withContext(Dispatchers.Default) {
        val parentFile = likelyParentFile()
        val reportWriter = if (parentFile != null) PrintWriter(parentFile) else null
        if (reportWriter != null) reportWriter.println("sample_name\ttotal_reads\tparent\torder\tnumber_of_reads\tcumulative_reads")
        for (result in resultChannel) {
            val keyFileEntry = result.keyFileEntry
            val path = result.path
            val likelyParents = result.likelyParents
            val encodedPath = DBLoadingUtils.encodePathArrayFromSet(path.toSortedSet())
            val taxonName = keyFileEntry[sampleNameColIndex]
            //Loop through each readMappingID
            val readMappingIds = keyFileEntry[readMappingColIndex].split(",").map { mappingId -> mappingId.toInt() }
            phg.putPathsData(pathMethodName(), pluginParameters(), taxonName, readMappingIds, encodedPath, isTestMethod())

            //write the likelyParents to a file
            //header: myWriter.println("$sampleName\t$totalReads\t$parent\t$order\t$numberOfReads\t$cumulativeReads")
            if (reportWriter != null && likelyParents != null) {
                var cumulativeReads = 0
                likelyParents.forEachIndexed { index, pair ->
                    val numberOfReads = pair.second
                    cumulativeReads += numberOfReads
                    reportWriter.println(
                        "${taxonName}\t${result.totalReads}\t${pair.first}\t${index + 1}" +
                                "\t$numberOfReads\t${cumulativeReads.toDouble() / result.totalReads.toDouble()}"
                    )
                }
            }
        }
        if (reportWriter != null) reportWriter.close()
    }

            /**
     * Function to get the hapIds->RefRange Mappings
     */
    /**
     * Function to get the hapIds->RefRange Mappings
     */
    private fun getHapIdToRefRangeMapping(graph: HaplotypeGraph) : Map<Int, ReferenceRange> {
        return graph.referenceRanges().flatMap { graph.nodes(it) }
                .map { Pair(it.id(),it.referenceRange()) }
                .toMap()
    }

    private fun removeRangesMissingRequiredTaxa(phGraph : HaplotypeGraph) : HaplotypeGraph {

        val listOfRangesToRemove = phGraph.referenceRangeList().filter {
            val nodeTaxa = phGraph.nodes(it).flatMap { it.taxaList() }.toSet()
            !nodeTaxa.containsAll(requiredTaxaList.value())
        }.toList()
        myLogger.info("filtering on required taxa removed ")
        if (listOfRangesToRemove.size < 1) return phGraph
        if (listOfRangesToRemove.size == phGraph.numberOfRanges()) throw java.lang.IllegalArgumentException("all ranges removed when filtering for required taxa.")

        return FilterGraphPlugin(null, false).refRanges(listOfRangesToRemove).filter(phGraph)
    }

    private fun applyClassicRangeFilters(phGraph : HaplotypeGraph) : HaplotypeGraph {
        //counter 0: number of ranges that do not contain the required taxa
        //counter 1: number of ranges that have more than maxNodesPerRange
        //counter 2: number of ranges that fewer than minTaxaPerRange
        val counters = IntArray(3, {0})
        val listOfRangesToRemove = phGraph.referenceRangeList().filter {
            val myNodes = phGraph.nodes(it)
            val nodeTaxa = myNodes.flatMap { it.taxaList() }.toSet()
            val containsRequiredTaxa = if (requiredTaxaList() != null) nodeTaxa.containsAll(requiredTaxaList.value()) else true
            val numberOfNodes = myNodes.size
            val numberOfTaxa = nodeTaxa.size
            if (!containsRequiredTaxa) counters[0]++
            if (numberOfNodes > maxNodesPerRange()) counters[1]++
            if (numberOfTaxa < minTaxaPerRange()) counters[2]++

            !containsRequiredTaxa || numberOfNodes > maxNodesPerRange() || numberOfTaxa < minTaxaPerRange()
        }.toList()


        //filter on maxNodes && minTaxa
        //check for all ranges removed
        myLogger.info("Classic filter found ${counters[0]} ranges missing one of the required taxa, " +
                "${counters[1]} ranges with too many nodes, and ${counters[2]} with too few taxa}")
        myLogger.info("Classic filter removed ${listOfRangesToRemove.size} ranges")

        if (listOfRangesToRemove.size < 1) return phGraph
        if (listOfRangesToRemove.size == phGraph.numberOfRanges()) throw java.lang.IllegalArgumentException("all ranges removed in range pre-filtering step.")
        return FilterGraphPlugin(null, false).refRanges(listOfRangesToRemove.toList()).filter(phGraph)

    }

    override fun getToolTipText(): String {
        return "Impute the best path through a graph"
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = BestHaplotypePathPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "HapPath"
    }

    override fun pluginDescription(): String {
        return "Plugin that takes a haplotype graph and a set of read mappings to infer the best path through the graph. " +
                "The input read mappings can be supplied as a file, a directory of files, or entries from a DB."
    }

    /**
     * KeyFile file name.  Must be a tab separated file using
     * the following headers:
     * SampleName	ReadMappingIds	LikelyParents
     * ReadMappingIds and LikelyParents need to be comma separated
     * for multiple values
     *
     * @return KeyFile
     */
    /**
     * KeyFile file name.  Must be a tab separated file using
     * the following headers:
     * SampleName	ReadMappingIds	LikelyParents
     * ReadMappingIds and LikelyParents need to be comma separated
     * for multiple values
     *
     * @return KeyFile
     */
    fun keyFile(): String {
        return keyFile.value()
    }

    /**
     * Set KeyFile. KeyFile file name.  Must be a tab separated
     * file using the following headers:
     * SampleName	ReadMappingIds	LikelyParents
     * ReadMappingIds and LikelyParents need to be comma separated
     * for multiple values
     *
     * @param value KeyFile
     *
     * @return this plugin
     */
    fun keyFile(value: String): BestHaplotypePathPlugin {
        keyFile = PluginParameter<String>(keyFile, value)
        return this
    }

    /**
     * Filename of read mappings. Do not supply both a filename
     * and a directory.
     *
     * @return Read Map File
     */
    fun readMapFilename(): String? {
        return readMapFilename.value()
    }

    /**
     * Set Read Map File. Filename of read mappings. Do not
     * supply both a filename and a directory.
     *
     * @param value Read Map File
     *
     * @return this plugin
     */
    fun readMapFilename(value: String?): BestHaplotypePathPlugin {
        readMapFilename = PluginParameter(readMapFilename, value)
        return this
    }

    /**
     * Directory of read mapping files. If this is supplied,
     * do not also assign a read filename.
     *
     * @return Read Map Directory
     */
    fun readMapDirectory(): String? {
        return readMapDirectory.value()
    }

    /**
     * Set Read Map Directory. Directory of read mapping files.
     * If this is supplied, do not also assign a read filename.
     *
     * @param value Read Map Directory
     *
     * @return this plugin
     */
    fun readMapDirectory(value: String?): BestHaplotypePathPlugin {
        readMapDirectory = PluginParameter(readMapDirectory, value)
        return this
    }

    /**
     * Directory to which path files will be written.
     *
     * @return Read Map Directory
     */
    fun pathOutDirectory(): String? {
        return pathOutDirectory.value()
    }

    /**
     * Set Read Map Directory. Directory to which path files
     * will be written.
     *
     * @param value Read Map Directory
     *
     * @return this plugin
     */
    fun pathOutDirectory(value: String): BestHaplotypePathPlugin {
        pathOutDirectory = PluginParameter(pathOutDirectory, value)
        return this
    }

    /**
     * The name of the read method in the PHG DB
     *
     * @return Read Method
     */
    fun readMethodName(): String? {
        return readMethodName.value()
    }

    /**
     * Set Read Method. The name of the read method in the
     * PHG DB
     *
     * @param value Read Method
     *
     * @return this plugin
     */
    fun readMethodName(value: String): BestHaplotypePathPlugin {
        readMethodName = PluginParameter(readMethodName, value)
        return this
    }

    /**
     * The name of the path method used to write the results
     * to the PHG DB
     *
     * @return Path Method
     */
    fun pathMethodName(): String? {
        return pathMethodName.value()
    }

    /**
     * Set Path Method. The name of the path method used to
     * write the results to the PHG DB
     *
     * @param value Path Method
     *
     * @return this plugin
     */
    fun pathMethodName(value: String): BestHaplotypePathPlugin {
        pathMethodName = PluginParameter(pathMethodName, value)
        return this
    }

    /**
     * An additional description that will be stored with
     * the path method name, if desired.
     *
     * @return Path Method Description
     */
    fun pathMethodDescription(): String? {
        return pathMethodDescription.value()
    }

    /**
     * Set Path Method Description. An additional description
     * that will be stored with the path method name, if desired.
     *
     * @param value Path Method Description
     *
     * @return this plugin
     */
    fun pathMethodDescription(value: String): BestHaplotypePathPlugin {
        pathMethodDescription = PluginParameter<String>(pathMethodDescription, value)
        return this
    }

    /**
     * Overwrite
     *
     * @return Overwrite
     */
    fun overwrite(): Boolean {
        return overwrite.value()
    }

    /**
     * Set Overwrite. Overwrite
     *
     * @param value Overwrite
     *
     * @return this plugin
     */
    fun overwrite(value: Boolean): BestHaplotypePathPlugin {
        overwrite = PluginParameter(overwrite, value)
        return this
    }

    /**
     * minimum number of taxa per anchor reference range.
     * Ranges with fewer taxa will not be included in the
     * output node list.
     *
     * @return Min Taxa
     */
    fun minTaxaPerRange(): Int {
        return minTaxaPerRange.value()
    }

    /**
     * Set Min Taxa. minimum number of taxa per anchor reference
     * range. Ranges with fewer taxa will not be included
     * in the output node list.
     *
     * @param value Min Taxa
     *
     * @return this plugin
     */
    fun minTaxaPerRange(value: Int): BestHaplotypePathPlugin {
        minTaxaPerRange = PluginParameter(minTaxaPerRange, value)
        return this
    }

    /**
     * minimum number of reads per anchor reference range.
     * Ranges with fewer reads will not be included in the
     * output node list.
     *
     * @return Min Reads
     */
    fun minReads(): Int {
        return minReads.value()
    }

    /**
     * Set Min Reads. minimum number of reads per anchor reference
     * range. Ranges with fewer reads will not be included
     * in the output node list.
     *
     * @param value Min Reads
     *
     * @return this plugin
     */
    fun minReads(value: Int): BestHaplotypePathPlugin {
        minReads = PluginParameter(minReads, value)
        return this
    }

    /**
     * maximum number of include counts per anchor reference
     * range Kb. Ranges with more reads will not be included
     * in the output node list.
     *
     * @return Max Reads
     */
    fun maxReadsPerKB(): Int {
        return maxReadsPerKB.value()
    }

    /**
     * Set Max Reads. maximum number of include counts per
     * anchor reference range Kb. Ranges with more reads will
     * not be included in the output node list.
     *
     * @param value Max Reads
     *
     * @return this plugin
     */
    fun maxReadsPerKB(value: Int): BestHaplotypePathPlugin {
        maxReadsPerKB = PluginParameter(maxReadsPerKB, value)
        return this
    }

    /**
     * maximum number of nodes per reference range. Ranges
     * with more nodes will not be included in the output
     * node list.
     *
     * @return Max Nodes
     */
    fun maxNodesPerRange(): Int {
        return maxNodesPerRange.value()
    }

    /**
     * Set Max Nodes. maximum number of nodes per reference
     * range. Ranges with more nodes will not be included
     * in the output node list.
     *
     * @param value Max Nodes
     *
     * @return this plugin
     */
    fun maxNodesPerRange(value: Int): BestHaplotypePathPlugin {
        maxNodesPerRange = PluginParameter(maxNodesPerRange, value)
        return this
    }

    /**
     * minimum probability of a transition between nodes at
     * adjacent reference ranges.
     *
     * @return Min Transition Prob
     */
    fun minTransitionProb(): Double {
        return minTransitionProb.value()
    }

    /**
     * Set Min Transition Prob. minimum probability of a transition
     * between nodes at adjacent reference ranges.
     *
     * @param value Min Transition Prob
     *
     * @return this plugin
     */
    fun minTransitionProb(value: Double): BestHaplotypePathPlugin {
        minTransitionProb = PluginParameter(minTransitionProb, value)
        return this
    }

    /**
     * minimum number of reads per anchor reference range.
     * Ranges with fewer reads will not be included in the
     * output node list.
     *
     * @return Prob Correct
     */
    fun probReadMappedCorrectly(): Double {
        return probReadMappedCorrectly.value()
    }

    /**
     * Set Prob Correct. minimum number of reads per anchor
     * reference range. Ranges with fewer reads will not be
     * included in the output node list.
     *
     * @param value Prob Correct
     *
     * @return this plugin
     */
    fun probReadMappedCorrectly(value: Double): BestHaplotypePathPlugin {
        probReadMappedCorrectly = PluginParameter(probReadMappedCorrectly, value)
        return this
    }

    /**
     * split consensus nodes into one node per taxon.
     *
     * @return Split Nodes
     */
    fun splitConsensusNodes(): Boolean {
        return splitConsensusNodes.value()
    }

    /**
     * Set Split Nodes. split consensus nodes into one node
     * per taxon.
     *
     * @param value Split Nodes
     *
     * @return this plugin
     */
    fun splitConsensusNodes(value: Boolean): BestHaplotypePathPlugin {
        splitConsensusNodes = PluginParameter(splitConsensusNodes, value)
        return this
    }

    /**
     * When the consensus nodes are split by taxa, this is
     * the transition probability for moving from a node to
     * the next node of the same taxon. It equals 1 minus
     * the probability of a recombination between adjacent
     * nodes.
     *
     * @return Split Prob
     */
    fun splitTransitionProb(): Double {
        return splitTransitionProb.value()
    }

    /**
     * Set Split Prob. When the consensus nodes are split
     * by taxa, this is the transition probability for moving
     * from a node to the next node of the same taxon. It
     * equals 1 minus the probability of a recombination between
     * adjacent nodes.
     *
     * @param value Split Prob
     *
     * @return this plugin
     */
    fun splitTransitionProb(value: Double): BestHaplotypePathPlugin {
        splitTransitionProb = PluginParameter(splitTransitionProb, value)
        return this
    }

    /**
     * Use the Backward-Forward algorithm instead of the Viterbi
     * algorithm for the HMM.
     *
     * @return Usebf
     */
    fun useBackwardForward(): Boolean {
        return useBackwardForward.value()
    }

    /**
     * Set Usebf. Use the Backward-Forward algorithm instead
     * of the Viterbi algorithm for the HMM.
     *
     * @param value Usebf
     *
     * @return this plugin
     */
    fun useBackwardForward(value: Boolean): BestHaplotypePathPlugin {
        useBackwardForward = PluginParameter(useBackwardForward, value)
        return this
    }

    /**
     * Only nodes with minP or greater probability will be
     * kept in the path when using the Backward-Forward algorithm,
     *
     * @return Min P
     */
    fun minProbBF(): Double {
        return minProbBF.value()
    }

    /**
     * Set Min P. Only nodes with minP or greater probability
     * will be kept in the path when using the Backward-Forward
     * algorithm,
     *
     * @param value Min P
     *
     * @return this plugin
     */
    fun minProbBF(value: Double): BestHaplotypePathPlugin {
        minProbBF = PluginParameter(minProbBF, value)
        return this
    }

    /**
     * The base name of the file to node probabilities from
     * the backward-forward algorithm will be written. taxonName.txt
     * will be appended to each file.
     *
     * @return Bf Info File
     */
    fun bfInfoFilename(): String? {
        return bfInfoFilename.value()
    }

    /**
     * Set Bf Info File. The base name of the file to node
     * probabilities from the backward-forward algorithm will
     * be written. taxonName.txt will be appended to each
     * file.
     *
     * @param value Bf Info File
     *
     * @return this plugin
     */
    fun bfInfoFilename(value: String?): BestHaplotypePathPlugin {
        bfInfoFilename = PluginParameter(bfInfoFilename, value)
        return this
    }

    /**
     * Ranges with equal read counts for all haplotypes should
     * be removed from the graph. Defaults to true but will
     * be always be false if minReads = 0.
     *
     * @return Remove Equal
     */
    fun removeRangesWithEqualCounts(): Boolean {
        return removeRangesWithEqualCounts.value()
    }

    /**
     * Set Remove Equal. Ranges with equal read counts for
     * all haplotypes should be removed from the graph. Defaults
     * to true but will be always be false if minReads = 0.
     *
     * @param value Remove Equal
     *
     * @return this plugin
     */
    fun removeRangesWithEqualCounts(value: Boolean): BestHaplotypePathPlugin {
        removeRangesWithEqualCounts = PluginParameter(removeRangesWithEqualCounts, value)
        return this
    }

    /**
     * Number of threads used to upload
     *
     * @return Num Threads
     */
    fun numThreads() : Int {
        return myNumThreads.value()
    }

    /**
     * Set Num Threads. Number of threads used to upload
     *
     * @param value Num Threads
     *
     * @return this plugin
     */
    fun numThreads(value : Int) : BestHaplotypePathPlugin {
        myNumThreads = PluginParameter(myNumThreads, value)
        return this
    }

    /**
     * Optional list of taxa required to have haplotypes.
     * Any reference range that does not have a haplotype
     * for one of these taxa will not be used for path finding.
     * This can be a comma separated list of taxa (no spaces
     * unless surrounded by quotes), file (.txt) with list
     * of taxa names to include, or a taxa list file (.json
     * or .json.gz). By default, all taxa will be included.
     *
     * @return Required Taxa
     */
    fun requiredTaxaList(): TaxaList? {
        return requiredTaxaList.value()
    }

    /**
     * Set Required Taxa. Optional list of taxa required to
     * be have haplotypes. Any reference range that does not
     * have a haplotype for one of these taxa will not be
     * used for path finding. This can be a comma separated
     * list of taxa (no spaces unless surrounded by quotes),
     * file (.txt) with list of taxa names to include, or
     * a taxa list file (.json or .json.gz). By default, all
     * taxa will be included.
     *
     * @param value Required Taxa
     *
     * @return this plugin
     */
    fun requiredTaxaList(value: TaxaList): BestHaplotypePathPlugin {
        requiredTaxaList = PluginParameter<TaxaList>(requiredTaxaList, value)
        return this
    }

    /**
     * the type of algorithm. Choices are classic, which is
     * the original implementation describe by Rabiner 1989,
     * or efficient, which is modified for improved computational
     * efficiency.
     *
     * @return Algorithm Type
     */
    fun algorithmType(): ALGORITHM_TYPE {
        return algorithmType.value()
    }

    /**
     * Set Algorithm Type. the type of algorithm. Choices
     * are classic, which is the original implementation describe
     * by Rabiner 1989, or efficient, which is modified for
     * improved computational efficiency.
     *
     * @param value Algorithm Type
     *
     * @return this plugin
     */
    fun algorithmType(value: ALGORITHM_TYPE): BestHaplotypePathPlugin {
        algorithmType = PluginParameter<ALGORITHM_TYPE>(algorithmType, value)
        return this
    }

    /**
     * To restrict path finding to the most likely parents,
     * the number of parents used will not be greater than
     * maxParents. The number of parents used will be the
     * minimum of maxParents and the number of parents needed
     * to reach minCoverage. If both maxParents and minCoverage
     * are left at the default, all parents in the input HaplotypeGraph
     * will be used.
     *
     * @return Max Parents
     */
    fun maxParents(): Int {
        return maxParents.value()
    }

    /**
     * Set Max Parents. To restrict path finding to the most
     * likely parents, the number of parents used will not
     * be greater than maxParents. The number of parents used
     * will be the minimum of maxParents and the number of
     * parents needed to reach minCoverage. If both maxParents
     * and minCoverage are left at the default, all parents
     * in the input HaplotypeGraph will be used.
     *
     * @param value Max Parents
     *
     * @return this plugin
     */
    fun maxParents(value: Int): BestHaplotypePathPlugin {
        maxParents = PluginParameter(maxParents, value)
        return this
    }

    /**
     * To restrict path finding to the most likely parents,
     * the smallest number of parents needed to provide read
     * coverage greater than or equal to minCoverage will
     * be used to find paths. If maxParents is smaller, that
     * number of parents will be used.
     *
     * @return Min Coverage
     */
    fun minCoverage(): Double {
        return minCoverage.value()
    }

    /**
     * Set Min Coverage. To restrict path finding to the most
     * likely parents, the smallest number of parents needed
     * to provide read coverage greater than or equal to minCoverage
     * will be used to find paths. If maxParents is smaller,
     * that number of parents will be used.
     *
     * @param value Min Coverage
     *
     * @return this plugin
     */
    fun minCoverage(value: Double): BestHaplotypePathPlugin {
        minCoverage = PluginParameter(minCoverage, value)
        return this
    }

    /**
     * The name and path of the file of likely parents and
     * their read counts.
     *
     * @return Parent Output File
     */
    fun likelyParentFile(): String? {
        return likelyParentFile.value()
    }

    /**
     * Set Parent Output File. The name and path of the file
     * of likely parents and their read counts.
     *
     * @param value Parent Output File
     *
     * @return this plugin
     */
    fun likelyParentFile(value: String): BestHaplotypePathPlugin {
        likelyParentFile = PluginParameter(likelyParentFile, value)
        return this
    }

    /**
     * Indication if the data is to be loaded against a test
     * method. Data loaded with test methods are not cached
     * with the PHG ktor server
     *
     * @return Is Test Method
     */
    fun isTestMethod(): Boolean {
        return isTestMethod.value()
    }

    /**
     * Set Is Test Method. Indication if the data is to be
     * loaded against a test method. Data loaded with test
     * methods are not cached with the PHG ktor server
     *
     * @param value Is Test Method
     *
     * @return this plugin
     */
    fun isTestMethod(value: Boolean): BestHaplotypePathPlugin {
        isTestMethod = PluginParameter<Boolean>(isTestMethod, value)
        return this
    }


}

private fun parseTaxonName(currentPath: String): String {
    val currentPathSplit = currentPath.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

    val fileName = currentPathSplit[currentPathSplit.size - 1]

    return fileName.substring(0, fileName.lastIndexOf("."))
}

fun main() {
    GeneratePluginCode.generateKotlin(BestHaplotypePathPlugin::class.java)

}

