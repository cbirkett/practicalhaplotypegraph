package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import org.apache.log4j.Logger
import java.awt.Frame
import javax.swing.ImageIcon

/**
 * Plugin to load in multisample BAM based readMappings into the DB. This is effectively the same result as FastqToMappingPlugin or SAMToMappingPlugin.
 *
 * The main difference is that this requires some additional architecture in order to correctly resplit up the BAM file into the correct alignments.
 *
 * This Plugin requires a fastqGroupingFile generated from MergeFastqPlugin in order to determine which reads match up to which samples.
 */
class MultisampleBAMToMappingPlugin (parentFrame: Frame?=null, isInteractive: Boolean=false) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(MultisampleBAMToMappingPlugin::class.java)

    private var keyFile = PluginParameter.Builder("keyFile", null, String::class.java)
            .guiName("keyFile")
            .inFile()
            .required(true)
            .description("Name of the Keyfile to process.  Must have columns cultivar, flowcell_lane, filename, and PlateID.  Optionally for paired end reads, filename2 is needed.  " +
                    "If filename2 is not supplied, Minimap2 will run in single end mode.  Otherwise will be paired.")
            .build()

    private var fastqGroupingFile = PluginParameter.Builder("fastqGroupingFile", null, String::class.java)
            .guiName("Fastq Grouping File")
            .inFile()
            .required(true)
            .description("Grouping file created by MergeFastqPlugin")
            .build()

    private var samDir = PluginParameter.Builder("samDir", null, String::class.java)
            .guiName("SAM/BAM dir to process")
            .inDir()
            .required(true)
            .description("Name of the SAM/BAM dir to process.")
            .build()

    private var maxRefRangeError = PluginParameter.Builder("maxRefRangeErr", .25, Double::class.javaObjectType)
            .required(false)
            .description("Maximum allowed error when choosing best reference range to count.  Error is computed 1 - (mostHitRefCount/totalHits)")
            .build()

    private var methodName = PluginParameter.Builder("methodName", null, String::class.java)
            .guiName("Method Name")
            .required(true)
            .description("Method name to be stored in the DB.")
            .build()

    private var methodDescription = PluginParameter.Builder("methodDescription", null, String::class.java)
            .guiName("Method Description")
            .required(false)
            .description("Method description to be stored in the DB.")
            .build()

    private var outputDebugDir = PluginParameter.Builder("debugDir", "", String::class.java)
            .guiName("Debug Directory to write out read Mapping files.")
            .outDir()
            .required(false)
            .description("Directory to write out the read mapping files.  This is optional for debug purposes.")
            .build()

    private var outputSecondaryMappingStats = PluginParameter.Builder("outputSecondaryStats",false,Boolean::class.javaObjectType)
            .guiName("Output secondary mapping statistics.")
            .required(false)
            .description("Ouptput Secondary Mapping Statistics such as total AS for each haplotype ID")
            .build()

    private var isTestMethod = PluginParameter.Builder("isTestMethod", false, Boolean::class.javaObjectType)
        .description("Indication if the data is to be loaded against a test method. Data loaded with test methods are not cached with the PHG ktor server")
        .required(false).build()

    override fun processData(input: DataSet?): DataSet? {
        //Load in the graph to check reference Ranges

        val temp = input?.getDataOfType(HaplotypeGraph::class.java)
                ?: throw IllegalArgumentException("FastqToMappingPlugin: processData: must input one HaplotypeGraph type not null: ")
        if (temp.size != 1) {
            throw IllegalArgumentException("FastqToMappingPlugin: processData: must input one HaplotypeGraph: " + temp.size)
        }
        val graph: HaplotypeGraph = temp[0].data as HaplotypeGraph

        createReadMappingsFromMultisampleBAMs(graph = graph,
            keyFileName = keyFile(),
            fastqGroupingFileName = fastqGroupingFile(),
            inputFileDir = samDir(),
            maxRefRangeError = maxRefRangeError(),
            methodName = methodName(),
            methodDescription = methodDescription(),
            pluginParams = pluginParameters(),
            outputDebugReadMappingDir = outputDebugDir(),
            outputSecondaryMappingStats = outputSecondaryMappingStats(),
            isTestMethod = isTestMethod())

        return null

    }

    override fun getIcon(): ImageIcon? {
        val imageURL = MultisampleBAMToMappingPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "MultisampleBAMToMappingPlugin"
    }

    override fun getToolTipText(): String {
        return "Plugin to take multisample BAM files and create readMapping counts for each sample. "
    }

    /**
     * Name of the Keyfile to process.  Must have columns
     * cultivar, flowcell_lane, filename, and PlateID.  Optionally
     * for paired end reads, filename2 is needed.  If filename2
     * is not supplied, Minimap2 will run in single end mode.
     *  Otherwise will be paired.
     *
     * @return keyFile
     */
    fun keyFile(): String {
        return keyFile.value()
    }

    /**
     * Set keyFile. Name of the Keyfile to process.  Must
     * have columns cultivar, flowcell_lane, filename, and
     * PlateID.  Optionally for paired end reads, filename2
     * is needed.  If filename2 is not supplied, Minimap2
     * will run in single end mode.  Otherwise will be paired.
     *
     * @param value keyFile
     *
     * @return this plugin
     */
    fun keyFile(value: String): MultisampleBAMToMappingPlugin {
        keyFile = PluginParameter<String>(keyFile, value)
        return this
    }

    /**
     * Grouping file created by MergeFastqPlugin
     *
     * @return Fastq Grouping File
     */
    fun fastqGroupingFile(): String {
        return fastqGroupingFile.value()
    }

    /**
     * Set Fastq Grouping File. Grouping file created by MergeFastqPlugin
     *
     * @param value Fastq Grouping File
     *
     * @return this plugin
     */
    fun fastqGroupingFile(value: String): MultisampleBAMToMappingPlugin {
        fastqGroupingFile = PluginParameter<String>(fastqGroupingFile, value)
        return this
    }

    /**
     * Name of the SAM/BAM dir to process.
     *
     * @return SAM/BAM dir to process
     */
    fun samDir(): String {
        return samDir.value()
    }

    /**
     * Set SAM/BAM dir to process. Name of the SAM/BAM dir
     * to process.
     *
     * @param value SAM/BAM dir to process
     *
     * @return this plugin
     */
    fun samDir(value: String): MultisampleBAMToMappingPlugin {
        samDir = PluginParameter<String>(samDir, value)
        return this
    }

    /**
     * Maximum allowed error when choosing best reference
     * range to count.  Error is computed 1 - (mostHitRefCount/totalHits)
     *
     * @return Max Ref Range Err
     */
    fun maxRefRangeError(): Double {
        return maxRefRangeError.value()
    }

    /**
     * Set Max Ref Range Err. Maximum allowed error when choosing
     * best reference range to count.  Error is computed 1
     * - (mostHitRefCount/totalHits)
     *
     * @param value Max Ref Range Err
     *
     * @return this plugin
     */
    fun maxRefRangeError(value: Double): MultisampleBAMToMappingPlugin {
        maxRefRangeError = PluginParameter<Double>(maxRefRangeError, value)
        return this
    }

    /**
     * Method name to be stored in the DB.
     *
     * @return Method Name
     */
    fun methodName(): String {
        return methodName.value()
    }

    /**
     * Set Method Name. Method name to be stored in the DB.
     *
     * @param value Method Name
     *
     * @return this plugin
     */
    fun methodName(value: String): MultisampleBAMToMappingPlugin {
        methodName = PluginParameter<String>(methodName, value)
        return this
    }

    /**
     * Method description to be stored in the DB.
     *
     * @return Method Description
     */
    fun methodDescription(): String {
        return methodDescription.value()
    }

    /**
     * Set Method Description. Method description to be stored
     * in the DB.
     *
     * @param value Method Description
     *
     * @return this plugin
     */
    fun methodDescription(value: String): MultisampleBAMToMappingPlugin {
        methodDescription = PluginParameter<String>(methodDescription, value)
        return this
    }

    /**
     * Directory to write out the read mapping files.  This
     * is optional for debug purposes.
     *
     * @return Debug Directory to write out read Mapping files.
     */
    fun outputDebugDir(): String {
        return outputDebugDir.value()
    }

    /**
     * Set Debug Directory to write out read Mapping files..
     * Directory to write out the read mapping files.  This
     * is optional for debug purposes.
     *
     * @param value Debug Directory to write out read Mapping files.
     *
     * @return this plugin
     */
    fun outputDebugDir(value: String): MultisampleBAMToMappingPlugin {
        outputDebugDir = PluginParameter<String>(outputDebugDir, value)
        return this
    }

    /**
     * Ouptput Secondary Mapping Statistics such as total
     * AS for each haplotype ID
     *
     * @return Output secondary mapping statistics.
     */
    fun outputSecondaryMappingStats(): Boolean {
        return outputSecondaryMappingStats.value()
    }

    /**
     * Set Output secondary mapping statistics.. Ouptput Secondary
     * Mapping Statistics such as total AS for each haplotype
     * ID
     *
     * @param value Output secondary mapping statistics.
     *
     * @return this plugin
     */
    fun outputSecondaryMappingStats(value: Boolean): MultisampleBAMToMappingPlugin {
        outputSecondaryMappingStats = PluginParameter<Boolean>(outputSecondaryMappingStats, value)
        return this
    }

    /**
     * Indication if the data is to be loaded against a test
     * method. Data loaded with test methods are not cached
     * with the PHG ktor server
     *
     * @return Is Test Method
     */
    fun isTestMethod(): Boolean {
        return isTestMethod.value()
    }

    /**
     * Set Is Test Method. Indication if the data is to be
     * loaded against a test method. Data loaded with test
     * methods are not cached with the PHG ktor server
     *
     * @param value Is Test Method
     *
     * @return this plugin
     */
    fun isTestMethod(value: Boolean): MultisampleBAMToMappingPlugin {
        isTestMethod = PluginParameter<Boolean>(isTestMethod, value)
        return this
    }

}

fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(MultisampleBAMToMappingPlugin::class.java)
}