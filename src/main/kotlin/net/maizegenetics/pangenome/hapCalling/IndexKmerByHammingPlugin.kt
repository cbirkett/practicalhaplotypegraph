@file:JvmName("IndexKmerByHammingPlugin")

package net.maizegenetics.pangenome.hapCalling

import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap
import it.unimi.dsi.fastutil.longs.Long2LongOpenHashMap
import it.unimi.dsi.fastutil.longs.Long2ObjectMap
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap
import net.maizegenetics.dna.BaseEncoder
import net.maizegenetics.dna.snp.NucleotideAlignmentConstants
import net.maizegenetics.pangenome.api.HaplotypeGraph
import net.maizegenetics.pangenome.api.HaplotypeNode
import net.maizegenetics.pangenome.api.ReferenceRange
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.PluginParameter
import org.apache.log4j.Logger
import java.awt.Frame
import java.util.*
import javax.swing.ImageIcon

/**
 * 1.  create fullGenomeKmerToRefIdMap mutable map:
 * 2.  call  createKmerToRefRangeMap() with the mutable map from above.
 * - kmers created based on PHG graph nodes
 * -  if a kmer is seen in multiple ranges its id is set to -1. If it is seen in more than once in a single range it is not.
 * 3.  Find purgable kmers:
 * - call markKmersForPurgeUsingHammingDistance() to mark kmers for purging
 * - creates a data class with 3 primitive arrays whose indexes correspond to each other: kmerArray, refRangeIDArray, purgeArray;
 * - hamming distance is created twice:  once for first half of the kmer, once for the second half
 * - purgeArray gets 1 at index for the kmer if hamming distance fails
 * 4. purge the kmers using maps from markKmersForPurgeUsingHammingDistance() output:
 * - purge if purgeArray index is set to 1 for this kmer; purge if refRangeId is set to -1 for this kmer
 * - return list of kmers not purged. (the "keep" list)
 * 5. second pass: create refRangeID-to-haplotypesIds map
 * - if a given kmer is in the "keep" list, create map associating the kmer with all the haplotypes
 * that contain it.
 * 6. write the data to tab-delimited file containing kmer and list of hapIds containing that kmer
 */
@Deprecated("No longer used for genotyping use Minimap2 Pipeline instead.")
class IndexKmerByHammingPlugin(parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {

    private val myLogger = Logger.getLogger(IndexKmerByHammingPlugin::class.java)
    private var myKmerMapFile = PluginParameter.Builder("kmerMapFile", null, String::class.java)
            .outFile()
            .required(true)
            .description("Binary file of kmer and haplotype ids")
            .build()
    private var myDuplicateSetFile = PluginParameter.Builder("duplicateSetFile", null, String::class.java)
            .outFile()
            .required(false)
            .description("Binary file of duplicate kmers")
            .build()
    private var kmerSize = PluginParameter.Builder("kmerSize", 32, Int::class.javaObjectType)
            .required(false)
            .description("kmer size for indexing genome. Maximum size is 32. Use the default of 32 unless you know what you are doing.")
            .build()
    private var kmerStepSize = PluginParameter.Builder("kmerStepSize", 1,  Int::class.javaObjectType)
            .required(false)
            .description("kmer step size for the sliding window when indexing genome. Minimum size is 1.  A higher step size will give you less kmers.")
            .build()
    private var indexKmersPrefix = PluginParameter.Builder("kmerPrefix", null, String::class.java)
            .required(false)
            .description("Prefix to keep kmers that start with that prefix.  This is to reduce the number of Kmers by 1/4")
            .build()
    private var revCompliment = PluginParameter.Builder("reverseCompliment", true, Boolean::class.java)
            .required(false)
            .description("Create Kmers on both strands.  If true, this will reverse compliment each haplotype node's sequence and will create kmers for both.")
            .build()
    private var minAllowedHamming = PluginParameter.Builder("minAllowedHamming", 2,  Int::class.javaObjectType)
            .required(false)
            .description("Minimum kmer count required to define a kmer as unique.  Within reference range, this filter criteria is not applied.  " +
                    "Only when comparing kmers across reference ranges is Hamming Distance taken into account.  " +
                    "A higher number here should result in fewer kmers as more will be marked as repetitive.")
            .build()
    private var minKmerCountPerRefRange = PluginParameter.Builder("minKmerCountPerRange", 0,  Int::class.javaObjectType)
            .required(false)
            .description("Minimum kmer counts to be included in processing.  This is to reduce the number of one off kmers")
            .build()
    private var maxKmerCountPerRefRange = PluginParameter.Builder("maxKmerCountPerRange", Int.MAX_VALUE,  Int::class.javaObjectType)
            .required(false)
            .description("Maximum kmer counts to be included in processing.  This is to reduce the number of highly repetative kmers")
            .build()
    private var myKmerDistFile = PluginParameter.Builder("kmerDistExport", null, String::class.java)
            .outFile()
            .required(false)
            .description("Kmer to Distance files.")
            .build()
    private var myPurgeOutputFile = PluginParameter.Builder("purgeArrayExport", null, String::class.java)
            .outFile()
            .required(false)
            .description("Purge Array export after marking file.")
            .build()


    override fun processData(input: DataSet?): DataSet? {

        val temp = input?.getDataOfType(HaplotypeGraph::class.java)
                ?: throw IllegalArgumentException("IndexKmerByHammingPlugin: processData: must input one HaplotypeGraph type not null: ")
        if (temp.size != 1) {
            throw IllegalArgumentException("IndexKmerByHammingPlugin: processData: must input one HaplotypeGraph: " + temp.size)
        }
        memoryReport("before graph building")
        val graph = temp[0].data as HaplotypeGraph
        memoryReport("after graph building")

        //Steps in the algorithm:
        var fullGenomeKmerToRefIdMap = MutableKmerToIdMap(kmerSize = kmerSize(), kmerPrefix = indexKmersPrefix()?.get(0))


        createKmerToRefRangeMap(graph = graph, fullGenomeKmerToRefIdMap = fullGenomeKmerToRefIdMap)

        myLogger.info("Marking Kmers for Purging")
        memoryReport("Before Marking Purge")
        //Find the Purgable kmers
        var purgeableKmers = markKmersForPurgeUsingHammingDistance(kmerToRefRange = fullGenomeKmerToRefIdMap, minAllowedHammingDist = minAllowedHamming(), verboseLogging = false, kmerSize = kmerSize())
        memoryReport("After Marking Purge")
        myLogger.info("Purging kmers.  Size of Map Pre Purge: ${purgeableKmers.kmerArray.size}")
        //Convert the kmerToRefIdToPurge values to a KmerToRefRangeMap only keeping the unique kmers

        //exportPurgeMap
        purgeOutputFile()?.let {
            exportPurgeArray(it, purgeableKmers)
        }

        val kmerMapAfterPurge = purgeKmers(purgeableKmers)
        memoryReport("After purging")
        myLogger.info("Kmers have been purged.  Size Of Map Post Purge: ${kmerMapAfterPurge.kmersAsLongSet().size}")

        //Clear out memory for second pass
        purgeableKmers = KmerToRefRangeIdToPurgeArray(LongArray(0),IntArray(0),ByteArray(0))
        memoryReport("After kmers are purged, asking for GC to be run.")

        //Using this kmer map, do a second pass over the the graph.  If a given kmer is in the to Keep kmer list, associate the kmer with all the haplotypes that contain it.
        val finalKmerMap = indexKmers(graph = graph, kmersToKeep = kmerMapAfterPurge)
        memoryReport("After second Pass")
        myLogger.info("Second Pass of Graph creating haplotypeNode mapping: ${finalKmerMap.keys.size}\n Exporting Map:")
        //Export the kmer to a file.
        exportKmerToHapIdMapToTextFile(fileName = kmerMapFile()+".txt.gz", kmerMap = finalKmerMap)
        exportKmerToHapIdMapToBinaryFile(fileName = kmerMapFile()+".bin", kmerMap = finalKmerMap)

        return null
    }

    /**
     * Method to produce a memory report for logging
     */
    private fun memoryReport(whenString: String) {
        for (i in 0..2) System.gc()
        val myRuntime = Runtime.getRuntime()
        val totalmem = myRuntime.totalMemory()
        val freemem = myRuntime.freeMemory()
        val msg = String.format("$whenString Memory used = %,d out of %,d total", totalmem - freemem, totalmem)
        myLogger.info(msg)
    }

    /**
     * Method which will create a Kmer -> RefRange Map.
     * If the kmer is found in multiple kmers, it will set to -1
     *
     * Steps:
     *     Loop through each reference range
     *         Loop through each node in the reference range
     *             Add the nodes sequence to a KmerCount map specific to the current refRange
     *         Filter out the reference Range based on counts
     *         Add in the kmers found to a Kmer -> RefRange map(The map takes care of marking the repetitive kmers)
     */
    private fun createKmerToRefRangeMap(graph: HaplotypeGraph, fullGenomeKmerToRefIdMap: MutableKmerToIdMap) {
        //Loop through each Reference Range //TODO parallelize this
        val refRangeIdToRangeMap:Map<Int, ReferenceRange> = graph.referenceRanges()
                .map { Pair(it.id(),it) }
                .toMap()
        val filterDistForExportMap = Long2IntOpenHashMap(1000000)
        for ((refRangeCounter, refRange) in graph.referenceRanges().withIndex()) {
            if (refRangeCounter % 1000 == 0) {
                myLogger.debug("RefrangeCounter: $refRangeCounter out of ${graph.referenceRanges().size}")
            }
            //Loop through each HaplotypeNode
            var currentKmerCounter = MutableKmerCounter(kmerSize = kmerSize(), stepSize = kmerStepSize(), kmerPrefix = indexKmersPrefix()?.get(0))
            for (node in graph.nodes(refRange)) {
                val hapSequence = node.haplotypeSequence().sequence()
                //Add each kmer to a KmerCounter Map.  Can use the Immutable one
                currentKmerCounter.addAll(KmerMap(kmerSize = kmerSize(), sequence = hapSequence, stepSize = kmerStepSize(), kmerPrefix = indexKmersPrefix()?.get(0)))
                if (revCompliment()) currentKmerCounter.addAll(KmerMap(kmerSize = kmerSize(), sequence = BaseEncoder.getReverseComplement(hapSequence), stepSize = kmerStepSize(), kmerPrefix = indexKmersPrefix()?.get(0)))
            }
            //Optional filter to filter out kmers based on the count
            val kmerSetToKeep = currentKmerCounter.kmersAsLongSet()
            val kmerSetIterator = kmerSetToKeep.iterator()
            while (kmerSetIterator.hasNext()) {
                val currentKmer = kmerSetIterator.nextLong()
                if (currentKmerCounter[currentKmer] < minKmerCountPerRefRange() ||
                        currentKmerCounter[currentKmer] > maxKmerCountPerRefRange())
                    kmerSetToKeep.remove(currentKmer)
            }

            //Using the left over kmers, get out the set of kmers and add each one to a KmerToRefRangeMap
            val kmerSetToKeepIterator = kmerSetToKeep.iterator()
            while(kmerSetToKeepIterator.hasNext()) {
                val currentKmer = kmerSetToKeepIterator.nextLong()
                val currentStoredRefRangeId = fullGenomeKmerToRefIdMap.get(currentKmer)
                fullGenomeKmerToRefIdMap.put(currentKmer,refRange.id())
                val newStoredRefRangeId = fullGenomeKmerToRefIdMap.get(currentKmer)
                if(currentStoredRefRangeId != -1 && currentStoredRefRangeId != refRange.id()) {
                    //Get the previous ref Range chr and start.
                    val prevRefRange:ReferenceRange? = refRangeIdToRangeMap.get(currentStoredRefRangeId)
                    if(prevRefRange != null) {
                        //If chrom of previous and current output the kmer and a distance of -1
                        if(prevRefRange.chromosome() != refRange.chromosome()) {
                            //export -1
                            filterDistForExportMap[currentKmer] = -1
                        }
                        else {
                            //If chroms are the same, subtract starts and do Math.abs()
                            val distance = Math.abs(prevRefRange.start() - refRange.start())
                            filterDistForExportMap[currentKmer] = distance
                        }
                    }
                }

            }

//            fullGenomeKmerToRefIdMap.putAll(kmerSetToKeep, refRange.id())
        }

        //write out the filterDistForExportMap if name is not null
        kmerDistFile()?.let {
            exportKmerToIntMap(it, filterDistForExportMap)
        }

    }

    /**
     * Function to purge the kmers from the KmerToRefRangeIdToPurgeArray
     *
     * purgableKmers is a data class which holds 3 arrays which share indices.
     *
     * If the refRange is set to -1 it means it was repetitive across different reference ranges
     * If the purgeArray is set to 1, it means it was marked for purging due to Hamming distance
     *
     * Basically if it wasnt marked to be purged, add it to a map.
     */
    private fun purgeKmers(purgableKmers: KmerToRefRangeIdToPurgeArray): KmerMap {
        val kmerMap = MutableKmerToIdMap(kmerSize = kmerSize())

        val kmers = purgableKmers.kmerArray
        val refRangeIds = purgableKmers.refRangeIdArray
        val purgeArray = purgableKmers.purgeArray

        // Create counters so we can tell how we are filtering things
        var bothCounter = 0
        var repetitiveCounter = 0
        var hammingCounter = 0

        //Loop through all the kmers.  If its non-purgable we add it to the map
        for (i in 0 until kmers.size) {
            if (refRangeIds[i] == -1 || purgeArray[i].toInt() == 1) {
                if (refRangeIds[i] == -1 && purgeArray[i].toInt() == 1) {
                    bothCounter++
                } else if (refRangeIds[i] == -1) {
                    repetitiveCounter++
                } else if (purgeArray[i].toInt() == 1) {
                    hammingCounter++
                }
            } else kmerMap.put(kmers[i], refRangeIds[i])
        }

        myLogger.info("Purge Due to Ref Range Repeat: $repetitiveCounter")
        myLogger.info("Purge Due to hamming: $hammingCounter")
        myLogger.info("Purge Due to both: $bothCounter")
        return kmerMap
    }

    /**
     * Function to index the Kmers after the purging process
     *
     * This is the secondary pass over the graph.
     * Loop through each of the ref ranges and extract out the nodes
     *
     * run the extractKmersBit on the sequence which only scores kmers which are in the post purge map
     */
    private fun indexKmers(graph: HaplotypeGraph, kmersToKeep: KmerMap): Long2ObjectMap<IntArray> {
        var finalKmerMapping: Long2ObjectMap<IntArray> = Long2ObjectOpenHashMap()

        var filterByLengthCount = 0
        for (refRange in graph.referenceRanges()) {
            val kmerMappingHaplotypeBitEncoded = Long2LongOpenHashMap()
            val nodeList = graph.nodes(refRange)

            for (nodeIdx in 0 until nodeList.size) {
                val initSize = kmerMappingHaplotypeBitEncoded.size
                extractKmersBit(currentNode = nodeList[nodeIdx], bitCurrentRefRangeKmerMap = kmerMappingHaplotypeBitEncoded,
                        kmersToKeep = kmersToKeep, kmerSize = kmerSize(), haplotypeIndexInRange = nodeIdx, kmerPrefix = indexKmersPrefix()?.get(0))
                if (kmerMappingHaplotypeBitEncoded.size == initSize) {
                    filterByLengthCount++
                }
            }

            kmerMappingHaplotypeBitEncoded.forEach { kmer, haps ->
                val hapIndices = IntArray(java.lang.Long.bitCount(haps))
                var index = 0
                for (i in nodeList.indices) {
                    if (haps.ushr(i) and 1 == 1L) {
                        hapIndices[index] = nodeList[i].id()
                        index++
                    }
                }
//                if(graph.nodes(refRange).size > index) TODO uncomment if we want to filter out any kmers that do not discriminate nodes within a refRange
                finalKmerMapping[kmer] = hapIndices
            }
        }

        myLogger.info("NumberFiltered: $filterByLengthCount")

        return finalKmerMapping
    }


    /**
     * Function to score the sequence and its reverse compliment.
     *
     * Instead of storing the actual node id here, we just set the next available bit
     *
     * We run extractKmersBitForOneSequence twice as we need to do both strands.
     */
    private fun extractKmersBit(currentNode: HaplotypeNode, bitCurrentRefRangeKmerMap: Long2LongOpenHashMap,
                                kmersToKeep: KmerMap, kmerSize: Int, haplotypeIndexInRange: Int, kmerPrefix: Char?) {
        val refRangeLength = currentNode.referenceRange().end() - currentNode.referenceRange().start()
        val sequence = currentNode.haplotypeSequence().sequence()
        val sequenceAsByte = NucleotideAlignmentConstants.convertHaplotypeStringToAlleleByteArray(sequence)

        extractKmersBitForOneSequence(sequenceAsByte = sequenceAsByte, bitCurrentRefRangeKmerMap = bitCurrentRefRangeKmerMap,
                kmersToKeep = kmersToKeep, kmerSize = kmerSize, haplotypeIndexInRange = haplotypeIndexInRange, kmerPrefix = kmerPrefix)

        //Reverse compliment
        val sequenceAsByteRevCompliment = NucleotideAlignmentConstants.reverseComplementAlleleByteArray(sequenceAsByte)
        extractKmersBitForOneSequence(sequenceAsByte = sequenceAsByteRevCompliment, bitCurrentRefRangeKmerMap = bitCurrentRefRangeKmerMap,
                kmersToKeep = kmersToKeep, kmerSize = kmerSize, haplotypeIndexInRange = haplotypeIndexInRange, kmerPrefix = kmerPrefix)

    }

    /**
     * Function that will extract out the kmers for each node's sequence.  This is independent of strand.
     */
    private fun extractKmersBitForOneSequence(sequenceAsByte: ByteArray, bitCurrentRefRangeKmerMap: Long2LongOpenHashMap,
                                              kmersToKeep: KmerMap, kmerSize: Int, haplotypeIndexInRange: Int, kmerPrefix: Char?) {
        for (i in 0 until sequenceAsByte.size - kmerSize) {
            if (kmerPrefix != null && sequenceAsByte[i] != NucleotideAlignmentConstants.getNucleotideAlleleByte(kmerPrefix)) continue
            val seqAsLong = BaseEncoder.getLongSeqFromByteArray(Arrays.copyOfRange(sequenceAsByte, i, i + kmerSize))//Might not need to copy, TODO Test this
            //exclude all kmers which are not in our kmersToKeep map
            if (!kmersToKeep.kmersAsLongSet().contains(seqAsLong)) continue
            val hapPresentLong = 1L shl haplotypeIndexInRange
            val haplotypePresence = bitCurrentRefRangeKmerMap[seqAsLong]
            //Check to see if we have the haplotype in the array
            if (haplotypePresence == bitCurrentRefRangeKmerMap.defaultReturnValue()) {
                bitCurrentRefRangeKmerMap[seqAsLong] = hapPresentLong
            } else {
                bitCurrentRefRangeKmerMap[seqAsLong] = haplotypePresence or hapPresentLong //If we already do, OR it with the currently set bit
            }

        }
    }

    override fun getIcon(): ImageIcon? {
        val imageURL = IndexKmerByHammingPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "Index Kmers"
    }

    override fun getToolTipText(): String {
        return "Create kmer index for haplotype counts"
    }


    /**
     * Binary file of kmer and haplotype ids
     *
     * @return Kmer Map File
     */
    fun kmerMapFile(): String {
        return myKmerMapFile.value()
    }

    /**
     * Set Kmer Map File. Binary file of kmer and haplotype
     * ids
     *
     * @param value Kmer Map File
     *
     * @return this plugin
     */
    fun kmerMapFile(value: String): IndexKmerByHammingPlugin {
        myKmerMapFile = PluginParameter(myKmerMapFile, value)
        return this
    }

    /**
     * Binary file of duplicate kmers
     *
     * @return Duplicate Set File
     */
    fun duplicateSetFile(): String {
        return myDuplicateSetFile.value()
    }

    /**
     * Set Duplicate Set File. Binary file of duplicate kmers
     *
     * @param value Duplicate Set File
     *
     * @return this plugin
     */
    fun duplicateSetFile(value: String): IndexKmerByHammingPlugin {
        myDuplicateSetFile = PluginParameter(myDuplicateSetFile, value)
        return this
    }

    /**
     * kmer size for indexing genome. Maximum size is 32.
     * Use the default of 32 unless you know what you are
     * doing.
     *
     * @return Kmer Size
     */
    fun kmerSize(): Int {
        return kmerSize.value()
    }

    /**
     * Set Kmer Size. kmer size for indexing genome. Maximum
     * size is 32. Use the default of 32 unless you know what
     * you are doing.
     *
     * @param value Kmer Size
     *
     * @return this plugin
     */
    fun kmerSize(value: Int): IndexKmerByHammingPlugin {
        kmerSize = PluginParameter(kmerSize, value)
        return this
    }

    /**
     * Set Kmer Step Size. kmer step size for the sliding
     * window when indexing genome. Minimum size is 1.  A
     * higher step size will give you less kmers.
     *
     * @param value Kmer Step Size
     *
     * @return this plugin
     */
    fun kmerStepSize(value: Int): IndexKmerByHammingPlugin {
        kmerStepSize = PluginParameter(kmerStepSize, value)
        return this
    }

    /**
     * kmer step size for the sliding window when indexing
     * genome. Minimum size is 1.  A higher step size will
     * give you less kmers.
     *
     * @return Kmer Step Size
     */
    fun kmerStepSize(): Int {
        return kmerStepSize.value()
    }

    /**
     * Prefix to keep kmers that start with that prefix.
     * This is to reduce the number of Kmers by 1/4
     *
     * @return Kmer Prefix
     */
    fun indexKmersPrefix(): String? {
        return indexKmersPrefix.value()
    }

    /**
     * Set Kmer Prefix. Prefix to keep kmers that start with
     * that prefix.  This is to reduce the number of Kmers
     * by 1/4
     *
     * @param value Kmer Prefix
     *
     * @return this plugin
     */
    fun indexKmersPrefix(value: String): IndexKmerByHammingPlugin {
        indexKmersPrefix = PluginParameter(indexKmersPrefix, value)
        return this
    }

    /**
     * Create Kmers on both strands.  If true, this will reverse
     * compliment each haplotype node's sequence and will
     * create kmers for both.
     *
     * @return Reverse Compliment
     */
    fun revCompliment(): Boolean {
        return revCompliment.value()
    }

    /**
     * Set Reverse Compliment. Create Kmers on both strands.
     * If true, this will reverse compliment each haplotype
     * node's sequence and will create kmers for both.
     *
     * @param value Reverse Compliment
     *
     * @return this plugin
     */
    fun revCompliment(value: Boolean): IndexKmerByHammingPlugin {
        revCompliment = PluginParameter(revCompliment, value)
        return this
    }

    /**
     * Minimum kmer count required to define a kmer as unique.
     * Within reference range, this filter criteria is not
     * applied.  Only when comparing kmers across reference
     * ranges is Hamming Distance taken into account.  A higher
     * number here should result in fewer kmers as more will
     * be marked as repetitive.
     *
     * @return Min Allowed Hamming
     */
    fun minAllowedHamming(): Int {
        return minAllowedHamming.value()
    }

    /**
     * Set Min Allowed Hamming. Minimum kmer count required
     * to define a kmer as unique.  Within reference range,
     * this filter criteria is not applied.  Only when comparing
     * kmers across reference ranges is Hamming Distance taken
     * into account.  A higher number here should result in
     * fewer kmers as more will be marked as repetitive.
     *
     * @param value Min Allowed Hamming
     *
     * @return this plugin
     */
    fun minAllowedHamming(value: Int): IndexKmerByHammingPlugin {
        minAllowedHamming = PluginParameter(minAllowedHamming, value)
        return this
    }

    /**
     * Minimum kmer counts to be included in processing.
     * This is to reduce the number of one off kmers
     *
     * @return Min Kmer Count Per Range
     */
    fun minKmerCountPerRefRange(): Int {
        return minKmerCountPerRefRange.value()
    }

    /**
     * Set Min Kmer Count Per Range. Minimum kmer counts to
     * be included in processing.  This is to reduce the number
     * of one off kmers
     *
     * @param value Min Kmer Count Per Range
     *
     * @return this plugin
     */
    fun minKmerCountPerRefRange(value: Int): IndexKmerByHammingPlugin {
        minKmerCountPerRefRange = PluginParameter(minKmerCountPerRefRange, value)
        return this
    }

    /**
     * Maximum kmer counts to be included in processing.
     * This is to reduce the number of highly repetative kmers
     *
     * @return Max Kmer Count Per Range
     */
    fun maxKmerCountPerRefRange(): Int {
        return maxKmerCountPerRefRange.value()
    }

    /**
     * Set Max Kmer Count Per Range. Maximum kmer counts to
     * be included in processing.  This is to reduce the number
     * of highly repetative kmers
     *
     * @param value Max Kmer Count Per Range
     *
     * @return this plugin
     */
    fun maxKmerCountPerRefRange(value: Int): IndexKmerByHammingPlugin {
        maxKmerCountPerRefRange = PluginParameter(maxKmerCountPerRefRange, value)
        return this
    }

    /**
     * Kmer to Distance files.
     *
     * @return Kmer Dist Export
     */
    fun kmerDistFile(): String? {
        return myKmerDistFile.value()
    }

    /**
     * Set Kmer Dist Export. Kmer to Distance files.
     *
     * @param value Kmer Dist Export
     *
     * @return this plugin
     */
    fun kmerDistFile(value: String): IndexKmerByHammingPlugin {
        myKmerDistFile = PluginParameter(myKmerDistFile, value)
        return this
    }

    /**
     * Purge Array export after marking file.
     *
     * @return Purge Array Export
     */
    fun purgeOutputFile(): String? {
        return myPurgeOutputFile.value()
    }

    /**
     * Set Purge Array Export. Purge Array export after marking
     * file.
     *
     * @param value Purge Array Export
     *
     * @return this plugin
     */
    fun purgeOutputFile(value: String): IndexKmerByHammingPlugin {
        myPurgeOutputFile = PluginParameter(myPurgeOutputFile, value)
        return this
    }

}

//fun main(args: Array<String>) {
//    GeneratePluginCode.generate(IndexKmerByHammingPlugin::class.java)
//}