package net.maizegenetics.pangenome.hapCalling

import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.pangenome.db_loading.ReadMappingDBRecord
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import java.io.File
import java.nio.file.Files
import javax.swing.ImageIcon

/**
 * This plugin will open up a connection to a DB which multiple sub DBs will need to be merged to.
 *
 * Then it will iterate over the DBs(or files) of readMappings and will load them up to the final DB one at a time.
 * This will effectively merge the Read Mapping data into a single DB for use with PathFinding.
 *
 * If -outputKeyFile is specified, a Path keyfile will be written which will work with Path finding.
 *
 */
class ImportReadMappingToDBPlugin(parentFrame: Frame?, isInteractive: Boolean) : AbstractPlugin(parentFrame, isInteractive) {
    private val myLogger = Logger.getLogger(ImportReadMappingToDBPlugin::class.java)

    private var configFileForFinalDB = PluginParameter.Builder("configFileForFinalDB", null, String::class.java)
            .guiName("Final DB Config File")
            .required(true).inFile()
            .description(
                    "File containing lines with data for host=, user=, password= and DB=, DBtype= used for db connection")
            .build()

    private var loadFromDB = PluginParameter.Builder("loadFromDB",true, Boolean::class.javaObjectType)
            .required(false)
            .description("Load from multiple DBs if this is set to true.  If false, we will treat the files in the directory to be raw flat files")
            .build()

    private var inputDir = PluginParameter.Builder("inputMappingDir", null, String::class.java)
            .required(true)
            .inDir()
            .description("Directory holding the input files.  This can either be DB config files or can be flat read mapping files")
            .build()

    private var readMappingMethod = PluginParameter.Builder("readMappingMethod",null,String::class.java)
            .required(true)
            .description("Read Mapping Method in the DB.  Must be consistent across all DBs/files")
            .build()
    private var outputPathFindingKeyFile = PluginParameter.Builder("outputKeyFile",null, String::class.java)
            .required(false)
            .outFile()
            .description("Output Key File that can be used in path finding. This is optional.  If no file path is supplied, no file will be written.")
            .build()

    private var isTestMethod = PluginParameter.Builder("isTestMethod", false, Boolean::class.javaObjectType)
        .description("Indication if the data is to be loaded against a test method. Data loaded with test methods are not cached with the PHG ktor server")
        .required(false).build()


    override fun processData(input: DataSet?): DataSet? {

        //Load up a connection
        var dbConnect = DBLoadingUtils.connection(configFileForFinalDB(),false)
        val phg = PHGdbAccess(dbConnect)
        myLogger.debug("Opened PHG connection to final DB.")

        if(loadFromDB()) {

            //Loop through the load Files/configs
            File(inputDir()).walk().filter { Files.isRegularFile(it.toPath()) }.forEach {
                myLogger.info("Processing ${it.toString()}")
                var dbConnectFileToLoad = DBLoadingUtils.connection(it.toString(), false)
                val phgFileToLoad = PHGdbAccess(dbConnectFileToLoad)

                //get the readMapping ids for this method name
                val mappings = phgFileToLoad.getReadMappingsForMethod(readMappingMethod())

                phgFileToLoad.close()

                var pluginParams = pluginParameters()
                for (mappingRecord in mappings) {
                    val dbMethodName = mappingRecord.methodName
                    val dbMethodDescription = mappingRecord.methodDetails
                    val readMappingId = mappingRecord.readMappingId
                    val readMappings = mappingRecord.readMappings
                    val lineName = mappingRecord.genoName
                    val fileGroupName = mappingRecord.fileGroupName

                    val methodDescMap = pluginParams.toMutableMap()
                    methodDescMap.put("notes",dbMethodDescription)
                    //Check to see if we already have this taxon and fileGroup name in the DB.
                    //If so, we need to skip this record
                    if (phg.isFileGroupNew(lineName, fileGroupName, readMappingMethod())) {
                        myLogger.info("Writing ${lineName} ${fileGroupName} to DB")
                        phg.putReadMappingData(dbMethodName, methodDescMap, lineName, fileGroupName, readMappings,isTestMethod(),-1)
                    }
                    else {
                        myLogger.warn("$lineName and $fileGroupName are already contained in the DB under method:${readMappingMethod()}.  This taxon+fileGroup will be skipped.")
                    }
                }
            }
        }
        else {
            //Load up specific read Mapping files;
            File(inputDir()).walk().filter { Files.isRegularFile(it.toPath()) }
                    .filter { !it.name.toString().startsWith(".") } //Need to filter out hidden files
                    .forEach {

                var lineName = ""
                var fileGroupName = ""
                var fileMethodName = ""
                var fileMethodDesc = ""
                val hapIdMap = mutableMapOf<List<Int>,Int>()
                //Extract out the metadata denoted by #
                Utils.getBufferedReader(it.toString()).use { reader ->
                    var currentLine = reader.readLine()

                    while(currentLine != null) {
                        if(currentLine.startsWith("#line_name=")) {
                            lineName = currentLine.split("=")[1]
                        }
                        else if(currentLine.startsWith("#file_group_name=")) {
                            fileGroupName = currentLine.split("=")[1]
                        }
                        else if(currentLine.startsWith("#method_name=")) {
                            fileMethodName = currentLine.split("=")[1]
                        }
                        else if(currentLine.startsWith("#method_description=")) {
                            fileMethodDesc = currentLine.split("=")[1]
                        }
                        else if(!currentLine.startsWith("HapIds") && currentLine != "") {
                            val currentLineSplit = currentLine.split("\t")
                            hapIdMap[currentLineSplit[0].split(",").map { hapId -> hapId.toInt() }.toList()] = currentLineSplit[1].toInt()
                        }

                        currentLine = reader.readLine()
                    }
                }

                if(readMappingMethod() != fileMethodName) {
                    myLogger.warn("Unable to process file: ${it.toString()}. Method Name contained in file is not matching the method specified for the plugin")
                }

                //Check to see if file is already in db. if no, keep processing
                if (phg.isFileGroupNew(lineName, fileGroupName,readMappingMethod())) {
                    //Encode the read mappings to byte[]
                    val readMappings = encodeHapIdMapping(hapIdMap)
                    myLogger.info("Writing ${lineName} ${fileGroupName} to DB")

                    var pluginParams = pluginParameters()
                    pluginParams.put("notes",fileMethodDesc)
                    //Push to the db
                    phg.putReadMappingData(fileMethodName, pluginParams, lineName, fileGroupName, readMappings,isTestMethod(),-1)
                }
                else {
                    myLogger.warn("$lineName and $fileGroupName are already contained in the DB under method:${readMappingMethod()}.  This taxon+fileGroup will be skipped.")
                }
            }
        }

        if(outputPathFindingKeyFile() != null) {
            myLogger.info("Writing out a new sample keyfile to be used in Path finding.")
            //Once we are done writing everything to the DB, we should pull down readMappings by line name and create a new Keyfile for Path finding
            val finalMappings = phg.getReadMappingsForMethod(readMappingMethod())
            Utils.getBufferedWriter(outputPathFindingKeyFile()).use { output ->
                output.write("SampleName\tReadMappingIds\tLikelyParents\n")
                val collectedReadMappings = finalMappings.map { Pair(it.genoName, it.readMappingId) }.groupBy({ it.first }, { it.second })
                collectedReadMappings.keys.forEach {
                    output.write("${it}\t${collectedReadMappings[it]?.joinToString(",")}\t\t\n")
                }
            }
        }

        myLogger.debug("Closing out phg Connection.")
        //Close out the connection
        phg.close()
        return null
    }


    override fun getIcon(): ImageIcon? {
        val imageURL = ImportReadMappingToDBPlugin::class.java.getResource("/net/maizegenetics/analysis/images/missing.gif")
        return if (imageURL == null) {
            null
        } else {
            ImageIcon(imageURL)
        }
    }

    override fun getButtonName(): String {
        return "ImportReadMappingToDBPlugin"
    }

    override fun getToolTipText(): String {
        return "ImportReadMappingToDBPlugin to Import Read mappings from multiple databases and upload to a new db."
    }

    /**
     * File containing lines with data for host=, user=, password=
     * and DB=, DBtype= used for db connection
     *
     * @return Final DB Config File
     */
    fun configFileForFinalDB(): String {
        return configFileForFinalDB.value()
    }

    /**
     * Set Final DB Config File. File containing lines with
     * data for host=, user=, password= and DB=, DBtype= used
     * for db connection
     *
     * @param value Final DB Config File
     *
     * @return this plugin
     */
    fun configFileForFinalDB(value: String): ImportReadMappingToDBPlugin {
        configFileForFinalDB = PluginParameter<String>(configFileForFinalDB, value)
        return this
    }

    /**
     * Load from multiple DBs if this is set to true.  If
     * false, we will treat the files in the directory to
     * be raw flat files
     *
     * @return Load From D B
     */
    fun loadFromDB(): Boolean {
        return loadFromDB.value()
    }

    /**
     * Set Load From D B. Load from multiple DBs if this is
     * set to true.  If false, we will treat the files in
     * the directory to be raw flat files
     *
     * @param value Load From D B
     *
     * @return this plugin
     */
    fun loadFromDB(value: Boolean): ImportReadMappingToDBPlugin {
        loadFromDB = PluginParameter<Boolean>(loadFromDB, value)
        return this
    }

    /**
     * Directory holding the input files.  This can either
     * be DB config files or can be flat read mapping files
     *
     * @return Input Mapping Dir
     */
    fun inputDir(): String? {
        return inputDir.value()
    }

    /**
     * Set Input Mapping Dir. Directory holding the input
     * files.  This can either be DB config files or can be
     * flat read mapping files
     *
     * @param value Input Mapping Dir
     *
     * @return this plugin
     */
    fun inputDir(value: String): ImportReadMappingToDBPlugin {
        inputDir = PluginParameter<String>(inputDir, value)
        return this
    }

    /**
     * Read Mapping Method in the DB.  Must be consistent
     * across all DBs/files
     *
     * @return Read Mapping Method
     */
    fun readMappingMethod(): String? {
        return readMappingMethod.value()
    }

    /**
     * Set Read Mapping Method. Read Mapping Method in the
     * DB.  Must be consistent across all DBs/files
     *
     * @param value Read Mapping Method
     *
     * @return this plugin
     */
    fun readMappingMethod(value: String): ImportReadMappingToDBPlugin {
        readMappingMethod = PluginParameter<String>(readMappingMethod, value)
        return this
    }

    /**
     * Output Key File that can be used in path finding. This
     * is optional.  If no file path is supplied, no file
     * will be written.
     *
     * @return Output Key File
     */
    fun outputPathFindingKeyFile(): String? {
        return outputPathFindingKeyFile.value()
    }

    /**
     * Set Output Key File. Output Key File that can be used
     * in path finding. This is optional.  If no file path
     * is supplied, no file will be written.
     *
     * @param value Output Key File
     *
     * @return this plugin
     */
    fun outputPathFindingKeyFile(value: String): ImportReadMappingToDBPlugin {
        outputPathFindingKeyFile = PluginParameter<String>(outputPathFindingKeyFile, value)
        return this
    }

    /**
     * Indication if the data is to be loaded against a test
     * method. Data loaded with test methods are not cached
     * with the PHG ktor server
     *
     * @return Is Test Method
     */
    fun isTestMethod(): Boolean {
        return isTestMethod.value()
    }

    /**
     * Set Is Test Method. Indication if the data is to be
     * loaded against a test method. Data loaded with test
     * methods are not cached with the PHG ktor server
     *
     * @param value Is Test Method
     *
     * @return this plugin
     */
    fun isTestMethod(value: Boolean): ImportReadMappingToDBPlugin {
        isTestMethod = PluginParameter<Boolean>(isTestMethod, value)
        return this
    }


}

fun main() {
    GeneratePluginCode.generateKotlin(ImportReadMappingToDBPlugin::class.java)
}