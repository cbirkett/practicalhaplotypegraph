package net.maizegenetics.pangenome.db_updateDelete

import net.maizegenetics.pangenome.db_loading.DBLoadingUtils
import net.maizegenetics.pangenome.db_loading.PHGdbAccess
import net.maizegenetics.pangenome.hapCalling.readInKeyFile
import net.maizegenetics.plugindef.AbstractPlugin
import net.maizegenetics.plugindef.DataSet
import net.maizegenetics.plugindef.GeneratePluginCode
import net.maizegenetics.plugindef.PluginParameter
import net.maizegenetics.util.Utils
import org.apache.log4j.Logger
import java.awt.Frame
import javax.swing.ImageIcon

/**
 * This method will delete read_mappings for a specified method used with the taxon
 * indicated in the keyfile.
 *
 * The keyfile should be the same format as used in the FastqToMappingPlugin.
 * This allows the user to use the same keyfile if they later detemine there were errors
 * and they need to delete these (or a subset of) these read-mapping entries.
 *
 * The method parameter is optional:  If present, the read_mapping entries will
 * be further filtered based on associated method.
 *
 * This method will delete from the read_mapping, read_mapping_paths and paths table
 * all entries related to the taxon in the keyfile which were loaded with the specified
 * method.
 *
 * Assumptions:  user loaded the ParmeterCache with config file connection values.
 *
 * @author lcj34 feb 2021
 */
class DeleteReadMappingPlugin (parentFrame: Frame? = null, isInteractive: Boolean = false) : AbstractPlugin(parentFrame, isInteractive){
    private val myLogger = Logger.getLogger(DeleteReadMappingPlugin::class.java)

    private var keyFile = PluginParameter.Builder("keyFile", null, String::class.java)
            .guiName("keyFile")
            .inFile()
            .required(false)
            .description("Name of the Keyfile to process.  Must have columns cultivar, flowcell_lane, filename, and PlateID." +
            "Taxa names are created from the cultivar column")
            .build()

    private var methodName = PluginParameter.Builder("methodName", null, String::class.java)
            .guiName("Method Name")
            .required(true)
            .description("Method name assigned to read mappings that will be deleted. " +
                    " If not present, read mappings for all taxa in the keyfile will be deleted.")
            .build()


    private var deleteMethod = PluginParameter.Builder("deleteMethod", false, Boolean::class.javaObjectType)
            .required(false)
            .description("Whether to delete the associated read_mapping method.  default=false.")
            .build()

    override fun processData(input: DataSet?): DataSet? {

        val dbConnect = DBLoadingUtils.connection( false)
        val phg = PHGdbAccess(dbConnect)

        var taxaList = mutableListOf<String>()
        if (keyFile() != null) {
            val colsAndData = readInKeyFile(keyFile()!!)
            val colNameMap = colsAndData.first
            val cultivar = colNameMap["cultivar"]?:-1
            if (cultivar == -1) {
                throw IllegalArgumentException("keyFile is missing cultivar column")
            }
            val keyFileLines = colsAndData.second
            keyFileLines.forEach { lineList ->
                val taxon = lineList[cultivar]
                taxaList.add(taxon)
            }
        }

        myLogger.info("Getting read mappings for ${taxaList.size} taxa")
        val readMappingIds = phg.getReadMappingIdsForTaxaMethod(taxaList, methodName())

        // This deletes from read_mapping, read_mapping_paths and paths tables
        myLogger.info("Calling deleteReadMappinsCascade for ${readMappingIds.size} read_mapping ids")
        val success = phg.deleteReadMappingsCascade( readMappingIds)

        if (deleteMethod() && methodName() != null) {
            myLogger.info("DeleteReadMappingPlugin: calling deleteMethodByName to delete this method")
            phg.deleteMethodByName(methodName())
        }

        phg.close()
        return null
    }

    override fun getIcon(): ImageIcon? {
        return null
    }

    override fun getButtonName(): String {
        return ("Delete Read Mappings")
    }

    override fun getToolTipText(): String {
        return ("Delete Read Mappings based on taxon in keyfile and method name.  Deletions will occur in read_mapping, read_mapping_paths and paths tables.")
    }

    /**
     * Name of the Keyfile to process.  Must have columns
     * cultivar, flowcell_lane, filename, and PlateID.Taxa
     * names are created from the cultivar column
     *
     * @return keyFile
     */
    fun keyFile(): String? {
        return keyFile.value()
    }

    /**
     * Set keyFile. Name of the Keyfile to process.  Must
     * have columns cultivar, flowcell_lane, filename, and
     * PlateID.Taxa names are created from the cultivar column
     *
     * @param value keyFile
     *
     * @return this plugin
     */
    fun keyFile(value: String?): DeleteReadMappingPlugin {
        keyFile = PluginParameter<String>(keyFile, value)
        return this
    }

    /**
     * Method name assigned to read mappings that will be
     * deleted.
     *
     * @return Method Name
     */
    fun methodName(): String {
        return methodName.value()
    }

    /**
     * Set Method Name. Method name assigned to read mappings
     * that will be deleted.
     *
     * @param value Method Name
     *
     * @return this plugin
     */
    fun methodName(value: String): DeleteReadMappingPlugin {
        methodName = PluginParameter<String>(methodName, value)
        return this
    }

    /**
     * Whether to delete the associated read_mapping method.
     *  default=false.
     *
     * @return Delete Read Mapping Method
     */
    fun deleteMethod(): Boolean {
        return deleteMethod.value()
    }

    /**
     * Set Delete Read Mapping Method. Whether to delete the
     * associated read_mapping method.  default=false.
     *
     * @param value Delete Read Mapping Method
     *
     * @return this plugin
     */
    fun deleteMethod(value: Boolean): DeleteReadMappingPlugin {
        deleteMethod = PluginParameter<Boolean>(deleteMethod, value)
        return this
    }

}
fun main(args: Array<String>) {
    GeneratePluginCode.generateKotlin(DeleteReadMappingPlugin::class.java)
}