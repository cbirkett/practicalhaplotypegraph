package net.maizegenetics.pangenome.fastaExtraction;

import com.google.common.collect.*;
import htsjdk.variant.variantcontext.Allele;
import htsjdk.variant.variantcontext.Genotype;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.vcf.VCFFileReader;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.dna.snp.GenotypeTable;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * GVCFGenotypeSequence
 *
 * This class should be used instead of GVCFSequence when you want to have diploid calls being returned.
 *
 * Due to the fact that this class will return diploids, you cannot query a range of positions as we cannot separate the two gametes.
 * If you need a range, you will have to loop through each position and retrieve them one at a time..
 *
 * The Diploids are separated by the / character
 */
public class GVCFGenotypeSequence extends GVCFSequence {

    //Set up instance datastructures
    private static final Logger myLogger = Logger.getLogger(GVCFGenotypeSequence.class);
    private static final Multimap<Position,VariantContext> myPositionToVariantContextMap = ArrayListMultimap.create();

    /**
     * Basic Constructor to create a GVCFSequence
     *
     * @param refFileName  File name of the reference sequence in FASTA format
     * @param gvcfFileName File name of the GVCF file containing the variants
     */
    private GVCFGenotypeSequence(String refFileName, String gvcfFileName, boolean missingAsRef) {
        this(GenomeSequenceBuilder.instance(refFileName), gvcfFileName,missingAsRef);
    }

    /**
     * Basic Constructor to create a GVCFSequence when the GenomeSequence was already created
     *
     * @param genomeSequence GenomeSequence object representing the reference sequence
     * @param gvcfFileName   File name of the GVCF file containing the variants
     */
    //Second constructor to the process faster when processing multiple fastas.  Create the Reference GenomeSequence once.
    private GVCFGenotypeSequence(GenomeSequence genomeSequence, String gvcfFileName, boolean missingAsRef) {
        myLogger.info("GVCFGenotype Sequence loading in genome sequence using a file:");
        myReferenceSequence = genomeSequence;
        chromLengthLookup = setupChromLengthLookup();
        wholeGenomeIndexMap = setupWholeGenomeIndexMap();
        myLogger.info("Loading GVCFFile: " + gvcfFileName);
        myGVCFRangeToAlleleCallMap = this.parseGVCFFileIntoRangeMap(gvcfFileName);
        myPositionToVariantContextMap.putAll(parseVCFIntoMultimap(gvcfFileName));
        myMissingAsRef = missingAsRef;
    }

    /**
     * Constructor to make a GVCFGenotypeSequence from memory
     * @param genomeSequence
     * @param variantContexts
     * @param missingAsRef
     * @param taxonName
     */
    private GVCFGenotypeSequence(GenomeSequence genomeSequence, List<VariantContext> variantContexts, boolean missingAsRef, String taxonName) {
        myLogger.info("GVCFGenotypeSequence loading in genome sequence using List<VariantContexts>:");
        myReferenceSequence = genomeSequence;
        chromLengthLookup = setupChromLengthLookup();
        wholeGenomeIndexMap = setupWholeGenomeIndexMap();
        myLogger.info("Genome Sequence from List<VariantContext>: " );
        myGVCFRangeToAlleleCallMap = this.parseVariantContextsIntoRangeMap(variantContexts, taxonName);
        myPositionToVariantContextMap.putAll(parseVCFIntoMultimap(variantContexts));
        myMissingAsRef = missingAsRef;
    }

    /**
     * Returns an initialized GVCFSequence given the input Reference and GVCF Files.
     *
     * @param refFileName  File name of the Reference file used in creating the GVCF.
     * @param gvcfFileName File name of the GVCF file which defines the variants of the taxon to the reference
     * @return GenomeSequence which can be used to get sequence out
     */
    public static GenomeSequence instance(String refFileName, String gvcfFileName) {
        return new GVCFGenotypeSequence(refFileName, gvcfFileName,false);
    }

    /**
     * Returns an initialized GVCFSequence given the input Reference and GVCF Files.
     * This should be used if you are running multiple GVCFs through using the same Reference File.
     * Currently will speed up processing each GVCF by about a minute.
     *
     * @param referenceSequence GenomeSequence object already read into memory.
     * @param gvcfFileName   File name of the GVCF file which defines the variants of the taxon to the reference
     * @return GenomeSequence which can be used to get sequence out
     */
    public static GenomeSequence instance(GenomeSequence referenceSequence, String gvcfFileName) {
        return new GVCFGenotypeSequence(referenceSequence, gvcfFileName,false);
    }

    /**
     * Instance method to allow for reference filling in missing for a file
     * @param referenceSequence
     * @param gvcfFileName
     * @param missingAsRef
     * @return
     */
    public static GenomeSequence instance(GenomeSequence referenceSequence, String gvcfFileName, boolean missingAsRef) {
        return new GVCFGenotypeSequence(referenceSequence,gvcfFileName,missingAsRef);
    }

    /**
     * Instance method to build the GenomeSequence in memory using a List of VariantContexts
     * @param referenceSequence
     * @param variantContexts
     * @param missingAsRef
     * @param taxonName
     * @return
     */
    public static GenomeSequence instance(GenomeSequence referenceSequence, List<VariantContext> variantContexts, boolean missingAsRef, String taxonName) {
        return new GVCFGenotypeSequence(referenceSequence, variantContexts,missingAsRef,taxonName);

    }


    /**
     * Method to parse the variantContexts into a RangeMap correctly for the genotype case
     * @param variantContexts
     * @param taxonName
     * @return
     */
    @Override
    protected RangeMap<Position,String> parseVariantContextsIntoRangeMap(List<VariantContext> variantContexts, String taxonName) {
        RangeMap<Position, String> gvcfRangeMap = TreeRangeMap.create();
        try {

            //Remove any vcf records where we do not have the taxon
            List<VariantContext> unknownVariantsRemovedList = variantContexts.stream()
                    .filter(variantContext -> variantContext.getGenotype(taxonName)!=null)
                    .collect(Collectors.toList());
            for(VariantContext currentVariant : unknownVariantsRemovedList) {
                //Check to see if currentVariant is a reference block in the VCF file.
                // This is not the same as a ReferenceRange object in the PHG API
                if(isRefRange(currentVariant)) {
                    addRange(gvcfRangeMap, Range.closed(Position.of(currentVariant.getContig(), currentVariant.getStart()),
                            Position.of(currentVariant.getContig(), currentVariant.getEnd())), "REFRANGE");
                }
                //If not, its either SNP, indel or straight monomorphic call
                //Get the allele value from the genotype and add to the map.
                else {
                    Genotype currentGenotype = currentVariant.getGenotype(taxonName);
                    if(currentGenotype.getAlleles().size() == 1) {
                        String callString = currentGenotype.getAlleles().stream().map(allele -> allele.getBaseString()).collect(Collectors.joining(""));
                        addRange(gvcfRangeMap, Range.closed(Position.of(currentVariant.getContig(), currentVariant.getStart()),
                                Position.of(currentVariant.getContig(), currentVariant.getEnd())), callString+"/"+callString);
                    }
                    else {
                        String callString = currentGenotype.getAlleles().stream().map(allele -> allele.getBaseString()).collect(Collectors.joining("/"));
                        addRange(gvcfRangeMap, Range.closed(Position.of(currentVariant.getContig(), currentVariant.getStart()),
                                Position.of(currentVariant.getContig(), currentVariant.getEnd())), callString);
                    }
                }
            }
        } catch (Exception e) {
            myLogger.debug(e.getMessage(), e);
            throw new IllegalStateException("GVCFSequence parseVariantContextsIntoRangeMap error converting List<GenotypeTable> to RangeMap for Taxon:" + taxonName);
        }

        return gvcfRangeMap;
    }

    /**
     * Method to parse the List of variantContexts into a multimap
     * @param variantContexts
     * @return
     */
    private Multimap<Position, VariantContext> parseVCFIntoMultimap(List<VariantContext> variantContexts) {
        Multimap<Position, VariantContext> variantMap = ArrayListMultimap.create();

        for(VariantContext variant : variantContexts) {
            variantMap.putAll(parseSingleVCFIntoMultimap(variant));
        }
        return variantMap;
    }

    /**
     * Method to parse the vcf into a multimap
     * @param gvcfFileName
     * @return
     */
    private Multimap<Position, VariantContext> parseVCFIntoMultimap(String gvcfFileName) {
        try(VCFFileReader reader = new VCFFileReader(new File(gvcfFileName), false)) {

            return reader.iterator().stream()
                    .map(variant -> parseSingleVCFIntoMultimap(variant))
                    .collect(Collector.of(ArrayListMultimap::create,
                            (finalMap, intermediateMap) -> finalMap.putAll(intermediateMap),
                            (finalMap1, finalMap2) -> {
                                finalMap1.putAll(finalMap2);
                                return finalMap1;
                            }));

        }
        catch(Exception e) {
            throw new IllegalStateException("GVCFGenotypeSequence parseVCFIntoMultimap: Unable to open VCF file:", e);
        }

    }

    /**
     * Method that will take a single variant context and have it cover each bp in a multimap.
     * We use this instead of a RangeMap as we can have overlapping records.
     * @param variant
     * @return
     */
    private Multimap<Position,VariantContext> parseSingleVCFIntoMultimap(VariantContext variant) {
        int startPos = variant.getStart();
        int endPos = variant.getEnd();

        return IntStream.range(startPos,endPos+1).mapToObj(pos -> Position.of(variant.getContig(),pos))
                .collect(Collector.of(ArrayListMultimap::create,
                        (map, position) -> map.put(position,variant),
                        (map1, map2) -> {
                            map1.putAll(map2);
                            return map1;
                        }));
    }



    /**
     * addRange method adapted from Louis Wasserman's(author of RangeMap) answer here: https://stackoverflow.com/questions/41418416/intersecting-ranges-when-using-rangemap
     * This method will concatenate the string if the range overlaps an existing range in the map.  Because VCF files should be ordered(in the spec) the concatenation should be ordered correctly
     *
     * @param originalRangeMap rangeMap object which we want to add a new range to
     * @param newRange         Range<Position> object which we need to make sure the range does not collide with any ranges in originalRangeMap
     * @param newCall          String value for the new call associated with newRange
     */
    @Override
    protected void addRange(RangeMap<Position, String> originalRangeMap, Range<Position> newRange, String newCall) {
        //Get the list of overlapping ranges with the new range
        //This will just return the intersecting range and not the full range that intersects.
        //ex if [1,10] is in the map and we are adding [8,12], overlaps will contain [8,10]
        List<Map.Entry<Range<Position>, String>> overlaps = new ArrayList<>(
                originalRangeMap.subRangeMap(newRange).asMapOfRanges().entrySet());
        //if overlaps has length, we have to merge ranges together
        if (overlaps.size() != 0) {
            //because gvcf is ordered, we can just grab the first one and append the call on the end
            //Because overlap just gives intersecting range, we need to get the original range map so we can union
            Map.Entry<Range<Position>, String> overlappingEntry = originalRangeMap.getEntry(overlaps.get(0).getKey().lowerEndpoint());


            //Fixing overlapping bug PHG-65
            //Check to see if the call is a part of a Reference Block in the vcf file
            if(newCall.equals("REFRANGE") && overlappingEntry.getValue().equals("REFRANGE")) {
                //If both are REFRANGEs we should only store a REFRANGE
                //This means they are consecutive and should be handled like originally done.
                //then use the combined range and assign the call
                String newMergedCall = overlappingEntry.getValue();

                //putCoalescing will merge the two records together
                originalRangeMap.putCoalescing(newRange, newMergedCall);
            }
            else if(newCall.equals("REFRANGE") && !overlappingEntry.getValue().equals("REFRANGE")) {
                //Because GVCF is ordered, shift the newCall's starting position up to the overlappingEntry.key().upperEndPoint
                //make sure shifting will not remove the new range(in examples where new range is 1 bp)
                Position endPositionOfOverlap = overlappingEntry.getKey().upperEndpoint();
                if(endPositionOfOverlap.getPosition() < newRange.upperEndpoint().getPosition()) {
                    Position newStartPosition = Position.of(endPositionOfOverlap.getChromosome(), endPositionOfOverlap.getPosition() + 1);
                    originalRangeMap.put(Range.closed(newStartPosition, newRange.upperEndpoint()), newCall);
                }
                //else do nothing as the 1 bp ref range was covered by a previous record(likely multi bp indel)
            }
            else if(!newCall.equals("REFRANGE") && overlappingEntry.getValue().equals("REFRANGE")) {
                //This means the previous record was a reference range that spanned too much
                //This likely does not happen much in practice
                Position endPositionOfOverlap = newRange.lowerEndpoint();
                //Make sure we do not have a 1bp ref range already in the map
                if(endPositionOfOverlap.getPosition() > overlappingEntry.getKey().lowerEndpoint().getPosition()) {
                    Position newEndPosition = Position.of(endPositionOfOverlap.getChromosome(), endPositionOfOverlap.getPosition() - 1);
                    //Remove the current record
                    originalRangeMap.remove(overlappingEntry.getKey());
                    //Add in the refrange with new bounds and the call
                    originalRangeMap.put(Range.closed(overlappingEntry.getKey().lowerEndpoint(), newEndPosition), overlappingEntry.getValue());
                    originalRangeMap.put(newRange,newCall);
                }
                else {
                    //We need to remove the previous entry and add in the new one as it overrides the REF call
                    originalRangeMap.remove(overlappingEntry.getKey());
                    originalRangeMap.put(newRange,newCall);
                }

            }
            else {
                //This means they are consecutive and should be handled like originally done.
                //then use the combined range and assign the call
                //Need to split the overlapping entry and the new Call
                String[] overlappingSplit = overlappingEntry.getValue().split("/");
                String[] newCallSplit = newCall.split("/");
                List<String> calls = new ArrayList<>();
                for(int i = 0; i < overlappingSplit.length && i < newCallSplit.length; i++) {
                    calls.add(overlappingSplit[i] + newCallSplit[i]);
                }

                String newMergedCall = calls.stream().collect(Collectors.joining("/"));

                //Changing the above to use RangeMap.putCoalescing
                //First update the existing Range Key with the new value
                //Second coalesce by the new range
                //The range map will merge the two overlapping ranges and will use newMergedCall as the value
                originalRangeMap.put(overlappingEntry.getKey(), newMergedCall);
                originalRangeMap.putCoalescing(newRange, newMergedCall);
            }
        }
        //else just add the range and value to the map
        else {
            originalRangeMap.put(newRange, newCall);
        }

    }


    /**
     * Unsupported method to get the sequence for the whole chromosome
     * @param chrom
     * @return
     */
    @Override
    public byte[] chromosomeSequence(Chromosome chrom) {
        throw new UnsupportedOperationException("GVCFGenotypeSequence.chromosomeSequence(chrom) is not supported.  GVCFGenotypeSequence cannot be used to call multiple Base Pairs at once:");
    }

    @Override
    public byte[] chromosomeSequence(Chromosome chrom, int startSite, int endSite) {
        throw new UnsupportedOperationException("GVCFGenotypeSequence.chromosomeSequence(chrom, startSite, endSite) is not supported.  GVCFGenotypeSequence cannot be used to call multiple Base Pairs at once:");
    }

    @Override
    public byte[] genomeSequence(long startSite, long lastSite) {
        throw new UnsupportedOperationException("GVCFGenotypeSequence.genomeSequence(startSite, endSite) is not supported.  GVCFGenotypeSequence cannot be used to call multiple Base Pairs at once:");
    }

    @Override
    public String genomeSequenceAsString(long startSite, long lastSite) {
        throw new UnsupportedOperationException("GVCFGenotypeSequence.genomeSequenceAsString(startSite, endSite) is not supported.  GVCFGenotypeSequence cannot be used to call multiple Base Pairs at once:");
    }

    @Override
    public byte genotype(Chromosome chrom, int position) {
        throw new UnsupportedOperationException("GVCFGenotypeSequence.genotype(chrom, position) is not supported.  GVCFGenotypeSequence cannot be used to call byte values:");
    }

    @Override
    public byte genotype(Chromosome chrom, Position positionObject) {
        throw new UnsupportedOperationException("GVCFGenotypeSequence.genotype(chrom, positionObject) is not supported.  GVCFGenotypeSequence  cannot be used to call byte values:");
    }

    @Override
    public String genotypeAsString(Chromosome chrom, int position) {
        return genotypeAsString(chrom, Position.of(chrom.getName(), position));
    }

    @Override
    public String genotypeAsString(Chromosome chrom, Position positionObject) {
        if (!isPositionInMap(positionObject)) {
            if(myMissingAsRef) {
                return myReferenceSequence.genotypeAsString(chrom,positionObject)+"/"+myReferenceSequence.genotypeAsString(chrom,positionObject);
            }
            else {
                return GenotypeTable.UNKNOWN_ALLELE_STR + "/" + GenotypeTable.UNKNOWN_ALLELE_STR;
            }
        }

        //Get the List<VariantContext> from the map.
        List<VariantContext> variants = (List<VariantContext>)myPositionToVariantContextMap.get(positionObject);

        //If the first one is a reference range we want to keep track of it, but we need to ignore any refranges(referenceBlocks) after the first as alts/indels take priority
        VariantContext variantToProcess = variants.get(0);
        for(int i = 1; i < variants.size(); i++) {
            //Make sure the variant is not part of a reference block
            if(!isRefRange(variants.get(i))) {
                variantToProcess = variants.get(i);
            }
        }

        return processVariantContextForGenotype(variantToProcess, positionObject);
    }

    /**
     * Method to extract out the correct genotype string for the position
     * @param variantContext
     * @param pos
     * @return
     */
    private String processVariantContextForGenotype(VariantContext variantContext, Position pos) {
        if(isRefRange(variantContext)) {
            return myReferenceSequence.genotypeAsString(pos.getChromosome(),pos)+"/"+myReferenceSequence.genotypeAsString(pos.getChromosome(),pos);
        }
        else {
            //We need to count how many bps off from the start of the variant context to pos
            int offset = pos.getPosition() - variantContext.getStart();
            int vcLength = variantContext.getEnd() - variantContext.getStart();
            //check the length of the variantContext
            //If the offset is equal to the length, it is an insertion and we should print out the rest of the string
            List<Allele>  genotypeAlleles = variantContext.getGenotype(0).getAlleles();

            if(genotypeAlleles.size()==1) {
                genotypeAlleles.add(genotypeAlleles.get(0));
            }
            return genotypeAlleles
                    .stream()
                    .map(allele -> allele.getBaseString())
                    .map(alleleString -> getSingleGenotype(alleleString,offset, vcLength))
                    .limit(2)
                    .collect(Collectors.joining("/"));
        }
    }


    /**
     * Simple method to check to see if the position is currently in the map.
     *
     * @param position Position object we want to check
     * @return true if Multimap.get(position) != null
     */
    protected boolean isPositionInMap(Position position) {
        return myPositionToVariantContextMap.get(position).size() != 0;
    }
    @Override
    public String genotypeAsString(Chromosome chrom, int startSite, int endSite) {
        throw new UnsupportedOperationException("GVCFGenotypeSequence.genotypeAsString(chrom, startSite, endSite) is not supported.  GVCFGenotypeSequence  cannot be used to call multiple Base Pairs at once:");
    }


    /**
     * Simple method to figure out if a gap character needs to be added or not
     * @param alleleString
     * @param offset
     * @param vcLength
     * @return
     */
    private String getSingleGenotype(String alleleString, int offset, int vcLength) {
        if(offset >= vcLength) {
            return alleleString.substring(offset);
        }
        else if(offset>=alleleString.length()) {
            return "-";
        }
        else {
            return ""+alleleString.charAt(offset);
        }

    }

}
