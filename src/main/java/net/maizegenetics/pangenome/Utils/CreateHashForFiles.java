/**
 * 
 */
package net.maizegenetics.pangenome.Utils;


import java.security.MessageDigest;
import java.io.File;
import java.io.FileInputStream;

/**
 * This class creates the MD4 or SHA-1 hash for a specified file.
 * 
 * input:
 *   1.  Path to file for which the hash should be created.
 *   2.  digest Type:  either MD5 or SHA-1
 *   
 * output:
 *   1.  The hash value as hex created by the digest.
 *   
 * @author lcj34
 *
 */
public class CreateHashForFiles {
    
    public static String processCreateHash(String inputFile, String digestType) {
        // digestTYpe is expected to be with MD5 or SHA-1
        try {
            MessageDigest md = MessageDigest.getInstance(digestType);
  
            FileInputStream fis = new FileInputStream(new File(inputFile));
            byte[] byteArray = new byte[1024];
            int bytesCount = 0;
            
          //Read file data and update in message digest
            while ((bytesCount = fis.read(byteArray)) != -1) {
                md.update(byteArray, 0, bytesCount);
            };
             
            //close the stream; We don't need it now.
            fis.close();
             
            byte[] byteData = md.digest();
            // convert the byte to hex format
            StringBuffer sb = new StringBuffer();
            for (int idx = 0; idx < byteData.length; idx++) {
                sb.append(Integer.toString((byteData[idx] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
  
        return null; // errored out
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
       //String inputFile = "/Volumes/Samsung_T1/wgs_pipeline/refGenomeFiles/Zea_mays.AGPv4.dna.toplevel.fa";
       // String inputFile = "/Volumes/Samsung_T1/wgs_pipeline/refGenomeFiles/Zm-B104-DRAFT-ISU_USDA-0.2.fa";
       // String inputFile = "/Volumes/Samsung_T1/wgs_pipeline/refGenomeFiles/CML247_3.genome.fasta";
        //String inputFile = "/Volumes/Samsung_T1/wgs_pipeline/refGenomeFiles/GCA_001984235.2_Zm-EP1-REFERENCE-TUM-1.0_genomic_fixedIDLine.fna";
        //String inputFile = "/Volumes/Samsung_T1/wgs_pipeline/refGenomeFiles/W22__Ver12.genome.fasta";
        String inputFile = "/Volumes/Samsung_T1/wgs_pipeline/refGenomeFiles/Zm-PH207-REFERENCE_NS-UIUC_UMN-1.0.fasta";
        String digestType = "SHA-1";
        
//        if (args.length != 2) {
//            System.out.println(" Expecting 2 input arguments in this order: " );            
//            System.out.println("     Full path name to inputFile to be hashed,");
//            System.out.println("     Digest type, either MD5 or SHA-1");    
//            System.out.println(" Please fix the arguments and try again - aborting the run");
//            return;
//        }
 
//        inputFile = args[0];
//        digestType = args[1];

        long time = System.nanoTime();
        System.out.println("Begin createHashForFiles");
       // String hashAsHex = processCreateHash(inputFile,digestType);
        String hashAsHex = processCreateHash(inputFile,digestType);
        System.out.println("\nFinished " + inputFile + "- took " + (System.nanoTime()-time)/1e9 + " seconds");
        System.out.println(digestType + " hash as hex for " + inputFile + " : " + hashAsHex);
        
        time = System.nanoTime();
        digestType = "MD5";
        System.out.println("Begin createHashForFiles");
       // String hashAsHex = processCreateHash(inputFile,digestType);
         hashAsHex = processCreateHash(inputFile,digestType);
        System.out.println("\nFinished " + inputFile + "- took " + (System.nanoTime()-time)/1e9 + " seconds");
        System.out.println(digestType + " hash as hex for " + inputFile + " : " + hashAsHex);
        
        
    }
}
