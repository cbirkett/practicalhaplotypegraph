package net.maizegenetics.pangenome.gvcfFiltering;

import com.google.common.collect.*;
import com.google.common.math.Stats;
import net.maizegenetics.analysis.phg.ParseGVCF;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GeneralPosition;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.util.Tuple;
import net.maizegenetics.util.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Simple Utility to Extract Mean, Median and Mode for Depth in a GVCF file
 * Currently supports only DP as defined in the FORMAT Tag.
 * If the value is missing, it will not count anything.
 * For a reference block, it will count the depth for each base pair in the block.
 * For a variant, it will count the depth once.
 * Created by zrm22 on 6/30/17.
 */

public class ComputeMedianAnnotation {

    /**
     * Simple method to compute the mean of the depths.
     *
     * @param gvcfFile Full file path to the gvcf file
     * @return The mean of the depths in gvcfFile
     */
    public static double getMeanDepth(String gvcfFile) {
        //Get the depth counts from the gvcf file
        Multiset<Integer> depthSet = processGVCFFileForDepth(gvcfFile);
        return Stats.meanOf(depthSet);
    }

    public static double getMeanDepth(String gvcfFile, String intervalFile) {
        //Get the depth counts from the gvcf file
        Multiset<Integer> depthSet = processGVCFFileForDepth(gvcfFile,intervalFile);
        return Stats.meanOf(depthSet);
    }

    /**
     * Simple method to get the mode of the depth.
     *
     * @param gvcfFile Full file path to the gvcf file
     * @return The mode of the depths in gvcfFile
     */
    public static int getModeDepth(String gvcfFile) {
        //Get the depth counts from the gvcf file
        Multiset<Integer> depthSet = processGVCFFileForDepth(gvcfFile);
        int maxCount = 0;
        int mode = 0;
        //Loop through all the depths and find the one that has the most counts
        for (Integer depthNumber : depthSet.elementSet()) {
            if (depthSet.count(depthNumber) > maxCount) {
                maxCount = depthSet.count(depthNumber);
                mode = depthNumber;
            }
        }
        return mode;
    }

    public static int getModeDepth(String gvcfFile,String intervalFile) {
        //Get the depth counts from the gvcf file
        Multiset<Integer> depthSet = processGVCFFileForDepth(gvcfFile,intervalFile);
        int maxCount = 0;
        int mode = 0;
        //Loop through all the depths and find the one that has the most counts
        for (Integer depthNumber : depthSet.elementSet()) {
            if (depthSet.count(depthNumber) > maxCount) {
                maxCount = depthSet.count(depthNumber);
                mode = depthNumber;
            }
        }
        return mode;
    }

    /**
     * Simple method to calculate the Median depth for a GVCF file.
     *
     * @param gvcfFile Full file path to the gvcf file
     * @return The median depth in gcvfFile
     */
    public static int getMedianDepth(String gvcfFile) {
        //Get the depth counts from the gvcf file
        Multiset<Integer> depthSet = processGVCFFileForDepth(gvcfFile);
        //Compute the median https://stackoverflow.com/questions/3052924/how-to-calculate-median-of-a-mapint-int
        return Iterables.get(depthSet, (depthSet.size() - 1) / 2);
    }

    public static int getMedianDepth(String gvcfFile,String intervalFile) {
        //Get the depth counts from the gvcf file
        Multiset<Integer> depthSet = processGVCFFileForDepth(gvcfFile,intervalFile);
        //Compute the median https://stackoverflow.com/questions/3052924/how-to-calculate-median-of-a-mapint-int
        return Iterables.get(depthSet, (depthSet.size() - 1) / 2);
    }

    /**
     * Method to read each line of the gvcf file and put the depths into a Multiset
     *
     * @param gvcfFile Full file path to the gvcf file
     * @return A multiset containing the counts for each depth
     */
    private static Multiset<Integer> processGVCFFileForDepth(String gvcfFile) {

        Multiset<Integer> depthMultiset = TreeMultiset.create();
        try {
//            int numThreads = Runtime.getRuntime().availableProcessors();
           // ExecutorService pool = Executors.newFixedThreadPool(numThreads);
           // Tuple<List<String>, BlockingQueue<Future<ParseGVCF.ProcessLines>>> temp = ParseGVCF.parse(gvcfFile,pool);
            Tuple<List<String>, BlockingQueue<Future<ParseGVCF.ProcessLines>>> temp = ParseGVCF.parse(gvcfFile);

            List<String> headers = temp.getX();
            BlockingQueue<Future<ParseGVCF.ProcessLines>> queue = temp.getY();
            ParseGVCF.ProcessLines next = queue.take().get();
            while (!next.isFinal()) {
                for (ParseGVCF.GVCFLine current : next.processedLines()) {
                    //pass current to the getDepth method
                    //get the Tuple<Depth, NumberOfBpsCovered)
                    Tuple<Range<Position>,Tuple<Integer, Integer>> depthAndNumOccurrences = getDepthAndOccurrencesForGVCFLine(current);

                    //Add the depth to the Multiset
                    depthMultiset.add(depthAndNumOccurrences.getY().getX(), depthAndNumOccurrences.getY().getY());

                }
                next = queue.take().get();
            }
           // pool.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return depthMultiset;
    }

    /**
     * Method to parse the gvcf file and put the depths into a multiset
     * @param gvcfFile input gvcf file
     * @param intervalFile input interval file
     * @return turn multiset of all the depth counts
     */
    private static Multiset<Integer> processGVCFFileForDepth(String gvcfFile, String intervalFile) {
        Multiset<Integer> depthMultiset = TreeMultiset.create();
        int[][] alleleDepths=new int[100][100];
        try (BufferedReader intervalFileReader = Utils.getBufferedReader(intervalFile)) {
            //Load in the interval file
            RangeSet<Position> intervalRanges = TreeRangeSet.create();
            String currentIntervalLine = "";
            while((currentIntervalLine = intervalFileReader.readLine()) != null) {
                Range<Position> parsedIntervalRange = parseIntervalLine(currentIntervalLine);
                intervalRanges.add(parsedIntervalRange);
            }

            //int numThreads = Runtime.getRuntime().availableProcessors();
            //ExecutorService pool = Executors.newFixedThreadPool(numThreads);

//            Tuple<List<String>, BlockingQueue<Future<ParseGVCF.ProcessLines>>> temp = ParseGVCF.parse(gvcfFile,pool);
            Tuple<List<String>, BlockingQueue<Future<ParseGVCF.ProcessLines>>> temp = ParseGVCF.parse(gvcfFile);
            List<String> headers = temp.getX();
            BlockingQueue<Future<ParseGVCF.ProcessLines>> queue = temp.getY();
            ParseGVCF.ProcessLines next = queue.take().get();
            while (!next.isFinal()) {
                for (ParseGVCF.GVCFLine current : next.processedLines()) {
                    //pass current to the getDepth method
                    //get the Tuple<Depth, NumberOfBpsCovered)
                    Tuple<Range<Position>,Tuple<Integer, Integer>> depthAndNumOccurrences = getDepthAndOccurrencesForGVCFLine(current);

                    if(!intervalRanges.subRangeSet(depthAndNumOccurrences.getX()).isEmpty()) {
                        //Add the depth to the Multiset
                        depthMultiset.add(depthAndNumOccurrences.getY().getX(), depthAndNumOccurrences.getY().getY());
                    }
                    if(current.alleleDepths().size()==2) {
                        alleleDepths[Math.min(alleleDepths.length-1,current.alleleDepths().get(0))]
                                [Math.min(alleleDepths.length-1,current.alleleDepths().get(1))]++;
                    }
                }
                next = queue.take().get();
            }
//            pool.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch(Exception generalException) {
            generalException.printStackTrace();
        }
        return depthMultiset;
    }


    /**
     * Method to get the depth and count the number of basePairs this gvcfRecord will cover.
     * Will be replaced by a method in the GVCF Parser.
     *
     * Currently counts depth for each base pair if gvcfLine represents a reference block
     * If the current gvcfLine is a variant only count its depth once
     *
     * @param gvcfLine Single line read from the gvcf parser
     * @return a Tuple containing the depth and the number of base pairs covered by the gvcfLine
     */
    private static Tuple<Range<Position>,Tuple<Integer, Integer>> getDepthAndOccurrencesForGVCFLine(String gvcfLine) {
        String[] elements = gvcfLine.split("\t");
        Chromosome chr = new Chromosome(elements[0]);
        int startPosition = Integer.parseInt(elements[1]);

        Position startPos = new GeneralPosition.Builder(chr,startPosition).build();

        //Get the format tag
        //TODO fix the DP parsing, perhaps pre-parse the file and pass in the indices
        List<String> formatList = Arrays.asList(elements[8].split(":"));
        Integer annoIndex = formatList.indexOf("DP");
        String[] valueList = elements[9].split(":");
        //Now that we know where each index is, we can grab the correct column in the call
        if (annoIndex == -1 || annoIndex >= valueList.length) {
            //skip
        } else {
            //check to see how many bps this line covers
            if (elements[7].startsWith("END=")) {  //a range element
                //Get the actual end position and subtract the start to figure out how many total positions have the depth
                Integer endPosition = Integer.parseInt(elements[7].split("=")[1]);
                Integer numberOfPositions = endPosition - startPosition + 1; //Add one as the gvcf range is inclusive-inclusive
                Position endPos = new GeneralPosition.Builder(chr,endPosition).build();

                return new Tuple(Range.closed(startPos,endPos),new Tuple(Integer.parseInt(valueList[annoIndex]), numberOfPositions));


//                return new Tuple(Integer.parseInt(valueList[annoIndex]), numberOfPositions);
            } else {
                //Its a snp return only one count of the depth
                return new Tuple(Range.closed(startPos,startPos),new Tuple(Integer.parseInt(valueList[annoIndex]), 1));
//                return new Tuple(Integer.parseInt(valueList[annoIndex]), 1);
            }
        }
        return new Tuple(Range.closed(startPos,startPos),new Tuple(0, 0));
//        return new Tuple(0, 0);
    }

    private static Tuple<Range<Position>,Tuple<Integer,Integer>> getDepthAndOccurrencesForGVCFLine(ParseGVCF.GVCFLine gvcfLine) {
        String chr = gvcfLine.chromosome();

        Position startPos = Position.of(chr,gvcfLine.startPosition());
        Position endPos = Position.of(chr,gvcfLine.endPosition());

        if(startPos.equals(endPos)) {
            //its a SNP, return only once count of the depth
            return new Tuple(Range.closed(startPos,startPos),new Tuple(gvcfLine.depth(), 1));
        }
        else {
            int numberOfPositions = endPos.getPosition() - startPos.getPosition() + 1;
            return new Tuple(Range.closed(startPos,endPos),new Tuple(gvcfLine.depth(), numberOfPositions));
        }
    }

    private static Range<Position> parseIntervalLine(String intervalLine) {
        String[] intervalLineSplit = intervalLine.split(":");
        Chromosome chr = new Chromosome(intervalLineSplit[0]);

        String[] positionsSplit = intervalLineSplit[1].split("-");
        Position startPos = new GeneralPosition.Builder(chr,Integer.parseInt(positionsSplit[0])).build();
        Position endPos = new GeneralPosition.Builder(chr,Integer.parseInt(positionsSplit[1])).build();

        return Range.closed(startPos,endPos);
    }

}
