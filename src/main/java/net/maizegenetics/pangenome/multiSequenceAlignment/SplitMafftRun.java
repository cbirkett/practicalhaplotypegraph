package net.maizegenetics.pangenome.multiSequenceAlignment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * Simple utility to create a multithreaded script to run multiple anchors through MAFFT at a single time.
 * This helps when running on cbsu as we can then make use of all the cores.
 * Creates a single master script and a set of subscripts which will all run on a single thread.
 * TODO This should be refractored to be able to run using CMD line calls from java
 * Created by zrm22 on 6/7/17.
 */
public class SplitMafftRun {
    public static void main(String args[]) {
        SplitMafftRun app = new SplitMafftRun();
        app.run(args[0],args[1],args[2]);
    }

    public void run(String listOfFiles, String numberOfScripts, String outputDirectory) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(listOfFiles));
            String currLine = "";
            ArrayList<String> fileList = new ArrayList<>();
            while((currLine = reader.readLine())!=null) {
                fileList.add(currLine);
            }

            reader.close();

            ArrayList<BufferedWriter> writers = new ArrayList<>();
            int numberOfScriptsInt = Integer.parseInt(numberOfScripts);
            for(int i = 0; i < numberOfScriptsInt; i++) {
                writers.add(new BufferedWriter(new FileWriter("./Scripts/IndividualScripts/runMAFFTScript_"+i+".sh")));
                System.out.println("./Scripts/IndividualScripts/runMAFFTScript_"+i+".sh > ./logs/log_"+i+" &");
            }

            int writerCount = 0;
            for(int i = 0; i < fileList.size(); i++) {
                BufferedWriter writer = writers.get(writerCount);

                writerCount++;
                if(writerCount>=writers.size()) {
                    writerCount = 0;
                }

                String currentLine = fileList.get(i);
                String[] currLineSplit = currentLine.split("/");
                String[] fileNameSplit = currLineSplit[currLineSplit.length-1].split("\\.");
                StringBuilder outputFileBuilder = new StringBuilder();
                outputFileBuilder.append(outputDirectory);
                outputFileBuilder.append("FastaOutputs/");
                outputFileBuilder.append(fileNameSplit[0]);
                outputFileBuilder.append("_MAFFTAligned.fa");

                writer.write("/programs/mafft/bin/mafft "+fileList.get(i)+ " > " + outputFileBuilder.toString());
                writer.newLine();
            }




            for(int i = 0; i < numberOfScriptsInt; i++) {
                writers.get(i).write(" > "+outputDirectory+"/MAFFT_DONE/DONE_"+i);
                writers.get(i).close();
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
