/*
 *  ConvertToHaplotypeCallerInput
 * 
 *  Created on Mar 3, 2017
 */
package net.maizegenetics.pangenome;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.math.BigDecimal;

import net.maizegenetics.util.Utils;

/**
 * @author Terry Casstevens
 */
public class ConvertToHaplotypeCallerInput {

    public static void convertCSVToBed(String[] args) {

        String input = args[0];
        if (!input.endsWith(".csv")) {
            throw new IllegalArgumentException("Expecting .csv input: " + input);
        }
        System.out.println("Input file: " + input);

        String output = Utils.getDirectory(input) + "/" + Utils.getFilename(input, ".csv") + ".bed";
        System.out.println("Output File: " + output);

        String line = null;
        try (BufferedReader reader = Utils.getBufferedReader(input);
             BufferedWriter writer = Utils.getBufferedWriter(output)) {

            line = reader.readLine();
            if (line.startsWith("Chr")) {
                line = reader.readLine();
            }
            while (line != null) {
                String[] tokens = line.split(",");
                if (tokens.length != 3) {
                    throw new IllegalArgumentException("Should be 3 fields per line");
                }
                writer.append(tokens[0]);
                writer.append("\t");
                int start = new BigDecimal(tokens[1]).intValueExact() - 1;
                writer.append(String.valueOf(start));
                writer.append("\t");
                int end = new BigDecimal(tokens[2]).intValueExact();
                writer.append(String.valueOf(end));
                writer.append("\n");
                line = reader.readLine();
            }

        } catch (Exception e) {
            System.out.println("current line: " + line);
            e.printStackTrace();
        }

    }

    public static void convertCSVToIntervals(String[] args) {

        String input = args[0];
        if (!input.endsWith(".csv")) {
            throw new IllegalArgumentException("Expecting .csv input: " + input);
        }
        System.out.println("Input file: " + input);

        String output = Utils.getDirectory(input) + "/" + Utils.getFilename(input, ".csv") + ".intervals";
        System.out.println("Output File: " + output);

        String line = null;
        try (BufferedReader reader = Utils.getBufferedReader(input);
             BufferedWriter writer = Utils.getBufferedWriter(output)) {

            line = reader.readLine();
            if (line.startsWith("Chr")) {
                line = reader.readLine();
            }
            while (line != null) {
                String[] tokens = line.split(",");
                if (tokens.length != 3) {
                    throw new IllegalArgumentException("Should be 3 fields per line");
                }
                writer.append(tokens[0]);
                writer.append(":");
                int start = new BigDecimal(tokens[1]).intValueExact();
                writer.append(String.valueOf(start));
                writer.append("-");
                int end = new BigDecimal(tokens[2]).intValueExact();
                writer.append(String.valueOf(end));
                writer.append("\n");
                line = reader.readLine();
            }

        } catch (Exception e) {
            System.out.println("current line: " + line);
            e.printStackTrace();
        }

    }
}
