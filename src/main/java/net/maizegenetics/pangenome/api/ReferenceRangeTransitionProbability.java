package net.maizegenetics.pangenome.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import net.maizegenetics.analysis.imputation.TransitionProbability;

public class ReferenceRangeTransitionProbability extends TransitionProbability {
	private int currentAnchor;
	private final ArrayList<List<HaplotypeNode>> myAnchorNodeList;
	private final HaplotypeGraph myGraph;
	double minProb = .001;
	
	public ReferenceRangeTransitionProbability(ArrayList<List<HaplotypeNode>> anchorNodeList, HaplotypeGraph hapGraph, double minTransitionProb) {
		currentAnchor = 0;
		myAnchorNodeList = anchorNodeList;
		myGraph = hapGraph;
		minProb = minTransitionProb;
	}
	
	@Override
	public void setNode(int anchor) {
		currentAnchor = anchor;
	}

	@Override
	public double getTransitionProbability(int state1, int state2) {
		//get the edge for this transition and return the probability
		//if edge is null return minProb
		//if edge probability is less than minProb return minProb
		HaplotypeNode node1 = myAnchorNodeList.get(currentAnchor - 1).get(state1);
		HaplotypeNode node2 = myAnchorNodeList.get(currentAnchor).get(state2);
		Optional<HaplotypeEdge> thisEdge = myGraph.edge(node1, node2);
		
		if (thisEdge.isPresent()) return Math.max(thisEdge.get().edgeProbability(), minProb);
		return minProb;
		
	}

	@Override
	public int getNumberOfStates() {
		return myAnchorNodeList.get(currentAnchor).size();
	}
	
}
