package net.maizegenetics.pangenome.api;

import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.plugindef.PluginParameter;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * @author Terry Casstevens Created November 14, 2017
 */
public class WriteFastaFromGraphPlugin extends AbstractPlugin {

    private static final Logger myLogger = Logger.getLogger(WriteFastaFromGraphPlugin.class);

    private PluginParameter<String> myOutputFile = new PluginParameter.Builder<>("outputFile", null, String.class)
            .description("Output filename")
            .outFile()
            .required(true)
            .build();

    public WriteFastaFromGraphPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    public DataSet processData(DataSet input) {

        List<Datum> graphs = input.getDataOfType(HaplotypeGraph.class);
        if (graphs.size() != 1) {
            throw new IllegalArgumentException("FilterGraphPlugin: processData: input one HaplotypeGraph.");
        }
        HaplotypeGraph graph = (HaplotypeGraph) graphs.get(0).getData();

        String filename = GraphIO.writeFasta(graph, outputFile());

        return null;

    }

    @Override
    public String pluginDescription() {
        return "This plugin exports the given haplotype graph as a FASTA formatted file. It will bwa index the file if possible.";
    }

    /**
     * Output filename
     *
     * @return Output File
     */
    public String outputFile() {
        return myOutputFile.value();
    }

    /**
     * Set Output File. Output filename
     *
     * @param value Output File
     *
     * @return this plugin
     */
    public WriteFastaFromGraphPlugin outputFile(String value) {
        myOutputFile = new PluginParameter<>(myOutputFile, value);
        return this;
    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {
        return "Write Fasta from Graph";
    }

    @Override
    public String getToolTipText() {
        return "Write Fasta from Graph";
    }
}
