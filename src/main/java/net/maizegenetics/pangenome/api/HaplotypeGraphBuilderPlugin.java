package net.maizegenetics.pangenome.api;

import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.util.Tuple;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.NavigableMap;
import java.util.SortedSet;

/**
 * @author Terry Casstevens Created August 03, 2017
 */
public class HaplotypeGraphBuilderPlugin extends AbstractPlugin {

    private static final Logger myLogger = Logger.getLogger(HaplotypeGraphBuilderPlugin.class);

    private PluginParameter<String> configFile = new PluginParameter.Builder<>("configFile", null, String.class)
            .description("Database configuration file")
            .guiName("Config File")
            .required(true)
            .inFile()
            .build();

    private PluginParameter<String> myMethods = new PluginParameter.Builder<>("methods", null, String.class)
            .description("Pairs of methods (haplotype method name and range group method name). Method pair separated by a comma, and pairs separated by colon. The range group is optional \n" +
                    "Usage: <haplotype method name1>,<range group name1>:<haplotype method name2>,<range group name2>:<haplotype method name3>")
            .build();

    private PluginParameter<Boolean> myIncludeSequences = new PluginParameter.Builder<>("includeSequences", true, Boolean.class)
            .description("Whether to include sequences in haplotype nodes.")
            .build();

    private PluginParameter<Boolean> myIncludeVariantContexts = new PluginParameter.Builder<>("includeVariantContexts", false, Boolean.class)
            .description("Whether to include variant contexts in haplotype nodes.")
            .build();

    private PluginParameter<SortedSet<Integer>> myHapids = new PluginParameter.Builder("haplotypeIds", null, SortedSet.class)
            .description("List of haplotype ids to include in the graph. If not specified, all ids are included.")
            .build();

    private PluginParameter<List<String>> myChromosomes = new PluginParameter.Builder("chromosomes", null, List.class)
            .description("List of chromosomes to include in graph.  Default is to include all chromosomes.  (i.e. -chromosomes 1,3)")
            .build();

    private PluginParameter<TaxaList> myTaxaList = new PluginParameter.Builder<>("taxa", null, TaxaList.class)
            .description("Optional list of taxa to include. This can be a comma separated list of taxa (no spaces unless surrounded by quotes), file (.txt) with list of taxa names to include, or a taxa list file (.json or .json.gz). By default, all taxa will be included.")
            .build();

    public HaplotypeGraphBuilderPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    protected void postProcessParameters() {
        if (hapids() != null && !hapids().isEmpty() && taxaList() != null && !taxaList().isEmpty()) {
            throw new IllegalStateException("HaplotypeGraphBuilderPlugin: postProcessParameters: can't specify both hapids and taxa");
        }
        if (methods() == null && (hapids() == null || hapids().isEmpty())) {
            throw new IllegalArgumentException("HaplotypeGraphBuilderPlugin: postProcessParameters: either methods or haplotypeIds must be specified.");
        }
    }

    @Override
    public DataSet processData(DataSet input) {
        try (Connection connection = DBLoadingUtils.connection(configFile(), false)) {
            NavigableMap<ReferenceRange, List<HaplotypeNode>> nodes = CreateGraphUtils.createHaplotypeNodes(connection, convertMethods(methods()), includeSequences(), includeVariantContexts(), hapids(), chromosomes(), taxaList());
            HaplotypeGraph graph = new HaplotypeGraph(nodes);
            return new DataSet(new Datum("PHG", graph, "methods: " + methods()), this);
        } catch (Exception e) {
            myLogger.debug(e.getMessage(), e);
            throw new IllegalStateException("processData: Problem creating graph: " + e.getMessage());
        }
    }

    public HaplotypeGraph build() {
        return (HaplotypeGraph) processData(null).getData(0).getData();
    }

    public static List<Tuple<String, String>> convertMethods(String methodPairs) {

        if (methodPairs == null) return null;

        String[] pairs = methodPairs.split(":");
        List<Tuple<String, String>> result = new ArrayList<>();
        for (String current : pairs) {
            String[] methods = current.split(",");
            if (methods.length == 1) {
                result.add(new Tuple<>(methods[0].trim(), null));
            } else if (methods.length == 2) {
                if (methods[1].trim().equals("*")) {
                    result.add(new Tuple<>(methods[0].trim(), null));
                } else {
                    result.add(new Tuple<>(methods[0].trim(), methods[1].trim()));
                }
            }
        }
        return result;

    }

    /**
     * Database configuration file
     *
     * @return Database Config File
     */
    public String configFile() {
        return configFile.value();
    }

    /**
     * Set Database Config File. Database configuration file
     *
     * @param value Database Config File
     *
     * @return this plugin
     */
    public HaplotypeGraphBuilderPlugin configFile(String value) {
        configFile = new PluginParameter<>(configFile, value);
        return this;
    }

    /**
     * Pairs of methods (haplotype method name and range group method name). Method pair separated by a comma, and pairs
     * separated by semicolon. The range group is optional
     *
     * Usage: <haplotype method name1>,<range group name1>;<haplotype method name2>,<range group name2>;<haplotype
     * method name3>
     *
     * @return Methods
     */
    public String methods() {
        return myMethods.value();
    }

    /**
     * Set Methods. Pairs of methods (haplotype method name and range group method name). Method pair separated by a
     * comma, and pairs separated by semicolon. The range group is optional Usage: <haplotype method name1>,<range group
     * name1>;<haplotype method name2>,<range group name2>;<haplotype method name3>
     *
     * @param value Methods
     *
     * @return this plugin
     */
    public HaplotypeGraphBuilderPlugin methods(String value) {
        myMethods = new PluginParameter<>(myMethods, value);
        return this;
    }

    /**
     * Whether to include sequences in haplotype nodes.
     *
     * @return Include Sequences
     */
    public Boolean includeSequences() {
        return myIncludeSequences.value();
    }

    /**
     * Set Include Sequences. Whether to include sequences in haplotype nodes.
     *
     * @param value Include Sequences
     *
     * @return this plugin
     */
    public HaplotypeGraphBuilderPlugin includeSequences(Boolean value) {
        myIncludeSequences = new PluginParameter<>(myIncludeSequences, value);
        return this;
    }

    /**
     * Whether to include variant contexts in haplotype nodes.
     *
     * @return Include Variant Contexts
     */
    public Boolean includeVariantContexts() {
        return myIncludeVariantContexts.value();
    }

    /**
     * Set Include Variant Contexts. Whether to include variant contexts in haplotype nodes.
     *
     * @param value Include Variant Contexts
     *
     * @return this plugin
     */
    public HaplotypeGraphBuilderPlugin includeVariantContexts(Boolean value) {
        myIncludeVariantContexts = new PluginParameter<>(myIncludeVariantContexts, value);
        return this;
    }

    /**
     * List of haplotype ids to include in the graph. If not specified, all ids are included.
     *
     * @return Haplotype Ids
     */
    public SortedSet<Integer> hapids() {
        return myHapids.value();
    }

    /**
     * Set Haplotype Ids. List of haplotype ids to include in the graph. If not specified, all ids are included.
     *
     * @param value Haplotype Ids
     *
     * @return this plugin
     */
    public HaplotypeGraphBuilderPlugin hapids(SortedSet<Integer> value) {
        myHapids = new PluginParameter<>(myHapids, value);
        return this;
    }

    /**
     * Chromosomes
     *
     * @return Chromosomes
     */
    public List<String> chromosomes() {
        return myChromosomes.value();
    }

    /**
     * Set Chromosomes. Chromosomes
     *
     * @param value Chromosomes
     *
     * @return this plugin
     */
    public HaplotypeGraphBuilderPlugin chromosomes(List<String> value) {
        myChromosomes = new PluginParameter<>(myChromosomes, value);
        return this;
    }

    /**
     * Optional list of taxa to include. This can be a comma
     * separated list of taxa (no spaces unless surrounded
     * by quotes), file (.txt) with list of taxa names to
     * include, or a taxa list file (.json or .json.gz). By
     * default, all taxa will be included.
     *
     * @return Taxa
     */
    public TaxaList taxaList() {
        return myTaxaList.value();
    }

    /**
     * Set Taxa. Optional list of taxa to include. This can
     * be a comma separated list of taxa (no spaces unless
     * surrounded by quotes), file (.txt) with list of taxa
     * names to include, or a taxa list file (.json or .json.gz).
     * By default, all taxa will be included.
     *
     * @param value Taxa
     *
     * @return this plugin
     */
    public HaplotypeGraphBuilderPlugin taxaList(TaxaList value) {
        myTaxaList = new PluginParameter<>(myTaxaList, value);
        return this;
    }

    public HaplotypeGraphBuilderPlugin taxaList(String value) {
        myTaxaList = new PluginParameter<>(myTaxaList, convert(value, TaxaList.class));
        return this;
    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {
        return "Haplotype Graph Builder";
    }

    @Override
    public String getToolTipText() {
        return "Haplotype Graph Builder";
    }
}
