package net.maizegenetics.pangenome.api;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.ImmutableSortedSet;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.TaxaListBuilder;
import net.maizegenetics.taxa.Taxon;
import net.maizegenetics.util.Tuple;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collector;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * This Class is a wrapper around a complete Practical Haplotype Graph (PHG).  Including HaplotypeNodes, HaplotypeEdges,
 * HaplotypePaths, HaplotypeSequences, ReferenceRanges, etc.
 *
 * @author Terry Casstevens Created August 03, 2017
 */
public class HaplotypeGraph {

    private static final Logger myLogger = Logger.getLogger(HaplotypeGraph.class);

    private final NavigableMap<Chromosome, NavigableMap<ReferenceRange, List<HaplotypeNode>>> myRangeToNode = new TreeMap<>();
    private final NavigableMap<ReferenceRange, List<HaplotypeEdge>> myEdges;

    private final List<Chromosome> myChromosomes;
    private final int myNumberNodes;

    private final BiFunction<List<HaplotypeNode>, List<HaplotypeNode>, List<HaplotypeEdge>> myEdgeFunction;

    private final static int EDGE_CACHE_SIZE = 10000;

    private final Cache<HaplotypeNode, List<HaplotypeEdge>> myLeftEdges = CacheBuilder.newBuilder()
            .initialCapacity(EDGE_CACHE_SIZE)
            .maximumSize(EDGE_CACHE_SIZE)
            .build();

    private final Cache<HaplotypeNode, List<HaplotypeEdge>> myRightEdges = CacheBuilder.newBuilder()
            .initialCapacity(EDGE_CACHE_SIZE)
            .maximumSize(EDGE_CACHE_SIZE)
            .build();

    private TaxaList myTaxaInGraph = null;

    /**
     * Constructor for HaplotypeGraph.  See {@link HaplotypeGraphBuilderPlugin}
     *
     * @param edges HaplotypeEdges
     */
    public HaplotypeGraph(Collection<HaplotypeEdge> edges) {

        myEdgeFunction = (leftNodes, rightNodes) -> createEdges(leftNodes, rightNodes);

        myEdges = new TreeMap<>();

        NavigableMap<Chromosome, NavigableMap<ReferenceRange, Set<HaplotypeNode>>> builders = new TreeMap<>();

        for (HaplotypeEdge edge : edges) {

            HaplotypeNode leftNode = edge.leftHapNode();
            ReferenceRange leftRange = leftNode.referenceRange();

            HaplotypeNode rightNode = edge.rightHapNode();
            ReferenceRange rightRange = rightNode.referenceRange();

            Chromosome chr = leftRange.chromosome();

            NavigableMap<ReferenceRange, Set<HaplotypeNode>> rangeToBuilder = builders.get(chr);
            if (rangeToBuilder == null) {
                rangeToBuilder = new TreeMap<>();
                builders.put(chr, rangeToBuilder);
            }

            Set<HaplotypeNode> builder = rangeToBuilder.get(leftRange);
            if (builder == null) {
                builder = new HashSet<>();
                rangeToBuilder.put(leftRange, builder);
            }

            builder.add(leftNode);

            builder = rangeToBuilder.get(rightRange);
            if (builder == null) {
                builder = new HashSet<>();
                rangeToBuilder.put(rightRange, builder);
            }

            builder.add(rightNode);

            List<HaplotypeEdge> temp = myEdges.get(leftRange);
            if (temp == null) {
                temp = new ArrayList<>();
                myEdges.put(leftRange, temp);
                temp.add(edge);
            } else if (temp.contains(edge)) {
                throw new IllegalStateException("HaplotypeGraph: init: duplicate edge: " + edge.toString());
            } else {
                temp.add(edge);
            }

        }

        for (Map.Entry<Chromosome, NavigableMap<ReferenceRange, Set<HaplotypeNode>>> entry : builders.entrySet()) {
            Chromosome chr = entry.getKey();
            ImmutableSortedMap.Builder<ReferenceRange, List<HaplotypeNode>> rangeListTreeMap = ImmutableSortedMap.naturalOrder();
            for (Map.Entry<ReferenceRange, Set<HaplotypeNode>> builderEntry : entry.getValue().entrySet()) {
                rangeListTreeMap.put(builderEntry.getKey(), ImmutableList.copyOf(builderEntry.getValue()));
            }
            myRangeToNode.put(chr, rangeListTreeMap.build());
        }

        Tuple<List<Chromosome>, Integer> temp = init();

        myChromosomes = temp.x;
        myNumberNodes = temp.y;

        verifyGraph();

        myLogger.info("Created graph number of edges: " + edges.size() + "  number of nodes: " + myNumberNodes + "  number of reference ranges: " + numberOfRanges());

    }

    public HaplotypeGraph(NavigableMap<ReferenceRange, List<HaplotypeNode>> nodes) {
        this(nodes, (leftNodes, rightNodes) -> CreateGraphUtils.createEdges(leftNodes, rightNodes));
    }

    /**
     * Constructor for HaplotypeGraph.  See {@link HaplotypeGraphBuilderPlugin}
     *
     * @param nodes HaplotypeNodes
     * @param edgeFunction function used to create edges
     */
    public HaplotypeGraph(NavigableMap<ReferenceRange, List<HaplotypeNode>> nodes, BiFunction<List<HaplotypeNode>, List<HaplotypeNode>, List<HaplotypeEdge>> edgeFunction) {

        myEdgeFunction = edgeFunction;

        myEdges = null;

        NavigableMap<Chromosome, NavigableMap<ReferenceRange, ImmutableList.Builder<HaplotypeNode>>> builders = new TreeMap<>();

        for (Map.Entry<ReferenceRange, List<HaplotypeNode>> entry : nodes.entrySet()) {
            for (HaplotypeNode node : entry.getValue()) {

                ReferenceRange range = node.referenceRange();

                Chromosome chr = range.chromosome();

                NavigableMap<ReferenceRange, ImmutableList.Builder<HaplotypeNode>> rangeToBuilder = builders.get(chr);
                if (rangeToBuilder == null) {
                    rangeToBuilder = new TreeMap<>();
                    builders.put(chr, rangeToBuilder);
                }

                ImmutableList.Builder<HaplotypeNode> builder = rangeToBuilder.get(range);
                if (builder == null) {
                    builder = ImmutableList.builder();
                    rangeToBuilder.put(range, builder);
                }

                builder.add(node);

            }
        }

        for (Map.Entry<Chromosome, NavigableMap<ReferenceRange, ImmutableList.Builder<HaplotypeNode>>> entry : builders.entrySet()) {
            Chromosome chr = entry.getKey();
            ImmutableSortedMap.Builder<ReferenceRange, List<HaplotypeNode>> rangeListTreeMap = ImmutableSortedMap.naturalOrder();
            for (Map.Entry<ReferenceRange, ImmutableList.Builder<HaplotypeNode>> builderEntry : entry.getValue().entrySet()) {
                rangeListTreeMap.put(builderEntry.getKey(), builderEntry.getValue().build());
            }
            myRangeToNode.put(chr, rangeListTreeMap.build());
        }

        Tuple<List<Chromosome>, Integer> temp = init();

        myChromosomes = temp.x;
        myNumberNodes = temp.y;

        verifyGraph();

        if (myEdgeFunction == null) {
            myLogger.info("Created graph edges: 0  number of nodes: " + myNumberNodes + "  number of reference ranges: " + numberOfRanges());
        } else {
            myLogger.info("Created graph edges: created when requested  number of nodes: " + myNumberNodes + "  number of reference ranges: " + numberOfRanges());
        }

    }

    private Tuple<List<Chromosome>, Integer> init() {

        ImmutableList.Builder<Chromosome> chromosomeBuilder = new ImmutableList.Builder<>();
        for (Chromosome chr : myRangeToNode.keySet()) {
            chromosomeBuilder.add(chr);
        }

        int numNodes = 0;
        for (Chromosome chr : myRangeToNode.keySet()) {
            for (List<HaplotypeNode> current : myRangeToNode.get(chr).values()) {
                numNodes += current.size();
            }
        }

        return new Tuple<>(chromosomeBuilder.build(), numNodes);

    }

    /**
     * Checks structure of graph for problems.
     */
    private void verifyGraph() {

        int estimatedNumTaxa = 0;

        int numReferenceRanges = numberOfRanges();
        Random random = new Random();

        List<ReferenceRange> ranges = referenceRangeList();

        for (int count = 0; count < 5; count++) {
            int tempNumTaxa = 0;
            for (HaplotypeNode node : nodes(ranges.get(random.nextInt(numReferenceRanges)))) {
                tempNumTaxa += node.numTaxa();
            }

            estimatedNumTaxa = Math.max(estimatedNumTaxa, tempNumTaxa);
        }

        int setInitialSize = (int) ((double) estimatedNumTaxa / 0.7);

        for (Chromosome chr : myRangeToNode.keySet()) {

            for (Map.Entry<ReferenceRange, List<HaplotypeNode>> entry : myRangeToNode.get(chr).entrySet()) {

                // Each taxon should be represented only once per ReferenceRange
                Set<Taxon> temp = new HashSet<>(setInitialSize);
                for (HaplotypeNode node : entry.getValue()) {

                    for (Taxon taxon : node.taxaList()) {
                        if (temp.contains(taxon)) {
                            throw new IllegalStateException("HaplotypeGraph: verifyGraph: taxon: " + taxon.getName() + " represented more than once for reference range: " + entry.getKey().toString());
                        } else {
                            temp.add(taxon);
                        }
                    }

                }

            }

        }

        if (myEdges != null) {

            // Check that out-going edge probabilities sum to 100%
            nodeStream().forEach(
                    node -> {
                        List<HaplotypeEdge> edges = rightEdges(node);
                        verifyEdgesForSingleNode(edges);
                    }
            );

        }

        // TODO Check that every taxa represented in a reference range that's also represented in the
        // TODO next reference range has an edge between the two nodes.

    }

    public int numberOfNodes() {
        return myNumberNodes;
    }

    public List<Chromosome> chromosomes() {
        return myChromosomes;
    }

    public int numberOfChromosomes() {
        return myChromosomes.size();
    }

    /**
     * Returns most probable {@link HaplotypePath} for each {@link Chromosome} given the specified taxon and list of
     * known {@link HaplotypeNode}s. List of nodes can have at most one node per {@link ReferenceRange}. Nodes
     * constructed with specified taxon will always be used in resulting paths. Specified nodes must either have been
     * constructed with specified taxon or reside in a reference range that has no node constructed with specified
     * taxon.
     *
     * @param taxon taxon
     * @param nodes list of known nodes
     *
     * @return paths for each chromosome
     */
    public NavigableMap<Chromosome, HaplotypePath> path(String taxon, List<HaplotypeNode> nodes) {
        return path(new Taxon(taxon), nodes);
    }

    /**
     * Returns most probable {@link HaplotypePath} for each {@link Chromosome} given the specified {@link Taxon} and
     * list of known {@link HaplotypeNode}s. List of nodes can have at most one node per {@link ReferenceRange}. Nodes
     * constructed with specified taxon will always be used in resulting paths. Specified nodes must either have been
     * constructed with specified taxon or reside in a reference range that has no node constructed with specified
     * taxon.
     *
     * @param taxon taxon
     * @param nodes list of known nodes
     *
     * @return paths for each chromosome
     */
    public NavigableMap<Chromosome, HaplotypePath> path(Taxon taxon, List<HaplotypeNode> nodes) {

        NavigableMap<ReferenceRange, HaplotypeNode> input = new TreeMap<>();

        if (taxon != null) {
            for (Chromosome chr : myChromosomes) {
                for (Map.Entry<ReferenceRange, List<HaplotypeNode>> entry : myRangeToNode.get(chr).entrySet()) {
                    for (HaplotypeNode node : entry.getValue()) {
                        if (node.taxaList().contains(taxon)) {
                            input.put(entry.getKey(), node);
                            break;
                        }
                    }
                }
            }
        }

        if (nodes != null && !nodes.isEmpty()) {
            for (HaplotypeNode node : nodes) {
                HaplotypeNode temp = input.putIfAbsent(node.referenceRange(), node);
                if (temp != null && !temp.equals(node)) {
                    throw new IllegalStateException("HaplotypeGraph: path: can't add more than one node per reference range.");
                }
            }
        }

        return path(input);

    }

    /**
     * Returns most probable {@link HaplotypePath} for each {@link Chromosome} given the specified list of known {@link
     * HaplotypeNode}s. List of nodes can have at most one node per {@link ReferenceRange}.
     *
     * @param nodes list of known nodes
     *
     * @return paths for each chromosome
     */
    public NavigableMap<Chromosome, HaplotypePath> path(List<HaplotypeNode> nodes) {
        return path((Taxon) null, nodes);
    }

    /**
     * Returns most probable {@link HaplotypePath} for each {@link Chromosome} given the specified taxon. Nodes
     * constructed with specified taxon will always be used in resulting paths.
     *
     * @param taxon taxon
     *
     * @return paths for each chromosome
     */
    public NavigableMap<Chromosome, HaplotypePath> path(String taxon) {
        return path(new Taxon(taxon), null);
    }

    /**
     * Returns most probable {@link HaplotypePath} for each {@link Chromosome} given the specified {@link Taxon}. Nodes
     * constructed with specified taxon will always be used in resulting paths.
     *
     * @param taxon taxon
     *
     * @return paths for each chromosome
     */
    public NavigableMap<Chromosome, HaplotypePath> path(Taxon taxon) {
        return path(taxon, null);
    }

    /**
     * Returns most probable {@link HaplotypePath} for each {@link Chromosome}.
     *
     * @return paths for each chromosome
     */
    public NavigableMap<Chromosome, HaplotypePath> path() {
        return path((Taxon) null, null);
    }

    private NavigableMap<Chromosome, HaplotypePath> path(NavigableMap<ReferenceRange, HaplotypeNode> nodes) {

        NavigableMap<Chromosome, HaplotypePath> result = new TreeMap<>();

        for (Chromosome chr : myChromosomes) {

            ImmutableList.Builder<HaplotypeEdge> path = ImmutableList.builder();

            Iterator<Map.Entry<ReferenceRange, List<HaplotypeNode>>> ranges = myRangeToNode.get(chr).entrySet().iterator();
            ReferenceRange firstRange = null;
            HaplotypeNode firstNode = null;

            if (ranges.hasNext()) {
                Map.Entry<ReferenceRange, List<HaplotypeNode>> entry = ranges.next();
                firstRange = entry.getKey();
                firstNode = nodes.get(entry.getKey());
            }
            while (ranges.hasNext()) {

                ReferenceRange secondRange = null;
                HaplotypeNode secondNode = null;

                while (ranges.hasNext()) {
                    Map.Entry<ReferenceRange, List<HaplotypeNode>> entry = ranges.next();
                    secondRange = entry.getKey();
                    secondNode = nodes.get(entry.getKey());
                    if (secondNode != null) {
                        break;
                    }
                }

                List<HaplotypeEdge> subPath;
                if (firstNode != null) {
                    if (secondNode != null) {
                        subPath = mostProbablePath(firstNode, secondNode);
                    } else {
                        subPath = mostProbablePath(firstNode, secondRange);
                    }
                } else if (secondNode != null) {
                    subPath = mostProbablePath(firstRange, secondNode);
                } else {
                    subPath = mostProbablePath(firstRange, secondRange);
                }

                path.addAll(subPath);

                firstNode = subPath.get(subPath.size() - 1).rightHapNode();
                firstRange = firstNode.referenceRange();

            }

            result.putIfAbsent(chr, new HaplotypePath(path.build()));

        }

        return result;

    }

    /**
     * Returns list of HaplotypeNodes that start graph.
     *
     * @return Starting HaplotypeNodes
     */
    public List<HaplotypeNode> startNodes() {
        return startNodes(myChromosomes.get(0));
    }

    public List<HaplotypeNode> startNodes(Chromosome chr) {
        return Collections.unmodifiableList(myRangeToNode.get(chr).firstEntry().getValue());
    }

    /**
     * Returns list of HaplotypeNodes that end graph.
     *
     * @return Ending HaplotypeNodes
     */
    public List<HaplotypeNode> endNodes() {
        return endNodes(myChromosomes.get(myChromosomes.size() - 1));
    }

    public List<HaplotypeNode> endNodes(Chromosome chr) {
        return Collections.unmodifiableList(myRangeToNode.get(chr).lastEntry().getValue());
    }

    /**
     * Returns list of HaplotypeNodes for given ReferenceRange
     *
     * @param range ReferenceRange
     *
     * @return List of HaplotypeNodes
     */
    public List<HaplotypeNode> nodes(ReferenceRange range) {
        return Collections.unmodifiableList(myRangeToNode.get(range.chromosome()).get(range));
    }

    public Stream<HaplotypeNode> nodeStream() {
        return StreamSupport.stream(new NodeSpliterator(), false);
    }

    private class NodeSpliterator implements Spliterator<HaplotypeNode> {

        @Override
        public boolean tryAdvance(Consumer<? super HaplotypeNode> action) {
            forEachRemaining(action);
            return true;
        }

        @Override
        public void forEachRemaining(Consumer<? super HaplotypeNode> action) {

            for (Chromosome chr : myChromosomes) {
                for (List<HaplotypeNode> nodes : myRangeToNode.get(chr).values()) {
                    for (HaplotypeNode node : nodes) {
                        action.accept(node);
                    }
                }
            }

        }

        @Override
        public Spliterator<HaplotypeNode> trySplit() {
            return null;
        }

        @Override
        public long estimateSize() {
            return myNumberNodes;
        }

        @Override
        public int characteristics() {
            return IMMUTABLE | NONNULL | SIZED;
        }
    }

    /**
     * Creates a sorted (on keys ReferenceRanges) map to list of HaplotypeNodes for given chromosome.
     *
     * @param chr chromosome
     *
     * @return map of ReferenceRanges to Lists of HaplotypeNodes
     */
    public NavigableMap<ReferenceRange, List<HaplotypeNode>> tree(Chromosome chr) {
        return myRangeToNode.get(chr);
    }

    /**
     * Return number of left edges for given node.
     *
     * @param node node
     *
     * @return number of left edges
     */
    public int numberOfLeftEdges(HaplotypeNode node) {
        return leftEdges(node).size();
    }

    /**
     * Return number of right edges for given node.
     *
     * @param node node
     *
     * @return number of right edges
     */
    public int numberOfRightEdges(HaplotypeNode node) {
        return rightEdges(node).size();
    }

    /**
     * Returns List of Left HaplotypeEdges for given HaplotypeNode
     *
     * @param node HaplotypeNode
     *
     * @return List of Left HaplotypeEdges
     */
    public List<HaplotypeEdge> leftEdges(HaplotypeNode node) {

        List<HaplotypeEdge> result = myLeftEdges.getIfPresent(node);
        if (result == null) {

            ReferenceRange previousRange = previousRange(node.referenceRange());
            if (previousRange == null) {
                return Collections.emptyList();
            }

            Map<HaplotypeNode, ImmutableList.Builder<HaplotypeEdge>> leftEdges = new HashMap<>();
            List<HaplotypeEdge> edgesBetweenTwoRanges = myEdgeFunction.apply(nodes(previousRange), nodes(node.referenceRange()));

            verifyEdgesForSingleRange(edgesBetweenTwoRanges);
            
            for (HaplotypeEdge edge : edgesBetweenTwoRanges) {

                HaplotypeNode rightNode = edge.rightHapNode();

                ImmutableList.Builder<HaplotypeEdge> leftBuilder = leftEdges.get(rightNode);
                if (leftBuilder == null) {
                    leftBuilder = ImmutableList.builder();
                    leftEdges.put(rightNode, leftBuilder);
                }
                leftBuilder.add(edge);

            }

            for (Map.Entry<HaplotypeNode, ImmutableList.Builder<HaplotypeEdge>> entry : leftEdges.entrySet()) {
                HaplotypeNode rightNode = entry.getKey();
                List<HaplotypeEdge> edges = entry.getValue().build();
                if (rightNode == node) {
                    result = edges;
                }
                myLeftEdges.put(rightNode, edges);
            }

        }

        return result;

    }

    /**
     * Returns List of Right HaplotypeEdges for given HaplotypeNode
     *
     * @param node HaplotypeNode
     *
     * @return List of Right HaplotypeEdges
     */
    public List<HaplotypeEdge> rightEdges(HaplotypeNode node) {

        List<HaplotypeEdge> result = myRightEdges.getIfPresent(node);
        if (result == null) {

            ReferenceRange nextRange = nextRange(node.referenceRange());
            if (nextRange == null) {
                return Collections.emptyList();
            }

            Map<HaplotypeNode, ImmutableList.Builder<HaplotypeEdge>> rightEdges = new HashMap<>();
            List<HaplotypeEdge> edgesBetweenTwoRanges = myEdgeFunction.apply(nodes(node.referenceRange()), nodes(nextRange));

            for (HaplotypeEdge edge : edgesBetweenTwoRanges) {

                HaplotypeNode leftNode = edge.leftHapNode();

                ImmutableList.Builder<HaplotypeEdge> rightBuilder = rightEdges.get(leftNode);
                if (rightBuilder == null) {
                    rightBuilder = ImmutableList.builder();
                    rightEdges.put(leftNode, rightBuilder);
                }
                rightBuilder.add(edge);

            }

            for (Map.Entry<HaplotypeNode, ImmutableList.Builder<HaplotypeEdge>> entry : rightEdges.entrySet()) {
                HaplotypeNode leftNode = entry.getKey();
                List<HaplotypeEdge> edges = entry.getValue().build();
                if (leftNode == node) {
                    result = edges;
                }
                myRightEdges.put(leftNode, edges);
            }

        }

        verifyEdgesForSingleNode(result);
        return result;

    }

    private void verifyEdgesForSingleRange(List<HaplotypeEdge> edgesBetweenTwoRanges) {

        Map<HaplotypeNode, List<HaplotypeEdge>> rightEdges = new HashMap<>();

        for (HaplotypeEdge edge : edgesBetweenTwoRanges) {

            HaplotypeNode leftNode = edge.leftHapNode();

            List<HaplotypeEdge> rightBuilder = rightEdges.get(leftNode);
            if (rightBuilder == null) {
                rightBuilder = new ArrayList<>();
                rightEdges.put(leftNode, rightBuilder);
            }
            rightBuilder.add(edge);

        }

        for (Map.Entry<HaplotypeNode, List<HaplotypeEdge>> entry : rightEdges.entrySet()) {
            List<HaplotypeEdge> edges = entry.getValue();
            verifyEdgesForSingleNode(edges);
        }

    }

    private void verifyEdgesForSingleNode(List<HaplotypeEdge> edges) {

        double totalProbability = 0.0;
        if (edges != null && !edges.isEmpty()) {
            for (HaplotypeEdge edge : edges) {
                totalProbability += edge.edgeProbability();
            }
            if (totalProbability < 0.995 || totalProbability > 1.005) {
                for (HaplotypeEdge edge : edges) {
                    myLogger.info("left taxa: " + Arrays.deepToString(edge.leftHapNode().taxaList().toArray()) + " right taxa: " + Arrays.deepToString(edge.rightHapNode().taxaList().toArray()) + " edge probability: " + edge.edgeProbability());
                }
                throw new IllegalStateException("HaplotypeGraph: verifyEdgesForSingleNode: outgoing edges don't sum to 1.0: " + totalProbability);
            }
        }

    }

    public ReferenceRange previousRange(ReferenceRange range) {
        return myRangeToNode.get(range.chromosome()).lowerKey(range);
    }

    public ReferenceRange nextRange(ReferenceRange range) {
        return myRangeToNode.get(range.chromosome()).higherKey(range);
    }

    private List<HaplotypeEdge> createEdges(List<HaplotypeNode> leftNodes, List<HaplotypeNode> rightNodes) {
        if (leftNodes == null || rightNodes == null || leftNodes.isEmpty() || rightNodes.isEmpty()) {
            return Collections.emptyList();
        }
        HaplotypeNode leftNode = leftNodes.get(0);
        ReferenceRange range = leftNode.referenceRange();
        return myEdges.get(range);
    }

    /**
     * Returns optional edge between given nodes.
     *
     * @param node1 first node
     * @param node2 second node
     *
     * @return edge
     */
    public Optional<HaplotypeEdge> edge(HaplotypeNode node1, HaplotypeNode node2) {

        ReferenceRange range1 = node1.referenceRange();
        ReferenceRange range2 = node2.referenceRange();

        if (range1.equals(range2)) {
            throw new IllegalArgumentException("HaplotypeGraph: edge: given nodes are in the same reference range.");
        }

        if (range1.compareTo(range2) < 0) {
            for (HaplotypeEdge edge : rightEdges(node1)) {
                if (node2.equals(edge.rightHapNode())) {
                    return Optional.of(edge);
                }
            }
            return Optional.empty();
        } else {
            for (HaplotypeEdge edge : rightEdges(node2)) {
                if (node1.equals(edge.rightHapNode())) {
                    return Optional.of(edge);
                }
            }
            return Optional.empty();
        }

    }

    /**
     * Returns number of reference ranges in this graph.
     *
     * @return number of reference ranges
     */
    public int numberOfRanges() {
        int result = 0;
        for (Chromosome chr : myChromosomes) {
            result += myRangeToNode.get(chr).size();
        }
        return result;
    }

    /**
     * Returns sorted set of reference ranges for whole graph.  Consider using {@link #referenceRangeStream()}
     *
     * @return sorted set of all reference ranges
     */
    public Set<ReferenceRange> referenceRanges() {
        return referenceRangeStream().collect(new Collector<ReferenceRange, ImmutableSortedSet.Builder<ReferenceRange>, ImmutableSortedSet<ReferenceRange>>() {
            @Override
            public Supplier<ImmutableSortedSet.Builder<ReferenceRange>> supplier() {
                return () -> ImmutableSortedSet.naturalOrder();
            }

            @Override
            public BiConsumer<ImmutableSortedSet.Builder<ReferenceRange>, ReferenceRange> accumulator() {
                return (builder, range) -> builder.add(range);
            }

            @Override
            public BinaryOperator<ImmutableSortedSet.Builder<ReferenceRange>> combiner() {
                return (builder, builder2) -> builder.addAll(builder2.build());
            }

            @Override
            public Function<ImmutableSortedSet.Builder<ReferenceRange>, ImmutableSortedSet<ReferenceRange>> finisher() {
                return builder -> builder.build();
            }

            @Override
            public Set<Characteristics> characteristics() {
                return Collections.EMPTY_SET;
            }
        });
    }

    /**
     * Returns list of reference ranges for whole graph.  Consider using {@link #referenceRangeStream()}
     *
     * @return list of all reference ranges
     */
    public List<ReferenceRange> referenceRangeList() {
        return referenceRangeStream().collect(new Collector<ReferenceRange, ImmutableList.Builder<ReferenceRange>, ImmutableList<ReferenceRange>>() {
            @Override
            public Supplier<ImmutableList.Builder<ReferenceRange>> supplier() {
                return () -> ImmutableList.builder();
            }

            @Override
            public BiConsumer<ImmutableList.Builder<ReferenceRange>, ReferenceRange> accumulator() {
                return (builder, range) -> builder.add(range);
            }

            @Override
            public BinaryOperator<ImmutableList.Builder<ReferenceRange>> combiner() {
                return (builder, builder2) -> builder.addAll(builder2.build());
            }

            @Override
            public Function<ImmutableList.Builder<ReferenceRange>, ImmutableList<ReferenceRange>> finisher() {
                return builder -> builder.build();
            }

            @Override
            public Set<Characteristics> characteristics() {
                return Collections.EMPTY_SET;
            }
        });
    }

    /**
     * Returns stream of reference ranges for entire graph.
     *
     * @return stream of reference ranges
     */
    public Stream<ReferenceRange> referenceRangeStream() {
        Stream<ReferenceRange>[] temp = new Stream[myChromosomes.size()];
        for (int i = 0; i < myChromosomes.size(); i++) {
            temp[i] = myRangeToNode.get(myChromosomes.get(i)).keySet().stream();
        }
        return Stream.of(temp).flatMap(i -> i);
    }

    /**
     * Returns stream of reference ranges for given chromosome.
     *
     * @param chr chromosome
     *
     * @return stream of reference ranges
     */
    public Stream<ReferenceRange> referenceRangeStream(Chromosome chr) {
        return myRangeToNode.get(chr).keySet().stream();
    }

    /**
     * Returns the first reference range in this graph.
     *
     * @return first reference range
     */
    public ReferenceRange firstReferenceRange() {
        return myRangeToNode.get(myChromosomes.get(0)).firstKey();
    }

    /**
     * Returns the last reference range in this graph.
     *
     * @return last reference range
     */
    public ReferenceRange lastReferenceRange() {
        return myRangeToNode.get(myChromosomes.get(myChromosomes.size() - 1)).lastKey();
    }

    /**
     * Returns the first reference range for the given chromosome.
     *
     * @param chr chromosome
     *
     * @return first reference range
     */
    public ReferenceRange firstReferenceRange(Chromosome chr) {
        return myRangeToNode.get(chr).firstKey();
    }

    /**
     * Returns the last reference range for the given chromosome.
     *
     * @param chr chromosome
     *
     * @return last reference range
     */
    public ReferenceRange lastReferenceRange(Chromosome chr) {
        return myRangeToNode.get(chr).lastKey();
    }

    /**
     * Return all taxa represented in this graph.
     *
     * @return all taxa
     */
    public TaxaList taxaInGraph() {

        if (myTaxaInGraph != null) return myTaxaInGraph;

        Set<Taxon> taxa = new TreeSet<>();

        for (Chromosome chr : myChromosomes) {

            for (Map.Entry<ReferenceRange, List<HaplotypeNode>> entry : myRangeToNode.get(chr).entrySet()) {
                for (HaplotypeNode node : entry.getValue()) {
                    for (Taxon taxon : node.taxaList()) {
                        taxa.add(taxon);
                    }
                }
            }

        }

        TaxaListBuilder builder = new TaxaListBuilder();
        builder.addAll(taxa);
        myTaxaInGraph = builder.build();

        return myTaxaInGraph;

    }

    /**
     * Returns total number of taxa represented by this graph
     *
     * @return total number of taxa
     */
    public int totalNumberTaxa() {
        return taxaInGraph().numberOfTaxa();
    }

    /**
     * Returns taxa represented by given reference range.
     *
     * @param range reference range
     *
     * @return taxa in reference range
     */
    public TaxaList taxaInRange(ReferenceRange range) {

        Set<Taxon> taxa = new TreeSet<>();

        for (HaplotypeNode node : myRangeToNode.get(range.chromosome()).get(range)) {
            for (Taxon taxon : node.taxaList()) {
                taxa.add(taxon);
            }
        }

        TaxaListBuilder builder = new TaxaListBuilder();
        builder.addAll(taxa);
        return builder.build();

    }

    /**
     * Returns number of taxa represented by given reference range.
     *
     * @param range reference range
     *
     * @return number of taxa in reference range
     */
    public int numberTaxa(ReferenceRange range) {

        int result = 0;
        for (HaplotypeNode node : myRangeToNode.get(range.chromosome()).get(range)) {
            result += node.numTaxa();
        }
        return result;

    }

    /**
     * Returns most probable path from start node to end reference range.
     *
     * @param startNode start node
     * @param endRange end reference range
     *
     * @return most probable path
     */
    private List<HaplotypeEdge> mostProbablePath(HaplotypeNode startNode, ReferenceRange endRange) {

        if (startNode.referenceRange().compareTo(endRange) >= 0) {
            throw new IllegalStateException("HaplotypeGraph: mostProbablePath: start node should be earlier than end range.");
        }

        if (!startNode.referenceRange().chromosome().equals(endRange.chromosome())) {
            throw new IllegalStateException("HaplotypeGraph: mostProbablePath: start node range's chromosome should be same as end range chromosome.");
        }

        return mostProbablePath(accumulateProbabilities(startingMap(startNode), endRange));

    }

    /**
     * Returns most probable path from start reference range to end node.
     *
     * @param startRange start reference range
     * @param endNode end node
     *
     * @return most probable path
     */
    private List<HaplotypeEdge> mostProbablePath(ReferenceRange startRange, HaplotypeNode endNode) {

        if (startRange.compareTo(endNode.referenceRange()) >= 0) {
            throw new IllegalStateException("HaplotypeGraph: mostProbablePath: start range should be earlier than end node range.");
        }

        if (!startRange.chromosome().equals(endNode.referenceRange().chromosome())) {
            throw new IllegalStateException("HaplotypeGraph: mostProbablePath: start range chromosome should be same as end node range's chromosome.");
        }

        return mostProbablePath(endNode, accumulateProbabilities(startingMap(startRange), endNode.referenceRange()));

    }

    /**
     * Returns most probable path from start node to end node.
     *
     * @param startNode start node
     * @param endNode end node
     *
     * @return most probable path
     */
    private List<HaplotypeEdge> mostProbablePath(HaplotypeNode startNode, HaplotypeNode endNode) {

        if (startNode.referenceRange().compareTo(endNode.referenceRange()) >= 0) {
            throw new IllegalStateException("HaplotypeGraph: mostProbablePath: start node should be in earlier reference range than end node.");
        }

        if (!startNode.referenceRange().chromosome().equals(endNode.referenceRange().chromosome())) {
            throw new IllegalStateException("HaplotypeGraph: mostProbablePath: start node range's chromosome should be same as end node range's chromosome.");
        }

        return mostProbablePath(endNode, accumulateProbabilities(startingMap(startNode), endNode.referenceRange()));

    }

    /**
     * Returns most probable path from start range to end range.
     *
     * @param startRange start reference range
     * @param endRange end reference range
     *
     * @return most probable path
     */
    private List<HaplotypeEdge> mostProbablePath(ReferenceRange startRange, ReferenceRange endRange) {

        if (startRange.compareTo(endRange) >= 0) {
            throw new IllegalStateException("HaplotypeGraph: mostProbablePath: start range should be earlier than end range.");
        }

        if (!startRange.chromosome().equals(endRange.chromosome())) {
            throw new IllegalStateException("HaplotypeGraph: mostProbablePath: start range chromosome should be same as end range chromosome.");
        }

        return mostProbablePath(accumulateProbabilities(startingMap(startRange), endRange));

    }

    /**
     * Creates starting map for accumulative probabilities for given node. The node (map key) is the ending location for
     * the accumulative path.
     *
     * @param node starting node
     *
     * @return map of accumulative probabilities
     */
    private Map<HaplotypeNode, ProbabilityForPath> startingMap(HaplotypeNode node) {

        Map<HaplotypeNode, ProbabilityForPath> result = new HashMap<>();
        for (HaplotypeEdge edge : rightEdges(node)) {
            List<HaplotypeEdge> oneEdge = new ArrayList<>();
            oneEdge.add(edge);
            result.put(edge.rightHapNode(), new ProbabilityForPath(edge.edgeProbability(), oneEdge));
        }
        return result;

    }

    /**
     * Creates starting map for accumulative probabilities for given list of nodes.
     *
     * @param nodes starting nodes
     *
     * @return map of accumulative probabilities
     */
    private Map<HaplotypeNode, ProbabilityForPath> startingMap(List<HaplotypeNode> nodes) {

        Map<HaplotypeNode, ProbabilityForPath> result = new HashMap<>();
        for (HaplotypeNode node : nodes) {
            for (HaplotypeEdge edge : rightEdges(node)) {
                ProbabilityForPath temp = result.get(edge.rightHapNode());
                if (temp == null || (edge.edgeProbability() > temp.myProbability && temp.myPath.get(0).leftHapNode().numTaxa() <= node.numTaxa())) {
                    List<HaplotypeEdge> oneEdge = new ArrayList<>();
                    oneEdge.add(edge);
                    result.put(edge.rightHapNode(), new ProbabilityForPath(edge.edgeProbability(), oneEdge));
                }
            }
        }
        return result;

    }

    /**
     * Creates starting map for accumulative probabilities for given reference range. The node (map key) is the ending
     * location for the accumulative path.
     *
     * @param range starting reference range
     *
     * @return map of accumulative probabilities
     */
    private Map<HaplotypeNode, ProbabilityForPath> startingMap(ReferenceRange range) {
        return startingMap(nodes(range));
    }

    /**
     * This returns the most probable path given a map of accumulated probabilities.
     *
     * @param finalMap final map
     *
     * @return most probable path
     */
    private List<HaplotypeEdge> mostProbablePath(Map<HaplotypeNode, ProbabilityForPath> finalMap) {

        double highestProbability = 0.0;
        List<HaplotypeEdge> mostLikelyPath = null;
        for (Map.Entry<HaplotypeNode, ProbabilityForPath> entry : finalMap.entrySet()) {
            if (entry.getValue().myProbability > highestProbability) {
                highestProbability = entry.getValue().myProbability;
                mostLikelyPath = entry.getValue().myPath;
            }
        }

        return mostLikelyPath;

    }

    /**
     * This returns the most probable path give a map of accumulated probabilities and an ending node.
     *
     * @param node end node
     * @param finalMap final map
     *
     * @return most probable path
     */
    private List<HaplotypeEdge> mostProbablePath(HaplotypeNode node, Map<HaplotypeNode, ProbabilityForPath> finalMap) {

        ProbabilityForPath result = finalMap.get(node);
        if (result == null) {
            throw new IllegalStateException("HaplotypeGraph: mostProbablePath: no paths between given nodes.");
        }
        return result.myPath;

    }

    /**
     * This accumulates probabilities starting at given map until ending reference range.
     *
     * @param currentMap current map of probabilities
     * @param endRange ending reference range
     *
     * @return map of accumulative probabilities
     */
    private Map<HaplotypeNode, ProbabilityForPath> accumulateProbabilities(Map<HaplotypeNode, ProbabilityForPath> currentMap, ReferenceRange endRange) {

        while (true) {

            if (currentMap.size() == 0) {
                throw new IllegalStateException("HaplotypeGraph: accumulateProbabilities: no paths between given ranges.");
            }

            ReferenceRange currentReferenceRange = currentMap.keySet().iterator().next().referenceRange();
            if (endRange.equals(currentReferenceRange)) {
                return currentMap;
            } else if (currentReferenceRange.compareTo(endRange) > 0) {
                throw new IllegalStateException("HaplotypeGraph: accumulateProbabilities: end reference range not found.");
            }

            currentMap = moveForward(currentMap);

        }

    }

    private Map<HaplotypeNode, ProbabilityForPath> moveForward(Map<HaplotypeNode, ProbabilityForPath> input) {

        Map<HaplotypeNode, ProbabilityForPath> result = new HashMap<>();

        for (Map.Entry<HaplotypeNode, ProbabilityForPath> current : input.entrySet()) {
            HaplotypeNode leftNode = current.getKey();
            double probability = current.getValue().myProbability;
            List<HaplotypeEdge> path = current.getValue().myPath;

            for (HaplotypeEdge edge : rightEdges(leftNode)) {
                HaplotypeNode rightNode = edge.rightHapNode();
                double newProbability = probability + edge.edgeProbability();
                ProbabilityForPath existing = result.get(rightNode);
                if (existing == null || newProbability > existing.myProbability) {
                    List<HaplotypeEdge> newPath = new ArrayList<>();
                    newPath.addAll(path);
                    newPath.add(edge);
                    result.put(rightNode, new ProbabilityForPath(newProbability, newPath));
                }
            }
        }

        return result;

    }

    private class ProbabilityForPath {

        public final double myProbability;
        public final List<HaplotypeEdge> myPath;

        public ProbabilityForPath(double probability, List<HaplotypeEdge> path) {
            myProbability = probability;
            myPath = path;
        }
    }

}
