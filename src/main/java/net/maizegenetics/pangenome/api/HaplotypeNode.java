package net.maizegenetics.pangenome.api;

import htsjdk.variant.variantcontext.VariantContext;
import net.maizegenetics.pangenome.db_loading.GZipCompression;
import net.maizegenetics.pangenome.db_loading.VariantsProcessingUtils;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.util.Tuple;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Terry Casstevens Created June 19, 2017
 * @author Zack Miller
 * @author Karl Kremling
 * @author Jacob Washburn
 *
 * Class for representing haplotype nodes (HaplotypNode) , also known as anchor or inner anchor haplotypes. Haplotype
 * nodes contain are the basic unit of the graph and represent sequence blocks of a given single haplotype.  They are
 * derived from a single chromosome from a single line/genotype/variety. In many cases, a haplotype node may also use
 * the consensus sequence of very similar haplotypes from multiple lines/genotypes/varieties.
 */
public class HaplotypeNode implements Comparable<HaplotypeNode> {

    private static final Optional<List<VariantInfo>> EMPTY_VARIANT_INFOS = Optional.empty();
    private static final Optional<List<Long>> EMPTY_VARIANT_CONTEXTS_LONG = Optional.empty();

    private final HaplotypeSequence myHaplotypeSequence;
    private final TaxaList myTaxaList;
    private final int myHapId;
    private final byte[] byteEncodedVariants;
    private final Map<Integer, Variant> myVariantMap;
    private final int myGenomeFileID;
    private final String myASMContig;
    private final int myASMStartCoordinate;
    private final int myASMEndCoordinate;
    private final String myASMStrand;

    /**
     * Constructor
     *
     * @param haplotypeSequence sequence
     * @param taxaList taxa list
     * @param id hapid from database
     * @param asmContig assembly contig / chromosome
     * @param asmStart assembly start coordinate
     * @param asmEnd assembly end coordinate
     * @param genomeFileID genome file ID
     * @param encodedVariants variant encodings
     * @param variantMap the variant map for this graph. can be null if variants not included.
     */
    public HaplotypeNode(HaplotypeSequence haplotypeSequence, TaxaList taxaList, int id, String asmContig, int asmStart, int asmEnd, String asmStrand, int genomeFileID, byte[] encodedVariants, Map<Integer, Variant> variantMap) {

        myHaplotypeSequence = haplotypeSequence;
        if (taxaList == null || taxaList.isEmpty()) {
            throw new IllegalArgumentException("HaplotypeNode: init: taxa list must contain at least one taxon.");
        }
        myTaxaList = taxaList;
        myHapId = id;

        myASMContig = asmContig;
        myASMStartCoordinate = asmStart;
        myASMEndCoordinate = asmEnd;
        myASMStrand = asmStrand;

        myGenomeFileID = genomeFileID;

        byteEncodedVariants = encodedVariants;

        myVariantMap = variantMap;

    }

    public HaplotypeNode(HaplotypeSequence haplotypeSequence, TaxaList taxaList, int id, String asmContig, int asmStart, int asmEnd, String asmStrand, int genomeFileID) {
        this(haplotypeSequence, taxaList, id, asmContig, asmStart, asmEnd, asmStrand,genomeFileID, null, null);
    }

    public HaplotypeNode(HaplotypeSequence haplotypeSequence, TaxaList taxaList, int id) {
        this(haplotypeSequence, taxaList, id, "0", 0, 0, ".",-1);
    }

    public HaplotypeNode(HaplotypeSequence haplotypeSequence, TaxaList taxaList) {
        this(haplotypeSequence, taxaList, -1, "0", 0, 0,".", -1);
    }

    /**
     * HaplotypeSequence object containing the nucleotide sequence and other sequence related information for a given
     * HaplotypeNode object.
     */
    public HaplotypeSequence haplotypeSequence() {
        return myHaplotypeSequence;
    }

    /**
     * List of taxa used when the consensus sequence was created or a single taxon.
     */
    public TaxaList taxaList() {
        return myTaxaList;
    }

    public int id() {
        return myHapId;
    }

    public String asmContig() {
        return myASMContig;
    }

    public int asmStart() {
        return myASMStartCoordinate;
    }

    public int asmEnd() {
        return myASMEndCoordinate;
    }

    public String asmStrand() { return myASMStrand;}

    public int genomeFileID() {
        return myGenomeFileID;
    }

    public Map<Integer, Variant> variantMap() {
        return myVariantMap;
    }

    /**
     * Object containing the range of genomic coordinate values associated with a given HaplotypeNode and other details
     * associated with that range of values. These values correspond to the reference genome being used (Generally B73
     * in Maize).
     */
    public ReferenceRange referenceRange() {
        return myHaplotypeSequence.referenceRange();
    }

    /**
     * Number of taxa used when consensus sequence was created or one if this is an original sequence.
     *
     * @return number of taxa
     */
    public int numTaxa() {
        return myTaxaList.numberOfTaxa();
    }

    /**
     * For most uses, {@link #variantContextsLong() variantContextsLong} will be more convenient because
     * it returns the same information decoded as a List of Longs.
     *
     * @return byte encoded variant information stored in the PHG DB
     */
    public byte[] byteEncodedVariants() {
        return byteEncodedVariants;
    }

    @Override
    public String toString() {
        return "HaplotypeNode{" +
                "myHaplotypeSequence=" + myHaplotypeSequence +
                ", myTaxaList=" + Arrays.deepToString(myTaxaList.toArray()) +
                ", myReferenceRange=" + referenceRange() +
                '}';
    }


    /**
     * Returns the List<Long> of variant info
     *
     * @return a list of Long variant records
     */
    public Optional<List<Long>> variantContextsLong() {
        if (byteEncodedVariants == null) {
            return EMPTY_VARIANT_CONTEXTS_LONG;
        }
        List<Long> temp = VariantsProcessingUtils.decodeByteArrayToVariantLongList(byteEncodedVariants);
        return Optional.of(temp);
    }

    /**
     * These correspond to the VariantContext but holds less information for memory efficiency.
     * For reference blocks, isVariant will be false and refAllele may equal "REF" rather than
     * the actual allele for the first base pair of the block.
     * For that reason, converting VariantInfo to VariantContext will require
     * a copy of the reference genome sequence to get the actual refAllele values.
     *
     * @return variant infos
     */
    public Optional<List<VariantInfo>> variantInfos() {
        if (byteEncodedVariants == null) return EMPTY_VARIANT_INFOS;

        String chrom = myHaplotypeSequence.referenceRange().chromosome().getName();
        List<VariantInfo> variantInfoList = VariantsProcessingUtils.decodeByteArrayToVariantLongList(byteEncodedVariants)
                .stream()
                .map(var -> new Tuple<>(VariantUtils.decodeLongVariant(var), var))
                .filter(info -> info.x[1] > 0)
                .filter(info -> info.x[0] != 1 || myVariantMap.containsKey(info.x[1]))
                .map(info -> {
                    if (info.x[0] == 1) {  //info[0] = 1 for a variant, -1 for a reference block
                        // get the variant from variantMap
                        Variant thisVariant = myVariantMap.get(info.x[1]);

                        String refAlleleStr = GZipCompression.decompress(thisVariant.refAllele().baseString());
                        String altAlleleStr = GZipCompression.decompress(thisVariant.altAllele().baseString());

                        String genotypeStr = VariantUtils.assignGenotpe(refAlleleStr, altAlleleStr, info.x[2], info.x[3]);

                        //calculate end as start + refAllele length -1
                        int endpos = thisVariant.position() + thisVariant.refAllele().length() - 1;

                        return new VariantInfo(thisVariant.chromosome(), thisVariant.position(), endpos, genotypeStr, refAlleleStr, altAlleleStr, true, info.y);
                    }
                    // info is a refblock
                    int end = info.x[3] + info.x[1] - 1;
                    return new VariantInfo(chrom, info.x[3], end, VariantInfo.Ref,
                            VariantInfo.Ref, VariantInfo.NonRef, false, info.y);
                })
                .collect(Collectors.toList());

        return Optional.of(variantInfoList);
    }

    @Override
    public int compareTo(HaplotypeNode o) {
        int result = myHaplotypeSequence.referenceRange().compareTo(o.haplotypeSequence().referenceRange());
        if (result != 0) {
            return result;
        }
        return myTaxaList.taxaName(0).compareTo(o.taxaList().taxaName(0));
    }

    public Optional<List<VariantContext>> variantContexts() {
        throw new UnsupportedOperationException("variantContexts are no longer available from HaplotypeNode");
    }

    /**
     * These will be instantiated only when requested.
     */
    public static class VariantInfo {

        private final String chromosome;
        private final int start;
        private final int end;
        private final String genotypeString;
        private final String refAlleleString;
        private final String altAlleleString;
        private boolean isVariant;
        private long variantLong;

        private int[] alleleDepths;

        //fields with package level access
        public static final String Ref = "REF";
        public static final String NonRef = "NON_REF";
        public static final String missing = "N";

        public VariantInfo(String chr, int startPos, int endPos, String genotype, String refAllele,
                           String altAllele, boolean isVariant, long varLong) {
            chromosome = chr;
            start = startPos;
            end = endPos;
            genotypeString = genotype;
            refAlleleString = refAllele;
            altAlleleString = altAllele;

            this.isVariant = isVariant;
            variantLong = varLong;
        }

        public VariantInfo(String chr, int startPos, int endPos, String genotype, String refAllele,
                           String altAllele, boolean isVariant, int[] alleleDepths) {
            chromosome = chr;
            start = startPos;
            end = endPos;
            genotypeString = genotype;
            refAlleleString = refAllele;
            altAlleleString = altAllele;

            this.isVariant = isVariant;
            this.alleleDepths = alleleDepths;
        }

        public String chromosome() {
            return chromosome;
        }

        public int start() {
            return start;
        }

        public int end() {
            return end;
        }

        public String genotypeString() {
            return genotypeString;
        }

        public String refAlleleString() {
            return refAlleleString;
        }

        public String altAlleleString() {
            return altAlleleString;
        }

        public boolean isVariant() {
            return isVariant;
        }

        public boolean isIndel() {
            return isVariant && ((start != end) || (altAlleleString.length() > 1));
        }

        public int variantId() {
            if (variantLong == 0l) {
                throw new IllegalStateException("VariantInfo.variantId(): variantLong is not set so no VariantId can be found.");
            }
            int[] info = VariantUtils.decodeLongVariant(variantLong);
            if (info[0] == -1) return -1;
            return info[1];
        }

        public int length() {
            return end - start + 1;
        }

        /**
         * @return an int array of ref depth, alt depth
         */
        public int[] depth() {
            if (alleleDepths != null) {
                return alleleDepths;
            }
            int[] info = VariantUtils.decodeLongVariant(variantLong);
            if (info[0] == -1) {
                return new int[]{info[2], 0};
            } else {
                return new int[]{info[2], info[3]};
            }
        }

        public long toLong() {
            if (variantLong == 0l) {
                throw new IllegalStateException("VariantInfo.toLong(): variantLong is not set.");
            }
            return variantLong;
        }
    }

}
