package net.maizegenetics.pangenome.api;

/**
 * The AlleleInfo class holds the data for a record coming from the alleles tables
 * in the PHG database.
 * @author peterbradbury
 *
 */
public class AlleleInfo {
    private final int id;
    private final byte[] baseString;
    private final String displayString;
    private final int length;
    
    public AlleleInfo(int alleleId, byte[] baseStr, String displayStr, int len) {
        id = alleleId;
        baseString = baseStr;
        displayString = displayStr;
        length = len;
    }
    
    public int id() {return id;}
    public byte[] baseString() {return baseString;}
    public String displayString() {return displayString;}
    public int length() {return length;}
}
