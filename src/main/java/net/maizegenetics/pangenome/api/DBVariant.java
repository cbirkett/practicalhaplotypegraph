package net.maizegenetics.pangenome.api;

import htsjdk.variant.variantcontext.Allele;
import net.maizegenetics.dna.map.Chromosome;

public class DBVariant {



    private int myVariantID, myStartPosition, myAncestralID;
    private Chromosome myChromosome;
    private Allele myRefAllele, myAltAllele;

    public DBVariant(int variantID, Chromosome chromosome, int startPosition, Allele refAllele, Allele altAllele, int ancestralID) {
        myVariantID = variantID;
        myChromosome = chromosome;
        myStartPosition = startPosition;
        myRefAllele = refAllele;
        myAltAllele = altAllele;
        myAncestralID = ancestralID;
    }

    public int getMyVariantID() {
        return myVariantID;
    }

    public int getMyStartPosition() {
        return myStartPosition;
    }

    public int getMyAncestralID() {
        return myAncestralID;
    }

    public Chromosome getMyChromosome() {
        return myChromosome;
    }

    public Allele getMyRefAllele() {
        return myRefAllele;
    }

    public Allele getMyAltAllele() {
        return myAltAllele;
    }
}
