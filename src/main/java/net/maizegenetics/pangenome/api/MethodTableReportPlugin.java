package net.maizegenetics.pangenome.api;

import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.util.TableReportBuilder;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.Frame;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * MethodTableReportPlugin exports the contents of the PHG db method table to a table report.
 * Because a number of plugins require method names as parameters, the table contents make a useful reference.
 */
public class MethodTableReportPlugin extends AbstractPlugin {
    private static final Logger myLogger = Logger.getLogger(MethodTableReportPlugin.class);

    private PluginParameter<String> configFile = new PluginParameter.Builder<>("configFile", null, String.class)
            .inFile()
            .required(true)
            .description("A config file path containing database connection parameters")
            .guiName("Config File")
            .build();

    public MethodTableReportPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    public MethodTableReportPlugin() {
        this(null, false);
    }

    @Override
    public DataSet processData(DataSet input) {
        Connection conn = DBLoadingUtils.connection(configFile.value(), false);
        String sqlstr = "select method_id, method_type, name, description from methods";
        String[] headers = new String[]{"method_id","method_type","type_name","method_name","description"};
        TableReportBuilder reportBuilder = TableReportBuilder.getInstance("PHG Methods", headers);

        Map<Integer, String> typeMap = Arrays.stream(DBLoadingUtils.MethodType.values())
                .collect(Collectors.toMap(m -> m.getValue(), m -> m.name()));

        try(ResultSet rsMethods = conn.createStatement().executeQuery(sqlstr)) {
            while(rsMethods.next()) {
                int typeId = rsMethods.getInt("method_type");
                Object[] row = new Object[]{rsMethods.getInt("method_id"), typeId,
                        typeMap.get(typeId), rsMethods.getString("name"),
                        rsMethods.getString("description")};
                reportBuilder.add(row);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException("Problem executing query: " + sqlstr, e);
        }

        String comment = String.format("methods table for config file %s", configFile);
        Datum reportDatum = new Datum("Methods", reportBuilder.build(), comment);
        return new DataSet(reportDatum, this);
    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {
        return "PHG Method Table";
    }

    @Override
    public String getToolTipText() {
        return "PHG methods table listing";
    }

    public MethodTableReportPlugin configFile(String filename) {
        configFile = new PluginParameter<>(configFile, filename);
        return this;
    }

    public String configFile() {
        return configFile.value();
    }
}
