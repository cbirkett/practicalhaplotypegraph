package net.maizegenetics.pangenome.api;

/**
 * The Variant class is used to store records from the variants table in the PHG database.
 * 
 * @author peterbradbury
 *
 */
public class Variant {
    private int id;
    private final String chromosome;
    private final int position;
    private final AlleleInfo refAllele;
    private final AlleleInfo altAllele;

    public Variant(int variantId, String chrom, int pos, AlleleInfo ref, AlleleInfo alt) {
        id = variantId;
        chromosome = chrom;
        position = pos;
        refAllele = ref;
        altAllele = alt;
    }
    
    public int id() {return id;}
    public String chromosome() {return chromosome;}
    public int position() {return position;}
    public AlleleInfo refAllele() {return refAllele;}
    public AlleleInfo altAllele() {return altAllele;}
    
}
