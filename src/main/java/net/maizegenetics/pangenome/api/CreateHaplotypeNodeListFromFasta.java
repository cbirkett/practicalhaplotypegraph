package net.maizegenetics.pangenome.api;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.pangenome.db_loading.AnchorDataPHG;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.taxa.TaxaListBuilder;
import net.maizegenetics.util.CheckSum;
import net.maizegenetics.util.DirectoryCrawler;
import net.maizegenetics.util.Tuple;
import net.maizegenetics.util.Utils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Simple class to create a haplotype node list from a set of or a single fasta file
 * Created by zrm22 on 8/14/17.
 */
public class CreateHaplotypeNodeListFromFasta {
    private static final Logger myLogger = Logger.getLogger(CreateHaplotypeNodeListFromFasta.class);


    public static List<HaplotypeNode> createHaplotypeNodeList(String fastaFile,String intervalFile) {
        List<HaplotypeNode> nodesInCurrentFasta = createNodes(fastaFile,intervalFile);
        return nodesInCurrentFasta;
    }

    public static List<HaplotypeNode> createHaplotypeNodeListFromDirectory(String fastaDir, String intervalFile) {
        ImmutableList.Builder<HaplotypeNode> nodeListBuilder = new ImmutableList.Builder<>();
        java.util.List<Path> fastaPaths = DirectoryCrawler.listPaths("glob:*.fa", Paths.get(fastaDir));

        for (Path fastaPath: fastaPaths) {
            List<HaplotypeNode> nodesInCurrentFasta = createHaplotypeNodeList(fastaPath.toString(),intervalFile);
            nodeListBuilder.addAll(nodesInCurrentFasta);
        }
        return nodeListBuilder.build();
    }

    private static List<HaplotypeNode> createNodes(String fastaFile,String intervalFile) {
        RangeMap<Position,Integer> positionRangeToIdMap = createIdMapping(intervalFile);
        ImmutableList.Builder<HaplotypeNode> nodeList = new ImmutableList.Builder<>();
        //Read in the file
        try(BufferedReader reader = Utils.getBufferedReader(fastaFile)) {
            String currentLine = "";
            while((currentLine = reader.readLine())!=null) {
                String[] infoLineSplit = currentLine.split(";");
                ReferenceRange currentRange = getCurrentRange(infoLineSplit[0],positionRangeToIdMap);

                //create a taxa list for all the haplotypes which were part of this consensus
                TaxaList taxaList = createTaxaList(infoLineSplit[1]);

                currentLine = reader.readLine();
                HaplotypeSequence sequence = createHaplotypeSequence(currentRange,currentLine);

                HaplotypeNode currentHaplotypeNode = createHaplotypeNode(sequence,taxaList);

                nodeList.add(currentHaplotypeNode);
            }
        }
        catch(Exception e) {
            myLogger.warn("Error parsing fasta file: "+fastaFile+": "+e.toString());
            e.printStackTrace();
        }

        return nodeList.build();
    }

    private static RangeMap<Position,Integer> createIdMapping(String intervalFile) {
        TreeRangeMap<Position,Integer> positionToRangeMap = TreeRangeMap.create();
        try(BufferedReader reader = Utils.getBufferedReader(intervalFile)){
            //id, chr, stPos, endPos
            String currentLine = "";
            while((currentLine=reader.readLine())!=null) {
                String[] currentLineSplit = currentLine.split(",");
                positionToRangeMap.put(Range.closed(Position.of(currentLineSplit[1],Integer.parseInt(currentLineSplit[2])),
                        Position.of(currentLineSplit[1],Integer.parseInt(currentLineSplit[3]))),
                        Integer.parseInt(currentLineSplit[0]));
            }
        }catch(Exception e) {
            myLogger.warn("Error building id map:"+e.getMessage());
        }

        return positionToRangeMap;
    }

    private static ReferenceRange getCurrentRange(String infoLine,RangeMap<Position, Integer> positionToIdMap) {
        //Parse out the information for the anchor
        String[] infoLineSplit = infoLine.substring(1).split(":");
        Chromosome chr = Chromosome.instance(infoLineSplit[0]);
        int startPos = Integer.parseInt(infoLineSplit[1]);
        int endPos = Integer.parseInt(infoLineSplit[2]);
        Map.Entry<Range<Position>,Integer> mapEntry = positionToIdMap.getEntry(Position.of(chr.getName(),startPos));
        int anchorIdStart = positionToIdMap.get(Position.of(chr.getName(),startPos));
        int anchorIdEnd = positionToIdMap.get(Position.of(chr.getName(),endPos));
        if(anchorIdStart != anchorIdEnd) {
            myLogger.error("Anchor ids do not match for the start and end positions."+anchorIdStart);
            throw new IllegalStateException("Anchor ids do not match for the start and end positions.");
        }

//        return new ReferenceRange("B73",chr,startPos,endPos,anchorIdStart);
        return new ReferenceRange("B73",chr,mapEntry.getKey().lowerEndpoint().getPosition(),mapEntry.getKey().upperEndpoint().getPosition(),anchorIdStart);
    }

    private static TaxaList createTaxaList(String nonParsedTaxaString) {
        String[] taxaStringSplit = nonParsedTaxaString.split(":");
        TaxaListBuilder builder = new TaxaListBuilder();
        builder.addAll(taxaStringSplit);
        return builder.build();
    }

    private static HaplotypeSequence createHaplotypeSequence(ReferenceRange anchorRange, String rawSequence) {
        return HaplotypeSequence.getInstance(rawSequence, anchorRange, 1.0, AnchorDataPHG.getChecksumForString(rawSequence,"MD5"));
    }

    private static HaplotypeNode createHaplotypeNode(HaplotypeSequence sequence, TaxaList taxaList) {
        return new HaplotypeNode(sequence,taxaList);
    }

}
