package net.maizegenetics.pangenome.hapCalling;

import com.google.common.collect.Multimap;
import net.maizegenetics.pangenome.api.HaplotypeNode;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.plugindef.GeneratePluginCode;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.util.Utils;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedWriter;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collector;

/**
 * Plugin to export the Multimap<TaxonName,HaplotypeNode> exported by a Path finding algorithm to a directory of text
 * files Created by zrm22 on 10/13/17.
 */
public class ExportHaplotypePathToFilePlugin extends AbstractPlugin {

    private static final Logger myLogger = Logger.getLogger(ExportHaplotypePathToFilePlugin.class);

    private PluginParameter<String> myOutputFileDirectory = new PluginParameter.Builder<>("outputFileDirectory", null, String.class)
            .description("Output file directory")
            .required(true)
            .outDir()
            .build();
    private PluginParameter<String> myRefVersion = new PluginParameter.Builder<>("refVersion", null, String.class)
            .description("Name of reference version as stored in the DB table genome_inteval_versions")
            .required(true)
            .build();
    private PluginParameter<String> myHapCountMethod = new PluginParameter.Builder<>("hapCountMethod", null, String.class)
            .description("Method name used to create haplotype counts, as stored in db table methods")
            .required(true)
            .build();
    private PluginParameter<String> myPathMethod = new PluginParameter.Builder<>("pathMethod", null, String.class)
            .description("Name of method used to create paths, as it should appear in the paths table")
            .required(true)
            .build();
    private PluginParameter<String> myPathMethodDetails = new PluginParameter.Builder<>("pathMethodDetails", null, String.class)
            .description("Description of method used to create paths - optional")
            .build();


    public ExportHaplotypePathToFilePlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    public DataSet processData(DataSet input) {
        List<Datum> temp = input.getDataOfType(Multimap.class);
        if (temp.size() != 1) {
            throw new IllegalArgumentException("ExportHaplotypePathToFilePlugin: processData: must input one paths map");
        }
        myLogger.info("Extracting paths from previous plugin");
        Multimap<String, HaplotypeNode> paths = (Multimap<String, HaplotypeNode>) temp.get(0).getData();

        myLogger.info("Exporting paths to text file");
        exportPathsToTextFiles(paths);
        return null;
    }

    /**
     * Method to export each taxa into a path file
     *
     * @param paths
     */
    private void exportPathsToTextFiles(Multimap<String, HaplotypeNode> paths) {

        //loop through each taxa name that we have a path for
        for (String taxonName : paths.keySet()) {
            myLogger.debug("Processing Taxon:" + taxonName);
            try (BufferedWriter writer = Utils.getBufferedWriter(outputFileDirectory() + "/" + taxonName + "_path.txt")) {
                // Write header data - necessary if this file is used to load the DB paths table.
                writer.write("#version=" + refVersion() + "\n");
                writer.write("#hapCountMethod=" + hapCountMethod() + "\n");
                writer.write("#pathMethod=" + pathMethod() + "\n");
                if (pathMethodDetails() != null) {
                    writer.write("#pathMethodDetails=" + pathMethodDetails() + "\n");
                } else {
                    writer.write("#pathMethodDetails=none\n");
                }
                
                myLogger.debug("Extracting the haplotypeIds");
                SortedSet<Integer> sortedIdSet = paths.get(taxonName).stream()
                        .map(haplotypeNode -> haplotypeNode.id())
                        .filter(hapId -> hapId != -1)
                        .collect(Collector.of(TreeSet::new,
                                (set, hapId) -> set.add(hapId),
                                (leftSet, rightSet) -> {
                                    leftSet.addAll(rightSet);
                                    return leftSet;
                                }));
                myLogger.debug("Writing out each hapId to the textFile");
                for (Integer hapId : sortedIdSet) {
                    writer.write("" + hapId);
                    writer.write("\n");
                }

            } catch (Exception e) {
                throw new IllegalStateException("ExportHaplotypePathToFilePlugin: exportPathsToTextFiles: error opening up the file");
            }
        }

    }

    
//    public static void main(String[] args) {
//        GeneratePluginCode.generate(ExportHaplotypePathToFilePlugin.class);
//    }
    
    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {
        return "Paths to textFile";
    }

    @Override
    public String getToolTipText() {
        return "Paths to textFile";
    }

    /**
     * Output file directory
     *
     * @return Output File Directory
     */
    public String outputFileDirectory() {
        return myOutputFileDirectory.value();
    }

    /**
     * Set Output File Directory. Output file directory
     *
     * @param value Output File Directory
     *
     * @return this plugin
     */
    public ExportHaplotypePathToFilePlugin outputFileDirectory(String value) {
        myOutputFileDirectory = new PluginParameter<>(myOutputFileDirectory, value);
        return this;
    }
    
    /**
     * Name of reference version as stored in the DB table
     * genome_inteval_versions
     *
     * @return Ref Version
     */
    public String refVersion() {
        return myRefVersion.value();
    }

    /**
     * Set Ref Version. Name of reference version as stored
     * in the DB table genome_inteval_versions
     *
     * @param value Ref Version
     *
     * @return this plugin
     */
    public ExportHaplotypePathToFilePlugin refVersion(String value) {
        myRefVersion = new PluginParameter<>(myRefVersion, value);
        return this;
    }

    /**
     * Method name used to create haplotype counts, as stored
     * in db table methods
     *
     * @return Hap Counts Method
     */
    public String hapCountMethod() {
        return myHapCountMethod.value();
    }

    /**
     * Set Hap Count Method. Method name used to create haplotype
     * counts, as stored in db table methods
     *
     * @param value Hap Counts Method
     *
     * @return this plugin
     */
    public ExportHaplotypePathToFilePlugin hapCountMethod(String value) {
        myHapCountMethod = new PluginParameter<>(myHapCountMethod, value);
        return this;
    }

    /**
     * Name of method used to create paths, as it should appear
     * in the paths table
     *
     * @return Path Method
     */
    public String pathMethod() {
        return myPathMethod.value();
    }

    /**
     * Set Path Method. Name of method used to create paths,
     * as it should appear in the paths table
     *
     * @param value Path Method
     *
     * @return this plugin
     */
    public ExportHaplotypePathToFilePlugin pathMethod(String value) {
        myPathMethod = new PluginParameter<>(myPathMethod, value);
        return this;
    }

    /**
     * Description of method used to create paths - optional
     *
     * @return Path Method Details
     */
    public String pathMethodDetails() {
        return myPathMethodDetails.value();
    }

    /**
     * Set Path Method Details. Description of method used
     * to create paths - optional
     *
     * @param value Path Method Details
     *
     * @return this plugin
     */
    public ExportHaplotypePathToFilePlugin pathMethodDetails(String value) {
        myPathMethodDetails = new PluginParameter<>(myPathMethodDetails, value);
        return this;
    }

}
