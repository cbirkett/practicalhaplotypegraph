package net.maizegenetics.pangenome.hapCalling;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.primitives.Ints;
import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TLongObjectHashMap;
import gnu.trove.set.TLongSet;
import gnu.trove.set.hash.TLongHashSet;
import htsjdk.variant.variantcontext.VariantContext;
import net.maizegenetics.dna.BaseEncoder;
import net.maizegenetics.dna.snp.NucleotideAlignmentConstants;
import net.maizegenetics.pangenome.api.HaplotypeGraph;
import net.maizegenetics.pangenome.api.HaplotypeNode;
import net.maizegenetics.pangenome.api.ReferenceRange;
import net.maizegenetics.plugindef.*;
import net.maizegenetics.util.Tuple;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * author edbuckler
 *
 */

@Deprecated
public class IndexHaplotypeKmersPlugin extends AbstractPlugin {
    private static final Logger myLogger = Logger.getLogger(IndexHaplotypeKmersPlugin.class);
    private PluginParameter<String> myKmerMapFile = new PluginParameter.Builder<>("kmerMapFile", null, String.class)
            .outFile()
            .required(true)
            .description("Binary file of kmer and haplotype ids")
            .build();
    private PluginParameter<String> myDuplicateSetFile = new PluginParameter.Builder<>("duplicateSetFile", null, String.class)
            .outFile()
            .required(false)
            .description("Binary file of duplicate kmers")
            .build();
    private PluginParameter<Integer> kmerSize = new PluginParameter.Builder<>("kmerSize", 32, Integer.class)
            .required(false)
            .description("kmer size for indexing genome. Maximum size is 32. Use the default of 32 unless you know what you are doing.")
            .build();


    public IndexHaplotypeKmersPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    public DataSet processData(DataSet input) {
        List<Datum> temp = input.getDataOfType(HaplotypeGraph.class);
        if (temp.size() != 1) {
            throw new IllegalArgumentException("ImportHaplotypePathFilePlugin: processData: must input one HaplotypeGraph: " + temp.size());
        }
        HaplotypeGraph graph = (HaplotypeGraph) temp.get(0).getData();
        
        //Create a map of all ranges to haplotypeIDs, which can be used to determine if a kmer is diagnostic for multiple nodes
        TLongObjectMap<int[]> kmerIndex = indexHaplotypeGraphPrimitives(graph);
        try {
            serializeMapToFile(myKmerMapFile.value(),kmerIndex);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        String phgName = temp.get(0).getName();
        return new DataSet(new Datum("Kmer_Index_" + phgName, kmerIndex, "Primitive mapping of Kmer to haplotypeNodeId."), this);

    }


    /**
     * Create map of the kmers to an array of the haplotypes ids with the kmer
     * @param graph base haplotype graph to index
     * @return trove map kmer -> int[haplotypeIds]
     */
    private TLongObjectMap<int[]> indexHaplotypeGraphPrimitives(HaplotypeGraph graph) {
        //the resulting map
        TLongObjectMap<int[]> kmerMap = new TLongObjectHashMap<>(70_000);
        //once duplicates are identified they will be excluded
        //duplicates include kmers found in multiple reference ranges
        TLongSet duplicateKmerSet=new TLongHashSet();

        Set<ReferenceRange> referenceRanges = graph.referenceRanges();
        int rangeCount = 0; //use this to monitor memory usage
        for (ReferenceRange currentReferenceRange : referenceRanges) {
            if (rangeCount++ % 1000 == 0) {
                ((TLongObjectHashMap<int[]>) kmerMap).compact();
                for (int i = 0; i < 3; i++) System.gc(); 
                Runtime myRuntime = Runtime.getRuntime();
                long totalmem = myRuntime.totalMemory();
                long freemem = myRuntime.freeMemory();
                String msg = String.format("at range count = %d: Memory used = %d out of %d total", rangeCount, totalmem - freemem, totalmem);
                myLogger.info(msg);
                myLogger.info("kmermap size = " + kmerMap.size());
            }

            List<HaplotypeNode> nodesAtThisRefRange = graph.nodes(currentReferenceRange);

            //TLongObjectMap currentRefRangeKmerIndex = new TLongObjectHashMap();
            Multimap<Long,Integer> currentRefRangeKmerMap=HashMultimap.create();
            //Go Through each node in this reference range
            for (HaplotypeNode currentHaplotypeNode : nodesAtThisRefRange) {
                //extract kmers add collect them into the current refRanges Kmer mapping
                extractKmers(currentHaplotypeNode, currentRefRangeKmerMap, duplicateKmerSet, kmerSize.value());
            }

            //Find kmers found in currentRangeKmer map and prior kmers
            Set<Long> newDuplicates= currentRefRangeKmerMap.keySet().stream()
                    .filter(kmerMap::containsKey)
                    .collect(Collectors.toSet());

            //once we find a duplicate within a range remove it
            newDuplicates.stream().forEach(kmer -> {
                kmerMap.remove(kmer);
                duplicateKmerSet.add(kmer);
                currentRefRangeKmerMap.removeAll(kmer);
            });

            //add all the unique kmers to the kmerMap
            currentRefRangeKmerMap.asMap().forEach((kmer,haps) -> {
                kmerMap.put(kmer,Ints.toArray(haps));
            });

        }
        ((TLongObjectHashMap<int[]>) kmerMap).compact();
        printStats(kmerMap, duplicateKmerSet);
        return kmerMap;
    }

    /**
     * Adds the kmers for current haplotype node to the currentRefRangeKmerMap, while ignoring the known duplicates
     * @param currentNode
     * @param currentRefRangeKmerMap
     * @param duplicateSet
     * @param kmerSize
     */
    private void extractKmers(HaplotypeNode currentNode, Multimap<Long,Integer> currentRefRangeKmerMap,
                              TLongSet duplicateSet, int kmerSize) {
        String sequence = currentNode.haplotypeSequence().sequence();
        for (int i = 0; i < sequence.length() - kmerSize; i++) {
            //get the byte values for the subsequence
            byte[] subSequence = NucleotideAlignmentConstants.convertHaplotypeStringToAlleleByteArray(sequence.substring(i, i + kmerSize));
            long seqAsLong = BaseEncoder.getLongSeqFromByteArray(Arrays.copyOfRange(subSequence, 0, subSequence.length));//Might not need to copy, TODO Test this
            //exclude all kmers previously seen
            if(duplicateSet.contains(seqAsLong)) continue;
            currentRefRangeKmerMap.put(seqAsLong,currentNode.id());
        }
    }

    private void extractKmersJava(HaplotypeNode currentNode, Multimap<Long,Integer> currentRefRangeKmerMap,
            Set<Long> duplicateSet, int kmerSize) {
        String sequence = currentNode.haplotypeSequence().sequence();
        for (int i = 0; i < sequence.length() - kmerSize; i++) {
            //get the byte values for the subsequence
            byte[] subSequence = NucleotideAlignmentConstants.convertHaplotypeStringToAlleleByteArray(sequence.substring(i, i + kmerSize));
            long seqAsLong = BaseEncoder.getLongSeqFromByteArray(Arrays.copyOfRange(subSequence, 0, subSequence.length));//Might not need to copy, TODO Test this
            //exclude all kmers previously seen
            if(duplicateSet.contains(seqAsLong)) continue;
            currentRefRangeKmerMap.put(seqAsLong,currentNode.id());
        }
    }

    // Assembly processing stores the List<VariantContext>
    private void serializeMapToFile(String kmerFile, Object kmerMap) throws IOException {
        FileOutputStream fileOutputStream=new FileOutputStream(new File(kmerFile));
        ObjectOutputStream objectStream = new ObjectOutputStream(fileOutputStream);
        objectStream.writeObject(kmerMap);
        objectStream.close();
        fileOutputStream.close();
    }

    private void addToMap(TLongObjectMap<int[]> kmerMap, int[] duplicateList, long seqL, int id, int maxNumHaplotypes) {
        if (kmerMap.containsKey(seqL)) {
            int[] hapIds = kmerMap.get(seqL);

            if (hapIds == duplicateList) {
                //skip the rest as it means we have already marked it as missing.
            } else {
                int indexOfId = Ints.indexOf(hapIds, id);
                if (indexOfId == -1) {
                    //currently not in the list so we should add
                    //check to see if the list is full
                    int indexOfZero = Ints.indexOf(hapIds, 0);
                    if (indexOfZero != -1) {
                        hapIds[indexOfZero] = id;
                    }

//                    if (indexOfZero == hapIds.length - 1) {
//                        kmerMap.put(seqL, duplicateList);
//                    }
//                    else {
//                        hapIds[indexOfZero] = id;
//                    }
                }
            }
        } else {
            int[] listOfHapIds = new int[maxNumHaplotypes];
            listOfHapIds[0] = id;
            //This handles the reverse compliment as they share the same array object
            kmerMap.put(seqL, listOfHapIds);
            kmerMap.put(BaseEncoder.getReverseComplement(seqL), listOfHapIds);
        }
    }

    private void addAllKmersToFullMap(TLongObjectMap kmerMap, TLongObjectMap nodeKmerMap, int[] duplicateSet) {
        for (long currentKmer : nodeKmerMap.keys()) {
            if (kmerMap.containsKey(currentKmer)) {
                //set to duplicate as we have seen this kmer in another reference range
                kmerMap.put(currentKmer, duplicateSet);
            } else {
                kmerMap.put(currentKmer, nodeKmerMap.get(currentKmer));
            }
        }
    }

    private void printStats(TLongObjectMap<int[]> kmerMap, TLongSet duplicateSet) {
        myLogger.info("Number of kmers: " + kmerMap.keys().length);
        System.out.println("Number of kmers: " + kmerMap.keys().length);
        int sumOfNodes = 0;
        for (long kmer : kmerMap.keys()) {
            int[] hapIds = (int[]) kmerMap.get(kmer);
            sumOfNodes += hapIds.length;
        }
        double avgNodesPerKmer = (double) sumOfNodes / kmerMap.keys().length;
        myLogger.info("Number Of duplicate Kmers:" + duplicateSet.size());
        myLogger.info("AvgNumber of nodes to each kmer:" + avgNodesPerKmer);

        System.out.println("Number Of duplicate Kmers:" + duplicateSet.size());
        System.out.println("AvgNumber of nodes to each kmer:" + avgNodesPerKmer);

    }


    @Override
    public String pluginUserManualURL() {
        //TODO
        return "https://bitbucket.org/tasseladmin/tassel-5-source/wiki/UserManual/Kinship/Missing";
    }

    @Override
    public ImageIcon getIcon() {
        URL imageURL = IndexHaplotypeKmersPlugin.class.getResource("/net/maizegenetics/analysis/images/missing.gif");
        if (imageURL == null) {
            return null;
        } else {
            return new ImageIcon(imageURL);
        }
    }

    @Override
    public String getButtonName() {
        return "Index Kmers";
    }

    @Override
    public String getToolTipText() {
        return "Create kmer index for haplotype counts";
    }

    // The following getters and setters were auto-generated.
    // Please use this method to re-generate.
    //
    public static void main(String[] args) {
        GeneratePluginCode.generate(IndexHaplotypeKmersPlugin.class);
    }

    /**
     * Convenience method to run plugin with one return object.
     */
    // TODO: Replace <Type> with specific type.
//    public <Type> runPlugin(DataSet input) {
//        return (<Type>) performFunction(input).getData(0).getData();
//    }

    /**
     * Text file to store haplotype scoring
     *
     * @return Kmer Map File
     */
    public String kmerMapFile() {
        return myKmerMapFile.value();
    }

    /**
     * Set Kmer Map File. Text file to store haplotype scoring
     *
     * @param value Kmer Map File
     *
     * @return this plugin
     */
    public IndexHaplotypeKmersPlugin kmerMapFile(String value) {
        myKmerMapFile = new PluginParameter<>(myKmerMapFile, value);
        return this;
    }

    /**
     * kmer size for indexing genome
     *
     * @return Kmer Size
     */
    public Integer kmerSize() {
        return kmerSize.value();
    }

    /**
     * Set Kmer Size. kmer size for indexing genome
     *
     * @param value Kmer Size
     *
     * @return this plugin
     */
    public IndexHaplotypeKmersPlugin kmerSize(Integer value) {
        kmerSize = new PluginParameter<>(kmerSize, value);
        return this;
    }

}
