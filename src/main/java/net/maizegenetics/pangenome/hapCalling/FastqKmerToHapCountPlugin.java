package net.maizegenetics.pangenome.hapCalling;

import java.awt.Frame;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.swing.ImageIcon;

import com.google.common.collect.*;
import org.apache.log4j.Logger;

import com.google.common.primitives.SignedBytes;

import htsjdk.samtools.fastq.FastqReader;
import htsjdk.samtools.fastq.FastqRecord;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import net.maizegenetics.dna.BaseEncoder;
import net.maizegenetics.dna.snp.NucleotideAlignmentConstants;
import net.maizegenetics.pangenome.api.HaplotypeGraph;
import net.maizegenetics.pangenome.api.HaplotypeNode;
import net.maizegenetics.pangenome.api.ReferenceRange;
import net.maizegenetics.pangenome.db_loading.LoadHaplotypeCountsTablePlugin;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.plugindef.GeneratePluginCode;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.prefs.TasselPrefs;
import net.maizegenetics.util.Utils;

/**
 * Generate inclusion/exclusion counts by
 * matching kmers generated from sequencer reads to kmers in the kmerMap. 
 * If the kmers from a read map to a single range, then any node in that range
 * that matches 75% or more of the read kmers
 * is considered included. Otherwise, it is considered excluded.
 * This method is not expected to work well with haplotypes that contain a lot of N's. 
 * 
 * Expected Input: 
 * one HaplotypeGraph
 * one kmerMap
 * The kmerMap is a TLongObjectMap<int[]> that was created by IndexHaplotypeKmersPlugin.
 * It can be provided in a DataSet as input or imported from a file containing a serialized kmerMap. 
 *  
 *  Output:
 *  This plugin returns no output. Instead, if myLoadDB is true, 
 *  it writes include and exclude counts to the database listed in the configFile.
 *  If an export file name is given it also writes the counts to that file.
 *  
 * @author peterbradbury
 *
 */
@Deprecated
public class FastqKmerToHapCountPlugin extends AbstractPlugin {
    Logger myLogger = Logger.getLogger(FastqKmerToHapCountPlugin.class);
    
    private PluginParameter<String> myConfigFile = new PluginParameter.Builder<>("configFile", null, String.class)
            .inFile()
            .required(true)
            .description("DB Config File containing properties host,user,password,DB and DBtype where DBtype is either sqlite or postgres.")
            .build();

    private PluginParameter<String> myKmerFile = new PluginParameter.Builder<>("kmerFile", null, String.class)
            .inFile()
            .required(false)
            .description("Kmer map file. The plugin expects either this file or a data set created by IndexHaplotypeKmersPlugin.")
            .guiName("Kmer File")
            .build();

    private PluginParameter<String> myReadDir = new PluginParameter.Builder<>("readDirectory", null, String.class)
            .inDir()
            .required(true)
            .description("Directory containing the sequence read files to be process. Any files ending in .fastq or .fq will be processed.")
            .build();

    private PluginParameter<String> myExportHaplotypeFile = new PluginParameter.Builder<>("exportHaploFile", null, String.class)
            .outFile()
            .required(false)
            .description("Text file to store haplotype scoring")
            .build();

    private PluginParameter<String> myKmerPrefix = new PluginParameter.Builder<>("kmerPrefix", null, String.class)
            .required(false)
            .description("Kmer Prefix Character")
            .build();

    private PluginParameter<String> myMethod = new PluginParameter.Builder<>("method", null, String.class)
            .required(true)
            .description("The name of method used to create haplotype counts, for the haplotype_counts table.")
            .build();

    private PluginParameter<Boolean> myLoadDb = new PluginParameter.Builder<>("loadDb", true, Boolean.class)
            .required(false)
            .description("Whether to populate the haplotype_counts table - may be false when testing.")
            .build();

    private PluginParameter<String> myExportDebugReadDir = new PluginParameter.Builder<>("exportDebugReadDir", null, String.class)
            .outDir()
            .required(false)
            .description("Text file to store List of reads which did not hit kmers")
            .build();

    public FastqKmerToHapCountPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }
    
    public FastqKmerToHapCountPlugin() {
        super(null, false);
    }
    
    
    private static final int kmerLength = 32;
    private static final int kmerStep = 16;
    private static final double minProportionKmersMatched = 0.75;
    private static final double minProportionMaxCount = 0.9;
    
    private HaplotypeGraph myGraph;
    private Map<Integer, ReferenceRange> hapidRangeMap;
    
    @Override
    public DataSet processData(DataSet input) {
        
        myLogger.info("starting FastqKmerToHapCountPlugin");
        List<Datum> inputList = input.getDataOfType(HaplotypeGraph.class);
        if (inputList.size() != 1) {
            throw new IllegalArgumentException("FastqKmerToHapCountPlugin requires one HaplotypeGraph as input. " + inputList.size() + " supplied.");
        }
        myGraph = (HaplotypeGraph) inputList.get(0).getData();
        
        //Input a TLongObjectMap or supply a kmer file name
        Long2ObjectMap<int[]> kmerMap;

        if (myKmerFile.value() != null) {
            myLogger.info("Reading kmerMap.");
            long start = System.currentTimeMillis();
            kmerMap = KmerUtils.importKmerToHapIdMapFromBinaryFile(myKmerFile.value());
            myLogger.info("kmerMap read into memory in " + (System.currentTimeMillis() - start) + " ms.");
        } else {
            List<Datum> temp = input.getDataOfType(Long2ObjectMap.class);
            if(temp.size() != 1) {
                throw new IllegalArgumentException("FastqToKmerCountPlugin: processData: must input one TLongObjectMap: " + temp.size());
            }
            kmerMap = (Long2ObjectMap<int[]>) temp.get(0).getData();
        }

        myLogger.info("kmerMapSize: " + kmerMap.keySet().size());
        //create the hapid -> refRange map
        hapidRangeMap = myGraph.nodeStream().collect(Collectors.toMap(node -> node.id(), node -> node.referenceRange()));
        
        
        //get a list of files in myReadDir ending in .fq or .fastq
        File fastqDir = new File(myReadDir.value());
        File[] myReadFiles = fastqDir.listFiles((dir, name) -> {
            if (name.startsWith(".")) {
                return false;
            }
            return name.endsWith(".fq") || name.endsWith(".fastq")
                    || name.endsWith(".fq.gz") || name.endsWith(".fastq.gz");
        });
        
        for (File readFile : myReadFiles) {
            //Load in the read file
            int posOfDotf = readFile.getName().lastIndexOf(".f");
            String taxonName = readFile.getName().substring(0, posOfDotf);
            
            FastqReader reader = new FastqReader(readFile);
            BufferedWriter debugWriter = (exportDebugReadDir() == null)? null : Utils.getBufferedWriter(exportDebugReadDir()+"debug_"+taxonName);

            long start = System.currentTimeMillis();
            myLogger.info("Processing fastqFile:"+readFile.getName());
            List<Multiset<HaplotypeNode>> hapidCounts = processFastqFileMultithread(kmerMap,reader);
            myLogger.info("fastq file processes in " + (System.currentTimeMillis() - start) + " ms.");
            
            int haplotypesIncluded = hapidCounts.get(0).elementSet().size();
            int haplotypesExcluded = hapidCounts.get(1).elementSet().size();
            int totalInclusions = hapidCounts.get(0).entrySet().stream().mapToInt(ent -> ent.getCount()).sum();
            int totalExclusions = hapidCounts.get(1).entrySet().stream().mapToInt(ent -> ent.getCount()).sum();
            
            myLogger.info("Counts for " + readFile.getPath() + ":");
            myLogger.info("haplotypes included = " + haplotypesIncluded);
            myLogger.info("haplotypes excluded = " + haplotypesExcluded);
            myLogger.info("total inclusions = " + totalInclusions);
            myLogger.info("total exclusions = " + totalExclusions);
            
            DataSet myDataSet = new DataSet(new Datum[]{
                    new Datum("PerfectHitsSet", hapidCounts.get(0), null),
                    new Datum("ExclusionHitsSet", hapidCounts.get(1), null),
                    new Datum("Taxon", taxonName, null),
                    }, null);

            if (myExportHaplotypeFile.value() != null) {
                String augmentedExportName = myExportHaplotypeFile.value() + "_" + taxonName + ".txt";
                exportHitsToFile(hapidCounts.get(0), hapidCounts.get(1), augmentedExportName, myMethod.value(), 
                        taxonName, readFile.getPath());
            }

            // load database unless user has specified otherwise
            if (myLoadDb.value()) {
                // Create a methodDetails string based on plugin parameters, load data to haplotype_counts table
                String methodDetails = "FastqKmerToHapCountPlugin";
                new LoadHaplotypeCountsTablePlugin()
                    .method(myMethod.value())
                    .methodDetails(methodDetails)
                    .configFile(myConfigFile.value())
                    .fastqFile(readFile.getPath())
                    .performFunction(myDataSet);
            }

            if(debugWriter!=null) {
                try {
                    debugWriter.close();
                }
                catch(Exception e) {
                    throw new IllegalStateException("Unable To Close Debug Writer",e);
                }
            }

        }
 
        return null;
    }

    private List<Multiset<HaplotypeNode>> processFastqFile(Long2ObjectMap<int[]> kmerMap, FastqReader reader, BufferedWriter debugWriter) {

        Multiset<HaplotypeNode> includeNodes = HashMultiset.create();
        Multiset<HaplotypeNode> excludeNodes = HashMultiset.create();
        List<Multiset<HaplotypeNode>> includedExcludedNodes = Arrays.asList(includeNodes, excludeNodes);

        //loop through the read file for each sequence
        for(FastqRecord currentRecord : reader) {
            //loop through each read in batches of 32bps
            //retrieve the list of haplotypeNodeIds for each 32bp kmer
            //Loop through each kmer and add up how many times a node Id is seen
            //If #kmers containing a nodeId == number of non-filtered kmers it should be counted as an inclusion
            //else do some filtering
            //Idea if #kmers containing a nodeId > 50% but not == to number of non-filtered kmers it should be an exclusion
            //                List<Integer> hapIdCountsForRead = extractInclusionAndExclusionFromKmers(kmerMap,currentRecord.getReadBases(), 32, 50);
            if(SignedBytes.min(currentRecord.getBaseQualities()) > 10 && currentRecord.getReadLength()>60) {
                Optional<List<List<HaplotypeNode>>> optHapIdCounts = mapReadKmersToHapids(kmerMap, currentRecord.getReadString());
                if (optHapIdCounts.isPresent()) {
                    includeNodes.addAll(optHapIdCounts.get().get(0));
                    excludeNodes.addAll(optHapIdCounts.get().get(1));
                }
                else if(debugWriter != null) {
                    try {
                        debugWriter.write("NoKmerHit\t" + currentRecord.getReadName()+"\t"+currentRecord.getReadString()+"\n");
                    }
                    catch(Exception e) {
                        throw new IllegalStateException("Error writing debug:KmerHit",e);
                    }
                }
            }
            else if(debugWriter != null) {
                try {
                    debugWriter.write("FiltQOrLength\t" + currentRecord.getReadName()+"\t"+currentRecord.getReadString()+"\n");
                }
                catch(Exception e) {
                    throw new IllegalStateException("Error writing debug:Filter",e);
                }
            }

        }

        return includedExcludedNodes;
    }

    private List<Multiset<HaplotypeNode>> processFastqFileMultithread(Long2ObjectMap<int[]> kmerMap, FastqReader reader) {
        int batchSize = 1000;
        Multiset<HaplotypeNode> includeNodes = HashMultiset.create();
        Multiset<HaplotypeNode> excludeNodes = HashMultiset.create();
        List<Multiset<HaplotypeNode>> includedExcludedNodes = Arrays.asList(includeNodes, excludeNodes);
        
        int maximumPoolSize = Math.max(2, TasselPrefs.getMaxThreads());
        int corePoolSize = 2;
        long keepAliveTime = 5; //in seconds
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(250); 
        ExecutorService myExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.SECONDS, workQueue, new ThreadPoolExecutor.CallerRunsPolicy());
        BlockingQueue<List<List<HaplotypeNode>>> nodeQueue = new LinkedBlockingQueue<List<List<HaplotypeNode>>>();
        
        //multiple FastqRecordProcessors will generate a list of included and excluded haplotype nodes and put those on nodeQueue
        //a single MultisetAdder will take those lists off nodeQueue and add them to includedExcludedNodes
        Future<?> adderFuture = myExecutor.submit(new MultisetAdder(includedExcludedNodes, nodeQueue));
        
        //loop through the read file for each sequence
        int batchCount = 0;
        List<FastqRecord> recordList = new LinkedList<>();
        List<Future<?>> processorList = new LinkedList<>();
   
        for(FastqRecord currentRecord : reader) {
            //loop through each read in batches of 32bps
            //retrieve the list of haplotypeNodeIds for each 32bp kmer
            //Loop through each kmer and add up how many times a node Id is seen
            //If #kmers containing a nodeId == number of non-filtered kmers it should be counted as an inclusion
            //else do some filtering
            recordList.add(currentRecord);
            if (recordList.size() >= batchSize) {
                batchCount++;
                processorList.add(myExecutor.submit(new FastqRecordProcessor(recordList, kmerMap, nodeQueue, batchCount)));
                recordList = new ArrayList<>();
            }
        }

        //process any remaining records
        if (recordList.size() > 0) {
            batchCount++;
            processorList.add(myExecutor.submit(new FastqRecordProcessor(recordList, kmerMap, nodeQueue, batchCount)));
        }
        
        myLogger.info("Finished reading records from FastqReader in processFastqFileMultithread.");
        myLogger.info("Number of batches submitted = " + batchCount);
        
        //clean up once processing is complete
        try {
            for (Future<?> nextFuture : processorList) nextFuture.get();
            nodeQueue.put(new LinkedList<>());
        } catch (InterruptedException | ExecutionException e) {
            myLogger.error("processFastqFileMultithread interupted before it finished.");
            throw new IllegalStateException(e);
        } finally {
            myExecutor.shutdown();
        }
        
        try {
            myExecutor.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            //proceed as processing should be complete by now
        }

        return includedExcludedNodes;
    }
    
    private class MultisetAdder implements Runnable {
        final List<Multiset<HaplotypeNode>> nodeList;
        final BlockingQueue<List<List<HaplotypeNode>>> nodeQueue;
        
        MultisetAdder(List<Multiset<HaplotypeNode>> includeExcludeNodeList, BlockingQueue<List<List<HaplotypeNode>>> nodeQueue) {
            nodeList = includeExcludeNodeList;
            this.nodeQueue = nodeQueue;
        }
        
        @Override
        public void run() {
            int batchCount = 0;
            List<List<HaplotypeNode>> nodeCounts = new LinkedList<>();
            try {
                nodeCounts = nodeQueue.take();
            } catch (InterruptedException e) {
                myLogger.error("MultisetAdder interrupted before retrieving first batch.");
                throw new RuntimeException(e);
            }
            while (nodeCounts.size() > 0) {
                nodeList.get(0).addAll(nodeCounts.get(0));
                nodeList.get(1).addAll(nodeCounts.get(1));
                batchCount++;
//                myLogger.info("MultisetAdder processed " + batchCount + " batches.");
                try {
                    nodeCounts = nodeQueue.take();
                } catch (InterruptedException e) {
                    myLogger.error("MultisetAdder interrupted before finishing.");
                    throw new RuntimeException(e);
                }
            }
            myLogger.info("MultisetAdder stopped processing at " + batchCount + " batches.");
        }
    }
    
    private class FastqRecordProcessor implements Runnable {
        final List<FastqRecord> myRecordList;
        final Long2ObjectMap<int[]> myKmerMap;
        final BlockingQueue<List<List<HaplotypeNode>>> myNodeQueue; 
        
        FastqRecordProcessor(List<FastqRecord> recordList, Long2ObjectMap<int[]> kmerMap, BlockingQueue<List<List<HaplotypeNode>>> nodeQueue, int index) {
            myRecordList = recordList;
            myKmerMap = kmerMap;
            myNodeQueue = nodeQueue;
        }
        
        @Override
        public void run() {
            List<List<HaplotypeNode>> includedExcludedNodes = Arrays.asList(new LinkedList<>(), new LinkedList<>());
            for (FastqRecord record : myRecordList) {
                if (SignedBytes.min(record.getBaseQualities()) <= 10 || record.getReadLength() <= 60) continue;
                Multiset<Integer> hapIdCounts = TreeMultiset.create();

                int totalKmersExamined = 0;
                int kmersMatched = 0;
                String sequence = record.getReadString();
                byte[] fullReadByteArray = NucleotideAlignmentConstants.convertHaplotypeStringToAlleleByteArray(sequence);
                byte kmerPrefixByte = (kmerPrefix() != null) ? NucleotideAlignmentConstants.getNucleotideAlleleByte(kmerPrefix().charAt(0)): -1;
                for(int i = 0; i < sequence.length() - kmerLength; i++) {
                    if ((kmerPrefix() != null) && fullReadByteArray[i] != kmerPrefixByte) continue;

                    totalKmersExamined++;

                    long kmerEncoded =  BaseEncoder.getLongSeqFromByteArray(Arrays.copyOfRange(fullReadByteArray, i, i + kmerLength));

                    int[] kmerCounts = (int[]) myKmerMap.get(kmerEncoded);

                    if (kmerCounts != null) {
                        for (int hapId : kmerCounts) hapIdCounts.add(hapId);
                        kmersMatched++;
                        //Slide up the kmer by the step for speed efficiency.
                        i += kmerStep;
                    }
                }

                //what ranges do the hapids belong to?
                //if not equal to 1 stop and return
                Set<ReferenceRange> rangeIds = hapIdCounts.elementSet().stream().map(hapid -> hapidRangeMap.get(hapid)).collect(Collectors.toCollection(HashSet::new));
                if (rangeIds.size() != 1) continue;

                //sort hapid-count pairs, descending by count
                int[][] hapcount = hapIdCounts.entrySet().stream()
                        .map(ent -> new int[] {ent.getElement(), ent.getCount()})
                        .toArray(int[][]::new);

                Arrays.sort(hapcount, (a,b) -> b[1] - a[1]);

                //generate include/excludes for read, if any
                //for now the thresholds are arbitrary
                //do most of the kmers match?
                if (kmersMatched < minProportionKmersMatched * totalKmersExamined) continue;

                //count Nodes with the most matches as includes, others as excludes
                int maxCount = hapcount[0][1];
                double minCount = minProportionMaxCount * maxCount;
                List<Integer> includedHapIds = Arrays.stream(hapcount)
                        .filter(hc -> hc[1] > minCount)
                        .map(hc -> new Integer(hc[0]))
                        .collect(Collectors.toList());

                //get the nodes in this range, put those that are includes in the include list
                //put all others in the exclude list
                //if no nodes are on the exclude list, do not add hapids to include list
                
                List<HaplotypeNode> nodesInRange = myGraph.nodes(rangeIds.iterator().next());
                List<HaplotypeNode> includes = new LinkedList<>();
                List<HaplotypeNode> excludes = new LinkedList<>();
                for (HaplotypeNode node : nodesInRange) {
                    if (includedHapIds.contains(node.id())) includes.add(node);
                    else excludes.add(node);
                }

                if (includes.size() > 0 && excludes.size() > 0) {
                    includedExcludedNodes.get(0).addAll(includes);
                    includedExcludedNodes.get(1).addAll(excludes);
                }
            }
            //if no nodes included and no nodes excluded do not add includedExcluded nodes to queue
            if (includedExcludedNodes.get(0).size() > 0 && includedExcludedNodes.get(1).size() > 0) 
                myNodeQueue.add(includedExcludedNodes);
        }

    }
    
    private  Optional<List<List<HaplotypeNode>>> mapReadKmersToHapids(Long2ObjectMap<int[]> kmerMap, String sequence) {
        
        Multiset<Integer> hapIdCounts = TreeMultiset.create();
        List<List<HaplotypeNode>> includedExcludedNodes = Arrays.asList(new ArrayList<>(), new ArrayList<>());
        
        int totalKmersExamined = 0;
        int kmersMatched = 0;
        byte[] fullReadByteArray = NucleotideAlignmentConstants.convertHaplotypeStringToAlleleByteArray(sequence);
        byte kmerPrefixByte = (kmerPrefix() != null) ? NucleotideAlignmentConstants.getNucleotideAlleleByte(kmerPrefix().charAt(0)): -1;
        for(int i = 0; i < sequence.length() - kmerLength; i++) {
            if ((kmerPrefix() != null) && fullReadByteArray[i] != kmerPrefixByte) continue;

            totalKmersExamined++;

            long kmerEncoded =  BaseEncoder.getLongSeqFromByteArray(Arrays.copyOfRange(fullReadByteArray, i, i + kmerLength));

            int[] kmerCounts = (int[])kmerMap.get(kmerEncoded);
            
            if (kmerCounts != null) {
                for (int hapId : kmerCounts) hapIdCounts.add(hapId);
                kmersMatched++;
                //Slide up the kmer by the step for speed efficiency.
                i += kmerStep;
            }
        }
        
        //what ranges do the hapids belong to?
        //if not equal to 1 stop and return
        Set<ReferenceRange> rangeIds = hapIdCounts.elementSet().stream().map(hapid -> hapidRangeMap.get(hapid)).collect(Collectors.toCollection(HashSet::new));
        if (rangeIds.size() != 1) return Optional.empty();
        
        //sort hapid-count pairs, descending by count
        int[][] hapcount = hapIdCounts.entrySet().stream()
                .map(ent -> new int[] {ent.getElement(), ent.getCount()})
                .toArray(int[][]::new);
        
        Arrays.sort(hapcount, (a,b) -> b[1] - a[1]);
        
        //generate include/excludes for read, if any
        //for now the thresholds are arbitrary
        //do most of the kmers match?
        if (kmersMatched < minProportionKmersMatched * totalKmersExamined) return Optional.empty();
        
        //count Nodes with the most matches as includes, others as excludes
        int maxCount = hapcount[0][1];
        double minCount = minProportionMaxCount * maxCount;
        List<Integer> includedHapIds = Arrays.stream(hapcount)
                .filter(hc -> hc[1] > minCount)
                .map(hc -> new Integer(hc[0]))
                .collect(Collectors.toList());
        
        //get the nodes in this range, put those that are includes in the include list
        //put all others in the exclude list
        List<HaplotypeNode> nodesInRange = myGraph.nodes(rangeIds.iterator().next());
        for (HaplotypeNode node : nodesInRange) {
            if (includedHapIds.contains(node.id())) includedExcludedNodes.get(0).add(node);
            else includedExcludedNodes.get(1).add(node);
        }
        
        //if no nodes included or no nodes excluded return empty
        if (includedExcludedNodes.get(0).size() == 0 || includedExcludedNodes.get(1).size() == 0) return Optional.empty();
        return Optional.of(includedExcludedNodes);
    }

    private Object deserializeMapFromFile() throws IOException, ClassNotFoundException {
        FileInputStream fileOutputStream = new FileInputStream(new File(myKmerFile.value()));
        ObjectInputStream objectStream = new ObjectInputStream(fileOutputStream);
        Object obj = objectStream.readObject();
        objectStream.close();
        fileOutputStream.close();
        return obj;
    }

    private void exportHitsToFile(Multiset<HaplotypeNode> perfectHitSet, Multiset<HaplotypeNode> exclusionHitSet, String fileName,
            String method,  String taxonName, String readFile) {
        try (BufferedWriter bw = Utils.getBufferedWriter(fileName)) {
            // Write header data.  Needed if these files are used to load the DB table haplotype_counts
            bw.write("#method=" + method + "\n");
            bw.write("#taxon=" + taxonName + "\n");
            bw.write("#readfile=" + readFile + "\n");
            List<HaplotypeNode> toSort = new ArrayList<>(Sets.union(perfectHitSet.elementSet(), exclusionHitSet.elementSet()));
            toSort.sort(Comparator.comparingInt(HaplotypeNode::id));
            for (HaplotypeNode haplotypeNode : toSort) {
                bw.write(haplotypeNode.id() + "\t");
                bw.write(perfectHitSet.count(haplotypeNode) + "\t");
                bw.write(exclusionHitSet.count(haplotypeNode) + "\n");
            }
        } catch (IOException e) {
            myLogger.error("IOException in exportHitsToFile", e);
        }
    }

    @Override
    public ImageIcon getIcon() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getButtonName() {
        return "Kmer Hap Counter";
    }

    @Override
    public String getToolTipText() {
        return "Generate haplotype counts using Kmer method";
    }

    public static void main(String[] args) {
        GeneratePluginCode.generate(FastqKmerToHapCountPlugin.class);      
    }
    
    /**
     * DB Config File containing properties host,user,password,DB
     * and DBtype where DBtype is either sqlite or postgres.
     *
     * @return Config File
     */
    public String configFile() {
        return myConfigFile.value();
    }

    /**
     * Set Config File. DB Config File containing properties
     * host,user,password,DB and DBtype where DBtype is either
     * sqlite or postgres.
     *
     * @param value Config File
     *
     * @return this plugin
     */
    public FastqKmerToHapCountPlugin configFile(String value) {
        myConfigFile = new PluginParameter<>(myConfigFile, value);
        return this;
    }

    /**
     * Binary kmer map file. The plugin expects either this
     * file or a data set created by IndexHaplotypeKmersPlugin.
     *
     * @return Kmer File
     */
    public String kmerFile() {
        return myKmerFile.value();
    }

    /**
     * Set Kmer File. Binary kmer map file. The plugin expects
     * either this file or a data set created by IndexHaplotypeKmersPlugin.
     *
     * @param value Kmer File
     *
     * @return this plugin
     */
    public FastqKmerToHapCountPlugin kmerFile(String value) {
        myKmerFile = new PluginParameter<>(myKmerFile, value);
        return this;
    }

    /**
     * Directory containing the sequence read files to be
     * process. Any files ending in .fastq or .fq will be
     * processed.
     *
     * @return Read Directory
     */
    public String readDir() {
        return myReadDir.value();
    }

    /**
     * Set Read Directory. Directory containing the sequence
     * read files to be process. Any files ending in .fastq
     * or .fq will be processed.
     *
     * @param value Read Directory
     *
     * @return this plugin
     */
    public FastqKmerToHapCountPlugin readDir(String value) {
        myReadDir = new PluginParameter<>(myReadDir, value);
        return this;
    }

    /**
     * Text file to store haplotype scoring
     *
     * @return Export Haplo File
     */
    public String exportHaplotypeFile() {
        return myExportHaplotypeFile.value();
    }

    /**
     * Set Export Haplo File. Text file to store haplotype
     * scoring
     *
     * @param value Export Haplo File
     *
     * @return this plugin
     */
    public FastqKmerToHapCountPlugin exportHaplotypeFile(String value) {
        myExportHaplotypeFile = new PluginParameter<>(myExportHaplotypeFile, value);
        return this;
    }

    /**
     * Kmer Prefix Character
     *
     * @return Kmer Prefix
     */
    public String kmerPrefix() {
        return myKmerPrefix.value();
    }

    /**
     * Set Kmer Prefix. Kmer Prefix Character
     *
     * @param value Kmer Prefix
     *
     * @return this plugin
     */
    public FastqKmerToHapCountPlugin kmerPrefix(String value) {
        myKmerPrefix = new PluginParameter<>(myKmerPrefix, value);
        return this;
    }

    /**
     * Name of method used to create hap counts, for the haplotype_counts
     * table.
     *
     * @return Method
     */
    public String method() {
        return myMethod.value();
    }

    /**
     * Set Method. Name of method used to create hap counts,
     * for the haplotype_counts table.
     *
     * @param value Method
     *
     * @return this plugin
     */
    public FastqKmerToHapCountPlugin method(String value) {
        myMethod = new PluginParameter<>(myMethod, value);
        return this;
    }

    /**
     * Whether to populate the haplotype_counts table - often
     * false when testing.
     *
     * @return Load Db
     */
    public Boolean loadDb() {
        return myLoadDb.value();
    }

    /**
     * Set Load Db. Whether to populate the haplotype_counts
     * table - often false when testing.
     *
     * @param value Load Db
     *
     * @return this plugin
     */
    public FastqKmerToHapCountPlugin loadDb(Boolean value) {
        myLoadDb = new PluginParameter<>(myLoadDb, value);
        return this;
    }

    /**
     * Text file to store List of reads which did not hit
     * kmers
     *
     * @return Export Debug Read Dir
     */
    public String exportDebugReadDir() {
        return myExportDebugReadDir.value();
    }

    /**
     * Set Export Debug Read Dir. Text file to store List
     * of reads which did not hit kmers
     *
     * @param value Export Debug Read Dir
     *
     * @return this plugin
     */
    public FastqKmerToHapCountPlugin exportDebugReadDir(String value) {
        myExportDebugReadDir = new PluginParameter<>(myExportDebugReadDir, value);
        return this;
    }
}
