package net.maizegenetics.pangenome.hapCalling;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import net.maizegenetics.pangenome.api.GraphUtils;
import net.maizegenetics.pangenome.api.HaplotypeGraph;
import net.maizegenetics.pangenome.api.HaplotypeNode;
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import net.maizegenetics.pangenome.db_loading.PHGdbAccess;
import net.maizegenetics.plugindef.*;
import net.maizegenetics.util.DirectoryCrawler;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.Frame;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collector;
import java.util.stream.Stream;

/**
 * Plugin to import the haplotypePath files produced by ExportHaplotypePathToFilePlugin.  User can also provide a list of haplotype ids for a given path.
 * Created by zrm22 on 10/13/17.
 * Do not use with PathsToVCFPlugin because it no longer works with that.
 * @deprecated Use ImportDiploidPathPlugin instead.
 */
public class ImportHaplotypePathFilePlugin extends AbstractPlugin {

    private static final Logger myLogger = Logger.getLogger(ImportHaplotypePathFilePlugin.class);

    private PluginParameter<String> myInputFileDirectory = new PluginParameter.Builder<>("inputFileDirectory", null, String.class)
            .description("Input file directory")
            .required(false)
            .inDir()
            .build();

    private PluginParameter<String> myPathMethodName = new PluginParameter.Builder<>("pathMethodName", null, String.class)
            .description("Path Method Name Stored in the DB")
            .required(false)
            .build();


    public ImportHaplotypePathFilePlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    public DataSet processData(DataSet input) {
        List<Datum> temp = input.getDataOfType(HaplotypeGraph.class);
        if (temp.size() != 1) {
            throw new IllegalArgumentException("ImportHaplotypePathFilePlugin: processData: must input one HaplotypeGraph: " + temp.size());
        }
        Datum graphDatum = temp.get(0);
        HaplotypeGraph graph = (HaplotypeGraph) graphDatum.getData();

        Multimap<String, HaplotypeNode> haplotypePaths = null;

        //Check to see if we have a pathMethod name.  If so pull the paths from the DB.
        if (pathMethodName() != null) {
            haplotypePaths = importPathFromDB(graph, pathMethodName());
        } else if (inputFileDirectory() != null) { //Otherwise check for path files
            haplotypePaths = importTextFilesToPaths(graph, inputFileDirectory());
        } else { //If there is neither a method or a path directory, throw an error.
            throw new IllegalStateException("ImportHaplotypePathFilePlugin ERROR.  Need to specify either a pathMethodName or a inputFileDirectory.");
        }

        myLogger.debug(haplotypePaths.keySet().size() + "taxon paths return by ImportHaplotypePathFilePlugin.");
        return new DataSet(new Datum[]{graphDatum, new Datum("Paths", haplotypePaths, "Multimap containing a path for each taxon")}, this);
    }

    /**
     * Method to import the text files into a Multimap of taxon names to a list of HaplotypeNodes
     *
     * @param graph
     * @param inputFileDirectory
     *
     * @return
     */
    private Multimap<String, HaplotypeNode> importTextFilesToPaths(HaplotypeGraph graph, String inputFileDirectory) {
        Multimap<String, HaplotypeNode> haplotypePaths = HashMultimap.create();
        List<Path> pathFiles = DirectoryCrawler.listPaths("glob:*.txt", Paths.get(inputFileDirectory));

        for (Path currentTaxonPath : pathFiles) {
            try (Stream<String> fileLineStream = Files.lines(currentTaxonPath)) {
                //Get the taxonName remove _paths.txt
                int endPostion = currentTaxonPath.getFileName().toString().lastIndexOf("_");
                String taxonName = currentTaxonPath.getFileName().toString().substring(0, endPostion);
                //Get a sorted list of all the nodes we need from the graph
                SortedSet<Integer> nodeHitSet = fileLineStream
                        .filter(line -> !line.startsWith("#"))
                        .map(line -> Integer.parseInt(line))
                        .collect(Collector.of(TreeSet::new, (set, nodeId) -> set.add(nodeId), (leftSet, rightSet) -> {
                            leftSet.addAll(rightSet);
                            return leftSet;
                        }));

                //Extract the nodes we hit from the graph
                List<HaplotypeNode> nodeList = GraphUtils.nodes(graph, nodeHitSet);

                haplotypePaths.putAll(taxonName, nodeList);

            } catch (Exception e) {
                e.printStackTrace();
                throw new IllegalStateException("Error reading in taxonPathFile:" + currentTaxonPath.getFileName().toString());
            }
        }

        myLogger.info("Done with reading files.");
        return haplotypePaths;
    }

    /**
     * Method to import HaplotypePaths from a DB instead of from files.
     * @param graph
     * @param pathMethod
     * @return
     */
    private Multimap<String, HaplotypeNode> importPathFromDB(HaplotypeGraph graph, String pathMethod) {
        Multimap<String, HaplotypeNode> haplotypePaths = HashMultimap.create();

        try {
            Connection dbConnect = DBLoadingUtils.connection(false);
            PHGdbAccess phg = new PHGdbAccess(dbConnect);

            Map<String,byte[]> taxonToPathByteArray = phg.getTaxonPathsForMethod(pathMethod);
            myLogger.debug("Retrieved paths for " + taxonToPathByteArray.size() + " taxa from db.");
            taxonToPathByteArray.keySet().forEach(taxon -> {
                byte[] encodedPath = taxonToPathByteArray.get(taxon);

                int[] path = DBLoadingUtils.decodePathsArray(encodedPath);

                SortedSet<Integer> nodeHitSet = new TreeSet<>();

                for(int hapId : path) {
                    nodeHitSet.add(hapId);
                }

                List<HaplotypeNode> nodeList = GraphUtils.nodes(graph,nodeHitSet);
                haplotypePaths.putAll(taxon, nodeList);
            });

        }
        catch(Exception exc) {
            throw new IllegalStateException(exc);
        }
        myLogger.info("Done loading readMappings from the DB.");
        return haplotypePaths;
    }

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {
        return "Paths import";
    }

    @Override
    public String getToolTipText() {
        return "Paths import";
    }

    /**
     * Input file directory
     *
     * @return Input File Directory
     */
    public String inputFileDirectory() {
        return myInputFileDirectory.value();
    }

    /**
     * Set Input File Directory. Input file directory
     *
     * @param value Input File Directory
     *
     * @return this plugin
     */
    public ImportHaplotypePathFilePlugin inputFileDirectory(String value) {
        myInputFileDirectory = new PluginParameter<>(myInputFileDirectory, value);
        return this;
    }


    /**
     * Path Method Name Stored in the DB
     *
     * @return Path Method Name
     */
    public String pathMethodName() {
        return myPathMethodName.value();
    }

    /**
     * Set Path Method Name. Path Method Name Stored in the
     * DB
     *
     * @param value Path Method Name
     *
     * @return this plugin
     */
    public ImportHaplotypePathFilePlugin pathMethodName(String value) {
        myPathMethodName = new PluginParameter<>(myPathMethodName, value);
        return this;
    }
//    public static void main(String[] args) {
//             GeneratePluginCode.generate(ImportHaplotypePathFilePlugin.class);
//         }
}
