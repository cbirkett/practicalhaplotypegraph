package net.maizegenetics.pangenome;

import net.maizegenetics.util.Utils;

import java.io.BufferedReader;
import java.io.File;

/**
 * Created by terry on 3/19/17.
 */
public class GetFastaSequenceLengths {

    public static void main(String[] args) {

        String filename = args[0];
        if (!new File(filename).isFile()) {
            throw new IllegalArgumentException("no file: " + filename);
        }

        try (BufferedReader reader = Utils.getBufferedReader(filename)) {
            String line = reader.readLine();
            while (line != null) {
                if (!line.startsWith(">")) {
                    throw new IllegalStateException("line not start with >: " + line);
                }
                String[] tokens = line.split(" ");
                String chrStart = tokens[1];
                int start = Integer.parseInt(chrStart.split(":")[1]);
                line = reader.readLine();
                int len = 0;
                while ((line != null) && !line.startsWith(">")) {
                    line = line.trim();
                    len += line.length();
                    line = reader.readLine();
                }
                int end = start + len - 1;
                System.out.println(chrStart + "-" + end);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
