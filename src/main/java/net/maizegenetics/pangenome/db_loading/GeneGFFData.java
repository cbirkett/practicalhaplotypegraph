/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

/**
 * @author lcj34
 *
 */
public class GeneGFFData  implements Comparable<GeneGFFData>{

    private final  int start;
    private final  int end;
    private final  String name;

    public GeneGFFData(int start, int end, String name) {
        this.start = start;
        this.end = end;
        this.name = name;
    }
    public int start() {
        return start;
    }
    public int end() {
        return end;
    }
    public String name() {
        return name;
    }
    @Override
    public int compareTo(GeneGFFData that) {
        // this.compareTo(that) returns
        //   -1 if this < that
        //   0 if this == that
        //   1 positive int if this > that

        if (this.start < that.start) return -1;
        if (this.start > that.start) return 1;
        return 0;
    }
    @Override
    public String toString() {
        String gffDataString = "start: " + start + ", end: " + end + ", name: " + name;
        return gffDataString;
    }

}
