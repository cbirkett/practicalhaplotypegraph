/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

import net.maizegenetics.dna.map.Position;

/**
 * Class created to store Variant mapping data for populating the
 * variants PHG table.  It implements Comparable to enable
 * sorting an unordered list of variants by chrom/position
 * @author lcj34
 *
 */
public class VariantMappingData implements Comparable<VariantMappingData>{


    private final Position position; // position on the chrom where the variant begins
    private final int refAlleleID; // reference allele id from the allele db table
    private final int altAlleleID; // alternate allele id from the allele db table
    private final int ancID; // variant_id of the ancester/contig used for alignment
    private final int refDepth; // depth at reference allele
    private final int altDepth; // depth at alternate allele   
    private final boolean isReference; // whether the VC record is a refernce or variant record
    private final int refLen; // relevant when isReference = true
    private final byte isIndel; // 1 for is indel, 0 if not
    private final int otherData; // TBD


    public VariantMappingData( Position pos, int refAlleleID, int altAlleleID, 
            int refDepth, int altDepth, boolean isRef, int refLen, byte indel, int otherData) {
        this.position = pos;
        this.refAlleleID = refAlleleID;
        this.altAlleleID = altAlleleID;
        this.isReference = isRef;
        this.refDepth = refDepth;
        this.altDepth = altDepth;
        this.refLen = refLen;
        this.isIndel = indel;
        this.otherData = otherData;
        this.ancID = -1; // default to -1 until we know how it is used/populated
        
    }
    
    public Position position() {
        return position;
    }

    public int refAlleleID() {
        return refAlleleID;
    }
    public int altAlleleID() {
        return altAlleleID;
    }
 
    public boolean isReference() {
        return isReference;
    }
    public int refDepth() {
        return refDepth;
    }
    public int altDepth() {
        return altDepth;
    }
    public int refLen() {
        return refLen;
    }
    
    public byte isIndel() {
        return isIndel;
    }
    public int otherData() {
        return otherData;
    }
    
    public int ancID() {
        return ancID;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        VariantMappingData that = (VariantMappingData) obj;

        if (!(position().equals(that.position()))) return false;
        if (refAlleleID() != that.refAlleleID()) return false;
        if (altAlleleID() != that.altAlleleID()) return false;
        if (ancID() != that.ancID()) return false;
        if (isReference() != that.isReference()) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.position().hashCode();
        hash = 37 * hash + this.refAlleleID();
        hash = 37 * hash + this.altAlleleID();
        hash = 37 * hash + this.ancID();
        hash = 37 * hash + this.refLen;
        return hash;
    }

    @Override
    public int compareTo(VariantMappingData other) {
        // VariantMappingData objects are sorted based on chrom/position
        return position().compareTo(other.position());
    }
}
