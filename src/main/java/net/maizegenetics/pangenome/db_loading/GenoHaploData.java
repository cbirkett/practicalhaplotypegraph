/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

/**
 * Object to hold data needed to populate the genotypes and haplotypes tables.
 * These tables should be populated at the same time.  The data is stored
 * in a single object to facilitate making a single call to WGSSqlite
 * to populate both.
 * 
 * Hopefully combining them won't be a problem when we need to grab data later.
 * I assume we'd want data from both tables.
 * 
 * @author lcj34
 *
 */
public class GenoHaploData {
    private final int ploidy;
    private final boolean is_reference;
    private final String line_name;
    private final String line_data;
    private final boolean isPhasedAcrossGenes;
    private final boolean isPhasedAcrossChromosomes;
    private final int hapNumber;
    private final float phasingConfidence;

    public GenoHaploData(int ploidy, boolean is_ref,String line_name,String line_data, boolean isPhasedAcrossGenes, 
            boolean isPhasedAcrossChromosomes,int hapNumber, float phasingConfidence) {
        this.ploidy = ploidy;
        this.is_reference = is_ref;
        this.line_name = line_name;
        this.line_data = line_data;
        this.isPhasedAcrossGenes = isPhasedAcrossGenes;
        this.isPhasedAcrossChromosomes = isPhasedAcrossChromosomes;
        this.hapNumber = hapNumber;
        this.phasingConfidence = phasingConfidence;
    }
    
    public int ploidy() {
        return ploidy;
    }
    public boolean is_reference() {
        return is_reference;
    }
    public String line_name() {
        return line_name;
    }
    public String line_data() {
        return line_data;
    }
    public boolean isPhasedAcrossGenes() {
        return isPhasedAcrossGenes;
    }
    public boolean isPhasedAcrossChromosomes() {
        return isPhasedAcrossChromosomes;
    }
    public int hapNumber() {
        return hapNumber;
    }
    public float phasingConfidence() {
        return phasingConfidence;
    }
}
