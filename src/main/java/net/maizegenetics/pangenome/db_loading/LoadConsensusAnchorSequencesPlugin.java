/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

import java.awt.Frame;
import java.io.BufferedReader;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Range;

import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GeneralPosition;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.util.DirectoryCrawler;
import net.maizegenetics.util.Tuple;
import net.maizegenetics.util.Utils;

/**
 * This method takes an input file in fasta format and adds the sequences
 * to the data base.  The fasta file's idline must be of the format:
 *   > refChrom:refStartPos:refEndPos;TaxaName_hapNumber:TaxaName2_hapNumber:... gvcfFile
 * A semi-colon separates the ref coordinates from the list of taxa.  The ref coordinates
 * are used to identify the anchor to which the sequence belongs.  The taxa names indicate
 * the taxa (haplotypes) whose sequences at the indicated anchor collapse to this consensus.
 * 
 * A space separates the taxaNames from the gvcf file.  This should be gvcf name only. The
 * directory containing the gvcfs is a plugin parameter.
 * 
 * The names on the taxa list must consist of the genotype line name as stored in the 
 * genotypes table line_name field connect with an underscore to the hap number for this
 * line.  This allows identifying the haplotype id stored in the db.
 * 
 * INPUT:
 *   1.  the config file with specifics for connection to the db 
 *   2.  a fasta file of consensus anchor sequences
 *   3.  name of anchor version as stored in the db.  This identifies anchors is different sets were loaded.
 *   4.  name of method used to collapse anchors.  Can be an existing method or a new one.
 *   5.  String explaining the collapsing algorithm used.  Can be empty (which would imply this already exists)
 *   
 * OUTPUT:
 *   Data is written to the following PHG tables:
 *   1.  gamete_groups
 *   2.  gamete_haplotypes
 *   3.  haplotypes
 *   
 * NOTE: Files are processed in batches to facilitate being called from the RunHapCollapsePipeline,
 * which creates a separate file for every anchor/ref-range (37804 for maize).  
 * 
 * @author lcj34
 *
 */
@Deprecated
public class LoadConsensusAnchorSequencesPlugin extends AbstractPlugin {
    private static final Logger myLogger = Logger.getLogger(LoadConsensusAnchorSequencesPlugin.class);

    private PluginParameter<String> myInputFile = new PluginParameter.Builder<String>("inputFile", null, String.class).guiName("Input File").required(true).inDir()
            .description("Input fasta file, or directory containing fasta files with consensus sequences").build();
    private PluginParameter<String> myVersion = new PluginParameter.Builder<String>("version", null, String.class).guiName("Anchor Version").required(true)
            .description("Version name for this set of anchors as stored in anchor_versions table in db").build();
    private PluginParameter<String> vcfDir = new PluginParameter.Builder<String>("vcfDir", null, String.class).guiName("Consensus VCF Directory").required(true).inDir()
            .description("Directory that contains the vcf files for the consensus, including trailing / ").build();
    private PluginParameter<String> myCollapseMethod = new PluginParameter.Builder<String>("cmethod", null, String.class).guiName("Collapse Method").required(true)
            .description("Name of method used to collapse the anchors.  If it doesn't exist, will be added to methods table.").build();
    private PluginParameter<String> myMethodDetails = new PluginParameter.Builder<String>("method_details", null, String.class).guiName("Collapse Method Description")
            .description("Description of methods used to collapse the anchor sequences.").build();
    private PluginParameter<Boolean> isTestMethod = new PluginParameter.Builder<Boolean>("isTestMethod", false, Boolean.class)
            .description("Indication if the data to be loaded against a test method. Data loaded with test methods are not cached with the PHG ktor server")
            .required(false).build();
    
    public LoadConsensusAnchorSequencesPlugin() {
        super(null, false);
    }

    public LoadConsensusAnchorSequencesPlugin(Frame parentFrame) {
        super(parentFrame, false);
    }

    public LoadConsensusAnchorSequencesPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    public DataSet processData(DataSet input) {

        Long totalTime = System.nanoTime();
        Long time = System.nanoTime();

        // Create list of files, open database connection
        List<Path> infiles;
        File dirList = new File(inputFile());
        if (!(dirList.exists())) {
            throw new IllegalStateException("Input file or directory not found !!") ;
        }

        if (dirList.isDirectory()) {
            String inputFileGlob="glob:*{.fa}";
            infiles= DirectoryCrawler.listPaths(inputFileGlob, Paths.get(inputFile()).toAbsolutePath());
            if (infiles.size() < 1) {
                throw new IllegalStateException("no .fa files found in input directory !!");
            }
        } else {
            infiles=new ArrayList<>();
            Path filePath= Paths.get(inputFile()).toAbsolutePath(); 
            infiles.add(filePath);
        }

        Connection dbConnect = (Connection)input.getData(0).getData();
 
        if (dbConnect == null) {
           myLogger.error("LoadConsensusAnchorSequecesPlugin:  No DB CONNECTION supplied !");
           return null;
        }
        myLogger.info("LoadConsensusAnchorSequecesPlugin: have connection, create PHGdbAccess object");
        PHGDataWriter phg = new PHGdbAccess(dbConnect);               

        // Deprecated, so not changing to allow for test method type
        int methodId = phg.putMethod(collapseMethod(), DBLoadingUtils.MethodType.CONSENSUS_ANCHOR_SEQUENCE, pluginParameters());
 
        myLogger.info("LoadConsensusAnchorSequencesPlugin: methodID=" + methodId);
        time = System.nanoTime();

        Multimap<Position,Tuple<AnchorDataPHG,List<String>>> mergedConsensusDataMap = HashMultimap.create();
        
        int fileCount = 0;
        int totalFileCount = 0;
        for (Path file : infiles) {
            Multimap<Position,Tuple<AnchorDataPHG,List<String>>> fileConsensusDataMap = processConsensusFiles(  
                    file.toString());
            mergedConsensusDataMap.putAll(fileConsensusDataMap); // add this batch for loading
            fileCount++;
            totalFileCount++;
            // For maize, there are 37804 anchors.  When concatenated into files by chrom,
            // there are only 10 files, meaning an average of 3780 files per load. Use batches of 3000.
            if (fileCount == 3000) {
                phg.putConsensusSequences(mergedConsensusDataMap,methodId);
                fileCount = 0;
                // clear the map after loading data
                mergedConsensusDataMap.clear();
                myLogger.info("LoadConsensusANchorSequencesPlugin: number of fastas processed so far: " + totalFileCount);
            }
        }

        if (mergedConsensusDataMap.size() > 0) {
            phg.putConsensusSequences(mergedConsensusDataMap,  methodId);
        }
        
        myLogger.info("All files processed - time to process files: " + (System.nanoTime()-time)/1e9 + " seconds");
        try {
            ((PHGdbAccess)phg).close();
        } catch (Exception exc) {
            myLogger.error("LoadConsensusAnchorSequencesPlugin: error closing PHG db " + exc.getMessage());
        }

        myLogger.info("Finished processing: time to complete " + (System.nanoTime()-totalTime)/1e9 + " seconds");
        return null;
    }

    private  Multimap<Position,Tuple<AnchorDataPHG,List<String>>> processConsensusFiles( String consensusFile) {

        Multimap<Position,Tuple<AnchorDataPHG,List<String>>> consensusDataMap = HashMultimap.create();
        try (BufferedReader br = Utils.getBufferedReader(consensusFile)) {
            String fileLine;
            // It would be nice if we could pass a list<Integer> in the tuple,
            // a list of hapids values instead of the String name.  But TASSEL Utils.Tuple()
            // complains with error "Tuple: compareTo: neither x or y is Comparable types"
            // It accepts a List<String>
            
            StringBuilder sequenceSB = new StringBuilder();
            String[] coordinates = null;  
            List<String> haplotypes = new ArrayList<String>(); 
            String gvcfFile = null;
            while ((fileLine = br.readLine()) != null) {
                if (fileLine.startsWith(">"))  {
                    if (sequenceSB.length() > 0) {                       
                        // create anchorPHG data, add to map 
                        String chrom = coordinates[0];
                        Position startPos = new GeneralPosition.Builder( Chromosome.instance(chrom),Integer.parseInt(coordinates[1])).build(); 
                        Position endPos = new GeneralPosition.Builder( Chromosome.instance(chrom),Integer.parseInt(coordinates[2])).build(); 
                        Range<Position> intervalRange =  Range.closed(startPos, endPos);
                        Position geneStart = new GeneralPosition.Builder(Chromosome.instance("01"),-1).build();
                        Range<Position> geneRange = Range.closed(geneStart, geneStart); // better this or null ??
                        // asmFileId is only for assemblies
                        AnchorDataPHG adata = new AnchorDataPHG(intervalRange,"0",0,0,".",gvcfFile,null,sequenceSB.toString(), -1);

                        Position anchorPos = Position.of(coordinates[0], Integer.parseInt(coordinates[1]));
                        consensusDataMap.put(anchorPos, new Tuple<AnchorDataPHG,List<String>>(adata,haplotypes)); 
                    }
                    fileLine = fileLine.replace(">", "");
                    sequenceSB.setLength(0);                   
                    coordinates = getCoordinates(fileLine);
                    haplotypes = getHaplotypes(fileLine); // returns haplotypes stored as a list
                    gvcfFile = getGVCFFileName(fileLine,vcfDir()); // just store file name, not directory
                    if (coordinates == null || haplotypes == null || gvcfFile == null) {
                        myLogger.error("LoadConsensusAnchorSequencesPlugin - badly formed fasta file idline in : " + consensusFile + " line: " + fileLine);
                        throw new IllegalArgumentException("LoadConsensusAnchorSequencesPlugin: badly formatted idline in fasta file.\n" +
                                "Expecting >refchrom:refstart:refEnd;TaxaName_hapnumber:taxaName2_hapnumber");
                    }

                } else {
                    sequenceSB.append(fileLine); // keep appending until we hit next id line.
                }
            }
            if (sequenceSB.length() > 0) {
                String chrom = coordinates[0];
                Position startPos = new GeneralPosition.Builder(Chromosome.instance(chrom),Integer.parseInt(coordinates[1])).build(); 
                Position endPos = new GeneralPosition.Builder( Chromosome.instance(chrom),Integer.parseInt(coordinates[2])).build(); 
                Range<Position> intervalRange =  Range.closed(startPos, endPos);
                // asmFileID is only for assemblies
                AnchorDataPHG adata = new AnchorDataPHG(intervalRange,"0",0,0,".",gvcfFile,null,sequenceSB.toString(), -1);
                Position anchorPos = Position.of(coordinates[0], Integer.parseInt(coordinates[1]));
                consensusDataMap.put(anchorPos, new Tuple<AnchorDataPHG,List<String>>(adata,haplotypes));
 
            }
            br.close();
 
        } catch (Exception exc) {
            myLogger.error("LoadConsensusAnchorSequencesPlugin: Error processing file : " + consensusFile);           
            throw new IllegalStateException("LoadConsensusAnchorSequencesPlugin Error processing file " + consensusFile + "" + exc.getMessage());
        }

        return consensusDataMap;
    }
    
    private String getGVCFFileName(String idline,String vcfDir) {
        // gvcf file is last item on idline, first item after the space
        // >refChrom:refStartPos:refEndPos;taxa_0:taxa2_0: gvcfFileName
        String tokens[] = idline.split(" ");
        if (tokens.length < 2) return null;
        String gvcfFile = idline.split(" ")[1];
        
        StringBuilder variantSB = new StringBuilder().append(vcfDir).append(gvcfFile);             
        return variantSB.toString();
    }
    
    private  String[] getCoordinates(String idline) {
        int semiIndex = idline.indexOf(";");
        if (semiIndex == -1){
            myLogger.error("LoadConsensusSequences: badly formatted id line, missing a semicolon: " + idline);
            throw new IllegalArgumentException("LoadConsensusSequences: badly formatted idline, missing the semicolon: " + idline);
        }
        return idline.substring(0,semiIndex).split(":");
    }

    // Retrieve the haplotype part of the list
    private static List<String> getHaplotypes(String idline) {
        int semiIndex = idline.indexOf(";");
        if (semiIndex == -1) {
            throw new IllegalArgumentException("LoadConsensusSequences: badly formatted idLine, missing the semicolon: " + idline);
            
        }
        int spaceIdex = idline.indexOf(" ");
        List<String> haplotypes = new ArrayList<String>(Arrays.asList(idline.substring(semiIndex+1,spaceIdex).split(":")));
        return haplotypes;
    }

    @Override
    public ImageIcon getIcon() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getButtonName() {
        return "Load Consensus Anchor sequences to data base";
    }

    @Override
    public String getToolTipText() {
        return "Load Consensus Anchor sequences to data base";
    }

    //    // The following getters and setters were auto-generated.
    //    // Please use this method to re-generate.    
//         public static void main(String[] args) {
//             GeneratePluginCode.generate(LoadConsensusAnchorSequencesPlugin.class);
//         }
    /**
     * Convenience method to run plugin with one return object.
     */
    // TODO: Replace <Type> with specific type.
    //     public <Type> runPlugin(DataSet input) {
    //         return (<Type>) performFunction(input).getData(0).getData();
    //     }


    /**
     * Input fasta file with consensus sequences
     *
     * @return Input File
     */
    public String inputFile() {
        return myInputFile.value();
    }

    /**
     * Set Input File. Input fasta file with consensus sequences
     *
     * @param value Input File
     *
     * @return this plugin
     */
    public LoadConsensusAnchorSequencesPlugin inputFile(String value) {
        myInputFile = new PluginParameter<>(myInputFile, value);
        return this;
    }

    /**
     * Version name for this set of anchors as stored in anchor_versions
     * table in db
     *
     * @return Anchor Version
     */
    public String version() {
        return myVersion.value();
    }

    /**
     * Set Anchor Version. Version name for this set of anchors
     * as stored in anchor_versions table in db
     *
     * @param value Anchor Version
     *
     * @return this plugin
     */
    public LoadConsensusAnchorSequencesPlugin version(String value) {
        myVersion = new PluginParameter<>(myVersion, value);
        return this;
    }
    
    /**
     * Directory containing vcf files for consensus sequences.
     * 
     * @return value VCF Directory
     */
    public String vcfDir() {
        return vcfDir.value();
    }

    /**
     * Set VCF Directory.
     * 
     * @param value
     * @return
     */
    public LoadConsensusAnchorSequencesPlugin vcfDir(String value) {
        vcfDir = new PluginParameter<>(vcfDir, value);
        return this;
    }
    
    /**
     * Name of method used to collapse the anchors.  If it
     * doesn't exist, will be added to methods table.
     *
     * @return Collapse Method
     */
    public String collapseMethod() {
        return myCollapseMethod.value();
    }

    /**
     * Set Collapse Method. Name of method used to collapse
     * the anchors.  If it doesn't exist, will be added to
     * methods table.
     *
     * @param value Collapse Method
     *
     * @return this plugin
     */
    public LoadConsensusAnchorSequencesPlugin collapseMethod(String value) {
        myCollapseMethod = new PluginParameter<>(myCollapseMethod, value);
        return this;
    }

    /**
     * Description of methods used to collapse the anchor
     * sequences.
     *
     * @return Collapse Method Description
     */
    public String methodDetails() {
        return myMethodDetails.value();
    }

    /**
     * Set Collapse Method Description. Description of methods
     * used to collapse the anchor sequences.
     *
     * @param value Collapse Method Description
     *
     * @return this plugin
     */
    public LoadConsensusAnchorSequencesPlugin methodDetails(String value) {
        myMethodDetails = new PluginParameter<>(myMethodDetails, value);
        return this;
    }
    /**
     * Indication of whether the data should be loaded as a test
     *
     * @return true or false
     */
    public Boolean isTestMethod() {
        return isTestMethod.value();
    }

    /**
     * Set whether the data is loaded as a test.
     *
     * @param value true or false
     *
     * @return this plugin
     */
    public LoadConsensusAnchorSequencesPlugin isTestMethod(Boolean value) {
        isTestMethod = new PluginParameter<Boolean>(isTestMethod, value);
        return this;
    }

}
