/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

import java.awt.Frame;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;

import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.GeneratePluginCode;
import net.maizegenetics.plugindef.PluginParameter;

/**
 * This plugin will create the phg_postgres docker if it doesn't exist.  If it does exist,
 * the plugin returns with a message indicating so.
 * 
 * WHy don't we delete existing images, then create a new image?  Multiple reasons:
 * 1.  Because the phg_postgres Dockerfile does little more then run a specific
 *     postgres docker (with possible extensions to add users), the docker entry point
 *     is the postgres docker entry point.  
 * 2.  Because of 1, any containers run against phg_postgres are actually associated with
 *     the official postgres docker image (you will see it if you run "docker images" )
 * 3.  Because of 2, any real delete must involve deleting the postgres docker.
 * 4.  Deleting the postgres docker will fail if it has containers against it.  Only the user
 *     can determine if those containers need to remain, or can be cancelled.
 * 5.  If there are no containers against the postgres docker, there still could be other
 *    applications using the postgres image.  Deleting it makes assumptions about the user
 *    system.  We don't want to go there.
 *    
 * Therefore, no attempt will be made to delete a phg_postgres docker if it already exists.
 *
 * Dec 20. 2019:  This is deprecated as we no longer supported a phg_postgres docker.
 * Users should pull whatever postgres docker image they want and run that using
 * the files we created.
 * 
 * @author lcj34
 *
 */
@Deprecated
public class CreatePHGPostgresDockerPlugin extends AbstractPlugin {
    private static final Logger myLogger = Logger.getLogger(CreatePHGPostgresDockerPlugin.class);
    
    private PluginParameter<String> dockerDir = new PluginParameter.Builder<String>("dockerDir", null, String.class).guiName("Docker Directory").required(true).inDir()
            .description("Directory path where the file 'Dockerfile' exists for creating the PHG Postgresql docker").build();
    private PluginParameter<String> dockerCmd = new PluginParameter.Builder<String>("dockerCmd", "docker", String.class).guiName("Docker Command").required(false)
            .description("The docker command name.  Normally this is docker.  On Cornell CBSU machines it is docker1. The default is docker.").build();

    public CreatePHGPostgresDockerPlugin() {
        super(null, false);
    }

    public CreatePHGPostgresDockerPlugin(Frame parentFrame) {
        super(parentFrame, false);
    }

    public CreatePHGPostgresDockerPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }
     
    @Override
    public DataSet processData(DataSet input) {
        // Check if phgrepository_base and phgrepository_test images exist.
        boolean imageExists = false;
        boolean postgresExists = false;

        try {
            String cmd = dockerCmd() + " images";
                    
            myLogger.info("Executing command: " + cmd);
            Process start = Runtime.getRuntime().exec(cmd);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(start.getInputStream()));
            String line = null;

            while ((line = br.readLine()) != null)
            {
                if (line.contains("CREATED")) continue; // skip header line
                String imageName = line.substring(0, line.indexOf(" ")); // up to first white space               
                if (imageName.equals("phg_postgres")) {
                    imageExists = true;
                }
 
            }
        } catch (Exception exc) {
            throw new IllegalStateException("CreatePHGPostgresDockerPlugin:processData - error reading docker images: " + exc.getMessage());
        }

        myLogger.info("Image phg_postgres exists = "+ imageExists );

        try {
                               
            if (!imageExists) {
                // no phg_postgres image - create it                
                myLogger.info("Creating phg_postgres docker ");
                ProcessBuilder builder = new ProcessBuilder(
                        dockerCmd(), "build", "-t", "phg_postgres", dockerDir());
                builder.inheritIO();
                myLogger.info("Please wait, begin Command:" + builder.command().stream().collect(Collectors.joining(" ")));
                Process process = builder.start();
                int error = process.waitFor();
                if (error != 0) {                    
                    myLogger.info("Error creating phg_postgres, Error: " + error);            
                }
            } else {
                myLogger.info("Docker image phg_postgres already exists.  Not creating a new one. "
                        + "\nIf a new image is desired, please delete your old old image from the command line using the docker rmi <image name> command.");
            }

        } catch(Exception exc) {
            throw new IllegalStateException("Error deleting/creating docker images " + exc.getMessage());
        }

        return null;
    }
    @Override
    public ImageIcon getIcon() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getButtonName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getToolTipText() {
        // TODO Auto-generated method stub
        return null;
    }
    
    // The following getters and setters were auto-generated.
    // Please use this method to re-generate.
    //
     public static void main(String[] args) {
         //GeneratePluginCode.generate(CreatePHGPostgresDockerPlugin.class);
     }


     /**
      * Directory path where the file 'Dockerfile' exists for
      * creating the PHG Postgresql docker
      *
      * @return Docker Directory
      */
     public String dockerDir() {
         return dockerDir.value();
     }

     /**
      * Set Docker Directory. Directory path where the file
      * 'Dockerfile' exists for creating the PHG Postgresql
      * docker
      *
      * @param value Docker Directory
      *
      * @return this plugin
      */
     public CreatePHGPostgresDockerPlugin dockerDir(String value) {
         dockerDir = new PluginParameter<>(dockerDir, value);
         return this;
     }

     /**
      * The docker command name.  Normally this is docker.
      *  On Cornell CBSU machines it is docker1. The default
      * is docker.
      *
      * @return Docker Command
      */
     public String dockerCmd() {
         return dockerCmd.value();
     }

     /**
      * Set Docker Command. The docker command name.  Normally
      * this is docker.  On Cornell CBSU machines it is docker1.
      * The default is docker.
      *
      * @param value Docker Command
      *
      * @return this plugin
      */
     public CreatePHGPostgresDockerPlugin dockerCmd(String value) {
         dockerCmd = new PluginParameter<>(dockerCmd, value);
         return this;
     }

}
