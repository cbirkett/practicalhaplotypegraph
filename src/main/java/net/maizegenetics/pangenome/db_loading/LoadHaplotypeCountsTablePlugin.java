/**
 * 
 */
package net.maizegenetics.pangenome.db_loading;

import java.awt.Frame;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;
import org.xerial.snappy.Snappy;

import com.google.common.collect.Multiset;

import net.maizegenetics.dna.tag.Tag;
import net.maizegenetics.dna.tag.TaxaDistribution;
import net.maizegenetics.pangenome.api.HaplotypeGraph;
import net.maizegenetics.pangenome.api.HaplotypeNode;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.Datum;
import net.maizegenetics.plugindef.GeneratePluginCode;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.util.DirectoryCrawler;
import net.maizegenetics.util.Utils;

/**
 * This method reads haplotype counts from an incoming DataSet object, and stores results to a specified database.
 * This method will be called from FastqToHapCount plugin, which will pass a DataSet containing the
 * hits counts (perfect and exclusion) as well as the taxon.
 * 
 * Assumptions:  The haplotype counts files are specific to a single taxon. That taxon
 * name may be determined from the file name. The db genotypes table is queried for a line_name
 * that matches the taxon.  IF none exists, a new entry is added.  The new genotypes entries will only
 * link to the haplotype_counts table.
 * 
 * 1.  method:  unique name for method used to create counts.
 * 2.  methodDetails:  anything user wants to say about the method.
 * 3.  configFile:  name of file containing parameters for connecting to DB, including host, user,
 *          password, type (sqlite or postgres)
 * 4.  fastqFile:  name of file used to process the data.
 * 
 * Outputs:
 *   Data is stored to the haplotype_counts table.
 *
 * NOTE: August, 2019.  This plugin is obsolete. The haplotypes_counts table has been
 * removed and is replaced with read_mapping tables.
 * 
 * @author lcj34
 *
 */
@Deprecated
public class LoadHaplotypeCountsTablePlugin extends AbstractPlugin {
    private static final Logger myLogger = Logger.getLogger(LoadHaplotypeCountsTablePlugin.class);
 
     private PluginParameter<String> method = new PluginParameter.Builder<String>("method", null, String.class).guiName("Hapcount Method").required(true)
            .description("Name of method used for determining inclusion/exclusion counts").build();
     
     private PluginParameter<String> methodDetails = new PluginParameter.Builder<String>("methodDetails", "none", String.class).guiName("Method Details")
             .description("Text describing method used to create inclusion/exclusion counts.")
             .build();

    private PluginParameter<String> configFile = new PluginParameter.Builder<String>("configFile", null, String.class).guiName("DB Config File").required(true).inFile()
            .description("Name of config file to use for db connection")
            .build();
    private PluginParameter<String> fastqFile = new PluginParameter.Builder<String>("fastqFile", null, String.class).guiName("DB Config File").required(true).inFile()
            .description("Path to fastq used in FastqToHapCountPlugin.")
            .build();

    public LoadHaplotypeCountsTablePlugin() {
        super(null, false);
    }

    public LoadHaplotypeCountsTablePlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }

    @Override
    public DataSet processData(DataSet input) {
       
        long totalTime = System.nanoTime();
        
        // grab the inclusion/exclusion hit sets and taxon name from previous plugin
        List<Datum> perfectHitSetList = input.getDataWithName("PerfectHitsSet");
        Multiset<HaplotypeNode> perfectHitSet = (Multiset<HaplotypeNode>)perfectHitSetList.get(0).getData();
        
        List<Datum> exclusionHitSetList = input.getDataWithName("ExclusionHitsSet");
        Multiset<HaplotypeNode> exclusionHitSet = (Multiset<HaplotypeNode>)exclusionHitSetList.get(0).getData();
        
        List<Datum> taxonNameList= input.getDataWithName("Taxon");
        String taxonName = (String) taxonNameList.get(0).getData();
        
        Connection dbconn = DBLoadingUtils.connection(configFile(), false);
        if (dbconn == null) {
            myLogger.error("Could not get db connection from configFile : " + configFile());
        }
        
        PHGDataWriter phg = new PHGdbAccess(dbconn);
        
        byte[] counts = DBLoadingUtils.encodeHapCountsArrayFromMultiset(perfectHitSet, exclusionHitSet);
        
        // load data to db
        //phg.putHaplotypeCountsData( method(), methodDetails(),taxonName, fastqFile(), counts);
        
        try {
            ((PHGdbAccess)phg).close();
        } catch (Exception e) {
           myLogger.error("LoadHaplotypeCountsTablePlugin: error closeing PHGdbAccess");
        }
        
        System.out.println("Total time to run LoadHaplotypeCountsTablePLugin: " + (System.nanoTime() - totalTime)/1e9 + " seconds");
        return null;
    }
    
//    public static void main(String[] args) {
//        GeneratePluginCode.generate(LoadHaplotypeCountsTablePlugin.class) ;      
//    }
    
    @Override
    public ImageIcon getIcon() {        
        return null;
    }

    @Override
    public String getButtonName() {

        return ("Load Haplotype Counts");
    }

    @Override
    public String getToolTipText() {

        return ("Load haplotype count values to database table haplotye_counts");
    }
    

    /**
     * Name of method used for determining inclusion/exclusion
     * counts
     *
     * @return Hapcount Method
     */
    public String method() {
        return method.value();
    }

    /**
     * Set Hapcount Method. Name of method used for determining
     * inclusion/exclusion counts
     *
     * @param value Hapcount Method
     *
     * @return this plugin
     */
    public LoadHaplotypeCountsTablePlugin method(String value) {
        method = new PluginParameter<>(method, value);
        return this;
    }

    /**
     * Text describing method used to create inclusion/exclusion
     * counts.
     *
     * @return Method Details
     */
    public String methodDetails() {
        return methodDetails.value();
    }

    /**
     * Set Method Details. Text describing method used to
     * create inclusion/exclusion counts.
     *
     * @param value Method Details
     *
     * @return this plugin
     */
    public LoadHaplotypeCountsTablePlugin methodDetails(String value) {
        methodDetails = new PluginParameter<>(methodDetails, value);
        return this;
    }

    /**
     * Name of config file to use for db connection
     *
     * @return DB Config File
     */
    public String configFile() {
        return configFile.value();
    }

    /**
     * Set DB Config File. Name of config file to use for
     * db connection
     *
     * @param value DB Config File
     *
     * @return this plugin
     */
    public LoadHaplotypeCountsTablePlugin configFile(String value) {
        configFile = new PluginParameter<>(configFile, value);
        return this;
    }

    /**
     * Name of fastQ file used in FastqToHapCountPlugin
     *
     * @return Fastq File
     */
    public String fastqFile() {
        return fastqFile.value();
    }

    /**
     * Set Fastq File. Name of fastq file used in
     * FastqToHapCountPlugin
     *
     * @param value Fastq File
     *
     * @return this plugin
     */
    public LoadHaplotypeCountsTablePlugin fastqFile(String value) {
        fastqFile = new PluginParameter<>(fastqFile, value);
        return this;
    }

}

