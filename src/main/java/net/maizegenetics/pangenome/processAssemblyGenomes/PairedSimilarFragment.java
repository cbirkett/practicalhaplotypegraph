/**
 * 
 */
package net.maizegenetics.pangenome.processAssemblyGenomes;

/**
 * Class that holds reference and assembly alignment data
 * to be sorted and filtered in a longest path algorithm.
 * 
 * @author Baoxing Song, lcj34
 *
 */
public class PairedSimilarFragment implements Comparable<PairedSimilarFragment>{
    private int refStartPos; // start reference coordinate
    private int refEndPos; // end reference coordinate
    private int asmStartPos; // start position of assembly/query 
    private int asmEndPos; // end position of assembly/query
    private double score; // alignment score using as graph edge
    private int strand; // forward/reverse = 0/1
    
    public PairedSimilarFragment(int refStartPos, int refEndPos,int asmStartPos, int asmEndPos, double score, int strand) {
        this.refStartPos = refStartPos;
        this.refEndPos = refEndPos;
        this.asmStartPos = asmStartPos;
        this.asmEndPos = asmEndPos;
        this.score = score;
        this.strand = strand;
    }
    
    @Override
    public int compareTo(PairedSimilarFragment compareFruit) {
        int result = this.getRefStartPos() - compareFruit.getRefStartPos();
        
        // The tie breaker is asm start as it is these 2 values that
        // are used when creating the longest fragment
        if (result == 0) {
            return this.getAsmStartPos() - compareFruit.getAsmStartPos();
        }
        return result;
    }
    
    public int getStrand() {
        return strand;
    }   
    public void setStrand(int strand) {
        this.strand = strand;
    }
    public double getScore() {
        return score;
    }    
    public void setScore(double score) {
        this.score = score;
    }
    
 
    public int getRefStartPos() {
        return refStartPos;
    }
    public void setRefStartPos(int position) {
        this.refStartPos = position;
    }
    public int getRefEndPos() {
        return refEndPos;
    }
    public void setRefEndPos(int position) {
        this.refEndPos = position;
    }
    public int getAsmStartPos() {
        return asmStartPos;
    }
    public void setAsmStartPos(int position) {
        this.asmStartPos = position;
    }
    
    public int getAsmEndPos() {
        return asmEndPos;
    }
    public void setAsmEndPos(int position) {
        this.asmEndPos = position;
    }

}



