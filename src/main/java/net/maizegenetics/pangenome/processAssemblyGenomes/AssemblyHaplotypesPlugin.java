/**
 * 
 */
package net.maizegenetics.pangenome.processAssemblyGenomes;

import java.awt.Frame;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;

import net.maizegenetics.pangenome.api.ConvertVariantContextToVariantInfo;
import net.maizegenetics.pangenome.api.HaplotypeNode;
import net.maizegenetics.plugindef.GeneratePluginCode;
import org.apache.log4j.Logger;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.RangeSet;
import com.google.common.collect.TreeRangeMap;
import com.google.common.collect.TreeRangeSet;

import htsjdk.variant.variantcontext.VariantContext;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GenomeSequence;
import net.maizegenetics.dna.map.GenomeSequenceBuilder;
import net.maizegenetics.dna.map.Position;
import net.maizegenetics.pangenome.api.CreateGraphUtils;
import net.maizegenetics.pangenome.api.ReferenceRange;
import net.maizegenetics.pangenome.db_loading.AnchorDataPHG;
import net.maizegenetics.pangenome.db_loading.DBLoadingUtils;
import net.maizegenetics.pangenome.db_loading.GenoHaploData;
import net.maizegenetics.pangenome.db_loading.PHGDataWriter;
import net.maizegenetics.pangenome.db_loading.PHGdbAccess;
import net.maizegenetics.pangenome.fastaExtraction.GVCFSequence;
import net.maizegenetics.pangenome.hapCalling.HapCallingUtils;
import net.maizegenetics.plugindef.AbstractPlugin;
import net.maizegenetics.plugindef.DataSet;
import net.maizegenetics.plugindef.PluginParameter;
import net.maizegenetics.util.Tuple;

/**
 * This Class is DEPRECATED!! Should not be used anymore.  It will not create
 * or store the genome_file_data table entries.
 *
 * Process Assemblies - both anchor and interanchor.
 * This class exercises the following tools:
 *   mummer4 scripts: (nucmer, delta-filter, show-coords, show-snps)
 * 
 * Algorithm when processing all steps:  
 *  1.  run mummer4:nucmer with the parameters -c 250 --mum
 *  2.  run mummer4:show-coords with parameters -T -r (tab-delimited, sorted by ref)
 *  3.  run mummer4:show-snps with parameters -T -r (tab-delimited, sorted by ref ID and SNP positions)
 *  4.  Interanchors.  
 *      a. Processed the same as the anchors.  The VariantContext code grabs sequence aligning to both.
 *         We expect a certain linearity to the alignments.  There will be large indels between sequences
 *         that are picked up as inter-genic regions.
 *  5.  The DB is loaded in batches of 3000 reference_ranges at a time.  
 *         
 * Assumptions/requirements:
 *  1.  alignments are on a per-chromosome basis: I expect fastas to be broken down by chromosome,
 *      1 chrom per fasta
 *  2.  Id lines in fasta must start with >X, or >chrX or >chromosomeX  where X is the chromosome number.
 *      Additional data is allowed after a space, but the first data must identify the chromosome as above.
 *  3.  The assembly and reference chromosomes need to be named in a similar fashion to allow for db matching of genome_interval_ids
 *      IE, if the ref chrom name has a number without leading 0's, so must the assembly.  IE, chr1 for ref, then chr1 for assembly.
 *  4.  For populating the DB per Ed: ploidy=1, genes/chroms phased=TRUE (put the confidence at 1)
 *  
 * NOTE: Entry points
 *   There are now 3 places where the user may enter this code.  The default is to run all steps.
 *   - "all":  entryPoint parameter is "all", all steps are run
 *   - "refilter": entryPoint parameter is "refilter"
 *      Processing begins with the call to MummerScriptProcessing.refilterCoords()  It is assumed the
 *      alignment directory contains mummer output for nucmer, delta-filter and show-coords on both
 *      the original and filtered delta files.
 *   - "haplotypes":  entryPoint parameter is "haplotypes"
 *      Processing begins after all mummer4 scripts have finished, and the coordinates and snps files
 *      have been filtered.  Only the VariantContext/sequence creation and db loading steps are performed.
 *      It is assumed the algin directory contains the necessary mummer files for processing.
 * 
 * @author lcj34
 *
 */

@Deprecated
public class AssemblyHaplotypesPlugin extends AbstractPlugin {
    
    // This enum specifies where this code should be entered.
    // "all" indicates run everything
    // "filter" indicates run from the point of processing the mummer coords files
    // "haplotypes" means to skip all the mummer processing, pick this up where haplotypes are created
    // In all cases other than "all", it assumes mummer files needed have been previously created.
    // This allows the user to run mummer4 alignment once, then use these files to populate 
    // multiple dbs.
    public static enum ASSEMBLY_ENTRY_POINT {all, deltafilter, refilter, haplotypes};
    private static final Logger myLogger = Logger.getLogger(AssemblyHaplotypesPlugin.class);
    
    private PluginParameter<String> myRef = new PluginParameter.Builder<String>("ref", null, String.class).guiName("Reference Fasta File").required(true).inFile()
            .description("Input reference fasta file for single chromosome").build();
    private PluginParameter<String> myAssembly = new PluginParameter.Builder<String>("assembly", null, String.class).guiName("Assembly Fasta File").required(true).inFile()
            .description("Assembly fasta file for a single chromosome to align against the reference").build();
    private PluginParameter<String> myOutputDir = new PluginParameter.Builder<String>("outputDir", null, String.class).guiName("Output Directory").required(true).outDir()
            .description("Output directory including trailing / for writing files").build();
    private PluginParameter<String> dbConfigFile = new PluginParameter.Builder<String>("dbConfigFile", null, String.class).guiName("DB Config File").required(true).inFile()
            .description("File containing lines with data for host=, user=, password= and DB=, DBtype= used for db connection").build();
    private PluginParameter<String> assemblyName = new PluginParameter.Builder<String>("assemblyName", null, String.class).guiName("Assembly Name").required(true)
            .description("Name of Assembly Taxon, to be stored as taxon name in the DB").build();
    private PluginParameter<String> chrom = new PluginParameter.Builder<String>("chrom", null, String.class).guiName("Chromosome Name").required(true)
            .description("Name of chromosome as it appears both for the reference in the db reference_ranges table, and in the fasta file idLine for the assembly").build();
    
    // Non-required parameters - provide if not on user path
    private PluginParameter<String> mummer4Path = new PluginParameter.Builder<String>("mummer4Path", "/mummer/bin/", String.class).guiName("Mummer4 Executables Path").inDir()
            .description("Path where mummer4 executables live: nucmer, delta-filter, show-snps, show-coords  ").build();
    private PluginParameter<Integer> clusterSize = new PluginParameter.Builder<Integer>("clusterSize", 250, Integer.class).guiName("Mummer4 Nucmer Cluster Size ")
            .description("Cluster size to use with mummer4 nucmer script. ").build();

    private PluginParameter<String> myGVCFOutputDir = new PluginParameter.Builder<>("gvcfOutputDir",null, String.class).guiName("GVCF Output Dir").required(false).outDir()
            .description("Directory for gvcf files to be output for later use").build();
    
    private PluginParameter<ASSEMBLY_ENTRY_POINT> entryPoint = new PluginParameter.Builder<>("entryPoint", ASSEMBLY_ENTRY_POINT.all, ASSEMBLY_ENTRY_POINT.class)
            .guiName("Assembly Entry Point")
            .description("Where to begin processing. All runs everything.  Refilter means run from the re-filtering of the coords files. hapltypes runs just the haplotypes processing.")
            .build();
    
    private PluginParameter<Integer> minInversionLen = new PluginParameter.Builder<Integer>("minInversionLen", 7500, Integer.class).guiName("Minimum Inversion Length").required(false)
            .description("Minimum length of inversion for it to be kept as part of the alignment. Default is 7500").build();

    private PluginParameter<String> assemblyMethod = new PluginParameter.Builder<String>("assemblyMethod", "mummer4", String.class).guiName("Assembly Method")
            .description("Method name to load to db for assembly processing, default is mummer4  ").build();
    public AssemblyHaplotypesPlugin() {
        super(null, false);
    }

    public AssemblyHaplotypesPlugin(Frame parentFrame) {
        super(parentFrame, false);
    }

    public AssemblyHaplotypesPlugin(Frame parentFrame, boolean isInteractive) {
        super(parentFrame, isInteractive);
    }
    
    @Override
    public DataSet processData(DataSet input) {
        Long totalTime = System.nanoTime();
        Long time = System.nanoTime();
        
        // mummer4 will write output to the directory indicated with the prefix.
        String outputDeltaFilePrefix = outputDir() + "/" + "ref_" + assemblyName() + "_" + chrom() + "_c" + clusterSize();
        
        // Define output files to be created or used during processing
        String outputCoordsOrig = outputDeltaFilePrefix + ".coords_orig";
        String outputCoordsFiltered = outputDeltaFilePrefix + ".coords_filtered";
        
        // *.coords_filteredPlu_noEmbedded contains coordinates from *.coords_filtered, some inversion
        // from the original coords that were returned, all embedded entries removed.
        String outputCoordsNoEmbedded = outputDeltaFilePrefix + ".coords_filteredPlus_noEmbedded";  
        String coordsFile = outputDeltaFilePrefix + ".coords_final"; // used to create haplotypes
        
        String snpsForOverlaps = outputDeltaFilePrefix + ".snps_prefiltered"; // created in runShowSNPsWithCat
        String snpFile = outputDeltaFilePrefix + ".snps_final";

        if (entryPoint() == ASSEMBLY_ENTRY_POINT.all) {
            time = System.nanoTime();
            myLogger.info("Call alignWithNucmer, delta file will be written to " + outputDeltaFilePrefix + ".delta");
            MummerScriptProcessing.alignWithNucmer( ref(),  assembly(),   outputDeltaFilePrefix,  outputDir(), mummer4Path(), clusterSize());
            myLogger.info("align with Nucmer took: " + (System.nanoTime() - time)/1e9 + " seconds");

        }

        if (entryPoint() == ASSEMBLY_ENTRY_POINT.all || entryPoint() == ASSEMBLY_ENTRY_POINT.deltafilter ) {

            time = System.nanoTime();
            myLogger.info("call runDeltaFilter");
            MummerScriptProcessing.runDeltaFilter(outputDeltaFilePrefix,outputDir(),mummer4Path());
            myLogger.info("runDeltaFilter took: " + (System.nanoTime() - time)/1e9 + " seconds");

            time = System.nanoTime();
            myLogger.info("call runShowCoords");
            MummerScriptProcessing.runShowCoords(outputDeltaFilePrefix,outputDir(),mummer4Path());
            myLogger.info("runShowCoords took: " + (System.nanoTime() - time)/1e9 + " seconds");
        }

        if (entryPoint() == ASSEMBLY_ENTRY_POINT.all || entryPoint() == ASSEMBLY_ENTRY_POINT.deltafilter || entryPoint() == ASSEMBLY_ENTRY_POINT.refilter) {
            time = System.nanoTime();
            myLogger.info("Entry point includes REFILTER: call refilterCoords");
            MummerScriptProcessing.refilterCoordsFile(outputDeltaFilePrefix,outputCoordsOrig, outputCoordsFiltered, outputCoordsNoEmbedded, 
                    chrom(),minInversionLen());
            myLogger.info("refilterCoordsFile took: " + (System.nanoTime() - time)/1e9 + " seconds");
            
            // The coords parameter is the original, non-merged coords file entries containing
            // coordinates removed when filtering that were added back in refilterCoordsFile().
            // This file is used to filter SNPs added back when the original and filtered
            // SNP lists are merged.
            time = System.nanoTime();
            myLogger.info("call runShowSNPs");
            MummerScriptProcessing.runShowSNPsWithCat( outputDeltaFilePrefix, outputCoordsNoEmbedded,  outputDir(),mummer4Path(), chrom());
            myLogger.info("runShowSNPsWithCat took: " + (System.nanoTime() - time)/1e9 + " seconds");
            
            // *.coords_final starts with the coords_filteredPlus_noEmbedded and splits overlapping entries.
            String outputCoordsFinal = outputDeltaFilePrefix + ".coords_final";
            // The *.snps_prefiltered file is used when processing overlaps to prevent splitting
            // in the middle of an indel
            myLogger.info("second coordinates filtering, call MummerScriptProcessing.filterOverlaps");

            time = System.nanoTime();
            MummerScriptProcessing.filterCoordsOverlaps(outputCoordsNoEmbedded, snpsForOverlaps, outputCoordsFinal);
            myLogger.info("filterCoordsOverlaps took: " + (System.nanoTime() - time)/1e9 + " seconds");
            
            // Write final snps file.  This method will create the <outputDeltaFilePrefix>.snps_final file.
            // It filters out any SNP that doesn't fall within a region represented in the *.coords_final file
            time = System.nanoTime();
            myLogger.info("call finalSnpFiltering, verifySnps");
            MummerScriptProcessing.finalSnpFiltering( snpsForOverlaps,  outputDeltaFilePrefix,  chrom());
            myLogger.info("finalSnpFiltering took: " + (System.nanoTime() - time)/1e9 + " seconds");
        }
 
        // The code below is executed for all entry points.

        RangeMap<Position,Tuple<String,String>>  snpMap = null;
        if (Files.exists(Paths.get(snpFile)) && ( new File(snpFile).length() > 0) ){
            myLogger.info("Call parseMummerSNPFile");
            time = System.nanoTime();
            snpMap = AssemblyProcessingUtils.parseMummerSNPFile(snpFile,chrom());
            myLogger.info("parseMummerSNPFile took: " + (System.nanoTime() - time)/1e9 + " seconds");
        } else {
            myLogger.warn(" NO mummerSNP file created !! processing without SNPs");
            snpMap = TreeRangeMap.create();
        }

        myLogger.info("Call parseCoordinateRegions with refChrom : " + chrom());
        time = System.nanoTime();
        Map<Range<Position>,List<Position>> refMappings = AssemblyProcessingUtils.parseCoordinateRegions(coordsFile,chrom());
        myLogger.info("parseCoordinateRegions took: " + (System.nanoTime() - time)/1e9 + " seconds");

        GenomeSequence refSequence = GenomeSequenceBuilder.instance(ref());
        GenomeSequence assemblySequence = GenomeSequenceBuilder.instance(assembly());
        
        //Get the mappings between the nonMapped regions in the corrdinates and add them as SNPs
        time = System.nanoTime();
        RangeMap<Position,Tuple<String,String>> nonMappedSnpMap = AssemblyProcessingUtils.setupIndelVariants(refMappings,refSequence,assemblySequence);
        myLogger.info("setupIndelVariants took: " + (System.nanoTime() - time)/1e9 + " seconds");
        snpMap.putAll(nonMappedSnpMap);
        
       // We have the alignments.  Connect to the db, pull reference Ranges, create anchors and interanchors
        Connection dbConn = DBLoadingUtils.connection(dbConfigFile(), false);

        myLogger.info("call referenceRangeForChromMap - getting anchors for chrom from the db");
        Map<Integer, ReferenceRange> idByRangeMap = AssemblyProcessingUtils.referenceRangeForChromMap( dbConn,   chrom());
        
        // Get a rangeSet of these anchors
        RangeSet<Position>  anchors = AssemblyProcessingUtils.getAnchorRangeSet(idByRangeMap);
        
        // Get VariantContext list from the anchors and refMappingPositions created above
        time = System.nanoTime();

        List<VariantContext> fullVC = new ArrayList<>();
        if (snpMap.asMapOfRanges().size() == 0 && refMappings.size() == 1) {
            // no snps from above - we likely have assembly and reference files matching.
            // Just split the chrom based on referencer ranges, make all as reference blocks
            myLogger.warn("Creating variantContext as single reference block per ref range");
            fullVC = AssemblyProcessingUtils.createVCasRefBlock(refSequence, assemblyName(), anchors, refMappings);
        } else {
            myLogger.info("calling extractAnchorVariantContextsFromAssemblyAlignments");
            fullVC = AssemblyProcessingUtils.extractAnchorVariantContextsFromAssemblyAlignments(refSequence, assemblyName(), anchors,refMappings,snpMap);
        }
        myLogger.info("extractAnchorVariantContextsFromAssemblyAlignments took: " + (System.nanoTime() - time)/1e9 + " seconds");

        time = System.nanoTime();
        List<VariantContext> vcsResized = AssemblyProcessingUtils.splitRefRange(fullVC, idByRangeMap, refSequence);
        myLogger.info("splitRefRange took: " + (System.nanoTime() - time)/1e9 + " seconds");
        
        if(gVCFOutputDir() != null) {
            String outputGVCFFileName = gVCFOutputDir() +assemblyName() + "_Chr_"+chrom()+".g.vcf";

            HapCallingUtils.writeVariantContextsToVCF(fullVC, outputGVCFFileName, null, assemblyName());
        }

        // create map of data for db loading.       
        myLogger.info("calling createMapDataForDBLoading, fullVC list size: " + fullVC.size() + ", vcsResized list: " + vcsResized.size());
        time = System.nanoTime();
        Map<Integer,AnchorDataPHG> gidToAssemblyDataMap = createAndLoadAssemblyData(idByRangeMap, vcsResized, refSequence, dbConn, assemblyName(),
                chrom(), clusterSize(),assemblyMethod(), pluginParameters()) ;
        myLogger.info("createAndLoadAssemblyData took: " + (System.nanoTime() - time)/1e9 + " seconds");
        
        try  {
            dbConn.close();
        } catch (Exception exc) {
            myLogger.error("AssemblyHaplotypesPlugin: Error closing db connection");
        }
        myLogger.info("Finished with AssemblyHaplotypesPlugin for assembly/chrom " + assemblyName() + "/" + chrom() + ": Total time to run " + (System.nanoTime()-totalTime)/1e9 + " seconds");        
        return null;
    }
    

    
    // Create the map of data for db loading
    /**
     * Create the assembly genotype/haplotype  data and load to the PHG database
     * @param idByRangeMap
     * @param fullVC
     * @param refSequence
     * @param dbConn
     * @param assemblyName
     * @param chrom
     * @return
     */
    public static Map<Integer,AnchorDataPHG> createAndLoadAssemblyData(Map<Integer, ReferenceRange> idByRangeMap, List<VariantContext> fullVC, 
            GenomeSequence refSequence, Connection dbConn, String assemblyName,  String chrom, int clusterSize, String method, Map<String,String> pluginParams) {
        
        Long time = System.nanoTime();

        Map<Integer,AnchorDataPHG> gidToAssemblyDataMap = new HashMap<Integer,AnchorDataPHG>();
        GenomeSequence hapGS = GVCFSequence.instance(refSequence, fullVC, true, assemblyName); // make this once for full list
        
        int droppedCount = 0;
        int noAnchorSpecificVC = 0;
        try  {

            long time2 = System.nanoTime();
            myLogger.info("createAndLoadAssemblyData: call loadInitialAssemblyData");
            // load initial data to tables genotypes, gamete_groups, etc
            // This is only called from the deprecated class AssemblyHaplotypesPlugin, so defaulting isTestMethod to false
            Tuple<Integer,Integer> gameteGrpAndGenomeDataIds = AssemblyProcessingUtils.loadInitialAssemblyData( assemblyName,  method,  clusterSize, dbConn, pluginParams, null, false);
            int gamete_grp_id = gameteGrpAndGenomeDataIds.getX();
            myLogger.info("createAndLoadAssemblyData: time to loadInitialAssemblyData " + (System.nanoTime()-time2)/1e9 + " seconds");
            int mapCount = 0;
            int totalCount = 0;
                        
            // Create RangeMap for Variant Context
            RangeMap<Position, VariantContext> positionRangeToVariantContextMap = TreeRangeMap.create();
            for(VariantContext variantContext : fullVC) {
                positionRangeToVariantContextMap.put(Range.closed(Position.of(variantContext.getContig(),variantContext.getStart()),
                        Position.of(variantContext.getContig(),variantContext.getEnd())),
                        variantContext);
            }
            
            // Get  reference range intervals for inter-anchors
            // Don't close phg after use as it will kill the connection.
            PHGDataWriter phg = new PHGdbAccess(dbConn);
            List<Integer> interAnchorList = phg.getRefRangesForMethod(DBLoadingUtils.INTER_REGION_REFERENCE_RANGE_GROUP);

            List<Integer> vcEndProcessed = new ArrayList<>();
            List<Integer> vcStartProcessed = new ArrayList<>();
            
            // Sort the ref_range_ids into a list, process them in a sorted manner
            List<Integer> intervalIDList = new ArrayList<>(idByRangeMap.keySet());
            Collections.sort(intervalIDList);
            
            myLogger.info("createAndLoadAssemblyData: begin creating assembly haplotypes data");
            // get variant contexts for each anchor position.
            for (Integer genome_interval_id : intervalIDList) {
                boolean isIA = interAnchorList.contains(genome_interval_id) ? true : false;
 
                ReferenceRange refRange = idByRangeMap.get(genome_interval_id);
 
                time2 = System.nanoTime();  
                List<VariantContext> anchorSpecificVCList = AssemblyProcessingUtils.findVCListForAnchor(positionRangeToVariantContextMap, 
                        Position.of(chrom, refRange.start()), Position.of(chrom, refRange.end()));
  
                if (anchorSpecificVCList.size() == 0) {
                    noAnchorSpecificVC++;
                    continue ; // no assembly mappings for this anchor
                }
                 
                VariantContext vcStart = anchorSpecificVCList.get(0);
                VariantContext vcEnd = anchorSpecificVCList.get(anchorSpecificVCList.size()-1);
                int asmStart = vcStart.getStart();
                int asmEnd = vcEnd.getEnd();
                

                // Check if last VC spans more than 1 reference range.
                // We will only use it to add sequence to the first range it represents.                
                // ONLY variants span reference ranges.  Reference Blocks
                // have previously been split by reference range.
                if (asmStart < refRange.start() ) {
                    // This record spans multiple reference ranges
                    if (anchorSpecificVCList.size() == 1) {
                        myLogger.info("CreateAndLoadAssembly: VC START overlaps: skipping haplotype for refRange " 
                          + genome_interval_id + ", asmStart=" + asmStart + ", refRange.start=" + refRange.start());
                        noAnchorSpecificVC++; // no haplotype will be created for this reference range
                        continue;
                    }
                    // more than 1 VC record.  Skip the first if this is an ANCHOR or its sequence has
                    // already been added to the end of a previous inter-anchor record.
                    // Start from the second entry in the VC list. 
                    int vcTempEnd = vcStart.getEnd();
                    if (!isIA || vcStartProcessed.contains(asmStart) || vcEndProcessed.contains(vcTempEnd)) {
                        myLogger.info("CreateAndLoadAssembly: VC START overlaps: skip first VC records for refRange ID " 
                                + genome_interval_id + ", asmStart=" + asmStart + ", refRange.start=" + refRange.start());
                        vcStart = anchorSpecificVCList.get(1);
                        asmStart = vcStart.getStart();
                        anchorSpecificVCList.remove(0); // remove so isn't included with variants below
                    } else {
                        myLogger.info("CreateAndLoadAssembly: VC overlaps, giving sequence to reference_range " + genome_interval_id);
                        vcStartProcessed.add(asmStart);
                        vcEndProcessed.add(vcTempEnd);
                    }

                }
                
                // Now check for VC extending beyond the end.
                // We will only use it to add sequence to an inter-anchor.                
                // ONLY variants span reference ranges.  Reference Blocks
                // have previously been split by reference range.
                if (asmEnd > refRange.end() ) {
                    // This record spans multiple reference ranges
                    if (anchorSpecificVCList.size() == 1) {
                        myLogger.info("CreateAndLoadAssembly: VC END overlaps: skipping haplotype for refRange " 
                          + genome_interval_id );
                        noAnchorSpecificVC++; // no haplotype will be created for this reference range
                        continue;
                    }
                    // more than 1 VC record.  Skip the last if this is an ANCHOR or its sequence has
                    // already been added. Otherwise its sequence will be added to the start
                    // of the next record that it overlaps, which should be an inter-anchor.  
                    // Here, end with the previous last entry in the VC list. 
                    int vcTempStart = vcEnd.getStart();
                    if (!isIA || vcStartProcessed.contains(vcTempStart) || vcEndProcessed.contains(asmEnd)) {       
                        myLogger.info("CreateAndLoadAssembly: VC END overlaps: skip last VC records for refRange ID " 
                                + genome_interval_id );
                        int lastEntry = anchorSpecificVCList.size()-1;
                        vcEnd = anchorSpecificVCList.get(lastEntry-1);
                        asmEnd = vcEnd.getEnd();
                        anchorSpecificVCList.remove(lastEntry); // remove so isn't included with variants below
                    } else {
                        myLogger.info("CreateAndLoadAssembly: VC overlaps, giving sequence to reference_range " + genome_interval_id);
                        vcStartProcessed.add(vcTempStart);
                        vcEndProcessed.add(asmEnd);
                    }

                }
                int asmLen = (vcEnd.getEnd() - vcStart.getStart()) + 1;
                int refLen = (refRange.end() - refRange.start()) +  1;


                int asmStartCoordinate = vcStart.getAttributeAsInt("ASM_Start",-1);
                int asmEndCoordinate = vcEnd.getAttributeAsInt("ASM_End",-1);

                // skip anchors that are too short. 250 is our mincluster size in nucmer
                // there are some ref anchors whose length < 250.  keep those
                if ((asmLen < clusterSize) && asmLen < refLen) {
                    droppedCount++;
                    myLogger.info("createAndLoadAssemblyData: Skipping anchor with asmLen " + asmLen + " and refLen " + refLen);                    
                    continue; 
                }
                
                // This is still reference sequence.  The asm sequence is created based on what is stored
                // for the SNPs and the ref sequence.                
                String hapSequence = hapGS.genotypeAsString(refRange.chromosome(), asmStart, asmEnd);

                StringBuilder sb = new StringBuilder();
                sb.append("RefRangeId = ").append(genome_interval_id);
                sb.append(" chr/st/end: ").append(refRange.chromosome().getName());
                sb.append("/").append(refRange.start()).append("/").append(refRange.end());
                sb.append(" AsmStart= ").append(asmStart);
                sb.append(", asmEnd= ").append(asmEnd).append(", asmLen= ").append(asmLen);
                sb.append(", refLen= ").append(refLen);
                sb.append(", seqLen= ").append(hapSequence.length());
                sb.append(" vcList size: ").append(anchorSpecificVCList.size());

                //myLogger.info(sb.toString()); // keep for debugging

                List<HaplotypeNode.VariantInfo> variants = ConvertVariantContextToVariantInfo.convertVCListToVariantInfoList(anchorSpecificVCList, assemblyName,0);

               // Create AnchorDataPHG object for map
                Range<Position> refIntervals = Range.closed(Position.of(refRange.chromosome(), refRange.start()),
                        Position.of(refRange.chromosome(), refRange.end()));
                Range<Position> geneIntervals = Range.closed(Position.of(refRange.chromosome(), 0), Position.of(refRange.chromosome(), 0));// not used

                time2 = System.nanoTime();
                AnchorDataPHG aData = new AnchorDataPHG(refIntervals, "0",0,0, ".","none",null, variants, hapSequence, -1);
                gidToAssemblyDataMap.put(genome_interval_id, aData);

                mapCount++;
                totalCount++;
                if (mapCount == 3000) {
                    // load after each 3000 entries
                    myLogger.info("createANdLoadAssemblyData process 3000 before load took: " + (System.nanoTime() - time)/1e9 + " seconds");
                    myLogger.info("AssemblyHaplotypesPlugin:createAndLoadAssemblyData call loadAssemblyDataToD with next 3000, totalCount= " + totalCount);
                    time = System.nanoTime();

                    AssemblyProcessingUtils.loadAssemblyDataToDB( gamete_grp_id, method,  dbConn, gidToAssemblyDataMap, chrom );
                    myLogger.info("createAndLoadAssemblyData process DB 3000 load took: " + (System.nanoTime() - time)/1e9 + " seconds");
                    gidToAssemblyDataMap.clear();
                    mapCount = 0;
                    time = System.nanoTime();
                }
            }
            myLogger.info("createAndLoadAssemblyData: count of assembly data to load: " + mapCount);
            myLogger.info("createAndLoadAssemblyData: size of vcStartProcessed " + vcStartProcessed.size()
                    + " size of vcEndProcessed " + vcEndProcessed.size());
            if (mapCount > 0) {
                myLogger.info("createAndLoadAssemblyData: call loadAssemblyDataToD with last data, totalCount= " + totalCount);
                AssemblyProcessingUtils.loadAssemblyDataToDB( gamete_grp_id, method,  dbConn, gidToAssemblyDataMap, chrom );
            }
            
            // Close db connection
            dbConn.close();            
    
        } catch (Exception ioe) {
            ioe.printStackTrace();
            try {
                dbConn.close();
            } catch (SQLException sqle) {
                myLogger.warn("createAndLoadAssemblyData: error closing SQL connection: " + sqle.getMessage());
            }
            
            throw new IllegalArgumentException("createAndLoadAssemblyData: error loading assembly haplotypes" + ioe.getMessage());
        }
      
        myLogger.info("createAndLoadAssemblyData: Finished loading assembly chrom data in " + (System.nanoTime()-time)/1e9 
                + " seconds, num dropped due to length:  " + droppedCount + ", no anchorSpecificVCs: " + noAnchorSpecificVC);
        return gidToAssemblyDataMap;
    }
    

    @Override
    public ImageIcon getIcon() {
        return null;
    }

    @Override
    public String getButtonName() {
        return ("Assembly Haplotypes");
    }

    @Override
    public String getToolTipText() {
        
        return ("Create and load Assembly Haplotypes from mummer4 alignment");
    }
    // The following getters and setters were auto-generated.
    // Please use this method to re-generate.
    //
     public static void main(String[] args) {
         GeneratePluginCode.generate(AssemblyHaplotypesPlugin.class);
     }

    /**
     * Input reference fasta file for single chromosome
     *
     * @return Reference Fasta File
     */
    public String ref() {
        return myRef.value();
    }

    /**
     * Set Reference Fasta File. Input reference fasta file
     * for single chromosome
     *
     * @param value Reference Fasta File
     *
     * @return this plugin
     */
    public AssemblyHaplotypesPlugin ref(String value) {
        myRef = new PluginParameter<>(myRef, value);
        return this;
    }

    /**
     * Assembly fasta file for a single chromosome to align
     * against the reference
     *
     * @return Assembly Fasta File
     */
    public String assembly() {
        return myAssembly.value();
    }

    /**
     * Set Assembly Fasta File. Assembly fasta file for a
     * single chromosome to align against the reference
     *
     * @param value Assembly Fasta File
     *
     * @return this plugin
     */
    public AssemblyHaplotypesPlugin assembly(String value) {
        myAssembly = new PluginParameter<>(myAssembly, value);
        return this;
    }

    /**
     * Output directory including trailing / for writing files
     *
     * @return Output Directory
     */
    public String outputDir() {
        return myOutputDir.value();
    }

    /**
     * Set Output Directory. Output directory including trailing
     * / for writing files
     *
     * @param value Output Directory
     *
     * @return this plugin
     */
    public AssemblyHaplotypesPlugin outputDir(String value) {
        myOutputDir = new PluginParameter<>(myOutputDir, value);
        return this;
    }

    /**
     * File containing lines with data for host=, user=, password=
     * and DB=, DBtype= used for db connection
     *
     * @return DB Config File
     */
    public String dbConfigFile() {
        return dbConfigFile.value();
    }

    /**
     * Set DB Config File. File containing lines with data
     * for host=, user=, password= and DB=, DBtype= used for
     * db connection
     *
     * @param value DB Config File
     *
     * @return this plugin
     */
    public AssemblyHaplotypesPlugin dbConfigFile(String value) {
        dbConfigFile = new PluginParameter<>(dbConfigFile, value);
        return this;
    }

    /**
     * Name of Assembly Taxon, to be stored as taxon name
     * in the DB
     *
     * @return Assembly Name
     */
    public String assemblyName() {
        return assemblyName.value();
    }

    /**
     * Set Assembly Name. Name of Assembly Taxon, to be stored
     * as taxon name in the DB
     *
     * @param value Assembly Name
     *
     * @return this plugin
     */
    public AssemblyHaplotypesPlugin assemblyName(String value) {
        assemblyName = new PluginParameter<>(assemblyName, value);
        return this;
    }

    /**
     * Name of chromosome as it appears both for the reference
     * in the db reference_ranges table, and in the fasta
     * file idLine for the assembly
     *
     * @return Chromosome Name
     */
    public String chrom() {
        return chrom.value();
    }

    /**
     * Set Chromosome Name. Name of chromosome as it appears
     * both for the reference in the db reference_ranges table,
     * and in the fasta file idLine for the assembly
     *
     * @param value Chromosome Name
     *
     * @return this plugin
     */
    public AssemblyHaplotypesPlugin chrom(String value) {
        chrom = new PluginParameter<>(chrom, value);
        return this;
    }

    /**
     * Path to mummer4 binaries
     *
     * @return Path to mummer 4 binaries
     */
    public String mummer4Path() {
        return mummer4Path.value();
    }

    /**
     * Set Mummer4 binary path
     *
     * @param value Mummer4 binary path
     *
     * @return this plugin
     */
    public AssemblyHaplotypesPlugin mummer4Path(String value) {
        mummer4Path = new PluginParameter<>(mummer4Path, value);
        return this;
    }
    
    /**
     * Cluster size to use with mummer4 nucmer script. 
     *
     * @return Mummer4 Nucmer Cluster Size 
     */
    public Integer clusterSize() {
        return clusterSize.value();
    }

    /**
     * Set Mummer4 Nucmer Cluster Size . Cluster size to use
     * with mummer4 nucmer script. 
     *
     * @param value Mummer4 Nucmer Cluster Size 
     *
     * @return this plugin
     */
    public AssemblyHaplotypesPlugin clusterSize(Integer value) {
        clusterSize = new PluginParameter<>(clusterSize, value);
        return this;
    }

    /**
     * Directory for gvcf files to be output for later use
     *
     * @return GVCF Output Dir
     */
    public String gVCFOutputDir() {
        return myGVCFOutputDir.value();
    }

    /**
     * Set GVCF Output Dir. Directory for gvcf files to be
     * output for later use
     *
     * @param value GVCF Output Dir
     *
     * @return this plugin
     */
    public AssemblyHaplotypesPlugin gVCFOutputDir(String value) {
        myGVCFOutputDir = new PluginParameter<>(myGVCFOutputDir, value);
        return this;
    }

    /**
     * Where to begin processing. All runs everything.  Refilter
     * means run from the re-filtering of the coords files.
     * hapltypes runs just the haplotypes processing.
     *
     * @return Assembly Entry Point
     */
    public ASSEMBLY_ENTRY_POINT entryPoint() {
        return entryPoint.value();
    }

    /**
     * Set Assembly Entry Point. Where to begin processing.
     * All runs everything.  Refilter means run from the re-filtering
     * of the coords files. hapltypes runs just the haplotypes
     * processing.
     *
     * @param value Assembly Entry Point
     *
     * @return this plugin
     */
    public AssemblyHaplotypesPlugin entryPoint(ASSEMBLY_ENTRY_POINT value) {
        entryPoint = new PluginParameter<>(entryPoint, value);
        return this;
    }
    
    /**
     * Minimum length of inversion for it to be kept as part
     * of the alignment.  Default is 7500.
     *
     * @return Minimum Inversion Length
     */
    public Integer minInversionLen() {
        return minInversionLen.value();
    }

    /**
     * Set Minimum Inversion Length. Minimum length of inversion
     * for it to be kept as part of the alignment.  Default is 7500
     *
     * @param value Minimum Inversion Length
     *
     * @return this plugin
     */
    public AssemblyHaplotypesPlugin minInversionLen(Integer value) {
        minInversionLen = new PluginParameter<>(minInversionLen, value);
        return this;
    }

    /**
     * Method name to load to db for assembly processing,
     * default is mummer4
     *
     * @return Assembly Method
     */
    public String assemblyMethod() {
        return assemblyMethod.value();
    }

    /**
     * Set Assembly Method. Method name to load to db for
     * assembly processing, default is mummer4
     *
     * @param value Assembly Method
     *
     * @return this plugin
     */
    public AssemblyHaplotypesPlugin assemblyMethod(String value) {
        assemblyMethod = new PluginParameter<>(assemblyMethod, value);
        return this;
    }

}
