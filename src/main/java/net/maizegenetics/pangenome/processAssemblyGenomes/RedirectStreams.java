/**
 * 
 */
package net.maizegenetics.pangenome.processAssemblyGenomes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * This comes from 
 *   https://stackoverflow.com/questions/35706921/redirecting-the-output-of-a-process-into-the-input-of-another-process-using-proc/35709166
 * It is used to aid in piping output from 1 ProcessBuilder command to another.
 * 
 * @author lcj34
 *
 */
public class RedirectStreams {
    public RedirectStreams(Process process1, Process process2) {
        final Process tmpProcess1 = process1;
        final Process tmpProcess2 = process2;
        new Thread(new Runnable() {
            @Override
            public void run() {
                String lineToPipe;

                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(tmpProcess1.getInputStream()));
                     BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(tmpProcess2.getOutputStream()));){

                    while ((lineToPipe = bufferedReader.readLine()) != null){
                        //System.out.println("Output process1 / Input process2:" + lineToPipe);
                        bufferedWriter.write(lineToPipe + '\n');
                        bufferedWriter.flush();
                    }
                    System.out.println("RedirectStream: finished writing process1 output to process2 input");

                } catch (IOException ioe) {
                    ioe.printStackTrace();
                    throw new IllegalStateException("RedirectStreams: error processing piped java stream output files");
                }
            }
        }).start();
    }
}
