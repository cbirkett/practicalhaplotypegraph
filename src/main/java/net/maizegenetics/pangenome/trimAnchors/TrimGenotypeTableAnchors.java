package net.maizegenetics.pangenome.trimAnchors;

import com.google.common.collect.HashMultiset;
import net.maizegenetics.dna.map.Chromosome;
import net.maizegenetics.dna.map.GeneralPosition;
import net.maizegenetics.dna.map.PositionList;
import net.maizegenetics.dna.map.PositionListBuilder;
import net.maizegenetics.dna.snp.*;
import net.maizegenetics.taxa.TaxaList;
import net.maizegenetics.util.GeneralAnnotationStorage;
import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * Simple class holding utilities to trim a genotype table by identity and coverage thresholds
 * TODO needs to be refractored and have a standardized api
 * Created by zrm22 on 7/6/17.
 */
public class TrimGenotypeTableAnchors {

    private static final Logger myLogger = Logger.getLogger(TrimGenotypeTableAnchors.class);

    private TrimGenotypeTableAnchors() {
        //Do not instantiate this class
    }


    /**
     * Method which will create a trimmed GenotypeTable based on the gene start and end positions
     * @param fastaFileName Import fasta file to be read.
     * @param refTaxaName Name of the Reference taxa(ex B73Ref) which is a row in the fasta file.
     * @param idThreshold Percentage of Identity which needs to be met to be considered conserved.
     * @param coverageThreshold Percentage of Coverage across all lines in the fasta file which needs to be met to be considered conserved.
     * @param leftTrimStart Start point for the left direction trimming.  Generally this is the gene start.
     * @param rightTrimStart Start point for the right direction trimming.  Generally this is the gene end.
     * @param consecFailingThresholds Number of consecutive base pairs which need to fail to stop growing the region and start walking back to the gene bounds.
     * @param consecPassingThresholds Number of consecutive base pairs which need to pass the thresholds to stop shrinking the region and stop trimming.
     * @return GenotypeTable which is trimmed by the input parameters
     */
    public static GenotypeTable createTrimmedFastaGenotypeTable(String fastaFileName, String refTaxaName,
                                                                double idThreshold, double coverageThreshold, int leftTrimStart,
                                                                int rightTrimStart, int consecFailingThresholds, int consecPassingThresholds) {
        //Get the chromosome start and end position from the fastaFileName
        //split on _
        String[] parseOffExtension = fastaFileName.split("\\.");
        String[] fileNameSplit = parseOffExtension[0].split("_");
        String chr = "";
        int startPos = 0;
        for(String token : fileNameSplit) {
            if(token.startsWith("chr")) {
                chr = token.substring(3);
            }
            else if(token.startsWith("stPos")){
                startPos = Integer.parseInt(token.substring(5));
            }
        }

        return createTrimmedFastaGenotypeTable(fastaFileName,refTaxaName,chr,startPos,idThreshold,coverageThreshold,leftTrimStart, rightTrimStart, consecFailingThresholds,consecPassingThresholds);
    }

    /**
     * Method which will create a trimmed GenotypeTable based on the gene start and end positions
     * @param fastaFileName Import fasta file to be read.
     * @param refTaxaName Name of the Reference taxa(ex B73Ref) which is a row in the fasta file.
     * @param chr Name of the chromosome we need for the GenotypeTable
     * @param startPos Start position of the reference sequence so we can annotate the Position list
     * @param idThreshold Percentage of Identity which needs to be met to be considered conserved.
     * @param coverageThreshold Percentage of Coverage across all lines in the fasta file which needs to be met to be considered conserved.
     * @param leftTrimStart Start point for the left direction trimming.  Generally this is the gene start.
     * @param rightTrimStart Start point for the right direction trimming.  Generally this is the gene end.
     * @param consecFailingThresholds Number of consecutive base pairs which need to fail to stop growing the region and start walking back to the gene bounds.
     * @param consecPassingThresholds Number of consecutive base pairs which need to pass the thresholds to stop shrinking the region and stop trimming.
     * @return GenotypeTable which is trimmed by the input parameters
     */
    public static GenotypeTable createTrimmedFastaGenotypeTable(String fastaFileName, String refTaxaName, String chr, int startPos,
                                                                double idThreshold, double coverageThreshold, int leftTrimStart,
                                                                int rightTrimStart, int consecFailingThresholds, int consecPassingThresholds) {
        try {
            GenotypeTable gt = ImportUtils.readFasta(fastaFileName);
            //now that we have loaded in the genotype table, we need to create a new position list

            PositionListBuilder positionListBuilder = new PositionListBuilder(gt.numberOfSites());
            PositionList currentPositionList = gt.positions();

            //get the index of the referenceTaxa
            TaxaList taxaList = gt.taxa();
            int refTaxaIndex = taxaList.indexOf(refTaxaName);


            //handle the first "hanging" positions where we do not have a reference Position
            int startRefDeletionCounters = 0;
            int startOfRefIndex = 0;
            for (int i = 0; i < currentPositionList.size(); i++) {
                if (gt.genotype(refTaxaIndex, i) == NucleotideAlignmentConstants.GAP_ALLELE|| gt.genotype(refTaxaIndex, i) == NucleotideAlignmentConstants.GAP_DIPLOID_ALLELE) {
                    startRefDeletionCounters++;
                } else {
                    startOfRefIndex = i;
                    break;
                }
            }

            //loop through the currentPositionList and reset the position/cm
            int deletionCounter = -1 * startRefDeletionCounters;
            int currentPosition = startPos;
            //Need to have a boolean to keep track of if we are at the start
            //Otherwise we cannot do negative CM values correctly
            boolean atStart = true;
            int leftTrimIndex = -1;
            int rightTrimIndex = -1;
            PositionListBuilder plb = new PositionListBuilder();
            Chromosome currentChrom = new Chromosome(chr);
            for (int i = 0; i < currentPositionList.size(); i++) {
                if (gt.genotype(refTaxaIndex, i) == NucleotideAlignmentConstants.GAP_ALLELE || gt.genotype(refTaxaIndex, i) == NucleotideAlignmentConstants.GAP_DIPLOID_ALLELE) {
                    deletionCounter++;
                } else {
                    deletionCounter = 0;
                    if(atStart) {
                        atStart = false;
                    }
                    else {
                        currentPosition++;
                    }
                    if(currentPosition==leftTrimStart) {
                        leftTrimIndex = i;
                    }
                    if(currentPosition==rightTrimStart) {
                        rightTrimIndex = i;
                    }
                }
                GeneralPosition.Builder posBuilder = new GeneralPosition.Builder(currentPositionList.get(i));
                plb.add(posBuilder.chromosome(currentChrom).position(currentPosition).insertionPosition((short)deletionCounter).build());
                //currentPositionList.set(i, posBuilder.chromosome(currentChrom).position(currentPosition).cM(deletionCounter).build());

            }

            GenotypeTable result = GenotypeTableBuilder.getInstance(gt.genotypeMatrix(), plb.build(), taxaList);

            int leftTrimmedSite = findTrimIndex(result,leftTrimIndex,false,idThreshold,coverageThreshold,consecFailingThresholds, consecPassingThresholds);
            int rightTrimmedSite = findTrimIndex(result, rightTrimIndex,true, idThreshold, coverageThreshold,consecFailingThresholds, consecPassingThresholds);


            if(leftTrimmedSite==-1) {
                leftTrimmedSite = 0;
            }
            if(rightTrimmedSite==-1) {
                rightTrimmedSite = result.positions().size()-1;
            }

            GenotypeTable trimmedResult = FilterGenotypeTable.getInstance(result, leftTrimmedSite, rightTrimmedSite);

            //Need to create a new genotypetable with annos
            GeneralAnnotationStorage.Builder annoBuilder = GeneralAnnotationStorage.getBuilder();
            annoBuilder.addAnnotation("leftTrimCount",leftTrimmedSite);
            annoBuilder.addAnnotation("rightTrimCount",result.positions().size() - rightTrimmedSite-1);

            GenotypeTable trimmedResultWithAnnos = GenotypeTableBuilder.getInstance(trimmedResult.genotypeMatrix(), trimmedResult.positions(), trimmedResult.taxa(),
                    null, null,null,null,annoBuilder.build());


            System.out.println("Number of positions before trimming: "+result.positions().size());
            System.out.println("Number of positions after trimming: "+trimmedResult.positions().size());
            System.out.println("LeftIndex: "+leftTrimmedSite+", Original: "+leftTrimIndex);
            System.out.println("RightIndex: "+rightTrimmedSite+", Original: "+rightTrimIndex);
            System.out.println("**************************************");
            return trimmedResultWithAnnos;


        }
        catch(IOException e) {
            myLogger.debug(e.getMessage(), e);
            throw new IllegalStateException("Problem loading file: " + fastaFileName + ".\n  Error: " + e.getMessage());
        }
    }

    /**
     * Simple method to find the trim index using the thresholds, start position and direction.
     * @param gt Input GenotypeTable which is used to check the coverage and identity..
     * @param positionStart Position where algorithm starts walking.
     * @param direction Direction in which to walk
     * @param idThreshold Identity Threshold which is used to determine conservation
     * @param covThreshold Coverage Threshold which is used to determine conservation
     * @param consecutiveThreshold Number of consecutive base pairs which need to fail to stop expanding the region
     * @param consecPassingThreshold Number of consecutive base pairs which need to pass the thresholds to stop trimming
     * @return index of where the genotypeTable will be trimmed
     */
    private static int findTrimIndex(GenotypeTable gt, int positionStart, boolean direction, double idThreshold, double covThreshold, int consecutiveThreshold, int consecPassingThreshold) {
        //direction = false go left
        //direction = true go right
        //returns the index in the positionList

        //get the positionListIndex
        int consecutiveCounter = 0;
        if(direction) {
            int stepOnePositionIndex = -1;
            for(int i = positionStart+1; i < gt.positions().size(); i++) {
                if(!checkThreshold(gt,i,idThreshold,covThreshold)) {
                    consecutiveCounter++;
                    if(consecutiveCounter>=consecutiveThreshold) {
                        stepOnePositionIndex = i - consecutiveCounter;
                        break;
                    }
                }
                else {
                    consecutiveCounter = 0;
                }
            }

            // System.out.println(stepOnePositionIndex);
            if(stepOnePositionIndex == -1) {
                return -1;
            }
            else {
                //loop back towards the gene to find consecutive good base pair
                consecutiveCounter = 0;
                for(int i = stepOnePositionIndex; i >positionStart+1; i--) {
                    if(checkThreshold(gt,i,idThreshold,covThreshold)) {
                        consecutiveCounter++;
                        if(consecutiveCounter>=consecPassingThreshold) {
                            return i + consecutiveCounter-1;
                        }
                    }
                    else {
                        consecutiveCounter = 0;
                    }
                }
            }
            return stepOnePositionIndex;

        }
        else {
            int stepOnePositionIndex = -1;
            for(int i = positionStart-1; i>=0; i--) {
                if(!checkThreshold(gt,i,idThreshold,covThreshold)) {
                    consecutiveCounter++;
                    if(consecutiveCounter>=consecutiveThreshold) {
                        stepOnePositionIndex = i + consecutiveCounter;
                        break;
                    }
                }
                else {
                    consecutiveCounter = 0;
                }
            }

            if(stepOnePositionIndex == -1) {
                return -1;
            }
            else {
                //loop back towards the gene to find consecutive good base pair
                consecutiveCounter = 0;
                for(int i = stepOnePositionIndex; i <positionStart-1; i++) {
                    if(checkThreshold(gt,i,idThreshold,covThreshold)) {
                        consecutiveCounter++;
                        if(consecutiveCounter>=consecPassingThreshold) {
                            return i - consecutiveCounter+1;
                        }
                    }
                    else {
                        consecutiveCounter = 0;
                    }
                }
            }
            return stepOnePositionIndex;
        }
    }

    /**
     * Simple method to check if the genotype calls at a provided position pass or fail the thresholds.
     * @param gt Input GenotypeTable which will be used to check if the position passes the thresholds.
     * @param posIndex Position to be checked
     * @param idThreshold Input threshold for Identity
     * @param covThreshold Input threshold for Coverage
     * @return true if the position is above both threshold values.
     */
    //Return true if the position is above both threshold values
    private static boolean checkThreshold(GenotypeTable gt,int posIndex, double idThreshold, double covThreshold) {
        boolean passesThreshold = false;

        TaxaList tl = gt.taxa();
        //calculate the number of taxa that are not a gap
        int coverageCount = 0;
        HashMultiset<Byte> idCountMap =  HashMultiset.create();

        //Loop through the taxaList and get the genotypes
        for(int taxaIndex = 0; taxaIndex<tl.size(); taxaIndex++) {
            //If it is not a gap, we can count it as coverage and we count for identity
            if(gt.genotype(taxaIndex,posIndex)!=NucleotideAlignmentConstants.GAP_ALLELE && gt.genotype(taxaIndex,posIndex)!=NucleotideAlignmentConstants.GAP_DIPLOID_ALLELE) {
                coverageCount++;
                idCountMap.add(gt.genotype(taxaIndex,posIndex));
            }
        }

        //Calculate if it passes the coverage threshold
        boolean passesCoverageThreshold = ((double)coverageCount/tl.size()>=covThreshold)?true:false;
        //Figure out what the most common key was in the Multiset
        int maxIdentity = Integer.MIN_VALUE;
        for(byte allele: idCountMap.elementSet()) {
            if(idCountMap.count(allele)>maxIdentity) {
                maxIdentity = idCountMap.count(allele);
            }
        }

        //If the count of the most common key divided by the coverage is above the id threshold, we want to trim here.
        //This was how it was in Terry's/Lynn's code
        boolean passesIdentityThreshold = ((double)maxIdentity/coverageCount>=idThreshold)?true:false;

        //Make sure we pass both thresholds
        if(passesCoverageThreshold && passesIdentityThreshold) {
            passesThreshold = true;
        }

        return passesThreshold;
    }

}
