package net.maizegenetics.pangenome.trimAnchors;

/**
 * Simple class which holds various information about an anchor.
 * TODO formalize this datastructure
 * Created by zrm22 on 6/8/17.
 */
public class AnchorInfo {
    private final int myAnchorId;
    private final String myChrom;
    private final int myAnchorStart;
    private final int myAnchorEnd;
    private final int myGeneStart;
    private final int myGeneEnd;
    private final int myLeftTrimCount;
    private final int myRightTrimCount;

    public AnchorInfo(int anchorId, String chrom, int anchorStart, int anchorEnd, int geneStart, int geneEnd) {
        this(anchorId, chrom, anchorStart, anchorEnd, geneStart, geneEnd, 0,0);
    }

    public AnchorInfo(int anchorId, String chrom, int anchorStart, int anchorEnd, int geneStart, int geneEnd, int leftTrimCount, int rightTrimCount) {
        myAnchorId = anchorId;
        myChrom = chrom;
        myAnchorStart = anchorStart;
        myAnchorEnd = anchorEnd;
        myGeneStart = geneStart;
        myGeneEnd = geneEnd;
        myLeftTrimCount = leftTrimCount;
        myRightTrimCount = rightTrimCount;
    }

    public int anchorId() { return myAnchorId; }

    public String chrom() {
        return myChrom;
    }

    public int anchorStart() {
        return myAnchorStart;
    }

    public int anchorEnd() {
        return myAnchorEnd;
    }

    public int geneStart() {
        return myGeneStart;
    }

    public int geneEnd() {
        return myGeneEnd;
    }

    public int leftTrimCount() { return myLeftTrimCount; }

    public int rightTrimCount() { return myRightTrimCount; }

}
