package net.maizegenetics.pangenome.hapcollapse;

import net.maizegenetics.analysis.distance.DistanceMatrixPlugin;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.ImportUtils;
import net.maizegenetics.taxa.Taxon;
import net.maizegenetics.taxa.distance.DistanceMatrix;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by mm2842 on 6/20/17.
 * Check the output of AnchorFastaReader.java using GenotypeTools for stats
 * check some of the input files vs the expected output (check that it is working as expected)
 * files in the box files are Ed code's output
 * The output files will be tested and a logging file with stats will be output
 */
public class QualityReportAnchorsToHaplotypes {
    public static final String workdir = "/Users/mm2842/Documents/Regulatory_Regions_Project/PHG_Hackhaton/";
    public static final String anchorIDFile = workdir + "test.txt";
    public static final String alignmentRaw = workdir+"Chrom10raw/";
    public static final String alignmentFiltered = workdir+"Chrom10CleanMSA/";
    public static final String consensusHaplotypes = workdir+"Chrom10CleanMSAConsensus/";
    public static final String reportFile = workdir+"temp/report.tsv";

    private static final double maxDistance=0.002; //maximum distance between taxa to merge
    private static final int minSites=100; //minimum number of sites to find a match

    public static String comparaRawFilteredAlignment (String inMSA, String outMSA) throws IOException  {
        GenotypeTable rawInGenotypeTable = ImportUtils.readFasta( inMSA );
        GenotypeTable cleanOutGenotypeTable = ImportUtils.readFasta( outMSA );

        //ratios for sites and taxa
        double sitesRatio = cleanOutGenotypeTable.numberOfSites() / (double) rawInGenotypeTable.numberOfSites();
        double taxaRatio = cleanOutGenotypeTable.taxa().size() / (double) rawInGenotypeTable.taxa().size();

        //string to be included in the report
        String lineToReport = String.format("%d\t%d\t%.2f\t%d\t%d\t%.2f\t",
                rawInGenotypeTable.numberOfSites(),
                cleanOutGenotypeTable.numberOfSites(),
                100 * sitesRatio,
                rawInGenotypeTable.taxa().size(),
                cleanOutGenotypeTable.taxa().size(),
                100 * taxaRatio);

        return lineToReport;
    }

    private static String statsGeneticDistanceAlignment(String alignmentFile) throws IOException {
        GenotypeTable genotypeTable = ImportUtils.readFasta( alignmentFile );
        DistanceMatrix matrixDistanceOriginal = DistanceMatrixPlugin.getDistanceMatrix(genotypeTable);

        //distance between haplotype calling and genome assembly
        double distBetweenB73 = distanceTaxaPair(matrixDistanceOriginal, "B73_ref", "B73_Haplotype_Caller");
        double distBetweenW22 = distanceTaxaPair(matrixDistanceOriginal, "W22Assembly", "W22_Haplotype_Caller");
        double distBetweenCML247 = distanceTaxaPair(matrixDistanceOriginal, "CML247Assembly", "CML247_Haplotype_Caller");
        double distW22assemblyToB73 = distanceTaxaPair(matrixDistanceOriginal, "B73_ref", "W22Assembly");
        double distW22hapcallToB73 = distanceTaxaPair(matrixDistanceOriginal, "B73_ref", "W22_Haplotype_Caller");
        double distCML247assemblyToB73 = distanceTaxaPair(matrixDistanceOriginal, "B73_ref", "CML247Assembly");
        double distCML247hapcallToB73 = distanceTaxaPair(matrixDistanceOriginal, "B73_ref", "CML247_Haplotype_Caller");

        //distance of all the taxa to B73 haplotype
        double distanceAllToB73 = distanceAllTaxaToGivenTaxa(matrixDistanceOriginal, "B73_ref");

        //averageDistance between all sequences
        double averageDistance = matrixDistanceOriginal.meanDistance();

        //string to be included in the report
        String lineToReport =String.format("%.4f\t%.4f\t%.4f\t%.4f\t%.4f",
                distBetweenB73,
                distBetweenW22,
                distBetweenCML247,
                distW22assemblyToB73,
                distW22hapcallToB73,
                distCML247assemblyToB73,
                distCML247hapcallToB73,
                distanceAllToB73,
                averageDistance);

        return lineToReport;
    }

    private static String statstConsensusHaplotypes(String cleanOutPath, String consensusOutPath) throws IOException {
        GenotypeTable cleanOutGenotypeTable = ImportUtils.readFasta( cleanOutPath );
        DecimalFormat formatter = new DecimalFormat("#0.0");
        StringBuffer lineToReport = new StringBuffer();
        lineToReport.append("");
        List<Double> taxaHapValues =  new ArrayList<Double>();
        int singletonHaps=0;
        double singletonTaxa = 0.0;
        try (BufferedReader br = new BufferedReader(new FileReader(consensusOutPath))){
            for(String line; (line = br.readLine()) != null; ){
                Set<String> haplotype_taxa = new HashSet<String>();
                if (line.startsWith(">")) {
                    Collections.addAll(haplotype_taxa, line.replace(">","").split(",|=")); // a set of uniq taxa names
                    double taxaInCurrentHaplotype = 100 * ( (double) haplotype_taxa.size()/cleanOutGenotypeTable.taxa().size() );
                    if (taxaInCurrentHaplotype >= 1.0) {
                        taxaHapValues.add(taxaInCurrentHaplotype);
                    } else {
                        singletonHaps++;
                        singletonTaxa = singletonTaxa + taxaInCurrentHaplotype;
                    }
                } else {
                    int countN = (int) (long) line.codePoints().filter(ch -> ch == 'N').count();
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        }
        taxaHapValues.sort((a, b) -> Double.compare(b, a));
        lineToReport.append(taxaHapValues.size()+":"+taxaHapValues.stream().map(i -> formatter.format(i)).collect( Collectors.joining(",","[","]") ));;
        lineToReport.append("\t"+singletonHaps+":["+formatter.format(singletonTaxa)+"]"+"\t");
        return lineToReport.toString();
    }


    public static void main(String[] args) {
        String header = "anchorID\tnumberOfSites\tnumberOfTaxa\tdistBetweenB73\tdistBetweenW22\tdistBetweenCML247\tdistanceAllToB73\taverageDistance";
        try (BufferedReader br = new BufferedReader(new FileReader(anchorIDFile))){ //I like more the dir crawling as Ed have it
            BufferedWriter writerRawReport = Files.newBufferedWriter(Paths.get(reportFile));
            for(String anchorID; (anchorID = br.readLine()) != null; ) {
                String MSAfile = "exportedFastaFromV2DB_anchorId"+anchorID+"_LongNsRemoved_MAFFTAligned_Trimmed.fa.gz";
                String rawfile = alignmentRaw+MSAfile;
                String cleanfile = alignmentFiltered+MSAfile;
                String consensus = consensusHaplotypes+"anchor"+anchorID+".fa";
                String towrite = statstConsensusHaplotypes(cleanfile,consensus);
                System.out.println(towrite);
                writerRawReport.write(comparaRawFilteredAlignment(rawfile, cleanfile));
                writerRawReport.write(statsGeneticDistanceAlignment(rawfile));
                writerRawReport.write(statsGeneticDistanceAlignment(cleanfile));
                writerRawReport.newLine();
            }
            writerRawReport.flush();
            writerRawReport.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static double distanceAllTaxaToGivenTaxa(DistanceMatrix matrixDistance, String taxaName) {
        int indexOfGivenTaxa = matrixDistance.getTaxaList().indexOf(taxaName);
        double AvgDistanceToGiven = Double.NaN;
        if (indexOfGivenTaxa > -1) {
            List<Float> distances = new ArrayList<Float>();
            for (Taxon t : matrixDistance.getTaxaList()) {
                int currentTaxaIdx = matrixDistance.getTaxaList().indexOf(t.getName());
                if (matrixDistance.getDistance(indexOfGivenTaxa, currentTaxaIdx) < 0.001){
                    distances.add(matrixDistance.getDistance(indexOfGivenTaxa, currentTaxaIdx));
                }
            }
            AvgDistanceToGiven = distances.stream().mapToDouble(val -> val).average().getAsDouble();
        }
        return AvgDistanceToGiven;
    }

    public static double distanceTaxaPair(DistanceMatrix matrixDistance, String taxaPairOne, String taxaPairTwo){
        int indexOfTaxaOne = matrixDistance.getTaxaList().indexOf(taxaPairOne);
        int indexOfTaxaTwo = matrixDistance.getTaxaList().indexOf(taxaPairTwo);
        double distBetweenTaxa = Double.NaN;
        if (indexOfTaxaOne > -1 && indexOfTaxaTwo > -1) {
            distBetweenTaxa = matrixDistance.getDistance(indexOfTaxaOne, indexOfTaxaTwo);
        }
        return distBetweenTaxa;
    }
}

