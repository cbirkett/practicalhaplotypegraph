/**
 * 
 */
package net.maizegenetics.pangenome.hapcollapse;

/**
 * This class used when processing consensus data.
 * 
 * @author lcj34
 *
 */
public class RefAltData {

    int variantId;
    String chrom;
    int position;
    int len;
    int refDepth;
    int altDepth;


    public RefAltData(int variantId, String chr, int pos, int length, int refDepth, int altDepth) {
        this.variantId = variantId;
        this.chrom = chr;
        this.position = pos;
        this.len = length;
        this.refDepth = refDepth;
        this.altDepth = altDepth;
    }


    /**
     * @return the variant_id
     */
    public int getVariantId() {
        return variantId;
    }


    /**
     * @return the chrom
     */
    public String getChrom() {
        return chrom;
    }


    /**
     * @return the position
     */
    public int getPosition() {
        return position;
    }


    /**
     * @return the len
     */
    public int getLen() {
        return len;
    }


    /**
     * @return the refDepth
     */
    public int getRefDepth() {
        return refDepth;
    }


    /**
     * @return the altDepth
     */
    public int getAltDepth() {
        return altDepth;
    }
 
}
