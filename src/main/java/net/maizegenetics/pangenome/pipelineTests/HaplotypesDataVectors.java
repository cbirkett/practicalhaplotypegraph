/**
 * 
 */
package net.maizegenetics.pangenome.pipelineTests;

/**
 * Class that holds data for haplotypes in a graph in vector form
 * 
 * @author lcj34
 *
 */
public class HaplotypesDataVectors {
    public int[] hapIds;
    public int[] refRangeIds;
    public int[] methodIds;
    public String[] taxa;
    public String[] sequence;    
    public String[] variantInfo;

    public HaplotypesDataVectors(int[] hapids, int[] refRangeIds, int[] methodIds, String[] taxa, String[] sequence, String[] variantInfo) {
        this.hapIds = hapids;
        this.refRangeIds = refRangeIds;
        this.methodIds = methodIds;
        this.taxa = taxa;
        this.sequence = sequence;
        this.variantInfo = variantInfo;
    }

}
