#!/bin/bash

license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201611.02
SENTIEON='/programs/sentieon-genomics-201611.02/bin/sentieon'
NUMBER_THREADS=20
REFERENCE='/workdir/terry/agpv4/Zea_mays.AGPv4.dna.toplevel.fa'
time $SENTIEON driver \
   -t $NUMBER_THREADS \
   -r $REFERENCE \
   -i /local/workdir/terry/BAMFiles/CML333/4881_6665_17285_HB1JCADXX_CML333_GTCCGC_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/CML333/4881_6665_17285_HFFGKADXX_CML333_GTCCGC_sorted_dedup.bam \
   -i /local/workdir/terry/BAMFiles/CML333/CML333_HL5WNCCXX_L3_sorted_dedup.bam \
   --interval /workdir/terry/anchorsFile_all10_MergedPlus1000orGapDiffcoordinates.intervals \
   --algo Haplotyper \
   --ploidy 1 \
   --emit_mode gvcf \
   --min_map_qual 48 \
   --min_base_qual 21 \
   CML333_haplotype_caller_output.g.vcf

