#!/bin/bash

LINE_ARRAY=(Mo18W Mo18W Mo18W OH7B OH7B OH7B M37W M37W M37W Il14H Il14H Il14H CML69 CML69 CML69 CML333 CML333 CML333 CML277 CML277 CML277 CML322 CML322 CML322 Ky21 Ky21 Ky21)
ID_ARRAY=(4882_6665_17308_HBEN2ADXX_Mo18W_CCGTCC 4882_6665_17308_HFFGKADXX_Mo18W_CCGTCC Mo18W_HL5WNCCXX_L6 4882_6665_17302_HBEN2ADXX_OH7B_TAGCTT 4882_6665_17302_HFFGKADXX_OH7B_TAGCTT OH7B_HL5WNCCXX_L6 4882_6665_17307_HBEN2ADXX_M37W_ATGTCA 4882_6665_17307_HFFGKADXX_M37W_ATGTCA M37W_HL5WNCCXX_L6 4881_6665_17290_HB1JCADXX_Il14H_GAGTGG 4881_6665_17290_HFFGKADXX_Il14H_GAGTGG Il14H_HL5WNCCXX_L4 4881_6665_17289_HB1JCADXX_CML69_CGTACG 4881_6665_17289_HFFGKADXX_CML69_CGTACG CML69_HL5WNCCXX_L4 4881_6665_17285_HB1JCADXX_CML333_GTCCGC 4881_6665_17285_HFFGKADXX_CML333_GTCCGC CML333_HL5WNCCXX_L3 4881_6665_17277_HB1JCADXX_CML277_GATCAG 4881_6665_17277_HFFGKADXX_CML277_GATCAG CML277_HL5WNCCXX_L4 4881_6665_17276_HB1JCADXX_CML322_ACTTGA 4881_6665_17276_HFFGKADXX_CML322_ACTTGA CML322_HL5WNCCXX_L4 4881_6665_17275_HB1JCADXX_Ky21_CAGATC 4881_6665_17275_HFFGKADXX_Ky21_CAGATC Ky21_HL5WNCCXX_L4)
FIRST_FILE_NAME=(4882_6665_17308_HBEN2ADXX_Mo18W_CCGTCC_R1.fastq.gz 4882_6665_17308_HFFGKADXX_Mo18W_CCGTCC_R1.fastq.gz Mo18W_HL5WNCCXX_L6_1.clean.renamed.fq.gz 4882_6665_17302_HBEN2ADXX_OH7B_TAGCTT_R1.fastq.gz 4882_6665_17302_HFFGKADXX_OH7B_TAGCTT_R1.fastq.gz OH7B_HL5WNCCXX_L6_1.clean.renamed.fq.gz 4882_6665_17307_HBEN2ADXX_M37W_ATGTCA_R1.fastq.gz 4882_6665_17307_HFFGKADXX_M37W_ATGTCA_R1.fastq.gz M37W_HL5WNCCXX_L6_1.clean.renamed.fq.gz 4881_6665_17290_HB1JCADXX_Il14H_GAGTGG_R1.fastq.gz 4881_6665_17290_HFFGKADXX_Il14H_GAGTGG_R1.fastq.gz Il14H_HL5WNCCXX_L4_1.clean.renamed.fq.gz 4881_6665_17289_HB1JCADXX_CML69_CGTACG_R1.fastq.gz 4881_6665_17289_HFFGKADXX_CML69_CGTACG_R1.fastq.gz CML69_HL5WNCCXX_L4_1.clean.renamed.fq.gz 4881_6665_17285_HB1JCADXX_CML333_GTCCGC_R1.fastq.gz 4881_6665_17285_HFFGKADXX_CML333_GTCCGC_R1.fastq.gz CML333_HL5WNCCXX_L3_1.clean.renamed.fq.gz 4881_6665_17277_HB1JCADXX_CML277_GATCAG_R1.fastq.gz 4881_6665_17277_HFFGKADXX_CML277_GATCAG_R1.fastq.gz CML277_HL5WNCCXX_L4_1.clean.renamed.fq.gz 4881_6665_17276_HB1JCADXX_CML322_ACTTGA_R1.fastq.gz 4881_6665_17276_HFFGKADXX_CML322_ACTTGA_R1.fastq.gz CML322_HL5WNCCXX_L4_1.clean.renamed.fq.gz 4881_6665_17275_HB1JCADXX_Ky21_CAGATC_R1.fastq.gz 4881_6665_17275_HFFGKADXX_Ky21_CAGATC_R1.fastq.gz Ky21_HL5WNCCXX_L4_1.clean.renamed.fq.gz)
SECOND_FILE_NAME=(4882_6665_17308_HBEN2ADXX_Mo18W_CCGTCC_R2.fastq.gz 4882_6665_17308_HFFGKADXX_Mo18W_CCGTCC_R2.fastq.gz Mo18W_HL5WNCCXX_L6_2.clean.renamed.fq.gz 4882_6665_17302_HBEN2ADXX_OH7B_TAGCTT_R2.fastq.gz 4882_6665_17302_HFFGKADXX_OH7B_TAGCTT_R2.fastq.gz OH7B_HL5WNCCXX_L6_2.clean.renamed.fq.gz 4882_6665_17307_HBEN2ADXX_M37W_ATGTCA_R2.fastq.gz 4882_6665_17307_HFFGKADXX_M37W_ATGTCA_R2.fastq.gz M37W_HL5WNCCXX_L6_2.clean.renamed.fq.gz 4881_6665_17290_HB1JCADXX_Il14H_GAGTGG_R2.fastq.gz 4881_6665_17290_HFFGKADXX_Il14H_GAGTGG_R2.fastq.gz Il14H_HL5WNCCXX_L4_2.clean.renamed.fq.gz 4881_6665_17289_HB1JCADXX_CML69_CGTACG_R2.fastq.gz 4881_6665_17289_HFFGKADXX_CML69_CGTACG_R2.fastq.gz CML69_HL5WNCCXX_L4_2.clean.renamed.fq.gz 4881_6665_17285_HB1JCADXX_CML333_GTCCGC_R2.fastq.gz 4881_6665_17285_HFFGKADXX_CML333_GTCCGC_R2.fastq.gz CML333_HL5WNCCXX_L3_2.clean.renamed.fq.gz 4881_6665_17277_HB1JCADXX_CML277_GATCAG_R2.fastq.gz 4881_6665_17277_HFFGKADXX_CML277_GATCAG_R2.fastq.gz CML277_HL5WNCCXX_L4_2.clean.renamed.fq.gz 4881_6665_17276_HB1JCADXX_CML322_ACTTGA_R2.fastq.gz 4881_6665_17276_HFFGKADXX_CML322_ACTTGA_R2.fastq.gz CML322_HL5WNCCXX_L4_2.clean.renamed.fq.gz 4881_6665_17275_HB1JCADXX_Ky21_CAGATC_R2.fastq.gz 4881_6665_17275_HFFGKADXX_Ky21_CAGATC_R2.fastq.gz Ky21_HL5WNCCXX_L4_2.clean.renamed.fq.gz)
# to get the arrayLength, ${#ArrayName[@]}

#Set up the senteion variables and the reference path
license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201611.02

SENTIEON='/programs/sentieon-genomics-201611.02/bin/sentieon'
SENTIEONBWA='/programs/sentieon-genomics-201611.02/bin/bwa'
NUMBER_THREADS=20
REFERENCE='/SSD/zrm22/BAMFileGeneration/agpv4Ref/Zea_mays.AGPv4.dna.toplevel.fa.gz'

for((i=0;i<${#LINE_ARRAY[@]};i++));
do
    SAMPLE1=/workdir/zrm22/ZeaWGSRawData/${LINE_ARRAY[i]}RawReads/${FIRST_FILE_NAME[i]}
    SAMPLE2=/workdir/zrm22/ZeaWGSRawData/${LINE_ARRAY[i]}RawReads/${SECOND_FILE_NAME[i]}
    SORTED_BAM_1=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted.bam
    echo $SAMPLE1
    echo $SAMPLE2
    echo $SORTED_BAM_1
 
    time $SENTIEONBWA mem -M -R '@RG\tID:${ID_ARRAY[i]} \tSM:${LINE_ARRAY[i]} \tPL:ILLUMINA' -t $NUMBER_THREADS $REFERENCE $SAMPLE1 $SAMPLE2 | $SENTIEON util sort -o $SORTED_BAM_1 -t $NUMBER_THREADS --sam2bam -i -

    SCORE_TXT=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted_score.txt
    DEDUP_METRIC_TXT=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted_dedupMetric.txt
    DEDUP_BAM=/workdir/zrm22/BAMFiles/${LINE_ARRAY[i]}/${ID_ARRAY[i]}_sorted_dedup.bam

    time $SENTIEON driver -t $NUMBER_THREADS -i $SORTED_BAM_1 --algo LocusCollector --fun score_info $SCORE_TXT

    time $SENTIEON driver -t $NUMBER_THREADS -i $SORTED_BAM_1 --algo Dedup --rmdup --score_info $SCORE_TXT --metrics $DEDUP_METRIC_TXT $DEDUP_BAM
done
