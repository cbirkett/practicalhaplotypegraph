#!/bin/bash

# Manual: http://www.htslib.org/doc/samtools.html

cd BAMFiles
bamfiles="`ls *.bam | cut -d. -f1`"
cd ..

for bam in $bamfiles
do

echo $bam

# filter command:
samtools view -b -o FilteredBAMFiles/${bam}_trimmed_intervals_mapQ_48.bam -L hackathon_trimmed_intervals_plusminus1000.bed -q 48 BAMFiles/${bam}.bam

# make a new index for your filtered BAM file:
samtools index FilteredBAMFiles/${bam}_trimmed_intervals_mapQ_48.bam

done


