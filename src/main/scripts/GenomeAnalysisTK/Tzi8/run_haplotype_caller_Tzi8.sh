#!/bin/bash

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   --num_cpu_threads_per_data_thread 20 \
   -T HaplotypeCaller \
   -R ../../maize.fa \
   -I /SSD/terry/haplotype_caller/bam_files/Tzi8_HB1JEADXX_GTGAAA.bam \
   -I /SSD/terry/haplotype_caller/bam_files/Tzi8_HLC27CCXX_L8.clean_srt_dedup.bam \
   -I /SSD/terry/haplotype_caller/bam_files/Tzi8_HB1LCADXX_GTGAAA.bam \
   --emitRefConfidence GVCF \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -o Tzi8_haplotype_caller_output.g.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T GenotypeGVCFs \
   -R ../../maize.fa \
   --variant Tzi8_haplotype_caller_output.g.vcf \
   -stand_call_conf 30 \
   -o Tzi8_haplotype_caller_output.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T FastaAlternateReferenceMaker \
   -R ../../maize.fa \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -V Tzi8_haplotype_caller_output.vcf \
   -o Tzi8_haplotype_caller_output.fasta

