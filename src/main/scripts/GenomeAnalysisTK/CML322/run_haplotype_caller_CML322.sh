#!/bin/bash

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   --num_cpu_threads_per_data_thread 20 \
   -T HaplotypeCaller \
   -R ../../maize.fa \
   -I /SSD/terry/haplotype_caller/bam_files/CML322_HL5WNCCXX_L4.clean_srt_dedup.bam \
   -I /SSD/terry/haplotype_caller/bam_files/CML322_HB1JCADXX_ACTTGA.bam \
   -I /SSD/terry/haplotype_caller/bam_files/CML322_HFFGKADXX_ACTTGA.bam \
   --emitRefConfidence GVCF \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -o CML322_haplotype_caller_output.g.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T GenotypeGVCFs \
   -R ../../maize.fa \
   --variant CML322_haplotype_caller_output.g.vcf \
   -stand_call_conf 30 \
   -o CML322_haplotype_caller_output.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T FastaAlternateReferenceMaker \
   -R ../../maize.fa \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -V CML322_haplotype_caller_output.vcf \
   -o CML322_haplotype_caller_output.fasta

