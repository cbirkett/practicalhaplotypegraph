#!/bin/bash

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   --num_cpu_threads_per_data_thread 20 \
   -T HaplotypeCaller \
   -R ../../maize.fa \
   -I /SSD/terry/haplotype_caller/bam_files/Mo18W_HBEN2ADXX_CCGTCC.bam \
   -I /SSD/terry/haplotype_caller/bam_files/Mo18W_HL5WNCCXX_L5.clean_srt_dedup.bam \
   -I /SSD/terry/haplotype_caller/bam_files/Mo18W_HFFGKADXX_CCGTCC.bam \
   --emitRefConfidence GVCF \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -o Mo18W_haplotype_caller_output.g.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T GenotypeGVCFs \
   -R ../../maize.fa \
   --variant Mo18W_haplotype_caller_output.g.vcf \
   -stand_call_conf 30 \
   -o Mo18W_haplotype_caller_output.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T FastaAlternateReferenceMaker \
   -R ../../maize.fa \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -V Mo18W_haplotype_caller_output.vcf \
   -o Mo18W_haplotype_caller_output.fasta

