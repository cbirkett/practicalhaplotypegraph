#!/bin/bash

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   --num_cpu_threads_per_data_thread 20 \
   -T HaplotypeCaller \
   -R ../../maize.fa \
   -I /SSD/terry/haplotype_caller/bam_files/NC358_HFFGKADXX_TTAGGC.bam \
   -I /SSD/terry/haplotype_caller/bam_files/NC358_HL5WNCCXX_L4.clean_srt_dedup.bam \
   -I /SSD/terry/haplotype_caller/bam_files/NC358_HB1JCADXX_TTAGGC.bam \
   --emitRefConfidence GVCF \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -o NC358_haplotype_caller_output.g.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T GenotypeGVCFs \
   -R ../../maize.fa \
   --variant NC358_haplotype_caller_output.g.vcf \
   -stand_call_conf 30 \
   -o NC358_haplotype_caller_output.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T FastaAlternateReferenceMaker \
   -R ../../maize.fa \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -V NC358_haplotype_caller_output.vcf \
   -o NC358_haplotype_caller_output.fasta

