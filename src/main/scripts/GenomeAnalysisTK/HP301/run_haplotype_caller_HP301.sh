#!/bin/bash

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   --num_cpu_threads_per_data_thread 20 \
   -T HaplotypeCaller \
   -R ../../maize.fa \
   -I /SSD/terry/haplotype_caller/bam_files/HP301_HBEN2ADXX_GAGTGG.bam \
   -I /SSD/terry/haplotype_caller/bam_files/HP301_HL5WNCCXX_L6.clean_srt_dedup.bam \
   -I /SSD/terry/haplotype_caller/bam_files/HP301_HFFGKADXX_GAGTGG.bam \
   --emitRefConfidence GVCF \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -o HP301_haplotype_caller_output.g.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T GenotypeGVCFs \
   -R ../../maize.fa \
   --variant HP301_haplotype_caller_output.g.vcf \
   -stand_call_conf 30 \
   -o HP301_haplotype_caller_output.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T FastaAlternateReferenceMaker \
   -R ../../maize.fa \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -V HP301_haplotype_caller_output.vcf \
   -o HP301_haplotype_caller_output.fasta

