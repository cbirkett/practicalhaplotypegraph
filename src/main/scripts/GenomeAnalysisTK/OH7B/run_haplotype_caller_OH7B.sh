#!/bin/bash

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   --num_cpu_threads_per_data_thread 20 \
   -T HaplotypeCaller \
   -R ../../maize.fa \
   -I /SSD/terry/haplotype_caller/bam_files/OH7B_HL5WNCCXX_L6.clean_srt_dedup.bam \
   -I /SSD/terry/haplotype_caller/bam_files/OH7B_HFFGKADXX_TAGCTT.bam \
   -I /SSD/terry/haplotype_caller/bam_files/OH7B_HBEN2ADXX_TAGCTT.bam \
   --emitRefConfidence GVCF \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -o OH7B_haplotype_caller_output.g.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T GenotypeGVCFs \
   -R ../../maize.fa \
   --variant OH7B_haplotype_caller_output.g.vcf \
   -stand_call_conf 30 \
   -o OH7B_haplotype_caller_output.vcf

time java -Xmx100g -jar /SSD/gatk/GenomeAnalysisTK.jar \
   -T FastaAlternateReferenceMaker \
   -R ../../maize.fa \
   -L ../../overlapWindowsWithTrimmingThreshold2.intervals \
   -V OH7B_haplotype_caller_output.vcf \
   -o OH7B_haplotype_caller_output.fasta

