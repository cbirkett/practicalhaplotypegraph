#Data Readme

This repository will keep up to date versions of key files.  
We cannot store all the FASTQ, BAM, or DB files directly in this repository.

We will retain the list of files processed here with their full paths and md5.

For testing, we will try to place some test files into the a Git LFS.

Key files:
Testing Reference Genome of Maize:



Instruction for installing git LFS (large file service on your local machine)
````commandline
brew install git-lfs
git lfs install
 
````
If using Tower, just click on settings and enable the tracking with LFS.