This folder holds the results of randomly selecting(by hand) 20 anchors on chromosome 10 from the hackathon_trimmed.intervals file.  
They are as follows:
* >10:1049718-1053027
* >10:2048082-2049546
* >10:5037511-5038986   _HighNs_
* >10:7108509-7109597   _AllNs_
* >10:10568673-10569512
* >10:14155391-14155903 _HighNs_
* >10:29361245-29364158
* >10:72345008-72358928
* >10:88761617-88769115
* >10:147812443-147818648
* >10:40715044-40719323
* >10:30082820-30083380 _HighNs_
* >10:16864202-16869411
* >10:7212807-7213604   _AllNs_
* >10:3840240-3855309   _HighNs_
* >10:823861-825990
* >10:19359190-19359808 _HighNs_
* >10:23731605-23734016
* >10:34616171-34616984 _HighNs_
* >10:45702962-45704205
* >10:53540016-53540394

The ones which are tagged with HighNs or AllNs are in the directory /data/20AnchorMAFFTRunForTesting/HighNumberNs/.

From the ones I(Zack) have checked, I think the new pipeline with the filtering does do a bit better at differentiating from B73.  
Still, the majority of the sequence does match B73 completely(minus Ns), but the trees do seem better balanced than before.
I am still not seeing many cases where there are very deep Non-B73 branches, but that may be because I need to look at them all one by one.

Just to be certain 10:72345008-72358928 looks like a good anchor as we have a branch clustered to B73 and then an additional branch clustered to a bunch of other stuff.
Looking at the genotype table, it might be due to the high number of Gaps caused by shrinking down consecutive runs of Ns, but a lot of the deletions do seem real.